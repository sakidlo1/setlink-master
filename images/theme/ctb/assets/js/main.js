(function() {
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: false,
        format: 'yyyy-mm-dd'
    });

    $('.timepicker').timepicker({
        defaultTIme : false,
        showMeridian : false,
        minuteStep : 1
    });

    $('.home-grid-logo').owlCarousel({
        navText: ['', ''],
        nav: false,
        dots: true,
        items: 1,
        autoplay: true,
        animateOut: 'fadeOut',
        autoHeight: true
    });

    $('.banner').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        items: 1,
        autoplay:true,
        animateOut: 'fadeOut',
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.slide-article, .slide-activity').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        items: 1,
        autoHeight:true
    });

    $('.slide-home-activity, .slide-home-tips').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            768: {
                items: 3,
                margin: 20
            }
        }
    });

    $('.navbar-toggle, #overlay').click(function(){
        $('body').toggleClass('show-menu');
    });

    $('.gallery').each(function(){
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
              enabled:true
            }
        });
    });

    AOS.init({
        duration: 800
    });

})();