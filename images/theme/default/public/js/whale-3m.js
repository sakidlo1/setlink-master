$(document).ready(function() {
    $('.wrap-3M').inview(function(wrap) {
        var child_3m = $(wrap).find('img');
        child_3m.el.forEach((child,index) => {
            setTimeout(() => {
                $(child).addClass('show');
            }, (index*100));
        });
    });
});