var playerID = 0;
var Player = function(elem, source,opts) {
    // create element
    this.parent = elem;
    this.parent.html('<div class="wrap-video"></div>');
    this.wrap = this.parent.find('.wrap-video');

    // init
    this.id = ++playerID;
    this.source = source;
    this.options = $.extend({}, {
        muted : false,
        autoplay : false,
        playsinline : false,
        controls : false,
        loop : false,
        clickable : false,
        inview : false,
        onReady : function(){},
        onPlay : function(){},
        onPause : function(){},
        onEnded : function(){},
    }, opts);
    var src = '';

    if (this.isYoutube()) {
        this.el = document.createElement('iframe');
        this.el.setAttribute('frameborder','0');
        this.el.setAttribute('allow','accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture');
        var youtubeID = this.youtubeGetID();
        var param = {};
        param['enablejsapi'] = '1';
        param['modestbranding'] = '0';
        param['rel'] = '0';
        param['showinfo'] = '0';
        param['controls'] = '0';
        if (this.options.autoplay) {
            param['autoplay'] = '1';
        }
        if (this.options.loop) {
            param['loop'] = '1';
            param['playlist'] = youtubeID;
        }
        if (this.options.muted) {
            param['mute'] = '1';
        }
        if (this.options.playsinline) {
            param['playsinline'] = '1';
        }
        if (this.options.controls) {
            param['controls'] = '1';
        }
        src = 'https://www.youtube.com/embed/' + youtubeID + '?' + serialize(param);
    } else {
        this.el = document.createElement('video');
        this.el.setAttribute('preload', '');
        if (this.options.autoplay) {
            this.el.setAttribute('autoplay', this.options.autoplay);
        }
        if (this.options.loop) {
            this.el.setAttribute('loop', this.options.loop);
        }
        if (this.options.muted) {
            this.el.setAttribute('muted', this.options.muted);
        }
        if (this.options.controls) {
            this.el.setAttribute('controls', this.options.controls);
        }
        if (this.options.playsinline) {
            this.el.setAttribute('playsinline', this.options.playsinline);
            this.el.setAttribute('webkit-playsinline', this.options.playsinline);
        }
        src = this.source;
    }
    // if (this.options.autoplay === true) {
    //     if (_mobile.android()) return;
    //     if (_mobile.ios() && this.isYoutube()) return;
    // }
    this.$el = $(this.el);
    this.$el.addClass('video-element');
    this.wrap.append(this.$el)
    this.listen();
    if (this.options.muted) this.mute();
    this.parent.addClass('player');
    this.el.setAttribute('src', src);
    // this.action();
    return this;
};
Player.prototype.isHTML5 = function() {
    return this.el.tagName === 'VIDEO';
};
Player.prototype.isIFrame = function() {
    return this.el.tagName === 'IFRAME';
};
Player.prototype.isYoutube = function() {
    return !!this.source.match(/\/\/.*?youtube(-nocookie)?\.[a-z]+\/(watch\?v=[^&\s]+|embed)|youtu\.be\/.*/);
};
Player.prototype.youtubeGetID = function() {
    var ID = '';
    var url = this.source.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if (url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        ID = ID[0];
    } else {
        ID = url;
    }
    return ID;
};
Player.prototype.play = function() {
    if (this.isIFrame()) {
        this.post({ func: 'playVideo', method: 'play' });
    } else if (this.isHTML5()) {
        try {
            this.el.play();
        } catch (e) {}
    }
};
Player.prototype.pause = function() {
    if (this.isIFrame()) {
        this.post({ func: 'pauseVideo', method: 'pause' });
    } else if (this.isHTML5()) {
        this.el.pause();
    }
};
Player.prototype.mute = function() {
    if (this.isIFrame()) {
        this.post({func: 'mute', method: 'setVolume', value: 0});
    } else if (this.isHTML5()) {
        this.el.muted = true;
        this.el.setAttribute('muted', '');
    }
};
Player.prototype.post = function(cmd) {
    try {
        this.el.contentWindow.postMessage(JSON.stringify($.extend({ event: 'command' }, cmd)), '*');
    } catch (e) { console.error(e); }
};
Player.prototype.paused = function() {
    if (this.isIFrame()) {
        return this.isPause;
    } else if (this.isHTML5()) {
        return this.el.paused;
    }
};
Player.prototype.destroy = function() {
    $(window).off('message.'+this.id);
    $(window).off('resize.'+this.id);
    $(window).off('scroll.'+this.id);
    this.parent.find('.wrap-action').off('click.'+this.id);
    this.parent.empty();
    this.parent.removeClass('player');
};
Player.prototype.resize = function(ratio) {
    var _this = this;
    var blockVideo = this.parent;
    var _event = 'resize.' + this.id;
    var _resize = function(){
        var blockWidth = blockVideo.get(0).outerWidth;
        var blockHeight = blockVideo.get(0).outerHeight;
        var width = blockWidth;
        var height = width / ratio;
        if (height < blockHeight) {
            height = blockHeight;
            width = height * ratio;
        }
        _this.$el.width(Math.ceil(width));
        _this.$el.height(Math.ceil(height));
    };
    $(window).off(_event);
    $(window).on(_event,_resize);
    _resize();
};
Player.prototype.listen = function() {
    var _this = this;
    if (this.isIFrame()) {
        this.isPause = true;
        var poller;
        const postYoutube = () => {
            var listener = function () { return _this.post({ event: 'listening', id: _this.id }); };
            poller = setInterval(listener, 100);
            listener();
            this.$el.on('load.' + _this.id, postYoutube);
        }
        this.$el.on('load.' + _this.id, postYoutube);
        $(window).on('message.'+_this.id, function (e) {
            try {
                var data = JSON.parse(e.data);
                if (data.id !== _this.id) return;
                if (data.event && data.event == 'onReady') {
                    _this.ratio = 9 / 16;
                    _this.resize(_this.ratio);
                    _this.options.onReady();
                    poller && clearInterval(poller);
                } else if (data.info && typeof(data.info.playerState) !== 'undefined') {
                    switch (data.info.playerState) {
                        case 0:
                            _this.options.onEnded();
                        break;
                        case 1:
                            _this.options.onPlay();
                            _this.isPause = false;
                        break;
                        case 2:
                            _this.options.onPause();
                        break;
                        case 5:
                            // video cued
                        break;
                    }
                    if (data.info.playerState !== 1) _this.isPause = true;
                }
            } catch (e) {
                return;
            }
        });
    } else if (this.isHTML5()) {
        this.el.onended = function(){
            _this.options.onEnded();
        };
        this.el.onplay = function(){
            _this.options.onPlay();
        };
        this.el.onpause = function(){
            _this.options.onPause();
        };
        this.el.onloadeddata = function(){
            if (_this.el.readyState >= 2) {
                _this.ratio = _this.el.videoHeight / _this.el.videoWidth;
                _this.resize(_this.ratio);
                _this.options.onReady();
            }
        };
    }
};