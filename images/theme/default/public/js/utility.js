;(function(global){

    function isDocument(selector) {
        return document == selector && selector.nodeType == 9;
    }
    function isWindow(selector) {
        return window == selector;
    }
    function isObject(obj) {
        return obj !== null && typeof obj === 'object';
    }
    function isElement(obj) {
        return nodeType(obj) === 1;
    }
    function nodeType(obj) {
        return !isWindow(obj) && isObject(obj) && obj.nodeType;
    }
    function isNode(obj) {
        return nodeType(obj) >= 1;
    }
    function toArray(arr) {
        return Array.from(arr);
    }
    function toNode(element) {
        return toNodes(element)[0];
    }
    function toNodes(element) {
        return element && (isNode(element) ? [element] : toArray(element).filter(isNode)) || [];
    }
    function createEvent(e, bubbles, cancelable, detail) {
        if ( bubbles === void 0 ) bubbles = true;
        if ( cancelable === void 0 ) cancelable = false;

        if (typeof e === 'string') {
            var event = document.createEvent('CustomEvent'); // IE 11
            event.initCustomEvent(e, bubbles, cancelable, detail);
            e = event;
        }

        return e;
    }

    var strPrototype = String.prototype;
    var startsWithFn = strPrototype.startsWith || function (search) { return this.lastIndexOf(search, 0) === 0; };

    function startsWith(str, search) {
        return startsWithFn.call(str, search);
    }
    function scrollTo(el,mode,value,duration) {
        // https://plainjs.com/javascript/styles/get-and-set-scroll-position-of-an-element-26/
        var scrollMode = mode === 'top' ? 'scrollTop' : 'scrollLeft' ;
        if (value == undefined) {
            // get
            var pageOffset = mode === 'top' ? 'pageYOffset' : 'pageXOffset' ;
            el = Array.isArray(el) ? el[0] : el;
            return !isElement(el) ? (window[pageOffset] || document.documentElement[scrollMode]) : el[scrollMode] ;
        } else if (typeof value == 'number') {
            // set
            // https://codepen.io/rebosante/pen/eENYBv
            if (typeof(duration) === 'undefined') duration = 1000;
            return new Promise.all(
                el.map(function(el){
                    return new Promise(function(resolve) {
                        var start = $(el)[scrollMode]();
                        var change = parseInt(value) - start;
                        var currentTime = 0;
                        var increment = 20;
                        var easeInOutQuad = function (t, b, c, d) {
                            t /= d/2;
                            if (t < 1) return c/2*t*t + b;
                            t--;
                            return -c/2 * (t*(t-2) - 1) + b;
                        };
                        var animateScroll = function(){        
                            currentTime += increment;
                            var val = easeInOutQuad(currentTime, start, change, duration);
                            moveTo(val);
                            if(currentTime < duration) {
                                setTimeout(animateScroll, increment);
                            } else {
                                resolve();
                            }
                        };
                        var moveTo = function(val) {
                            if (!isElement(el)) {
                                document.documentElement[scrollMode] = document.body[scrollMode] = val;
                            } else {
                                el[scrollMode] = val;
                            }
                        };
                        if (duration > increment) {
                            animateScroll();
                        } else {
                            moveTo(parseInt(value));
                            resolve();
                        }
                    });
                })
            ).then(function() {
                return new Promise.resolve();
            });
        }
    }

    function $$(selector) {
        let arrElm = [];
        if (typeof selector == 'string') {
            document.querySelectorAll(selector).forEach(element => {
                arrElm.push(element);
            });
            return arrElm;
        } else if (selector instanceof HTMLElement) {
            return toArray([selector]);
        } else if (Array.isArray(selector)) {
            return selector;
        } else {
            return [window];
        }
    }
    function noJquery(selector,context) {
        this.el = isDocument(selector) ? [window] : $$(selector);
        this.nojquery = true;
        this.length = this.el.length;
    }
    var objEvent = {};
    noJquery.prototype = {
        hasClass : function(className) {
            var bool = undefined;
            this.el.forEach((el) => {
                if (className && el.classList.contains(className)) {
                    bool = true;
                } else {
                    if (!bool) {
                        bool = false;
                    }
                }
            });
            return bool;
        },
        addClass : function(cls) {
            var clsArr = cls.split(' ');
            this.el.forEach((el) => {
                clsArr.forEach(split_cls => {
                    el.classList.add(split_cls);
                });
            });
            return this;
        },
        removeClass: function(cls) {
            var clsArr = cls.split(' ');
            this.el.forEach((el) => {
                clsArr.forEach(split_cls => {
                    el.classList.remove(split_cls);
                });
            });
            return this;
        },
        find : function(selector) {
            let arr = [];
            this.el.forEach((el) => {
                if (isWindow(el)) el = document;
                arr = toArray(el.querySelectorAll(selector));
            });
            return new noJquery(arr);
        },
        attr : function(attrName, value) {
            if (typeof value === 'boolean') {
                if (value) {
                    value = '';
                } else {
                    value = null;
                }
            }
            if (value != undefined) {
                this.el.forEach(el => {
                    el.setAttribute(attrName, value);
                });
                return this;
            } else {
                return this.el[0].getAttribute(attrName);
            }
        },
        removeAttr : function(attr) {
            this.el.forEach((el) => {
                el.removeAttribute(attr);
            });
        },
        not : function(selector) {
            selector = new noJquery(selector);
            var selected = this.el.filter(function(elem) {
                return !selector.el.includes(elem);
            });
            return new noJquery(selected);
        },
        children: function() {
            let arrElm = [];
            this.el.forEach( el => {
                arrElm = arrElm.concat(toArray(this.el[0].children));
            })
            return new noJquery(arrElm);
        },
        on : function(eventName,fn) {
            var _event = eventName.split('.');
            var _off = [];
            this.el.forEach(function(el) {
                el.addEventListener(_event[0], fn);
                let callback_off = function() {
                    el.removeEventListener(_event[0], fn);
                };
                if (typeof(objEvent[eventName]) == 'undefined') objEvent[eventName] = [];
                objEvent[eventName].push([el,callback_off]);
                _off.push(callback_off);
            });
            return function(){
                while (_off.length > 0) {
                    (_off.shift())();
                }
            };
        },
        off : function(eventName) {
            var _this = this;
            if (objEvent[eventName] == undefined) return this;
            objEvent[eventName] = objEvent[eventName].filter(function(item) {
                if (_this.el.indexOf(item[0]) != -1) (item[1])();
                return (_this.el.indexOf(item[0]) == -1);
            });
            return this;
        },
        eq : function(index) {
            return this.el[index] ? new noJquery(this.el[index]) : null ;
        },
        get : function(index) {
            return this.el[index] ? this.el[index] : null ;
        },
        ready : function(fn) {
            if (isWindow(this.el[0])) {
                if (document.readyState != 'loading') {
                    fn();
                } else {
                    document.addEventListener('readystatechange', e => {
                        if (document.readyState == 'interactive') {
                            fn();
                        }
                    });
                }
            }
        },
        data : function(attribute) {
            var ref; 
            this.el.forEach( el => {
                if (el.hasAttribute(("data-" + attribute))) {
                    ref = el.getAttribute("data-" + attribute);
                }
            });
            return ref;
        },
        parent : function() {
            if (!isElement(this.el[0])) return null;
            return toNode(this.el).parentNode;
        },
        trigger: function(eventName, data) {
            this.el.forEach(el => {
                el.dispatchEvent(createEvent(eventName, true, true, data));
            })
            // let event = document.createEvent('HTMLEvents');
            // event.initEvent(eventName, true, false);
            // this.el.forEach( el => {
            //     el.dispatchEvent(event);
            // })
        },
        parents : function(selector) {
            let elements = [];
            let tmpElm = this.el;
            while ((tmpElm = $(tmpElm).parent())) {
                if (selector) {
                    let getElm = $(selector);
                    let searchParentsElm = getElm.el.filter( (el) => {
                        return tmpElm === el;
                    })
                    if (searchParentsElm.length > 0) {
                        elements = elements.concat(searchParentsElm);
                    }
                }
            }
            return new noJquery(elements);
        },
        val : function(value) {
            if (typeof value === 'undefined') {
                if (this.el.length > 0) {
                    return this.el[0].value;
                }
                return null;
            } else {
                this.el[0].value = value;
            }
        },
        html : function(html) {
            this.el.forEach((el) => {
                el.innerHTML = html;
            });
            return this;
        },
        closest : function(element, selector) {
            if (selector && startsWith(selector, '>')) {
                selector = selector.slice(1);
            }

            return isElement(element)
                ? closestFn.call(element, selector)
                : toNodes(element).map(function (element) { return this.closest(element, selector); }).filter(Boolean);
        },
        width: function() {
            if (isWindow(this.el[0])) {
                return parseFloat(window.innerWidth);
            }
            return parseFloat(getComputedStyle(this.el[0], null).width.replace("px", ""));
        },
        height: function() {
            if (isWindow(this.el[0])) {
                return parseFloat(window.innerHeight);
            }
            return parseFloat(getComputedStyle(this.el[0], null).height.replace("px", ""));
        },
        outerHeight : function(margin) {
            // http://youmightnotneedjquery.com/#outer_height_with_margin
            if (this.el.length > 0) {
                var height = parseInt(this.el[0].offsetHeight);
                if (margin) {
                    var style = getComputedStyle(this.el[0]);
                    height += parseInt(style.marginTop) + parseInt(style.marginBottom);
                }
                return height;
            }
            return 0;
        },
        scrollTop : function(value,duration) {
            return scrollTo(this.el,'top',value,duration);
        },
        scrollLeft : function(value,duration) {
            return scrollTo(this.el,'left',value,duration);
        },
        position : function() {
            return { left: this.el[0].offsetLeft, top: this.el[0].offsetTop };
        },
        offset : function() {
            var rect = this.el[0].getBoundingClientRect();
            
            return {
              top: rect.top + document.body.scrollTop,
              left: rect.left + document.body.scrollLeft
            }
        },
        append : function(html) {
            if (typeof html == 'string') {
                this.el.forEach(el => {
                    el.insertAdjacentHTML('beforeend', html.trim());
                });
            } else if (isNode(html.get(0))) {
                this.el.forEach(el => {
                    el.appendChild(html.get(0));
                });
                return this;
            }
        },
        prepend: function(html) {
            if (typeof html == 'string') {
                this.el.forEach(el => {
                    el.insertAdjacentHTML('afterbegin', html.trim());
                });
            } else if (isNode(html.get(0))) {
                this.el.forEach(el => {
                    el.insertBefore(html.get(0), el.firstChild);
                });
                return this;
            }
        },
        remove : function() {
            this.el.forEach( el => {
                if (el.parentNode !== null) {
                    el.parentNode.removeChild(el);
                }
            });
            return this;
        },
        toggleClass: function(className) {
            this.el.forEach( el => {
                el.classList.toggle(className);
            });
            return this;
        },
        empty: function() {
            this.el.forEach( el => {
                while (el.firstChild) el.removeChild(el.firstChild);
            });
            return;
        },
        serializeArray : function() {
            var form = this.el[0];
            var field, l, s = [];
            if (typeof form == 'object' && form.nodeName == "FORM") {
                var len = form.elements.length;
                for (var i=0; i<len; i++) {
                    field = form.elements[i];
                    if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                        if (field.type == 'select-multiple') {
                            for (j=0; j<form.elements[i].options.length; j++) {
                                if(field.options[j].selected)
                                    s[s.length] = { name: field.name, value: field.options[j].value };
                            }
                        } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                            s[s.length] = { name: field.name, value: field.value };
                        }
                    }
                }
            }
            return s;
        },
        serializeObject : function() {
            var o = {};
            var a = this.serializeArray();
            a.forEach((elm, i) => {
                if (o[elm.name]) {
                    if (!o[elm.name].push) {
                        o[elm.name] = [o[elm.name]];
                    }
                    o[elm.name].push(elm.value || '');
                } else {
                    o[elm.name] = elm.value || '';
                }
            });
            // util.each(a, function() {
            //     if (o[this.name]) {
            //         if (!o[this.name].push) {
            //             o[this.name] = [o[this.name]];
            //         }
            //         o[this.name].push(this.value || '');
            //     } else {
            //         o[this.name] = this.value || '';
            //     }
            // });
            return o;
        },
        transformTag: function (tagType) {
            return new Promise(function(resolve){
                var tagIdOrElem = this.get(0);
                var elem = (tagIdOrElem instanceof HTMLElement) ? tagIdOrElem : document.getElementById(tagIdOrElem);
                if (!elem || !(elem instanceof HTMLElement)) return;
                var children = elem.childNodes;
                var parent = elem.parentNode;
                var newNode = document.createElement(tagType || 'span');
                for (var a = 0; a < elem.attributes.length; a++) {
                    newNode.setAttribute(elem.attributes[a].nodeName, elem.attributes[a].value);
                }
                for (var i = 0, clen = children.length; i < clen; i++) {
                    newNode.appendChild(children[0]); //0...always point to the first non-moved element
                }
                newNode.style.cssText = elem.style.cssText;
                parent.replaceChild(newNode, elem);
                resolve($(newNode));
            }.bind(this));
        },
        focus: function () {
            var input  = this.get(0);
            if (input) {
                input.focus();
            }
        }
    };
    global.$ = function(selector,context) {
        return new noJquery(selector,context);
    };
    global.$ = Object.assign(global.$,{
        extend : Object.assign,
        fn : function(name,fn) {
            noJquery.prototype[name] = function(){
                return fn.apply(this,arguments);
            };
        },
        trim : function(value) {
            return value.toString().trim();
        }
    });
    
})(window);