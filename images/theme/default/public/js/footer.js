
function footerHTML(cls) {
    var html = '';
    html += '<div class="wrap_footer '+cls+'" style="display: none;">';
        html += '<div class="footer_content">';
            html += '<div class="title f_med">ตลาดหลักทรัพย์แห่งประเทศไทย | สงวนลิขสิทธิ์</div>';
            html += '<div class="desc">เนื้อหาทั้งหมดบนเว็บไซต์นี้ มีขึ้นเพื่อวัตถุประสงค์ในการให้ข้อมูล<br class="show-xs"/>และเพื่อการศึกษาเท่านั้น ตลาดหลักทรัพย์ฯ มิได้ให้การรับรอง<br class="show-xs"/>และขอปฏิเสธต่อความรับผิดใด ๆ ในเว็บไซต์นี้</div>';
            html += '<div class="wrap_link">';
                html += '<a href="https://www.set.or.th/th/privacy-notice" target="_blank" class="footer_link">ข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์</a>';
                html += '<a href="https://www.set.or.th/th/rights-execution" target="_blank" class="footer_link">การคุ้มครองข้อมูลส่วนบุคคล</a>';
                html += '<a href="https://www.set.or.th/th/cookies-policy" target="_blank" class="footer_link">นโยบายการใช้คุกกี้</a>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}

function registerBtnHTML(hide = false, link) {
    var defaultLink = 'register.html';
    if (typeof link === 'undefined') {
        link = defaultLink
    }
    var html = '';
    html += '<div class="sticky_register '+(hide?'hide':'')+'">';
        html += '<div class="wrap_btn">';
            html += '<div class="button shadow">';
                html += '<a href="' + base_url +'member/register" class="button_label f_med">สมัครเข้าร่วม<br class="show-xs">โครงการ</a>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}

function panelRegisterHTML(cls) {
    var panelStyle = '';
    if (cls) panelStyle = cls;
    var html = '';
    html += '<div class="panel-register dim_panel">';
        html += '<div class="panel_content '+cls+'">';
            html += '<div class="menu_wrapper">';
                html += '<a href="care_the_wild.html#form_wild" class="menu menu-position-fixed flex center" style="cursor: url(public/images/home/register/cursor_register.svg) 40 40, pointer">';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/hover_elephant.svg" alt="Elephant" class="hover_bg lazy-img"/>';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/idle_elephant.svg" alt="Elephant" class="icon_cate lazy-img"/>';
                html += '</a>';
                html += '<a href="https://climatecare.setsocialimpact.com/member/register" class="menu menu-position-fixed flex center" style="cursor: url(public/images/home/register/cursor_register.svg) 40 40, pointer">';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/hover_whale.svg" alt="Whale" class="hover_bg lazy-img"/>';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/idle_whale.svg" alt="Whale" class="icon_cate lazy-img"/>';
                html += '</a>';
                html += '<a href="https://www.carethebear.com/member/register" class="menu menu-position-fixed flex center" style="cursor: url(public/images/home/register/cursor_register.svg) 40 40, pointer">';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/hover_bear.svg" alt="Bear" class="hover_bg lazy-img"/>';
                    html += '<img src="' + image_url + 'theme/default/public/images/home/register/idle_bear.svg" alt="Bear" class="icon_cate lazy-img"/>';
                html += '</a>';
            html += '</div>';
            html += '<div class="wrap_close">';
                html += '<div class="close_btn">';
                    html += '<span class="icon icon-cross"></span>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}

/**
 * @deprecate no longer use, change to <a> link
 * @todo remove in future commit
 */
function showPanelRegister() {
    return;
    var body = $('body');
    body.append(panelRegisterHTML(panelPosition));
    body.addClass('dim');
    setTimeout(() => {
        $('.panel-register .menu_wrapper').addClass('show')
        $('.panel-register .wrap_close').on('click', function() {
            $('.panel-register .menu_wrapper').removeClass('show');
            body.removeClass('dim');
            setTimeout(() => {
                $('.panel-register').remove();
            }, 300);
        });
        $('.menu-position-fixed').on('click', function() {
            $('.panel-register .menu_wrapper').removeClass('show');
            body.removeClass('dim');
            setTimeout(() => {
                $('.panel-register').remove();
            }, 300);
        });

    }, 100);
}
window.panelPosition = 'right';
$(document).ready(function() {
    
    var body = $('body');
    var path = window.location.pathname;
    var expandFooterCls = 'expand';
    var hideRegisterBtn = false;
    var excludePath = [
        '/'
        ,'/index.html'
        ,'/index' 
        ,'/news'
        ,'/news.html'
        ,'/news-detail.html'
        ,'/register'
        ,'/register.html'
        ,'/dashboard.html'
        ,'/dashboard'
        ,'/outcome.html'
        ,'/outcome'
    ];
    var noregisterPath = [
        '/dashboard.html'
        ,'/dashboard'
        ,'/outcome.html'
        ,'/outcome'
        ,'/thank-you-dashboard-bear'
        ,'/thank-you-dashboard-bear.html'
        ,'/thank-you-dashboard-whale'
        ,'/thank-you-dashboard-whale.html'
        ,'/thank-you-dashboard-wild'
        ,'/thank-you-dashboard-wild.html'
    ];

    if (excludePath.includes(path)) { 
        expandFooterCls = '';
        hideRegisterBtn = true;
    }else if (path == '/thank_you.html') {
        expandFooterCls = 'thank_you';
    }

    if(is_login)
    {
        hideRegisterBtn = true;
    }

    setTimeout(() => {
        body.append(footerHTML(expandFooterCls));
        $('.wrap_footer').addClass('appended');
        if (!noregisterPath.includes(path)) {
            body.append(registerBtnHTML(hideRegisterBtn));
        }
        $('.sticky_register').on('click', function() {
            showPanelRegister();
        });
        loadJS(image_url + 'theme/default/public/js/privacy/privacy-notice.js', function () {
            window.privacyNotice = new PrivacyNotice();
        });
    }, 100);
});