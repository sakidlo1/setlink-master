function append_ty(cate){
    var html = ''
    let bg = ''
    let logo = ''
    let text_desc = ''
    let text_color = ''
    let icon_wrapper = ''
    switch (cate) {
        case 'bear':
            bg = 'bg_ty_bear'
            logo = 'logo_bear'
            text_desc = 'Care the Bear'
            text_color = 'text_name_bear'
            icon_wrapper = 'img_content_wrapper_bear'
            break;
        case 'wild':
            bg = 'bg_ty_wild'
            logo = 'logo_wild'
            text_desc = 'Care the Wild'
            text_color = 'text_name_wild'
            icon_wrapper = 'img_content_wrapper_wild'
            break;
        case 'wal':
            bg = 'bg_ty_wal'
            logo = 'logo_wal'
            text_desc = 'Care the Whale'
            text_color = 'text_name_wal'
            icon_wrapper = 'img_content_wrapper_wal'
            break;
    
        default:
            bg = 'bg_ty_wal'
            logo = 'logo_wal'
            text_desc = 'Care the Whale'
            text_color = 'text_name_wal'
            break;
    }
    html += '<div class="padding_top"></div>'
    html += '<div class="main_ty_dashboard">'
        html += '<div class="bg">'
            html += '<img src="' + image_url + 'theme/default/public/images/dashboard_register/'+ bg +'.png" alt="">'
        html += '</div>'
        html += '<div class="container">'
            html += '<div class="content_wrapper">'
                html += '<div class="block_content">'
                    html += '<div class="'+icon_wrapper+'">'
                        html += '<img src="' + image_url + 'theme/default/public/images/dashboard_register/'+ logo +'.png" alt="">'
                    html += '</div>'
                html += '<span class="text_desc f_bold">ขอบคุณที่ร่วมเป็นส่วนหนึ่งของ'
                    html += '<br class="hide-xs">   โครงการ '
                    html += '<span class="'+text_color+'">'+text_desc+'</span>'
                html += '</span>'
                html += '<a href="index.html" class="button_ty">'
                    html += '<span class="f_med">ตกลง</span>'
                html += '</a>'
            html += '</div>'
        html += '</div>'
    html += '</div>'
    return html
}