function PrivacyNotice() {
   this.main = $('[data-modal="privacy-notice"]');
   if (!this.main.length) {
      return {};
   }
   var defaultSettings = {
      lang: 'th',
   }
   if (typeof arguments[0] !== 'undefined' &&
      typeof arguments[0] === 'object'
   ) {
      this.settings = $.extend(arguments[0], defaultSettings);
   }
   this.html = '';
   this.data = [{}];
   this.settings = defaultSettings;
   this.prepareJSON();
   this.initModal();
   this.initEvent();
   return this;
}
PrivacyNotice.prototype.initModal = function () {
   var self = this;
   if (this.modal) {
      return;
   }
   this.loadCSS(image_url + 'theme/default/public/css/custom-privacy-modal.css');

   if (typeof modal === 'undefined') {
      this.loadCSS(image_url + 'theme/default/public/css/modal.css');
      this.loadJS(image_url + 'theme/default/public/js/modal.js', function () {
         self.modal = modal
     });
   } else {
      self.modal = modal;
   }
};
PrivacyNotice.prototype.initEvent = function () {
   var self = this;
   var myModal = {};
   this.main.on('click', function(e) {
      e.preventDefault();
      myModal = self.modal.confirm({
         class: 'modal-privacy-notice',
         ok: '',
         cancel: '',
         content: '',
         onOpen: function () {
            myModal.elem.find('.modal-content').append(self.html);
         }
      })
   });
};
PrivacyNotice.prototype.prepareJSON = function () {
   var self = this;
   var fetchSetting = {
      headers: {
         'Content-Type': 'application/json',
      },
   }

   var nativeFetch = function () {
      if (self.loadJS) {
         self.loadJS(image_url + 'theme/default/public/js/privacy/data/context.js', function () {
            if (privacyContext) {
               self.data = privacyContext[self.settings.lang];
               self.prepareHTML();
            }
         });
      } else {
         console.error('function loadJS missing...');
      }
   }

   fetch(image_url + 'theme/default/public/json/privacy/context.json', fetchSetting)
      .then(function(res) {
         return res.json();
      })
      .then(function(json) {
         self.data = json[self.settings.lang];
         self.prepareHTML();
      })
      .catch(function(_error) {
         console.log('...load native');
         nativeFetch();
      });
};
PrivacyNotice.prototype.prepareHTML = function () {
   var container = document.createElement('div');
   var header = document.createElement('div');
   var content = document.createElement('div');
   var html = '';

   container.className = 'privacy-context-container';
   header.className = 'privacy-context--header';
   content.className = 'privacy-context--content';

   var htmlIntro = function (data) {
      var dataIntro = '';
      dataIntro += '<section>'
      dataIntro += '<ul class="list-item">';
      data.forEach(function (intro) {
         dataIntro += '<li>' + intro + '</li>';
      });
      dataIntro += '</ul>';
      dataIntro += '</article>';
      return dataIntro;
   }
   var htmlDefine = function (title, data) {
      var dataDefine = '';
      dataDefine += '<p class="define--title f_bold">' + title + '</p>';
      dataDefine += '<section>';
      dataDefine += '<ul class="scrollable-table list-item">';
      dataDefine += '<div class="scrollable-items">';
      dataDefine += '<div class="scrollable-items__inner">';
      data.forEach(function (define) {
         dataDefine += '<li class="scrollable-item flex">'
            + '<p class="label-definition f_bold">' + define.subject + '</p>'
            + '<p class="label-description">' + define.desc + '</p>'
         dataDefine += '</li>';
      });
      dataDefine += '</div>';
      dataDefine += '</div>';
      dataDefine += '</ul>';
      dataDefine += '</section>';
      return dataDefine;
   }
   var htmlContext = function (data) {
      var dataContext = '<div class="container--contexts">';
      data.forEach(function (context) {
         dataContext += '<article id="privacy-context-' + context.order + '" data-article="context-' + context.order + '">';
         dataContext += '<section>';
         dataContext += '<h4 class="context-label f_bold" title="' + context.subject + '">' + context.subject + '</h4>';
         dataContext += '<div class="context-list">';
         dataContext += '<ul>';
         context.lists.forEach(function(item) {
            dataContext += '<li class="list-item item-parent">';
               if (item.subject && item.subject.length) {
                  item.subject.forEach(function(label) {
                     dataContext += '<p class="item-child level-0 item-type--' + item.type + '">' + label + '</p>';
                  });
               }
               if (item.children && item.children.length) {
                  item.children.forEach(function(child) {
                     if (child.subject && child.subject.length) {
                        child.subject.forEach(function(subject) {
                           dataContext += '<p class="item-child level-1 item-type--' + child.type + '" data-type="' + child.type + '">' + subject + '</p>'
                        });
                     }
                  });
               }
            dataContext += '</li>';
         });
         dataContext += '</ul>';
         dataContext += '</div>';
         dataContext += '</section>';
         dataContext += '</article>';
      });
      dataContext += '</div>'
      return dataContext;
   }
   this.data.forEach(function(data) {
      header.innerHTML = 
         '<h3 class="title f_bold">' + data.title + '</h3>'
            + '<p class="subtitle f_bold">' + data.sub_title + '</p>'
            + '<a href="' + data.sub_title_link + '" class="subtitle-link f_med" target="_blank">(' + data.sub_title_link + ')</a>';
      html = '<div class="content--inner">'
               + '<article id="privacy-intro" data-article="intro">' + htmlIntro(data.intro) +'</article>'
               + '<article id="privacy-define" data-article="define">' + htmlDefine(data.define_title, data.define) + '</article>'
               + htmlContext(data.context);
            + '<div>';
   });
   content.innerHTML = html;
   container.appendChild(header);
   container.appendChild(content);
   this.html = $(container);
};
PrivacyNotice.prototype.loadCSS = function (e,n,o,t) {
   if (typeof loadCSS !== 'undefined') {
      loadCSS(e,n,o,t);
      return;
   }
   if (e.trim().length <= 0){ 
      return;
   }
   var d = window.document.createElement("link"),
      i = n || window.document.getElementsByTagName("script")[0],
      s = window.document.styleSheets;
      return d.rel="stylesheet", 
            d.href=e,
            d.media="only x",
            t && (d.onload=t),
            i.parentNode.insertBefore(d,i),
            d.onloadcssdefined = function(n){
               for (var o, t=0; t<s.length; t++)
                  s[t].href && s[t].href.indexOf(e) >-1 &&(o=!0);
                  o ? n() : setTimeout(function(){
                     d.onloadcssdefined(n)})
                  }, d.onloadcssdefined(function(){
                     d.media=o||"all"
                  }),
                  d;
};
PrivacyNotice.prototype.loadJS = function (e, a) {
   if (typeof loadJS !== 'undefined') {
      loadJS(e, a);
      return;
   }
   if (e.length <= 0) { 
      return; 
   } 
   var t = document.getElementsByTagName("head")[0], 
      n = document.createElement("script"); 
   n.type = "text/javascript", 
   n.src = e, n.async = !0, 
   n.onreadystatechange = a, 
   n.onload = a, 
   t.appendChild(n);
};
