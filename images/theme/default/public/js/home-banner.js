$(document).ready(function() {
    var characterArr = $('.wrap_character');
    var body = $('body');
    var banner = $('#banner');
    var bannerDim = $('.wrap_img_banner .dim');
    var open = false;

    function hideDim(elm) {
        elm.removeClass('active');
        elm.find('.wrap_box').removeClass('active');
        elm.find('.floating_btn').removeClass('active');
        bannerDim.removeClass('show');
        banner.removeClass('dim_active');
        banner.off('click', hideDim);
        body.removeClass('dim');
        $('.sticky_register').removeClass('top');
        open = false;
    }
    characterArr.el.forEach(char => {
        var ch = $(char);
        ch.find('.bubble').on('click', function() {
            if (!open) {
                open = !open;
                ch.find('.floating_btn').addClass('active');
                ch.find('.wrap_box').addClass('active');
                body.addClass('dim');
                $('.sticky_register').addClass('top');
                ch.addClass('active');
                banner.toggleClass('dim_active');
                bannerDim.toggleClass('show');
                var a = ch.find('.running-no');
                a.el.forEach( el => {
                   run_number(el);
                });
                setTimeout(() => {
                    banner.on('click', function() {
                        hideDim(ch);
                    });
                }, 1);
            } else {
                hideDim(ch);
            }
        });
        ch.find('.floating_btn').on('click', function() {
            if (!open) {
                open = !open;
                ch.find('.floating_btn').addClass('active');
                ch.find('.wrap_box').addClass('active');
                body.addClass('dim');
                $('.sticky_register').addClass('top');
                ch.addClass('active');
                banner.toggleClass('dim_active');
                bannerDim.toggleClass('show');
                var a = ch.find('.running-no');
                a.el.forEach( el => {
                    run_number(el);
                });
                setTimeout(() => {
                    banner.on('click', function() {
                        hideDim(ch);
                    });
                }, 1);
            } else {
                hideDim(ch);
            }
        });
    });

    // cloud animation
    var cloud = $('.cloud_canvas');
    var translateCloud = 0;
    function moveCloud(translateCloud) {
        var duration = 0;
        if (translateCloud >= (window.innerWidth+cloud.width())) {
            translateCloud = 0;
            cloud.attr('style', 'transform: translateX('+translateCloud+'px); transition: all 0s;');
            duration = 0;
        } else {
            translateCloud+=100;
            cloud.attr('style', 'transform: translateX('+translateCloud+'px)');
            duration = 3000;
        }
        setTimeout(() => {
            moveCloud(translateCloud);
        }, duration);
    }
    moveCloud(translateCloud);

    // remove class inviewspy character
    banner.find('.wrap_character').el.forEach(wrap => {
        $(wrap).inview(function(e) {
            setTimeout(() => {
                $(wrap).removeClass('inviewspy inviewed');
            }, 1050); // --> wait for inviewspy animation finished
        });
    })
});