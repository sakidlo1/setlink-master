/*
function menuModal() {
    var html = '';
    html += '<div class="popup_cate_menu">';
        html += '<div class="dim show"></div>';
        html += '<div class="wrap_box">';
            html += '<div class="box_cate_menu">';
                html += '<a href="care-the-bear.html" class="cate_menu bear flex v-center">';
                    html += '<div class="wrap_character">';
                        html += '<img src="' + image_url + 'theme/default/public/images/header/login_bear.svg" alt="bear" />';
                    html += '</div>';
                    html += '<div class="text f_med">';
                        html += 'Care the Bear';
                    html += '</div>';
                html += '</a>';
                html += '<a href="care-the-whale.html" class="cate_menu whale flex v-center">';
                    html += '<div class="wrap_character">';
                        html += '<img src="' + image_url + 'theme/default/public/images/header/login_whale.svg" alt="whale" />';
                    html += '</div>';
                    html += '<div class="text f_med">';
                        html += 'Care the Whale';
                    html += '</div>';
                html += '</a>';
                html += '<a href="care_the_wild.html" class="cate_menu elephant flex v-center">';
                    html += '<div class="wrap_character">';
                        html += '<img src="' + image_url + 'theme/default/public/images/header/menu_elephant.svg" alt="elephant" />';
                    html += '</div>';
                    html += '<div class="text f_med">';
                        html += 'Care the Wild';
                    html += '</div>';
                html += '</a>';
                html += '<div class="menu login_menu flex show-940">';
                    html += '<div class="icon-user">';
                        html += '<span class="user icon-profile"></span>';
                    html += '</div>';
                    html += '<div class="flex column">';
                        html += '<span class="txt-log f_med">'+ user.name +'<br> <span class="txt-sub f_reg">ออกจากระบบ</span></span>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}
*/

function loginPopup2() {
    var html = '';
    html += '<div class="popup_login">';
    html += '<div class="popup_content flex v-center">';
    html += '<div class="wrap_close hide-xs">';
    html += '<div class="black">';
    html += '<span class="icon icon-cross"></span>';
    html += '</div>';
    html += '</div>';
    html += '<div class="login-txt col-50">';
    html += '<p class="txt-big f_bold t_dark">เข้าสู่ระบบ</p>';
    html += '<p class="f_bold t_yellow">Climate Care Platform</p>';
    html += '</div>';
    html += '<form class="login-form common-form col-50">';
    html += '<div class="wrap-col-form">';
    html += '<div class="col-form">';
    html += '<div class="wrap-input-text">';
    html += '<input type="text" class="input-type-text input-required f_light" required name="email" autocomplete="false" data-error="กรุณากรอกข้อมูล">';
    html += '<span class="placeholder f_med">อีเมล</span>';
    html += '</div>';
    html += '</div>';
    html += '<div class="col-form">';
    html += '<div class="wrap-input-text">';
    html += '<input type="password" class="input-type-text input-required f_light" required name="password" autocomplete="false" data-error="กรุณากรอกข้อมูล">';
    html += '<span class="placeholder">รหัสผ่าน</span>';
    html += '<span class="ic-eye icon-eye"></span>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="wrap-login-btn flex h-justify v-center">';
    html += '<a href="' + base_url + 'member/forget_password" class="link-forget-password">ลืมรหัสผ่าน?</a>';
    html += '<div class="button-loginsetlink setlink-btn">';
    html += '<div class="button_label f_med">Login as<img src="' + image_url + 'theme/default/public/images/header/logo-setlink.png" /></div>';
    html += '</div>';
    html += '<div class="button submit-btn">';
    html += '<div class="button_label f_med">เข้าสู่ระบบ</div>';
    html += '</div>';
    html += '</div>';
    html += '</form>';
    html += '<a href="' + base_url + 'member/register" class="txt-register f_med show-xs">สมัครเข้าร่วมโครงการ</a>';
    html += '</div>';
    html += '</div>';
    return html;

}

function loginPopup() {
    var html = '';
    html += '<div class="popup_login flex center">';
    html += '<div class="dim show"></div>';
    html += '<div class="popup_content flex column center">';
    html += '<div class="wrap_close">';
    html += '<div class="close_btn black XL">';
    html += '<span class="icon icon-cross"></span>';
    html += '</div>';
    html += '</div>';
    html += '<div class="section-title f_bold" inviewspy>เลือกโครงการเข้าสู่ระบบ</div>';
    html += '<div class="wrap_profile flex h-justify">';
    html += '<div class="profile bear flex column center" inviewspy="200">';
    html += '<div class="profile_img">';
    html += '<img src="' + image_url + 'theme/default/public/images/header/login_bear.svg" alt="bear" />';
    html += '</div>';
    html += '<div class="profile_name f_med">Care the Bear</div>';
    html += '<div class="wrap_btn">';
    html += '<div class="button">';
    html += '<a href="' + base_url + 'member/login" target="_blank" class="button_label f_med">เข้าสู่ระบบ</a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="profile whale flex column center" inviewspy="300">';
    html += '<div class="profile_img">';
    html += '<img src="' + image_url + 'theme/default/public/images/header/login_whale.svg" alt="whale" />';
    html += '</div>';
    html += '<div class="profile_name f_med">Care the Whale</div>';
    html += '<div class="wrap_btn">';
    html += '<div class="button">';
    html += '<a href="' + base_url + 'member/login" target="_blank" class="button_label f_med">เข้าสู่ระบบ</a>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}

var user = {
    name: "เบญจมินทร์"
}


function headerHTML(noLogin = false, logcheck) {
    var html = '';
    html += '<header class="header flex h-justify v-center">';
    html += '<div class="constraint_header flex h-justify v-center">';
    html += '<a href="/" class="logo flex center">';
    html += '<img src="' + image_url + 'theme/default/public/images/header/logo.svg" alt="Set Climate logo" />';
    html += '</a>';
    html += '<div class="header_menu flex h-justify v-center">';
    if (logcheck == true) {
        html += '<div class="menu flex center hide-940" data-btn="login">';
        html += '<div class="icon-user">';
        html += '<span class="user icon-profile"></span>';
        html += '</div>';
        html += '<div class="flex column">';
        html += '<span class="txt f_med">' + user.name + '<br> <span class="txt-sub f_reg">ออกจากระบบ</span></span>';
        html += '</div>';
        html += '</div>';
    } else if (!noLogin) {
        html += '<div class="menu flex center hide-940" data-btn="login">';
        html += '<span class="icon icon-profile"></span>';
        html += '<span class="txt f_med">เข้าสู่ระบบ</span>';
        html += '</div>';
    }
    html += '<div class="menu flex center" data-btn="menu">';
    html += '<span class="icon menu icon-menu"></span>';
    html += '<span class="txt f_med">เมนู</span>';
    html += '</div>';
    html += '<div class="menu flex center" data-btn="close">';
    html += '<span class="icon close icon-cross"></span>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</header>';
    //return html;
    return '';
}

function isMobile() {
    return $(window).width() < 941;
}

$(document).ready(function () {
    var body = $('body');
    var page = window.location.pathname;
    var nolog = false;
    var logcheck = false;
    if (page == '/care_the_wild.html' || page == '/care_the_wild') {
        nolog = true;
    }
    nolog = false;

    body.prepend(headerHTML(nolog, logcheck));
    var header = $('header');
    var loginBtn = header.find('[data-btn=login]');
    var menuBtn = header.find('[data-btn=menu]');
    var closeBtn = header.find('[data-btn=close]');
    var dim = '';
    var menuOpen = false;
    var loginOpen = false;
    var popUpLogin = '';

    function closeMenu() {
        menuOpen = false;
        header.removeClass('menu-opened');
        body.find('.popup_cate_menu').remove();
        body.removeClass('active dim');
    }

    function closeLogin() {
        loginOpen = false;
        popUpLogin.removeClass('open');
        header.removeClass('login-opened');
        body.removeClass('active dim');
        setTimeout(() => {
            popUpLogin.remove();
        }, 600);
    }

    function setLinkLogin() {
        var newTab = window.location.href = oauth2_authen_param + '&state=' + makeid(5);
        newTab.focus(); // เพื่อให้แท็บใหม่เป็นแท็บที่มีการโฟกัส

    }

    function makeid(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }

    function submitLogin() {

        $('.wrap-login-btn .form-display-error').remove();

        var loginForm = new CommonForm('.login-form.common-form');
        var formValid = loginForm.checkValidForm();
        if (formValid.valid) {

            var xhr = new XMLHttpRequest();
            var url = base_url + "member/login";
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var response = JSON.parse(xhr.responseText);
                    if (response.status === 'success') {
                        window.location = base_url + 'dashboard';
                    } else if (response.status === 'login_fail') {
                        $('.wrap-login-btn').prepend('<label style="color: red;position: absolute;margin-top: -80px;" class="control-label form-display-error">อีเมล และรหัสผ่านไม่ถูกต้อง</label>');
                    } else if (response.status === 'not_found') {
                        $('.wrap-login-btn').prepend('<label style="color: red;position: absolute;margin-top: -80px;" class="control-label form-display-error">ไม่พบข้อมูลในระบบ กรุณาสมัครเข้าร่วมโครงการ</label>');
                    }

                }
            };
            var data = JSON.stringify(formValid.form);
            xhr.send(data);

        }
    }

    menuBtn.on('click', function (e) {
        e.stopPropagation();
        if (loginOpen) loginBtn.trigger('click');
        if (menuOpen === true) {
            closeMenu();
        } else {
            menuOpen = true;
            body.append(menuModal());
            inviewSpy();
            header.addClass('menu-opened');
            dim = body.find('.popup_cate_menu .dim');
            setTimeout(() => {
                body.addClass('active dim');
                body.find('.popup_cate_menu .login_menu').on('click', function () {
                    body.find('.popup_cate_menu').remove();
                    loginBtn.trigger('click');
                });
            }, 300);
        }
    });
    loginBtn.on('click', function (e) {
        e.stopPropagation();
        if (isMobile()) {
            header.addClass('menu-opened');
            loginOpen = true;
            body.append(loginPopup2());

            $('.popup_login .ic-eye').off('click');
            $('.popup_login .ic-eye').on('click', function () {

                if (!$('.popup_login .ic-eye').hasClass("icon-eye")) {
                    $('.popup_login .ic-eye').addClass("icon-eye");
                    $('.popup_login .ic-eye').removeClass("icon-eye-close");
                } else {
                    $('.popup_login .ic-eye').removeClass("icon-eye");
                    $('.popup_login .ic-eye').addClass("icon-eye-close");
                }

                if ($('.popup_login input[name="password"]').attr('type') == 'password') {
                    $('.popup_login input[name="password"]').attr('type', 'text');
                } else {
                    $('.popup_login input[name="password"]').attr('type', 'password');
                }

            });

            popUpLogin = body.find('.popup_login');
            setTimeout(() => {
                popUpLogin.addClass('open');
            }, 200);
            inviewSpy();
            header.addClass('login-opened');
            dim = body.find('.popup_login .dim');
            setTimeout(() => {
                body.addClass('active dim');
                popUpLogin.on('click', function (e) {
                    e.stopPropagation();
                })
                body.find('.popup_login .wrap_close').on('click', function (e) {
                    e.stopPropagation();
                    closeLogin();
                });
                body.find('.popup_login .submit-btn').on('click', function (e) {
                    e.stopPropagation();
                    submitLogin();
                });
                body.find('.popup_login .setlink-btn').on('click', function (e) {
                    e.stopPropagation();
                    setLinkLogin();
                });

                dim.on('click', function () {
                    closeLogin();
                });
            }, 300);
        } else {
            if (menuOpen === true) {
                closeMenu()
            }
            if (loginOpen === true) {
                loginOpen = false;
                header.removeClass('login-opened');
                body.find('.popup_login').removeClass('open');
                setTimeout(() => {
                    body.find('.popup_login').remove();
                }, 700);
                body.removeClass('active dim');
            } else {
                loginOpen = true;
                body.append(loginPopup2());

                $('.popup_login .ic-eye').off('click');
                $('.popup_login .ic-eye').on('click', function () {

                    if (!$('.popup_login .ic-eye').hasClass("icon-eye")) {
                        $('.popup_login .ic-eye').addClass("icon-eye");
                        $('.popup_login .ic-eye').removeClass("icon-eye-close");
                    } else {
                        $('.popup_login .ic-eye').removeClass("icon-eye");
                        $('.popup_login .ic-eye').addClass("icon-eye-close");
                    }

                    if ($('.popup_login input[name="password"]').attr('type') == 'password') {
                        $('.popup_login input[name="password"]').attr('type', 'text');
                    } else {
                        $('.popup_login input[name="password"]').attr('type', 'password');
                    }

                });

                popUpLogin = body.find('.popup_login');
                setTimeout(() => {
                    popUpLogin.addClass('open');
                }, 200);
                inviewSpy();
                header.addClass('login-opened');
                dim = body.find('.popup_login .dim');
                setTimeout(() => {
                    body.addClass('active dim');
                    popUpLogin.on('click', function (e) {
                        e.stopPropagation();
                    })
                    body.find('.popup_login .wrap_close').on('click', function (e) {
                        e.stopPropagation();
                        closeLogin();
                    });
                    body.find('.popup_login .submit-btn').on('click', function (e) {
                        e.stopPropagation();
                        submitLogin();
                    });
                    body.find('.popup_login .setlink-btn').on('click', function (e) {
                        e.stopPropagation();
                        setLinkLogin();
                    });
                    dim.on('click', function () {
                        loginBtn.trigger('click');
                    });
                }, 300);
            }
        }
    });

    closeBtn.on('click', function (e) {
        e.stopPropagation();
        header.removeClass('menu-opened login-opened');
        body.removeClass('active dim');
        if (menuOpen === true) {
            menuOpen = false;
            body.find('.popup_cate_menu').remove();
        }
        if (loginOpen === true) {
            closeLogin();
        }
    });

    body.on('click', function (e) {
        if (menuOpen === true && loginOpen === false) {
            menuOpen = false;
            header.removeClass('menu-opened');
            body.find('.popup_cate_menu').remove();
        }
        if (loginOpen === true) {
            if (!isMobile()) {
                loginOpen = false;
                closeLogin();
            }
            header.removeClass('login-opened');
            body.find('.popup_cate_menu').remove();
        }
        body.removeClass('dim active');
    });
});