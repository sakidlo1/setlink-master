function resultHtml(data) {
    var html = '';
    html += '<div class="result flex column v-center" inviewSpy="200">';
    html += '<div class="result--icon ' + data.color + ' flex center">';
    html += '<span class="' + data.icon + '"></span>';
    html += '</div>';
    html += '<div class="result--desc f_med">' + data.desc + '</div>';
    html += '<div class="result--value f_bold" data-no="' + data.value + '">0</div>';
    html += '<div class="result--unit f_reg t_grey">' + data.unit + '</div>';
    html += '</div>';
    return html;
}

function mapResultType(key, mapParam) {
    var param = {}
    if (key == 'greenhouse_gas_reduced') {
        var _desc = 'โดยสามารถลดก๊าซเรือนกระจกได้';
        if (mapParam.color == 'green') {
            _desc = 'สามารถดูดซับก๊าซเรือนกระจกได้';
        }
        param = Object.assign(mapParam, {
            icon: 'icon-cloud',
            desc: _desc,
            unit: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า'
        });
    } else if (key == 'trees_planted') {
        param = Object.assign(mapParam, {
            icon: 'icon-tree',
            desc: 'คำนวณออกมาแล้วเทียบเท่ากับ <br class="hide-xs">การปลูกต้นไม้อายุ 10 ปี',
            unit: 'ต้น'
        });
    } else {
        param = Object.assign(mapParam, {
            icon: 'icon-cloud',
            desc: 'ดูดซับก๊าซเรือนกระจกจาก<br class="hide-xs">การปลูกป่าได้',
            unit: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า'
        });
    }
    return param;
}

function bearOutcomeHtml(data) {
    var html = '';
    html += '<div class="summary-result t_26 no-top" inviewSpy="200">';
    html += 'ตั้งแต่ <span class="f_med t_30">'+$(".pick--since").get(0).innerHTML+'</span> ถึง <span class="f_med t_30">'+$(".pick--to").get(0).innerHTML+'</span> <span class="f_med t_30">' + data.company_name + '</span><br>ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการดำเนินกิจกรรมในรูปแบบต่างๆ ทั้งสิ้น <span class="f_med hilight">' + data.reduce_gas_activity + '</span> กิจกรรม <br>ได้แก่ การประชุมผู้ถือหุ้น <span class="f_med hilight">' + data.meeting + '</span> ครั้ง การจัดอบรม <span class="f_med hilight">' + data.training + '</span> ครั้ง การจัดงานมอบรางวัล <span class="f_med hilight">' + data.give_awards + '</span> ครั้ง  ';
    html += '</div>';
    html += '<div class="result-list flex">';
    for (const key in data.climate_data) {
        var param = {
            icon: '',
            desc: '',
            value: data.climate_data[key],
            unit: '',
            color: 'blue'
        }
        param = mapResultType(key, param);
        html += resultHtml(param);
    }
    html += '</div>';
    html += '<div class="table--title t_26 no-top f_med" inviewSpy="200">' + data.tableTitle + '</div>';
    html += '<div class="hidden-frame" inviewSpy="200">';
    html += '<div class="result--table">';
    html += tableHtml(data.tableStructure, data.progress_detail, false);
    html += '</div>';
    html += '</div>';
    return html;
}

function bear_tsdOutcomeHtml(data) {
    var html = '';
    html += '<div class="summary-result t_26 no-top" inviewSpy="200">';
    html += 'ตั้งแต่ <span class="f_med t_30">'+$(".pick--since").get(0).innerHTML+'</span> ถึง <span class="f_med t_30">'+$(".pick--to").get(0).innerHTML+'</span> <span class="f_med t_30">' + data.company_name + '</span><br>ได้มีการลดการปล่อยก๊าซเรือนกระจกจากการลดใช้กระดาษของกิจกรรม <br> ทั้งสิ้น <span class="f_med hilight">' + data.bear_tsd_total + '</span> Corporate Action ของบริษัทผู้ออกหลักทรัพย์ ';
    html += '</div>';
    html += '<div class="result-list flex">';
    for (const key in data.climate_data) {
        var param = {
            icon: '',
            desc: '',
            value: data.climate_data[key],
            unit: '',
            color: 'blue'
        }
        param = mapResultType(key, param);
        html += resultHtml(param);
    }
    html += '</div>';
    html += '<div class="table--title t_26 no-top f_med" inviewSpy="200">' + data.tableTitle_tsd + '</div>';
    html += '<div class="hidden-frame" inviewSpy="200">';
    html += '<div class="result--table">';
    html += tableHtml(data.tableStructure_tsd, data.progress_detail_tsd, false);
    html += '</div>';
    html += '</div>';
    return html;
}

function whaleOutcomeHtml(data) {
    var html = '';
    html += '<div class="summary-result t_26 no-top" inviewSpy="200">';
    html += '<span class="f_med t_30">' + data.company_name + '</span><br>ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการ<br class="show-xs">คัดแยกขยะและจัดการขยะ';
    html += '</div>';
    html += '<div class="result-list flex">';
    for (const key in data.climate_data) {
        var param = {
            icon: '',
            desc: '',
            value: data.climate_data[key],
            unit: '',
            color: 'blue'
        }
        param = mapResultType(key, param);
        html += resultHtml(param);
    }
    html += '</div>';
    html += '<div class="table--title t_26 no-top f_med" inviewSpy="200">' + data.wasteTableTitle + '</div>';
    html += '<div class="hidden-frame" inviewSpy="200">';
    html += '<div class="result--table">';
    html += tableHtml(data.wasteTableStructure, data.progress_detail.waste);
    html += '</div>';
    html += '</div>';
    html += '<div class="wrap-chart flex center" inviewSpy="200">';
    html += '<div class="wrap-tooltip-area flex column center">'
    html += '<canvas id="waste"></canvas>';
    html += '<div id="waste-tooltip"></div>'
    html += '</div>';
    html += '</div>';
    html += '<div class="table--title t_26 no-top f_med" inviewSpy="200">' + data.wasteManageTableTitle + '</div>';
    html += '<div class="hidden-frame" inviewSpy="200">';
    html += '<div class="result--table">';
    html += tableHtml(data.wasteManageTableStructure, data.progress_detail.wasteManage, undefined, { col: 0, suffix: '%' });
    html += '</div>';
    html += '</div>';
    html += '<p class="f_med graph-title t_26" inviewSpy="200">อันดับวิธีการจัดการขยะ ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML+'</p>';
    html += '<div class="hidden-frame" inviewSpy="200">'
    html += '<div class="wrap-chart bar">';
    html += '<div class="wrap-tooltip-area flex column center">'
    html += '<canvas id="wasteManage"></canvas>';
    html += '<div id="waste-manage-tooltip"></div>'
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}
function elephantOutcomeHtml(data) {
    var html = '';
    html += '<div class="summary-result t_26 no-top" inviewSpy="200">';
    html += '<span class="f_med t_30">' + data.company_name + '</span><br> ได้ระดมทุนปลูกป่าในโครงการฯ';
    html += '</div>';

    html += '<div class="wrap-village-result">';
    data.progress_detail.locations.forEach((detail, index) => {
        html += '<p class="t_26 no-top" inviewSpy="200">' + (index + 1) + '. <span class="f_med hilight">' + detail.area + ' ' + addComma(detail.trees) + ' ต้น ณ ' + detail.village + ' ' + detail.location + '</span></p>';
    });
    html += '</div>';
    html += '<div class="result-list flex">';
    for (const key in data.climate_data) {
        var param = {
            icon: '',
            desc: '',
            value: data.climate_data[key],
            unit: '',
            color: 'green'
        }
        param = mapResultType(key, param);
        html += resultHtml(param);
    }
    html += '</div>';
    html += '<div class="wrap-village-result" inviewSpy="200">';
    html += '<span class="t_26">ทำในการนี้ ทำให้ <span class="hilight f_med">' + data.progress_detail.houses + '</span> ครัวเรือนใน <span class="hilight f_med">';
    data.progress_detail.locations.forEach((location, i) => {
        html += location.village + (i + 1 < data.progress_detail.locations.length ? ', ' : '');
    });
    html += '</span> สามารถหาอยู่หากินมีรายได้<br class="hide-940">จากการเพาะกล้าไม้ ซึ่งสร้างเศรษฐกิจโดยรอบให้กับป่าชุมชน <br class="show-xs">อีกทั้งยังช่วยในเรื่องการ<br class="hide-940">รักษาสมดุลทางธรรมชาติให้กับระบบนิเวศ</span>';
    html += '</div>';
    return html;
}

function outcomeHtml(data) {
    var html = '';
    html += '<div class="result-box cate-' + data.cate + '">';
    html += '<div class="result--head" inviewSpy="200">';
    html += '<div class="title f_med t_40">' + data.climate_name + '</div>';
    html += '<div class="desc t_26 no-top">' + data.climate_desc + '</div>';
    html += '</div>';
    html += '<div class="result--body">';
    switch (data.cate) {
        case 'bear':
            html += bearOutcomeHtml(data);
            break;
        case 'bear_tsd':
            html += bear_tsdOutcomeHtml(data);
            break;
        case 'whale':
            html += whaleOutcomeHtml(data);
            break;
        case 'elephant':
            html += elephantOutcomeHtml(data);
            break;
    }
    html += '</div>';
    html += '</div>';
    return html;
}

function tableHtml(data_head, data_body, run_no = true, colSuffix = {}) {
    var html = '';
    html += '<div class="wrap-table">';
    html += '<div class="table">';
    html += '<div class="tb-head tb-row f_med">';
    data_head.nested.forEach((head_nest, i) => {
        if (head_nest[1] == undefined || head_nest[1].length == 0) {
            html += '<div class="tb-col-' + head_nest[i] + '">' + data_head.head[i][0] + '</div>';
        } else {
            html += '<div class="tb-col-' + head_nest[0] + ' flex h-justify v-center">';
            head_nest[1].forEach((col_nest, j) => {
                html += '<div class="tb-col-' + col_nest + '">' + data_head.head[i][j] + '</div>';
            });
            html += '</div>';
        }
    });
    html += '</div>';
    var dataTable = [];
    data_body.forEach((data, key) => {
        html += '<div class="tb-row">';
        data_head.nested.forEach((head_nest, i) => {
            if (head_nest[1] == undefined || head_nest[1].length == 0) {
                html += '<div class="tb-col-' + head_nest[i] + '">' + (run_no ? (key + 1) + '. ' : '') + data.txt + '</div>';
            } else {
                html += '<div class="tb-col-' + head_nest[0] + ' flex h-justify v-center">';
                head_nest[1].forEach((col_nest, j) => {
                    if (dataTable[j] == undefined) dataTable[j] = parseFloat(data.value[j]);
                    else dataTable[j] += parseFloat(data.value[j]);
                    html += '<div class="tb-col-' + col_nest + '"><span class="f_med hilight">' + addComma(parseFloat(data.value[j])) + (colSuffix.col != undefined && colSuffix.col == j ? colSuffix.suffix : '') + '</span></div>';
                });
                html += '</div>';
            }
        });
        html += '</div>';

    });
    html += '<div class="tb-row">';
    data_head.nested.forEach((head_nest, i) => {
        if (head_nest[1] == undefined || head_nest[1].length == 0) {
            html += '<div class="tb-col-' + head_nest[i] + '"><span class="f_bold">รวม</span></div>';
        } else {
            html += '<div class="tb-col-' + head_nest[0] + ' flex h-justify">';
            head_nest[1].forEach((col_nest, j) => {
                html += '<div class="tb-col-' + col_nest + '"><span class="f_bold">' + addComma(dataTable[j]) + (colSuffix.col != undefined && colSuffix.col == j ? colSuffix.suffix : '') + '</span></div>';
            });
            html += '</div>';
        }
    });
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}

function calendar() {
    const calendarSettings = {
        locale: {
            firstDayOfWeek: 0,
            locale: 'th'
        },
        position: 'auto center',
        maxDate: new Date(),
        monthSelectorType: 'static',
        prevArrow: '<span class="icon icon-arrow-down2"></span>',
        nextArrow: '<span class="icon icon-arrow-down2"></span>',
        disableMobile: true,
        defaultDate: new Date(),
        formatDate: function(i, j, k) {
            var thaMonthLocale = flatpickr.l10ns.th.months.shorthand;
            return i.getDate()+ ' ' + thaMonthLocale[i.getMonth()]+ ' '+ (i.getFullYear()+543);
        }
    }

    flatpickr.localize(flatpickr.l10ns.th);
    window.flatpickerSince = flatpickr($(".pick--since").get(0), Object.assign(calendarSettings, {
        onChange: function(i, j, k) {
            $(".pick--since").html(j);
            $('.table-area').html('');
            $('.climate-result .result-list').html('<div class="flex loader"></div>');
            $('.climate-result .result-list .loader').loading();
            $('.text-to').addClass('hide');
            $('.pick--since').addClass('not-ready');
            $('.pick--to').addClass('not-ready');
            getOutcomeDataWithPeriod(flatpickerSince.selectedDates[0], flatpickerTo.selectedDates[0]).then(res => {
                $('.pick--since').removeClass('not-ready');
                $('.pick--to').removeClass('not-ready');
                mapDataOutCome();
                calendarReady = true;
            });
        },
        onMonthChange: function(j,k,l) {
            flatpickerSince.thaiYear = true;
            setTimeout(() => {
                $(l.currentYearElement).val(parseInt($(l.currentYearElement).val()) + 543);
                $(l.monthsElement).html(flatpickr.l10ns.th.months.shorthand[l.currentMonth]);
            }, 1);
        },
        onOpen: function(j,k,l) {
            if (flatpickerSince.thaiYear == undefined) {
                flatpickerSince.thaiYear = true;
                setTimeout(() => {
                    $(l.currentYearElement).val(parseInt($(l.currentYearElement).val()) + 543);
                    $(l.monthsElement).html(flatpickr.l10ns.th.months.shorthand[l.currentMonth]);
                }, 1);
            }
            if ($(window).width() < 565) {
                var calendar = $('.flatpickr-calendar.open');
                var width = calendar.width();
                setTimeout(() => {
                    calendar.get(0).style.setProperty('left', ($(window).width() - width) / 2+ 'px');
                    calendar.get(0).style.setProperty('right', ($(window).width() - width) / 2+ 'px');
                }, 10);
            }
        }
    }));
    window.flatpickerTo = flatpickr($(".pick--to").get(0), Object.assign(calendarSettings, {
        onChange: function(i, j, k) {
            $(".pick--to").html(j);
            $('.table-area').html('');
            $('.climate-result .result-list').html('<div class="flex loader"></div>');
            $('.climate-result .result-list .loader').loading();
            $('.text-to').addClass('hide');
            flatpickerSince.set('maxDate', flatpickerTo.selectedDates[0]);
            $('.pick--since').addClass('not-ready');
            $('.pick--to').addClass('not-ready');
            getOutcomeDataWithPeriod(flatpickerSince.selectedDates[0], flatpickerTo.selectedDates[0]).then(res => {
                $('.pick--since').removeClass('not-ready');
                $('.pick--to').removeClass('not-ready');
                mapDataOutCome();
                calendarReady = true;
            });
        },
        onMonthChange: function(j,k,l) {
            flatpickerTo.thaiYear = true;
            setTimeout(() => {
                $(l.currentYearElement).val(parseInt($(l.currentYearElement).val()) + 543);
                $(l.monthsElement).html(flatpickr.l10ns.th.months.shorthand[l.currentMonth]);
            }, 1);
        },
        onOpen: function(j,k,l) {
            if (flatpickerTo.thaiYear == undefined) {
                flatpickerTo.thaiYear = true;
                setTimeout(() => {
                    $(l.currentYearElement).val(parseInt($(l.currentYearElement).val()) + 543);
                    $(l.monthsElement).html(flatpickr.l10ns.th.months.shorthand[l.currentMonth]);
                }, 1);
            }
            if ($(window).width() < 565) {
                var calendar = $('.flatpickr-calendar.open');
                var width = calendar.width();
                setTimeout(() => {
                    calendar.get(0).style.setProperty('left', ($(window).width() - width) / 2+ 'px');
                    calendar.get(0).style.setProperty('right', ($(window).width() - width) / 2+ 'px');
                }, 10);
            }
        }
    }));

    window.legendRangeDate = function() {
        var html = '';
        html += flatpickerSince.selectedDates[0].getDate() + ' ' + flatpickerSince.l10n.months.shorthand[flatpickerSince.selectedDates[0].getMonth()] + ' ' + (flatpickerSince.selectedDates[0].getFullYear()+543);
        html += ' ถึง ';
        html += flatpickerTo.selectedDates[0].getDate() + ' ' + flatpickerTo.l10n.months.shorthand[flatpickerTo.selectedDates[0].getMonth()] + ' ' + (flatpickerTo.selectedDates[0].getFullYear()+543);
        return html;
    }

    flatpickerSince.setDate(from_data_date, true);
    flatpickerTo.setDate(to_data_date, true);

    $(window).on('resize', function() {
        if ($('.flatpickr-calendar:not(.arrowTop)').length > 0) {
            $('.flatpickr-calendar:not(.arrowTop)').get(0).style.setProperty('left', null);
        }
        if ($('.flatpickr-calendar:not(.arrowTop)').length > 0) {
            $('.flatpickr-calendar:not(.arrowTop)').get(0).style.setProperty('right', null);
        }
    });
}

function getOutcomeDataWithPeriod(start, end) {
    
    if(first_set_date == 2)
    {
        start = new Date(start.getTime());
        end = new Date(end.getTime());
        start.setDate(start.getDate() + 1);
        end.setDate(end.getDate() + 1);
        window.location = base_url + 'dashboard/outcome?from=' + start.toISOString().split('T')[0] + '&to=' + end.toISOString().split('T')[0];
    }
    else
    {
        first_set_date++;
    }

    console.log('from : ' + start, 'to : ' + end);
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(dashboardData); // TODO: get data with date range param [start, end]
        }, 2000);
    });
}

var chart = {};
var barChart = {};
var calendarReady = false;

// Warning: this use for testing only
// TODO: remove this
function recieveOutcomeSettingsFromQueryString() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    let value = params.register;
    if (value) {
        outcomeSummary.bear = value.split('')[0] == '1' ? outcomeSummary.bear : undefined;
        outcomeSummary.bear_tsd = value.split('')[3] == '1' ? outcomeSummary.bear_tsd : '0';
        outcomeSummary.whale = value.split('')[1] == '1' ? outcomeSummary.whale : undefined;
        outcomeSummary.elephant = value.split('')[2] == '1' ? outcomeSummary.elephant : undefined;
        outcomeExportData.bear = value.split('')[0] == '1' ? outcomeExportData.bear : undefined;
        outcomeExportData.bear_tsd = value.split('')[3] == '1' ? outcomeExportData.bear_tsd : undefined;
        outcomeExportData.whale = value.split('')[1] == '1' ? outcomeExportData.whale : undefined;
        outcomeExportData.elephant = value.split('')[2] == '1' ? outcomeExportData.elephant : undefined;
    }
}

function mapDataOutCome() {
    if (!calendarReady) return;
    var includeCate = { bear: false, whale: false, elephant: false, bear_tsd: false};
    // outcome summary
    const outcomeSummaryElm = $('.outcome-summary');
    const companyLogoElm = outcomeSummaryElm.find('.company--logo img');
    const companyFullNameElm = outcomeSummaryElm.find('.company--name');
    const companyNameElm = outcomeSummaryElm.find('.company-name');
    companyLogoElm.attr('src', dashboardData.logoImg);
    companyLogoElm.attr('alt', dashboardData.logoImgAlt);
    companyFullNameElm.html(dashboardData.fullname);
    companyNameElm.html(dashboardData.name);

    const listResultClimate = $('.climate-result .result-list');
    var listHtml = '';
    for (const key in outcomeData.result_climate) {
        var param = {
            icon: '',
            desc: '',
            value: outcomeData.result_climate[key],
            unit: '',
            color: 'orange'
        }
        if (key == 'greenhouse_gas_reduced') {
            param = Object.assign(param, {
                icon: 'icon-cloud',
                desc: 'โดยสามารถ <br class="hide-xs">ลดก๊าซเรือนกระจกได้',
                unit: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า'
            });
        } else if (key == 'trees_planted') {
            param = Object.assign(param, {
                icon: 'icon-tree',
                desc: 'คำนวณออกมาแล้วเทียบเท่ากับ <br class="hide-xs">การปลูกต้นไม้อายุ 10 ปี',
                unit: 'ต้น'
            });
        } else {
            param = Object.assign(param, {
                icon: 'icon-cloud',
                desc: 'ดูดซับก๊าซเรือนกระจกจาก<br class="hide-xs">การปลูกป่าได้',
                unit: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า'
            });
        }
        listHtml += resultHtml(param);
    }
    listResultClimate.html(listHtml);
    var listRunningNo = listResultClimate.find('[data-no]');
    listRunningNo.el.forEach(noElm => {
        $(noElm).inview((e) => {
            run_number(e, 1);
        });
    });

    var sectionTable = $('.table-area');
    if (outcomeSummary.bear) {
        sectionTable.append(outcomeHtml(outcomeSummary.bear));
        $('.text-to').removeClass('hide');
        includeCate.bear = true;
    }
    if (outcomeSummary.bear_tsd) {
        sectionTable.append(outcomeHtml(outcomeSummary.bear_tsd));
        $('.text-to').removeClass('hide');
        includeCate.bear_tsd = true;
    }
    if (outcomeSummary.whale) {
        sectionTable.append(outcomeHtml(outcomeSummary.whale));
        $('.text-to').removeClass('hide');
        includeCate.whale = true;
        // chartjs 
        // circle chart
        var doughnutLabel = [];
        var doughnutValue = [];
        outcomeSummary.whale.progress_detail.waste.sort(function (a, b) {
            return b.percent - a.percent;
        }).forEach((text) => {
            doughnutValue.push(text.percent);
            doughnutLabel.push(text.txt + ' ' + text.percent + '%');
        });


        // bar chart
        var barLabel = [];
        var barValue = [];
        let dataList = outcomeSummary.whale.progress_detail.wasteManage;
    
        function getBarOptionMedia(media) {
            if (media == 'mb') {
                return {
                    responsive: false,
                    barThickness: 20,
                    indexAxis: 'y',
                    scales: {
                        x: {
                            grid: {
                                display: true,
                            },
                            ticks: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 18
                                },
                                color: '#424242'
                            }
                        },
                        y: {
                            grid: {
                                display: false,
                            },
                            ticks: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 21
                                },
                                color: '#000'
                            }
                        }
                    },
                    borderRadius: 4,
                    plugins: {
                        title: {
                            display: false,
                            text: 'อันดับวิธีการจัดการขยะ ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML,
                            font: {
                                family: 'DB Helvethaica X',
                                weight: '500',
                                size: 26
                            },
                            color: '#000',
                            padding: {
                                bottom: 24
                            }
                        },
                        legend: {
                            display: false
                        },
                        tooltip: {
                            enabled: false
                        }
                    },
                    animation: {
                        onProgress: function () {
                            var chartInstance = barChart;
                            var ctx = chartInstance.ctx;
                            ctx.textAlign = "left";
                            ctx.font = "16px DB Helvethaica X";
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText('[VALUE]', bar.x + 10, bar.y - 3);
                                    ctx.fillText('kg.CO2e', bar.x + 10, bar.y + 12);
                                }), this)
                            }), this);
                        },
                        onComplete: function () {
                            var chartInstance = barChart;
                            var ctx = chartInstance.ctx;
                            ctx.textAlign = "left";
                            ctx.lineWidth = 2;
                            ctx.font = "16px DB Helvethaica X"
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText('[VALUE]', bar.x + 10, bar.y - 3);
                                    ctx.fillText('kg.CO2e', bar.x + 10, bar.y + 12);
                                }), this)
                            }), this);
                        }
                    }
                }
            } else {
                return {
                    responsive: false,
                    barThickness: 20,
                    indexAxis: 'y',
                    scales: {
                        x: {
                            grid: {
                                display: true,
                            },
                            ticks: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 18
                                },
                                color: '#424242'
                            }
                        },
                        y: {
                            grid: {
                                display: false,
                            },
                            ticks: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 26
                                },
                                color: '#000'
                            }
                        }
                    },
                    borderRadius: 4,
                    plugins: {
                        title: {
                            display: false,
                            text: 'อันดับวิธีการจัดการขยะ ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML,
                            font: {
                                family: 'DB Helvethaica X',
                                weight: '500',
                                size: 26
                            },
                            color: '#000',
                            padding: {
                                bottom: 24
                            }
                        },
                        legend: {
                            display: false
                        },
                        tooltip: {
                            enabled: false
                        }
                    },
                    animation: {
                        onProgress: function () {
                            var chartInstance = barChart;
                            var ctx = chartInstance.ctx;
                            ctx.textAlign = "left";
                            ctx.font = "16px DB Helvethaica X";
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText('[VALUE]', bar.x + 10, bar.y - 3);
                                    ctx.fillText('kg.CO2e', bar.x + 10, bar.y + 12);
                                }), this)
                            }), this);
                        },
                        onComplete: function () {
                            var chartInstance = barChart;
                            var ctx = chartInstance.ctx;
                            ctx.textAlign = "left";
                            ctx.lineWidth = 2;
                            ctx.font = "16px DB Helvethaica X"
                            Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.getDatasetMeta(i);
                                Chart.helpers.each(meta.data.forEach(function (bar, index) {
                                    ctx.fillText('[VALUE]', bar.x + 10, bar.y - 3);
                                    ctx.fillText('kg.CO2e', bar.x + 10, bar.y + 12);
                                }), this)
                            }), this);
                        }
                    }
                }
            }
        }
    
        var barChartOpts = {};
        if ($(window).width() < 941) {
            barChartOpts = getBarOptionMedia('mb');
        } else {
            barChartOpts = getBarOptionMedia('dt');
        }
        $(window).on('resize', function () {
            if ($(window).width() < 941) {
                barChart.options = getBarOptionMedia('mb');
            } else {
                barChart.options = getBarOptionMedia('dt');
            }
            barChart.update('none');
        });
    
        dataList.sort(function (a, b) {
            return b.value[2] - a.value[2];
        }).forEach(data => {
            barLabel.push(data.txt);
            barValue.push(data.value[2]);
        });
        const wasteManage = document.getElementById('wasteManage');
        barChart = new Chart(wasteManage, {
            type: 'bar',
            data: {
                labels: barLabel,
                datasets: [{
                    axis: 'y',
                    label: '',
                    data: barValue,
                    backgroundColor: [
                        '#843D0B',
                        '#C3580C',
                        '#F4B183',
                        '#F8CBAD',
                        '#FBE5D6',
                        '#FFF2CC',
                        '#FFF9F6'
                    ],
                }]
            },
            options: barChartOpts
        });

        function getDoughnutOptionMedia(media) {
            if (media == 'mb') {
                return {
                    aspectRatio: 0.8,
                    cutout: '70%',
                    plugins: {
                        legend: {
                            title: {
                                text: legendRangeDate(),
                                display: true,
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: '500',
                                    size: 21,
                                },
                                color: '#000',
                                padding: {
                                    top: 0,
                                }
                            },
                            position: 'top',
                            align: 'center',
                            textAlign: 'left',
                            labels: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 20,
                                    lineHeight: 28,
                                    color: 'black',
                                    textAlign: 'left',
                                    padding: {
                                        top: 16
                                    }
                                },
                                color: '#000',
                                boxWidth: 3000,
                                boxHeight: 10,
                                usePointStyle: true,
                                pointStyle: 'circle'
                            }
                        },
                        tooltip: {
                            enabled: false,
                            external: function (context) {
                                let tooltipElm = $('#waste-tooltip');
                                let backgroundColor = context.tooltip.labelColors[0].backgroundColor;
                                tooltipElm.html('<span class="f_bold">' + context.tooltip.body[0].lines[0] + '%</span>');
                                tooltipElm.attr('style',
                                    'top: ' + context.tooltip._eventPosition.y + 'px;' +
                                    'left: ' + context.tooltip._eventPosition.x + 'px;' +
                                    'background-color: ' + backgroundColor + ';'
                                    + 'opacity: ' + context.tooltip.opacity + ';'
                                );
                            }
                        }
                    },
                    options: {
                        animation: false
                    }
                }
            } else {
                return {
                    responsive: false,
                    aspectRatio: 2,
                    cutout: '70%',
                    plugins: {
                        legend: {
                            title: {
                                text: legendRangeDate(),
                                display: true,
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: '500',
                                    size: 24,
                                },
                                color: '#000',
                                padding: {
                                    top: 25,
                                }
                            },
                            position: 'left',
                            align: 'start',
                            textAlign: 'left',
                            labels: {
                                font: {
                                    family: 'DB Helvethaica X',
                                    weight: 'normal',
                                    size: 26,
                                    lineHeight: 28,
                                    color: 'black',
                                    textAlign: 'left',
                                    padding: {
                                        top: 16,
                                    }
                                },
                                color: '#000',
                                boxWidth: 3000,
                                boxHeight: 10,
                                usePointStyle: true,
                                pointStyle: 'circle'
                            }
                        },
                        tooltip: {
                            enabled: false,
                            external: function (context) {
                                let tooltipElm = $('#waste-tooltip');
                                tooltipElm.removeClass('hide');
                                let backgroundColor = context.tooltip.labelColors[0].backgroundColor;
                                tooltipElm.html('<span class="f_bold">' + context.tooltip.body[0].lines[0] + '%</span>');
                                tooltipElm.attr('style',
                                    'top: ' + context.tooltip._eventPosition.y + 'px;' +
                                    'left: ' + context.tooltip._eventPosition.x + 'px;' +
                                    'background-color: ' + backgroundColor + ';'
                                    + 'opacity: ' + context.tooltip.opacity + ';'
                                );
                            }
                        }
                    },
                    options: {
                        animation: false
                    }
                }
            }
        }
    
        var chartOps = {};
        if ($(window).width() < 941) {
            chartOps = getDoughnutOptionMedia('mb');
        } else {
            chartOps = getDoughnutOptionMedia('dt');
        }

        setTimeout(() => {
            const ctx = document.getElementById('waste');
            chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: doughnutLabel,
                    datasets: [{
                        label: '',
                        data: doughnutValue,
                        borderWidth: 0,
                    }]
                },
                options: chartOps
            });
            $(window).on('resize', function () {
                if ($(window).width() < 941) {
                    chart.options = getDoughnutOptionMedia('mb');
                } else {
                    chart.options = getDoughnutOptionMedia('dt');
                }
                chart.update('none');
            });
        }, 100);
    }

    if (outcomeSummary.elephant) {
        sectionTable.append(outcomeHtml(outcomeSummary.elephant));
        $('.text-to').removeClass('hide');
        includeCate.elephant = true;
    }
    var listRunningNo = sectionTable.find('[data-no]');
    listRunningNo.el.forEach(noElm => {
        $(noElm).inview((e) => {
            run_number(e, 1);
        })
    });

    $('.result--table').el.forEach((wrap) => {
        gradientScroll(wrap);
    });

    $('.wrap-chart.bar').el.forEach((elm) => {
        gradientScroll(elm);
    });

    $('.wrap-download-btn .download-btn').el.forEach(btn => {
        convertPathToBase64([
            image_url + 'theme/default/public/images/outcome-export-img/table_bear_new.png',
            image_url + 'theme/default/public/images/outcome-export-img/table_bear_tsd.png',
            image_url + 'theme/default/public/images/outcome-export-img/table_whale_waste.png',
            image_url + 'theme/default/public/images/outcome-export-img/table_whale_waste_manage.png',
            image_url + 'theme/default/public/images/outcome-export-img/climate_collab_logo.png',
            outcomeExportData.logoImg
        ]).then((res) => {
            $(btn).on('click', function () {
                var type = $(this).data('type');
                if (type == 'pdf') {
                    pdf(res, includeCate);
                } else if (type == 'doc') {
                    exportDocx(res, includeCate);
                }
            });
        })
    });

    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
}

$(document).ready(function () {
    recieveOutcomeSettingsFromQueryString(); // TODO: remove this
    sidebarMenuBundle();
    companyDetailBundle();
    calendar();
});

function convertPathToBase64(path = []) {
    var arrPromise = path.map(pathItem => {
        return toDataURL(pathItem);
    })
    return Promise.all(arrPromise);
}

function gradientScroll(elm) {
    $($(elm).parent()).addClass('inspecting--scroll')
    $(elm).prepend('<div class="before--gradient"></div>');
    $(elm).append('<div class="after--gradient"></div>');

    $(elm).on('scroll', function (e) {
        $(elm).removeClass('begin end between');
        if (e.target.scrollLeftMax == 0) {
            return;
        }
        if (e.target.scrollLeft == e.target.scrollLeftMax) {
            $(elm).addClass('end');
        } else if (e.target.scrollLeft == 0) {
            $(elm).addClass('begin');
        } else {
            $(elm).addClass('between');
        }
    });

    $(elm).trigger('scroll');
}

function pdf(base64Img, includedCate) {

    const base64_table_bear = base64Img[0];
    const base64_table_bear_tsd = base64Img[1];
    const base64_table_whale_waste = base64Img[2];
    const base64_table_whale_waste_manage = base64Img[3];

    const cateSequences = function(includedCate) {
        return `${includedCate.bear == '1' ? 'X':'O'}${includedCate.bear_tsd == '1' ? 'X':'O'}${includedCate.whale == '1' ? 'X':'O'}${includedCate.elephant == '1' ? 'X':'O'}`;
    };
    var pages = 0;
    var pageState = cateSequences(includedCate);

    switch(pageState) {
        case 'XXX':
            pages = 5;
            break;
        case 'XXO': 
            pages = 4;
            break;
        case 'XOX': 
            pages = 3;
            break;
        case 'XOO': 
            pages = 2;
            break;
        case 'OXX': 
            pages = 4;
            break;
        case 'OXO': 
            pages = 3;
            break;
        case 'OOX': 
            pages = 3;
            break;
    }

    var _pdf = new jspdf.jsPDF({
        unit: 'px'
    });

    /**
     * font, fontBold is in pdf-font.js
     */

    // font reg
    _pdf.addFileToVFS("angsanaUPC.tff", fontAngsana);
    _pdf.addFont('angsanaUPC.tff', 'angsanaUPC', 'normal');

    // font bold
    _pdf.addFileToVFS("angsanaUPC-bold.tff", fontAngsanaBold);
    _pdf.addFont('angsanaUPC-bold.tff', 'angsanaUPC', 'bold');

    _pdf.setFontSize(26);
    _pdf.setFont('angsanaUPC', 'bold');
    _pdf.text("สรุปผลการดำเนินงานการลดก๊าซเรือนกระจก", 223, 50, null, null, "center");

    _pdf.setFontSize(20);
    _pdf.setTextColor('#000000');
    _pdf.setFont('angsanaUPC', 'bold');
    _pdf.text('ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML, 223, 71, null, null, "center");

    // company fullname
    _pdf.setFontSize(24);
    _pdf.setTextColor('#FFA400');
    _pdf.text(outcomeExportData.fullname, 223, 90, null, null, "center");
    // 
    // company image
    _pdf.addImage($('.company--logo img').el[0], 'PNG', 147, 120, 65, 23);

    // climate image
    _pdf.addImage($('.climate--logo img').el[0], 'PNG', 240, 100, 54, 37);

    // climate desc
    _pdf.setFontSize(10);
    _pdf.setTextColor('#424242');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('แพลต์ฟอร์มคำนวณการลดก๊าซเรือนกระจก\nตอบโจทย์ทุกองค์กร วัดผลเป็นรูปธรรม เริ่มได้ทันที', 266, 150, null, null, "center");

    // summary climate progress
    _pdf.setFontSize(18);
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text(outcomeExportData.name + ' ได้ร่วมบริหารจัดการดูแลสิ่งแวดล้อมภายใต้', 223, 175, { maxWidth: 300, align: 'center' }, null, "center");
    _pdf.setTextColor('#FFA400');
    _pdf.text('Climate Care Collaboration Platform', 223, 210, null, null, "center");

    // results
    // (1)
    _pdf.addImage(icon_cloud, 'PNG', 100, 220, 32, 32);

    _pdf.setFontSize(12);
    _pdf.setTextColor('#424242');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('โดยสามารถ\nลดก๊าซเรือนกระจกได้', 116, 263, null, null, "center");

    _pdf.setFontSize(26);
    _pdf.setTextColor('#000000');
    _pdf.setFont('angsanaUPC', 'bold');
    _pdf.text(addComma(outcomeExportData.result_climate.greenhouse_gas_reduced), 118, 293, null, null, "center");

    _pdf.setFontSize(12);
    _pdf.setTextColor('#818181');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 118, 309, null, null, "center");
    _pdf.setDrawColor('#C1C1C1');
    _pdf.line(173, 315, 173, 216, 'S');

    // (2)
    _pdf.addImage(icon_trees, 'PNG', 206, 220, 32, 32);

    _pdf.setFontSize(12);
    _pdf.setTextColor('#424242');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('คำนวณออกมาแล้วเทียบเท่ากับ\nการปลูกต้นไม้อายุ 10 ปี', 223, 263, null, null, "center");

    _pdf.setFontSize(26);
    _pdf.setTextColor('#000000');
    _pdf.setFont('angsanaUPC', 'bold');
    _pdf.text(addComma(outcomeExportData.result_climate.trees_planted), 223, 293, null, null, "center");

    _pdf.setFontSize(12);
    _pdf.setTextColor('#818181');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('ต้น', 223, 304, null, null, "center");
    _pdf.setDrawColor('#C1C1C1');
    _pdf.line(273, 315, 273, 216, 'S');

    // (3)
    _pdf.addImage(icon_cloud, 'PNG', 313, 215, 32, 32);

    _pdf.setFontSize(12);
    _pdf.setTextColor('#424242');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('ดูดซับก๊าซเรือนกระจกจาก\nการปลูกป่าได้', 329, 258, null, null, "center");

    _pdf.setFontSize(26);
    _pdf.setTextColor('#000000');
    _pdf.setFont('angsanaUPC', 'bold');
    _pdf.text(addComma(outcomeExportData.result_climate.greenhouse_gas_absorb), 329, 288, null, null, "center");

    _pdf.setFontSize(12);
    _pdf.setTextColor('#818181');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 329, 304, null, null, "center");

    // text to
    _pdf.setFontSize(20);
    _pdf.setTextColor('#000000');
    _pdf.setFont('angsanaUPC', 'normal');
    _pdf.text('โดยแยกเป็นผลการดำเนินงานในแต่ละโครงการความร่วมมือดังนี้', 223, 339, null, null, "center");

    if (pageState != 'OXX' && pageState != 'OXO' && pageState != 'OOX') {
        // bear
        _pdf.setFillColor('#20BEEE');
        _pdf.rect(0, 360, 503, 65, 'F');
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#ffffff');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text('โครงการ Care the Bear', 223, 383, null, null, "center");
    
        _pdf.setFontSize(20);
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โครงการลดการปล่อยก๊าซเรือนกระจกจากการจัดงานหรือทุกกิจกรรม', 223, 400, null, null, "center");
        _pdf.text('ในรูปแบบ Online และ Onsite', 223, 415, null, null, "center");
    
        _pdf.setTextColor('#000000');
        _pdf.setLineHeightFactor(1);
        _pdf.text('ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML+' ' + outcomeExportData.name + ' ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการดำเนินกิจกรรมในรูปแบบต่างๆ\nทั้งสิ้น ' + outcomeExportData.bear.reduce_gas_activity + ' กิจกรรม ได้แก่ การประชุมผู้ถือหุ้น ' + outcomeExportData.bear.meeting + ' ครั้ง การจัดอบรม ' + outcomeExportData.bear.training + ' ครั้ง \nการจัดงานมอบรางวัล ' + outcomeExportData.bear.give_awards + ' ครั้ง', 223, 445, { maxWidth: 460, align: 'center' }, null, "center");
    
        //result : bear
        // (1)
        _pdf.addImage(icon_cloud_bear, 'PNG', 135, 520, 35, 35);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โดยสามารถลดก๊าซเรือนกระจกได้', 152, 575, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.bear.climate_data.greenhouse_gas_reduced), 152, 595, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 152, 610, null, null, "center");
        _pdf.setDrawColor('#C1C1C1');
        _pdf.line(223, 620, 223, 510, 'S');
    
        // (2)
        _pdf.addImage(icon_trees_bear, 'PNG', 277, 520, 35, 35);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('เทียบเท่ากับการปลูกต้นไม้อายุ 10 ปีได้', 295, 575, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.bear.climate_data.compare_tree_planted), 295, 595, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('ต้น', 295, 610, null, null, "center");
    
        _pdf.addPage();
        _pdf.setPage(2);
    
        // PAGE 2
        _pdf.setFontSize(20);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text('ปริมาณการลดก๊าซเรือนกระจก หลักการ 6 Cares', 223, 50, null, null, "center");
    
    
        // bear table
        _pdf.setFontSize(10);
        _pdf.setTextColor('#424242');
        _pdf.addImage(base64_table_bear, 'PNG', 50, 70, 345, 241); // WARNING: use image to create table instead of pdf.table()
    
        _pdf.setFontSize(16);
        _pdf.setTextColor('#20BEEE');
        _pdf.setFont('angsanaUPC', 'normal');
    
        var tableRowX = ['125', '158', '186', '214', '242', '270', '298']
        var sumCol1 = 0;
        var sumCol2 = 0;
        outcomeExportData.bear.progress_detail.forEach((detail, i) => {
            sumCol1 += detail.value[0];
            sumCol2 += detail.value[1];
            _pdf.setFont('angsanaUPC', 'normal');
            _pdf.text(addComma(detail.value[0]), 280, tableRowX[i], null, null, "center");
            _pdf.text(addComma(detail.value[1]), 355, tableRowX[i], null, null, "center");
        });
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(sumCol1), 280, 300, null, null, "center");
        _pdf.text(addComma(sumCol2), 355, 300, null, null, "center");
    }    
    if (pageState != 'OXX' && pageState != 'OXO' && pageState != 'OOX') {
        // bear_tsd
        _pdf.setFillColor('#20BEEE');
        _pdf.rect(0, 360, 503, 65, 'F');
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#ffffff');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text('โครงการ TSD Care the Bear', 223, 383, null, null, "center");
    
        _pdf.setFontSize(20);
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โครงการลดการปล่อยก๊าซเรือนกระจกจากการลดใช้กระดาษ', 223, 400, null, null, "center");
        _pdf.text('ของ Corporate Action', 223, 415, null, null, "center");
        _pdf.setTextColor('#000000');
        _pdf.setLineHeightFactor(1);
        _pdf.text('ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML+' ' + outcomeExportData.name + ' ได้มีการดำเนินการลดก๊าซเรือนกระจก\nจากการดำเนินโครงการ TSD Care the Bear' /*+ outcomeExportData.bear_tsd.reduce_gas_activity +  'ครั้ง' */, 223, 448, { maxWidth: 460, align: 'center' }, null, "center");


    //result : bear_tsd
        // (1)
        _pdf.addImage(icon_cloud_bear, 'PNG', 135, 500, 35, 35);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โดยสามารถลดก๊าซเรือนกระจกได้', 152, 555, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.bear_tsd.climate_data.greenhouse_gas_reduced), 152, 575, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 152, 590, null, null, "center");
        _pdf.setDrawColor('#C1C1C1');
        _pdf.line(223, 620, 223, 490, 'S');
    
        // (2)
        _pdf.addImage(icon_trees_bear, 'PNG', 277, 500, 35, 35);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('เทียบเท่ากับการปลูกต้นไม้อายุ 10 ปีได้', 295, 555, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.bear_tsd.climate_data.compare_tree_planted), 295, 575, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('ต้น', 295, 590, null, null, "center");
    
        _pdf.addPage();
        _pdf.setPage(3);
    
        // PAGE 3
        _pdf.setFontSize(20);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text('TSD Care the Bear', 223, 50, null, null, "center");
    
    
        // bear_tsd table
        _pdf.setFontSize(10);
        _pdf.setTextColor('#424242');
        _pdf.addImage(base64_table_bear_tsd, 'PNG', 50, 70, 345, 500); // WARNING: use image to create table instead of pdf.table()
    
        _pdf.setFontSize(18);
        _pdf.setTextColor('#20BEEE');
        _pdf.setFont('angsanaUPC', 'normal');
    
        var tableRowX = ['125', '148', '171', '194', '217', '245', '268', '292', '317', '342', '367', '392', '417', '442', '465', '488', '512', '536', '561']
        var sumCol1 = 0;
        var sumCol2 = 0;
        var sumCol3 = 0;
        outcomeExportData.bear_tsd.progress_detail_tsd.forEach((detail, i) => {
            sumCol1 += detail.value[0];
            sumCol2 += detail.value[1];
            sumCol3 += detail.value[2];
            _pdf.setFont('angsanaUPC', 'normal');
            _pdf.text(addComma(detail.value[0]), 220, tableRowX[i], null, null, "center");
            _pdf.text(addComma(detail.value[1]), 290, tableRowX[i], null, null, "center");
            _pdf.text(addComma(detail.value[2]), 350, tableRowX[i], null, null, "center");
        });

        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(sumCol1), 220, 561, null, null, "center");
        _pdf.text(addComma(sumCol2), 285, 561, null, null, "center");
        _pdf.text(addComma(sumCol3), 355, 561, null, null, "center");
    }
    _pdf.addPage();
    _pdf.setPage(4);

    //Page4
    if (pageState != 'XOX' && pageState != 'XOO' && pageState != 'OOX') {
        var adjustSpace = 0;
        if (pageState.charAt(0) == 'O') {
            adjustSpace = 30;
        }
        // whale
        _pdf.setFillColor('#106BAF');
        _pdf.rect(0, 30 + adjustSpace, 503, 60, 'F');
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#ffffff');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text('โครงการ Care the Whale', 223, 50 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(20);
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โครงการลดการปล่อยก๊าซเรือนกระจกจากการบริหารจัดการขยะตั้งแต่ต้นทางถึงปลายทาง', 223, 65 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(20);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(outcomeExportData.name, 223, 108 + adjustSpace, null, null, "center");
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการคัดแยกขยะและจัดการขยะ', 223, 125 + adjustSpace, null, null, "center");
    
        // result : whale
        // (1)
        _pdf.addImage(icon_cloud_whale, 'PNG', 130, 148 + adjustSpace, 45, 45);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('โดยสามารถลดก๊าซเรือนกระจกได้', 152, 223 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.whale.climate_data.greenhouse_gas_reduced), 152, 243 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 152, 258 + adjustSpace, null, null, "center");
        _pdf.setDrawColor('#C1C1C1');
        _pdf.line(223, 148 + adjustSpace, 223, 260 + adjustSpace, 'S');
    
        // (2)
        _pdf.addImage(icon_trees_whale, 'PNG', 272, 148 + adjustSpace, 45, 45);
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#424242');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('เทียบเท่ากับการปลูกต้นไม้อายุ 10 ปีได้', 295, 223 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(26);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(outcomeExportData.whale.climate_data.compare_tree_planted), 295, 243 + adjustSpace, null, null, "center");
    
        _pdf.setFontSize(12);
        _pdf.setTextColor('#818181');
        _pdf.setFont('angsanaUPC', 'normal');
        _pdf.text('ต้น', 295, 258 + adjustSpace, null, null, "center");
        
        //result waste
        _pdf.setFontSize(20);
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(outcomeExportData.whale.wasteTableTitle, 223, 280, null, null, "center");
    
        _pdf.addImage(base64_table_whale_waste, 'PNG', 50, 300, 345, 181); // WARNING: use image to create table instead of pdf.table()
    
        _pdf.setFontSize(18);
        _pdf.setTextColor('#106BAF');
        _pdf.setFont('angsanaUPC', 'normal');
    
        var tableRowX = ['360', '388', '416', '443']
        var sumCol1 = 0;
        var sumCol2 = 0;
        var sumCol3 = 0;
        outcomeExportData.whale.progress_detail.waste.forEach((detail, i) => {
            sumCol1 += detail.value[0];
            sumCol2 += detail.value[1];
            sumCol3 += detail.value[2];
            _pdf.setFont('angsanaUPC', 'normal');
            _pdf.text(addComma(detail.value[0]), 203, tableRowX[i], null, null, "center");
            _pdf.text(addComma(detail.value[1]), 279, tableRowX[i], null, null, "center");
            _pdf.text(addComma(detail.value[2]), 355, tableRowX[i], null, null, "center");
        });
        _pdf.setTextColor('#000000');
        _pdf.setFont('angsanaUPC', 'bold');
        _pdf.text(addComma(sumCol1), 203, 470, null, null, "center");
        _pdf.text(addComma(sumCol2), 279, 470, null, null, "center");
        _pdf.text(addComma(sumCol3), 355, 470, null, null, "center");   
          // doughnut chart

        _pdf.addImage($('#waste').el[0], 'PNG', 65, 485, 345, 175);
        _pdf.addPage();
        _pdf.setPage(5);
    
 // PAGE 5


 _pdf.setFontSize(20);
 _pdf.setTextColor('#000000');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text(outcomeExportData.whale.wasteManageTableTitle, 223, 50, null, null, "center");

 _pdf.addImage(base64_table_whale_waste_manage, 'PNG', 50, 70, 345, 300); // WARNING: use image to create table instead of pdf.table()

 _pdf.setFontSize(18);
 _pdf.setTextColor('#106BAF');
 _pdf.setFont('angsanaUPC', 'normal');

 var tableRowX = ['130', '155', '180', '205', '233', '258', '283', '308', '333']
 var sumCol1 = 0;
 var sumCol2 = 0;
 var sumCol3 = 0;
 var sumCol4 = 0;
 outcomeExportData.whale.progress_detail.wasteManage.forEach((detail, i) => {
     sumCol1 += parseFloat(detail.value[0]);
     sumCol2 += detail.value[1];
     sumCol3 += detail.value[2];
     sumCol4 += detail.value[3];
     _pdf.setFont('angsanaUPC', 'normal');
     _pdf.text(addComma(detail.value[0]), 160, tableRowX[i], null, null, "center");
     _pdf.text(addComma(detail.value[1]), 216, tableRowX[i], null, null, "center");
     _pdf.text(addComma(detail.value[2]), 285, tableRowX[i], null, null, "center");
     _pdf.text(addComma(detail.value[3]), 360, tableRowX[i], null, null, "center");
 });
 _pdf.setTextColor('#000000');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text(addComma(sumCol1) + '%', 160, 358, null, null, "center");
 _pdf.text(addComma(sumCol2), 216, 358, null, null, "center");
 _pdf.text(addComma(sumCol3), 285, 358, null, null, "center");
 _pdf.text(addComma(sumCol4), 360, 358, null, null, "center");

 _pdf.setFontSize(20);
 _pdf.setTextColor('#000000');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text('อันดับวิธีการจัดการขยะ ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML, 223, 400, null, null, "center");

 // bar chart
 _pdf.addImage($('#wasteManage').el[0], 'PNG', 20, 410, 398, 206);
}    
if (pageState != 'XXO' && pageState != 'XOO' && pageState != 'OXO') {
 var adjustSpace = 0;
 var elephantStarts = (pageState.charAt(0) == 'O' && pageState.charAt(1) == 'O');
 if (elephantStarts) {
     adjustSpace = 320;
 } else {
     
     _pdf.addPage();
     _pdf.setPage(6);
 }
//Page6
 _pdf.setFillColor('#6DC169');
 _pdf.rect(0, 40 + adjustSpace, 503, 55, 'F');
 _pdf.setFontSize(20);
 _pdf.setTextColor('#FFFFFF');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text(outcomeExportData.elephant.climate_name, 225, 63 + adjustSpace, null, null, "center");
 _pdf.setFontSize(16);
 _pdf.setFont('angsanaUPC', 'normal');
 _pdf.text(outcomeExportData.elephant.climate_desc, 223, 81 + adjustSpace, null, null, "center");

 _pdf.setFontSize(20);
 _pdf.setTextColor('#000000');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text(outcomeExportData.name, 223, 120 + adjustSpace, null, null, "center");
 _pdf.setFontSize(16);
 _pdf.setFont('angsanaUPC', 'normal');
 _pdf.text('ได้ระดมทุนปลูกป่าในโครงการฯ', 223, 134 + adjustSpace, null, null, "center");

 var listLocationPosY = 0;
 outcomeExportData.elephant.progress_detail.locations.forEach((location, i) => {
     var txt = location.area + ' ' + location.trees + ' ต้น ณ ' + location.village + ' ' + location.location;
     var txtDimension = _pdf.getTextDimensions(txt);
     _pdf.setTextColor('#000000');
     _pdf.text(((i + 1) + '. '), (231 - (txtDimension.w / 2) - 4), (140 + (i * 15 + txtDimension.h)) + adjustSpace, { maxWidth: 300, align: 'center' }, null, "center");
     _pdf.setTextColor('#6DC169');
     _pdf.setFont('angsanaUPC', 'normal');
     _pdf.text(txt, 231, (140 + (i * 15 + txtDimension.h)) + adjustSpace, null, null, "center");
     listLocationPosY = (140 + (i * 15 + txtDimension.h)) + adjustSpace;
 });

 if (adjustSpace != 0) {
     listLocationPosY = 15;
     adjustSpace = 10;
     // PAGE 7
     _pdf.addPage();
     _pdf.setPage(7);
 }

 // (1)
 _pdf.addImage(icon_cloud_wild, 'PNG', 203, listLocationPosY + 25 + adjustSpace, 40, 40);

 _pdf.setFontSize(12);
 _pdf.setTextColor('#424242');
 _pdf.setFont('angsanaUPC', 'normal');
 _pdf.text('สามารถดูดซับก๊าซเรือนกระจกได้', 223, listLocationPosY + 80 + adjustSpace, null, null, "center");

 _pdf.setFontSize(26);
 _pdf.setTextColor('#000000');
 _pdf.setFont('angsanaUPC', 'bold');
 _pdf.text(addComma(outcomeExportData.elephant.climate_data.greenhouse_gas_absorb), 223, listLocationPosY + 100 + adjustSpace, null, null, "center");

 _pdf.setFontSize(12);
 _pdf.setTextColor('#818181');
 _pdf.setFont('angsanaUPC', 'normal');
 _pdf.text('กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า', 223, listLocationPosY + 115 + adjustSpace, null, null, "center");

 _pdf.setFontSize(16);
 _pdf.setTextColor('#000000');
 var txtForest = '';

 outcomeExportData.elephant.progress_detail.locations.forEach((location, i) => {
     txtForest += location.village + (i + 1 < outcomeExportData.elephant.progress_detail.locations.length ? ', ' : '');
 });
 var spaceText = '';
 if (parseInt(outcomeExportData.elephant.progress_detail.houses) > 99999) {
     spaceText = '                  ';
 } else if (parseInt(outcomeExportData.elephant.progress_detail.houses) > 9999) {
     spaceText = '                ';
 } else if (parseInt(outcomeExportData.elephant.progress_detail.houses) > 999) {
     spaceText = '              ';
 } else if (parseInt(outcomeExportData.elephant.progress_detail.houses) > 99) {
     spaceText = '          ';
 } else if (parseInt(outcomeExportData.elephant.progress_detail.houses) > 9) {
     spaceText = '       ';
 } else {
     spaceText = '   ';
 }
 _pdf.text('ทำในการนี้ ทำให้' + spaceText + 'ครัวเรือนใน', 223, listLocationPosY + 140 + adjustSpace, null, null, "center");
 _pdf.setTextColor('#6DC169');
 _pdf.text(addComma(outcomeExportData.elephant.progress_detail.houses), 231, listLocationPosY + 140 + adjustSpace, null, null, "center");
 _pdf.text(txtForest, 223, listLocationPosY + 155 + adjustSpace, { maxWidth: 400, align: 'center' }, null, "center");
 var textDimension = _pdf.getTextDimensions(txtForest);
 _pdf.setTextColor('#000000');
 _pdf.text('สามารถหาอยู่หากินมีรายได้จากการเพาะกล้าไม้ ซึ่งสร้างเศรษฐกิจโดยรอบให้ กับป่าชุมชนอีกทั้งยังช่วยในเรื่องการรักษาสมดุลทางธรรมชาติให้กับระบบนิเวศ', 223, (listLocationPosY + 155) + (Math.ceil(textDimension.w / 400) * 15) + adjustSpace, { maxWidth: 400, align: 'center' }, null, "center");
}
    _pdf.save("Report_Climatecare.pdf");
}
function getImage(source) {
    return new Promise(resolve => {
        var img = new Image();
        img.onload = function () {
            resolve(this);
        }
        img.src = source;
    });
}

function exportDocx(base64Img, includedCate) {

    const base64_climate_logo = base64Img[3];
    const base64_company_logo = base64Img[4];

    if (includedCate.whale) {
        var doughnutImg = chart.toBase64Image('image/png', 1);
        var doughnutImg_width = 0;
        var doughnutImg_height = 0;
        getImage(doughnutImg).then( res => { 
            doughnutImg_width = res.width; 
            doughnutImg_height = res.height;
        })
        var barImg = barChart.toBase64Image('image/png', 1);
    }
    getImage(base64_company_logo).then(img => {

        function imgSizeWithRatio(img) {
            var imgSize = { width: img.width, height: img.height };
            const maxWidth = 115;
            const maxHeight = 41;
            var imgRatio = img.height / img.width;
            if (img.width > maxWidth) {
                imgSize = { width: maxWidth, height: maxWidth * imgRatio }
            } else {
                imgSize = { width: maxHeight * imgRatio, height: maxHeight }
            }
            return imgSize;
        }

        const cellTableTmpl = function (msg, fill = 'ffffff', color = '424242', bold = false, align = 'left', border = { Top: false, Bottom: false, Left: false, Right: false, color: 'ffffff' }) {
            return new docx.TableCell({
                borders: {
                    top: {
                        style: docx.BorderStyle.SINGLE,
                        size: border.Top ? 8 : 1,
                        color: border.Top ? "E0E0E0" : fill,
                    },
                    left: {
                        style: border.Left ? docx.BorderStyle.SINGLE : docx.BorderStyle.NIL,
                        size: border.Left ? 8 : 1,
                        color: border.Left ? "E0E0E0" : fill,
                    },
                    right: {
                        style: border.Right ? docx.BorderStyle.SINGLE : docx.BorderStyle.NIL,
                        size: border.Right ? 8 : 1,
                        color: border.Right ? "E0E0E0" : fill,
                    },
                    bottom: {
                        style: docx.BorderStyle.SINGLE,
                        size: border.Bottom ? 8 : 1,
                        color: border.Bottom ? "E0E0E0" : fill,
                    },
                },
                shading: {
                    fill: fill
                },
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                    new docx.Paragraph({
                        style: 'CENTER',
                        children: [
                            new docx.TextRun({
                                text: addComma(msg),
                                size: 32,
                                font: 'Angsana New',
                                color: color,
                                bold: bold
                            }),
                        ],
                        alignment: align,
                        indent: {
                            start: 100,
                            left: 100,
                            end: 100
                        }
                    })
                ]
            });
        }

        const tableLogo = new docx.Table({
            columnWidths: [1500, 2005, 3500, 1500],
            rows: [
                new docx.TableRow({
                    children: [
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'Space',
                                            color: 'ffffff',
                                            size: 50
                                        })
                                    ],
                                    alignment: 'center',
                                    style: 'CENTER'
                                })
                            ]
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 2005,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: base64_company_logo,
                                            transformation: imgSizeWithRatio(img),
                                            wrap: {
                                                type: docx.TextWrappingType.TOP_AND_BOTTOM,
                                                side: docx.TextWrappingSide.BOTH_SIDES,
                                            },
                                        })
                                    ],
                                    alignment: 'center',
                                    style: 'CENTER'
                                })
                            ],
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: base64_climate_logo,
                                            transformation: {
                                                width: 89,
                                                height: 60
                                            },
                                            wrap: {
                                                type: docx.TextWrappingType.TOP_AND_BOTTOM,
                                                side: docx.TextWrappingSide.BOTH_SIDES,
                                            },
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'แพลต์ฟอร์มคำนวณการลดก๊าซเรือนกระจก',
                                            size: 24,
                                            color: '424242'
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ตอบโจทย์ทุกองค์กร วัดผลเป็นรูปธรรม เริ่มได้ทันที',
                                            size: 24,
                                            color: '424242'
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                            ],
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'Space',
                                            color: 'ffffff',
                                            size: 50
                                        })
                                    ],
                                    alignment: 'center',
                                    style: 'CENTER'
                                })
                            ]
                        }),
                    ],
                }),
            ],
            alignment: docx.AlignmentType.CENTER
        });
        const tableClimateResult = new docx.Table({
            columnWidths: [3500, 3500, 3500],
            rows: [
                new docx.TableRow({
                    children: [
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_cloud,
                                            transformation: {
                                                width: 70,
                                                height: 70,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'โดยสามารถ',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ลดก๊าซเรือนกระจกได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.greenhouse_gas_reduced),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),

                            ]
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_trees,
                                            transformation: {
                                                width: 70,
                                                height: 70,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'คำนวณออกมาแล้วเทียบเท่ากับ',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'การปลูกต้นไม้อายุ 10 ปี',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.trees_planted),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ต้น',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                            ]
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_cloud,
                                            transformation: {
                                                width: 70,
                                                height: 70,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ดูดซับก๊าซเรือนกระจกจาก',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'การปลูกป่าได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.greenhouse_gas_absorb),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                            ]
                        }),
                    ]
                }),
            ],
            alignment: docx.AlignmentType.CENTER
        });
        const tableBearResult = new docx.Table({
            columnWidths: [3500, 3500],
            rows: [
                new docx.TableRow({
                    children: [
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_cloud_bear,
                                            transformation: {
                                                width: 80,
                                                height: 80,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'โดยสามารถลดก๊าซเรือนกระจกได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.greenhouse_gas_reduced),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),

                            ]
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_trees_bear,
                                            transformation: {
                                                width: 80,
                                                height: 80,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'เทียบเท่ากับการปลูกต้นไม้อายุ 10 ปีได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.trees_planted),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ต้น',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                            ]
                        }),
                    ]
                }),
            ],
            alignment: docx.AlignmentType.CENTER
        });
        const tableWhaleResult = new docx.Table({
            columnWidths: [3500, 3500],
            rows: [
                new docx.TableRow({
                    children: [
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: []
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_cloud_whale,
                                            transformation: {
                                                width: 80,
                                                height: 80,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: []
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'โดยสามารถลดก๊าซเรือนกระจกได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.greenhouse_gas_reduced),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: []
                                }),
                            ]
                        }),
                        new docx.TableCell({
                            borders: {
                                top: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                left: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 8,
                                    color: "E0E0E0",
                                },
                                right: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                                bottom: {
                                    style: docx.BorderStyle.SINGLE,
                                    size: 1,
                                    color: "ffffff",
                                },
                            },
                            verticalAlign: docx.VerticalAlign.CENTER,
                            width: {
                                size: 3500,
                                type: docx.WidthType.DXA,
                            },
                            children: [
                                new docx.Paragraph({
                                    children: []
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.ImageRun({
                                            data: icon_trees_whale,
                                            transformation: {
                                                width: 80,
                                                height: 80,
                                            }
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: []
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'เทียบเท่ากับการปลูกต้นไม้อายุ 10 ปีได้',
                                            color: '424242',
                                            size: 24,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: addComma(outcomeExportData.result_climate.trees_planted),
                                            color: '000000',
                                            size: 44,
                                            bold: true,
                                            font: 'Angsana New',
                                        }),
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: [
                                        new docx.TextRun({
                                            text: 'ต้น',
                                            color: '818181',
                                            size: 24,
                                            font: 'Angsana New',
                                        })
                                    ],
                                    style: 'CENTER',
                                    alignment: 'center'
                                }),
                                new docx.Paragraph({
                                    children: []
                                }),
                            ]
                        }),
                    ]
                }),
            ],
            alignment: docx.AlignmentType.CENTER
        });
        var arrDocComponents = {
            summary: [

                new docx.Paragraph({
                    text: "สรุปผลการดำเนินงานการลดก๊าซเรือนกระจก",
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML,
                            bold: true,
                            size: 40,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_2,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    text: outcomeExportData.fullname,
                    style: 'bold-orange',
                    alignment: 'center',
                    spacing: {
                        after: 200,
                    },
                }),
                tableLogo,
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: outcomeExportData.name,
                            bold: false,
                            size: 40,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: ' ได้ร่วมบริหารจัดการดูแลสิ่งแวดล้อมภายใต้',
                            bold: false,
                            size: 40,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_2,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'Climate Care Collaboration Platform ',
                            color: '#FFA400',
                            bold: false,
                            size: 46,
                            font: 'Angsana New',
                        }),

                    ],
                    heading: docx.HeadingLevel.HEADING_2,
                    alignment: 'center'
                }),
                tableClimateResult,
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'โดยแยกเป็นผลการดำเนินงานในแต่ละโครงการความร่วมมือดังนี้',
                            size: 42,
                            color: '000000',
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_2,
                    alignment: 'center',
                    spacing: {
                        before: 300,
                        after: 400
                    }
                }),
            ],
            bear: [],
            whale: [],
            elephant: []
        };

        // BEAR
        if (includedCate.bear) {
            var sumTableBearCol1 = 0;
            var sumTableBearCol2 = 0;
            outcomeExportData.bear.progress_detail.forEach((a, b) => {
                sumTableBearCol1 += a.value[0];
                sumTableBearCol2 += a.value[1];
            })
            const tableBear = new docx.Table({
                columnWidths: [4500, 2100, 2100],
                rows: [
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        children: [
                                            new docx.TextRun({
                                                text: 'หลักการ 6 Cares',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณการลดก๊าซ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เรือนกระจก (kg.CO',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                            new docx.TextRun({
                                                text: '2',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242',
                                                subScript: true
                                            }),
                                            new docx.TextRun({
                                                text: 'e)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เทียบเท่ากับ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'การปลูกต้นไม้ (ต้น)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[0].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[0].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[0].value[1], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[1].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[1].value[0], 'ffffff', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[1].value[1], 'ffffff', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[2].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[2].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[2].value[1], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[3].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[3].value[0], 'ffffff', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[3].value[1], 'ffffff', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[4].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[4].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[4].value[1], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl(outcomeSummary.bear.progress_detail[5].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[5].value[0], 'ffffff', '20BEEE', false, 'center'),
                            cellTableTmpl(outcomeSummary.bear.progress_detail[5].value[1], 'ffffff', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('รวม', 'D2F2FC', '000000', true, 'left', { Left: true, Bottom: true }),
                            cellTableTmpl(addComma(sumTableBearCol1), 'D2F2FC', '000000', true, 'center', { Bottom: true, color: 'D2F2FC' }),
                            cellTableTmpl(addComma(sumTableBearCol2), 'D2F2FC', '000000', true, 'center', { Right: true, Bottom: true, color: 'D2F2FC' }),
                        ]
                    }),
                ],
                alignment: docx.AlignmentType.CENTER
            })
            arrDocComponents.bear = [
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '20BEEE',
                            size: 12,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการ Care the Bear',
                            color: 'ffffff',
                            bold: true,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 200,
                        after: 0
                    }
                }),

                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการจัดงานหรือทุกกิจกรรม',
                            color: 'ffffff',
                            size: 42,
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center'
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'ในรูปแบบ Online และ Onsite',
                            color: 'ffffff',
                            size: 42,
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 0,
                        after: 200
                    }
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '20BEEE',
                            size: 12,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ตั้งแต่ ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: $(".pick--since").get(0).innerHTML,
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: ' ถึง ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: $(".pick--to").get(0).innerHTML + ' ',
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: outcomeExportData.name,
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        before: 400
                    }
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการดำเนินกิจกรรมในรูปแบบต่างๆ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ทั้งสิ้น ' + outcomeExportData.bear.reduce_gas_activity + ' กิจกรรม ได้แก่ การประชุมผู้ถือหุ้น ' + outcomeExportData.bear.meeting + ' ครั้ง การจัดอบรม ' + outcomeExportData.bear.training + ' ครั้ง \nการจัดงานมอบรางวัล ' + outcomeExportData.bear.give_awards + ' ครั้ง',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [],
                }),
                tableBearResult,
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ปริมาณการลดก๊าซเรือนกระจกการหลักการ 6 Cares',
                            size: 44,
                            bold: true
                        })
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 300,
                        after: 300
                    }
                }),
                tableBear,
                new docx.Paragraph({
                    children: [],
                    spacing: {
                        after: 800
                    }
                }),
            ];
        }

        //bear_tsd
        if (includedCate.bear_tsd) {
            var sumTableBear_tsdCol1 = 0;
            var sumTableBear_tsdCol2 = 0;
            var sumTableBear_tsdCol3 = 0;
            outcomeExportData.bear_tsd.progress_detail_tsd.forEach((a, b) => {
                sumTableBear_tsdCol1 += a.value[0];
                sumTableBear_tsdCol2 += a.value[1];
                sumTableBear_tsdCol3 += a.value[2];
            })
            const tableBear_tsd = new docx.Table({
                columnWidths: [20000, 5100, 5100, 5100],
                rows: [
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        children: [
                                            new docx.TextRun({
                                                text: 'Corporate Action',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        children: [
                                            new docx.TextRun({
                                                text: 'จำนวนครั้ง',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณการลดก๊าซ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เรือนกระจก (kg.CO',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                            new docx.TextRun({
                                                text: '2',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242',
                                                subScript: true
                                            }),
                                            new docx.TextRun({
                                                text: 'e)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เทียบเท่ากับ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'การปลูกต้นไม้ (ต้น)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Add Warrant (AW)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[0].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[0].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[0].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Capital Decrease (CD)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[1].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[1].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[1].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Capital Decrease / Change Security (CN)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[2].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[2].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[2].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Deregistration (DG)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[3].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[3].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[3].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Merger and Acquisition (MA)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[4].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[4].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[4].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('New Registration (NR)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[5].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[5].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[5].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Capital Decrease / Change Security (PO)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[6].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[6].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[6].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Split / Consolidate (Change Par)(SC)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[7].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[7].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[7].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),

                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Excluding Other Benefit (XB)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[8].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[8].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[8].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Dividend (XD)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[9].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[9].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[9].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Right Exercise', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[10].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[10].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[10].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Interest Payment (XI)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[11].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[11].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[11].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Voting/Meeting (XM)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[12].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[12].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[12].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Book Closing for Other Purpose (XO)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[13].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[13].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[13].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),new docx.TableRow({
                        children: [
                            cellTableTmpl('Bond Principal (XP)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[14].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[14].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[14].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Right Offering (XR)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[15].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[15].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[15].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('Short Term Warrant (XS)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[16].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[16].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[16].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('TSR Transferable Subscription Right (XT)', 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[17].value[0], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[17].value[1], 'FBFBFB', '20BEEE', false, 'center',),
                            cellTableTmpl(outcomeSummary.bear_tsd.progress_detail_tsd[17].value[2], 'FBFBFB', '20BEEE', false, 'center', { Right: true }),
                        ]
                    }),
               new docx.TableRow({
                        children: [
                            cellTableTmpl('รวม', 'D2F2FC', '000000', true, 'left', { Left: true, Bottom: true }),
                            cellTableTmpl(addComma(sumTableBear_tsdCol1), 'D2F2FC', '000000', true, 'center', { Bottom: true, color: 'D2F2FC' }),
                            cellTableTmpl(addComma(sumTableBear_tsdCol2), 'D2F2FC', '000000', true, 'center', { Bottom: true, color: 'D2F2FC' }),
                            cellTableTmpl(addComma(sumTableBear_tsdCol3), 'D2F2FC', '000000', true, 'center', { Right: true, Bottom: true, color: 'D2F2FC' }),
                        ]
                    }),
                ],
                alignment: docx.AlignmentType.CENTER
            })
            arrDocComponents.bear_tsd = [
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '20BEEE',
                            size: 12,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการ Care the Bear_tsd',
                            color: 'ffffff',
                            bold: true,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 200,
                        after: 0
                    }
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการจัดงานหรือทุกกิจกรรม',
                            color: 'ffffff',
                            size: 42,
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center'
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'ในรูปแบบ Online และ Onsite',
                            color: 'ffffff',
                            size: 42,
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 0,
                        after: 200
                    }
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '20BEEE'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '20BEEE',
                            size: 12,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ตั้งแต่ ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: $(".pick--since").get(0).innerHTML,
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: ' ถึง ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: $(".pick--to").get(0).innerHTML + ' ',
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                        new docx.TextRun({
                            text: outcomeExportData.name,
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        before: 400
                    }
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการดำเนินกิจกรรม',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ในการลดการใช้กระดาษ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {after: 3000}
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'TSD Care The Bear',
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {after: 300}
                }), 
                tableBear_tsd,
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: '',
                            size: 44,
                            bold: true
                        })
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 300,
                        after: 4000
                    }
                }),

            ];
        }

        // WHALE
        if (includedCate.whale) {
            var sumTableWhaleCol1 = 0;
            var sumTableWhaleCol2 = 0;
            var sumTableWhaleCol3 = 0;
            var wasteDatalist = outcomeExportData.whale.progress_detail.waste.sort((a, b) => {
                return b.value[0] - a.value[0];
            });
            wasteDatalist.forEach((a, b) => {
                sumTableWhaleCol1 += a.value[0];
                sumTableWhaleCol2 += a.value[1];
                sumTableWhaleCol3 += a.value[2];
            })
            const tableWhale__Waste = new docx.Table({
                columnWidths: [3200, 2200, 2200, 2200],
                rows: [
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ประเภทขยะ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณขยะ (กก.)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณการลดก๊าซ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เรือนกระจก (kg.CO',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                            new docx.TextRun({
                                                text: '2',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242',
                                                subScript: true
                                            }),
                                            new docx.TextRun({
                                                text: 'e)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เทียบเท่ากับ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'การปลูกต้นไม้ (ต้น)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                        ],
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('1. ' + wasteDatalist[0].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteDatalist[0].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[0].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[0].value[2], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('2. ' + wasteDatalist[1].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteDatalist[1].value[0], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[1].value[1], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[1].value[2], 'ffffff', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('3. ' + wasteDatalist[2].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteDatalist[2].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[2].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[2].value[2], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('4. ' + wasteDatalist[3].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteDatalist[3].value[0], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[3].value[1], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteDatalist[3].value[2], 'ffffff', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('รวม', 'CFE1EF', '000000', true, 'left', { Left: true, Bottom: true }),
                            cellTableTmpl(addComma(sumTableWhaleCol1), 'CFE1EF', '000000', true, 'center', { Bottom: true, color: 'CFE1EF' }),
                            cellTableTmpl(addComma(sumTableWhaleCol2), 'CFE1EF', '000000', true, 'center', { Bottom: true, color: 'CFE1EF' }),
                            cellTableTmpl(addComma(sumTableWhaleCol3), 'CFE1EF', '000000', true, 'center', { Right: true, Bottom: true, color: 'CFE1EF' }),
                        ]
                    }),
                ],
                alignment: docx.AlignmentType.CENTER
            })
            var sumTableWhaleCol1_manage = 0;
            var sumTableWhaleCol2_manage = 0;
            var sumTableWhaleCol3_manage = 0;
            var sumTableWhaleCol4_manage = 0;
            var wasteManageDatalist = outcomeExportData.whale.progress_detail.wasteManage.sort((a, b) => {
                return b.percent - a.percent;
            });
            outcomeExportData.whale.progress_detail.wasteManage.forEach((a, b) => {
                sumTableWhaleCol1_manage += parseInt(a.value[0].replace('%', ''));
                sumTableWhaleCol2_manage += a.value[1];
                sumTableWhaleCol3_manage += a.value[2];
                sumTableWhaleCol4_manage += a.value[3];
            });
            const tableWhale__WasteManage = new docx.Table({
                columnWidths: [2500, 1000, 1900, 1900, 1900],
                rows: [
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        children: [
                                            new docx.TextRun({
                                                text: 'วิธีการจัดการขยะ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'สัดส่วน',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณขยะ (กก.)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'ปริมาณการลดก๊าซ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เรือนกระจก (kg.CO',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                            new docx.TextRun({
                                                text: '2',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242',
                                                subScript: true
                                            }),
                                            new docx.TextRun({
                                                text: 'e)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 8,
                                        color: "E0E0E0",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                children: [
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'เทียบเท่ากับ',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ]
                                    }),
                                    new docx.Paragraph({
                                        style: 'CENTER',
                                        alignment: 'center',
                                        children: [
                                            new docx.TextRun({
                                                text: 'การปลูกต้นไม้ (ต้น)',
                                                size: 24,
                                                font: 'Angsana New',
                                                color: '424242'
                                            }),
                                        ],
                                        indent: {
                                            start: 100,
                                            left: 100,
                                            end: 100
                                        }
                                    })
                                ]
                            }),
                        ],
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('1. ' + wasteManageDatalist[0].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[0].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[0].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[0].value[2], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[0].value[3], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('2. ' + wasteManageDatalist[1].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[1].value[0], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[1].value[1], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[1].value[2], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[1].value[3], 'ffffff', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('3. ' + wasteManageDatalist[2].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[2].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[2].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[2].value[2], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[2].value[3], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('4. ' + wasteManageDatalist[3].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[3].value[0], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[3].value[1], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[3].value[2], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[3].value[3], 'ffffff', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('5. ' + wasteManageDatalist[4].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[4].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[4].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[4].value[2], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[4].value[3], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('6. ' + wasteManageDatalist[5].txt, 'ffffff', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[5].value[0], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[5].value[1], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[5].value[2], 'ffffff', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[5].value[3], 'ffffff', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('7. ' + wasteManageDatalist[6].txt, 'FBFBFB', '000000', false, 'left', { Left: true }),
                            cellTableTmpl(wasteManageDatalist[6].value[0], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[6].value[1], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[6].value[2], 'FBFBFB', '106BAF', false, 'center',),
                            cellTableTmpl(wasteManageDatalist[6].value[3], 'FBFBFB', '106BAF', false, 'center', { Right: true }),
                        ]
                    }),
                    new docx.TableRow({
                        children: [
                            cellTableTmpl('รวม', 'CFE1EF', '000000', true, 'left', { Left: true, Bottom: true }),
                            cellTableTmpl(addComma(sumTableWhaleCol1_manage + '%'), 'CFE1EF', '000000', true, 'center', { Bottom: true, color: 'CFE1EF' }),
                            cellTableTmpl(addComma(sumTableWhaleCol2_manage), 'CFE1EF', '000000', true, 'center', { Bottom: true, color: 'CFE1EF' }),
                            cellTableTmpl(addComma(sumTableWhaleCol3_manage), 'CFE1EF', '000000', true, 'center', { Bottom: true, color: 'CFE1EF' }),
                            cellTableTmpl(addComma(sumTableWhaleCol4_manage), 'CFE1EF', '000000', true, 'center', { Right: true, Bottom: true, color: 'CFE1EF' }),
                        ]
                    }),
                ],
                alignment: docx.AlignmentType.CENTER
            });
            arrDocComponents.whale = [
                new docx.Paragraph({
                    shading: {
                        fill: '106BAF'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '106BAF',
                            size: 24,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '106BAF'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการ Care the Whale',
                            color: 'ffffff',
                            bold: true,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '106BAF'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการบริหารจัดการขยะตั้งแต่ต้นทางถึงปลายทาง',
                            color: 'ffffff',
                            size: 42,
                            bold: false,
                            font: 'Angsana New',
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '106BAF'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '106BAF',
                            size: 24,
                        }),
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: outcomeExportData.name,
                            bold: true,
                            size: 44,
                            font: 'Angsana New',
                        })
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        before: 300,
                    }
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ได้มีการดำเนินการลดก๊าซเรือนกระจกจากการคัดแยกขยะและจัดการขยะ',
                            bold: false,
                            size: 44,
                            font: 'Angsana New',
                        })
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        after: 300,
                    }
                }),
                tableWhaleResult,
                new docx.Paragraph({
                    children: []
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ประเภทขยะที่ได้จากการคัดแยกขยะ',
                            size: 44,
                            bold: true
                        })
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 300,
                        after: 300
                    }
                }),
                tableWhale__Waste,
                new docx.Paragraph({
                    children: [
                        new docx.ImageRun({
                            data: doughnutImg,
                            transformation: $(window).width() > 940 ? {width: 600,height: 301} : {width: doughnutImg_width,height: doughnutImg_height}
                        })
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        before: 400
                    }
                }),
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'ประเภทการจัดการขยะ',
                            size: 44,
                            bold: true
                        })
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 200,
                        after: 300
                    }
                }),
                tableWhale__WasteManage,
                new docx.Paragraph({
                    children: [
                        new docx.TextRun({
                            text: 'อันดับวิธีการจัดการขยะตั้งแต่ '+$(".pick--since").get(0).innerHTML+' ถึง '+$(".pick--to").get(0).innerHTML,
                            size: 44,
                            bold: true
                        })
                    ],
                    heading: docx.HeadingLevel.HEADING_1,
                    alignment: 'center',
                    spacing: {
                        before: 6000,
                        after: 100
                    }
                }),
                new docx.Paragraph({
                    children: [
                        new docx.ImageRun({
                            data: barImg,
                            transformation: {
                                width: 600,
                                height: 301
                            }
                        })
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                }),
                new docx.Paragraph({
                    children: [],
                    style: 'CENTER',
                    alignment: 'center',
                    spacing: {
                        after: 300
                    }
                }),
                new docx.Paragraph({
                    shading: {
                        fill: '6DC169'
                    },
                    children: [
                        new docx.TextRun({
                            text: 'Space',
                            color: '6DC169',
                            size: 18
                        }),
                    ],
                    style: 'CENTER',
                    alignment: 'center',
                }),
            ];
        }

        // ELEPHANT
        if (includedCate.elephant) {
            const tableElephantResult = new docx.Table({
                columnWidths: [3500],
                rows: [
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                verticalAlign: docx.VerticalAlign.CENTER,
                                width: {
                                    size: 3500,
                                    type: docx.WidthType.DXA,
                                },
                                children: [
                                    new docx.Paragraph({
                                        children: [
                                            new docx.ImageRun({
                                                data: icon_cloud_wild,
                                                transformation: {
                                                    width: 80,
                                                    height: 80,
                                                }
                                            }),
                                        ],
                                        style: 'CENTER',
                                        alignment: 'center'
                                    }),
                                    new docx.Paragraph({
                                        children: []
                                    }),
                                    new docx.Paragraph({
                                        children: [
                                            new docx.TextRun({
                                                text: 'โดยสามารถลดก๊าซเรือนกระจกได้',
                                                color: '424242',
                                                size: 24,
                                                font: 'Angsana New',
                                            }),
                                        ],
                                        style: 'CENTER',
                                        alignment: 'center'
                                    }),
                                    new docx.Paragraph({
                                        children: [
                                            new docx.TextRun({
                                                text: addComma(outcomeExportData.elephant.climate_data.greenhouse_gas_absorb),
                                                color: '000000',
                                                size: 44,
                                                bold: true,
                                                font: 'Angsana New',
                                            }),
                                        ],
                                        style: 'CENTER',
                                        alignment: 'center'
                                    }),
                                    new docx.Paragraph({
                                        children: [
                                            new docx.TextRun({
                                                text: 'กิโลกรัมคาร์บอนไดซ์ออกไซต์เทียบเท่า',
                                                color: '818181',
                                                size: 24,
                                                font: 'Angsana New',
                                            }),
                                        ],
                                        style: 'CENTER',
                                        alignment: 'center'
                                    }),
                                    new docx.Paragraph({
                                        children: []
                                    }),
    
                                ]
                            })
                        ]
                    }),
                ],
                alignment: docx.AlignmentType.CENTER
            });
            const listForest = [];
            var stringListForest = '';
            outcomeExportData.elephant.progress_detail.locations.forEach((forest, index) => {
                listForest.push(
                    new docx.TableRow({
                        children: [
                            new docx.TableCell({
                                borders: {
                                    top: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    left: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    right: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                    bottom: {
                                        style: docx.BorderStyle.SINGLE,
                                        size: 1,
                                        color: "ffffff",
                                    },
                                },
                                children: [
                                    new docx.Paragraph({
                                        children: [
                                            new docx.TextRun({
                                                text: (index + 1) + '. ',
                                                size: 44,
                                                bold: false
                                            }),
                                            new docx.TextRun({
                                                text: forest.area + forest.trees + ' ต้น ณ ' + forest.village + ' ' + forest.location,
                                                size: 44,
                                                bold: false,
                                                color: '6DC169'
                                            })
                                        ],
                                        verticalAlign: docx.VerticalAlign.CENTER,
                                        style: 'CENTER',
                                        alignment: 'center'
                                    })
                                ]
                            })
                        ],
                    })
                );
                stringListForest += forest.village + (index == outcomeExportData.elephant.progress_detail.locations.length - 1 ? '' : ', ')
            });
    
            const tableListForest = new docx.Table({
                columnWidths: [10900],
                rows: listForest,
                alignment: docx.AlignmentType.CENTER
            })
            arrDocComponents.elephant = [
                    new docx.Paragraph({
                        shading: {
                            fill: '6DC169'
                        },
                        children: [
                            new docx.TextRun({
                                text: 'โครงการ Care the Wild “ปลูกป้อง Plant & Protect”',
                                color: 'ffffff',
                                bold: true,
                                font: 'Angsana New',
                            }),
                        ],
                        heading: docx.HeadingLevel.HEADING_1,
                        alignment: 'center',
                    }),
                    new docx.Paragraph({
                        shading: {
                            fill: '6DC169'
                        },
                        children: [
                            new docx.TextRun({
                                text: 'โครงการความร่วมมือปลูกป่าเพื่อเพิ่มผืนป่าและรักษาสมดุลระบบนิเวศตามธรรมชาติ',
                                color: 'ffffff',
                                size: 42,
                                bold: false,
                                font: 'Angsana New',
                            }),
                        ],
                        heading: docx.HeadingLevel.HEADING_1,
                        alignment: 'center',
                    }),
                    new docx.Paragraph({
                        shading: {
                            fill: '6DC169'
                        },
                        children: [
                            new docx.TextRun({
                                text: 'Space',
                                color: '6DC169',
                                size: 18
                            }),
                        ],
                        style: 'CENTER',
                        alignment: 'center',
                    }),
                    new docx.Paragraph({
                        style: 'CENTER',
                        alignment: 'center',
                        children: [
                            new docx.TextRun({
                                text: outcomeExportData.name,
                                bold: true,
                                size: 44,
                            }),
                        ],
                        spacing: {
                            before: 200
                        }
                    }),
                    new docx.Paragraph({
                        style: 'CENTER',
                        alignment: 'center',
                        children: [
                            new docx.TextRun({
                                text: 'ได้ระดมทุนปลูกป่าในโครงการฯ',
                                bold: false,
                                size: 32,
                            })
                        ]
                    }),
                    tableListForest,
                    tableElephantResult,
                    new docx.Paragraph({
                        children: [],
                        style: 'CENTER',
                        alignment: 'center',
                        spacing: {
                            after: 50
                        }
                    }),
                    new docx.Paragraph({
                        children: [
                            new docx.TextRun({
                                text: 'ทำให้ ',
                                size: 32
                            }),
                            new docx.TextRun({
                                text: outcomeExportData.elephant.progress_detail.houses,
                                size: 44,
                                color: '6DC169'
                            }),
                            new docx.TextRun({
                                text: ' ครัวเรือนใน ',
                                size: 32
                            }),
                            new docx.TextRun({
                                text: stringListForest,
                                size: 28,
                                color: '6DC169'
                            }),
                        ],
                        style: 'CENTER',
                        alignment: 'center',
                    }),
                    new docx.Paragraph({
                        children: [
                            new docx.TextRun({
                                text: 'สามารถหาอยู่หากินมีรายได้จากการเพาะกล้าไม้ ซึ่งสร้างเศรษฐกิจโดยรอบให้กับป่าชุมชน',
                                size: 32
                            }),
                        ],
                        style: 'CENTER',
                        alignment: 'center',
                    }),
                    new docx.Paragraph({
                        children: [
                            new docx.TextRun({
                                text: 'อีกทั้งยังช่วยในเรื่องการรักษาสมดุลทางธรรมชาติให้กับระบบนิเวศ',
                                size: 32
                            }),
                        ],
                        style: 'CENTER',
                        alignment: 'center',
                    }),
            ]
        }


        const doc = new docx.Document({
            styles: {
                default: {
                    heading1: {
                        run: {
                            bold: true,
                            font: 'Angsana New',
                            size: 56,
                            color: "000000"
                        },
                    },
                    heading2: {
                        run: {
                            bold: true,
                            font: 'Angsana New',
                            size: 48,
                            color: "000000"
                        },
                    }
                },
                paragraphStyles: [
                    {
                        id: 'bold-orange',
                        name: "bold orange",
                        basedOn: "Normal",
                        next: "Normal",
                        run: {
                            color: '#FFA400',
                            bold: true,
                            size: 48,
                            font: 'Angsana New',
                        }
                    },
                    {
                        id: 'bold-white',
                        name: "bold white",
                        basedOn: "Normal",
                        next: "Normal",
                        run: {
                            color: '#FFFFFF',
                            bold: true,
                            size: 52,
                            font: 'Angsana New',
                        }
                    },
                    {
                        id: 'CENTER',
                        name: 'CENTER',
                        run: {
                            font: 'Angsana New',
                        }
                    }
                ],
            },
            sections: [
                {
                    properties: {
                        page: {
                            margin: {
                                top: 800,
                                bottom: 0,
                                left: 0,
                                right: 0
                            },
                        },
                    },
                    children: arrDocComponents.summary.concat(arrDocComponents.bear.concat(arrDocComponents.bear_tsd.concat(arrDocComponents.whale.concat(arrDocComponents.elephant)))),
                },
            ]
        });

        docx.Packer.toBlob(doc).then(blob => {
            saveAs(blob, "Report_Climatecare.docx");
        });
    });
}