function compantDetailTmpl(data) {
    var html = '';
    html += '<div class="company flex column v-center collapsed">';
        html += '<div class="wrap_company_detail">';
            html += '<div class="company--logo flex center" inviewspy>';
                html += '<img class="IMG" src="'+data.logoImg+'" alt="'+data.logoImgAlt+'">';
            html += '</div>';
            html += '<div class="row-xs">';
                html += '<div class="company--name f_med" inviewspy>'+data.name+'</div>';
            html += '</div>';
        html += '</div>';
        html += '<div class="section_objective">';
            html += '<div class="objective-txt" inviewspy>';
                html += '<p>';
                    html += 'บนความร่วมมือการดูแลจัดการ<br>';
                    html += 'สิ่งแวดล้อมอย่างยั่งยืน โดยมีเป้าหมาย<br>';
                    html += 'ลดก๊าซเรือนกระจกและบริหารจัดการ<br>';
                    html += 'สิ่งแวดล้อมอย่างสมดุล';
                html += '</p>';
                html += '<span class="hilight f_med">ภายใต้เป้าประสงค์ SDG Goals</span>';
            html += '</div>';
            html += '<div class="wrap_list_objective flex column">';
                html += '<div class="objective flex v-center" inviewspy="100">';
                    html += '<img class="lazy-img" data-src="' + image_url + 'theme/default/public/images/home/objective/objective-1.png" alt="obj 1">';
                    html += '<p class="obj_title f_med">รับรองแผนการบริโภคและการผลิตที่ยั่งยืน</p>';
                html += '</div>';
                html += '<div class="objective flex v-center" inviewspy="200">';
                    html += '<img class="lazy-img" data-src="' + image_url + 'theme/default/public/images/home/objective/objective-2.png" alt="obj 2">';
                    html += '<p class="obj_title f_med">ดำเนินมาตรการเร่งด่วนเพื่อรับมือการเปลี่ยนแปลงสภาพภูมิอากาศและผลกระทบ</p>';
                html += '</div>';
                html += '<div class="objective flex v-center" inviewspy="300">';
                    html += '<img class="lazy-img" data-src="' + image_url + 'theme/default/public/images/home/objective/objective-3.png" alt="obj 3">';
                    html += '<p class="obj_title f_med">สร้างพลังแห่งการเป็นหุ้นส่วนความร่วมมือระดับสากลต่อการพัฒนาที่ยั่งยืน</p>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        html += '<div class="btn-collapse flex center show-940" onClick="collapseDetail()">';
            html += '<span class="icon-arrow-down"></span>';
        html += '</div>';
    html += '</div>';
    return html;
}

function collapseDetail() {
    $('#content .company').toggleClass('collapsed','');
}

$(document).ready(function() {
    $('#content').append(compantDetailTmpl(dashboardData));
    inviewSpy();
    $('.lazy-img:not(.image-loaded)').lazy();
});