function newsHTML(newData, delay) {
    var html = '';
    html += '<div class="wrap-news" inviewspy="'+delay+'">'
        html += '<div class="news">';
            html += '<a href="'+newData.link+'" class="link_slide">';
                html += '<div class="wrap_img">';
                    html += '<img class="lazy-img" data-src="'+newData.image+'" alt="'+newData.imageAlt+'" />';
                html += '</div>';
                html += '<div class="wrap_detail">';
                    html += '<div class="date">'+newData.date+'</div>';
                    html += '<div class="news_title f_med">'+newData.title+'</div>';
                html += '</div>';
            html += '</a>';
        html += '</div>';
    html += '</div>';
    return html;
}
function menuNews() {
    var menues = $('.menu-cate-news').find('.cate-news');
    menues.el.forEach(el => {
        $(el).on('click', function() {
            menues.removeClass('active');
            $(this).addClass('active');
            getListWithCate($(this).data('cate'), 'replace');
            appendLoading();
        });
    })
}
function appendLoading() {
    $('.wrap-list-news').html('<div class="w_100 flex center" inviewspy="200"><div class="loadmore loading button f_med"><span class="button_label">ดูเพิ่ม</span></div></div>');
    inviewSpy();
}
function loadmoreListnews() {
    $('.wrap-list-news').append('<div class="w_100 flex center" inviewspy="200"><div class="loadmore button f_med"><span class="button_label">ดูเพิ่ม</span></div></div>');
    $('.loadmore').on('click', function() {
        $(this).loading();
        getListWithCate(currentCate, 'continue');
    })
}

function getListWithCate(cate, cmd = 'replace') {
    currentCate = cate;

    if(cmd == 'replace') {
        countNews = 0;
        total = 0;
        currentPage = 1;
    }

     var xhr = new XMLHttpRequest();
     var url = base_url + "news/get/" + currentCate + "/" + currentPage;
     xhr.open("GET", url, true);
     xhr.onload = function () {
         
        var usedata__News = JSON.parse(this.responseText);
        var htmlNews = '';
        new Promise((resolve) => {
            usedata__News.forEach( (news, count) => {
                htmlNews += newsHTML(news, count*100);
                if (count+1 == usedata__News.length) {
                    setTimeout(function() {
                        resolve(htmlNews);
                    }, 100);
                }
            });
        }).then((html) => {
            if(cmd == 'replace') {
                $('.wrap-list-news').html(html);
            } else {
                $('.wrap-list-news').append(html);
            }
            countNews += usedata__News.length;
            currentPage++;
            $('.loadmore').remove();
            if (usedata__News.length == 12) loadmoreListnews(); // TODO : usedata__News.length must be the total of news (with category) => assumed got 20 news 
            inviewSpy();
            $('.lazy-img:not(.img-loaded)').lazy();
        });

     };
     xhr.send();
}

let countNews = 0;
let total = 0;
let currentPage = 1;
let currentCate = '';
$(document).ready(function() {
    menuNews();
    getListWithCate('all');
});