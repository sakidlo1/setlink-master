function cardClimateHTML(data, cate) {
    var cateTemplateData = {
        'bear': {
            characterImg: image_url + 'theme/default/public/images/dashboard/bear.png',
            characterImgAlt: 'bear',
            cateBg: image_url + 'theme/default/public/images/dashboard/bear_bg.png',
            cateTitle: 'Care the Bear',
            cateDesc: 'ปรับพฤติกรรมองค์กร ลดก๊าซเรือนกระจกจากการจัดงาน Online/Onsite',
            year: '2018',
            delay: '450',
            registerLink: 'dashboard-register.html',
        },
        'whale': {
            characterImg: image_url + 'theme/default/public/images/dashboard/whale.png',
            characterImgAlt: 'whale',
            cateBg: image_url + 'theme/default/public/images/dashboard/whale_bg.png',
            cateTitle: 'Care the Whale',
            cateDesc: 'ลดโลกร้อน ด้วยการรวมพลัง ทำให้ “ขยะล่องหน” ไม่มีอะไรกลายเป็นขยะ',
            year: '2019',
            delay: '200',
            registerLink: '',
        },
        'wild': {
            characterImg: image_url + 'theme/default/public/images/dashboard/wild.png',
            characterImgAlt: 'wild',
            cateBg: image_url + 'theme/default/public/images/dashboard/wild_bg.png',
            cateTitle: 'Care the Wild',
            cateDesc: 'สร้างสมดุลของระบบนิเวศให้ประเทศด้วยการปลูกป่าดูดซับก๊าซเรือนกระจก',
            year: '2020',
            delay: '450',
            registerLink: '',
        }
    }

    var html = '';
    html += '<div class="wrap-card" inviewSpy="'+cateTemplateData[cate].delay+'">';
        html += '<div class="card-climate">';
            html += '<div class="disable-area ' + cate + ' ' + (data.climateMember === true ? "" : "disabled")+'">';
                html += '<div class="wrap-character">';
                    html += '<img class="IMG" src="'+cateTemplateData[cate].characterImg+'" alt="'+cateTemplateData[cate].characterImgAlt+'">';
                html += '</div>';
                html += '<div class="wrap-content">';
                    html += '<div class="top-box flex column h-justify">';
                        html += '<div>';
                            html += '<div class="title f_med">'+cateTemplateData[cate].cateTitle+'</div>';
                            html += '<div class="desc">'+cateTemplateData[cate].cateDesc+'</div>';
                        html += '</div>';
                        html += '<div class="since">- Since '+data.year+' -</div>';
                    html += '</div>';
                    html += '<div class="bottom-box">';
                        html += '<div class="wrap-cate-bg">';
                            html += '<img class="IMG" src="'+cateTemplateData[cate].cateBg+'" alt="'+cate +' background">';
                        html += '</div>';
                        if (data.climateMember === true) {
                            for (const key in data.stats) {
                                if (key == 'greenhouse_gas' &&  cate !== 'wild') {
                                    html += '<div class="GW-case greenhouse-gas flex v-center">';
                                        html += '<div class="icon-cloud"></div>';
                                        html += '<div class="flex column">';
                                            html += '<div class="f_reg txt-climate">ลดก๊าซเรือนกระจก</div>';
                                            html += '<div class="greenhouse-value f_med"><span class="run_number f_bold" data-no="'+data.stats.greenhouse_gas+'">0</span> kg.CO<sub>2</sub>e</div>';
                                        html += '</div>';
                                    html += '</div>';
                                }
                                if (key == 'greenhouse_absorb' &&  cate !== 'wild') {
                                    html += '<div class="GW-case greenhouse-absorb flex v-center">';
                                        html += '<div class="icon-cloud"></div>';
                                        html += '<div class="flex column">';
                                            html += '<div class="f_reg txt-climate">ลดก๊าซเรือนกระจก</div>';
                                            html += '<div class="greenhouse-value f_med"><span class="run_number f_bold" data-no="'+data.stats.greenhouse_absorb+'">0</span> Ton.CO<sub>2</sub>eq</div>';
                                        html += '</div>';
                                    html += '</div>';
                                }
                                if (key == 'planting_trees' &&  cate !== 'wild') {
                                    html += '<div class="GW-case planting-trees flex v-center">';
                                        html += '<div class="icon-tree"></div>';
                                        html += '<div class="flex column">';
                                            html += '<div class="f_reg txt-climate">เทียบเท่าปลูกต้นไม้</div>';
                                            html += '<div class="tree-value f_med"><span class="run_number f_bold" data-no="'+data.stats.planting_trees+'">0</span> ต้น</div>';
                                        html += '</div>';
                                    html += '</div>';
                                }
                                if (key == 'trees_planted' &&  cate !== 'wild') {
                                    html += '<div class="GW-case trees-planted flex v-center">';
                                        html += '<div class="icon-tree"></div>';
                                        html += '<div class="flex column">';
                                            html += '<div class="f_reg txt-climate">เทียบเท่าปลูกต้นไม้</div>';
                                            html += '<div class="tree-value f_med"><span class="run_number f_bold" data-no="'+data.stats.trees_planted+'">0</span> ต้น หรือ 100 ไร่</div>';
                                        html += '</div>';
                                    html += '</div>';
                                }
                                if (key == 'waste_manage') {
                                    html += '<div class="GW-case waste-manage flex v-center">';
                                        html += '<div class="icon-recycle-bin"></div>';
                                        html += '<div>';
                                            html += '<div class="f_reg txt-climate">ปริมาณขยะที่จัดการ </div>';
                                            html += '<div class="waste-value f_med"><span class="run_number f_bold" data-no="'+data.stats.waste_manage+'">0</span> kg</div>';
                                        html += '</div>';
                                    html += '</div>';
                                }
                            }

                            if (cate === 'wild') {
                                html += '<div class="register-box flex column h-justify">';
                                    html += '<div class="txt-register flex column center">';
                                        html += '<div class="title f_med">';
                                            html += 'ร่วมรวมพลังบวก';
                                        html += '</div>';
                                        html += '<div class="desc">';
                                            html += 'กู้วิกฤตโลกร้อน <br>กับ '+cateTemplateData[cate].cateTitle;
                                        html += '</div>';
                                    html += '</div>';
                                    html += '<a href="" data-href="'+cateTemplateData[cate].registerLink+'" data-cate="'+cate+'" class="register-btn button flex center">';
                                        html += '<div class="button_label f_med flex center">ติดตามผล</div>';
                                    html += '</a>';
                                html += '</div>';
                            }
                        } else {
                            html += '<div class="register-box flex column h-justify">';
                                html += '<div class="txt-register flex column center">';
                                    html += '<div class="title f_med">';
                                        html += 'ร่วมรวมพลังบวก';
                                    html += '</div>';
                                    html += '<div class="desc">';
                                        html += 'กู้วิกฤตโลกร้อน <br>กับ '+cateTemplateData[cate].cateTitle;
                                    html += '</div>';
                                html += '</div>';
                                html += '<a href="" data-href="'+cateTemplateData[cate].registerLink+'" data-cate="'+cate+'" class="register-btn button flex center">';
                                    html += '<div class="button_label f_med flex center">สมัครเข้าร่วมโครงการ</div>';
                                html += '</a>';
                            html += '</div>';
                        }
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}

function registerLoading(link, cate) {
    switch (cate) {
        case 'bear':
            window.location = base_url + 'member/register';
            break;
        case 'whale':
            window.location = base_url + 'member/register_carethewhale';
            break;
        case 'wild':
            sendRegister(cate);
            break;
    }
}

function sendRegister(cate) {
    return new Promise((resolve) => {
        // assumed waiting respond

        /*
        setTimeout(() => {
            resolve(
                window.location.href = 'thank-you-dashboard-'+cate+'.html'
            );
        }, 3000);
        */
        var myElement = document.getElementById('data-wild');
        if (myElement.classList.contains('data-wild')) {
            window.location = base_url + 'dashboard/carethewild';
        }else{
            window.location = base_url + 'care-the-wild#form_wild';
        }

    })
}
// Warning: this use for testing only
// TODO: remove this
function recieveRegisterSettingsFromQueryString() {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    let value = params.register;
    if (value) {
        dashboardData.climate_progress.bear.climateMember = value.split('')[0] == '1';
        dashboardData.climate_progress.whale.climateMember = value.split('')[1] == '1';
        dashboardData.climate_progress.wild.climateMember = value.split('')[2] == '1';
    }
}

$(document).ready(function() {
    recieveRegisterSettingsFromQueryString(); // TODO: remove this
    var registering = false;
    sidebarMenuBundle();
    companyDetailBundle();
    $('.detail .stats-value').el.forEach(de => {
        if ($(de).data('global') == 'gas') {
            $(de).attr('data-no', dashboardData.global_warming_progress.greenhouse_gas_reduced);
        } else {
            $(de).attr('data-no', dashboardData.global_warming_progress.tree_planted);
        }
        run_number(de);
    });
    for (const cate in dashboardData.climate_progress) {
        $('.company-detail').append(cardClimateHTML(dashboardData.climate_progress[cate], cate));
    }
    $('.wrap-card .register-btn').el.forEach( btn => {
        $(btn).on('click', function(e) {
            if (!registering) {
                registering = true;
                e.preventDefault();
                $(this).addClass('disabled');
                $(this).find('.button_label').loading();
                registerLoading($(this).data('href'), $(this).data('cate'));
            }
        });
    })
    inviewSpy();
    $('.company-detail .run_number').el.forEach(function(elem) {
        $(elem).inview( function(e) {
            run_number(e);
        });
    });
});