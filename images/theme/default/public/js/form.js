function CommonForm(elem) {
    this.main = $(elem);
    // this.customInputTypeEmail();    
    this.checkAnyType();
}

CommonForm.prototype.validateEmail = function (email) {
    var reg = new RegExp(/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/);
    return reg.test(email);
}
CommonForm.prototype.validNumber = function (value) {
    var numbers = /^([0-9]{9,12})+$/;
    return numbers.test(value);
}

CommonForm.prototype.checkValidForm = function (anchor,inpFocus) {

    if (typeof anchor === 'undefined') anchor = false;
    if (typeof inpFocus === 'undefined') inpFocus = false;
    var self = this;
    var $this = this.main;
    var _form = $this.serializeObject();
    var objValid = [];
    var checkValid = true;
    var tempKeyName = [];
    $this.find('.input-required').el.forEach(function (elem) {
        var input = elem; // DOM element.
        var value = input.value;
        var key = $(input).attr('name');
        // var type = $(input).attr('type');
        var type = input.type;
        var tagName = input.tagName.toLowerCase();
        var tmpGroup = [];    
        switch (tagName) {
            case 'input':
            case 'textarea':
                if (tmpGroup.indexOf(key) === -1 && (type === 'checkbox' || type === 'radio')) {
                    var inputElement = $this.find('[name="' + key + '"]:checked');
                    var emptyValue = inputElement.length <= 0;

                    var parent = $(input).parents('.wrap-input-checkbox');
                    if (type === 'radio') {
                        parent = $(input).parents('.wrap-input-radio') 
                    }

                    if (emptyValue) {
                        checkValid = false;
                        objValid.push(key);
                        $(input).addClass('invalid');
                        if (parent.length > 0) parent.addClass('invalid');
                    } else {
                        $(input).removeClass('invalid');
                        if (parent.length > 0) parent.removeClass('invalid');
                    }
                    tmpGroup.push(key);

                    // Event change invalid !
                    $('[name="' + key + '"]').eq(0).el.filter(function (Elem) {
                        var inputName = $(Elem).attr('name');
                        if (tempKeyName.indexOf(inputName) == -1) {
                            tempKeyName.push(inputName);
                            return inputName;
                        }
                    }).map(function(Item) {
                        var self = $(Item);
                        var name = self.attr('name');
                        $('[name=' + name + ']').on('change', function (Event) {
                            var elem = $('[name=' + name + ']:checked');
                            var allMathName = $('[name="' + name + '"]');
                            var parent = self.parents('.wrap-input-text');
                            var parentCheckBox = self.parents('.wrap-input-checkbox');                            
                            var emptyValue = elem.length <= 0;  
                            if (emptyValue) {
                                allMathName.addClass('invalid');
                                if (parent.length > 0) parent.addClass('invalid');                                
                                if (parentCheckBox.length > 0) parentCheckBox.addClass('invalid');                                
                            } else {
                                allMathName.removeClass('invalid');
                                if (parent.length > 0) parent.removeClass('invalid');
                                if (parentCheckBox.length > 0) parentCheckBox.removeClass('invalid');                                
                            }
                        });
                    });                    
                } else if (type == 'hidden') {
                    /**
                     * Extended plug-in dropdown.
                     * Remark Can delete or modify this condition.
                     */                                          
                    var emptyValue = $(input).val().length <= 0;
                    var parent = $(input).parents('.wrap-input-text');
                    var dropdown = parent.find('.simple-dropdown');             
                    if (dropdown.length > 0 && !dropdown.hasClass("disabled")) {                       
                        if (emptyValue) {
                            checkValid = false;
                            objValid.push(key);
                            $(input).addClass('invalid');
                            if (parent.length > 0) parent.addClass('invalid dropdown');
                            $(input).invalidText('', $.trim(value).length <= 0 ? 0 : 1);
                        } else {
                            $(input).removeClass('invalid');
                            if (parent.length > 0) parent.removeClass('invalid');
                        }
                        dropdown.each(function() {
                            var objDropdown = getRegisterDropdownData($(this).attr('id'));
                            $('#' + objDropdown.id).on('change', function () {                            
                                $(input).removeClass('invalid');
                                if (parent.length > 0) parent.removeClass('invalid dropdown');
                                $(input).invalidText('reset');
                            });
                        });
                    }
                    //Group validate checkbox
                    if(typeof($(input).data('group')) != "undefined" && $(input).data('group') === "checkbox"){                        
                        let getParents = $(input).parents('.checkbox-group-parent');
                        getParents.addClass("invalid");     
                        var inputGroups = getParents.find('input[name='+$(input).data("name")+']:checked'); 
                        if (inputGroups.length === 0){ 
                            checkValid = false;
                            $(input).addClass("input-required");
                            // return false;
                        } else {
                            getParents.removeClass("invalid");
                        }
                    }
                } else {        
                    var emailCase = key.indexOf('email') !== -1 && !self.validateEmail(value);
                    if ($.trim(value).length <= 0 || emailCase ) {
                        checkValid = false;
                        objValid.push(key);
                        if(!$(input).hasClass("notinit")){
                            $(input).invalidText('', $.trim(value).length <= 0 ? 0 : 1);
                        }else{
                            checkValid = true;
                        }
                    } else {
                        $(input).invalidText('reset');
                    }
                    // =====================
                    // Event keyup invalid !
                    // =====================
                    $(input).el.filter(function (Input) {
                        var self = $(Input);
                        var parent = self.parents('.wrap-input-text');
                        if (parent.length > 0 && parent.hasClass('invalid')) {
                            return self;
                        }
                    }).forEach(function (Item) {                        
                        $(Item).off('keyup').on('keyup', function (e) {
                            if (e.which == '0' || e.which == '13' || e.ctrlKey || e.altKey) {
                                return;
                            }
                            var self = $(this);
                            if (self.val().length > 0) {
                                if (self.hasClass('invalid')) {
                                    self.removeClass('invalid');
                                }
                                self.parents('.wrap-input-text').removeClass('invalid');
                                self.invalidText('reset');
                            } else {
                                self.addClass('invalid');
                                self.parents('.wrap-input-text').addClass('invalid');
                                self.invalidText();
                            }
                        });
                        let actions = {
                            activate: function(el) {
                                $(el).parents('.wrap-input-text').addClass('focus');
                            },
                            deactivate: function(el) {
                                var elem = $(el);
                                var parent = elem.parents('.wrap-input-text');
                                if (elem.val() === '') parent.removeClass('focus'); 
                                if (elem.val().length > 0) parent.removeClass('focus');
                            }
                        };
                        let showBubbleInvalid = function (el) {
                            if (el.value !== '' && !emailCase) actions.activate(el);
                            $(el).off('focus').on('focus', function(){actions.activate(this)});
                            $(el).off('blur').on('blur', function(){actions.deactivate(this)});
                        }
                        showBubbleInvalid(Item);
                    });
                }
            break;
        } // end switch.
    }); // end each.
    if (!checkValid && anchor === true) {
        setTimeout(function() {
            var validInput = $this.find('.invalid').eq(0);
            if(validInput){
                if(validInput.find('.input-required:not([type=hidden]').length > 0){
                    var thatInputHeight = validInput.outerHeight();
                    var DefaultPopupHeight = document.getElementsByClassName('header').clientHeight; // header height
                    var customHeight = 0; // custom padding
                    var anchorOffset = thatInputHeight + DefaultPopupHeight + customHeight;
                    validInput.goToAnchor(400,anchorOffset).then(function(){
                        if(inpFocus === true){
                            validInput.find('.input-required').focus();
                        }
                    });            
                }
                else{
                    validInput.goToAnchor(400,100).then(function(){
                        validInput.find('.input-required').focus();
                        var validElm = $this.find('.invalid').el[0];
                        $(validElm).addClass('focus');
                    });
                }
            }
        },300);
        
        // else{            
        //     if(validCheckBox){
        //         validCheckBox.goToAnchor(400,10);
        //     }
        // }
    }
    if (!checkValid && inpFocus === true) {
        setTimeout(function() {
            var validInput = $this.find('.invalid').eq(0);
            if(validInput){
                if(validInput.find('.input-required:not([type=hidden]').length > 0){
                    validInput.find('.input-required').focus();          
                }else{
                    validInput.find('.input-required').focus();
                    var validElm = $this.find('.invalid').el[0];
                    $(validElm).addClass('focus');
                }
            }
        },300);
    }

    var result = {
        valid: checkValid,
        form: _form,
        list: objValid
    }

    return result;
}
CommonForm.prototype.customInputTypeEmail = function () {
    this.main.find('.input-type-text').map(function (input) {
        var type = $(input);
        if (type.attr('type') == 'email') {
            type.on('keyup.atEmail', function () {
                var self = $(this);
                if (self.val().length > 0) {
                    self.addClass('email-have-data');
                }
                else {
                    self.removeClass('email-have-data');
                }
            });
        }
    });
}
CommonForm.prototype.clear = function () {
    this.main.get(0).reset();
    this.main.find('[type="checkbox"]').each(function(elem) {
        elem.checked = false;
    });
    this.main.find('.simple-dropdown').each(function(elem) {
        var name = $(elem).find('input[type="hidden"]').attr('name');
        if (name) {
            getSimpleDropdown(name).clear();
        }
    });
    // radio
    this.main.find('[type="radio"]').each(function (Elem) {
        Elem.checked = false;
    });
    // email
    this.main.find('[type="email"]').each(function(Elem) {
        $(Elem).removeClass('email-have-data');
    });
    // checkbox email
    this.main.find('.box-check-box-email').each(function (Elem) {
        $(Elem).removeClass('pass');
    });
}

$.fn('invalidText',function(txt, index) {
    if (typeof index === 'undefined') index = 0;
    if (typeof txt === 'undefined') txt = '';
    var _parent = this.parents('.wrap-input-text'); // text
    var _parentChk = this.parents('.wrap-input-checkbox'); // text
    if (txt === 'reset') {
        _parent.removeClass('invalid');
        _parent.find('.valid-message').remove();
        if(_parentChk.length > 0){
            _parentChk.removeClass('invalid');
        }
    } else {
        var create = function (_txt) {
            _parent.find('.valid-message').remove();
            if (_parent.find('.valid-message').length <= 0) {
                _parent.append('<span class="valid-message"><span class="txt-valid">' + _txt + '</span></span>');
            } else {
                _parent.find('.valid-message').text(_txt);
            }
        };
        _parent.addClass('invalid');
        var errorText = this.data('error');
        if ($.trim(txt).length > 0) {
            create(txt);
        } else {
            if (errorText) {
                var tmp = errorText.split(',');
                create(tmp[index]);
            }
        }
    }
    return this;
});
CommonForm.prototype.checkAnyType = function () {
    //type text
    this.main.find('.input-type-text').el.forEach(function (input) {
        let this_ = $(input);
        var typeCheck = this_.data('check');
        if (typeCheck === 'numeric') {
            numericOnly(input);            
        }
        if (typeof(input.type) != "undefined" && input.type === 'password') {
            var iconTarget = 'ic-eye';
            let icEye = this_.parents(".wrap-input-text").find('.' + iconTarget);
            icEye.el.forEach(function(ic) {
                $(ic).on('click',function(){
                    if (!this.classList.contains('icon-eye')){
                        $(ic).addClass("icon-eye");
                        $(ic).removeClass("icon-eye-close");
                    } else {
                        $(ic).removeClass("icon-eye");
                        $(ic).addClass("icon-eye-close");
                    }
                    if (input.type === "password") {
                        input.type = "text";
                    } else {
                        input.type = "password";
                    }
                });
            });
        }
        if (typeof(input.type) != "text" && input.type === 'text') {
            $(input).on('keyup', function() {
                if ($(input).val().length > 0) {
                    $(input).addClass('have-data');
                } else {
                    $(input).removeClass('have-data');
                }
            })
        }
        if (typeof(input.type) != "undefined" && input.type === 'email' || $(input).attr('inputmode') == 'email') {
            let html = '<div class="box-check-box-email"><div class="preview-check draw"></div></div>';
            this_.parents(".wrap-input-text").append(html);
            this_.on('keyup', function(event) {
                var getTextInput = this_.val();
                var findIcon = this_.parents('.wrap-input-text').find('.box-check-box-email');                
                if (getTextInput.length > 5) {
                    if (!checkFormatEmail(getTextInput)) {
                        findIcon.addClass('pass');
                    }else{
                        //no
                        findIcon.removeClass('pass');
                    }
                }else{
                    findIcon.removeClass('pass');
                }
            });	

        }
    });
    
    //type check
    this.main.find('.input-type-checkbox').el.forEach(function (input) {
        let this_ = $(input);
        var type = this_.data('checkbox');
        if (typeof(type) != "undefined" && type === 'checkbox-other') {
            this_.on('change.other', function () {
                let parentsMain = this_.parents(".col-form").not(".-other");
                parentsMain.find(".-other-inp").removeClass("invalid");
                let parentsInpOther = parentsMain.find(".-other-inp input");
                if(parentsMain.hasClass("col-other")){
                    parentsMain.toggleClass("active-other-inp");
                    if(this_.get(0).checked){
                        parentsInpOther.addClass("input-required");
                    }else{
                        parentsInpOther.removeClass("input-required");
                    }
                }
            });  
        }
    });
    //type radio
    this.main.find('.input-type-radio-other').el.forEach(function (input) {
        let this_ = $(input);
        let parentsMain = this_.parents(".parents-radio");
        this_.on('change.other', function () {
            var type = this_.data('checkbox');
            let boxMainCol = parentsMain.find(".col-form.col-other").not(".-other");
            boxMainCol.find(".-other-inp").removeClass("invalid");
            let parentsInpOther = boxMainCol.find(".-other-inp input");
            if (typeof(type) != "undefined" && type === 'radio-other') {
                boxMainCol.addClass("active-other-inp");
                parentsInpOther.addClass("input-required");
            }else{
                boxMainCol.removeClass("active-other-inp");
                parentsInpOther.removeClass("input-required");
            }
        });
    });
}
CommonForm.prototype.validIdCardOrPassport = function (id) {
    id = id.replace(/\D/g,'');
    var sum = 0;
    var i = 0;
    for (;i < 12; i++) {
        sum += parseFloat(id.charAt(i))*(13-i); 
    }
    if ((11-sum%11)%10!=parseFloat(id.charAt(12))) return false;
    return true;
}
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    });
  }
function numericOnly(textbox, inputFilter) {
    setInputFilter(textbox, function(value) {
        return /^-?\d*$/.test(value); 
    });
}
function phoneFormat(input,patten) {
    if(typeof(patten) === "undefined") patten = ' ';
    // Strip all characters from the input except digits
    input = input.replace(/\D/g,'');

    // Trim the remaining input to ten characters, to preserve phone number format
    input = input.substring(0,10);

    // Based upon the length of the string, we add formatting as necessary
    var size = input.length;
    if(size == 0){
        input = input;
    }else if(size <= 3){
        input = input;
    }else if(size <= 6){
        input = input.substring(0,3)+patten+input.substring(3,6);
    }else{
        input = input.substring(0,3)+patten+input.substring(3,6)+patten+input.substring(6,10);
    }
    return input;
}
function idCardFormat(input,patten) {
    if(typeof(patten) === "undefined") patten = '-';
    // Strip all characters from the input except digits
    // input = input.replace(/\D/g,'');
    card_input = input.replace(/\D/g,'');

    // Trim the remaining input to ten characters, to preserve phone number format
    card_input = input.substring(0,18);
    // Based upon the length of the string, we add formatting as necessary
    var size = card_input.length;
    // 1-3404-00113-85-1
    // 0-1234 56789 1011 12
    if(size == 0){
        input = card_input;
    }else if(size < 2){
        input = card_input;
    }else if(size < 6){
        input = card_input.substring(0,1)+patten+card_input.substring(1,5);
    // }else if(size < 8){
    //     input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,7);
    }else if(size < 11){
        // input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,7)+patten+card_input.substring(7,10);
        input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,10);
    }else if(size < 13){
        // input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,7)+patten+card_input.substring(7,10)+patten+card_input.substring(10,12);
        input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,10)+patten+card_input.substring(10,12);
    }else{
        // input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,7)+patten+card_input.substring(7,10)+patten+card_input.substring(10,12)+patten+card_input.substring(12,13);
        input = card_input.substring(0,1)+patten+card_input.substring(1,5)+patten+card_input.substring(5,10)+patten+card_input.substring(10,12)+patten+card_input.substring(12,13);
    }
    return input;
}
function idCardBackFormat(input,patten) {
    if(typeof(patten) === "undefined") patten = '-';
    
    card_input = input.replace(/[^a-zA-Z0-9]/g,'');

    // Trim the remaining input to ten characters, to preserve phone number format
    card_input = input.substring(0,14);
    
    // Based upon the length of the string, we add formatting as necessary
    var size = card_input.length;
    if(size == 0){
        input = card_input;
    }else if(size <= 3){
        input = card_input;
    }else if(size <= 10){
        input = card_input.substring(0,3)+patten+card_input.substring(3,10);
    }else{
        input = card_input.substring(0,3)+patten+card_input.substring(3,10)+patten+card_input.substring(10,12);
    }

    return input;
}
function numberPressed(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 36 || charCode > 40)){
        return false;
    }
    return true;
}
function numberPressedAndDot(evt){
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode === 46) {
        return true;
    }
    return numberPressed(evt);
}
function numberMobile(e){
    e.target.value = e.target.value.replace(/[^\d]/g,'');
    return false;
}
function numberAndChar(evt,value){
    setInputFilter(evt, function(value) {
        return /^[a-zA-Z0-9]+(-[a-zA-Z0-9]+)*$/.test(value); 
    });
}
function inputText(value){
    return /[^+_()*&^]+$/.test(value); 
}
function is_numeridCard(str){
    return /^\d+$/.test(str);
}
function is_charidCard(str){
    return /^[a-zA-Z]+$/.test(str);
}
function is_formEdit(onchange = false , oncheck = false){
    // This default onbeforeunload event
    window.onbeforeunload = function (e) {
        if (onchange && !oncheck) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
}
function checkValidCustom(inp,idx,anchor, firstValid) {
    if (typeof anchor === 'undefined') anchor = true;
    if (typeof firstValid === 'undefined') firstValid = false;
    let parentInp = inp.parents(".wrap-input-text");
    inp.invalidText('',idx);
    if(parentInp.hasClass('invalid')){
        inp.on('focus', function(){                
            parentInp.addClass('focus');
        });
    }else{
        parentInp.removeClass('focus');
    }
    inp.on('blur', function(){parentInp.removeClass('focus');});   
    
    /** 
     * This code belows will alaway find first invalid class 
     * Method should find current input invlid.
     **/
    // let validInput = inp.parents('.common-form').find('.invalid').eq(0);

    // Fix !
    let validInput = parentInp;
    if (firstValid) {
        validInput = inp.parents('.common-form').find('.invalid').eq(0);
        inp.parents('.common-form').find('.invalid').removeClass('focus');
    }

    validInput.removeClass('focus');
    if (anchor) {
        validInput.goToAnchor(400,100).then(function(){
            // validInput.find('.input-required').focus();
            validInput.addClass('focus');
        });       
    } else {
        validInput.find('.input-required').focus();
        validInput.addClass('focus');
    }
}
function getRegisterDropdownData(name) {
    var snipId = 'simple_dropdown_';
    var name = name.replace(snipId,'');
    var dropdown = null;
    if (typeof getSimpleDropdown == 'function') {
        dropdown = getSimpleDropdown(name);
    }
    if (dropdown === null && $('#' + snipId + name).length > 0) {
        dropdown = new SimpleDropdown(document.getElementById(snipId + name));
    }
    return dropdown;
}
function checkValidCustomSpecial(inp,idx,anchor, firstValid) {
    if (typeof anchor === 'undefined') anchor = true;
    if (typeof firstValid === 'undefined') firstValid = false;
    let parentInp = inp.parents(".wrap-input-text");
    inp.invalidText('',idx);
    if(parentInp.hasClass('invalid')){
        inp.on('focus', function(){                
            parentInp.addClass('focus');
        });
    }else{
        parentInp.removeClass('focus');
    }
    inp.on('blur', function(){parentInp.removeClass('focus');});   
    
    /** 
     * This code belows will alaway find first invalid class 
     * Method should find current input invlid.
     **/
    // let validInput = inp.parents('.common-form').find('.invalid').eq(0);

    // Fix !
    let validInput = parentInp;
    if (firstValid) {
        validInput = inp.parents('.common-form').find('.invalid').eq(0);
        inp.parents('.common-form').find('.invalid').removeClass('focus');
    }

    validInput.removeClass('focus');
    if (anchor) {
        validInput.goToAnchor(400,100).then(function(){
            // validInput.find('.input-required').focus();
            validInput.addClass('focus');
        });       
    } else {
        validInput.find('.input-required').focus();
        validInput.addClass('focus');
    }
}
function checkFormatEmail(_str) {
    return !validateEmail(_str);
}
function validateEmail(email) {
    var reg = new RegExp(/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/);
    return reg.test(email);
}