var slide_swiper_organize = '';

// get slide on page
function slideWithImage(imageArr) {
    var html = '';
    html += '<div class="swiper-slide multi-pic-in-organize">';
    imageArr.forEach((logo, index) => {
        html += image(logo, 'logo-' + index);
    });
    html += '</div>';
    return html;
}

function image(img, imgAlt, cls='') {
    return '<img data-src="' + img + '" alt="' + imgAlt + '" class="lazy-img logo '+cls+'" style="width: calc(' + colWidth + '% - 8.5px);" inviewspy>';
}

// check media screen for check col
function checkMediaScreen() {
    return window.innerWidth < 767 ? 'mobile' : 'desktop';
}

function calSlidePage(row, cateArr) {
    let arrImg = [[]];
    let w = $('.main-slide-wrapper').width();
    let _col = 0;
    let logo_size = checkMediaScreen() == 'desktop' ? 96: 79;
    let min_col = checkMediaScreen() == 'desktop' ? 4 : 4;
    let max_col = checkMediaScreen() == 'desktop' ? 8 : 6;

    if (Math.ceil(w / logo_size) < min_col) {
        _col = min_col;
        colWidth = 100 / min_col;
    } else if (Math.ceil(w / logo_size) > max_col) {
        _col = max_col;
        colWidth = 100 / max_col;
    } else {
        _col = Math.ceil(w / logo_size);
        if (100 / _col > 25) colWidth = 25;
        else colWidth = 100 / _col;
    }
    colPerSlide = _col;
    var counter = 0;
    var addedImg = 0;
    cateArr.forEach((img) => {
        if (arrImg[counter] == undefined) arrImg[counter] = [];
        arrImg[counter].push(img);
        addedImg++;

        if (addedImg == (_col * row)) {
            counter++;
            addedImg = 0;
        }
    });
    arrImgOrgani = arrImg;
}


// check col for slide page
function mediaInitSlide(cate) {
    if(cate == "bear"){
        if (checkMediaScreen() == 'mobile') {
            calSlidePage(6,bear);
        } else {
            calSlidePage(4,bear);
        }
    }else if(cate == 'elephant'){
        if (checkMediaScreen() == 'mobile') {
            calSlidePage(6,elephant);
        } else {
            calSlidePage(4,elephant);
        }
    }else if(cate == 'wal'){
        if (checkMediaScreen() == 'mobile') {
            calSlidePage(6,whale);
        } else {
            calSlidePage(4,whale);
        }
    }
    appendSlide();
}


// get slide
function appendSlide() {
    var slideHTML = '';
    arrImgOrgani.forEach((slide, index) => {
        slideHTML += slideWithImage(slide);
    });
    $('.main-slide-wrapper .swiper-wrapper').html(slideHTML);
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    if (slide_swiper_organize != '' && slide_swiper_organize.destroyed && slide_swiper_organize.destroyed !== true) slide_swiper_organize.destroy();
    slide_swiper_organize = new Swiper('.main-slide-wrapper', { 
        loop: true, 
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        }, 
        speed: 1000, 
        allowTouchMove: true, 
        spaceBetween: 20,
        pagination: {
            el: '.swiper-pagination',
            clickable: true, 
            bulletClass: 'slide_bullet', 
            bulletActiveClass: 'active'
        }
    });
    slide_swiper_organize.on('slideChange', function() {
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
    });
}
function scroll_filter(){
    $('.items-in-modal').on('scroll', function() {
        let current_scroll = Math.ceil($(this).get(0).scrollTop + $(this).height());
        let all_scroll = $(this).get(0).scrollHeight;
        if((current_scroll == all_scroll) || (current_scroll+1 == all_scroll) || (current_scroll-1 == all_scroll)){
            $('.items-wrapper').addClass("dis-blur-on-modal")
        }else{
            $('.items-wrapper').removeClass("dis-blur-on-modal")
        }
    });
    $('.items-in-modal').trigger('scroll')
}

// get slide section
function main_slide(opt = {}) {
    var option = {
        side: 'left',
        cate: 'all',
        icon: ''
    };
    var useOption = $.extend(option, opt);
    var side_order_1 = '';
    var side_order_2 = '';
    if (useOption.side != 'left') {
        side_order_1 = 'order-1';
        side_order_2 = 'order-2';
    }
    var html = '';
    html += '<div class="main-organize '+'bg-'+useOption.cate+' '+' ">'
        html += '<div class="container">'
            html += '<div class="left-content ' +side_order_1+ ' ">'
                html += '<span class="title-organize"> องค์กร'
                    html += '<br> ที่เข้าร่วมโครงการ'
                html += '</span>'
                /*
                html += '<button class="btn-see-all button" id="get-see-all">'
                    html += '<span> ดูทั้งหมด'
                    html += '</span>'
                html += '</button>'
                */
                html += '<br />'
                html += '<br />'
                html += '<br />'
                html += '<br />'
                if (useOption.icon != '') {
                    html += '<img class="img-left-content" src="' + image_url + 'theme/default/public/images/bg-sec-slide/'+useOption.icon+'.png">'
                }
            html += '</div>'
            html += '<div class="swiper main-slide-wrapper '+side_order_2+'">'
                html += '<div class="swiper-wrapper organize">'
                html += '</div>'
                html += '<div class ="swiper-pagination">'
                html += '</div>'
            html += '</div>'
        html += '</div>'
    html += '</div>'
    return html;
}

function append_modal(arr_cate, cate=" ") {
    var html = '';
    html += '<div class="main-modal-care ">'
        html += '<div class="modal-content-relative">'
            html += '<div class="modal-content-absolute '+ cate +'">'
                html += '<div class="relative-button">'
                    html += '<button class="btn-close-modal">'
                        html += '<span class="icon-cross">'
                        html += '</span>'
                    html += '</button>'
                html += '</div>'
                html += '<div class="items-wrapper '+ cate +'">'
                    html += '<span class="modal-title"> องค์กร<br class="d-md-none">ที่เข้าร่วมโครงการ '
                    html += '</span>'
                    html += '<div class="items-in-modal">'
                        arr_cate.forEach((bear_img) => {
                            html += '<img class="img-in-modal" src = "'+ bear_img + '"/>'
                        })
                    html += '</div>'
                html += '</div>'
            html += '</div>'
        html += '</div>'
    html += '</div>'
    return html;
}
var arrImgOrgani = [];
var arr_img_modal = [];
