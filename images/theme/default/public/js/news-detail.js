$(document).ready(function () {
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
    });
    setNews();
    eventCopy();
    inviewSpy(0);
});
function eventCopy() {
    $('.media.icon-copy').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if ($(this).hasClass('copied')) return;
        $(this).addClass('copied');
        setTimeout(() => {
            $(this).removeClass('copied');
        }, 2000);
        navigator.clipboard.writeText($(this).attr('href'));
    })
}
function fn_set_data(data, key) {
    function removeWhenEmpty(data, fn) {
        if (data) fn();
    }
    switch (key) {
        case 'title':
            removeWhenEmpty(data.title, function () {
                $('#news-title').html(data.title);
                $('#news-title').removeAttr('data-remove');
            });
            break;
        case 'date':
            removeWhenEmpty(data.date, function () {
                $('#date').html(data.date);
                $('#date').removeAttr('data-remove');
            });
            break;
        case 'banner_img':
            removeWhenEmpty(data.banner_img, function () {
                $('#banner-img').attr('src', data.banner_img);
                $('#banner-img').attr('alt', data.banner_img_alt);
                $('#banner').removeAttr('data-remove');
            });
            break;
        case 'detail':
            removeWhenEmpty(data.detail, function () {
                $('#news-content').html(data.detail);
                $('#news-content').removeAttr('data-remove');
            });
            break;
        case 'gallery':
            removeWhenEmpty(data.gallery, function () {
                var slide = '';
                slide += '<div class="swiper">';
                slide += '<div class="swiper-wrapper">';
                data.gallery.forEach((gal, i) => {
                    slide += slideTMPL(gal);
                });
                slide += '</div>';
                slide += '<div class="back-btn icon-arrow-left flex center"></div>';
                slide += '<div class="next-btn icon-arrow-right flex center"></div>';
                slide += '</div>';
                $('#gallery').html(slide);
                initSlide();
                $('#gallery').removeAttr('data-remove');
            });
            break;
        case 'tags':
            removeWhenEmpty(data.tags, function () {
                data.tags.forEach(tag => {
                    $('#tags').append('<span class="tag">#' + tag + '</span>')
                });
                $('#tags').removeAttr('data-remove');
            });
            break;
    }
}

function setNews() {

    console.log(newsWysiwyg);

    var dataNews = newsWysiwyg;
    for (const key in dataNews) {
        fn_set_data(dataNews, key);
    }
    $('#share').append('<span id="share-fb" href="' + window.location.href + '" class="media icon-facebook"></span>');
    $('#share').append('<span id="share-tw" href="' + window.location.href + '" class="media icon-twitter"></span>');
    $('#share').append('<span id="share-line" href="' + window.location.href + '" class="media icon-line"></span>');
    $('#share').append('<span href="' + window.location.href + '" class="media icon-copy"></span>');
    objShare = Share.init();

    $('[data-remove="T"]').remove();
}
function slideTMPL(data) {
    var html = '';
    html += '<div class="swiper-slide">';
    html += '<img src="' + data.img + '" alt="' + data.img_alt + '"/>';
    html += '<span class="fullscreen icon-fullscreen"></span>';
    html += '</div>';
    return html;
}
function initSlide() {
    var slide = new Swiper('#gallery .swiper', {
        slidesPerView: 'auto',
        navigation: {
            nextEl: '#gallery .next-btn',
            prevEl: '#gallery .back-btn',
        },
    });
    $('#gallery .swiper-slide').on('click', function () {
        $('body').append(fullscreenImg($(this).find('img').attr('src'), $(this).find('img').attr('alt')));
        $('.popup_fullscreen .close_btn').on('click', function () {
            $(this).parents('.popup_fullscreen').remove();
        });
    });
}
function fullscreenImg(src, alt) {
    var html = '';
    html += '<div class="popup_fullscreen flex center">';
    html += '<div class="wrap_close">';
    html += '<div class="close_btn XL">';
    html += '<span class="icon icon-cross"></span>';
    html += '</div>';
    html += '</div>';
    html += '<div class="popup_box flex center">';
    html += '<div class="wrap-img">';
    html += '<img src="' + src + '" alt="' + alt + '">';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    return html;
}

// -=-=-=-=-=-=-=- share =-=-=-=-=-=-=-=-=-=
function Share() {
    const main = $('#share');
    if (!main.length)
        return;

    this.main = main;
    this.settings = {
        screen: {
            width: 500,
            height: 550
        },
        default: {
            title: '',
            desc: '',
            enableCampaign: true, // false
            haveOriginQueryString: false
        },
        social: {
            hashTag: ''
        },
    }
    this.getMetaOG();
    this.getQueryString();
    this.initEvent();
}
Share.prototype.getMetaOG = function () {
    let head = document.getElementsByTagName('head')[0];
    if (head) {
        try {
            this.settings.default.title = head.querySelector('[property="og:title"]').getAttribute('content');
            this.settings.default.desc = head.querySelector('[property="og:description"]').getAttribute('content');
        } catch (_error) {
            console.log(_error)
        }
    }
};
Share.prototype.getQueryString = function () {
    if (!parseUri)
        return;

    const URI = parseUri(window.location.href);
    if (URI.queryKey && Object.keys(URI.queryKey).length > 0) {
        this.settings.default.haveOriginQueryString = true;
    }
}
Share.prototype.initEvent = function () {
    this.sharedSubmit();
};
Share.prototype.setURICampaign = function (options, appendToURL) {
    if (typeof options !== 'object')
        options = {};
    if (typeof appendToURL !== 'boolean')
        appendToURL = false;

    if (!appendToURL)
        return;

    let queryString = [];
    let defaultOptions = {
        campaign_source: 'website',
        campaign_medium: 'detail',
        utm_campaign: 'title-campaign',
        utm_content: 'social-shared'
    }
    $.extend(defaultOptions, options);

    for (const key in defaultOptions) {
        queryString.push(key + '=' + defaultOptions[key]);
    }
    let setQueryObject = '&';
    if (!this.settings.default.haveOriginQueryString) {
        setQueryObject = '?';
    }
    return setQueryObject + queryString.join('&');
};
Share.prototype.setPopupProperty = function (options) {
    let queryString = [];
    let defaultOptions = {
        width: this.settings.screen.width,
        height: this.settings.screen.height,
        top: 100,
        left: 100,
        location: 'no',
        menubar: 'no',
        scrollbars: 'yes',
        resize: 'yes',
        status: 'no',
        toolbar: 'no'
    }
    $.extend(defaultOptions, options);
    var setCenterScreen = function (objScreen) {
        if (typeof objScreen === 'undefined' || typeof objScreen !== 'object') {
            objScreen = {};
        }

        return {
            width: (screen.width - objScreen.width) / 2,
            height: (screen.height - objScreen.height) / 2
        }
    }
    let setScreenCenter = setCenterScreen(defaultOptions);
    $.extend(defaultOptions, {
        top: setScreenCenter.height,
        left: setScreenCenter.width
    });
    for (const key in defaultOptions) {
        queryString.push(key + '=' + defaultOptions[key]);
    }
    return queryString.join(',');
};
Share.prototype.popupFacebook = function (URL, popupOptions) {
    if (typeof URL === 'undefined') return;
    if (typeof popupOptions !== 'object') {
        popupOptions = {};
    }
    let options = {
        width: this.settings.screen.width,
        height: this.settings.screen.height
    };
    $.extend(options, popupOptions);
    const URI = URL + this.setURICampaign({}, this.settings.default.enableCampaign);
    window.open(
        'https://www.facebook.com/sharer.php' +
        '?u=' + encodeURIComponent(URI), 'share_fb', this.setPopupProperty(options))
};
Share.prototype.popupLine = function (URL, popupOptions) {
    if (typeof URL === 'undefined') return;
    if (typeof popupOptions !== 'object') {
        popupOptions = {};
    }
    let options = {
        width: this.settings.screen.width,
        height: this.settings.screen.height
    };
    $.extend(options, popupOptions);
    const URI = URL + this.setURICampaign({}, this.settings.default.enableCampaign);
    window.open(
        'https://social-plugins.line.me/lineit/share' +
        '?url=' + encodeURIComponent(URI),
        'share_line',
        this.setPopupProperty(options)
    );
};
Share.prototype.popupTwitter = function (URL, popupOptions) {
    if (typeof URL === 'undefined') return;
    if (typeof popupOptions !== 'object') {
        popupOptions = {};
    }
    let options = {
        title: this.settings.default.title,
        hashTag: this.settings.default.hashTag,
        width: this.settings.screen.width,
        height: this.settings.screen.height
    };
    $.extend(options, popupOptions);
    let setURIHashTag = '';
    if (options.hashTag) {
        setURIHashTag = '&hashtags=' + options.hashTag;
    }
    const URI = URL + this.setURICampaign({}, this.settings.default.enableCampaign);
    window.open(
        'https://twitter.com/intent/tweet' +
        '?text=' + encodeURIComponent(options.title) +
        '&url=' + encodeURIComponent(URI) + setURIHashTag,
        '',
        this.setPopupProperty(options)
    );
};
Share.prototype.popupEmail = function (URL, popupOptions) {
    if (typeof URL === 'undefined') return;
    if (typeof popupOptions !== 'object') {
        popupOptions = {};
    }
    let options = {
        title: this.settings.default.title,
        desc: this.settings.default.desc
    };
    $.extend(options, popupOptions);
    const URI =
        'mailto:' +
        '?subject=' + encodeURIComponent(options.title) +
        '&body=' + encodeURIComponent(options.desc + '\n\n' + URL + this.setURICampaign({}, this.settings.default.enableCampaign))
    window.location.href = URI;
};
Share.prototype.openSocial = function (social, options) {
    if (typeof social === 'undefined' || !social)
        return;
    if (typeof options !== 'object') {
        options = {
            url: window.location.href
        };
    }
    if (options.url && options.url === 'javascript:void(0)' || !options.url) {
        options.url = window.location.href;
    }
    var cutAfterLink = function (element, index) {
        let target = element;
        element.split('/')
        if (element[element.length - 1] === index) {
            target = target.replace('/' + index, '');
        }
        return target;
    }
    switch (social.toLowerCase()) {
        case 'facebook':
            this.popupFacebook(cutAfterLink(options.url, 'fb'));
            break;
        case 'line':
            this.popupLine(cutAfterLink(options.url, 'line'));
            break;
        case 'twitter':
            this.popupTwitter(cutAfterLink(options.url, 'twitter'), options);
            break;
        case 'email':
            this.popupEmail(cutAfterLink(options.url, 'email'), options);
            break;

        default: break;
    }
};
Share.prototype.sharedSubmit = function () {
    const that = this;
    $('#share-fb').off('click').on('click', function () {
        var link = $(this).data('url');
        that.openSocial('facebook', { url: link });
    });
    $('#share-tw').off('click').on('click', function () {
        var link = $(this).data('url');
        that.openSocial('twitter', { url: link });
    });
    $('#share-line').off('click').on('click', function () {
        var link = $(this).data('url');
        that.openSocial('line', { url: link });
    });
    $('#share-mail').off('click').on('click', function () {
        var link = $(this).data('url');
        that.openSocial('email', { url: link });
    });
};
Share.init = function () {
    return new Share();
}

/**
 * parseUri 1.2.2
 * (c) Steven Levithan <stevenlevithan.com>
 * MIT License
 */
function parseUri(str) {
    var o = parseUri.options,
        m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
        name: "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};