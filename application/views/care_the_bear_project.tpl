{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
	<link rel="stylesheet" href="{$image_url}theme/default/public/css/care-the-bear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/care-6.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/contact-section.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/menu-bear.css">
{/block}
{block name=js}
	<script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script>
    const bear = [
        {foreach $company_bear as $company_bear_item}
            '{$company_bear_item.logo}',
        {/foreach}
    ];
    const dataOrganization = [
        ...bear
    ];
    </script>
    <script src="{$image_url}theme/default/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/public/js/contact-section.js"></script>
{/block}
{block name=body}
	<div class="menu-bear" inviewspy>
        <div class="wrap-menu">
			<a class="menu-link f_med" href="{$base_url}care-the-bear">Care the Bear</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-about">What’s Care the Bear</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-activity">Activities & Event</a>
			<span class="f_med">Climate Action Project</span>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-article">Articles</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-news">News</a>
        </div>
    </div>
	<div id="banner_html"></div>
	    <div class="banner-care-the-bear">
	        <div class="content-banner-left">
	            <img src="{$image_url}theme/default/public/images/care-the-bear/banner/logo-bear.png" alt="" inviewspy>
	            <span class="title-banner" inviewspy="100">Project<br class="d-xl-block"> Change the Climate Change</span>
	            <span class="desc-banner" inviewspy="200">ตลาดหลักทรัพย์ฯ สนับสนุนให้บริษัทจดทะเบียน<br class="d-xl-block">
	                และองค์กรที่สนใจร่วมปรับพฤติกรรมลดการปล่อย<br class="d-xl-block">
	                ก๊าซเรือนกระจกจากการจัดงานหรือทุกกิจกรรม<br class="d-xl-block">
	                ในรูปแบบ Online และ Onsite เช่น การประชุมผู้ถือหุ้น<br class="d-xl-block"> 
	                การประชุมผู้ถือหุ้นแบบ e-AGM การจัดงานอีเว้นต์ต่างๆ<br class="d-xl-block">
	                การจัดประชุมออนไลน์ การจัดกิจกรรมท่องเที่ยว<br class="d-xl-block">
	                การจัดงานมอบรางวัล การจัดงาน CSR เป็นต้น </span>
	        </div>
	        <div class="img-in-banner d-lg-block" inviewspy="200">
	            <img src="{$image_url}theme/default/public/images/care-the-bear/banner/img-in-desktop-banner.png" alt="">
	        </div>
	        <div class="img-in-banner d-lg-none" inviewspy="400">
	            <img src="{$image_url}theme/default/public/images/care-the-bear/banner/img-in-banner.png" alt="">
	        </div>
	    </div>
	    <div class="main-sec-2">
	        <div class="wrap_care">
	            <div class="max_width_care flex center hide-xs">
	                <div class="left">
	                    <div class="cycle">
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/circle.svg" alt="ring" class="ring"/>
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" class="world"/>

	                        <div class="wrap_6">
	                            <div class="care" inviewspy="200">
	                                <img class="no_1" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                                <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                            </div>
	                            <div class="care" inviewspy="300">
	                                <img class="no_2" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                                <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                            </div>
	                            <div class="care" inviewspy="400">
	                                <img class="no_3" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                                <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                            </div>
	                            <div class="care" inviewspy="500">
	                                <img class="no_4" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                                <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                            </div>
	                            <div class="care" inviewspy="600">
	                                <img class="no_5" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                                <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                            </div>
	                            <div class="care" inviewspy="700">
	                                <img class="no_6" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                                <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="right">
	                    <div class="care_txt">
	                        <div class="title f_bold">
	                            ทุกกิจกรรมสามารถใช้
	                            <p class="hilight f_bold hide-940">6 ปฏิบัติการ</p>
	                            <span class="hilight f_bold show-940"> 6 ปฏิบัติการ </span>
	                            ของ Care the Bear 
	                        </div>
	                        <div class="detail">
	                            มาออกแบบเพื่อประเมินวัดผลเป็นค่า
	                            การลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน 
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="show-xs">
	                <div class="flex center">
	                    <div class="left" inviewspy>
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" />
	                    </div>
	                    <div class="right" inviewspy="100">
	                        <div class="title f_bold">ทุกกิจกรรมสามารถใช้<p class="hilight f_bold hide-940">หลักการ 6 Cares</p><span class="hilight f_bold show-940"> หลักการ 6 Cares </span>ของ Care the Bear </div>
	                        <div class="detail">มาออกแบบเพื่อประเมินวัดผลเป็นค่าการลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน</div>
	                    </div>
	                </div>
	                <div class="wrap_6 flex h-justify wrap">
	                    <div class="care" inviewspy="200">
	                        <img class="no_1" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                        <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_2" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                        <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_3" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                        <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_4" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                        <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_5" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                        <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_6" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                        <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="main-care-the-bear">
	        <div class="container">
	            <span class="title-sec-3" inviewspy="300">สิทธิประโยชน์ใน<br class="d-md-none">การเข้าร่วม Care the Bear</span>
	            <div class="box-content">
	                <div class="single-content" inviewspy="300">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-1.png" alt="">
	                    <span class="text-content">คำนวณ วัดผล ปริมาณการ<br class="d-md-block"> ลดก๊าซเรือนกระจกได้ทันที </span>
	                </div>
	                <div class="single-content" inviewspy="600">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-2.png" alt="">
	                    <span class="text-content">ผลการดำเนินงานนำไปแสดงใน<br class="d-md-block"> Annual Report หรือ<br class="d-md-block">  56-1 One Report ได้          </span>
	                </div>
	                <div class="single-content" inviewspy="900">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-3.png" alt="">
	                    <span class="text-content">นำผลการดำเนินงานยื่นขอใบ<br class="d-md-block"> ประกาศเกียรติคุณ LESS จาก<br class="d-md-block"> องค์การบริหารจัดการก๊าซเรือน<br class="d-md-block"> กระจก (องค์การมหาชน) </span>
	                </div>
	            </div>
	            <div class="box-content">
	                <div class="single-content" inviewspy="300">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-4.png" alt="">
	                    <span class="text-content">เกิดภาพลักษณ์ที่ดีต่อองค์กรใน<br class="d-md-block"> ด้านการบริหารจัดการสิ่งแวดล้อม<br class="d-md-block"> อย่างเป็นรูปธรรม</span>
	                </div>
	                <div class="single-content" inviewspy="600">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-5.png" alt="">
	                    <span class="text-content">เกิดการสร้างจิตสำนึก การมีส่วน<br class="d-md-block"> ร่วมในการดูแลด้านสิ่งแวดล้อม<br class="d-md-block"> ของพนักงานในองค์กร</span>
	                </div>
	                <div class="single-content" inviewspy="900">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-6.png" alt="">
	                    <span class="text-content">องค์กรสามารถออกแบบแผน<br class="d-md-block"> การลดการใช้งบประมาณ เช่น<br class="d-md-block">ลดค่าไฟฟ้า ค่ากระดาษ</span>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="main-performance">
	        <div class="container">
	            <span class="title-performance" inviewspy>ผลการดำเนินงานโครงการ</span>
	            <span class="year-performance" inviewspy>ตั้งแต่ปี 2018 - ปัจจุบัน</span>
	            <div class="box-content-performance">
	                <div class="left-content">
	                    <div class="list-items" inviewspy>
	                        <span class="icon-building"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">จำนวนพันธมิตร</span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_company|replace:',':''}"></span>
	                                <span class="text-3">องค์กร</span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items" inviewspy="200">
	                        <span class="icon-cloud"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">ลดปริมาณก๊าซเรือนกระจกได้</span>
	                            <div>
	                                <span class="text-2" data-no="{(($bear_stats.total_cf|replace:',':''))|number_format:2:".":""}"></span>
	                                <span class="text-3 ">Kg 
	                                    <span class="hanging-co2-1">CO</span>
	                                    <span class="text-indent-eq">eq</span>
	                                </span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items" inviewspy="400">
	                        <span class="icon-tree"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">เทียบเท่าการดูดซับ 
	                                <span class="hanging-co2-2">CO</span> 
	                                <span class="text-indent-per-year">/ ปี ของต้นไม้</span>
	                            </span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_tree|replace:',':''}"></span>
	                                <span class="text-3">ต้น</span>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="right-content" inviewspy="500">
	                    <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-4/content-1.png" alt="">
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="slide-reciever"></div>
	    <div class="contact-html"></div>
	</div>
{/block}
{block name=script}
	<script src="{$image_url}theme/default/public/js/care-the-bear.js"></script>
{/block}