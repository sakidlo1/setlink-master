{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/responsive.css">
    {/block}
    {block name=js}
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/player.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/simpledropdown.js"></script>

        <script src="{$image_url}theme/ctb/assets/js/main.js?ver={$smarty.now}"></script>
    {/block}
{block name=body}
	<section id="article" style="margin-top: 90px;">
        <div class="container">
            <h2 class="title inline-block">{if $smarty.get.q != ''}ผลการค้นหา{else}ข่าวและประชาสัมพันธ์ (News) <i class="icon-circle"></i>{/if}</h2>
            <form action="{$base_url}whale_news" method="get" class="activity-search form-inline mt-20">
                <input type="text" id="q" name="q" value="{$smarty.get.q}" class="form-control mobile-full" placeholder="ระบุคำค้นหา">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            <div class="row">
                {if $list|count > 0}
                    {foreach $list as $news_item}
                        <div class="col-sm-12">
                            <div class="item article is-half">
                                <div class="head">
                                    <span>{$news_item.name}</span>
                                    <a href="{$base_url}whale_news/detail/{$news_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                                </div>
                                <img class="img" src="{$news_item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                <div class="text">
                                    {$news_item.description|nl2br}
                                </div>
                            </div>
                        </div>
                    {/foreach}
                {else}
                    <div class="col-sm-12">
                        <br/><br/><br/>
                        <center>ไม่มีข้อมูล</center>
                        <br/><br/><br/>
                    </div>
                {/if}
            </div>
            <hr/>
            {include file='paging.tpl'}
        </div>
    </section>
    <br/>
    <br class="hidden-xs"/>
{/block}