{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard-carethebear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/menu-bear.css">
    {* <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/aos.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/main.css?ver={$smarty.now}">
    <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/responsive.css?ver={$smarty.now}"> *}
    <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/fontawesome.min.css">
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: false,
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };
    </script>
    <script src="{$image_url}theme/default/public/js/dashboard.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/dashboard-report-carethebear.js"></script>
    {* <script src="{$base_url}carethebear/images/theme/default/assets/js/aos.js"></script> *}
    {*<script src="{$base_url}carethebear/images/theme/default/assets/js/main.js?ver={$smarty.now}"></script>*}
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-summary flex column h-top v-top h-top">
            <div style="z-index: 10;" class="menu-bear" inviewspy>
                <div class="wrap-menu">
                    <a class="menu-link f_med" href="{$base_url}carethebear">Care the Bear</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/about">What’s Care the Bear</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/activity">Activities & Event</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/project">Climate Action Project</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/article">Articles</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/news">News</a>
                </div>
            </div>
            <div class="global-warming-detail" inviewSpy="200">
                <div class="title f_bold">ผลรวมลดปริมาณก๊าซเรือนกระจก <br class="show-xs">และเทียบเท่าการปลูกต้นไม้
                </div>
                <div class="detail-wrapper flex column-xs">
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/air-pollution.png"
                                alt="air-pollution">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">ลดปริมาณก๊าซเรือนกระจกได้</p>
                            <span>
                                <span class="stats-value f_bold" data-global="gas" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">Kg CO<sub>2</sub>e</span>
                            </span>
                        </div>
                    </div>
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/trees.png" alt="trees">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">เทียบเท่าการปลูกต้นไม้ใหญ่ อายุ 10 ปี</p>
                            <span>
                                <span class="stats-value f_bold" data-global="tree" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">ต้น</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="wrap-bear flex row h-justify v-top" inviewSpy="300">
                <div class="bear-grap flex row h-center v-center">
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg"></img>
                    <div class="wrap_character char_bear" id="banner" inviewspy>
                            <div class="character">
                                <img class="lazy-img bear"
                                    data-src="{$image_url}theme/default/public/images/home/banner/bear.png" alt='bear'>
                                <img class="lazy-img sign"
                                    data-src="{$image_url}theme/default/public/images/home/banner/no-foam-sign.png"
                                    alt='bear sign'>
                            </div>
                    </div>
                </div>
                <div class="wrap-title f_bold">
                <span class="head f_bold">การจัดการกิจกรรมและโครงการ</span><br>
                       <span class="super f_bold">Care the Bear</span>
                </div>
            </div>


            
            

            <div class="member-actions" inviewSpy="300">
                <div class="wrap-menu">
                    <a href="{$base_url}dashboard/carethebear_activity" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                        <span class="right f_bold">กิจกรรม</span></a>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                    <div class="dropdown">
                        <div class="dropdown-content">
                            <div class="head flex v-center h-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                <span class="right f_bold">สร้างกิจกรรม</span>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}carethebear/member/activity_add">สร้างกิจกรรมใหม่</a>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}dashboard/carethebear_activity">กิจกรรมที่ดำเนินการ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrap-menu">
                    <a href="{$base_url}dashboard/carethebear" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg">
                        <span class="right f_bold">โครงการ</span></a>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                    <div class="dropdown">
                        <div class="dropdown-content">
                            <div class="head flex v-center h-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                <span class="right f_bold">สร้างโครงการ</span>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}carethebear/member/project_add">สร้างโครงการใหม่</a>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="#">โครงการที่ดำเนินการ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#" class="wrap-menu">
                    <div class="inner-select">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-report-wh.svg">
                        <span class="right f_bold">รายงาน / ผลลัพธ์</span></div>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow-hover.svg"></img>
                </a>
                <a href="{$base_url}dashboard/carethebear_document" class="wrap-menu">
                    <div class="inner">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-document-b.svg">
                        <span class="right f_bold">คู่มือและสื่อเผยแพร่</span></div>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                        <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                </a>
            </div>

            <div class="filter-action" inviewSpy="300">
                <div id="buttons-report" class="contain-wrap">
                    <button class="btn active report" onclick="filterSelection('my-report')">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/task-list.svg">
                        <span>รายงานของฉัน</span>
                    </button>
                    <button class="btn tsd" onclick="filterSelection('tsd-paper')">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/tsd-paper.svg">
                        <span>TSD Paper</span>
                    </button>
                    <div class="btn company-report" onclick="filterSelection('company')">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/group-company.svg">
                        <span>รายงานของบริษัท</span>
                    </div>
                </div>
            </div>

            <div class="wrap-section">
                <div class="section my-report">

                    <div class="member-box" style="margin: 0 auto;">
                        <h2 class="title">{$member.company}</h2>
                            {*<h2 class="title f_bold">ทดสอบ</h2>*}
                            {if $success_msg != ""}
                            <div class="alert alert-success">
                                {$success_msg}
                            </div>
                            {/if}
                            {if $error_msg != ""}
                            <div class="alert alert-danger">
                                {$error_msg}
                            </div>
                            {/if}
                            {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                            {* <h3 class="activity-head title f_med">
                                    <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                    <span>กิจกรรมที่ดำเนินการ</span>
                            </h3> *}
                            <div class="activity-control">
                                <h3 class="activity-head title f_med" style="padding-bottom: 0;">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg" style="width: auto; padding-left: 100px;">
                                <span style="font-weight: 400; color: #3F8CFF;">กิจกรรม</span>
                                </h3>
                                
                                {* <a href="{$base_url}carethebear/member/activity_add" class="activity-add f_bold">เพิ่มกิจกรรม <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                                <div class="clearfix"></div>
                                <div class="header-list">
                                    <div class="header-50 flex h-justify v-center">รายงานกิจกรรม</div>
                                    <div class="header-40 flex h-justify v-center">
                                        <div class="date" style="width: 42%;">วันที่</div>
                                        <div class="status">สถานะ</div>
                                        <div class="edit" style="width: 15%;">Preview</div>
                                        {* <img class="download" src="{$base_url}carethebear/images/theme/default/assets/images/icon-download2.png"> *}
                                    </div>
                                </div>
                                <div class="activity-list">
                                    {if $list_act|count > 0}
                                        {foreach $list_act as $key => $item}
                                            {if strtotime($item.activity_date) < strtotime('-6 day') }
                                                {$success = true}
                                            {else}
                                                {$success = false}
                                            {/if}
                                            <div class="inner">
                                                {* <div class="pull-right">
                                                    <a class="action" href="https://api.qrserver.com/v1/create-qr-code/?data={$base_url}carethebear/activity/detail/{$item.id}&amp;size=300x300" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i>
                                                </div> *}
                                                {* <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p>
                                                <p style="padding-left: 50px;">{$item.activity_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p> *}
                                                <div class="actions">
                                                    <span class="action" style="width: 5%; display: inline-block; text-align: right; ">{($key + 1)}.</span>
                                                    <a class="action item1">{$item.name}</a>
                                                    <p class="action item2" style="font-size: 22px!important;"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$item.activity_date|date_thai:"%e %B %Y"}</p>
{*                                                    <p class="action item3">In Progress</p>*}
                                                    <p class="action item3">
                                                        {if $success }
                                                            Success
                                                        {else}
                                                            In Progress
                                                        {/if}
                                                    </p>
                                                    <a class="action item4" href="{$base_url}carethebear/member/activity_report/{$item.id}" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/preview.svg"></a>
                                                    <a class="action item5"  style="opacity: 0;" href="{$base_url}carethebear/member/activity_pdf/{$item.id}" download><img src="{$image_url}theme/default/public/images/dashboard_carethebear/qr-code.svg"></a>
                                                </div>
                                            </div>
                                        {/foreach}
                                    {else}
                                        <div class="inner">
                                            <center>ไม่มีข้อมูล</center>
                                        </div>
                                    {/if}
                                </div>
                                {* {include file='paging.tpl'} *}
                            </div>
                    </div>


                    <div class="member-box" style="margin-top: 0;">
                        <h2 class="title">{$member.company}</h2>
                                {*<h2 class="title f_bold">ทดสอบ</h2>*}
                            {if $success_msg != ""}
                            <div class="alert alert-success">
                                {$success_msg}
                            </div>
                            {/if}
                            {if $error_msg != ""}
                            <div class="alert alert-danger">
                                {$error_msg}
                            </div>
                            {/if}
                            {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                            {* <h3 class="activity-head title f_bold">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg">
                                    <span>โครงการที่ดำเนินการ</span>
                                </h3> *}
                            <div class="activity-control">
                                <h3 class="activity-head title f_bold" style="padding-bottom: 0;">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg" style="width: auto; padding-left: 100px;">
                                    <span style="font-weight: 400; color: #3F8CFF;">โครงการ</span>
                                </h3>
                                {* <a href="{$base_url}carethebear/member/project_add" class="activity-add f_bold">เพิ่มโครงการ <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                                <div class="clearfix"></div>
                                <div class="header-list">
                                    <div class="header-50 flex h-justify v-center">รายงานโครงการ</div>
                                    <div class="header-40 flex h-justify v-center">
                                    <div class="date" style="width: 42%;">วันที่</div>
                                    <div class="status">สถานะ</div>
                                    <div class="edit" style="width: 15%;">Preview</div>
                                    </div>
                                </div>

                                <div class="activity-list">
                                    {if $list|count > 0}
                                        {foreach $list as $key => $item}
                                            {if strtotime($item.end_date) < strtotime('-6 day') }
                                                {$success = true}
                                            {else}
                                                {$success = false}
                                            {/if}
                                            <div class="inner">
                                                {* <div class="pull-right">
                                                    <a class="action" href="https://api.qrserver.com/v1/create-qr-code/?data={$base_url}carethebear/project/detail/{$item.id}&amp;size=300x300" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i>
                                                </div> *}
                                                {* <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p> *}
                                                {* <p style="padding-left: 50px;">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p> *}
                                                <div class="actions">
                                                    <span class="action" style="width: 5%; display: inline-block; text-align: right; ">{($key + 1)}.</span>
                                                    <a class="action item1">{$item.name}</a>
                                                    <p class="action item2"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} {*({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})*}</p>
                                                    <p class="action item3">
                                                        {if $success }
                                                            Success
                                                        {else}
                                                            In Progress
                                                        {/if}
                                                    </p>
                                                    <a class="action item4" href="{$base_url}carethebear/member/project_report/{$item.id}" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/preview.svg"></a>
                                                    <a class="action item5" style="opacity: 0;" href="#" download><img src="{$image_url}theme/default/public/images/dashboard_carethebear/qr-code.svg"></a>
                                                </div>
                                            </div>
                                        {/foreach}
                                    {else}
                                        <div class="inner">
                                            <center>ไม่มีข้อมูล</center>
                                        </div>
                                    {/if}
                                </div>
                                {* {include file='paging.tpl'} *}
                            </div>
                    </div>
                </div>


                <div class="section tsd-paper">

                    <div class="member-box" style="margin-top: 0;">
                        <h2 class="title">{$member.company}</h2>
                                {*<h2 class="title f_bold">ทดสอบ</h2>*}
                            {if $success_msg != ""}
                            <div class="alert alert-success">
                                {$success_msg}
                            </div>
                            {/if}
                            {if $error_msg != ""}
                            <div class="alert alert-danger">
                                {$error_msg}
                            </div>
                            {/if}
                            {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                            {* <h3 class="activity-head title f_bold">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg">
                                    <span>โครงการที่ดำเนินการ</span>
                                </h3> *}
                            <div class="activity-control">
                                <h3 class="activity-head title f_bold" style="padding-bottom: 0;">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg" style="width: auto; padding-left: 100px;">
                                    <span style="font-weight: 400; color: #3F8CFF;">รายละเอียดของโครงการ TSD</span>
                                </h3>
                                {* <a href="{$base_url}carethebear/member/project_add" class="activity-add f_bold">เพิ่มโครงการ <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                                <div class="clearfix"></div>
                                <div class="header-list">
                                    <div class="header-25 flex h-justify v-center" style="width: 22%;">Corporate Action</div>
                                    <div class="header-25 flex h-justify v-center" style="width: 22%;">วันที่เริ่ม - สิ้นสุด</div>
                                    <div class="header-h flex h-justify v-center" style="width: 66%;">
                                        <div class="tsd-title">กระดาษ A4 (70g)</div>
                                        <div class="tsd-title">กระดาษ A4 (80g)</div>
                                        <div class="tsd-title">กระดาษ A5 (80g)</div>
                                        <div class="tsd-title">จำนวนรวม (แผ่น)</div>
                                        <div class="tsd-title icon-cloud" style="font-size: 50px;color: #40C0EF;"></div>
                                    </div>
                                </div>

                                <div class="activity-list">
                                    {if $list_tsd|count > 0}
                                        {foreach $list_tsd as $key => $item}
                                            <div class="inner">
                                                {* <div class="pull-right">
                                                    <a class="action" href="https://api.qrserver.com/v1/create-qr-code/?data={$base_url}carethebear/project/detail/{$item.id}&amp;size=300x300" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i>
                                                </div> *}
                                                {* <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p> *}
                                                {* <p style="padding-left: 50px;">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p> *}
                                                <div class="actions" style="padding: 20px;">
                                                    <span class="action" style="width: 5%; display: inline-block; text-align: right; ">{($key + 1)}.</span>
                                                    <a class="action item1" style="width: 10%; justify-content: center;"  href="{$base_url}carethebear/member/project_report/{$item.id}" target="_blank">{$item.ca_type}</a>
                                                    <p class="action item2" style="width: 24%; padding-left: 40px;"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$item.start_date_time|date_thai:"%e %B %Y"} - {$item.end_date_time|date_thai:"%e %B %Y"} {*({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})*}</p>
                                                    <div class="wrap-value flex h-justify v-center" style="width: 65%;">
                                                        <div class="tsd-value">{$item.size_thickness_T70_A4}</div>
                                                        <div class="tsd-value">{$item.size_thickness_T80_A4}</div>
                                                        <div class="tsd-value">{$item.size_thickness_T80_A5}</div>
                                                        <div class="tsd-value">{$item.sum_care_total}</div>
                                                        <div class="tsd-value">{$item.cf_care_total}</div>
                                                        {*<div class="tsd-value">{(($item.cf_care_total)|replace:',':'')|number_format:2:".":""}</div>*}
                                                    </div>
                                                </div>
                                            </div>
                                        {/foreach}
                                    {else}
                                        <div class="inner">
                                            <center>ไม่มีข้อมูล</center>
                                        </div>
                                    {/if}
                                </div>
                                {* {include file='paging.tpl'} *}
                            </div>
                    </div>

                </div>

                <div class="section company-report">
                    

                    <div class="member-box" style="margin: 0 auto;">
                        <h2 class="title">{$member.company}</h2>
                            {*<h2 class="title f_bold">ทดสอบ</h2>*}
                            {if $success_msg != ""}
                            <div class="alert alert-success">
                                {$success_msg}
                            </div>
                            {/if}
                            {if $error_msg != ""}
                            <div class="alert alert-danger">
                                {$error_msg}
                            </div>
                            {/if}
                            {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                            {* <h3 class="activity-head title f_med">
                                    <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                    <span>กิจกรรมที่ดำเนินการ</span>
                            </h3> *}
                            <div class="activity-control">
                                <h3 class="activity-head title f_med" style="padding-bottom: 0;">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg" style="width: auto; padding-left: 100px;">
                                <span style="font-weight: 400; color: #3F8CFF;">กิจกรรม บริษัท</span>
                                </h3>
                                
                                {* <a href="{$base_url}carethebear/member/activity_add" class="activity-add f_bold">เพิ่มกิจกรรม <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                                <div class="clearfix"></div>
                                <div class="header-list">
                                    <div class="header-50 flex h-justify v-center">รายงานกิจกรรม</div>
                                    <div class="header-40 flex h-justify v-center" style="width: 30%;">วันที่</div>
                                        <div class="company-title icon-cloud t_yellow" style="font-size: 50px;"></div>
                                        <div class="company-title icon-tree t_yellow" style="font-size: 50px;"></div>
                                </div>

                                <div class="activity-list">
                                    {if $list_comp_activity|count > 0}
                                        {foreach $list_comp_activity as $key => $item}
                                            <div class="inner">
                                                <div class="actions" style="padding: 25px;">
                                                    <span class="action" style="width: 5%; display: inline-block; text-align: right;">{($key + 1)}.</span>
                                                    <a class="action item1" style="width: 35%;" href="{$base_url}carethebear/member/activity_report/{$item.id}" target="_blank">{$item.name}</a>
                                                    <p class="action item2" style="font-size: 22px!important; margin: 0; width: 30%;"><img style="padding: 0 10px 0 0;" src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$item.activity_date|date_thai:"%e %B %Y"}</p>
                                                    <div class="company-value">{$item.cf_care_total}</div>
                                                    <div class="company-value">{(($item.cf_care_total) / 9|replace:',':'')|number_format:2:".":""}</div>
                                                </div>
                                            </div>
                                        {/foreach}
                                    {else}
                                        <div class="inner">
                                            <center>ไม่มีข้อมูล</center>
                                        </div>
                                    {/if}
                                </div>
                                {* {include file='paging.tpl'} *}
                            </div>
                    </div>


                    <div class="member-box" style="margin-top: 0;">
                        <h2 class="title">{$member.company}</h2>
                                {*<h2 class="title f_bold">ทดสอบ</h2>*}
                            {if $success_msg != ""}
                            <div class="alert alert-success">
                                {$success_msg}
                            </div>
                            {/if}
                            {if $error_msg != ""}
                            <div class="alert alert-danger">
                                {$error_msg}
                            </div>
                            {/if}
                            {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                            {* <h3 class="activity-head title f_bold">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg">
                                    <span>โครงการที่ดำเนินการ</span>
                                </h3> *}
                            <div class="activity-control">
                                <h3 class="activity-head title f_bold" style="padding-bottom: 0;">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg" style="width: auto; padding-left: 100px;">
                                    <span style="font-weight: 400; color: #3F8CFF;">โครงการ บริษัท</span>
                                </h3>
                                {* <a href="{$base_url}carethebear/member/project_add" class="activity-add f_bold">เพิ่มโครงการ <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                                <div class="clearfix"></div>
                                <div class="header-list">
                                    <div class="header-50 flex h-justify v-center">รายงานโครงการ</div>
                                    <div class="header-40 flex h-justify v-center" style="width: 30%;">วันที่</div>
                                        <div class="company-title icon-cloud t_yellow" style="font-size: 50px;"></div>
                                        <div class="company-title icon-tree t_yellow" style="font-size: 50px;"></div>
                                </div>

                                <div class="activity-list">
                                    {if $list_comp_project|count > 0}
                                        {foreach $list_comp_project as $key => $item}
                                            <div class="inner">
                                                {* <div class="pull-right">
                                                    <a class="action" href="https://api.qrserver.com/v1/create-qr-code/?data={$base_url}carethebear/project/detail/{$item.id}&amp;size=300x300" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i>
                                                </div> *}
                                                {* <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p> *}
                                                {* <p style="padding-left: 50px;">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p> *}
                                                <div class="actions" style="padding: 25px;">
                                                    <span class="action" style="width: 5%; display: inline-block; text-align: right; ">{($key + 1)}.</span>
                                                    <a class="action item1" style="width: 35%;" href="{$base_url}carethebear/member/project_report/{$item.id}" target="_blank">{$item.name}</a>
                                                    <p class="action item2" style="font-size: 22px!important; margin: 0; width: 30%;"><img style="padding: 0 10px 0 0;" src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} {*({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})*}</p>
                                                    <div class="company-value">{$item.cf_care_total}</div>
                                                    <div class="company-value">{(($item.cf_care_total) / 9|replace:',':'')|number_format:2:".":""}</div>
                                                </div>
                                            </div>
                                        {/foreach}
                                    {else}
                                        <div class="inner">
                                            <center>ไม่มีข้อมูล</center>
                                        </div>
                                    {/if}
                                </div>
                                {* {include file='paging.tpl'} *}
                            </div>
                    </div>
        
                </div>
            </div>




        </div>
    </div>
{/block}
