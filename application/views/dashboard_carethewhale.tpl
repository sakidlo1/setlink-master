{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard.css">
{/block}
{block name=js}
    <script src="{$image_url}theme/default/public/js/dashboard.js?v=1.0"></script>
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-summary flex column h-top v-top h-center">
            <iframe src="{$base_url}carethewhale/" style="width: 100%;height: 1000px;" frameBorder="0" scrolling="no"></iframe>
        </div>
    </div>
{/block}