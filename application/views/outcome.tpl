{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/outcome.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/calendar.css">
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: false,
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };
        const outcomeData = {
            result_climate: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                trees_planted: '{$stats.total.tree}',
                greenhouse_gas_absorb: '0'
            }
        }
        let outcomeSummary = {
            bear: {
                cate: 'bear',
                climate_name: 'โครงการ Care the Bear',
                climate_desc: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการ<br class="show-xs">จัดงานหรือทุกกิจกรรมในรูปแบบ Online และ Onsite',
                year: '{$smarty.now|date_format:"%Y"}',
                company_name: '{$member.carethebear.company}',
                reduce_gas_activity: '{$stats.bear.all_activity}',
                meeting: '{$stats.bear.meeting}',
                training: '{$stats.bear.training}',
                give_awards: '{$stats.bear.give_awards}',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.bear.cf}',
                    compare_tree_planted: '{$stats.bear.tree}'
                },
                progress_detail: [
                    {
                        txt: 'เดินทางโดยรถไฟฟ้า รถสาธารณะ หรือ Car pool หรือรถจักรยาน',
                        value: [
                            {$stats.bear.cf_care_1},
                            {$stats.bear.tree_care_1}
                        ],
                    },
                    {
                        txt: 'ลดการใช้กระดาษและพลาสติก',
                        value: [
                            {$stats.bear.cf_care_2},
                            {$stats.bear.tree_care_2}
                        ]
                    },
                    {
                        txt: 'เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่',
                        value: [
                            {$stats.bear.cf_care_3},
                            {$stats.bear.tree_care_3}
                        ]
                    },
                    {
                        txt: 'ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า',
                        value: [
                            {$stats.bear.cf_care_4},
                            {$stats.bear.tree_care_4}
                        ]
                    },
                    {
                        txt: 'งดการใช้โฟม',
                        value: [
                            {$stats.bear.cf_care_5},
                            {$stats.bear.tree_care_5}
                        ]
                    },
                    {
                        txt: 'ลดการเกิดขยะ ตักอาหารแต่พอดีและทานให้หมด',
                        value: [
                            {$stats.bear.cf_care_6},
                            {$stats.bear.tree_care_6}
                        ]
                    },
                ],
                tableTitle: 'ปริมาณการลดก๊าซเรือนกระจกการหลักการ 6 Cares',
                tableStructure: {
                    head: [
                        [
                            'หลักการ 6 Cares',
                        ],
                        [
                            'ปริมาณการลดก๊าซ<br>เรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                            'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                        ]
                    ],
                    nested: [[50, []], [40, [45, 45]]]
                }
            },

            bear_tsd: {
                cate: 'bear_tsd',
                climate_name: 'โครงการ TSD PAPER',
                climate_desc: 'การลดการปล่อยก๊าซเรือนกระจกจากการลดใช้กระดาษของกิจกรรม <br class="show-xs">Corporate Action บริษัทผู้ออกหลักทรัพย์',
                year: '{$smarty.now|date_format:"%Y"}',
                company_name: '{$member.carethebear.company}',
                bear_tsd_total: '{$stats.bear_tsd.tsd}',
                corporate_action: '{$stats.bear_tsd.corporate_action}',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.bear_tsd.cf}',
                    compare_tree_planted: '{$stats.bear_tsd.tree}'
                },
                progress_detail_tsd: [
                    {
                        
                        {if ($stats.bear_tsd.aw > 0)}
                            txt: 'Add Waraant (AW)',
                        value: [
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw_co},
                            {$stats.bear_tsd.aw_co_n}
                        ],
                        {else}
                            txt: 'Add Waraant (AW)',
                        value: [
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw}
                        ],
                        {/if}
                    },
                    {
                        
                        {if ($stats.bear_tsd.cd > 0)}
                            txt: 'Capital Decrease (CD)',
                        value: [
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cd_co},
                            {$stats.bear_tsd.cd_co_n}
                        ],
                        {else}
                            txt: 'Capital Decrease (CD)',
                        value: [
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cd}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.cn > 0}
                            txt: 'Capital Decrease / Change Security (CN)',
                        value: [
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Capital Decrease / Change Security (CN)',
                        value: [
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.cn}
                        ],
                        {/if}
                    },
                    
                    {
                        
                        {if $stats.bear_tsd.dg > 0}
                            txt: 'Deregistration (DG)',
                        value: [
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Deregistration (DG)',
                        value: [
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.dg}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.ma > 0}
                            txt: 'Merger and Acquisition (MA)',
                        value: [
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Merger and Acquisition (MA)',
                        value: [
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.ma}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.nr > 0}
                            txt: 'New Registration (NR)',
                        value: [
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'New Registration (NR)',
                        value: [
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.nr}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.po > 0}
                            txt: 'Public Offering (PO)',
                        value: [
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Public Offering (PO)',
                        value: [
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.po}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.sc > 0}
                            txt: 'Split / Consolidate (Change Par) (SC)',
                        value: [
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Split / Consolidate (Change Par) (SC)',
                        value: [
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.sc}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xb > 0}
                            txt: 'Excluding Other Benefit (XB)',
                        value: [
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Excluding Other Benefit (XB)',
                        value: [
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.xb}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xd > 0}
                            txt: 'Dividend (XD)',
                        value: [
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd_co},
                            {$stats.bear_tsd.xd_co_n}
                        ],
                        {else}
                            txt: 'Dividend (XD)',
                        value: [
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xe > 0}
                            txt: 'Right Exercise (XE)',
                        value: [
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Right Exercise',
                        value: [
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.xe}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xi > 0}
                            txt: 'Interest Payment (XI)',
                        value: [
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Interest Payment (XI)',
                        value: [
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.xi}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xm > 0}
                            txt: 'Voting/Meeting (XM)',
                        value: [
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Voting/Meeting (XM)',
                        value: [
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.xm}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xo > 0}
                            txt: 'Book Closing for Other Purpose (XO)',
                        value: [
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Book Closing for Other Purpose (XO)',
                        value: [
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.xo}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xp > 0}
                            txt: 'Bond Principal (XP)',
                        value: [
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Bond Principal (XP)',
                        value: [
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.xp}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xr > 0}
                            txt: 'Right Offering (XR)',
                        value: [
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Right Offering (XR)',
                        value: [
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.xr}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xs > 0}
                            txt: 'Short Term Warrant (XS)',
                        value: [
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'Short Term Warrant (XS)',
                        value: [
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.xs}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xt > 0}
                            txt: 'TSR Transferable Subscription Right (XT)',
                        value: [
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.tsd_cf_total},
                            {$stats.bear_tsd.tsd_tree_total}
                        ],
                        {else}
                            txt: 'TSR Transferable Subscription Right (XT)',
                        value: [
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.xt}
                        ],
                        {/if}
                    },
                ],
                tableTitle_tsd: 'TSD PROJECT',
                tableStructure_tsd: {
                    head: [
                        [
                            'Corporate Action',
                        ],
                        [
                            'จำนวน',
                            'ปริมาณการลดก๊าซ<br>เรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                            'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                        ]
                    ],
                    nested: [[40, []], [60, [30, 30, 30]]]
                }
            },

            whale: {
                cate: 'whale',
                climate_name: 'โครงการ Care the Whale',
                climate_desc: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการบริหาร<br class="show-xs">จัดการขยะตั้งแต่ต้นทางถึงปลายทาง',
                company_name: '{$member.carethebear.company}',
                year: '2565',
                month: 'ม.ค. - มิ.ย.',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.whale.cf}',
                    compare_tree_planted: '{$stats.whale.tree}'
                },
                progress_detail: {
                    waste: [
                        {foreach $stats.whale.waste as $waste_item}
                            {
                                txt: '{$waste_item.txt}',
                                value: [
                                    {$waste_item.value[0]},
                                    {$waste_item.value[1]},
                                    {$waste_item.value[2]}
                                ],
                                percent: {$waste_item.percent}
                            },
                        {/foreach}
                    ],
                    wasteManage: [
                        {foreach $stats.whale.wasteManage as $waste_item}
                            {
                                txt: '{$waste_item.txt}',
                                value: [
                                    '{$waste_item.value[0]}',
                                    {$waste_item.value[1]},
                                    {$waste_item.value[2]},
                                    {$waste_item.value[3]}
                                ],
                            },
                        {/foreach}
                    ]
                },
                wasteTableTitle: 'ประเภทขยะที่ได้จากการคัดแยกขยะ',
                wasteTableStructure: {
                    head: [
                        [
                            'ประเภทขยะ',
                        ],
                        [
                            'ปริมาณขยะ <span class="t_grey t_16">(กก.)</span>',
                            'ปริมาณการลดก๊าซเรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                            'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                        ]
                    ],
                    nested: [[30, []], [65, [25, 25, 25]]]
                },
                wasteManageTableTitle: 'ประเภทการจัดการขยะ',
                wasteManageTableStructure: {
                    head: [
                        [
                            'วิธีการจัดการขยะ',
                        ],
                        [
                            'สัดส่วน',
                            'ปริมาณขยะ <span class="t_grey t_16">(กก.)</span>',
                            'ปริมาณการลดก๊าซเรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                            'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                        ]
                    ],
                    nested: [[35, []], [65, [20, 30, 25, 25]]]
                }
            },
            elephant: {
                cate: 'elephant',
                climate_name: 'โครงการ Care the Wild <br class="show-xs">“ปลูกป้อง Plant & Protect” ',
                climate_desc: 'โครงการความร่วมมือปลูกป่าเพื่อเพิ่มผืนป่า<br class="show-xs">และรักษาสมดุลระบบนิเวศตามธรรมชาติ',
                company_name: '{$member.carethebear.company}',
                climate_data: {
                    greenhouse_gas_reduced: '0',
                },
                progress_detail: {
                    houses: '0',
                    locations: [
                        {
                            area: '0 ไร่',
                            trees: ' 0',
                            village: '...',
                            location: '...'
                        },
                        {
                            area: '0 ไร่',
                            trees: ' 0',
                            village: '...',
                            location: '...'
                        }
                    ]
                },
            },
        }

        let outcomeExportData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                year: '{($smarty.now|date_format:"%Y" + 543)}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                year: '{($smarty.now|date_format:"%Y" + 543)}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            result_climate: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                trees_planted: '{$stats.total.tree}',
                greenhouse_gas_absorb: '0'
            },
            bear: {
                reduce_gas_activity: '{$stats.bear.all_activity}',
                meeting: '{$stats.bear.meeting}',
                training: '{$stats.bear.training}',
                give_awards: '{$stats.bear.give_awards}',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.bear.cf}',
                    compare_tree_planted: '{$stats.bear.tree}'
                },
                progress_detail: [
                    {
                        txt: 'เดินทางโดยรถไฟฟ้า รถสาธารณะ หรือ Car pool หรือรถจักรยาน',
                        value: [
                            {$stats.bear.cf_care_1},
                            {$stats.bear.tree_care_1}
                        ],
                    },
                    {
                        txt: 'ลดการใช้กระดาษและพลาสติก',
                        value: [
                            {$stats.bear.cf_care_2},
                            {$stats.bear.tree_care_2}
                        ]
                    },
                    {
                        txt: 'เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่',
                        value: [
                            {$stats.bear.cf_care_3},
                            {$stats.bear.tree_care_3}
                        ]
                    },
                    {
                        txt: 'ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า',
                        value: [
                            {$stats.bear.cf_care_4},
                            {$stats.bear.tree_care_4}
                        ]
                    },
                    {
                        txt: 'งดการใช้โฟม',
                        value: [
                            {$stats.bear.cf_care_5},
                            {$stats.bear.tree_care_5}
                        ]
                    },
                    {
                        txt: 'ลดการเกิดขยะ ตักอาหารแต่พอดีและทานให้หมด',
                        value: [
                            {$stats.bear.cf_care_6},
                            {$stats.bear.tree_care_6}
                        ]
                    },
                ],
                tableTitle: 'ปริมาณการลดก๊าซเรือนกระจกการหลักการ 6 Cares',
                tableStructure: {
                    head: [
                        [
                            'หลักการ 6 Cares',
                        ],
                        [
                            'ปริมาณการลดก๊าซ\nเรือนกระจก (kg.CO2e)',
                            'เทียบเท่ากับ\nการปลูกต้นไม้ (ต้น)'
                        ]
                    ]
                }
            },
            bear_tsd: {
                cate: 'bear_tsd',
                climate_name: 'โครงการ TSD PAPER',
                climate_desc: 'การลดการปล่อยก๊าซเรือนกระจกจากการลดใช้กระดาษของกิจกรรม <br class="show-xs">Corporate Action บริษัทผู้ออกหลักทรัพย์',
                year: '{$smarty.now|date_format:"%Y"}',
                company_name: '{$member.carethebear.company}',
                bear_tsd_total: '{$stats.bear_tsd.tsd}',
                corporate_action: '{$stats.bear_tsd.corporate_action}',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.bear_tsd.cf}',
                    compare_tree_planted: '{$stats.bear_tsd.tree}'
                },
                progress_detail_tsd: [
                    {
                        
                        {if ($stats.bear_tsd.aw > 0)}
                            txt: 'Add Waraant (AW)',
                        value: [
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw_co},
                            {$stats.bear_tsd.aw_co_n}
                        ],
                        {else}
                            txt: 'Add Waraant (AW)',
                        value: [
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw},
                            {$stats.bear_tsd.aw}
                        ],
                        {/if}
                    },
                    {
                        
                        {if ($stats.bear_tsd.cd > 0)}
                            txt: 'Capital Decrease (CD)',
                        value: [
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cn_co},
                            {$stats.bear_tsd.cn_co_n}
                        ],
                        {else}
                            txt: 'Capital Decrease (CD)',
                        value: [
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cd},
                            {$stats.bear_tsd.cd}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.cn > 0}
                            txt: 'Capital Decrease / Change Security (CN)',
                        value: [
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.cn_co},
                            {$stats.bear_tsd.cn_co_n}
                        ],
                        {else}
                            txt: 'Capital Decrease / Change Security (CN)',
                        value: [
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.cn},
                            {$stats.bear_tsd.cn}
                        ],
                        {/if}
                    },
                    
                    {
                        
                        {if $stats.bear_tsd.dg > 0}
                            txt: 'Deregistration (DG)',
                        value: [
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.dg_co},
                            {$stats.bear_tsd.dg_co_n}
                        ],
                        {else}
                            txt: 'Deregistration (DG)',
                        value: [
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.dg},
                            {$stats.bear_tsd.dg}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.ma > 0}
                            txt: 'Merger and Acquisition (MA)',
                        value: [
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.ma_co},
                            {$stats.bear_tsd.ma_co_n}
                        ],
                        {else}
                            txt: 'Merger and Acquisition (MA)',
                        value: [
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.ma},
                            {$stats.bear_tsd.ma}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.nr > 0}
                            txt: 'New Registration (NR)',
                        value: [
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.nr_co},
                            {$stats.bear_tsd.nr_co_n}
                        ],
                        {else}
                            txt: 'New Registration (NR)',
                        value: [
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.nr},
                            {$stats.bear_tsd.nr}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.po > 0}
                            txt: 'Public Offering (PO)',
                        value: [
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.po_co},
                            {$stats.bear_tsd.po_co_n}
                        ],
                        {else}
                            txt: 'Public Offering (PO)',
                        value: [
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.po},
                            {$stats.bear_tsd.po}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.sc > 0}
                            txt: 'Split / Consolidate (Change Par) (SC)',
                        value: [
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.sc_co},
                            {$stats.bear_tsd.sc_co_n}
                        ],
                        {else}
                            txt: 'Split / Consolidate (Change Par) (SC)',
                        value: [
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.sc},
                            {$stats.bear_tsd.sc}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xb > 0}
                            txt: 'Excluding Other Benefit (XB)',
                        value: [
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.xb_co},
                            {$stats.bear_tsd.xb_co_n}
                        ],
                        {else}
                            txt: 'Excluding Other Benefit (XB)',
                        value: [
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.xb},
                            {$stats.bear_tsd.xb}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xd > 0}
                            txt: 'Dividend (XD)',
                        value: [
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd_co},
                            {$stats.bear_tsd.xd_co_n}
                        ],
                        {else}
                            txt: 'Dividend (XD)',
                        value: [
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd},
                            {$stats.bear_tsd.xd}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xe > 0}
                            txt: 'Right Exercise (XE)',
                        value: [
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.xe_co},
                            {$stats.bear_tsd.xe_co_n}
                        ],
                        {else}
                            txt: 'Right Exercise',
                        value: [
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.xe},
                            {$stats.bear_tsd.xe}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xi > 0}
                            txt: 'Interest Payment (XI)',
                        value: [
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.xi_co},
                            {$stats.bear_tsd.xi_co_n}
                        ],
                        {else}
                            txt: 'Interest Payment (XI)',
                        value: [
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.xi},
                            {$stats.bear_tsd.xi}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xm > 0}
                            txt: 'Voting/Meeting (XM)',
                        value: [
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.xm_co},
                            {$stats.bear_tsd.xm_co_n}
                        ],
                        {else}
                            txt: 'Voting/Meeting (XM)',
                        value: [
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.xm},
                            {$stats.bear_tsd.xm}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xo > 0}
                            txt: 'Book Closing for Other Purpose (XO)',
                        value: [
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.xo_co},
                            {$stats.bear_tsd.xo_co_n}
                        ],
                        {else}
                            txt: 'Book Closing for Other Purpose (XO)',
                        value: [
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.xo},
                            {$stats.bear_tsd.xo}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xp > 0}
                            txt: 'Bond Principal (XP)',
                        value: [
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.xp_co},
                            {$stats.bear_tsd.xp_co_n}
                        ],
                        {else}
                            txt: 'Bond Principal (XP)',
                        value: [
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.xp},
                            {$stats.bear_tsd.xp}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xr > 0}
                            txt: 'Right Offering (XR)',
                        value: [
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.xr_co},
                            {$stats.bear_tsd.xr_co_n}
                        ],
                        {else}
                            txt: 'Right Offering (XR)',
                        value: [
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.xr},
                            {$stats.bear_tsd.xr}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xs > 0}
                            txt: 'Short Term Warrant (XS)',
                        value: [
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.xs_co},
                            {$stats.bear_tsd.xs_co_n}
                        ],
                        {else}
                            txt: 'Short Term Warrant (XS)',
                        value: [
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.xs},
                            {$stats.bear_tsd.xs}
                        ],
                        {/if}
                    },
                    {
                        
                        {if $stats.bear_tsd.xt > 0}
                            txt: 'TSR Transferable Subscription Right (XT)',
                        value: [
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.xt_co},
                            {$stats.bear_tsd.xt_co_n}
                        ],
                        {else}
                            txt: 'TSR Transferable Subscription Right (XT)',
                        value: [
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.xt},
                            {$stats.bear_tsd.xt}
                        ],
                        {/if}
                    },
                ],
                tableTitle_tsd: 'TSD PROJECT',
                tableStructure_tsd: {
                    head: [
                        [
                            'Corporate Action',
                        ],
                        [
                            'จำนวน',
                            'ปริมาณการลดก๊าซ<br>เรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                            'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                        ]
                    ],
                    nested: [[40, []], [60, [30, 30, 30]]]
                }
            },
            
            whale: {
                year: '2565',
                month: 'ม.ค. - มิ.ย.',
                wasteTableTitle: 'ประเภทขยะที่ได้จากการคัดแยกขยะ',
                wasteManageTableTitle: 'ประเภทการจัดการขยะ',
                climate_data: {
                    greenhouse_gas_reduced: '{$stats.whale.cf}',
                    compare_tree_planted: '{$stats.whale.tree}'
                },
                progress_detail: {
                    waste: [
                        {foreach $stats.whale.waste as $waste_item}
                            {
                                txt: '{$waste_item.txt}',
                                value: [
                                    {$waste_item.value[0]},
                                    {$waste_item.value[1]},
                                    {$waste_item.value[2]}
                                ],
                                percent: {$waste_item.percent}
                            },
                        {/foreach}
                    ],
                    wasteManage: [
                        {foreach $stats.whale.wasteManage as $waste_item}
                            {
                                txt: '{$waste_item.txt}',
                                value: [
                                    '{$waste_item.value[0]}',
                                    {$waste_item.value[1]},
                                    {$waste_item.value[2]},
                                    {$waste_item.value[3]}
                                ],
                            },
                        {/foreach}
                    ]
                },
            },
            elephant: {
                climate_name: 'โครงการ Care the Wild “ปลูกป้อง Plant & Protect” ',
                climate_desc: 'โครงการความร่วมมือปลูกป่าเพื่อเพิ่มผืนป่าและรักษาสมดุลระบบนิเวศตามธรรมชาติ',
                company_name: '{$member.carethebear.company}',
                climate_data: {
                    greenhouse_gas_absorb: '0',
                },
                progress_detail: {
                    houses: '0',
                    locations: [
                        {
                            area: '0 ไร่',
                            trees: ' 0',
                            village: '...',
                            location: '...'
                        },
                        {
                            area: '0 ไร่',
                            trees: ' 0',
                            village: '...',
                            location: '...'
                        }
                    ]
                },
            }
        }
    </script>
    <script>
        var first_set_date = 0;

        {if $smarty.get.from != '' && $smarty.get.to != ''}
            {assign var="start_date" value="-"|explode:$smarty.get.from}
            {assign var="end_date" value="-"|explode:$smarty.get.to}
        {else}
            {assign var="start_date" value="-"|explode:"2020-01-01"}
            {$now_date = $smarty.now|date_format:"%Y-%m-%d"}
            {assign var="end_date" value="-"|explode:$now_date}
        {/if}

        var from_data_date = new Date({$start_date[0]}, {($start_date[1]|intval - 1)}, {$start_date[2]|intval});
        var to_data_date = new Date({$end_date[0]}, {($end_date[1]|intval - 1)}, {$end_date[2]|intval});
    </script>
    <script src="{$image_url}theme/default/public/js/plugins/flatpicker.js"></script>
    <script src="{$image_url}theme/default/public/js/plugins/locale_th.js"></script>
    <script src="{$image_url}theme/default/public/js/chart.js"></script>
    <script src="{$image_url}theme/default/public/js/pdf-icon.js"></script>
    <script src="{$image_url}theme/default/public/js/pdf-font.js"></script>
    <script src="{$image_url}theme/default/public/js/pdf.js"></script>
    <script src="{$image_url}theme/default/public/js/toDataURL.js"></script>
    <script src="{$image_url}theme/default/public/js/docx.js"></script>
    <script src="{$image_url}theme/default/public/js/fileSaver.js"></script>
    <script src="{$image_url}theme/default/public/js/outcome.js?v=1.7"></script>
{/block}
{block name=body}
    <div id="content" class=" flex h-justify">
        <div class="middle-content">
            <div class="outcome-summary flex column v-center">
                <div class="title f_bold" inviewSpy="100">สรุปผลการดำเนินงานการ<br class="show-xs">ลดก๊าซเรือนกระจก</div>
                <div class="pick--date flex" inviewSpy="100">
                    <span class="f_bold">ตั้งแต่</span>
                    <div class="pick--since"></div>
                    <span class="f_bold">ถึง</span>
                    <div class="pick--to"></div>
                </div>
                <div class="company--name f_bold" inviewSpy="100"></div>
                <div class="wrap-logoes flex column-xs v-center" inviewSpy="100">
                    <div class="company--logo flex center">
                        <img src="" alt="" class="IMG">
                    </div>
                    <div class="climate--logo">
                        <img src="{$image_url}theme/default/public/images/dashboard/climate_collab_logo.png" alt="climate logo" class="IMG">
                        <p class="climate-txt t_dark">แพลต์ฟอร์มคำนวณการลดก๊าซเรือนกระจก <br>ตอบโจทย์ทุกองค์กร วัดผลเป็นรูปธรรม เริ่มได้ทันที</p>
                    </div>
                </div>
                <div class="climate-result" inviewSpy="100">
                    <div class="desc f_reg"><span class="company-name f_med">{$member.carethebear.company}</span> ได้ร่วมบริหารจัดการดูแลสิ่งแวดล้อมภายใต้</div>
                    <div class="climate--project--name f_reg t_yellow">Climate Care Collaboration Platform</div>
                    <div class="result-list flex center"></div>
                </div>
                <div class="text-to t_26 no-top f_med hide" inviewSpy="200">
                    โดยแยกเป็นผลการดำเนินงานในแต่ละโครงการ<br class="show-xs">ความร่วมมือดังนี้
                </div>
            </div>
            <div class="table-area"></div>
            <div class="wrap-download-btn" inviewSpy="300">
                <div class="button-download flex center">
                    <div class="download-btn" data-type="doc">
                        <div class="flex center">
                            <span class="icon-download"></span>
                            <div class="download-txt button_label f_med">DOC</div>
                        </div>
                    </div>
                    <div class="download-btn" data-type="pdf">
                        <div class="flex center">
                            <span class="icon-download"></span>
                            <div class="download-txt button_label f_med">PDF</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}