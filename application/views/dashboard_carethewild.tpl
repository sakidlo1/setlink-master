{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard-carethewild.css">
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: {if !empty($data_wild)}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };

        const planting_area = {json_encode($data_plant)};
    </script>
    <script>
    const elephant = [
        {foreach $company_wild as $company_wild_item}
            '{$company_wild_item.logo}',
        {/foreach}
    ];
    const dataOrganization = [
        ...elephant
    ];
    </script>
    <script src="{$image_url}theme/default/public/js/dashboard.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/dashboard_care-the-wild.js"></script>
    
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-wild flex column h-top v-center">
            {* <img class="bg column h-top v-center no-repeat" src="{$image_url}theme/default/public/images/dashboard_carethewild/wild_bg.png" alt="bgAlt"> *}
            <div class="wild-detail flex row h-center v-center">
                <div class="wrap-logoes flex column-xs v-center h-top" inviewSpy="0">
                    <img src="{$image_url}theme/default/public/images/dashboard_carethewild/wild_logo.png">
                </div>
                <div class="title f_bold" inviewSpy="100">โครงการ Care the Wild Plant & Protect<br><br>
                <span class="sub-title f_reg" inviewSpy="150">Platform ระดมทุนปลูกต้นไม้ให้ได้ผืนป่าทั้งปลูกป่าใหม่ปลูกเสริม และส่งเสริมการดูแลป่า ด้วยเป้าหมายต้นไม้ที่ปลูกต้องรอดและเติบโต 100% และมีการติดตามดูแลอย่างมีส่วนร่วมตลอด 10 ปี เพื่อรักษาสมดุลของสิ่งแวดล้อม</span></div>
                

                
            </div>
            {* <div class="head f_bold" inviewspy="400">Climate Care Platform ร่วมกับ<br>องค์การบริหารก๊าซเรือนกระจก (อบก.)</div>
                <div class="text-data f_reg" inviewSpy="500">สนับสนุนให้ทุกองค์กรที่เป็นพันธมิตรดำเนินโครงการภายใต้ Climate Care Platform 
                สามารถนำผลการดำเนินงานจากโครงการ Care the Whale โครงการลดก๊าซเรือนกระจก
                จากการบริหารจัดการขยะ โครงการ Care the Wild โครงการปลูกป่าเพื่อดูดซับก๊าซเรือนกระจก
                มายืนขอรับรองผลการประเมินการลด หรือกักเก็บก๊าซเรือนกระจกจากโครงการ Less
                เพื่อรับใบประกาศเกียรติคุณที่ออกโดย อบก. ด้วยวิธีง่ายๆ ดังนี้</div> *}
            <div>
{*                <input id="data_plant" type="text" value="{json_encode($data_plant)}">*}
            </div>

            <div class="summary-wild flex column h-top v-center">
                <div class="content-info">
                    <div class="text-company f_bold" inviewSpy="550">{$data_wild[0]['companyTh']}</div>
                    <div class="content-summary">
                        <div class="content-icon" inviewSpy="600">
                            <div class="icon-cloud"></div>
                        </div>
                        <div class="head f_bold" inviewSpy="600">สามารถดูดซับ<br>ปริมาณก๊าซเรือนกระจก<br>จากการปลูกป่าได้</div>
                        <div class="wrap-result" inviewSpy="600">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/air-pollution.png"
                                alt="air-pollution">
                            <div class="text f_bold">{$data_wild[0]['donation_amount']}<br>
                            <span class="sub-text f_reg">กิโลกรัม<br>คาร์บอนไดออกไซด์เทียบเท่า</span></div>
                        </div>
                    </div>
                </div>
                <div class="content-location" inviewSpy="700">
                    <div class="content-icon">
                        <div class="icon-tree"></div>
                    </div>
                    <div class="text f_bold">พื้นที่ปลูกป่า</div>
                </div>

                {* <div class="img-section flex h-center swiper" inviewspy="200">
                <div class="swiper-wrapper"></div> *}
            </div>
            <div class="container_section5">
                <div class="content" >
                    <div class="header-section" style="display: none;">
                        {* <div class="flex h-justify in-head">
                            <div class="text-header f_bold " inviewspy="200">พื้นที่ปลูกป่า</div>
                            <a href="https://www.setsocialimpact.com/Article/Detail/77268" target="_blank"><button
                                    class="header-btn button f_med hid show-xs" inviewspy="200">ดูทั้งหมด</button></a>
                        </div> *}
                        <div class="flex" inviewspy="400">
                            <div class="simple-dropdown f_reg hide-placeholder " id="simple_dropdown_filter">
                                <input class="input-value">
                                {* <div class="dropdown-show-selected flex flex-v-c flex-r f_med" inviewspy="500">
                                    <span class="-text">พื้นที่ปลูกดำเนินการแล้ว</span>
                                    <span class="-icon icon-arrow-down"></span>
                                </div> *}
                                {* <div class="dropdown-section hide-serch">
                                    <ul>
                                        <li class="flex selected" data-value="processed">
                                            <span class="-text">พื้นที่ปลูกดำเนินการแล้ว </span>
                                        </li>
                                        <li class="flex" data-value="waited">
                                            <span class="-text">พื้นที่ปลูกที่รอรับการสนับสนุน </span>
                                        </li>
                                    </ul>
                                </div> *}
                            </div>
                            {* <a href="https://www.setsocialimpact.com/Article/Detail/77268" target="_blank"><button
                                    class="header-btn button f_med hide-xs" inviewspy="400">ดูทั้งหมด</button></a> *}
                        </div>
                    </div>

                    <div class="img-section flex h-center swiper" inviewspy="200">
                        <div class="swiper-wrapper"></div>
                    </div>
                </div>
            </div>


            
        </div>
    </div>
    
{/block}