{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css?v=1.0">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/modal.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/register.css?v=1.0">

{/block}
{block name=js}
   <script src="{$image_url}theme/default/public/js/chosen.jquery.min.js"></script>

    <script src="{$image_url}theme/default/public/js/form.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/upload-file.js"></script>
    <script src="{$image_url}theme/default/public/js/modal.js"></script>
    <script src="{$image_url}theme/default/public/js/register.js?v=1.0&_nc={time()}>"></script>
{/block}
{block name=body}
	<main class="content">
      <div class="page-register">
         <div class="container">
            <div class="page-header"> 
               <h1 class="page-header--title f_bold">Registration form</h1>
               <p class="page-header--description f_bold">Climate Care Platform</p>
            </div>
            <div class="container-forms">
               <div class="block-form">
                  <div id="form" class="common-form" autocomplete="new-password" novalidate>
                     <fieldset>
                        <div class="wrap-col-form flex wrap" >
                           <div class="group flex wrap" data-row="1">
                              <div class="col-form two" style="width:80%">
                                 <div class="wrap-input-text">
                                    <select name="company_name_chosen" class="chosen-select" id="company_name_chosen" readonly="">
                                       <option value="">--เลือก--</option>
                                       {foreach $company_lists as $company_item}
                                          <option class="" data-code="{$company_item['code']}" data-en="{$company_item['name_en']}" value="{$company_item['name_th']}">
                                             <span class="-text">{$company_item['name_th']}</span>
                                          </option>
                                       {/foreach}
                                    </select>
                                 </div>
                              </div>
                              <div class="col-form two" style="width:20%; padding-top:4px;">
                                 <div class="wrap-input-checkbox">
                                    <label class="flex" for="checkbox_other">
                                       <input class="input-type-checkbox" type="checkbox" name="company_other" id="checkbox_other" value="yes">
                                       <div class="checking-box">
                                          <span class="icon icon-check"></span>
                                       </div>
                                       <span class="checking-text flex wrap">บริษัทอื่นๆ
                                             </span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form">
                                 <div class="wrap-input-text type-dropdown">
                                    <div class="wrap-input-text">
                                       <input name="company_name" id="company_name" class="input-type-text f_reg input-required" autocomplete="new-password" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                       <label class="text-label placeholder flex v-center" for="company_name">
                                          <span class="txt-placeholder">ชื่อองค์กร / บริษัท</span>
                                       </label>
                                    </div>

                                    {*                                    <button type="button" class=" btn btn-secondary" name="other_company_name" data-modal="exampleModal" data-toggle="modal" data-target="#exampleModal">บริษัทอื่นๆ</button>*}

                                 </div>
                              </div>
                              <div class="col-form">
                                 <div class="wrap-input-text">
                                    <input name="company_name_en" id="company_name_en" class="input-type-text f_reg input-required" autocomplete="new-password" type="text" value="{$member.carethewhale.company}" required maxlength="100" data-error="กรุณากรอกข้อมูล" >
                                    <label class="text-label placeholder flex v-center" for="company_name">
                                       <span class="txt-placeholder">ชื่อองค์กร / บริษัท (EN)</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <!-- dropdown -->
                                 <div class="wrap-input-text type-dropdown">
                                    <div class="simple-dropdown f_reg" id="simple_dropdown_company_region">
                                       <input type="hidden" name="simple_dropdown_company_region" value="" class="input-value" data-error="">
                                       <div class="dropdown-show-selected flex v-center h-justify"><span class="-text"></span><span class="-icon icon-arrow-down2"></span></div>
                                       <div class="dropdown-placeholder">เลือกประเภทสำนักงาน</div>
                                       <div class="dropdown-section hide-serch">
                                          <ul>
                                             <li class="flex v-center h-justify f_reg" data-value="สำนักงานใหญ่">
                                                <span class="-text">สำนักงานใหญ่</span>
                                             </li>
                                             <li class="flex v-center h-justify f_reg" data-value="สาขา / โซน / จังหวัด">
                                                <span class="-text">สาขา / โซน / จังหวัด</span>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <!-- dropdown -->
                                 <div class="wrap-input-text type-dropdown">
                                    <div class="simple-dropdown f_reg" id="simple_dropdown_company_category">
                                       <input type="hidden" name="simple_dropdown_company_category" value="" class="input-value" data-error="">
                                       <div class="dropdown-show-selected flex v-center h-justify"><span class="-text"></span><span class="-icon icon-arrow-down2"></span></div>
                                       <div class="dropdown-placeholder">ประเภทองค์กร</div>
                                       <div class="dropdown-section hide-serch font-smaller">
                                          <ul>
                                             {foreach $company_type as $company_type_item}
                                                <li class="flex v-center h-justify f_reg" data-value="{$company_type_item.name}" data-readonly><span class="-text">{$company_type_item.name}</span></li>
                                                {foreach $company_type_item.item as $company_group_item}
                                                   <li class="flex v-center h-justify f_reg" data-value="{$company_group_item.company_type_id}-{$company_group_item.id}"><span class="-text">{$company_group_item.name}</span></li>
                                                {/foreach}
                                             {/foreach}
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="2">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text" id="company_short_name"  name="company_short_name" autocomplete="new-password" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="company_short_name">
                                       <span class="txt-placeholder">ชื่อย่อองค์กร (Symbol)</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="company_location" autocomplete="new-password" value="" required maxlength="200" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="company_location">
                                       <span class="txt-placeholder">โปรดระบุ สาขา / โซน / จังหวัด</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="3">
                              <!-- <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="company_tax_number" autocomplete="new-password" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="company_tax_number">
                                       <span class="txt-placeholder">เลขประจำตัวผู้เสียภาษี (สำหรับโครงการ Care the Wild)</span>
                                    </label>
                                 </div>
                              </div> -->
                           </div>
                           <div class="group flex wrap" data-row="4">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_first_name" autocomplete="new-password" value="{$member.name}" required maxlength="30" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_first_name">
                                       <span class="txt-placeholder">ชื่อ</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_last_name" autocomplete="new-password" value="{$member.surname}" required maxlength="30" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_last_name">
                                       <span class="txt-placeholder">นามสกุล</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="5">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_position" autocomplete="new-password" value="" required maxlength="50" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_position">
                                       <span class="txt-placeholder">ตำแหน่ง</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_department" autocomplete="new-password" value="" required maxlength="60" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_department">
                                       <span class="txt-placeholder">ฝ่ายงาน</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="6">
                              <div class="col-form">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_company_location" autocomplete="new-password" value="{$member.carethewhale.address}" required maxlength="150" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_company_location">
                                       <span class="txt-placeholder">ที่อยู่องค์กร</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="7">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_phone" autocomplete="new-password" value="{$member.carethewhale.tel1}" required maxlength="10" inputmode="numeric" data-error="กรุณากรอกข้อมูล,ข้อมูลไม่ถูกต้อง กรุณาระบุเบอร์โทรศัพท์ 9-10 หลัก,รูปแบบเบอร์โทรไม่ถูกต้อง กรุณาระบุเบอร์โทรขึ้นต้นด้วย 0">
                                    <label class="text-label placeholder flex v-center" for="customer_phone">
                                       <span class="txt-placeholder">เบอร์โทรศัพท์</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_phone_partner" autocomplete="new-password" value="{$member.carethewhale.tel2}" required maxlength="10" inputmode="numeric" data-error="กรุณากรอกข้อมูล,ข้อมูลไม่ถูกต้อง กรุณาระบุเบอร์โทรศัพท์ 9-10 หลัก,รูปแบบเบอร์โทรไม่ถูกต้อง กรุณาระบุเบอร์โทรขึ้นต้นด้วย 0">
                                    <label class="text-label placeholder flex v-center" for="customer_phone_partner">
                                       <span class="txt-placeholder">เบอร์มือถือ (ผู้ประสานงาน)   </span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="8">
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg input-required" type="text" name="customer_email" autocomplete="new-password" value="{$member.carethewhale.email}" required maxlength="50" inputmode="email" data-error="กรุณากรอกข้อมูล,รูปแบบอีเมลไม่ถูกต้อง">
                                    <label class="text-label placeholder flex v-center" for="customer_email">
                                       <span class="txt-placeholder">อีเมล</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <!--- <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_account_name" autocomplete="new-password" value="" required maxlength="50" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_account_name">
                                       <span class="txt-placeholder">ผู้ใช้งาน (Username)</span>
                                    </label>
                                 </div>
                              </div> ---!>
                           </div>
                           <div class="group flex wrap" data-row="9">
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg input-required" type="password"  name="customer_account_password" autocomplete="new-password" value="" required maxlength="50" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_account_password">
                                       <span class="txt-placeholder">รหัสผ่าน (Password)</span>
                                    </label>
                                    <span class="ic-eye icon-eye"></span>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg input-required" type="password"  name="customer_account_confirm_password" autocomplete="new-password" value="" required maxlength="50" data-error="กรุณากรอกข้อมูล,ยืนยันรหัสผ่านไม่ตรงกัน">
                                    <label class="text-label placeholder flex v-center" for="customer_account_confirm_password">
                                       <span class="txt-placeholder">ยืนยันรหัสผ่าน (Password)</span>
                                    </label>
                                    <span class="ic-eye icon-eye"></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="block-form" inviewSpy="300">
                  <div id="upload" class="common-form">
                     <fieldset>
                        <h2 class="upload-title register-form-title f_bold">อัพโหลด LOGO</h2>
                        <div class="wrap-upload flex column">
                           <div class="upload-wrap flex column h-center">
                              <div class="upload-area flex v-center h-justify show">
                                 <span class="block-upload-icon">
                                    <span class="icon icon-cloud-arrow-up"></span>
                                 </span>
                                 <div class="block-upload-desc flex column">
                                    <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                    <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 1 ไฟล์</span>
                                    <input type="file" name="customer_logo" id="customer_logo" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf">
                                 </div>
                              </div>
                              <div class="upload-preview">
                                 <span class="preview-wrap"></span>
                              </div>
                           </div>
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="block-form" inviewSpy="300">
                  <div id="projects" class="common-form">
                     <fieldset class="block-project">
                        <h2 class="projects-title register-form-title f_bold">เลือกโครงการ</h2>
                        <div class="wrap-projects">
                           <div class="wrap-input-checkbox flex v-center h-justify">
                              <label class="flex column v-center">
                                 <input class="input-type-checkbox f_reg input-required" type="checkbox" name="customer_select_project" value="bear">
                                 <div class="container-project" data-project="bear">
                                    <div class="project-graphic">
                                       <img class="project-graphic--image" src="{$image_url}theme/default/public/images/mascot/bear.svg" alt="Care the Bear">
                                    </div>
                                    <p class="project-name f_bold">Care the Bear</p>
                                 </div>
                                 <div class="checking-box">
                                    <span class="icon icon-check"></span>
                                 </div>
                              </label>
                              {if $member.is_whale != 'Y'}
                                  <label class="flex column v-center">
                                     <input class="input-type-checkbox f_reg input-required" type="checkbox" name="customer_select_project" value="whale">
                                     <div class="container-project" data-project="whale">
                                        <div class="project-graphic">
                                           <img class="project-graphic--image" src="{$image_url}theme/default/public/images/mascot/whale.svg" alt="Care the whale">
                                        </div>
                                        <p class="project-name f_bold">Care the whale</p>
                                     </div>
                                     <div class="checking-box">
                                        <span class="icon icon-check"></span>
                                     </div>
                                  </label>
                              {/if}
                              {*
                              <label class="flex column v-center">
                                 <input class="input-type-checkbox f_reg input-required" type="checkbox" name="customer_select_project" value="wild">
                                 <div class="container-project" data-project="wild">
                                    <div class="project-graphic">
                                       <img class="project-graphic--image" src="{$image_url}theme/default/public/images/mascot/elephant.svg" alt="Care the Wild">
                                    </div>
                                    <p class="project-name f_bold">Care the Wild</p>
                                 </div>
                                 <div class="checking-box">
                                    <span class="icon icon-check"></span>
                                 </div>
                              </label>
                              *}
                           </div>
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="block-form" inviewSpy="300">
                  <div id="agreement" class="common-form">
                     <fieldset class="block-agreement">
                        <div class="wrap-input-checkbox">
                           <label class="flex" for="checkbox_consent">
                              <input class="input-type-checkbox f_reg input-required" type="checkbox" name="customer_accept_consent" id="checkbox_consent" value="T">
                              <div class="checking-box">
                                    <span class="icon icon-check"></span>
                              </div>
                              <span class="checking-text flex wrap">
                                 <span>ข้าพเจ้าขอยืนยันว่าข้าพเจ้าได้อ่านและเข้าใจข้อตกลงในการใช้บริการเว็บไซต์ </span>
                                 <a class="link-website" href="https://climatecare.setsocialimpact.com" rel="noreferrer" target="_blank">https://climatecare.setsocialimpact.com</a>
                                 <a class="link-consent" href="#" data-modal="privacy-notice"><u>อ่านข้อตกลงที่นี่</u></a>
                              </span>
                           </label>
                        </div>
                     </fieldset>
                  </div>
               </div>
            </div>
            <div class="wrap-action" inviewSpy="300">
               <div class="button btn-action submit-form">
                  <span class="button_label f_med">Submit</span>
               </div>
            </div>

         </div>
      </div>
   </main>


{*   <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>*}
{*   <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>*}



{/block}