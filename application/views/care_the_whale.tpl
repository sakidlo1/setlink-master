{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/care-the-whale.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/whale_banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/whale_area.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/whale_participant.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/contact-section.css">

    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/bootstrap-timepicker.min.css">
    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/aos.css">

    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/main.css?ver=1701855139">
    <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/responsive.css?ver=1701855139">
        
{/block}
{block name=js}
    <script>
    const whale = [
        {foreach $company_whale as $company_whale_item}
            '{$company_whale_item.logo}',
        {/foreach}
    ];

    let dataNews = [
        {foreach $news as $news_item}
            {
                date: '{$news_item.created_on|datestring:'th':'date':'short'}',
                title: '{$news_item.name}',
                link: base_url + 'news/detail/{$news_item.id}',
                image: '{$news_item.thumbnail}',
                imageAlt: '{$news_item.name}'
            },
        {/foreach}
    ];
    
    const dataOrganization = [
        ...whale
    ];

    
    </script>
    <script src="{$image_url}theme/default/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/public/js/whale-3m.js"></script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/contact-section.js"></script>

    {* JS Carethebear *}
    {* <script src="{$image_url}theme/ctb/assets/js/jquery.min.js"></script> *}
    <script src="{$image_url}theme/ctb/assets/js/owl.carousel.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/main.js?ver={$smarty.now}"></script>

    
{/block}
{block name=body}
	<div class="banner-care-the-whale">
        <div class="flex h-center max-width-banner">
            <div class="img-in-banner" inviewspy="200">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/banner/banner.png" alt="">
            </div>
            <div class="content-banner-right">
                <img src="{$image_url}theme/default/public/images/care-the-whale/banner/logo.svg" alt="" inviewspy>
                <span class="title-banner f_bold" inviewspy="100">Care the Whale ขยะล่องหน</span>
                <span class="desc-banner" inviewspy="200">
                    มีเป้าหมายลดก๊าซเรือนกระจกด้วยการบริหาร<br class="hide-xs">จัดการขยะ ด้วยแนวคิด “ขยะล่องหน” กำจัดคำว่าขยะให้
                    หายไป ร่วมกันหาทางใช้ให้ถึงที่สุด โดยใช้หลักเศรษฐกิจหมุนเวียน หรือ Circular Economy มาใช้เพื่อดูแลและ<br class="show-375">จัดการขยะให้มีทางไปที่ถูกต้อง ในการร่วมแก้ไขปัญหา<br class="show-375">
                    ขยะที่นำไปสู่ภาวะโลกร้อน โดยเครือข่ายพันธมิตร<br class="show-375">
                    มีเป้าหมายร่วมกันในเรื่อง <br class="show-375">Zero-waste to landfill             
            </div>
        </div>
    </div>
    <div class="section-area flex center">
        <div class="wrap-bg">
            <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/area/bg.jpg" alt="city">
        </div>
        <div class="left">
            <div class="area-title f_med">
                <p inviewspy>มีพื้นที่นำร่องถนนรัชดาภิเษกกับ</p>
                <div class="XL f_bold" inviewspy>14 องค์กรสถานประกอบการ</div>
            </div>
            <div class="area-desc" inviewspy>
                เป็นต้นแบบในการเรียนรู้วิธีคิด และบริหารจัดการขยะ ปัจจุบันโครงการได้ขยายการดำเนินงาน ประยุกต์แนวคิด และถอดบทเรียน ไปยังพื้นที่และชุมชนต่างๆ ได้แก่ <br class="show-940">ย่านเพลินจิต คลองเตย สาทร รามคำแหง รามอินทรา <br class="show-940">ศาลายา นนทบุรี คุ้งบางกะเจ้า สมุทรปราการ สมุทรสาคร สระบุรี ขอนแก่น เชียงใหม่ หาดใหญ่ นครสวรรค์ พิษณุโลก อุตรดิตถ์ พิจิตร ลำพูน อุทัยธานี ชุมพร ศรีสะเกษ อุบลราชธานี เป็นต้น <br class="show-940">บริษัท สถานประกอบการที่สนใจสามารถเข้าร่วมโครงการในการเริ่มบริหารจัดการขยะอย่างเป็นรูปธรรม
            </div>
        </div>
        <div class="right">
            <div class="wrap-whales">
                <div class="whale big" inviewspy data-area="ย่านรัชดาภิเษก">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/01.svg" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="คลองเตย">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/02.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="900" data-area="วัชรพล">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/03.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="900" data-area="ศาลายา">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/04.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="700" data-area="ย่านเพลินจิต">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/05.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="สาธร">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/06.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="900" data-area="รามอินทรา">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/07.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="600" data-area="สมุทรปราการ">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/08.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="600" data-area="พิจิตร">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/09.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="700" data-area="ชุมพร">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/10.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="600" data-area="สระบุรี">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/11.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="700" data-area="ศรีสะเกษ">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/12.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="อุทัยธานี">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/13.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="700" data-area="พิษณุโลก">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/14.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="200" data-area="อุบล">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/15.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="700" data-area="นนทบุรี">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/16.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="นครสววรค์">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/17.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="200" data-area="คุ้งบางกะเจ้า">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/18.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="600" data-area="รามคำแหง">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/19.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="300" data-area="ขอนแก่น">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/20.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="300" data-area="สมุทรสาคร">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/21.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="หาดใหญ่">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/22.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="200" data-area="เชียงใหม่">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/23.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="400" data-area="ลำพูน">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/24.png" alt="">
                    </div>
                </div>
                <div class="whale" inviewspy="900" data-area="อุตรดิตถ์">
                    <div class="wrap-img">
                        <img src="{$image_url}theme/default/public/images/care-the-whale/area/25.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-sec-3">
        <div class="container">
            <p class="title-sec-3 f_bold" inviewspy="200">การดำเนินโครงการที่เป็นรูปธรรม<br class="show-xs">ขับเคลื่อนด้วยกลยุทธ์ 3 M</p>
            <p class="desc-sec-3" inviewspy="200">วัดผลค่าการลดก๊าซเรือนกระจกได้ทันทีตอบโจทย์ความยั่งยืนของทุกองค์กร</p>
            <div class="chart" inviewspy="200">
                <div class="wrap-border">
                    <img src="{$image_url}theme/default/public/images/care-the-whale/sec-3/chart-border.png" alt="chart-border">
                    <div class="wrap-3M">
                        <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/sec-3/recycle-bin.png" alt="recycle-bin">
                        <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/sec-3/container.png" alt="container">
                        <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/sec-3/garbage-truck.png" alt="garbage-truck">
                        <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/sec-3/trash.png" alt="trash">
                    </div>
                </div>
            </div>
            <div class="instruction-3M flex h-justify column-xs">
                <div class="instruction" inviewspy="200">
                    <div class="inst-head f_bold">Module</div>
                    <div class="inst-body">สถานประกอบการกำหนดนโยบาย กลยุทธ์ <br class="show-xs">แผนการดำเนินการ “ลด เก็บ แยก จัดการ” <br class="show-xs">ขยะหรือของเสียแต่ละประเภท จัดตั้งหน่วยงาน<br class="show-xs">รับผิดชอบระบบงานด้านสิ่งแวดล้อม <br class="show-xs">การรายงาน และวิเคราะห์การประเมินผล</div>
                </div>
                <div class="instruction" inviewspy="300">
                    <div class="inst-head f_bold">Monitor</div>
                    <div class="inst-body">ตลาดหลักทรัพย์ฯ สนับสนุนเครื่องมือบริหารจัดการข้อมูลในการคัดแยกขยะ <br class="hide-xs">และเครื่องมือคำนวณการวัดผลการลดการปล่อยก๊าซเรือนกระจก ให้กับสถานประกอบการ เพื่อนำข้อมูลมาวิเคราะห์<br class="hide-xs">และประมวลผล เพื่อการปรับแผนงานในวงจรที่เกี่ยวข้อง</div>
                </div>
                <div class="instruction" inviewspy="400">
                    <div class="inst-head f_bold">Multiply</div>
                    <div class="inst-body">สถานประกอบการและตลาดหลักทรัพย์ฯ ร่วมกันเผยแพร่การดำเนินงาน <br class="hide-xs">การลดก๊าซเรือนกระจก พร้อมทั้งรณรงค์ ร่วมเผยแพร่องค์ความรู้การจัดการขยะ เพื่อร่วมกันขยายผลลัพธ์ในวงกว้าง</div>
                </div>
            </div>
        </div>
    </div>
    <div class="whale-privilege">
        <div class="wrap-bg">
            <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-bg.jpg" alt="bg">
        </div>
        <div class="container">
            <span class="title-sec-3 f_bold" inviewspy="300">สิทธิประโยชน์<br class="d-md-none">การเข้าร่วมโครงการ Care the Whale</span>
            <div class="box-content flex v-top h-center wrap">
                <div class="single-content" inviewspy="100">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-01.png" alt="">
                    <span class="text-content f_med">คำนวณ วัดผล ปริมาณการ<br> ลดก๊าซเรือนกระจกได้ทันที</span>
                </div>
                <div class="single-content" inviewspy="150">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-02.png" alt="">
                    <span class="text-content f_med">ผลการดำเนินงานนำไปแสดงใน<br class="hide-940"> Annual Report หรือ <br>56-1 One Report ได้</span>
                </div>
                <div class="single-content" inviewspy="200">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-03.png" alt="">
                    <span class="text-content f_med">นำผลการดำเนินงานยื่นขอใบ<br class="d-md-block"> ประกาศเกียรติคุณ LESS จาก<br class="d-md-block"> องค์การบริหารจัดการก๊าซเรือน<br class="hide-940">กระจก (องค์การมหาชน) </span>
                </div>
                <div class="single-content" inviewspy="250">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-04.png" alt="">
                    <span class="text-content f_med">เกิดภาพลักษณ์ที่ดีต่อองค์กร <br class="show-940">สอดรับ การดำเนินงานระดับ Global Brand ที่ส่งเสริมการดูแลสิ่งแวดล้อมอย่างเป็นรูปธรรม</span>
                </div>
                <div class="single-content" inviewspy="300">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-05.png" alt="">
                    <span class="text-content f_med">เกิดการสร้างจิตสำนึก การมี<br class="show-940">ส่วน<br class="d-md-block">ร่วมในการดูแลด้านสิ่งแวดล้อม<br class="d-md-block"> ของพนักงานในองค์กร</span>
                </div>
                <div class="single-content" inviewspy="350">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-06.png" alt="">
                    <span class="text-content f_med">แลกเปลี่ยนองค์ความรู้ เรียนรู้<br class="hide-940">ดูงานกับเครือข่ายและสมาชิกอย่างต่อเนื่อง</span>
                </div>
                <div class="single-content" inviewspy="400">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/privilege/whale-privilege-07.png" alt="">
                    <span class="text-content f_med">เกิดเครือข่ายการทำงานร่วมกัน</span>
                </div>
            </div>
        </div>
    </div>
    <div class="section-participant">
        <div class="wrap-bg">
            <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-whale/participant/bg.png" alt="windy bg">
        </div>
        <div class="max-width-participant flex h-justify v-center">
            <div class="left">
                <div class="title f_bold" inviewspy="300">การเข้าร่วมโครงการ</div>
                <div class="desc" inviewspy="600">ไม่จำกัดเฉพาะว่าจะต้องเป็นหน่วยงาน หรือสถานประกอบการ ในพื้นที่ต่างๆ แต่สามารถเป็นหน่วยงาน องค์กร ที่ดำเนินการด้าน Circular Economy หรือการมี Know-How ความรู้เรื่องการบริหารจัดการขยะ เพื่อมาร่วมเป็นเครือข่ายทำงานร่วมกันกับสมาชิกในโครงการ</div>
            </div>
            <div class="right">
                <img class="lazy-img top-img" inviewspy="300" data-src="{$image_url}theme/default/public/images/care-the-whale/participant/infinity.png" alt="infinity">
                <img class="lazy-img bottom-img" inviewspy="600" data-src="{$image_url}theme/default/public/images/care-the-whale/participant/bins.png" alt="bins">
            </div>
        </div>
    </div>
    <div class="section-performance">
        <div class="container">
            <span class="title-performance" inviewspy="300">ผลการดำเนินงานโครงการ</span>
            <span class="desc-performance" inviewspy="600">ตั้งแต่ปี 2019 - ปัจจุบัน</span>
            <div class="wrapper-content">
                <div class="left-img" inviewspy="300">
                    <img src="{$image_url}theme/default/public/images/care-the-whale/performance/manage_rubbish.png" alt="">
                </div>
                <div class="right-content">
                    <div class="list-items" inviewspy>
                        <span class="icon-building"></span>
                        <div class="text-list-items">
                            <span class="text-1">สมาชิกหรือลูกบ้านในเครือข่าย</span>
                            <div>
                                <span class="text-2" id="run-num" data-no="{$whale_stats.total_company|replace:',':''}"></span>
                                <span class="text-3">องค์กร</span>
                            </div>
                        </div>
                    </div>
                    <div class="list-items" inviewspy>
                        <span class="icon-recycle-bin"></span>
                        <div class="text-list-items">
                            <span class="text-1">ปริมาณขยะที่บริหารจัดการรวม</span>
                            <div>
                                <span class="text-2" id="run-num" data-no="{(($whale_stats.total_weight|replace:',':''))|number_format:2:".":""}"></span>
                                <span class="text-3">กิโลกรัม</span>
                            </div>
                        </div>
                    </div>
                    <div class="list-items" inviewspy="200">
                        <span class="icon-cloud"></span>
                        <div class="text-list-items">
                            <span class="text-1">ลดปริมาณก๊าซเรือนกระจกได้</span>
                            <div>
                                <span class="text-2" id="run-num" data-no="{(($whale_stats.total_cf|replace:',':''))|number_format:2:".":""}"></span>
                                <span class="text-3 ">Kg
                                    <span class="hanging-co2-1">CO </span>
                                    <span class="text-indent-eq">eq</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="list-items" inviewspy="400">
                        <span class="icon-tree"></span>
                        <div class="text-list-items">
                            <span class="text-1">เทียบเท่าการดูดซับ 
                                <span class="hanging-co2-2">CO</span> 
                                <span class="text-indent-per-year">/ ปี ของต้นไม้</span>
                            </span>
                            <div>
                                <span class="text-2" id="run-num" data-no="{$whale_stats.total_tree|replace:',':''}"></span>
                                <span class="text-3">ต้น</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="main-sec-3">
            <div class="container">
                <h2 class="title-sec-3 f_bold">ข่าวและประชาสัมพันธ์ (News)</h2>
                 <div class="slide-article owl-carousel" style="margin-bottom: 30px;">
                    {foreach $news as $item}
                        <div class="article item">
                            <div class="head">
                                <span>{$item.name}</span>
                                <a href="{$base_url}whale_news/detail/{$item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            <div class="text">
                                {$item.description|nl2br}
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="wrap_btn" style="text-align: right;">
                    <a href="{$base_url}whale_news" class="button shadow">ดูทั้งหมด</a>
                </div>
            </div>
        </div>
        </section>

        
        <section class="main-tip">
        <div class="container">
            <h2 class="title">Tips <i class="icon-circle"></i></h2>
            <div class="slide-home-tips owl-carousel">
                {foreach $tips as $item}
                    <a href="{$base_url}tips/detail/{$item.id}">
                        <div class="item">
                            <div class="img">
                                <img src="{$item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            </div>
                        </div>
                    </a>
                {/foreach}
            </div>
            <br/>
            <div class="see-all">
                <a href="{$base_url}tips" class="button shadow">ดูทั้งหมด</a>
            </div>
        </div>
    	</section>
        
        
    <div class="slide-wal"></div>
    <div id="contact-html"></div>
{/block}
{block name=script}
    
    <script>
        {literal}
        function input_format(value, dec)
        {
            value = value.replace(/\,/g,"");
            if(value != "")
            {
                if(dec == 0)
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                }
                else
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                }
            }
            else
            {
                return '';
            }
        }
        {/literal}

    </script>
    
	<script src="{$image_url}theme/default/public/js/care-the-whale.js"></script>
    <script src="{$image_url}theme/default/public/js/care-the-bear.js"></script>

    <script src="{$image_url}theme/ctb/assets/js/jquery.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/bootstrap.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/owl.carousel.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/bootstrap-datepicker.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/bootstrap-timepicker.min.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/aos.js"></script>
    <script src="{$image_url}theme/ctb/assets/js/main.js?ver=1701854516"></script>

    <script>
    jQuery.noConflict();

    jQuery(document).ready(function($) {

      $('#owl-carousel').owlCarousel({
        items: 3,
        loop: true,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 3000,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          }
        }
      });
    });
  </script>
    
{/block}