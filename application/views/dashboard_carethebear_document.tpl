{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard-carethebear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/menu-bear.css">
    {* <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/aos.css">
    <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/main.css?ver={$smarty.now}">
    <link rel="stylesheet" type="text/css" href="{$base_url}carethebear/images/theme/default/assets/css/responsive.css?ver={$smarty.now}"> *}
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: false,
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };
    </script>
    <script src="{$image_url}theme/default/public/js/dashboard.js?v=1.0"></script>
    {* <script src="{$base_url}carethebear/images/theme/default/assets/js/aos.js"></script> *}
    {* <script src="{$base_url}carethebear/images/theme/default/assets/js/main.js?ver={$smarty.now}"></script> *}
    
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-summary flex column h-top v-top h-top">
            <div style="z-index: 10;" class="menu-bear" inviewspy>
                <div class="wrap-menu">
                    <a class="menu-link f_med" href="{$base_url}carethebear">Care the Bear</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/about">What’s Care the Bear</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/activity">Activities & Event</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/project">Climate Action Project</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/article">Articles</a>
                    <a class="menu-link f_med" href="{$base_url}carethebear/news">News</a>
                </div>
            </div> 
            <div class="global-warming-detail" inviewSpy="200">
                <div class="title f_bold">ผลรวมลดปริมาณก๊าซเรือนกระจก <br class="show-xs">และเทียบเท่าการปลูกต้นไม้
                </div>
                <div class="detail-wrapper flex column-xs">
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/air-pollution.png"
                                alt="air-pollution">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">ลดปริมาณก๊าซเรือนกระจกได้</p>
                            <span>
                                <span class="stats-value f_bold" data-global="gas" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">Kg CO<sub>2</sub>e</span>
                            </span>
                        </div>
                    </div>
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/trees.png" alt="trees">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">เทียบเท่าการปลูกต้นไม้ใหญ่ อายุ 10 ปี</p>
                            <span>
                                <span class="stats-value f_bold" data-global="tree" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">ต้น</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrap-bear flex row h-justify v-top" inviewSpy="300">
                <div class="bear-grap flex row h-center v-center">
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg"></img>
                    <div class="wrap_character char_bear" id="banner" inviewspy>
                            <div class="character">
                                <img class="lazy-img bear"
                                    data-src="{$image_url}theme/default/public/images/home/banner/bear.png" alt='bear'>
                                <img class="lazy-img sign"
                                    data-src="{$image_url}theme/default/public/images/home/banner/no-foam-sign.png"
                                    alt='bear sign'>
                            </div>
                    </div>
                </div>
                <div class="wrap-title f_bold">
                <span class="head f_bold">การจัดการกิจกรรมและโครงการ</span><br>
                       <span class="super f_bold">Care the Bear</span>
                </div>
            </div>
            
            <div class="member-actions" inviewSpy="300">
                <div class="wrap-menu">
                    <a href="{$base_url}dashboard/carethebear_activity" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                        <span class="right f_bold">กิจกรรม</span></a>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                    <div class="dropdown">
                        <div class="dropdown-content">
                            <div class="head flex v-center h-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                <span class="right f_bold">สร้างกิจกรรม</span>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}carethebear/member/activity_add">สร้างกิจกรรมใหม่</a>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}dashboard/carethebear_activity">กิจกรรมที่ดำเนินการ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrap-menu">
                    <a href="{$base_url}dashboard/carethebear" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-project-b.svg">
                        <span class="right f_bold">โครงการ</span></a>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                    <div class="dropdown">
                        <div class="dropdown-content">
                            <div class="head flex v-center h-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-activity-b.svg">
                                <span class="right f_bold">สร้างโครงการ</span>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}carethebear/member/project_add">สร้างโครงการใหม่</a>
                            </div>
                            <div class="list-content flex v-center">
                                <img src="{$image_url}theme/default/public/images/dashboard_carethebear/user-edit.svg">
                                <a href="{$base_url}dashboard/carethebear">โครงการที่ดำเนินการ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{$base_url}dashboard/carethebear_report" class="wrap-menu">
                    <div class="inner">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-report-b.svg">
                        <span class="right f_bold">รายงาน / ผลลัพธ์</span></div>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                    <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow.svg"></img>
                </a>
                <a href="#" class="wrap-menu">
                    <div class="inner-select">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-document-wh.svg">
                        <span class="right f_bold">คู่มือและสื่อเผยแพร่</span></div>
                    <img class="transparent" src="{$image_url}theme/default/public/images/dashboard_carethebear/circle_transparent.svg">
                        <img class="arrow" src="{$image_url}theme/default/public/images/dashboard_carethebear/arrow-hover.svg"></img>
                </a>
            </div>

            <div class="member-box" >
                <h2 class="title">{$member.company}</h2>
                        {*<h2 class="title f_bold">ทดสอบ</h2>*}
                    {if $success_msg != ""}
                    <div class="alert alert-success">
                        {$success_msg}
                    </div>
                    {/if}
                    {if $error_msg != ""}
                    <div class="alert alert-danger">
                        {$error_msg}
                    </div>
                    {/if}
                    {* <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/member-activity.png"> *}
                    <h3 class="activity-head title f_bold">
                        <img src="{$image_url}theme/default/public/images/dashboard_carethebear/icon-document-b.svg">
                            <span>คู่มือและสื่อเผยแพร่</span>
                        </h3>
                    <div class="activity-control">
                        
                        {* <a href="{$base_url}carethebear/member/project_add" class="activity-add f_bold">เพิ่มโครงการ <img src="{$base_url}carethebear/images/theme/default/assets/images/icon-add.png"></a> *}
                        <div class="clearfix"></div>
                        <div class="header-list">
                            <div class="header-50 flex h-justify v-center" style="width: 70%;">รายละเอียด</div>
                            <div class="header-40 flex h-justify v-center" style="width: 35%;">
                                <div class="date">วันที่เผยแพร่</div>
                                <div class="download_doc flex h-top v-center">Download<img src="{$base_url}carethebear/images/theme/default/assets/images/icon-download2.png"></div>
                            </div>
                        </div>

                        <div class="activity-list">
                            {* {if $list|count > 0} *}
                                {foreach $document as $key => $document_item}
                                    <div class="inner">
                                        {* <div class="pull-right">
                                            <a class="action" href="https://api.qrserver.com/v1/create-qr-code/?data={$base_url}carethebear/project/detail/{$item.id}&amp;size=300x300" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i>
                                        </div> *}
                                        {* <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p> *}
                                        {* <p style="padding-left: 50px;">{$item.start_date|date_thai:"%e %B %Y"} - {$item.end_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p> *}
                                        <div class="actions">
                                            <span class="action" style="width: 5%; display: inline-block; text-align: right; ">{($key + 1)}.</span>
                                            <a class="action item1" style="width: 70%;"  href="{$document_item.file}" download>{$document_item.name}</a>
                                            <p class="action item2" style="width: 30%; font-size: 22px!important;"><img src="{$image_url}theme/default/public/images/dashboard_carethebear/date-icon.svg">{$document_item.updated_on} {*({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})*}</p>
                                           
                                            <a  class="action item5" style="align-items: center; width: 15%; color: #3F8CFF;"  href="{$document_item.file}"  download>Download<img style="width: 30px; padding-left: 5px;margin: 0 15px 0 0;" src="{$image_url}theme/default/public/images/dashboard_carethebear/download-b.svg"></a>
                                        </div>
                                    </div>
                                {/foreach}
                            {* {else}
                                <div class="inner">
                                    <center>ไม่มีข้อมูล</center>
                                </div>
                            {/if} *}
                        </div>
                        {* {include file='paging.tpl'} *}
                    </div>
            </div>

            {* <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        <h2 class="title f_bold">ทดสอบ</h2>
                        <img class="img-cover" src="{$base_url}carethebear/images/theme/default/assets/images/media.png">
                        <div class="media-control">
                            <h3 class="title f_med">เอกสารเผยแพร่โครงการ Care the Bear</h3>
                            {foreach $document as $document_item}
                                <div class="media-box">
                                    <div class="col">
                                        <a class="name" href="{$document_item.file}" download>{$document_item.name}</a>
                                    </div>
                                    <div class="col">
                                        <a class="date" href="{$document_item.file}" download>{$document_item.update_on}</a>
                                    </div>                                    
                                    <div class="col">
                                        <a class="detail" href="{$document_item.file}" download>รายละเอียดไฟล์</a>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
             </div> *}
        </div>
    </div>
{/block}