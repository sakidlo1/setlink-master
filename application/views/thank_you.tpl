{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/wild_ty.css">
{/block}
{block name=body}
	<div class="container">
        <div class="bg">
            <!-- <img src="{$image_url}theme/default/public/images/wild/04_Care_the_wild_thank_you_desktop.jpg" class="hide-xs" alt="bg_care_the_wild">
            <img src="{$image_url}theme/default/public/images/wild/04_Care_the_wild_thank_you_moblie.jpg" class="show-xs" alt="bg_care_the_wild"> -->
            <div class="box">
                <div class="in-box">
                    <div class="content">
                    <div>
                        <img src="{$image_url}theme/default/public/images/wild/logo_Care_the_wlid.svg" class="in-box-img" alt="logo_care_the_wild">
                    </div>
                    <div class="text">
                        <div class="f_bold text-item f_bold">ขอบคุณที่ลงทะเบียนกับ<br>โครงการ <span class="text-highlight">Care the wild</span></div>
        
                    </div>
                    
                    <div class="btn">
                        <a href="{$base_url}" class="in-btn btn-text button f_med">ตกลง</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
{/block}