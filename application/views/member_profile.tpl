{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css?v=1.0">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/modal.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/register.css?v=1.0">
{/block}
{block name=js}
    <script src="{$image_url}theme/default/public/js/form.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/upload-file.js"></script>
    <script src="{$image_url}theme/default/public/js/modal.js"></script>
###   <script src="{$image_url}theme/default/public/js/profile.js?v=1.0"></script>
{/block}
{block name=body}
	<main class="content">
      <div class="page-register">
         <div class="container">
            <div class="page-header"> 
               <h1 class="page-header--title f_bold">Edit profile form</h1>
               <p class="page-header--description f_bold">Climate Care Platform</p>
            </div>
            <div class="container-forms">
               <div class="block-form">
                  <div id="form" class="common-form" autocomplete="new-password" novalidate>
                     <fieldset>
                        <div class="wrap-col-form flex wrap" >
                           <div class="group flex wrap" >
                              <div class="col-form ">
                                 <div class="wrap-input-text">
                                    <input name="company_name" class="input-type-text f_reg input-required" disabled autocomplete="new-password" type="text" value="{$item.company}" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="company_name">
                                       <span class="txt-placeholder">ชื่อองค์กร / บริษัท</span>
                                    </label>
                                 </div>
                              </div>
                              
                           </div>
                   
                        
                           <div class="group flex wrap" data-row="4">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" disabled type="text"  name="customer_first_name" autocomplete="new-password" value="{$member.name}" required maxlength="30" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_first_name">
                                       <span class="txt-placeholder">ชื่อ</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" disabled type="text"  name="customer_last_name" autocomplete="new-password" value="{$member.surname}" required maxlength="30" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_last_name">
                                       <span class="txt-placeholder">นามสกุล</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="5">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_position" autocomplete="new-password"  value="{$item.position}" required maxlength="50" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_position">
                                       <span class="txt-placeholder">ตำแหน่ง</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_department" autocomplete="new-password" value="{$item.department}" required maxlength="60" data-error="กรุณากรอกข้อมูล" >
                                    <label class="text-label placeholder flex v-center" for="customer_department" >
                                       <span class="txt-placeholder">ฝ่ายงาน</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="group flex wrap" data-row="6">
                              <div class="col-form">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required"  type="text"  name="customer_company_location" autocomplete="new-password" value="{$item.address}" required maxlength="150" data-error="กรุณากรอกข้อมูล">
                                    <label class="text-label placeholder flex v-center" for="customer_company_location">
                                       <span class="txt-placeholder">ที่อยู่องค์กร</span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="7">
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_phone"  value="{$item.tel}" required maxlength="10" inputmode="numeric" data-error="กรุณากรอกข้อมูล,ข้อมูลไม่ถูกต้อง กรุณาระบุเบอร์โทรศัพท์ 9-10 หลัก,รูปแบบเบอร์โทรไม่ถูกต้อง กรุณาระบุเบอร์โทรขึ้นต้นด้วย 0">
                                    <label class="text-label placeholder flex v-center" for="customer_phone">
                                       <span class="txt-placeholder">เบอร์โทรศัพท์</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text">
                                    <input class="input-type-text f_reg input-required" type="text"  name="customer_phone_partner" value="{$item.mobile}" required maxlength="10" inputmode="numeric" data-error="กรุณากรอกข้อมูล,ข้อมูลไม่ถูกต้อง กรุณาระบุเบอร์โทรศัพท์ 9-10 หลัก,รูปแบบเบอร์โทรไม่ถูกต้อง กรุณาระบุเบอร์โทรขึ้นต้นด้วย 0">
                                    <label class="text-label placeholder flex v-center" for="customer_phone_partner">
                                       <span class="txt-placeholder">เบอร์มือถือ (ผู้ประสานงาน)   </span>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="group flex wrap" data-row="8">
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg input-required"  type="text" name="customer_email" autocomplete="new-password" value="{$item.email}" required maxlength="50" inputmode="email" data-error="กรุณากรอกข้อมูล,รูปแบบอีเมลไม่ถูกต้อง">
                                    <label class="text-label placeholder flex v-center" for="customer_email">
                                       <span class="txt-placeholder">อีเมล</span>
                                    </label>
                                 </div>
                              </div>
                              <div class="col-form two">
                     
                              </div>
                           </div>
                          
                          <div class="group flex wrap" data-row="9">
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg " type="password"  name="customer_account_password" autocomplete="new-password" value=""  maxlength="50">
                                    <label class="text-label placeholder flex v-center" for="customer_account_password">
                                       <span class="txt-placeholder">รหัสผ่านใหม่ (Password)</span>
                                    </label>
                                    <span class="ic-eye icon-eye"></span>
                                 </div>
                              </div>
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg" type="password"  name="customer_account_confirm_password" autocomplete="new-password" value=""  maxlength="50" data-error="ยืนยันรหัสผ่านไม่ตรงกัน">
                                    <label class="text-label placeholder flex v-center" for="customer_account_confirm_password">
                                       <span class="txt-placeholder">ยืนยันรหัสผ่านใหม่ (Password)</span>
                                    </label>
                                    <span class="ic-eye icon-eye"></span>
                                 </div>
                              </div>
                           </div> 
                           <div class="group flex wrap" data-row="10">
                              <div class="col-form two">
                                 <div class="wrap-input-text have-icon">
                                    <input class="input-type-text f_reg" type="password"  name="customer_old_password" autocomplete="old-password" value=""  maxlength="50" data-error="ยืนยันรหัสเก่า">
                                    <label class="text-label placeholder flex v-center" for="customer_old_password">
                                       <span class="txt-placeholder">ยืนยันรหัสเก่า (Password)</span>
                                    </label>
                                    <span class="ic-eye icon-eye"></span>
                                 </div>
                              </div>
                           </div> 
                        </div>
                     </fieldset>
                  </div>
               </div>
               <div class="block-form" inviewSpy="300">
                  <div id="upload" class="common-form">
                     <fieldset>
                        <h2 class="upload-title register-form-title f_bold">อัพโหลด LOGO</h2>
                        <img src="{$item.logo}" width="50">
                        <div class="wrap-upload flex column">
                           <div class="upload-wrap flex column h-center">
                              <div class="upload-area flex v-center h-justify show">
                                 <span class="block-upload-icon">
                                    <span class="icon icon-cloud-arrow-up"></span>
                                 </span>
                                 <div class="block-upload-desc flex column">
                                    <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                    <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 1 ไฟล์</span>
                                    <input type="file" name="customer_logo" id="customer_logo" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf">
                                 </div>
                              </div>
                              <div class="upload-preview">
                                 <span class="preview-wrap"></span>
                              </div>
                           </div>
                        </div>
                     </fieldset>
                  </div>
               </div>
      
            </div>
               <div class="wrap-action "></div>
               <div class="flex item-center justify-center text-center" inviewSpy="300">
                  <div class="button btn-action submit-form " name="save" value="save">
                     <span class="button_label f_med">บันทึก</span>
                  </div>
                    <div class="button btn-action submit-form-two" name="save" value="save">
                     <span class="button_label f_med">บันทึกและแก้ไข whale</span>
                  </div>
               </div>
           
         </div>
      </div>
   </main>
{/block}