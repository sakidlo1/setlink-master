{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/news-detail.css">
{/block}
{block name=js}
    <script>
        let newsWysiwyg = {
            title: '{$news_item.name}',
            date: '{$news_item.created_on|datestring:'th':'date':'short'}',
            banner_img: '{$news_item.image}',
            banner_img_alt: '{$news_item.name}',
            detail: '{$news_item.content|regex_replace:'/\>[\s\r\t\n]*\</':'><'}',
            tags: [
                {foreach $news_item.tags as $tags}
                    '{$tags}', 
                {/foreach}
            ],
            gallery: [
                {foreach $news_item.gallery as $gallery}
                    {
                        img: '{$gallery.image}',
                        img_alt: '{$gallery.description}'
                    },
                {/foreach}
            ]
        };
    </script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/news-detail.js"></script>
    <script>
        function loadCSS(e, n, o, t) { "use strict"; if (e.trim().length <= 0) { return; } var d = window.document.createElement("link"), i = n || window.document.getElementsByTagName("script")[0], s = window.document.styleSheets; return d.rel = "stylesheet", d.href = e, d.media = "only x", t && (d.onload = t), i.parentNode.insertBefore(d, i), d.onloadcssdefined = function (n) { for (var o, t = 0; t < s.length; t++)s[t].href && s[t].href.indexOf(e) > -1 && (o = !0); o ? n() : setTimeout(function () { d.onloadcssdefined(n) }) }, d.onloadcssdefined(function () { d.media = o || "all" }), d };
        function loadJS(e, a) { if (e.length <= 0) { return; } var t = document.getElementsByTagName("head")[0], n = document.createElement("script"); n.type = "text/javascript", n.src = e, n.async = !0, n.onreadystatechange = a, n.onload = a, t.appendChild(n) }
    </script>
{/block}
{block name=body}
	<div id="content">
        <h1 id="news-title" class="t_40 f_bold" data-remove="T" inviewspy="200"></h1>
        <div id="date" class="t_26" data-remove="T" inviewspy="200"></div>
        <div id="banner" data-remove="T" inviewspy="200"><div class="wrap-img"><img id="banner-img" src="" alt=""></div></div>
        <div id="inner-max-width">
            <div id="news-content" data-remove="T" class="default_css" inviewspy="200"></div>
            <div id="gallery" data-remove="T" inviewspy="200"></div>
            <div id="tags" data-remove="T" class="flex wrap" inviewspy="200"></div>
        </div>
        <div id="share" class="flex center" inviewspy="200"></div>
    </div>
{/block}