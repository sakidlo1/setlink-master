<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{block name=meta_title}{$page} - {$site_name}{/block}</title>
        <link rel="apple-touch-icon" sizes="180x180" href="{$image_url}theme/default/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="{$image_url}theme/default/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="{$image_url}theme/default/favicon-16x16.png" />
        <link rel="manifest" href="{$image_url}theme/default/site.webmanifest" />
        <link rel="mask-icon" href="{$image_url}theme/default/safari-pinned-tab.svg" color="#5bbad5" />

        <link rel="stylesheet" href="{$image_url}theme/default/public/css/chosen.css" />
        <link rel="stylesheet" href="{$image_url}theme/default/public/css/bootstrap.min.css">

        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
        {block name=css}{/block}
        <link rel="stylesheet" href="{$image_url}theme/default/public/css/header.css">
        <link rel="stylesheet" href="{$image_url}theme/default/public/css/footer.css">
        <script>
            var base_url = '{$base_url}';
            var image_url = '{$image_url}';
            var is_login = {if $member.id > 0}true{else}false{/if};
        </script>

        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <script src="{$image_url}theme/default/public/js/utility.js"></script>
        <script src="{$image_url}theme/default/public/js/common.js?v=1.0"></script>

        <script>
            function menuModal() {
                var html = '';
                html += '<div class="popup_cate_menu">';
                    html += '<div class="dim show"></div>';
                    html += '<div class="wrap_box">';
                        html += '<div class="box_cate_menu">';
                            html += '<a href="{$base_url}carethebear" class="cate_menu bear flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/public/images/header/login_bear.svg" alt="bear" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Bear';
                                html += '</div>';
                            html += '</a>';
                            html += '<a href="{$base_url}care-the-whale" class="cate_menu whale flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/public/images/header/login_whale.svg" alt="whale" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Whale';
                                html += '</div>';
                            html += '</a>';
                            html += '<a href="{$base_url}care-the-wild" class="cate_menu elephant flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/public/images/header/menu_elephant.svg" alt="elephant" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Wild';
                                html += '</div>';
                            html += '</a>';

                            {if $member.id > 0}
                                html += '<div class="menu login_menu flex show-940">';
                                    html += '<div class="icon-user">';
                                        html += '<a href="{$base_url}dashboard"><span class="user icon-profile"></span></a>';
                                    html += '</div>';
                                    html += '<div class="flex column">';
                                        html += '<span class="txt-log f_med"><a href="{$base_url}dashboard" style="color: #000;">{$member.name} {$member.surname}</a><br> <span class="txt-sub f_reg"><a href="{$base_url}logout">แก้ไขข้อมูลสมาชิก</a></span><span class="f_bold" style="color: #FFA400;font-size: 20px;"> | </span><span class="txt-sub f_reg"><a href="{$base_url}logout">ออกจากระบบ</a></span></span>';
                                    html += '</div>';
                                html += '</div>';
                            {else}
                                html += '<div class="menu login_menu flex show-940">';
                                    html += '<span class="icon icon-profile"></span>';
                                    html += '<span class="txt f_med">เข้าสู่ระบบ</span>';
                                html += '</div>';
                            {/if}

                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                return html;
            }
            const oauth2_authen_param = `{$oauth2_authen_param}{$callback_url}`;
        </script>
        {block name=js}{/block}
        {literal}
        <script>
            function loadCSS(e,n,o,t){"use strict";if(e.trim().length<=0){return;}var d=window.document.createElement("link"),i=n||window.document.getElementsByTagName("script")[0],s=window.document.styleSheets;return d.rel="stylesheet",d.href=e,d.media="only x",t&&(d.onload=t),i.parentNode.insertBefore(d,i),d.onloadcssdefined=function(n){for(var o,t=0;t<s.length;t++)s[t].href&&s[t].href.indexOf(e)>-1&&(o=!0);o?n():setTimeout(function(){d.onloadcssdefined(n)})},d.onloadcssdefined(function(){d.media=o||"all"}),d};
            function loadJS(e, a) { if (e.length <= 0) { return; } var t = document.getElementsByTagName("head")[0], n = document.createElement("script"); n.type = "text/javascript", n.src = e, n.async = !0, n.onreadystatechange = a, n.onload = a, t.appendChild(n) }
        </script>
        {/literal}
    </head>
    <body>
        <header class="header flex h-justify v-center">
            <div class="constraint_header flex h-justify v-center">
                <a href="{$base_url}" class="logo flex center">
                    <img src="{$image_url}theme/default/public/images/header/logo.svg" alt="Set Climate logo" />
                </a>
                <div class="header_menu flex h-justify v-center">
                    {if $member.id > 0}
                        <div class="menu flex center hide-940" >
                            <div class="icon-user">
                                <a href="{$base_url}dashboard"><span class="user icon-profile"></span></a>
                            </div>
                            <div class="flex column">
                                <span class="txt f_med"><a href="{$base_url}dashboard" style="color: #000;">{$member.name} {$member.surname}</a><br> 
                                <span class="txt-sub f_reg"><a href="{$base_url}member/member_profile">แก้ไขข้อมูลสมาชิก</a></span>
                                <span class="line f_bold"> | </span>
                                <span class="txt-sub f_reg"><a href="{$base_url}logout">ออกจากระบบ</a></span>
                                </span>
                            </div>
                            {* <div class="icon-user-settings">
                                <a href="{$base_url}member/register"><img src="{$image_url}theme/default/public/images/header/user-settings.svg"/></a>
                            </div> *}
                        </div>
                    {else}
                        <div class="menu flex center hide-940" data-btn="login">
                            <span class="icon icon-profile"></span>
                            <span class="txt f_med">เข้าสู่ระบบ</span>
                        </div>
                    {/if}
                    <div class="menu flex center" data-btn="menu">
                        <span class="icon menu icon-menu"></span>
                        <span class="txt f_med">เมนู</span>
                    </div>
                    <div class="menu flex center" data-btn="close">
                        <span class="icon close icon-cross"></span>
                    </div>
                </div>
            </div>
        </header>
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//climatecare.setsocialimpact.com/analytics/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
        {block name=body}{/block}
        {block name=script}{/block}
    </body>
</html>
