{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/ty-dashboard.css">
{/block}
{block name=body}
  <div id="this_ty">
    <div class="padding_top"></div>
    <div class="main_ty_dashboard">
      <div class="bg">
        <img src="{$image_url}theme/default/public/images/dashboard_register/bg_ty_bear.png" alt="">
      </div>
      <div class="container">
        <div class="content_wrapper">
          <div class="block_content">
            <span class="text_desc f_bold">แจ้งลืมรหัสผ่านสำเร็จแล้ว<br>กรุณาตรวจสอบอีเมลเพื่อตั้งรหัสผ่านใหม่</span>
            <a href="{$base_url}" class="button_ty">
              <span class="f_med">ตกลง</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
{/block}