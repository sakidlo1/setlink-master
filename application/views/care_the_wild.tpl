{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/wild.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dropdown-wild.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/contact-section.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css">
{/block}
{block name=js}
    <script>
    const elephant = [
        {foreach $company_wild as $company_wild_item}
            '{$company_wild_item.logo}',
        {/foreach}
    ];
    const dataOrganization = [
        ...elephant
    ];
    </script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/care-the-wild.js"></script>
    <script src="{$image_url}theme/default/public/js/contact-section.js"></script>
    <script src="{$image_url}theme/default/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/public/js/form.js"></script>
{/block}
{block name=body}
	<!-- banner -->
    <div class="banner-wild">
        <div class="left-banner">
            <img class="lazy-img logo-banner" inviewspy="300" data-src="{$image_url}theme/default/public/images/wild/banner/logo-banner.png"
                alt="">
            <span inviewspy="600" class="title-banner">ปลูกป้อง Plant &Protect</span>
            <span inviewspy="900" class="desc-banner">เป็นอีกหนึ่งโครงการความร่วมมือในการระดมทุน<br class="d-lg-block">
                ด้วยการปลูกป่าใหม่ ปลูกป่าเสริม และส่งเสริมการดูแลป่า<br class="d-xl-block">
                โดยผ่านภาคีเครือข่ายการปลูกป่า คือ กรมป่าไม้ องค์กร<br class="d-xl-block">
                ภาคเอกชนที่ร่วมระดมทุนปลูกป่า ชุมชนที่ดูแลป่าชุมชน<br class="d-xl-block">
                และต้นไม้ที่ปลูกให้เติบโตขึ้น เพื่อมุ่งสร้างระบบนิเวศที่<br class="d-xl-block">
                สมดุล ด้วยการเพิ่มจำนวนต้นไม้ และขยายผืนป่าอย่าง<br class="d-xl-block">
                เป็นรูปธรรม ซึ่งถือเป็นต้นทางในการสร้างสมดุลให้กับ<br class="d-xl-block">
                ระบบนิเวศ และร่วมลดปัญหาภาวะโลกร้อนตั้งแต่ต้นทาง </span>
        </div>
        <div class="right-banner" inviewspy="1200">
            <img class="lazy-img d-lg-block" data-src="{$image_url}theme/default/public/images/wild/banner/img-dt.png" alt="">
            <img class="lazy-img d-lg-none" data-src="{$image_url}theme/default/public/images/wild/banner/img-mb.png" alt="">
        </div>
    </div>
    <!-- Section2 -->
    <div class="container_section2">
        <div class="content">
            <div class="left" inviewspy>
                <img data-src="{$image_url}theme/default/public/images/wild/plant_section2.jpg" class="img lazy-img" alt="img-plant">

            </div>
            <div class="right">
                <div class="text-right">
                    <div class="text-header-right f_bold" inviewspy="150">หลักการคัดเลือกพื้นที่ปลูกป่า</div>
                    <div class="desc-right">
                        <div class="text-desc-right f_reg">
                            <img data-src="{$image_url}theme/default/public/images/wild/leaf1_section2.png" inviewspy="300" class="img lazy-img"
                                alt="1">
                            <span class="space f_reg" inviewspy="450">
                                ป่าชุมชนที่มีพื้นที่ถูกกฎหมาย ได้ขึ้นทะเบียนเป็นป่าชุมชนตาม <br class="hide-xs">
                                พรบ.ป่าชุมชน และไม่มีข้อพิพาททางกฏหมายใดๆ
                            </span>
                        </div>
                        <div class="text-desc-right f_reg">
                            <img data-src="{$image_url}theme/default/public/images/wild/leaf2_section2.png" class="img lazy-img" alt="2"
                                inviewspy="600">
                            <span class="space f_reg" inviewspy="750">
                                ป่าชุมชนมีผู้นำชุมชนและสมาชิกที่มีศักยภาพและเข้มแข็งใน<br
                                    class="hide-xs">การปลูกและดูแลต้นไม้ให้รอด 100%
                            </span>
                        </div>
                        <div class="text-desc-right f_reg">
                            <img data-src="{$image_url}theme/default/public/images/wild/leaf3_section2.png" class="img lazy-img" alt="3"
                                inviewspy="900">
                            <span class="space f_reg" inviewspy="1050">
                                ชุมชนผู้ปลูกในแต่ละพื้นที่มีคณะทำงานของชุมชน (ผู้นำชุมชน<br
                                    class="show-820 hide-xs">และสมาชิก) และพร้อมให้ความร่วมมือและรับผิดชอบร่วมกันใน<br
                                    class="hide-xs">การดูแลผืนป่า ตามเงื่อนไขการปลูกของโครงการ ( รอด100% / <br
                                    class="hide-xs"> ปลูกซ่อมเสริม / ติดตามผลการปลูกต่อเนื่อง 10 ปี /จัดทำ<br
                                    class="hide-xs">แหล่งน้ำ / ป้องกันแนวกันไฟ / กำจัดวัชพืช เป็นต้น )
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- Section3 -->
    <div class="container_section3">
        <div class="content">
            <div class="text-header">
                <div class="f_reg first-line f_med" inviewspy="100">
                    ภาคเอกชนที่สนใจระดมทุนปลูกป่า<br class="show-xs"> สามารถร่วมเป็น
                    <div class="f_bold second-line" inviewspy="150">
                        Active Sponsor – <span class="color-green">ทีมปลูกป้อง</span>
                    </div>
                </div>
            </div>
            <div class="text-details">
                <img data-src="{$image_url}theme/default/public/images/wild/Chang_section3.png" alt="Chang" class="img lazy-img" inviewspy="250">
                <div class="text-header-details f_med" inviewspy="350">“ปลูก”</div>
                <div class="text-desc f_reg" inviewspy="450">ด้วยการบริจาคเงินเพื่อเป็นทุนในการปลูกไม้ และดูแลป่าไม้
                    <span class="color-green">ต้นละ 220 บาท (1 ไร่ ปลูกได้ 200 ต้น ) งบประมาณ 44,000 บาท / ไร่)</span>
                    และร่วมลงพื้นที่ปลูกป่าร่วมกันกับโครงการและชาวบ้านในชุมชน
                </div>
            </div>
            <div class="text-details">
                <img data-src="{$image_url}theme/default/public/images/wild/ppl-planting_section3_.png" alt="plant" class="img-nd lazy-img"
                    inviewspy="550">
                <div class="text-header-details f_med" inviewspy="650">“ป้อง”</div>
                <div class="text-desc f_reg" inviewspy="750">ด้วยเป้าหมายต้นไม้ที่ปลูกต้องรอดและเติบโต 100 %
                    บริษัทและโครงการฯ
                    ร่วมกันติดตามประเมินผลการปลูกป่า <span class="color-green">เป็นเวลาอย่างน้อย10 ปี</span> โดยทุกปี
                    จะมีการติดตามผลอย่างน้อยปีละ 2 ครั้ง โดยบริษัทสามารถลงพื้นที่เพื่อตรวจสอบการเติบโต
                    พูดคุยกับชาวบ้านถึงปัญหา และสามารถต่อยอด
                    ดูแลอนุรักษ์ป่าอย่างอื่นเพิ่มได้ </div>
            </div>
            <div class="bottom-section">
                <div class="text f_med" inviewspy="850">ข้อมูลของการติดตามการเติบโตต้นไม้<br
                        class="show-xs">และความคืบหน้าต่างๆ จะนำเสนอที่ </div>
                <div class="flex h-justify h-center v-center wrap">
                    <div class="bottom-contact flex" inviewspy="950">
                        <img data-src="{$image_url}theme/default/public/images/wild/icon-com_section3.png" alt="com-icon" class="img lazy-img">
                        <div class="text-contact f_bold">
                            ติดตามที่เว็บไซต์ <br> <span class="f_reg">www.setsocialimpact.com/carethewild </span>
                        </div>
                    </div>
                    <div class="bottom-contact flex v-center" inviewspy="1050">
                        <img data-src="{$image_url}theme/default/public/images/wild/icon-download_section3.png" alt="chang-icon"
                            class="img-nd lazy-img">
                        <div class="">
                            <div class="text-contact f_bold">
                                ดาวน์โหลดแอพได้แล้ววันนี้ <br> <span class="f_reg">Application Care the Wild </span>
                            </div>
                            <div class="app-download flex">
                                <a href="https://apps.apple.com/la/app/care-the-wild/id1517577968"><img
                                        src="{$image_url}theme/default/public/images/wild/icon-app-store.svg" alt="appstore"></a>
                                <a
                                    href="https://play.google.com/store/apps/details?id=com.stockexchange.carethewild&hl=en_SG&gl=US"><img
                                        src="{$image_url}theme/default/public/images/wild/icon-playstore.svg" alt="playstore"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer f_reg" inviewspy="1100">
                หมายเหตุ: บริจาคปลูกต้นไม้ ต้นละ 220 บาท ประกอบไปด้วย <br class="show-xs"> ค่าต้นกล้า ค่าเตรียมดิน
                ค่าปุ๋ย ค่าปลูกเสริมทดแทนต้นไม้ที่ตาย<br>
                ค่ากำจัดวัชพืช ค่าทำแนวกันไฟป่า ค่าทำระบบน้ำ <br class="show-xs"> ค่าพัฒนาชุมชน เป็นเวลา 10 ปี
            </div>

        </div>
    </div>

    <!-- Section4 -->
    <div class="main-care-the-bear">
        <div class="container">
            <span class="title-sec-3" inviewspy="300">สิทธิประโยชน์ <br
                    class="d-md-none">การเข้าร่วมปลูกป่าในโครงการ</span>
            <div class="box-content">
                <div class="single-content" inviewspy="300">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon1_section4.png" alt="">
                    <span class="text-content">ลดหย่อนภาษี ภาคเอกชนที่สนับสนุนพื้นที่ปลูกป่าชุมชน ของกรมป่าไม้
                        สามารถนำเงินบริจาคมาหักเป็นรายจ่ายได้ตามที่
                        จ่ายจริง แต่ไม่เกินร้อยละ 2 ของกำไรสุทธิ </span>
                </div>
                <div class="single-content" inviewspy="600">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon2_section4.png" alt="">
                    <span class="text-content">นำผลดำเนินงานการปลูกป่า ไปรายงานใน รายงานประจำปี หรือ 56-1 One Report ได้
                    </span>
                </div>
                <div class="single-content" inviewspy="900">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon3_section4.png" alt="">
                    <span class="text-content">นำพื้นที่ปลูกป่าไปยื่นกับองค์การบริหารจัดการก๊าซเรือนกระจกในการเป็น
                        ผู้พัฒนาโครงการสำหรับการขอแบ่งปันคาร์บอนเครดิตตามเงื่อนไขของ องค์การบริหารก๊าซเรือนกระจก </span>
                </div>
            </div>
            <div class="box-content">
                <div class="single-content" inviewspy="300">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon4_section4.png" alt="">
                    <span class="text-content">เกิดภาพลักษณ์ที่ดีต่อองค์กรในด้านการบริหารจัดการสิ่งแวดล้อม
                        อย่างเป็นรูปธรรม </span>
                </div>
                <div class="single-content" inviewspy="600">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon5_section4.png" alt="">
                    <span class="text-content">เกิดการสร้างจิตสำนึก การมีส่วนร่วมในการดูแลด้านสิ่งแวดล้อม
                        ของพนักงานในองค์กร </span>
                </div>
                <div class="single-content" inviewspy="900">
                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/wild/icon6_section4.png" alt="">
                    <span class="text-content">สามารถสร้างกิจกรรมปลูกป่า สานสัมพันธ์กับพนักงาน ลูกค้า และ คู่ค้า
                        เพื่อดูแลระบบนิเวศร่วมกัน </span>
                </div>
            </div>
        </div>
    </div>
    <!-- Section5 -->
    <div class="container_section5">
        <div class="content">
            <div class="header-section">
                <div class="flex h-justify in-head">
                    <div class="text-header f_bold " inviewspy="200">พื้นที่ปลูกป่า</div>
                    <a href="https://www.setsocialimpact.com/Article/Detail/77268" target="_blank"><button
                            class="header-btn button f_med hid show-xs" inviewspy="200">ดูทั้งหมด</button></a>
                </div>
                <div class="flex" inviewspy="400">
                    <div class="simple-dropdown f_reg hide-placeholder " id="simple_dropdown_filter">
                        <input type="hidden" name="simple_dropdown_filter" value="200" class="input-value"
                            data-error="">
                        <div class="dropdown-show-selected flex flex-v-c flex-r f_med" inviewspy="500">
                            <span class="-text">พื้นที่ปลูกดำเนินการแล้ว</span>
                            <span class="-icon icon-arrow-down"></span>
                        </div>
                        <div class="dropdown-section hide-serch">
                            <ul>
                                <li class="flex selected" data-value="processed">
                                    <span class="-text">พื้นที่ปลูกดำเนินการแล้ว </span>
                                </li>
                                <li class="flex" data-value="waited">
                                    <span class="-text">พื้นที่ปลูกที่รอรับการสนับสนุน </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <a href="https://www.setsocialimpact.com/Article/Detail/77268" target="_blank"><button
                            class="header-btn button f_med hide-xs" inviewspy="400">ดูทั้งหมด</button></a>
                </div>
            </div>

            <div class="img-section flex h-center swiper" inviewspy="200">
                <div class="swiper-wrapper"></div>
            </div>
        </div>
    </div>
    <!-- Section6 -->
    <div class="container_section6">
        <div class="content">
            <div class="header-section">
                <div class="text-header f_bold" inviewspy="250">
                    ผลการดำเนินงานโครงการ <br> <span class="f_reg text-header-sub">ตั้งแต่ปี 2020 - ปัจจุบัน</span>
                </div>
            </div>
            <div class="body">
                <div class="detail">
                    <div class="box" inviewspy="350">
                        <img data-src="{$image_url}theme/default/public/images/wild/trees-icon_section6.png" alt="tree" class="lazy-img img" inviewspy="400">
                        <div class="text f_med">
                            ปลูกป่าแล้ว
                            <div class="sub-box">
                                <span class="f_bold h-text" id="run-num" data-no="{$wild_stats.summary_stat_place|replace:',':''}">{$wild_stats.summary_stat_place}</span><span
                                    class="sub-text f_reg"> แห่ง รวม </span><span class="f_bold h-text" id="run-num"
                                    data-no="{$wild_stats.summary_stat_rai|replace:',':''}">{$wild_stats.summary_stat_rai}</span> <span class="sub-text f_reg">ไร่</span>
                            </div>
                        </div>
                    </div>
                    <div class="box" inviewspy="450">
                        <img data-src="{$image_url}theme/default/public/images/wild/tree1-icon_section6.png" alt="tree" class="lazy-img s-img" inviewspy="500">
                        <div class="text f_med">
                            ปลูกป่าแล้ว
                            <div class="sub-box">
                                <span class="f_bold h-text" id="run-num" data-no="{$wild_stats.summary_stat_tree|replace:',':''}">{$wild_stats.summary_stat_tree}</span> <span
                                    class="sub-text f_reg"> ต้น</span>
                            </div>
                        </div>
                    </div>
                    <div class="box" inviewspy="550">
                        <img data-src="{$image_url}theme/default/public/images/wild/Co2-icon_section6.png" alt="co2" class="lazy-img img" inviewspy="600">
                        <div class="text f_med">
                            ลดปริมาณก๊าซเรือนกระจกได้
                            <div class="sub-box">
                                <span class="f_bold h-text" id="run-num" data-no="{$wild_stats.summary_stat_co2|replace:',':''}">{$wild_stats.summary_stat_co2}</span> <span
                                    class="sub-text f_reg"> kg CO<sub>2</sub> eq/ต่อปี</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="img-left" inviewspy="350">
                    <img data-src="{$image_url}theme/default/public/images/wild/planting-people_section6.png" alt="ppl-planting" class="lazy-img" >
                </div>
            </div>
        </div>
    </div>
    <div id="organize-html"></div>
    <div class="section-register">
        <div class="container" id="form_wild">
            <span class="title-register">องค์กรธุรกิจที่สนใจร่วมโครงการ</span>
            <div class="content-register">
                <img class="lazy-img left-form" data-src="{$image_url}theme/default/public/images/wild/form/left-content.png" alt="">
                <div class="right-form">
                    <span class="title-form">เข้าร่วมโครงการ Care the Wild</span>
                    <form action="thank_you.html" id="form_register" novalidate action="." onsubmit="return false">
                        <div class="common-form">
                            <div class="wrap-col-form flex wrap" >
                                <div class="col-form ">
                                    <div class="wrap-input-text">
                                        <input id="name" name="name" autocomplete="off" type="text" autocomplete="none" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,bbb">
                                        <label class="text-label placeholder flex v-center" for="name">
                                            <span class="txt-placeholder">
                                                ชื่อ - นามสกุล
                                            </span>
                                        </label>
                                    </div>
                                     <div class="wrap-input-text">
                                        <input id="pos" name="pos" type="text" autocomplete="off" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,bbb">
                                        <label class="text-label placeholder flex v-center" for="pos">
                                            <span class="txt-placeholder">
                                                ตำแหน่งงาน
                                            </span>
                                        </label>
                                    </div>
                                    <div class="wrap-input-text">
                                        <input id="org" name="org" type="text" autocomplete="off" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,bbb">
                                        <label class="text-label placeholder flex v-center" for="org">
                                            <span class="txt-placeholder">
                                                บริษัท / องค์กร
                                            </span>
                                        </label>
                                    </div>
                                    <div class="wrap-input-text">
                                        <input id="email" name="email" type="text" autocomplete="off" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,กรุณากรอกอีเมลให้ถูกต้อง">
                                        <label class="text-label placeholder flex v-center" for="email">
                                            <span class="txt-placeholder">
                                                อีเมล
                                            </span>
                                        </label>
                                    </div>
                                    <div class="wrap-input-text">
                                        <input id="contact" name="contact" type="text" autocomplete="off" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,bbb">
                                        <label class="text-label placeholder flex v-center" for="contact">
                                            <span class="txt-placeholder">
                                                เบอร์ติดต่อ
                                            </span>
                                        </label>
                                    </div>
                                    <div class="wrap-input-text">
                                        <input id="detail" name="detail" type="text" autocomplete="off" class="input-type-text f_light input-required" value="" placeholder="" required maxlength="255" data-error="กรุณากรอกข้อมูล,bbb">
                                        <label class="text-label placeholder flex v-center" for="detail">
                                            <span class="txt-placeholder">
                                                รายละเอียดเพิ่มเติม
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrapper">
                                <button class="button" id="btn_submit" type="submit">
                                    <span class="text-btn">สมัครเลย</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="contact-html"></div>
{/block}