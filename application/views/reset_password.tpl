{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/modal.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/register.css">
{/block}
{block name=body}
	<main class="content">
      <div class="page-register">
        <script>
            function check_data()
            {
                document.getElementById("password_req").style.display = "none"; 
                document.getElementById("confirm_password_req").style.display = "none"; 
                document.getElementById("confirm_password_inc").style.display = "none"; 
                
                with(document.add_edit)
                {
                    if(password.value=="")
                    {
                        document.getElementById("password_req").style.display = ""; 
                        document.getElementById("password").focus();
                        return false;
                    }
                    else if(cpassword.value=="")
                    {
                        document.getElementById("confirm_password_req").style.display = ""; 
                        document.getElementById("cpassword").focus();
                        return false;
                    }
                    else if(password.value!=cpassword.value)
                    {
                        document.getElementById("confirm_password_inc").style.display = ""; 
                        document.getElementById("cpassword").focus();
                        return false;
                    }
                }
            }
        </script>
        <form name="add_edit" onsubmit="return check_data();" method="post">
         <div class="container">
            <div class="page-header"> 
               <h1 class="page-header--title f_bold">ตั้งรหัสผ่านใหม่</h1>
            </div>
            <div class="block-form" inviewSpy="200">
                 <div id="upload" class="common-form">
                   <fieldset>
                        <div class="wrap-upload flex column">
                           <div class="upload-wrap flex column h-center">
                              <div class="upload-area flex v-center h-justify show">
                                 <span class="block-upload-icon">
                                 </span>
                                 <div class="block-upload-desc flex column">
                                   <div class="wrap-input-text have-icon">
                                     <input class="input-type-text f_reg input-required" type="password" name="password" id="password" autocomplete="new-password" value="" maxlength="50" inputmode="email" data-error="กรุณากรอกข้อมูล,รูปแบบอีเมลไม่ถูกต้อง">
                                     <label class="text-label placeholder flex v-center" for="password"> <span class="txt-placeholder">รหัสผ่านใหม่</span> </label>
                                   </div>
                                    <p id="password_req" style="display:none; color:red;" class="required">กรุณาระบุรหัสผ่านใหม่</p>
                                 </div>
                               </div>
                               <div class="upload-area flex v-center h-justify show">
                                 <span class="block-upload-icon">
                                 </span>
                                 <div class="block-upload-desc flex column">
                                   <div class="wrap-input-text have-icon">
                                     <input class="input-type-text f_reg input-required" type="password" name="cpassword" id="cpassword" autocomplete="new-password" value="" maxlength="50" inputmode="email" data-error="กรุณากรอกข้อมูล,รูปแบบอีเมลไม่ถูกต้อง">
                                     <label class="text-label placeholder flex v-center" for="cpassword"> <span class="txt-placeholder">ยืนยันรหัสผ่าน</span> </label>
                                   </div>
                                    <p id="confirm_password_req" style="display:none; color:red;" class="required">กรุณายืนยันรหัสผ่าน</p>
                                    <p id="confirm_password_inc" style="display:none; color:red;" class="required">ยืนยันรหัสผ่านไม่ถูกต้อง</p>
                                 </div>
                               </div>
                           </div>
                        </div>
                      </fieldset>
                    </div>
                 </div>
            <div class="wrap-action" inviewSpy="300">
               <button type="submit" name="reset_password" value="reset_password" class="button btn-action submit-form" style="border:0px; display: block;">
                  <span class="button_label f_med">Submit</span>
               </button>
            </div>
         </div>
         </form>
      </div>
   </main>
{/block}