{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/register-complete.css">
{/block}
{block name=body}
	<main class="content">
      <div class="page-register-complete">
         <div class="container">
            <div class="block-complete flex column center">
               <div class="block-complete-icon flex center">
                  <span class="icon icon-check"></span>
               </div>
               <div class="block-complete-title f_bold">
                  คุณได้สมัครสมาชิกแล้ว
               </div>
               <div class="block-complete-desc f_med">
                  เจ้าหน้าที่จะทำการตรวจสอบและแจ้งผลอนุมัติผ่านทางอีเมล
               </div>
            </div>
         </div>
      </div>
   </main>
{/block}