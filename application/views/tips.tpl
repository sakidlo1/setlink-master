{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$base_url}carethebear/images/theme/default/climatecare/public/css/menu-bear.css">

        <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/ctb/assets/css/responsive.css">
    {/block}
    {block name=js}
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/player.js"></script>
        <script src="{$base_url}carethebear/images/theme/default/climatecare/public/js/simpledropdown.js"></script>

        <script src="{$image_url}theme/ctb/assets/js/main.js?ver={$smarty.now}"></script>
    {/block}
{block name=body}
    <section id="tips" inviewspy="200" style="margin-top: 60px;">
        <div class="container">
            <br /><br />
            <h2 class="title no-margin-top">Tips <i class="icon-circle"></i></h2>
            <div class="row tip-box">
                {if $list|count > 0}
                    {foreach $list as $tips_item}
                        <div class="col col-sm-4">
                            <a href="{$base_url}tips/detail/{$tips_item.id}">
                                <div class="tips_item">
                                    <div class="img">
                                        <img src="{$tips_item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                    </div>
                                </div>
                            </a>
                        </div>
                    {/foreach}
                {else}
                    <div class="inner">
                        <br/><br/><br/>
                        <center>ไม่มีข้อมูล</center>
                        <br/><br/><br/>
                    </div>
                {/if}
            </div>
            <hr/>
            {include file='paging.tpl'}
        </div>
    </section>
    <br/>
    <br class="hidden-xs"/>
{/block}