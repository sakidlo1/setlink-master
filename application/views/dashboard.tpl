{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard.css">
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: {if !empty($data_wild)}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };
    </script>
    <script src="{$image_url}theme/default/public/js/dashboard.js"></script>
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-summary flex column h-top v-top h-center">
            <div class="global-warming-detail" inviewSpy="200">
                <div class="title f_bold">ผลรวมลดปริมาณก๊าซเรือนกระจก <br class="show-xs">และเทียบเท่าการปลูกต้นไม้
                </div>
                <div class="detail-wrapper flex column-xs">
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/air-pollution.png"
                                alt="air-pollution">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">ลดปริมาณก๊าซเรือนกระจกได้</p>
                            <span>
                                <span class="stats-value f_bold" data-global="gas" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">Kg CO<sub>2</sub>e</span>
                            </span>
                        </div>
                    </div>
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/public/images/home/summary-progress/trees.png" alt="trees">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">เทียบเท่าการปลูกต้นไม้ใหญ่ อายุ 10 ปี</p>
                            <span>
                                <span class="stats-value f_bold" data-global="tree" data-no="">0</span><br
                                    class="show-xs">
                                <span class="f_reg">ต้น</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="company-detail flex"></div>
            <div id="data-wild" class="{if !empty($data_wild)}data-wild{/if}"></div>
        </div>
    </div>
{/block}