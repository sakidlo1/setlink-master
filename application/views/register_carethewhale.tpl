{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard-register.css?v=1.0">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css?v=1.0">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/modal.css">
{/block}
{block name=js}
    <script src="{$image_url}theme/default/public/js/modal.js"></script>
    <script src="{$image_url}theme/default/public/js/form.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/multi-upload-file.js?v=1.0"></script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/dashboard-register-form.js?v=1.0"></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=6LfZStwpAAAAAHMpauL6kjXDNYmO9ueSE1J2ap1u"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('form[data-recapture-key]').on('submit', function(e) {
                var form = $(this);
                grecaptcha.ready(function() {
                    grecaptcha.execute(form.data('recapture-key'), { action: 'submit' }).then(function(token) {
                        form.find('input[name=recapture_token]').val(token);
                        form.submit();
                    });
                });
                return false;
            });
        });
    </script>

{/block}
{block name=body}
   <div id="content" class="flex h-justify">
        <div class="container">
            <span class="title_form f_bold">สำหรับโครงการ Care the Whale</span>
            <span class="subtitle_form f_bold">กรุณากรอกข้อมูลเพิ่มเติม </span>
            <form data-recapture-key="6LfZStwpAAAAAHMpauL6kjXDNYmO9ueSE1J2ap1u" class="form_dashboard common-form" method="post" enctype="multipart/form-data" id="form_register" >
                    <div class="sec_company_detail">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">รายละเอียดบริษัท</span>
                        </div>
                        <div class="row_input">
                            <!-- dropdown -->
                            <div class="wrap-input-text type-dropdown">
                                <div class="simple-dropdown new_style_dropdown f_reg" id="simple_dropdown_building">
                                    <input type="hidden" name="simple_dropdown_building" value="" class="input-value" data-error="">
                                    <div class="dropdown-show-selected flex v-center h-justify"><span class="-text"></span><span class="-icon icon-arrow-down2"></span></div>
                                    <div class="dropdown-placeholder">ประเภทอาคาร</div>
                                    <div class="dropdown-section hide-serch">
                                        <ul>
                                        {foreach $company_type as $child_item}
                                            <li class="flex v-center h-justify f_reg" data-value="{$child_item.id}"><span class="-text">{$child_item.name}</span></li>
                                        {/foreach}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="mx_30"></div>
                            <div class="wrap-input-text type-dropdown">
                                <div class="simple-dropdown new_style_dropdown f_reg" id="simple_dropdown_area">
                                    <input type="hidden" name="simple_dropdown_area" value="" class="input-value" data-error="">
                                    <div class="dropdown-show-selected flex v-center h-justify"><span class="-text"></span><span class="-icon icon-arrow-down2"></span></div>
                                    <div class="dropdown-placeholder">โซน</div>
                                    <div class="dropdown-section hide-serch">
                                        <ul>
                                            {foreach $zone as $child_item}
                                              <li class="flex v-center h-justify f_reg" data-value="{$child_item.id}"><span class="-text">{$child_item.name}</span></li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="date_select_wrapper wrap-input-checkbox">
                                <span class="f_reg text_title checking-text">วันเปิดทำการ</span>
                                <div class="list_date_wrapper">
                                    <label id="mon" class="list_date checking-box" for="checkbox_date_mon">
                                        <span class="f_med date checking-text">จ</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_mon" value="mon">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="tue" class="list_date checking-box" for="checkbox_date_tue">
                                        <span class="f_med date checking-text">อ</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_tue" value="tue">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="wed" class="list_date checking-box" for="checkbox_date_wed">
                                        <span class="f_med date checking-text">พ</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_wed" value="wed">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="thu" class="list_date checking-box" for="checkbox_date_thu">
                                        <span class="f_med date checking-text">พฤ</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_thu" value="thu">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="fri" class="list_date checking-box" for="checkbox_date_fri">
                                        <span class="f_med date checking-text">ศ</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_fri" value="fri">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="sat" class="list_date checking-box" for="checkbox_date_sat">
                                        <span class="f_med date checking-text">ส</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_sat" value="sat">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                    <label id="sun" class="list_date checking-box" for="checkbox_date_sun">
                                        <span class="f_med date checking-text">อา</span>
                                        <input autocomplete="off" class="input-type-checkbox checkbox-date f_reg input-required" type="checkbox" name="date_" id="checkbox_date_sun" value="sun">
                                        <div class="checking-box">
                                            <span class="icon icon-check"></span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="mx_30"></div>
                            <div class="time">
                                <div class="start">
                                    <div class="wrap-input-text">
                                        <input autocomplete="off" id="time_open" name="time_open" class="input-type-text input_date f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                        <label class="text-label placeholder flex v-center" for="time_open">
                                            <span class="txt-placeholder">เวลาเปิดทำการ</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="end">
                                    <div class="wrap-input-text">
                                        <input autocomplete="off" id="time_off" name="time_off" class="input-type-text input_date f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                        <label class="text-label placeholder flex v-center" for="time_off">
                                            <span class="txt-placeholder">เวลาปิดทำการ</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text">
                                <input autocomplete="off" name="count_of_contact" class="input-type-text f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล" onkeypress="return numberPressed(event);">
                                <label class="text-label placeholder flex v-center" for="count_of_contact">
                                    <span class="txt-placeholder">จำนวนผู้มาติดต่อ (คน/เดือน)</span>
                                </label>
                            </div>
                            <div class="mx_30"></div>
                            <div class="wrap-input-text">
                                <input autocomplete="off" name="count_of_new_employee" class="input-type-text f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล" onkeypress="return numberPressed(event);">
                                <label class="text-label placeholder flex v-center" for="count_of_new_employee">
                                    <span class="txt-placeholder">จำนวนพนักงานในบริษัท (คน)</span>
                                </label>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text">
                                <input autocomplete="off" name="number_of_place" class="input-type-text f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล" onkeypress="return numberPressedAndDot(event);">
                                <label class="text-label placeholder flex v-center" for="number_of_place">
                                    <span class="txt-placeholder">ขนาดพื้นที่ (ตร.ม.)</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="sec_trash_cate">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">ประเภทขยะ</span>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <input autocomplete="off" name="cate_trash_number_of_place" class="input-type-text  f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                <label class="text-label placeholder flex v-center" for="cate_trash_number_of_place">
                                    <span class="txt-placeholder">ประเภท และปริมาณขยะต่อเดือนขององค์กร (โดยประมาณ)</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="sec_upload">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">นโยบายด้านสิ่งแวดล้อม</span>
                        </div>
                        <div id="upload_policy" class="row_upload">
                            <span class="subtitle_upload f_bold">อัพโหลดภาพ</span>
                            <div class="input_file_wrapper">
                                <div class="block-form new_form" inviewSpy="300">
                                    <div  class="common-form">
                                        <div class="wrap-upload flex column">
                                            <div class="upload-wrap flex column h-center">
                                                <div class="upload-area  show">
                                                    <span class="block-upload-icon">
                                                        <span class="icon icon-cloud-arrow-up"></span>
                                                    </span>
                                                    <div class="block-upload-desc flex column">
                                                        <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                                        <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 3 ไฟล์</span>
                                                        <input type="file" name="image_1" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx_30"></div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form mx_30_s">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <input autocomplete="off" name="cate_trash_desc" class="input-type-text  f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                <label class="text-label placeholder flex v-center" for="cate_trash_desc">
                                    <span class="txt-placeholder">มุ่งมั่นสู่ความเป็นผู้นำแห่งบ้านจัดสรรรักษ์สิ่งแวดล้อม</span>
                                </label>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <textarea autocomplete="off" rows="1"  name="cate_trash_detail" class="input-type-text auto_height  f_reg input-required" type="text" value="" required data-error="กรุณากรอกข้อมูล"></textarea>
                                <label class="text-label placeholder flex v-center" for="cate_trash_detail">
                                    <span class="txt-placeholder">คำอธิบาย</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="sec_waste_management">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">ผู้รับผิดชอบด้านการจัดการขยะ</span>
                        </div>
                        <div id="upload_responsible" class="row_upload">
                            <span class="subtitle_upload f_bold">อัพโหลดภาพ</span>
                            <div class="input_file_wrapper">
                                <div class="block-form new_form" inviewSpy="300">
                                    <div  class="common-form">
                                        <div class="wrap-upload flex column">
                                            <div class="upload-wrap flex column h-center">
                                                <div class="upload-area  show">
                                                    <span class="block-upload-icon">
                                                        <span class="icon icon-cloud-arrow-up"></span>
                                                    </span>
                                                    <div class="block-upload-desc flex column">
                                                        <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                                        <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 3 ไฟล์</span>
                                                        <input type="file" name="image_2" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx_30"></div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form mx_30_s">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <input autocomplete="off" name="responsible_dec" class="input-type-text  f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                <label class="text-label placeholder flex v-center" for="responsible_dec">
                                    <span class="txt-placeholder">การจัดกิจกรรมกับหน่วยงานภายนอก</span>
                                </label>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <textarea autocomplete="off" rows="1"  name="responsible_detail" class="input-type-text auto_height  f_reg input-required" type="text" value="" required data-error="กรุณากรอกข้อมูล"></textarea>
                                <label class="text-label placeholder flex v-center" for="responsible_detail">
                                    <span class="txt-placeholder">คำอธิบาย</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="sec_waste_management">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">การจัดกิจกรรมกับหน่วยงานภายนอก </span>
                        </div>
                        <div id="activity" class="row_upload">
                            <span class="subtitle_upload f_bold">อัพโหลดภาพ</span>
                            <div class="input_file_wrapper">
                                <div class="block-form new_form" inviewSpy="300">
                                    <div  class="common-form">
                                        <div class="wrap-upload flex column">
                                            <div class="upload-wrap flex column h-center">
                                                <div class="upload-area  show">
                                                    <span class="block-upload-icon">
                                                        <span class="icon icon-cloud-arrow-up"></span>
                                                    </span>
                                                    <div class="block-upload-desc flex column">
                                                        <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                                        <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 3 ไฟล์</span>
                                                        <input type="file" name="image_3" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx_30"></div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form  mx_30_s">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form ">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <input autocomplete="off" name="activity_decs" class="input-type-text  f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                <label class="text-label placeholder flex v-center" for="activity_decs">
                                    <span class="txt-placeholder">การจัดกิจกรรมกับหน่วยงานภายนอก</span>
                                </label>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <textarea autocomplete="off" rows="1"  name="activity_detail" class="input-type-text auto_height  f_reg input-required" type="text" value="" required data-error="กรุณากรอกข้อมูล"></textarea>
                                <label class="text-label placeholder flex v-center" for="activity_detail">
                                    <span class="txt-placeholder">คำอธิบาย</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="sec_waste_management">
                        <div class="title_wrapper">
                            <span class="title_list f_bold">การจัดกิจกรรมประชาสัมพันธ์</span>
                        </div>
                        <div id="upload_ad" class="row_upload">
                            <span class="subtitle_upload f_bold">อัพโหลดภาพ</span>
                            <div class="input_file_wrapper">
                                <div class="block-form new_form" inviewSpy="300">
                                    <div  class="common-form">
                                        <div class="wrap-upload flex column">
                                            <div class="upload-wrap flex column h-center">
                                                <div class="upload-area  show">
                                                    <span class="block-upload-icon">
                                                        <span class="icon icon-cloud-arrow-up"></span>
                                                    </span>
                                                    <div class="block-upload-desc flex column">
                                                        <span class="desc-caption f_med">วางที่นี่ หรือ คลิกที่นี่เพื่อเลือกไฟล์</span>
                                                        <span class="desc-note">ขนาดไฟล์ไม่เกิน 2 MB, รองรับ .jpg, jpeg, png, ไม่เกิน 3 ไฟล์</span>
                                                        <input type="file" name="image_4" class="input_customer_logo" accept=".jpg,.jpeg,.png,.pdf" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mx_30"></div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form mx_30_s">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-form">
                                    <div class="wrap-upload flex column">
                                        <div class="upload-wrap none_style flex column h-center">
                                            <div class="area_preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <input autocomplete="off" name="advertise_desc" class="input-type-text  f_reg input-required" type="text" value="" required maxlength="100" data-error="กรุณากรอกข้อมูล">
                                <label class="text-label placeholder flex v-center" for="advertise_desc">
                                    <span class="txt-placeholder">การจัดกิจกรรมประชาสัมพันธ์</span>
                                </label>
                            </div>
                        </div>
                        <div class="row_input">
                            <div class="wrap-input-text input_trash">
                                <textarea autocomplete="off" rows="1"  name="advertise_detail" class="input-type-text auto_height  f_reg input-required" type="text" value="" required data-error="กรุณากรอกข้อมูล"></textarea>
                                <label class="text-label placeholder flex v-center" for="advertise_detail">
                                    <span class="txt-placeholder">คำอธิบาย</span>
                                </label>
                            </div>
                        </div>
                    </div>
                <div class="block-form" inviewSpy="300">
                    <div id="agreement" class="common-form">
                        <div class="wrap-input-checkbox">
                            <label class="flex h-center" for="checkbox_consent">
                                <input class="input-type-checkbox f_reg input-required" type="checkbox" name="customer_accept_consent" id="checkbox_consent" value="T">
                                <div class="checking-box">
                                        <span class="icon icon-check"></span>
                                </div>
                                <span class="checking-text flex wrap">
                                    <span>ข้าพเจ้าขอยืนยันว่าข้าพเจ้าได้อ่านและเข้าใจข้อตกลงในการใช้บริการเว็บไซต์ </span>
                                    <a class="link-website" href="https://climatecare.setsocialimpact.com" rel="noreferrer" target="_blank"> https://climatecare.setsocialimpact.com </a>
                                    <a class="link-consent" href="#" data-modal="privacy-notice"> <u>อ่านข้อตกลงที่นี่</u></a>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="wrap-action" inviewSpy="300">
                    <div class="button btn-action submit-form" data-btn = 'submit_form'>
                       <span class="button_label f_med">Submit</span>
                       <input type="hidden" name="recapture_token" />
                    </div>
                </div>
            </form>
        </div>
    </div>
{/block}