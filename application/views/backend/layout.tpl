<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>{block name=meta_title}{$page} - {$site_name} : {$company_name}{/block}</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="{$image_url}theme/backend/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/dist/css/skins/_all-skins.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/iCheck/flat/blue.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/morris/morris.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/daterangepicker/daterangepicker.css">
		<link rel="stylesheet" href="{$image_url}theme/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css">
		<link rel="shortcut icon" href="{$image_url}theme/backend/favicon.ico?v=2" type="image/x-icon" />
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<a class="logo" href="{$base_url}backend">
					<span class="logo-mini"><img src="{$image_url}theme/backend/logo_small.png" height="40px"></span>
					<span class="logo-lg text-left" style="background-color: #ffffff; padding-left: 10px;"><img src="{$image_url}theme/backend/logo_mini.png" height="40px"></span>
			    </a>
				<nav class="navbar navbar-static-top">
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="{$image_url}upload/user/{($admin.id%4000)}/{$admin.id}/thumbnail.jpg" onerror="this.src='{$image_url}theme/backend/dist/img/avatar5.png';" class="user-image" alt="User Image">
									<span class="hidden-xs">{$admin.name}</span>
								</a>
								<ul class="dropdown-menu">
									<li class="user-header">
										<img src="{$image_url}upload/user/{($admin.id%4000)}/{$admin.id}/thumbnail.jpg" onerror="this.src='{$image_url}theme/backend/dist/img/avatar5.png';" class="img-circle" alt="User Image">
										<p>
											{$admin.name}
											<small>{$admin.role_name}</small>
										</p>
									</li>
									<li class="user-footer">
										<div class="pull-left">
											<a href="{$base_url}backend/user/edit_profile" class="btn btn-default btn-flat">Profile</a>
										</div>
										<div class="pull-right">
											<a href="{$base_url}backend/user/logout" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<ul class="sidebar-menu">
						<li class="header">{$site_name}</li>
						<li{if $authen->controller == 'home'} class="active"{/if}>
							<a href="{$base_url}backend">
								<i class="fa fa-bars"></i> <span>Dashboard</span>
							</a>
						</li>
						<li class="treeview{if $authen->controller == 'news' || $authen->controller == 'vdo' || $authen->controller == 'tips'} active{/if}">
							<a href="#">
								<i class="fa fa-file-text-o"></i> <span>Content</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li{if $authen->controller == 'news'} class="active"{/if}><a href="{$base_url}backend/news"><i class="fa fa-newspaper-o"></i> <span>News</span></a></li>
								<li{if $authen->controller == 'vdo'} class="active"{/if}><a href="{$base_url}backend/vdo"><i class="fa fa-film"></i> <span>Video</span></a></li>
								<li{if $authen->controller == 'tips'} class="active"{/if}><a href="{$base_url}backend/tips"><i class="fa fa-file-text-o"></i> <span>Tips</span></a></li>
								<li{if $authen->controller == 'whale_news'} class="active"{/if}><a href="{$base_url}backend/whale_news"><i class="fa fa-newspaper-o"></i> <span>Whale News</span></a></li>
							</ul>
						</li>
						<li class="treeview{if $authen->controller == 'company_wild'} active{/if}">
							<a href="#">
								<i class="fa fa-tree"></i> <span>Care the Wild</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li{if $authen->controller == 'company_wild'} class="active"{/if}><a href="{$base_url}backend/company_wild"><i class="fa fa-building"></i> <span>Company</span></a></li>
							</ul>
						</li>
						<li class="treeview{if $authen->controller == 'news_category'} active{/if}">
							<a href="#">
								<i class="fa fa-gears"></i> <span>Settings</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li{if $authen->controller == 'news_category'} class="active"{/if}><a href="{$base_url}backend/news_category"><i class="fa fa-folder-o"></i> <span>News Category</span></a></li>
							</ul>
						</li>
						{if $authen->is_developer == true || $authen->is_admin == true}
							<li class="treeview{if $authen->controller == 'user' || $authen->controller == 'role'} active{/if}">
								<a href="#">
									<i class="fa fa-users"></i> <span>Users</span>
									<span class="pull-right-container">
										<i class="fa fa-angle-left pull-right"></i>
									</span>
								</a>
								<ul class="treeview-menu">
									<li{if $authen->controller == 'user'} class="active"{/if}><a href="{$base_url}backend/user"><i class="fa fa-users"></i> <span>Users</span></a></li>
									{if $authen->is_developer == true}
										<li{if $authen->controller == 'role'} class="active"{/if}><a href="{$base_url}backend/role"><i class="fa fa-shield"></i> <span>Role</span></a></li>
									{/if}
								</ul>
							</li>
						{/if}
					</ul>
				</section>
			</aside>	
			<div class="content-wrapper">
				{block name=body}{/block}
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 1.0.0
				</div>
				<strong>Copyright &copy; 2020 {$company_name}</strong> All rights reserved.
			</footer>
		</div>
		<script src="{$image_url}theme/backend/plugins/jQuery/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
		<script src="{$image_url}theme/backend/bootstrap/js/bootstrap.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/modal/modal.js"></script>
		<script src="{$image_url}theme/backend/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<!--<script src="{$image_url}theme/backend/plugins/morris/morris.min.js"></script>-->
		<script src="{$image_url}theme/backend/plugins/sparkline/jquery.sparkline.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
		<script src="{$image_url}theme/backend/plugins/knob/jquery.knob.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/daterangepicker/daterangepicker.js"></script>
		<script src="{$image_url}theme/backend/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="{$image_url}theme/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="{$image_url}theme/backend/plugins/fastclick/fastclick.js"></script>
		<script src="{$image_url}theme/backend/dist/js/app.min.js"></script>
		<!--<script src="{$image_url}theme/backend/dist/js/pages/dashboard.js"></script>-->
		<script src="{$image_url}theme/backend/dist/js/demo.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>
		{block name=script}{/block}
	</body>
</html>