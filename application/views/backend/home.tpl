{extends file="backend/layout.tpl"}
{block name=meta_title}Home - {$site_name} : {$company_name}{/block}
{block name=body}
    <section class="content-header">
        <h1>
            {$site_name}
            <small>{$company_name}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
            <li class="active">Home</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp; Content</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/news" class="btn btn-app">
                            <i class="fa fa-newspaper-o"></i> News
                        </a>
                        <a href="{$base_url}backend/vdo" class="btn btn-app">
                            <i class="fa fa-film"></i> Video
                        </a>
                        <a href="{$base_url}backend/tips" class="btn btn-app">
                            <i class="fa fa-file-text-o"></i> Tips
                        </a>
                        <a href="{$base_url}backend/whale_news" class="btn btn-app">
                            <i class="fa fa-newspaper-o"></i> Whale News
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-tree"></i>&nbsp; Care the Wild</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/company_wild" class="btn btn-app">
                            <i class="fa fa-building"></i> Company
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-gears"></i>&nbsp; Settings</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/news_category" class="btn btn-app">
                            <i class="fa fa-folder-o"></i> News Category
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {if $authen->is_developer == true || $authen->is_admin == true}
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-users"></i>&nbsp; Users</h3>
                        </div>
                        <div class="box-body">
                            <a href="{$base_url}backend/user" class="btn btn-app">
                                <i class="fa fa-users"></i> Users
                            </a>
                            {if $authen->is_developer == true}
                                <a href="{$base_url}backend/role" class="btn btn-app">
                                    <i class="fa fa-shield"></i> Role
                                </a>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    </section>
{/block}