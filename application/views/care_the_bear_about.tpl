{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
	<link rel="stylesheet" href="{$image_url}theme/default/public/css/care-the-bear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/care-6.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/contact-section.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/menu-bear.css">
{/block}
{block name=js}
	<script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script>
    const bear = [
        {foreach $company_bear as $company_bear_item}
            '{$company_bear_item.logo}',
        {/foreach}
    ];
    const dataOrganization = [
        ...bear
    ];
    </script>
    <script src="{$image_url}theme/default/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/public/js/contact-section.js"></script>
{/block}
{block name=body}
	<div class="menu-bear" inviewspy>
        <div class="wrap-menu">
			<a class="menu-link f_med" href="{$base_url}care-the-bear">Care the Bear</a>
			<span class="f_med">What’s Care the Bear</span>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-activity">Activities & Event</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-project">Climate Action Project</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-article">Articles</a>
			<a class="menu-link f_med" href="{$base_url}care-the-bear-news">News</a>
        </div>
    </div>
	<div id="banner_html"></div>
	    <div class="banner-care-the-bear">
	        <div class="content-banner-left">
				<img src="{$image_url}theme/default/public/images/care-the-bear/banner/logo-bear.png" alt="" inviewspy>
					<span class="title-banner" inviewspy="100">โครงการ<br class="d-xl-block"> Care the Bear</span>
					<span class="tiny-banner" inviewspy="200">ปัญหาสิ่งแวดล้อมเป็นปัญหาใหญ่ของโลกใบนี้ ซึ่งมีความรุนแรงและ
						ส่งผลกระทบต่อประเทศไทยเป็นอย่างมาก เห็นได้จากความถี่และความรุนแรงของภัยธรรมชาติที่เกิดขึ้น
						ซึ่งส่งผลกระทบต่อทั้งเศรษฐกิจ สังคมและสิ่งแวดล้อมอย่างหนัก จึงเป็นวาระเร่งด่วนของประเทศทั่วโลกที่ต้องเร่งแก้ปัญหา
						<br class="d-xl-block">
						<br class="d-lg-block">
						จากข้อมูลของ Climate Watch Data ซึ่งได้รายงานเมื่อ 10 มีนาคม 2565 ที่ผ่านมานี้ ได้สรุปข้อมูลการปล่อยก๊าซ
						เรือนกระจกในปี 2560 ว่าทุกประเทศทั่วโลกได้มีการปล่อยก๊าซเรือนกระจกมากถึง &nbsp; 48,939.71 &nbsp; ล้านตัน
						คาร์บอนไดออกไซด์เทียบเท่า (MtCO<sub>2</sub>e) และประเทศไทยเป็นประเทศที่ปล่อยก๊าซเรือนกระจกมากถึง 432.22 
						MtCO<sub>2</sub>e คิดเป็นอันดับที่ 20 ของโลกมีสัดส่วน 0.88% ของทั้งโลก และเป็นลำดับที่ 2 ของอาเซียนรองจากอินโดนีเซีย ซึ่ง
						ประเทศที่ปล่อยก๊าซเรือนกระจกมากที่สุดในโลก ได้แก่ จีน (11,705.81 MtCO<sub>2</sub>e) รองลงมาเป็นสหรัฐอเมริกา
						(5,794.35 MtCO<sub>2</sub>e)  ซึ่งทั้ง 2 ประเทศนี้มีสัดส่วนมากกว่า 35.76% ของทั้งโลก สำหรับในประเทศไทยภาคที่ปล่อย
						ก๊าซเรือนกระจกมากที่สุดคือภาคพลังงาน ซึ่งมีสัดส่วนมากที่สุดถึง 61.11% รองลงมาคือภาคอุตสาหกรรม 16.67%
						ภาคการเกษตร 15.96% และภาคป่าไม้และการใช้ประโยชน์ที่ดิน 3.31% และการจัดการของเสีย 2.95%และในปี 
						พ.ศ. 2560 ประเทศไทย มีการปล่อยก๊าซเรือนกระจกเฉลี่ยต่อคนที่ 6.21 ตันคาร์บอนไดออกไซด์เทียบเท่า (tonCO<sub>2</sub>e) 
						ซึ่งต่ำกว่าค่าเฉลี่ยมาตรฐานของประชากรโลก²เพียง 3.57% ซึ่งปล่อยอยู่ที่ 6.44 tonCO<sub>2</sub>e นับได้ว่าเป็นต้นเหตุ
						ของสภาวะโลกร้อน ซึ่งการแก้ปัญหานี้จะเห็นผลอย่างเป็นรูปธรรมต้องมีการประเมินและวัดผลได้ ดังนั้นการทราบถึงปริมาณ
						คาร์บอนฟุตพริ้นท์ที่ปล่อยออกมาจากกิจกรรมต่าง ๆ ของมนุษย์อย่างต่อเนื่อง ทั้งการใช้พลังงาน การเกษตร การพัฒนาและ
						การขยายตัวของภาคอุตสาหกรรม การขนส่ง การตัดไม้ทำลายป่า หรือแม้กระทั่งการจัดงานอีเว้นท์ที่ต้องมีการใช้พลังงานไฟฟ้า
						ทั้งส่วนของการจัดงานและการพักแรม การเดินทางของผู้เข้าร่วมงาน การใช้พลังงานในการปรุงอาหาร สิ่งเหลือทิ้งจากการจัดงาน 
						ล้วนเป็นเหตุสำคัญของการเกิดภาวะโลกร้อน ซึ่งส่งผลกระทบต่อวิถีการดำรงชีวิตของมนุษย์ สิ่งมีชีวิตและนับวันปัญหาดังกล่าว
						ก็ยิ่งทวีความรุนแรงมากขึ้น
					</span>
				<div class="img-content-tiny" inviewspy="300">
				<img src="{$base_url}carethebear/images/upload/editor/source/Articles/ctb.jpg" alt="" inviewspy>
				</div>
				<span class="tiny-banner" inviewspy="400">
					ตลาดหลักทรัพย์แห่งประเทศไทย จึงริเริ่มโครงการ “Care the Bear” ภายใต้แนวคิด “Change the Climate Change” 
					ตั้งแต่ปี 2561 โดยร่วมกับพันธมิตร ทั้งภาคเอกชน ภาครัฐ และธุรกิจเพื่อสังคม ช่วยกันขับเคลื่อนการลดภาวะโลกร้อน
					ด้วยการลดการปล่อยก๊าซเรือนกระจกจากการจัดกิจกรรมขององค์กร ไม่ว่าจะเป็นกิจกรรม onsite หรือ online เช่น 
					การประชุม การอบรม การจัดงาน event  งานมอบรางวัล การประชุมผู้ถือหุ้น กิจกรรม CSR  เป็นต้น โดยใช้หลักการ 6 Cares  
					มุ่งเปลี่ยนแปลงในมิติของผู้บริโภคให้มีส่วนช่วยลดโลกร้อน ซึ่งสอดคล้องกับ Sustainable Development Goals (SDGs) 
					ข้อที่ 13 “Climate Action”
				</span>
				<div class="img-content-tiny"inviewspy="600">
				<img src="{$base_url}carethebear/images/upload/editor/source/Articles/unnamed-11_1.jpg" alt="" inviewspy>
				</div>
				<span class="tiny-banner" inviewspy="800">
					ปัจจุบันโครงการ Care the Bear มีองค์กรพันธมิตรกว่า 240 องค์กร (ข้อมูล ณ มีค.2565)&nbsp; และได้ร่วมกันลดการปล่อย
					ก๊าซเรือนกระจกแล้ว13,325 ตันคาร์บอนไดออกไซด์เทียบเท่า ซึ่งเทียบเท่าการดูดซับ CO<sub>2</sub> / ปีของต้นไม้จำนวน 
					1,480,657 ต้น (ข้อมูล ณ มีค.2565)&nbsp; โดยมีองค์กรภาคีหลัก คือ องค์การบริหารจัดการก๊าซเรือนกระจก (องค์การมหาชน) 
					และพันธมิตรอื่นร่วมขับเคลื่อน ไม่ว่าจะเป็นบริษัทจดทะเบียน บริษัทจำกัด หน่วยงานภาครัฐ สมาคม สถาบันการศึกษา โรงแรม 
					สถานที่จัดการประชุม และกิจการเพื่อสังคม
					<br class="d-xl-block">
					<br class="d-lg-block">
					โครงการ Care the Bear จะเป็นเครื่องมือที่สามารถช่วยสร้างจิตสำนึก ปรับเปลี่ยนพฤติกรรมของสมาชิกในองค์กร หรือชุมชน 
					เพื่อลดการปล่อยก๊าซเรือนกระจก โดยผลที่ได้จากการคำนวณการลดก๊าซเรือนกระจกนี้สามารถเปิดเผยในรายงานประจำปี 
					และรายงานความยั่งยืนขององค์กรได้ ซึ่งหลักการคำนวณการลดก๊าซเรือนกระจกเป็นไปตามมาตรฐานองค์การบริหารจัดการก๊าซเรือนกระจก 
					(องค์การมหาชน)
					<br class="d-xl-block">
					<strong>*&nbsp;</strong>องค์กรใดที่ยังไม่ได้เข้าร่วมโครงการจะไม่สามารถเข้าไปกรอกข้อมูลในสูตรการคำนวณการลดก๊าซเรือนกระจกได้
				</span>
	        </div>
	    </div>


	    {* <div class="main-sec-2">
	        <div class="wrap_care">
	            <div class="max_width_care flex center hide-xs">
	                <div class="left">
	                    <div class="cycle">
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/circle.svg" alt="ring" class="ring"/>
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" class="world"/>

	                        <div class="wrap_6">
	                            <div class="care" inviewspy="200">
	                                <img class="no_1" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                                <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                            </div>
	                            <div class="care" inviewspy="300">
	                                <img class="no_2" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                                <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                            </div>
	                            <div class="care" inviewspy="400">
	                                <img class="no_3" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                                <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                            </div>
	                            <div class="care" inviewspy="500">
	                                <img class="no_4" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                                <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                            </div>
	                            <div class="care" inviewspy="600">
	                                <img class="no_5" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                                <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                            </div>
	                            <div class="care" inviewspy="700">
	                                <img class="no_6" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                                <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="right">
	                    <div class="care_txt">
	                        <div class="title f_bold">
	                            ทุกกิจกรรมสามารถใช้
	                            <p class="hilight f_bold hide-940">6 ปฏิบัติการ</p>
	                            <span class="hilight f_bold show-940"> 6 ปฏิบัติการ </span>
	                            ของ Care the Bear 
	                        </div>
	                        <div class="detail">
	                            มาออกแบบเพื่อประเมินวัดผลเป็นค่า
	                            การลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน 
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="show-xs">
	                <div class="flex center">
	                    <div class="left" inviewspy>
	                        <img src="{$image_url}theme/default/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" />
	                    </div>
	                    <div class="right" inviewspy="100">
	                        <div class="title f_bold">ทุกกิจกรรมสามารถใช้<p class="hilight f_bold hide-940">หลักการ 6 Cares</p><span class="hilight f_bold show-940"> หลักการ 6 Cares </span>ของ Care the Bear </div>
	                        <div class="detail">มาออกแบบเพื่อประเมินวัดผลเป็นค่าการลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน</div>
	                    </div>
	                </div>
	                <div class="wrap_6 flex h-justify wrap">
	                    <div class="care" inviewspy="200">
	                        <img class="no_1" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                        <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_2" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                        <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_3" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                        <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_4" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                        <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_5" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                        <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                    </div>
	                    <div class="care" inviewspy="200">
	                        <img class="no_6" src="{$image_url}theme/default/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                        <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="main-care-the-bear">
	        <div class="container">
	            <span class="title-sec-3" inviewspy="300">สิทธิประโยชน์ใน<br class="d-md-none">การเข้าร่วม Care the Bear</span>
	            <div class="box-content">
	                <div class="single-content" inviewspy="300">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-1.png" alt="">
	                    <span class="text-content">คำนวณ วัดผล ปริมาณการ<br class="d-md-block"> ลดก๊าซเรือนกระจกได้ทันที </span>
	                </div>
	                <div class="single-content" inviewspy="600">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-2.png" alt="">
	                    <span class="text-content">ผลการดำเนินงานนำไปแสดงใน<br class="d-md-block"> Annual Report หรือ<br class="d-md-block">  56-1 One Report ได้          </span>
	                </div>
	                <div class="single-content" inviewspy="900">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-3.png" alt="">
	                    <span class="text-content">นำผลการดำเนินงานยื่นขอใบ<br class="d-md-block"> ประกาศเกียรติคุณ LESS จาก<br class="d-md-block"> องค์การบริหารจัดการก๊าซเรือน<br class="d-md-block"> กระจก (องค์การมหาชน) </span>
	                </div>
	            </div>
	            <div class="box-content">
	                <div class="single-content" inviewspy="300">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-4.png" alt="">
	                    <span class="text-content">เกิดภาพลักษณ์ที่ดีต่อองค์กรใน<br class="d-md-block"> ด้านการบริหารจัดการสิ่งแวดล้อม<br class="d-md-block"> อย่างเป็นรูปธรรม</span>
	                </div>
	                <div class="single-content" inviewspy="600">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-5.png" alt="">
	                    <span class="text-content">เกิดการสร้างจิตสำนึก การมีส่วน<br class="d-md-block"> ร่วมในการดูแลด้านสิ่งแวดล้อม<br class="d-md-block"> ของพนักงานในองค์กร</span>
	                </div>
	                <div class="single-content" inviewspy="900">
	                    <img class="img-content lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-3/content-6.png" alt="">
	                    <span class="text-content">องค์กรสามารถออกแบบแผน<br class="d-md-block"> การลดการใช้งบประมาณ เช่น<br class="d-md-block">ลดค่าไฟฟ้า ค่ากระดาษ</span>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="main-performance">
	        <div class="container">
	            <span class="title-performance" inviewspy>ผลการดำเนินงานโครงการ</span>
	            <span class="year-performance" inviewspy>ตั้งแต่ปี 2018 - ปัจจุบัน</span>
	            <div class="box-content-performance">
	                <div class="left-content">
	                    <div class="list-items" inviewspy>
	                        <span class="icon-building"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">จำนวนพันธมิตร</span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_company|replace:',':''}"></span>
	                                <span class="text-3">องค์กร</span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items" inviewspy="200">
	                        <span class="icon-cloud"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">ลดปริมาณก๊าซเรือนกระจกได้</span>
	                            <div>
	                                <span class="text-2" data-no="{(($bear_stats.total_cf|replace:',':''))|number_format:2:".":""}"></span>
	                                <span class="text-3 ">Kg 
	                                    <span class="hanging-co2-1">CO</span>
	                                    <span class="text-indent-eq">eq</span>
	                                </span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items" inviewspy="400">
	                        <span class="icon-tree"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">เทียบเท่าการดูดซับ 
	                                <span class="hanging-co2-2">CO</span> 
	                                <span class="text-indent-per-year">/ ปี ของต้นไม้</span>
	                            </span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_tree|replace:',':''}"></span>
	                                <span class="text-3">ต้น</span>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="right-content" inviewspy="500">
	                    <img class="lazy-img" data-src="{$image_url}theme/default/public/images/care-the-bear/sec-4/content-1.png" alt="">
	                </div>
	            </div>
	        </div>
	    </div> *}
	    <div class="slide-reciever"></div>
	    <div class="contact-html"></div>
	</div>
{/block}
{block name=script}
	<script src="{$image_url}theme/default/public/js/care-the-bear.js"></script>
{/block}