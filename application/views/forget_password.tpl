{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/form.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/modal.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/register.css">
{/block}
{block name=body}
	<main class="content">
      <div class="page-register">
        <script>
            function check_data()
            {
                document.getElementById("error_msg").style.display = "none"; 
                document.getElementById("email_req").style.display = "none"; 
                
                with(document.add_edit)
                {
                    if(email.value=="")
                    {
                        document.getElementById("email_req").style.display = ""; 
                        document.getElementById("email").focus();
                        return false;
                    }
                }
            }
        </script>
        <form name="add_edit" onsubmit="return check_data();" method="post">
         <div class="container">
            <div class="page-header"> 
               <h1 class="page-header--title f_bold">ลืมรหัสผ่าน</h1>
               <p class="page-header--description f_bold">กรุณาระบุอีเมลที่ลงทะเบียนไว้กับระบบ</p>
            </div>
               </div>
               <div class="block-form" inviewSpy="200">
                 <div id="upload" class="common-form">
                   <fieldset>
                        <div class="wrap-upload flex column">
                           <div class="upload-wrap flex column h-center">
                              <div class="upload-area flex v-center h-justify show">
                                 <span class="block-upload-icon">
                                 </span>
                                 <div class="block-upload-desc flex column">
                                   <div class="wrap-input-text have-icon">
                                     <input class="input-type-text f_reg input-required" type="text" name="email" id="email" autocomplete="new-password" value="" maxlength="50" inputmode="email" data-error="กรุณากรอกข้อมูล,รูปแบบอีเมลไม่ถูกต้อง">
                                     <label class="text-label placeholder flex v-center" for="customer_email"> <span class="txt-placeholder">อีเมล</span> </label>
                                   </div>
                           <p id="email_req" style="display:none; color:red;" class="required">กรุณากรอกอีเมล</p>
                                 </div>
                               </div>
                           </div>
                    {if $error_msg != ""}
                        <p id="error_msg" style="color:red;text-align: center;margin-top: 20px;" class="required">{$error_msg}</p>
                    {else}
                        <p id="error_msg" style="display:none; color:red;" class="required">{$error_msg}</p>
                    {/if}   
            <div class="wrap-action" inviewSpy="300">
               <button type="submit" name="forget_password" value="forget_password" class="button btn-action submit-form" style="border:0px; display: block;">
                  <span class="button_label f_med">Submit</span>
               </button>
            </div>
         </div>
         </form>
      </div>
   </main>
{/block}