{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/dashboard-tgo.css">
{/block}
{block name=js}
    <script>
        const dashboardData = {
            {if $member.is_bear == 'Y'}
                fullname: '{$member.carethebear.company}',
                name: '{$member.carethebear.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethebear.logo}',
                logoImgAlt: '{$member.carethebear.company}',
            {else}
                fullname: '{$member.carethewhale.company}',
                name: '{$member.carethewhale.company}',
                since: '{$smarty.now|date_format:"%Y-%m-%d"|datestring:'th':'date':'short'}',
                logoImg: '{$member.carethewhale.logo}',
                logoImgAlt: '{$member.carethewhale.company}',
            {/if}
            global_warming_progress: {
                greenhouse_gas_reduced: '{$stats.total.cf}',
                tree_planted: '{$stats.total.tree}',
            },
            climate_progress: {
                bear: {
                    climateMember: {if $member.is_bear == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '{$stats.bear.cf}',
                        planting_trees: '{$stats.bear.tree}',
                    }
                },
                whale: {
                    climateMember: {if $member.is_whale == 'Y'}true{else}false{/if},
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        waste_manage: '{$stats.whale.weight}',
                        greenhouse_gas: '{$stats.whale.cf}',
                        planting_trees: '{$stats.whale.tree}',
                    }
                },
                wild: {
                    waste: '0',
                    gas: '0',
                    tree: '0',
                    climateMember: false,
                    year: '{$smarty.now|date_format:"%Y"}',
                    stats: {
                        greenhouse_gas: '0',
                        greenhouse_absorb: '0',
                        trees_planted: '0',
                    }
                }
            }
        };
    </script>
    <script src="{$image_url}theme/default/public/js/dashboard.js?v=1.0"></script>
{/block}
{block name=body}
	<div id="content" class=" flex h-justify">
        <div class="climate-tgo flex column h-top v-center">
            <img class="bg column h-top v-center no-repeat" src="{$image_url}theme/default/public/images/dashboard/bg_tgo.png" alt="bgAlt">
            <div class="tgo-detail flex column v-center">
                <div class="wrap-logoes flex column-xs v-center h-top" inviewSpy="0">
                    <img src="{$image_url}theme/default/public/images/dashboard/climate_collab_logo.png">
                    <img src="{$image_url}theme/default/public/images/dashboard/logo_tgo.png">
                </div>
                <div class="title f_bold" inviewSpy="100">โครงการสนับสนุนกิจกรรมลดก๊าซเรือนกระจก</div>
                <div class="sub-title f_bold" inviewSpy="150">(Low Emission Support Scheme : LESS)</div>
                <div class="text-data f_reg" inviewSpy="200">ส่งเสริมและสนับสนุนให้ทุกภาคส่วนดำเนินกิจกรรมลดก๊าซเรือนกระจก 
                ซึ่งทุกองค์กรสามารถขอรับรองผลการประเมินการลดหรือกักเก็บก๊าซเรือนกระจกจากการดำเนินกิจกรรม เช่น โครงการด้านป่าไม้และการเกษตร โครงการจัดการ
                ของเสียโครงการด้านพลังงาน และโครงการอื่นๆ ได้จากองค์การบริหารจัดการ
                ก๊าซเรือนกระจก (องค์การมหาชน) (อบก.) โดย อบก. จะให้การรับรองในรูปแบบ
                ของใบประกาศเกียรติคุณ<br>(Letter of Recognition : LoR)</div>

                <div class="hero v-center" inviewSpy="300">
                    <img src="{$image_url}theme/default/public/images/dashboard/hero_less.png">
                </div>
                <div class="head f_bold" inviewspy="400">Climate Care Platform ร่วมกับ<br>องค์การบริหารก๊าซเรือนกระจก (อบก.)</div>
                <div class="text-data f_reg" inviewSpy="500">สนับสนุนให้ทุกองค์กรที่เป็นพันธมิตรดำเนินโครงการภายใต้ Climate Care Platform 
                สามารถนำผลการดำเนินงานจากโครงการ Care the Whale โครงการลดก๊าซเรือนกระจก
                จากการบริหารจัดการขยะ โครงการ Care the Wild โครงการปลูกป่าเพื่อดูดซับก๊าซเรือนกระจก
                มายืนขอรับรองผลการประเมินการลด หรือกักเก็บก๊าซเรือนกระจกจากโครงการ Less
                เพื่อรับใบประกาศเกียรติคุณที่ออกโดย อบก. ด้วยวิธีง่ายๆ ดังนี้</div>
            </div>

            <div class="process-less flex column h-top v-center">
                <div class="content-info">
                    <div class="text-header f_bold" inviewSpy="550">ขั้นตอนการขอรับรองผลการประเมิน
                    <br>การลดก๊าซเรือนกระจกโครงการ LESS</div>
                    <img class="bg" inviewSpy="600" src="{$image_url}theme/default/public/images/dashboard/bg_process.png">

                    <div class="icon" inview="700">
                        <div class="icon1"inviewSpy="720">
                        <a href="https://ghgreduction.tgo.or.th/th/user-register.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/icon_01.png"></a>
                        <div class="head f_bold">ลงทะเบียน<br><span class="text">สมัครสมาชิกในเว็บไซต์ อบก.</span></div>
                        </div>
                        <div class="icon2"inviewSpy="740">
                        <a href="https://ghgreduction.tgo.or.th/th/download-less/99-less-form.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/icon_02.png"></a>
                        <div class="head f_bold">ดาวน์โหลด<br><span class="text">แบบฟอร์ม<br>การสมัครขอรับรอง</span></div>
                        </div>
                        <div class="icon3"inviewSpy="760">
                        <a href="https://ghgreduction.tgo.or.th/th/calculation/less-calculate-document.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/icon_03.png"></a>
                        <div class="head f_bold">ดาวน์โหลด<br><span class="text">เอกสารการคำนวณ<br>ก๊าซเรือนกระจก</span></div>
                        </div>
                        <div class="icon4"inviewSpy="780">
                        <img src="{$image_url}theme/default/public/images/dashboard/icon_04.png">
                        <div class="head f_bold" style="margin-top: -30px;" >กรอกแบบฟอร์ม<br><span class="text">สมัครขอรับรอง<br>ผลการดำเนินงาน<br>ของโครงการ</span></div>
                        </div>
                        <div class="icon5"inviewSpy="800">
                        <a href="https://ghgreduction.tgo.or.th/th/register.html?view=form&layout=form&form_id=1" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/icon_05.png"></a>
                        <div class="head f_bold">ยื่นเอกสารโครงการ<br><span class="text">ที่เว็บไซต์ อบก.</span></div>
                        </div>
                        <div class="icon6"inviewSpy="820">
                        <a href="https://ghgreduction.tgo.or.th/th/register.html?view=datas" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/icon_06.png"></a>
                        <div class="head f_bold">ติดตามสถานะ<br><span class="text">ผลการรับรอง<br>ทางเว็บไซต์ อบก.</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="banner-more flex column h-top v-center"inviewSpy="900">
                <div class="head f_bold">รายละเอียดและเงื่อนไขเพิ่มเติม</div>
            </div>

            <div class="content-card"inviewSpy="920">
                <div class="card flex column-xs v-center h-top">
                        <a href="https://ghgreduction.tgo.or.th/th/about-less/about-less1.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/more-card_1.png"></a>
                        <a href="https://ghgreduction.tgo.or.th/th/about-less/less-conditions.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/more-card_2.png"></a>
                        <a href="https://ghgreduction.tgo.or.th/th/about-less/less-process.html" target="_blank"><img src="{$image_url}theme/default/public/images/dashboard/more-card_3.png"></a>
                </div>
                {* <div class="text f_bold">
                        <a style="margin-left: 80px;">Less คืออะไร</a>
                        <a style="margin-left: 148px;">ข้อกำหนดและเงื่อนไข</a>
                        <a style="margin-left: 95px;">ขั้นตอนการรับรอง LESS</a>
                    </div> *}
            </div>

            
        </div>

          

        
    </div>
{/block}