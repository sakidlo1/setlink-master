{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/news.css">
{/block}
{block name=js}
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/news.js"></script>
    <script>
        function loadCSS(e, n, o, t) { "use strict"; if (e.trim().length <= 0) { return; } var d = window.document.createElement("link"), i = n || window.document.getElementsByTagName("script")[0], s = window.document.styleSheets; return d.rel = "stylesheet", d.href = e, d.media = "only x", t && (d.onload = t), i.parentNode.insertBefore(d, i), d.onloadcssdefined = function (n) { for (var o, t = 0; t < s.length; t++)s[t].href && s[t].href.indexOf(e) > -1 && (o = !0); o ? n() : setTimeout(function () { d.onloadcssdefined(n) }) }, d.onloadcssdefined(function () { d.media = o || "all" }), d };
        function loadJS(e, a) { if (e.length <= 0) { return; } var t = document.getElementsByTagName("head")[0], n = document.createElement("script"); n.type = "text/javascript", n.src = e, n.async = !0, n.onreadystatechange = a, n.onload = a, t.appendChild(n) }
    </script>
{/block}
{block name=body}
	<div id="content">
        <h1 class=" news-title t_40 f_bold" inviewspy="300">ข่าวประชาสัมพันธ์กับกิจกรรม</h1>
        <div inviewspy="300">
            <div class="wrap-scroll-menu flex center">
                <div class="menu-cate-news">
                    <span class="cate-news f_med active" data-cate="all" >โครงการทั้งหมด</span>
                    <span class="cate-news f_med" data-cate="1" >Care the Bear</span>
                    <span class="cate-news f_med" data-cate="2" >Care the Whale</span>
                    <span class="cate-news f_med" data-cate="3" >Care the Wild</span>
                </div>
            </div>
        </div>
        <div class="wrap-list-news flex wrap h-justify"><div class="w_100 flex center" inviewspy="300"><div class="loadmore loading button f_med"><span class="button_label">ดูเพิ่ม</span></div></div></div>
    </div>
{/block}