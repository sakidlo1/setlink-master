{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/reset.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home_banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home_objective.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home_privilege.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home_vdo.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home-news.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home-list-organi.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home_register.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/simpledropdown.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/home-summary-progress.css">
    <link rel="stylesheet" href="{$image_url}theme/default/public/css/contact-section.css">
{/block}
{block name=js}
    <script src="{$image_url}theme/default/public/js/player.js"></script>
    <script src="{$image_url}theme/default/public/js/simpledropdown.js"></script>
    <script src="{$image_url}theme/default/public/js/swiper.js"></script>
    <script src="{$image_url}theme/default/public/js/contact-section.js"></script>
    <script src="{$image_url}theme/default/public/js/home-banner.js"></script>
    <script>
        let vdo_slider_data = [
            {
                cate: 'all',
                thumbImg: '{$vdo[0].cover}',
                thumbImgAlt: '{$vdo[0].name}',
                video: '{$vdo[0].vdo_url}',
                bgImg: '{$image_url}theme/default/public/images/home/sliderVDO/all_bg.jpg',
                bgImgAlt: '{$vdo[0].name}',
                bgImgMobile: '{$image_url}theme/default/public/images/home/sliderVDO/all_bg_mb.jpg',
                bgImgMobileAlt: '{$vdo[0].name}'
            },
            {
                cate: 'bear',
                thumbImg: '{$vdo[1].cover}',
                thumbImgAlt: '{$vdo[1].name}',
                video: '{$vdo[1].vdo_url}',
                bgImg: '{$image_url}theme/default/public/images/home/sliderVDO/bear_bg.png',
                bgImgAlt: '{$vdo[1].name}',
                bgImgMobile: '{$image_url}theme/default/public/images/home/sliderVDO/bear_bg_mb.jpg',
                bgImgMobileAlt: '{$vdo[1].name}'
            },
            {
                cate: 'whale',
                thumbImg: '{$vdo[2].cover}',
                thumbImgAlt: '{$vdo[2].name}',
                video: '{$vdo[2].vdo_url}',
                bgImg: '{$image_url}theme/default/public/images/home/sliderVDO/whale_bg.jpg',
                bgImgAlt: '{$vdo[2].name}',
                bgImgMobile: '{$image_url}theme/default/public/images/home/sliderVDO/whale_bg_mb.jpg',
                bgImgMobileAlt: '{$vdo[2].name}'
            },
            {
                cate: 'elephant',
                thumbImg: '{$vdo[3].cover}',
                thumbImgAlt: '{$vdo[3].name}',
                video: '{$vdo[3].vdo_url}',
                bgImg: '{$image_url}theme/default/public/images/home/sliderVDO/elephant_bg.jpg',
                bgImgAlt: '{$vdo[3].name}',
                bgImgMobile: '{$image_url}theme/default/public/images/home/sliderVDO/elephant_bg_mb.jpg',
                bgImgMobileAlt: '{$vdo[3].name}'
            }
        ];
        let dataNews = [
            {foreach $news as $news_item}
            {
                date: '{$news_item.created_on|datestring:'th':'date':'short'}',
                title: '{$news_item.name}',
                link: base_url + 'news/detail/{$news_item.id}',
                image: '{$news_item.thumbnail}',
                imageAlt: '{$news_item.name}'
            },
            {/foreach}
        ];
        const bear = [
            {foreach $company_bear as $company_bear_item}
            '{$company_bear_item.logo}',
            {/foreach}
        ];
        const elephant = [
            {foreach $company_wild as $company_wild_item}
            '{$company_wild_item.logo}',
            {/foreach}
        ];
        const whale = [
            {foreach $company_whale as $company_whale_item}
            '{$company_whale_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear, ...elephant, ...whale
        ];
    </script>
    <script src="{$image_url}theme/default/public/js/home-vdo.js"></script>
    <script src="{$image_url}theme/default/public/js/home-news.js"></script>
    <script src="{$image_url}theme/default/public/js/home-list-organi.js"></script>
    <script src="{$image_url}theme/default/public/js/home-register.js"></script>
    <script src="{$image_url}theme/default/public/js/home-summary-progress.js"></script>
    <script src="{$image_url}theme/default/public/js/home-contact.js"></script>
{/block}
{block name=body}
    <div class="ocean_bg">
        <img class="hide_556" src='{$image_url}theme/default/public/images/home/banner/ocean_dt.png' alt="ocean">
        <img class="show_556" src="{$image_url}theme/default/public/images/home/banner/ocean_mb.png" alt="ocean">
    </div>
    <div class="wrap_content_banner">
        <div class="title f_bold" inviewspy="200">Climate Care <br class="show-xs">Collaboration Platform</div>
        <div class="desc f_med" inviewspy="200">สามสหายสายกรีน รวมพลังบวกกู้วิกฤตโลกร้อน
            <br>โครงการความร่วมมือการดูแลจัดการสิ่งแวดล้อม<br class="show-xs">อย่างยั่งยืน<br
                    class="hide-xs">มีเป้าหมายลดก๊าซเรือนกระจกและบริหารจัดการ<br class="hide-xs">สิ่งแวดล้อมอย่างสมดุล
        </div>
    </div>
    <div class="wrap_cloud">
        <div class="cloud_canvas">
            <img class="cloud lazy-img" data-src="{$image_url}theme/default/public/images/home/banner/cloud01.png"
                 alt="cloud">
            <img class="cloud lazy-img" data-src="{$image_url}theme/default/public/images/home/banner/cloud02.png"
                 alt="cloud">
            <img class="cloud lazy-img" data-src="{$image_url}theme/default/public/images/home/banner/cloud03.png"
                 alt="cloud">
        </div>
    </div>
    <div id="banner" class="banner">
        <div class="wrap_img_banner">
            <div class="relative_frame">
                <img class="background hide-xs" src="{$image_url}theme/default/public/images/home/banner/banner.png"
                     alt="background">
                <img class="background show-xs" src="{$image_url}theme/default/public/images/home/banner/banner_mb.png"
                     alt="background">

            </div>
        </div>
        <div class="wrap_img_banner">
            <div class="relative_frame frame_character" data-elm="">
                <div class="dim"></div>
                <div class="wrap_character char_bear" inviewspy>
                    <div class="wrap_bubble">
                        <div class="flex">
                            <div class="bubble cate_bear">
                                <p class="title f_med">Care the Bear</p>
                                <p class="desc f_med">โครงการลดก๊าซเรือนกระจก<br class="hide-940">จากการจัดงานหรือ<br
                                            class="hide-940">กิจกรรมขององค์กร</p>
                            </div>
                            <div class="floating_btn">
                                <div class="btn_ring">
                                    <span class="icon_plus icon-plus"></span>
                                </div>
                            </div>
                            <div class="box_detail detail_right">
                                <div class="wrap_box cate_bear">
                                    <div class="title f_bold">
                                        Care the Bear
                                        <div class="wrap_close_btn">
                                            <div class="close_btn show-940">
                                                <span class="icon icon-cross"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail f_med">
                                        <div class="wrap_activity">
                                            <img class="lazy-img detail_bg"
                                                 data-src="{$image_url}theme/default/public/images/home/banner/ice-ocean.png"
                                                 alt="IceOcean">
                                            <div class="activity flex v-center">
                                                <span class="icon icon-building"></span>
                                                <div>
                                                    <p class="activity_head f_med">จำนวนพันธมิตร</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$bear_stats.total_company|replace:',':''}"></span>
                                                        องค์กร</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-tree"></span>
                                                <div>
                                                    <p class="activity_head f_med">เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี
                                                        ของต้นไม้</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$bear_stats.total_tree|replace:',':''}"></span>
                                                        ต้น</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-cloud"></span>
                                                <div>
                                                    <p class="activity_head f_med">ลดปริมาณก๊าซเรือนกระจกได้</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{(($bear_stats.total_cf|replace:',':'') / 1000)|number_format:2:".":""}"></span>
                                                        ton CO<sub>2</sub> eq</p>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{$base_url}carethebear" target="_self" class="cate_button bear f_med">ดูเพิ่มเติม</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="character">
                        <img class="lazy-img bear"
                             data-src="{$image_url}theme/default/public/images/home/banner/bear.png" alt='bear'>
                        <img class="lazy-img sign"
                             data-src="{$image_url}theme/default/public/images/home/banner/no-foam-sign.png"
                             alt='bear sign'>
                    </div>
                </div>
                <div class="wrap_character char_whale" inviewspy="200">
                    <div class="wrap_bubble">
                        <div class="flex">
                            <div class="bubble cate_whale">
                                <p class="title f_med">Care the Whale</p>
                                <p class="desc f_med">โครงการลดก๊าซเรือนกระจก<br
                                            class="hide-940">จากการบริหารจัดการขยะ<br class="hide-940">ตั้งแต่<br
                                            class="show-940">ต้นทางถึงปลายทาง</p>
                            </div>
                            <div class="floating_btn">
                                <div class="btn_ring">
                                    <span class="icon_plus icon-plus"></span>
                                </div>
                            </div>
                            <div class="box_detail detail_right">
                                <div class="wrap_box cate_whale">
                                    <div class="title f_bold">
                                        Care the Whale
                                        <div class="wrap_close_btn">
                                            <div class="close_btn show-940">
                                                <span class="icon icon-cross"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail f_med">
                                        <div class="wrap_activity">
                                            <img class="lazy-img detail_bg"
                                                 data-src="{$image_url}theme/default/public/images/home/banner/under-sea.png"
                                                 alt="undersea">
                                            <div class="activity flex v-center">
                                                <span class="icon icon-building"></span>
                                                <div>
                                                    <p class="activity_head f_med">สมาชิกหรือลูกบ้านในเครือข่าย</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$whale_stats.total_company|replace:',':''}"></span>
                                                        องค์กร</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-tree"></span>
                                                <div>
                                                    <p class="activity_head f_med">ปริมาณขยะที่บริหารจัดการรวม</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{(($whale_stats.total_weight|replace:',':'') / 1000)|number_format:2:".":""}"></span>
                                                        ตัน</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-cloud"></span>
                                                <div>
                                                    <p class="activity_head f_med">ลดปริมาณก๊าซเรือนกระจกได้</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{(($whale_stats.total_cf|replace:',':'') / 1000)|number_format:2:".":""}"></span>
                                                        ton CO<sub>2</sub> eq</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-tree"></span>
                                                <div>
                                                    <p class="activity_head f_med">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี
                                                        ของต้นไม้</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$whale_stats.total_tree|replace:',':''}"></span>
                                                        ต้น</p>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{$base_url}care-the-whale" target="_self"
                                           class="cate_button whale f_med">ดูเพิ่มเติม</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="character whale">
                        <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/banner/whale.png"
                             alt='whale'>
                        <img class="lazy-img wave"
                             data-src="{$image_url}theme/default/public/images/home/banner/whale-water-wave.png"
                             alt='whale wave'>
                        <img class="lazy-img waste"
                             data-src="{$image_url}theme/default/public/images/home/banner/water-waste.png"
                             alt='whale waste'>
                    </div>
                </div>
                <div class="wrap_character char_elephant" inviewspy="400">
                    <div class="wrap_bubble">
                        <div class="flex">
                            <div class="bubble cate_elephant">
                                <p class="title f_med">Care the Wild</p>
                                <p class="desc f_med">โครงการปลูกป่าดูดซับ<br>ก๊าซเรือนกระจก<br
                                            class="hide-940">สร้างสมดุลให้ระบบนิเวศ</p>
                            </div>
                            <div class="floating_btn">
                                <div class="btn_ring">
                                    <span class="icon_plus icon-plus"></span>
                                </div>
                            </div>
                            <div class="box_detail detail_left">
                                <div class="wrap_box cate_elephant">
                                    <div class="title f_bold">
                                        Care the Wild
                                        <div class="wrap_close_btn">
                                            <div class="close_btn show-940">
                                                <span class="icon icon-cross"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="detail f_med">
                                        <div class="wrap_activity">
                                            <img class="lazy-img detail_bg"
                                                 data-src="{$image_url}theme/default/public/images/home/banner/jungle.png"
                                                 alt="jungle">
                                            <div class="activity flex v-center">
                                                <span class="icon icon-trees"></span>
                                                <div>
                                                    <p class="activity_head f_med">ปลูกป่าแล้ว</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$wild_stats.summary_stat_place|replace:',':''}"></span>
                                                        แห่ง รวม <span
                                                                class="running-no XL f_bold"
                                                                data-no="{$wild_stats.summary_stat_rai|replace:',':''}"></span>
                                                        ไร่</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-tree"></span>
                                                <div>
                                                    <p class="activity_head f_med">เพิ่มต้นไม้จำนวนกว่า</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$wild_stats.summary_stat_tree|replace:',':''}"></span>
                                                        ต้น</p>
                                                </div>
                                            </div>
                                            <div class="activity flex v-center">
                                                <span class="icon icon-cloud"></span>
                                                <div>
                                                    <p class="activity_head f_med">ลดปริมาณก๊าซเรือนกระจกได้</p>
                                                    <p class="activity_value f_med"><span class="running-no XL f_bold"
                                                                                          data-no="{$wild_stats.summary_stat_co2|replace:',':''}"></span>
                                                        kg CO<sub>2</sub> eq / ต่อปี</p>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="{$base_url}care-the-wild" target="_self"
                                           class="cate_button elephant f_med">ดูเพิ่มเติม</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="character">
                        <img class="lazy-img elephant"
                             data-src="{$image_url}theme/default/public/images/home/banner/elephant.png" alt='elephant'>
                        <img class="lazy-img elephant tree"
                             data-src="{$image_url}theme/default/public/images/home/banner/tree.png"
                             alt='elephant tree'>
                        <img class="lazy-img elephant arm"
                             data-src="{$image_url}theme/default/public/images/home/banner/elephant-arm.png"
                             alt='elephant arm'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="summary-progress" class="summary-progress flex center column-xs">
        <div class="progress" inviewspy="300">
            <div class="wrap-img">
                <img src="{$image_url}theme/default/public/images/home/summary-progress/air-pollution.png"
                     alt="air pollution">
            </div>
            <div class="wrap-detail">
                <div class="txt flex wrap column f_med">
                    ลดปริมาณก๊าซเรือนกระจกได้
                    <span>
                        <span class="running-no t_60 t_yellow f_bold"
                              data-no="{((($bear_stats.total_cf|replace:',':'') + ($whale_stats.total_cf|replace:',':'') + ($wild_stats.summary_stat_co2|replace:',':'')) / 1000)|number_format:2:".":""}"></span><span
                                class="f_reg">ton CO<sub>2</sub> eq</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="progress" inviewspy="400">
            <div class="wrap-img">
                <img src="{$image_url}theme/default/public/images/home/summary-progress/trees.png" alt="trees">
            </div>
            <div class="wrap-detail">
                <div class="txt flex wrap column f_med">
                    เทียบเท่าการปลูกต้นไม้ใหญ่ อายุ 10 ปี
                    <span>
                        <span class="running-no t_60 t_yellow f_bold"
                              data-no="{(($bear_stats.total_tree|replace:',':'') + ($whale_stats.total_tree|replace:',':'') + ($wild_stats.summary_stat_tree|replace:',':''))}"></span><span
                                class="f_reg">ต้น</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div id="objective" class="section_objective">
        <div class="section-title f_bold" inviewspy="200">สอดคล้องกับ<br class="show-xs">เป้าประสงค์ SDGs</div>
        <div class="wrap_list_objective">
            <div class="objective" inviewspy>
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/objective/objective-1.png"
                     alt="obj 1">
                <p class="obj_title f_med">รับรองแผนการบริโภค<br class="hide-xs">และ<br
                            class="show-xs">การผลิตที่ยั่งยืน</p>
            </div>
            <div class="objective" inviewspy="300">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/objective/objective-2.png"
                     alt="obj 2">
                <p class="obj_title f_med">ดำเนินมาตรการเร่งด่วนเพื่อรับมือ<br
                            class="hide-xs">การเปลี่ยนแปลงสภาพภูมิอากาศ<br class="hide-xs">และผลกระทบ</p>
            </div>
            <div class="objective" inviewspy="600">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/objective/objective-3.png"
                     alt="obj 3">
                <p class="obj_title f_med">สร้างพลังแห่งการเป็นหุ้นส่วน<br class="hide-xs">ความร่วมมือระดับสากลต่อ<br
                            class="hide-xs">การพัฒนาที่ยั่งยืน</p>
            </div>
        </div>
        <div class="section-detail f_reg" inviewspy="100">ด้วยรูปแบบการดำเนินงานด้าน<br class="show-xs">สิ่งแวดล้อมที่ตอบ<span
                    class="HL f_bold">โจทย์ทุกองค์กรในการ<br
                        class="show-xs">ลดก๊าซเรือนกระจกที่เริ่มได้ทันทีและเป็นรูปธรรม</span> <br
                    class="show-xs">ผลลัพธ์การงานวัดผลเป็น<br class="hide-xs">ค่าการลดก๊าซเรือนกระจก<br
                    class="show-xs">ซึ่งเป็นไปตามมาตราฐานของ<br class="hide-xs">องค์การบริหารจัดการก๊าซเรือนกระจก (อบก.)
        </div>
        <div class="section-detail f_reg" inviewspy="300">มาเริ่มต้นดูแลสิ่งแวดล้อมไปด้วยกันตามแนวทางความยั่งยืน<br
                    class="hide-xs">กับ
            <span class="HL f_bold">Climate Care Collaboration Platform</span>
        </div>
    </div>
    <div id="privilege" class="section_privilege">
        <img class="lazy-img bg hide-xs" data-src="{$image_url}theme/default/public/images/home/privilege/bg_dt.jpg"
             alt="bgAlt">
        <img class="lazy-img bg show-xs" data-src="{$image_url}theme/default/public/images/home/privilege/bg_mb.jpg"
             alt="bgAlt">
        <div class="section-title f_bold" inviewspy>สิทธิประโยชน์<br class="show-xs">การเข้าร่วมโครงการ</div>

        <div class="wrap_list_privilege">
            <div class="privilege" inviewspy>
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/privilege/privi-1.png"
                     alt="privi 1">
                <p class="privi_title f_med">คำนวณ วัดผล ปริมาณการ<br class="hide-xs">ลดก๊าซเรือนกระจกได้ทันที</p>
            </div>
            <div class="privilege" inviewspy="200">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/privilege/privi-2.png"
                     alt="privi 2">
                <p class="privi_title f_med">ผลการดำเนินงานนำไปแสดงใน <br class="hide-xs">Annual Report หรือ<br>56-1 One
                    Report ได้</p>
            </div>
            <div class="privilege" inviewspy="200">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/privilege/privi-3.png"
                     alt="privi 3">
                <p class="privi_title f_med">นำผลการดำเนินงานยื่นขอ<br class="hide-xs">ใบประกาศเกียรติคุณ LESS จาก<br
                            class="hide-xs">องค์การบริหารก๊าซเรือนกระจกได้</p>
            </div>
            <div class="privilege" inviewspy="200">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/privilege/privi-4.png"
                     alt="privi 4">
                <p class="privi_title f_med">เกิดการสร้างจิตสำนึก การมีส่วน<br
                            class="hide-xs">ร่วมในการดูแลด้านสิ่งแวดล้อม <br class="hide-xs">ของพนักงานในองค์กร</p>
            </div>
            <div class="privilege" inviewspy="200">
                <img class="lazy-img" data-src="{$image_url}theme/default/public/images/home/privilege/privi-5.png"
                     alt="privi 5">
                <p class="privi_title f_med">เกิดภาพลักษณ์ที่ดีต่อองค์กรใน<br
                            class="hide-xs">ด้านการบริหารจัดการสิ่งแวดล้อม<br class="hide-xs">อย่างเป็นรูปธรรม</p>
            </div>
        </div>
    </div>
    <div class="section_vdo_slider">
        <div class="section-vdo-content flex column center">
            <div class="wrap_bg" data-cls="cut">
            </div>
            <div class="section-title f_bold" inviewspy="200">
                <div class="t_yellow">Climate Care Collaboration</div>
                <p class="t_30">รวมพลังบวกกู้วิกฤตโลกร้อน</p>
            </div>
            <div class="wrap_slider" inviewspy="200">
                <div class="max_width_section">
                    <div class="wrap_group_slider">
                        <div class="main swiper swiper1 fade_slider"
                             style="cursor: url('{$image_url}theme/default/public/images/home/sliderVDO/cursor_play.svg') 40 40, pointer">
                            <div class="swiper-wrapper wrap_slides"></div>
                            <div class="arrow back">
                                <span class="icon icon-arrow-left"></span>
                            </div>
                            <div class="arrow next">
                                <span class="icon icon-arrow-right"></span>
                            </div>
                        </div>
                        <div class="bg_slide_1 hide-xs">
                            <div class="swiper swiper2 fade_slider">
                                <div class="swiper-wrapper wrap_slides"></div>
                            </div>
                        </div>
                        <div class="bg_slide_2 hide-xs">
                            <div class="swiper swiper3 fade_slider">
                                <div class="swiper-wrapper wrap_slides"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-news flex v-top">
        <div class="wrap_slider flex">
            <div class="slide_panel" inviewspy>
                <div class="section-title f_bold">ติดตามข่าว<br class="hide-xs"/>ประชาสัมพันธ์<br
                            class="hide-xs"/>กับกิจกรรม
                </div>
                <div class="wrap_arrow flex">
                    <div class="button" inviewspy="200">
                        <a href="{$base_url}news" target="_blank" class="button_label f_med">ดูทั้งหมด</a>
                    </div>
                </div>
            </div>
            <div class="slider_news" inviewspy>
                <div class="swiper">
                    <div class="swiper-wrapper wrap_slides"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="list-organization" class="section_img_organi">
        <div class="wrap_inner_section">
            <div class="wrap_top_bar flex h-justify v-center" inviewspy>
                <div class="organi_title f_bold">
                    พันธมิตร<br class="show_375"/>และองค์กรที่เข้าร่วม
                </div>
                <div class="button">
                    <span target="_blank" class="button_label f_med">ดูทั้งหมด</span>
                </div>
            </div>
            <div class="swiper slider_organization" inviewspy>
                <div class="swiper-wrapper wrap_slides "></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    <div class="section-register">
        <div class="wrap_img">
            <img class="lazy-img hide-xs" data-src="{$image_url}theme/default/public/images/home/register/bg.jpg"
                 alt='grass bg'>
            <img class="lazy-img show-xs" data-src="{$image_url}theme/default/public/images/home/register/bg_mb.jpg"
                 alt='grass bg'>
        </div>
        <div class="wrap_detail flex column center">
            <p class="register_txt f_bold" inviewspy="200">สามสหายชวนสายกรีน<br>รวมพลังบวกกู้วิกฤตโลกร้อน</p>
            <div class="button" inviewspy="200">
                <a href="{$base_url}member/register" class="button_label f_med">สมัครเข้าร่วมโครงการ</a>
            </div>
        </div>
    </div>
    <div class="section-contact"></div>
{/block}