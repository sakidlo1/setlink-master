{if $total_page > 1}
<div class="paging">
	<div class="inner">
		{if $current_page > 1}
		<a href="{$back_page}" class="prev"><i class="fa fa-angle-left"></i></a>
		{else}
		<a href="{$url}1{$get}" class="prev"><i class="fa fa-angle-left"></i></a>
		{/if}
		{if $total_page <= 11}
			{section name=foo start=1 loop=($total_page+1) step=1}
			  {if $smarty.section.foo.index != $current_page}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no">{$smarty.section.foo.index}</a>
			  {else}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no active">{$smarty.section.foo.index}</a>
			  {/if}
			{/section}
		{elseif $total_page > 11 && $current_page<=6}
			{section name=foo start=1 loop=(($current_page+(5+(6-$current_page)))+1) step=1}
			  {if $smarty.section.foo.index != $current_page}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no">{$smarty.section.foo.index}</a>
			  {else}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no active">{$smarty.section.foo.index}</a>
			  {/if}
			{/section}
		{elseif $total_page > 11 && ($total_page-$current_page)<5}
			{section name=foo start=(($current_page-5)-(5-($total_page-$current_page))) loop=($total_page+1) step=1}
			  {if $smarty.section.foo.index != $current_page}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no">{$smarty.section.foo.index}</a>
			  {else}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no active">{$smarty.section.foo.index}</a>
			  {/if}
			{/section}
		{elseif $total_page > 11 && ($total_page-$current_page)>=5}
			{section name=foo start=($current_page-5) loop=($current_page+6) step=1}
			  {if $smarty.section.foo.index != $current_page}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no">{$smarty.section.foo.index}</a>
			  {else}
			  <a href="{$url}{$smarty.section.foo.index}{$get}" class="no active">{$smarty.section.foo.index}</a>
			  {/if}
			{/section}
		{/if}
		{if $current_page < $total_page}
		<a href="{$next_page}" class="next"><i class="fa fa-angle-right"></i></a>
		{else}
		<a href="{$url}{$total_page}{$get}" class="next"><i class="fa fa-angle-right"></i></a>
		{/if}
	</div>
</div>
{/if}