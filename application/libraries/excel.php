<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/Excel/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class excel extends Spreadsheet {

    function __construct()
    {
    	parent::__construct();
		return new Spreadsheet();
    }

    function save($data, $filename)
    {
    	$writer = new Xlsx($data);

    	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		ob_end_clean();
		$writer->save('php://output');
    }
}
 
?>