<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class authen_member {

    public $id;
    public $member_data;
    public $controller;
    public $function;
    public $is_login;
    public $lang;
    public $lang_code;
    
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->helper('url');
		
		if($CI->input->get('JWT') != '')
		{
			$token = $CI->input->get('JWT');
			$authorization = "Authorization: Bearer ".$token;
            $url = config_item('api_get_profile');

			/*if(config_item('base_url') == 'https://climate.setsocialimpact.com/')
			{
				$url = config_item('api_get_profile');
			}
			else
			{
				$url = 'https://test.setlink.set.or.th/api/user/profile';
			}*/

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$res = curl_exec($ch);
			curl_close($ch);
			$result = json_decode($res, true);

			if(@$result['id'] > 0)
			{
				// TODO : Check Email exists in carethebear & carethewhale before set SESSION
                // TODO 12/12/2023 = Sun
                if(!empty($result['email'])){
                    $CI->load->model('member_model','this_model');
                    $email = $result['email'];
                    $email_exits = $CI->this_model->check_email_exists($email);

                    if ($email_exits) {
                        $user = $CI->this_model->getUserByEmail($email);
                        if ($user) {
                            $member = $CI->this_model->loginByEmail($email);
                            if (@$member['id'] > 0) {
                                $_SESSION['climate_member'] = $member;

                                if (@$member['carethebear']['id'] > 0) {
                                    $_SESSION['carethebear_member'] = $member['carethebear'];
                                }

                                if (@$member['carethewhale']['id'] > 0) {
                                    $_SESSION['member'] = $member['carethewhale'];
                                }

                                //echo json_encode(['status' => 'success']);
                                redirect('/');


                            } else {
                                //echo $member;
                                echo json_encode(['status' => 'login_fail']);

                            }
                        }
                    } else {
                        //echo "ไม่มี";
                        redirect('member/register');

                    }
                }

				$_SESSION['climate_member'] = $result;
				
				$redirect = explode('?&JWT=', $_SERVER['REQUEST_URI']);
				redirect($redirect[0]);
				exit();
			}
		}
        
        if(@$_SESSION['climate_member']['id'] > 0)
        {
            $this->id = $_SESSION['climate_member']['id'];
            $this->member_data = $_SESSION['climate_member'];
            $this->is_login = true;
        }
        else
        {
            $this->is_login = false;
        }
        
        $this->controller = $CI->uri->segment(1);
        $this->function = $CI->uri->segment(2);
    }
}

?>