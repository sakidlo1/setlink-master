<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_news_category()
    {
        $this->db->select('*');
        $this->db->from('news_category');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('news');
        $this->db->join('news_category', 'news_category.id = news.news_category_id');

        $fCategory = @$this->input->post('columns')[0]['search']['value'];
        if($fCategory != "")
        {
            $this->db->where('news.news_category_id', $fCategory);
        }

        $fName = @$this->input->post('columns')[1]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(news.name LIKE '%".$fName."%' or news.url LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('news.status', $fStatus);
        }

        $this->db->where("news.status <> 'D'");

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('news.*, news_category.name as category', false);
        $this->db->from('news');
        $this->db->join('news_category', 'news_category.id = news.news_category_id');

        $fCategory = @$this->input->post('columns')[0]['search']['value'];
        if($fCategory != "")
        {
            $this->db->where('news.news_category_id', $fCategory);
        }

        $fName = @$this->input->post('columns')[1]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(news.name LIKE '%".$fName."%' or news.url LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('news.status', $fStatus);
        }

        $this->db->where("news.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('news.id asc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->db->select('news.*');
        $this->db->from('news');
        $this->db->where('news.id', $this->uri->segment(4));
        $this->db->where("news.status <> 'D'");
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function insert()
    {
        $data['news_category_id'] = $this->input->post('news_category_id');
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['url'] = $this->input->post('url');
        $data['status'] = $this->input->post('status');
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->authen->username;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->db->insert('news', $data);
    }
    
    function update()
    {
        $data['news_category_id'] = $this->input->post('news_category_id');
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['url'] = $this->input->post('url');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('news', $data);
    }
    
    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where("news.status <> 'D'");
        $this->db->update('news', $data);
    }
}