<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tips_model extends CI_Model {

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function count_all()
    {
        $this->whaledb->select('count(*) as count_rec');
        $this->whaledb->from('tips');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->whaledb->where("(tips.name LIKE '%".$fName."%' or tips.content LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->whaledb->where('tips.status', $fStatus);
        }

        $this->whaledb->where("tips.status <> 'D'");

        $query = $this->whaledb->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->whaledb->select('tips.*');
        $this->whaledb->from('tips');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->whaledb->where("(tips.name LIKE '%".$fName."%' or tips.content LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->whaledb->where('tips.status', $fStatus);
        }

        $this->whaledb->where("tips.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->whaledb->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->whaledb->order_by('tips.id asc');
        }

        if($limit > 0)
        {
            $this->whaledb->limit($limit, $start);   
        }
        
        $query = $this->whaledb->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->whaledb->select('tips.*');
        $this->whaledb->from('tips');
        $this->whaledb->where('tips.id', $this->uri->segment(4));
        $this->whaledb->where("tips.status <> 'D'");
        $query = $this->whaledb->get();
        return $query->row_array();
    }
    
    function insert()
    {
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['content'] = $this->input->post('content');
        $data['status'] = $this->input->post('status');
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->authen->username;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->whaledb->insert('tips', $data);
    }
    
    function update()
    {
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['content'] = $this->input->post('content');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->whaledb->where('id', $this->uri->segment(4));
        $this->whaledb->update('tips', $data);
    }
    
    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->whaledb->where('id', $this->uri->segment(4));
        $this->whaledb->where("tips.status <> 'D'");
        $this->whaledb->update('tips', $data);
    }
}