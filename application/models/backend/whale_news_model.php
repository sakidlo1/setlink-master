<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class whale_news_model extends CI_Model {

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function count_all()
    {
        $this->whaledb->select('count(*) as count_rec');
        $this->whaledb->from('news');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->whaledb->where("(news.name LIKE '%".$fName."%' or news.description LIKE '%".$fName."%' or news.content LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->whaledb->where('news.status', $fStatus);
        }

        $this->whaledb->where("news.status <> 'D'");

        $query = $this->whaledb->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->whaledb->select('news.*');
        $this->whaledb->from('news');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->whaledb->where("(news.name LIKE '%".$fName."%' or news.description LIKE '%".$fName."%' or news.content LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->whaledb->where('news.status', $fStatus);
        }

        $this->whaledb->where("news.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->whaledb->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->whaledb->order_by('news.id asc');
        }

        if($limit > 0)
        {
            $this->whaledb->limit($limit, $start);   
        }
        
        $query = $this->whaledb->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->whaledb->select('news.*');
        $this->whaledb->from('news');
        $this->whaledb->where('news.id', $this->uri->segment(4));
        $this->whaledb->where("news.status <> 'D'");
        $query = $this->whaledb->get();
        return $query->row_array();
    }
    
    function insert()
    {
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['content'] = $this->input->post('content');
        $data['status'] = $this->input->post('status');
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->authen->username;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->whaledb->insert('news', $data);
    }
    
    function update()
    {
        $data['thumbnail'] = $this->input->post('thumbnail');
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['content'] = $this->input->post('content');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->whaledb->where('id', $this->uri->segment(4));
        $this->whaledb->update('news', $data);
    }
    
    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->whaledb->where('id', $this->uri->segment(4));
        $this->whaledb->where("news.status <> 'D'");
        $this->whaledb->update('news', $data);
    }
}