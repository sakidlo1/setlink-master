<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class whale_news_model extends CI_Model {

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function count_all_news()
    {
        $this->whaledb->select('count(*) as count_rec');
        $this->whaledb->from('news');
        $this->whaledb->where('status', 'Y');

        if($this->input->get('q') != "")
        {
            $this->whaledb->where("(name like '%".$this->input->get('q')."%' or description like '%".$this->input->get('q')."%')");
        }

        $query = $this->whaledb->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_all_news($limit,$page)
    {
        $this->whaledb->select('*');
        $this->whaledb->from('news');
        $this->whaledb->where('status', 'Y');

        if($this->input->get('q') != "")
        {
            $this->whaledb->where("(name like '%".$this->input->get('q')."%' or description like '%".$this->input->get('q')."%')");
        }

        $this->whaledb->order_by('id desc');
        $this->whaledb->limit($limit, ($limit*$page)-$limit);
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_news_detail_by_id($id)
    {
        $this->whaledb->select('*');
        $this->whaledb->from('news');
        $this->whaledb->where('status', 'Y');
        $this->whaledb->where('id', $id);
        $query = $this->whaledb->get();
        return $query->row_array();
    }
}