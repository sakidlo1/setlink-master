<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_all($cat, $page)
    {
        $limit = 12;
        $start = (($page * $limit) - $limit);

        $this->db->select('*');
        $this->db->from('new_news');
        $this->db->where('status', 'Y');
        
        if($cat != 'all')
        {
            $this->db->where('news_category_id', $cat);
        }

        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $data = $query->result_array();

        $news = [];
        foreach ($data as $key => $value)
        {
            $news[] = [
                'date' => $this->datestring($value['created_on'], 'date', 'short'),
                'title' => $value['name'],
                'link' => config_item('base_url').'news/detail/'.$value['id'],
                'image' => $value['thumbnail'],
                'imageAlt' => $value['name']
            ];
        }

        return $news;
    }

    function get_news_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('new_news');
        $this->db->where('status', 'Y');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data = $query->row_array();

        $data['tags'] = explode(',', $data['tags']);

        if(@$data['tags'][0] == '')
        {
            $data['tags'] = [];
        }

        $this->db->select('*');
        $this->db->from('news_gallery');
        $this->db->where('news_id', $id);
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        $data['gallery'] = $query->result_array();

        return $data;
    }

    function datestring($string, $format = 'datetime', $type = 'long')
    {
        if($string == '0000-00-00' || $string == '0000-00-00 00:00:00')
        {
            return '-';
        }

        $_date = explode(' ', $string);
        $date = explode('-', $_date[0]);
        $time = array();
        if(@$_date[1] != '' && $format == 'datetime')
        {
            $time = explode(':', $_date[1]);
        }

        $month = array(
            '01' => array(
                'long' => 'มกราคม',
                'short' => 'ม.ค.'
            ),
            '02' => array(
                'long' => 'กุมภาพันธ์',
                'short' => 'ก.พ.'
            ),
            '03' => array(
                'long' => 'มีนาคม',
                'short' => 'มี.ค.'
            ),
            '04' => array(
                'long' => 'เมษายน',
                'short' => 'เม.ย.'
            ),
            '05' => array(
                'long' => 'พฤษภาคม',
                'short' => 'พ.ค.'
            ),
            '06' => array(
                'long' => 'มิถุนายน',
                'short' => 'มิ.ย.'
            ),
            '07' => array(
                'long' => 'กรกฎาคม',
                'short' => 'ก.ค.'
            ),
            '08' => array(
                'long' => 'สิงหาคม',
                'short' => 'ส.ค.'
            ),
            '09' => array(
                'long' => 'กันยายน',
                'short' => 'ก.ย.'
            ),
            '10' => array(
                'long' => 'ตุลาคม',
                'short' => 'ต.ค.'
            ),
            '11' => array(
                'long' => 'พฤศจิกายน',
                'short' => 'พ.ย.'
            ),
            '12' => array(
                'long' => 'ธันวาคม',
                'short' => 'ธ.ค.'
            )
        );

        if(count($time) > 0)
        {
            $datestring = ((int)$date[2]).' '.$month[$date[1]][$type].' '.($date[0] + 543).' '.$time[0].':'.$time[1].' น.';
        }
        else
        {
            $datestring = ((int)$date[2]).' '.$month[$date[1]][$type].' '.($date[0] + 543);
        }

        return $datestring;
    }
}