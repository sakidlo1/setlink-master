<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model {

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function get_news()
    {
        $this->db->select('*');
        $this->db->from('new_news');
        $this->db->where('status', 'Y');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_news_bear()
    {   
        
        $this->beardb->select('*');
        $this->beardb->from('news');
        $this->beardb->where('status', 'Y');
        $this->beardb->order_by('id', 'desc');
        $query = $this->beardb->get();
        return $query->result_array();
    }

    function get_news_whale()
    {   
        
        $this->whaledb->select('*');
        $this->whaledb->from('news');
        $this->whaledb->where('status', 'Y');
        $this->whaledb->order_by('created_on', 'desc');
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_tips_whale()
    {
        
        $this->whaledb->select('*');
        $this->whaledb->from('tips');
        $this->whaledb->where('status', 'Y');
        $this->whaledb->order_by('created_on', 'desc');
        $this->whaledb->limit(6);
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_activity_bear()
    {   
        
        $this->beardb->select('*');
        $this->beardb->from('activity');
        $this->beardb->where('status', 'Y');
        $this->beardb->order_by('id', 'desc');
        $query = $this->beardb->get();
        return $query->result_array();
    }

    function get_vdo()
    {
        $this->db->select('*');
        $this->db->from('vdo');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    

    function get_company_wild()
    {
        $this->db->select('*');
        $this->db->from('company_wild');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_wild_stats()
    {
        $json = file_get_contents(config_item('api_carethewild').'api/get_config');
        $result = json_decode($json, true);
        return $result['config'];
    }

    function get_bear_stats()
    {
        $json = file_get_contents(config_item('base_url').'carethebear/api/get_stats');
        $json=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
        return json_decode($json, true);
    }

    function get_whale_stats()
    {
        //$json = file_get_contents('https://climatecare.setsocialimpact.com/carethewhale/api/get_stats');
        $json = file_get_contents(config_item('base_url').'carethewhale/api/get_stats');
        $json=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
        return json_decode($json, true);
    }

    function get_bear_company()
    {
        //$json = file_get_contents('https://www.carethebear.com/api/get_company');
        $json = file_get_contents(config_item('base_url').'carethebear/api/get_company');
        $json=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
        return json_decode($json, true);
    }

    function get_whale_company()
    {
        //$json = file_get_contents('https://climatecare.setsocialimpact.com/carethewhale/api/get_company');
        $json = file_get_contents(config_item('base_url').'carethewhale/api/get_company');
        $json=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
        return json_decode($json, true);
    }
}

