<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class member_model extends CI_Model
{

    private $ci;
    private $beardb;
    private $whaledb;
    private $care_master;


    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->care_master = $this->ci->load->database('caremaster', TRUE);

    }

    function getUserByEmail($email)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $this->beardb->where('status', 'Y');
        $query = $this->beardb->get();

        return $query->row();  // ส่งคืนผลลัพธ์เป็นแถวเดียว
    }

    function get_profile($email)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $this->beardb->where('status', 'Y');
        $query = $this->beardb->get();
        $bear = $query->row_array();

        $this->whaledb->select('company_user.*, company.is_accept, company.logo, company.name as company, company.address, company.tel1, company.tel2');
        $this->whaledb->from('company_user');
        $this->whaledb->join('company', 'company.id = company_user.company_id');
        $this->whaledb->where('company_user.email', $email);
        $this->whaledb->where('company_user.status', 'Y');
        $this->whaledb->where('company.status', 'Y');
        $query = $this->whaledb->get();
        $whale = $query->row_array();

        $data = array();
        if (@$bear['id'] > 0) {
            $data = $bear;
        } else if (@$whale['id'] > 0) {
            $data = $whale;
        }
        return $data;
    }

    function update_profile()
    {
        $data = array();
        $current_password = $this->input->post('customer_current_password');
        $old_password = $this->input->post('customer_old_password');

        $memberPassword = $this->get_stored_password();
        $hashOldPassword = md5($old_password);
        if ($hashOldPassword !== $memberPassword) {
            echo "old_password_error";
            return;


        }


        $data['position'] = $this->input->post('customer_position');
        $data['department'] = $this->input->post('customer_department');
        $data['address'] = $this->input->post('customer_company_location');
        $data['tel'] = $this->input->post('customer_phone');
        $data['mobile'] = $this->input->post('customer_phone_partner');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->beardb->where('id', $this->authen->id);
        $result = $this->beardb->update('member', $data);

        if ($result) {
            echo "success";
        } else {
            echo json_encode(['status' => 'error', 'message' => 'มีข้อผิดพลาดในการบันทึก']);
        }
    }

    function get_stored_password()
    {

        $this->beardb->select('password');
        $this->beardb->where('id', $this->authen->id);
        $query = $this->beardb->get('member');

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->password;
        } else {
            return '';
        }
    }

    function loginByEmail($email)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $this->beardb->where('status', 'Y');
        $query = $this->beardb->get();
        $bear = $query->row_array();

        $this->whaledb->select('company_user.*, company.is_accept, company.logo, company.name as company, company.address, company.tel1, company.tel2');
        $this->whaledb->from('company_user');
        $this->whaledb->join('company', 'company.id = company_user.company_id');
        $this->whaledb->where('company_user.email', $email);
        $this->whaledb->where('company_user.status', 'Y');
        $this->whaledb->where('company.status', 'Y');
        $query = $this->whaledb->get();
        $whale = $query->row_array();

        if (@$bear['id'] > 0 || $whale['id'] > 0) {
            $member = [];
            if (@$whale['id'] > 0) {
                $member['id'] = $whale['id'];
                $member['name'] = @explode(' ', $whale['name'])[0];
                $member['surname'] = @explode(' ', $whale['name'])[1];
                $member['email'] = $whale['email'];
                $member['is_whale'] = 'Y';

                $data2 = array();
                $data2['last_login'] = date('Y-m-d H:i:s');
                $this->whaledb->where('id', $whale['id']);
                $this->whaledb->update('company_user', $data2);

                $this->whaledb->select('max(year) as year');
                $this->whaledb->from('company_garbage_type');
                $this->whaledb->where('company_id', $whale['company_id']);
                $query = $this->whaledb->get();
                $year = $query->row_array();
                $whale['year_data'] = $year['year'];

                $member['carethewhale'] = $whale;
            } else {
                $member['is_whale'] = 'N';
            }

            if (@$bear['id'] > 0) {
                $member['id'] = $bear['id'];
                $member['name'] = @explode(' ', $bear['name'])[0];
                $member['surname'] = @explode(' ', $bear['name'])[1];
                $member['email'] = $bear['email'];
                $member['username'] = $bear['username'];
                $member['is_bear'] = 'Y';

                $data2 = array();
                $data2['last_login'] = date('Y-m-d H:i:s');
                $this->beardb->where('id', $bear['id']);
                $this->beardb->update('member', $data2);

                $member['carethebear'] = $bear;
            } else {
                $member['is_bear'] = 'N';
            }

            return $member;
        } else {
            return 'not_found';
        }

    }

    function login($email, $password)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $this->beardb->where('status', 'Y');
        $query = $this->beardb->get();
        $bear = $query->row_array();

        $this->whaledb->select('company_user.*, company.is_accept, company.logo, company.name as company, company.address, company.tel1, company.tel2');
        $this->whaledb->from('company_user');
        $this->whaledb->join('company', 'company.id = company_user.company_id');
        $this->whaledb->where('company_user.email', $email);
        $this->whaledb->where('company_user.status', 'Y');
        $this->whaledb->where('company.status', 'Y');
        $query = $this->whaledb->get();
        $whale = $query->row_array();

        if (@$bear['id'] > 0 || $whale['id'] > 0) {
            if (@$bear['password'] == md5($password) || $whale['password'] == md5($password)) {
                $member = [];

                if (@$whale['id'] > 0) {
                    $member['id'] = $whale['id'];
                    $member['name'] = @explode(' ', $whale['name'])[0];
                    $member['surname'] = @explode(' ', $whale['name'])[1];
                    $member['email'] = $whale['email'];
                    $member['is_whale'] = 'Y';

                    $data2 = array();
                    $data2['last_login'] = date('Y-m-d H:i:s');
                    $this->whaledb->where('id', $whale['id']);
                    $this->whaledb->update('company_user', $data2);

                    $this->whaledb->select('max(year) as year');
                    $this->whaledb->from('company_garbage_type');
                    $this->whaledb->where('company_id', $whale['company_id']);
                    $query = $this->whaledb->get();
                    $year = $query->row_array();
                    $whale['year_data'] = $year['year'];

                    $member['carethewhale'] = $whale;
                } else {
                    $member['is_whale'] = 'N';
                }

                if (@$bear['id'] > 0) {
                    $member['id'] = $bear['id'];
                    $member['name'] = @explode(' ', $bear['name'])[0];
                    $member['surname'] = @explode(' ', $bear['name'])[1];
                    $member['email'] = $bear['email'];
                    $member['username'] = $bear['username'];
                    $member['is_bear'] = 'Y';

                    $data2 = array();
                    $data2['last_login'] = date('Y-m-d H:i:s');
                    $this->beardb->where('id', $bear['id']);
                    $this->beardb->update('member', $data2);

                    $member['carethebear'] = $bear;
                } else {
                    $member['is_bear'] = 'N';
                }

                return $member;
            } else {
                return 'login_fail';
            }
        } else {
            return 'not_found';
        }
    }

    function forget_password()
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $this->input->post('email'));
        $query = $this->beardb->get();
        $data = $query->row_array();

        if (@$data['id'] > 0) {
            $salt = md5($this->input->post('email') . '-' . $data['name'] . '-' . time());
            $data2['salt'] = $salt;
            $this->beardb->where('id', $data['id']);
            $this->beardb->update('member', $data2);

            return $salt;
        } else {
            $this->whaledb->select('company_user.*');
            $this->whaledb->from('company_user');
            $this->whaledb->join('company', 'company.id = company_user.company_id');
            $this->whaledb->where('company_user.email', $this->input->post('email'));
            $this->whaledb->where('company_user.status', 'Y');
            $this->whaledb->where('company.status', 'Y');
            $query = $this->whaledb->get();
            $data = $query->row_array();

            if (@$data['id'] > 0) {
                $salt = md5($this->input->post('email') . '-' . time());
                $data2['salt'] = $salt;
                $this->whaledb->where('id', $data['id']);
                $this->whaledb->update('company_user', $data2);

                $data['salt'] = $salt;
                return $data;
            } else {
                return false;
            }
        }
    }

    function get_salt($email)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $query = $this->beardb->get();
        $data = $query->row_array();

        if (@$data['id'] > 0) {
            return $data['salt'];
        } else {
            $this->whaledb->select('company_user.*');
            $this->whaledb->from('company_user');
            $this->whaledb->join('company', 'company.id = company_user.company_id');
            $this->whaledb->where('company_user.email', $email);
            $this->whaledb->where('company_user.status', 'Y');
            $this->whaledb->where('company.status', 'Y');
            $query = $this->whaledb->get();
            $data = $query->row_array();

            if (@$data['id'] > 0) {
                return $data['salt'];
            } else {
                return false;
            }
        }
    }

    function reset_password()
    {
        $data['password'] = md5($this->input->post('password'));
        $data['salt'] = '';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->beardb->where('email', $this->input->get('email'));
        $this->beardb->update('member', $data);

        $data['password'] = md5($this->input->post('password'));
        $data['salt'] = '';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = 0;
        $this->whaledb->where('email', $this->input->get('email'));
        $this->whaledb->where('company_user.status', 'Y');
        $this->whaledb->update('company_user', $data);
    }

    function get_company_type()
    {
        $this->beardb->select('*');
        $this->beardb->from('company_type');
        $this->beardb->where('status', 'Y');
        $this->beardb->order_by('order_on', 'asc');
        $query = $this->beardb->get();
        $company_type = $query->result_array();

        $this->beardb->select('*');
        $this->beardb->from('company_group');
        $this->beardb->where('status', 'Y');
        $this->beardb->order_by('order_on', 'asc');
        $query = $this->beardb->get();
        $company_group = $query->result_array();

        $data = [];
        foreach ($company_type as $key => $value) {
            $data[$key] = $value;
            $data[$key]['item'] = [];

            $_data = [];
            foreach ($company_group as $key2 => $value2) {
                if ($value['id'] == $value2['company_type_id']) {
                    $_data[] = $value2;
                }
            }

            if (count($_data) > 0) {
                $data[$key]['item'] = $_data;
            } else {
                $value['company_type_id'] = $value['id'];
                $value['id'] = 0;
                $data[$key]['item'][] = $value;
            }
        }

        return $data;
    }

    function check_email_exists($email)
    {
        $this->beardb->select('count(*) as count_rec');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $query = $this->beardb->get();

        $data = $query->row_array();
        if ($data['count_rec'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_email_exists_whale($email)
    {
        $this->whaledb->select('count(*) as count_rec');
        $this->whaledb->from('company_user');
        $this->whaledb->where('email', $email);
        $query = $this->whaledb->get();

        $data = $query->row_array();
        if ($data['count_rec'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_username_exists($username)
    {
        $this->beardb->select('count(*) as count_rec');
        $this->beardb->from('member');
        $this->beardb->where('username', $username);
        $query = $this->beardb->get();

        $data = $query->row_array();
        if ($data['count_rec'] > 0) {
            return true;
        } else {
            return false;
        }
    }

    function register($salt)
    {

        $data['email'] = $this->input->post('customer_email');
        $data['username'] = $this->input->post('customer_email');
        $data['password'] = md5($this->input->post('customer_account_password'));
        $data['company'] = $this->input->post('company_name');
        $data['company_en'] = $this->input->post('company_name_en');
        $data['company_code'] = $this->input->post('company_short_name');
        $data['logo'] = '';
        $data['company_type_id'] = explode('-', $this->input->post('simple_dropdown_company_category'))[0];
        $data['company_group_id'] = explode('-', $this->input->post('simple_dropdown_company_category'))[1];
        $data['name'] = $this->input->post('customer_first_name') . ' ' . $this->input->post('customer_last_name');
        $data['position'] = $this->input->post('customer_position');
        $data['department'] = $this->input->post('customer_department');
        $data['address'] = $this->input->post('customer_company_location');
        $data['tel'] = $this->input->post('customer_phone');
        $data['mobile'] = $this->input->post('customer_phone_partner');
        $data['is_set_staff'] = ((explode('-', $this->input->post('simple_dropdown_company_category'))[0] == '1') ? 'Y' : 'N');
        $data['salt'] = $salt;
        $data['status'] = 'N';
        $date = date('Y-m-d H:i:s');
        $data['created_on'] = $date;
        $data['updated_on'] = $date;
        $data['updated_by'] = '-';
        $data['last_login'] = $date;

        $project = $this->input->post('customer_select_project');
        if($project === 'whale') {
            $this->whaledb->insert('member', $data);
            return $this->whaledb->insert_id();
        }
        if(strpos($project,'whale') !== false) {
            $this->whaledb->insert('member', $data);
        }
        if($this->input->post('company_other') === 'yes') {
            $data['company_other'] = 1;
        }
        $this->beardb->insert('member', $data);
        return $this->beardb->insert_id();


    }

    function update_logo($id, $logo)
    {

        $data['logo'] = config_item('base_url') . 'carethebear/images/upload/member/' . ($id % 4000) . '/' . $id . '/' . $logo;
        $this->beardb->where('id', $id);
        $this->beardb->update('member', $data);
    }

    function get_whale_company_type()
    {
        $this->whaledb->select('*');
        $this->whaledb->from('company_type');
        $this->whaledb->where('status', 'Y');
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_zone()
    {
        $this->whaledb->select('*');
        $this->whaledb->from('zone');
        $this->whaledb->where('status', 'Y');
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_garbage_type()
    {
        $this->whaledb->select('*');
        $this->whaledb->from('garbage_type');
        $this->whaledb->where('status', 'Y');
        $query = $this->whaledb->get();
        return $query->result_array();
    }

    function get_bear_data_by_email($email)
    {
        $this->beardb->select('*');
        $this->beardb->from('member');
        $this->beardb->where('email', $email);
        $query = $this->beardb->get();
        return $query->row_array();
    }

    function register_carethewhale($member_data)
    {
        $year = date('Y');
        $now = date('Y-m-d H:i:s');

        $open_date = explode(',', $this->input->post('date_'));

        $data = array();
        $data['company_type_id'] = $this->input->post('simple_dropdown_building');
        $data['zone_id'] = $this->input->post('simple_dropdown_area');
        $data['logo'] = $member_data['logo'];
        $data['name'] = $member_data['company'];
        $data['open_mon'] = ((in_array('mon', $open_date)) ? 'Y' : 'N');
        $data['open_tue'] = ((in_array('tue', $open_date)) ? 'Y' : 'N');
        $data['open_wed'] = ((in_array('wed', $open_date)) ? 'Y' : 'N');
        $data['open_thu'] = ((in_array('thu', $open_date)) ? 'Y' : 'N');
        $data['open_fri'] = ((in_array('fri', $open_date)) ? 'Y' : 'N');
        $data['open_sat'] = ((in_array('sat', $open_date)) ? 'Y' : 'N');
        $data['open_sun'] = ((in_array('sun', $open_date)) ? 'Y' : 'N');
        $data['open_start'] = $this->input->post('time_open') . ':00';
        $data['open_end'] = $this->input->post('time_off') . ':00';
        $data['guest_qty'] = $this->input->post('count_of_contact');
        $data['staff_qty'] = $this->input->post('count_of_new_employee');
        $data['size'] = $this->input->post('number_of_place');
        $data['address'] = $member_data['address'];
        $data['tel1'] = $member_data['tel'];
        $data['tel2'] = $member_data['mobile'];
        $data['is_accept'] = 'Y';
        $data['status'] = 'N';
        $data['created_on'] = $now;
        $data['updated_on'] = $now;
        $data['updated_type'] = 'C';
        $data['updated_by'] = 0;
        $this->whaledb->insert('company', $data);

        $_id = $this->whaledb->insert_id();

        $data = array();
        $data['company_id'] = $_id;
        $data['year'] = $year;
        $data['policy_name'] = $this->input->post('cate_trash_desc');
        $data['policy_description'] = $this->input->post('cate_trash_detail');
        $data['person_name'] = $this->input->post('responsible_dec');
        $data['person_description'] = $this->input->post('responsible_detail');
        $data['model_name'] = '';
        $data['model_description'] = '';
        $data['activity_name'] = $this->input->post('activity_decs');
        $data['activity_description'] = $this->input->post('activity_detail');
        $data['public_name'] = $this->input->post('advertise_desc');
        $data['public_description'] = $this->input->post('advertise_detail');
        $data['created_on'] = $now;
        $this->whaledb->insert('company_manage', $data);

        $data = array();
        $data['company_id'] = $_id;
        $data['type'] = 'M';
        $data['email'] = $member_data['email'];
        $data['password'] = $member_data['password'];
        $data['avatar'] = $member_data['logo'];
        $data['name'] = $member_data['name'];
        $data['status'] = 'Y';
        $data['salt'] = '';
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['created_by'] = 0;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_type'] = 'C';
        $data['updated_by'] = 0;
        $this->whaledb->insert('company_user', $data);

        $data = array();
        $data['company_id'] = $_id;
        $data['year'] = $year;
        $data['description'] = $this->input->post('cate_trash_number_of_place');
        $this->whaledb->insert('company_garbage_type', $data);

        if (!is_dir('./carethewhale/images/upload/company')) {
            mkdir('./carethewhale/images/upload/company', 0777);
        }

        if (!is_dir('./carethewhale/images/upload/company/' . ($_id % 4000))) {
            mkdir('./carethewhale/images/upload/company/' . ($_id % 4000), 0777);
        }

        if (!is_dir('./carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id)) {
            mkdir('./carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id, 0777);
        }

        $config['upload_path'] = './carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id . '/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['max_size'] = 20480;
        $config['overwrite'] = FALSE;
        $this->load->library('upload', $config);

        for ($i = 0; $i < 3; $i++) {
            if (@$_FILES['upload_policy_' . $i]['name'] != "") {
                $this->upload->initialize($config);
                if ($this->upload->do_upload('upload_policy_' . $i)) {
                    $upload_info = $this->upload->data();
                    $image_url = config_item('base_url') . 'carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id . '/' . $upload_info['file_name'];

                    $data = array();
                    $data['company_id'] = $_id;
                    $data['year'] = $year;
                    $data['image'] = $image_url;
                    $data['order_on'] = 1;
                    $this->whaledb->insert('company_policy_image', $data);
                }
            }
        }

        for ($i = 0; $i < 3; $i++) {
            if (@$_FILES['upload_responsible_' . $i]['name'] != "") {
                $this->upload->initialize($config);
                if ($this->upload->do_upload('upload_responsible_' . $i)) {
                    $upload_info = $this->upload->data();
                    $image_url = config_item('base_url') . 'carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id . '/' . $upload_info['file_name'];

                    $data = array();
                    $data['company_id'] = $_id;
                    $data['year'] = $year;
                    $data['image'] = $image_url;
                    $data['order_on'] = 1;
                    $this->whaledb->insert('company_person_image', $data);
                }
            }
        }

        for ($i = 0; $i < 3; $i++) {
            if (@$_FILES['upload_activity_' . $i]['name'] != "") {
                $this->upload->initialize($config);
                if ($this->upload->do_upload('upload_activity_' . $i)) {
                    $upload_info = $this->upload->data();
                    $image_url = config_item('base_url') . 'carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id . '/' . $upload_info['file_name'];

                    $data = array();
                    $data['company_id'] = $_id;
                    $data['year'] = $year;
                    $data['image'] = $image_url;
                    $data['order_on'] = 1;
                    $this->whaledb->insert('company_activity_image', $data);
                }
            }
        }

        for ($i = 0; $i < 3; $i++) {
            if (@$_FILES['upload_advertise_' . $i]['name'] != "") {
                $this->upload->initialize($config);
                if ($this->upload->do_upload('upload_advertise_' . $i)) {
                    $upload_info = $this->upload->data();
                    $image_url = config_item('base_url') . 'carethewhale/images/upload/company/' . ($_id % 4000) . '/' . $_id . '/' . $upload_info['file_name'];

                    $data = array();
                    $data['company_id'] = $_id;
                    $data['year'] = $year;
                    $data['image'] = $image_url;
                    $data['order_on'] = 1;
                    $this->whaledb->insert('company_public_image', $data);
                }
            }
        }

        return true;
    }

    function get_list_company_master() {
        $this->care_master->select('id,companyTh as name_th,companyEn as name_en,companyCode as code');
        $this->care_master->from('company_masters');
        $this->care_master->where('isSetlink IS NULL');
        $this->care_master->or_where('isSetlink =','Y');
        $query = $this->care_master->get();
        
        return $query->result_array();
    }
}