<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class dashboard_model extends CI_Model
{

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function get_dashboard_stats()
    {
        $data = [
            'total' => [
                'cf' => '0.00',
                'tree' => '0'
            ],
            'bear' => [
                'cf' => '0.00',
                'tree' => '0'
            ],
            'whale' => [
                'weight' => '0.00',
                'cf' => '0.00',
                'tree' => '0'
            ]
        ];

        if (@$this->authen->member_data['carethebear']['id'] > 0) {
            $this->beardb->select('sum(activity.cf_care_total) as sum_cf');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $activity = $query->row_array();

            $this->beardb->select('sum(project.cf_care_total) as sum_cf');
            $this->beardb->from('project');
            $this->beardb->where('project.status', 'Y');
            $this->beardb->where('project.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $project = $query->row_array();

            $this->beardb->select('sum(tsd_projects.cf_care_total) as sum_cf');
            $this->beardb->from('tsd_projects');
            $this->beardb->join('member', 'member.company_master_id = tsd_projects.company_master_id');
            $this->beardb->where('member.id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $tsd_project = $query->row_array();
         
            $data['bear']['cf'] = str_replace(',', '', number_format(($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']), 2));
            $data['bear']['tree'] = str_replace(',', '', number_format((($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9), 0));

            $data['bear_tsd']['cf'] = str_replace(',', '', number_format(($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']), 2));
            $data['bear_tsd']['tree'] = str_replace(',', '', number_format((($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + ($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']);
            $data['total']['tree'] = $data['total']['tree'] + (($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9);
        } else {
            $data['bear']['cf'] = '0.00';
            $data['bear']['tree'] = '0';
        }

        if (@$this->authen->member_data['carethewhale']['company_id'] > 0) {
            $this->whaledb->select("sum(total_weight) as total_weight, sum(total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['carethewhale']['company_id']);
            $query = $this->whaledb->get();
            $summary = $query->row_array();

            $data['whale']['weight'] = str_replace(',', '', number_format($summary['total_weight'], 2));
            $data['whale']['cf'] = str_replace(',', '', number_format($summary['total_cf'], 2));
            $data['whale']['tree'] = str_replace(',', '', number_format(($summary['total_cf'] / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + $summary['total_cf'];
            $data['total']['tree'] = $data['total']['tree'] + ($summary['total_cf'] / 9);
        } else {
            $data['whale']['weight'] = '0.00';
            $data['whale']['cf'] = '0.00';
            $data['whale']['tree'] = '0';
        }

        $data['total']['cf'] = str_replace(',', '', number_format(($data['total']['cf'] ), 2));
        $data['total']['tree'] = str_replace(',', '', number_format($data['total']['tree'], 0));

        return $data;
    }

    function get_outcome_stats($start , $end )
    {
       $start = $this->input->get("from") ?  $this->input->get("from") : date("2020-01-01");
       $end = $this->input->get("to") ? $this->input->get("to") : date("Y-m-d");
      
        $data = [
            'total' => [
                'cf' => '0.00',
                'tree' => '0'
            ],
            'bear' => [
                'cf' => '0.00',
                'tree' => '0',
                'all_activity' => '0',
                'meeting' => '0',
                'training' => '0',
                'give_awards' => '0',
                'cf_care_1' => '0.00',
                'cf_care_2' => '0.00',
                'cf_care_3' => '0.00',
                'cf_care_4' => '0.00',
                'cf_care_5' => '0.00',
                'cf_care_6' => '0.00',
                'tree_care_1' => '0',
                'tree_care_2' => '0',
                'tree_care_3' => '0',
                'tree_care_4' => '0',
                'tree_care_5' => '0',
                'tree_care_6' => '0'
            ],
            'bear_tsd' => [
                'cf' => '0.00',
                'tree' => '0',
                'all_activity' => '0',
                'meeting' => '0',
                'training' => '0',
                'give_awards' => '0',
                'cf_care_1' => '0.00',
                'cf_care_2' => '0.00',
                'cf_care_3' => '0.00',
                'cf_care_4' => '0.00',
                'cf_care_5' => '0.00',
                'cf_care_6' => '0.00',
                'tree_care_1' => '0',
                'tree_care_2' => '0',
                'tree_care_3' => '0',
                'tree_care_4' => '0',
                'tree_care_5' => '0',
                'tree_care_6' => '0',
                'tsd' => '0.00',
                'corporate_action' => '0',
                'aw' => '0',
                'cd' => '0',
                'cn' => '0',
                'dg' => '0',
                'ma' => '0',
                'nr' => '0',
                'po' => '0',
                'sc' => '0',
                'xb' => '0',
                'xd' => '0',
                'xe' => '0',
                'xi' => '0',
                'xm' => '0',
                'xo' => '0',
                'xp' => '0',
                'xr' => '0',
                'xs' => '0',
                'xt' => '0',
                'tsd_cf_total' => '0.00',
                'tsd_tree_total' => '0',
            ],
            'whale' => [
                'weight' => '0.00',
                'cf' => '0.00',
                'tree' => '0',
                'waste' => [],
                'wasteManage' => []
            ]
        ];

        if (@$this->authen->member_data['carethebear']['id'] > 0) {
            $this->beardb->select('sum(activity.cf_care_total) as sum_cf, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where("activity.activity_date >= '" . $start . "'");
            $this->beardb->where("activity.activity_date <= '" . $end . "'");
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $activity = $query->row_array();

            $this->beardb->select('sum(project.cf_care_total) as sum_cf, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3');
            $this->beardb->from('project');
            $this->beardb->where('project.status', 'Y');
            $this->beardb->where("((project.start_date <= '" . $start . "' and project.end_date >= '" . $end . "') or (project.start_date <= '" . $start . "' and project.end_date >= '" . $start . "' and project.end_date <= '" . $end . "') or (project.start_date >= '" . $start . "' and project.end_date <= '" . $end . "') or (project.start_date >= '" . $start . "' and project.start_date <= '" . $end . "' and project.end_date >= '" . $end . "'))");
            $this->beardb->where('project.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $project = $query->row_array();

            $this->beardb->select('sum(tsd_projects.cf_care_total) as sum_cf');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_A3,tsd_projects.size_thickness_T70_A3,tsd_projects.size_thickness_T80_A3,tsd_projects.size_thickness_T90_A3,tsd_projects.size_thickness_T120_A3) as cf_care_1');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_A4,tsd_projects.size_thickness_T70_A4,tsd_projects.size_thickness_T80_A4,tsd_projects.size_thickness_T90_A4,tsd_projects.size_thickness_T120_A4) as cf_care_2');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_A5,tsd_projects.size_thickness_T70_A5,tsd_projects.size_thickness_T80_A5,tsd_projects.size_thickness_T90_A5,tsd_projects.size_thickness_T120_A3) as cf_care_3');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_B3,tsd_projects.size_thickness_T70_B3,tsd_projects.size_thickness_T80_B3,tsd_projects.size_thickness_T90_B3,tsd_projects.size_thickness_T120_B3) as cf_care_4');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_B4,tsd_projects.size_thickness_T70_B4,tsd_projects.size_thickness_T80_B4,tsd_projects.size_thickness_T90_B4,tsd_projects.size_thickness_T120_B4) as cf_care_5');
            $this->beardb->select('sum(tsd_projects.size_thickness_T55_B5,tsd_projects.size_thickness_T70_B5,tsd_projects.size_thickness_T80_B5,tsd_projects.size_thickness_T90_B5,tsd_projects.size_thickness_T120_B5) as cf_care_6');
            $this->beardb->from('tsd_projects');
            $this->beardb->join('member', 'member.company_master_id = tsd_projects.company_master_id');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.end_date_time <= '" . $end . "'");
            $this->beardb->where('member.id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            if($query){
                $tsd_project = $query->row_array();

            }else{
                $tsd_project['sum_cf'] = 0.00;
                $tsd_project['cf_care_1'] = 0.00;
                $tsd_project['cf_care_2'] = 0.00;
                $tsd_project['cf_care_3'] = 0.00;
                $tsd_project['cf_care_4'] = 0.00;
                $tsd_project['cf_care_5'] = 0.00;
                $tsd_project['cf_care_6'] = 0.00;
            }
            
            
            
            $data['bear']['cf'] = str_replace(',', '', number_format(($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']), 2));
            $data['bear']['tree'] = str_replace(',', '', number_format((($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9), 0));

            

            $data['total']['cf'] = $data['total']['cf'] + ($activity['sum_cf'] + $project['sum_cf']);
            $data['total']['tree'] = $data['total']['tree'] + (($activity['sum_cf'] + $project['sum_cf']) / 9);

            $data['bear']['cf_care_1'] = str_replace(',', '', number_format(($activity['cf_care_1'] + $project['cf_care_1'] + $tsd_project['cf_care_1']), 2));
            $data['bear']['tree_care_1'] = str_replace(',', '', number_format((($activity['cf_care_1'] + $project['cf_care_1'] + $tsd_project['cf_care_1']) / 9), 0));

            $data['bear']['cf_care_2'] = str_replace(',', '', number_format(($activity['cf_care_2'] + $project['cf_care_2'] + $tsd_project['cf_care_2']), 2));
            $data['bear']['tree_care_2'] = str_replace(',', '', number_format((($activity['cf_care_2'] + $project['cf_care_2'] + $tsd_project['cf_care_2']) / 9), 0));

            $data['bear']['cf_care_3'] = str_replace(',', '', number_format(($activity['cf_care_3'] + $project['cf_care_3']  + $tsd_project['cf_care_3']), 2));
            $data['bear']['tree_care_3'] = str_replace(',', '', number_format((($activity['cf_care_3'] + $project['cf_care_3']  + $tsd_project['cf_care_3']) / 9), 0));

            $data['bear']['cf_care_4'] = str_replace(',', '', number_format($activity['cf_care_4'], 2));
            $data['bear']['tree_care_4'] = str_replace(',', '', number_format(($activity['cf_care_4'] / 9), 0));

            $data['bear']['cf_care_5'] = str_replace(',', '', number_format($activity['cf_care_5'], 2));
            $data['bear']['tree_care_5'] = str_replace(',', '', number_format(($activity['cf_care_5'] / 9), 0));

            $data['bear']['cf_care_6'] = str_replace(',', '', number_format($activity['cf_care_6'], 2));
            $data['bear']['tree_care_6'] = str_replace(',', '', number_format(($activity['cf_care_6'] / 9), 0));


            $this->beardb->select('count(*) as cnt');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where("activity.activity_date >= '" . $start . "'");
            $this->beardb->where("activity.activity_date <= '" . $end . "'");
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $result = $query->row_array();
           
            $data['bear']['all_activity'] = $result['cnt'];

            $this->beardb->select('count(*) as cnt');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where("activity.activity_date >= '" . $start . "'");
            $this->beardb->where("activity.activity_date <= '" . $end . "'");
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $this->beardb->where("activity.activity_type_id IN (5, 6)");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear']['meeting'] = $result['cnt'];

            $this->beardb->select('count(*) as cnt');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where("activity.activity_date >= '" . $start . "'");
            $this->beardb->where("activity.activity_date <= '" . $end . "'");
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $this->beardb->where("activity.activity_type_id IN (3, 4)");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear']['training'] = $result['cnt'];

            $data['bear']['meeting'] = $result['cnt'];

            $this->beardb->select('count(*) as cnt');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where("activity.activity_date >= '" . $start . "'");
            $this->beardb->where("activity.activity_date <= '" . $end . "'");
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $this->beardb->where("activity.activity_type_id IN (11)");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear']['give_awards'] = $result['cnt'];

            

            $this->beardb->select('count(ca_type) as cnt');
            $this->beardb->select('sum(tsd_projects.cf_care_total) as tsd_cf_total');
            $this->beardb->select('sum(tsd_projects.cf_care_total) as tsd_tree_total');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $query = $this->beardb->get();
            $tsd_project = $query->row_array();

            $data['bear_tsd']['tsd'] = $tsd_project['cnt'];

            $data['bear_tsd']['tsd_cf_total'] = str_replace(',', '', number_format(($tsd_project['tsd_cf_total'] ), 5));
            $data['bear_tsd']['tsd_tree_total'] = str_replace(',', '', number_format(($tsd_project['tsd_tree_total']  / 9), 0));

            $data['bear_tsd']['cf'] = str_replace(',', '', number_format(($tsd_project['tsd_cf_total']), 2));
            $data['bear_tsd']['tree'] = str_replace(',', '', number_format((($tsd_project['tsd_tree_total']) / 9), 0));
            

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('AW')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['aw'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['aw_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['aw_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('CD')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['cd'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['cd_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['cd_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('CN')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['cn'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['cn_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['cn_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('DG')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['dg'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['dg_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['dg_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('MA')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['ma'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['ma_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['ma_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('NR')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['nr'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['nr_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['nr_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('PO')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['po'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['po_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['po_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.created_at >= '" . $start . "'");
            $this->beardb->where("tsd_projects.created_at <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('SC')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['sc'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['sc_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['sc_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.created_at >= '" . $start . "'");
            $this->beardb->where("tsd_projects.created_at <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XB')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xb'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xb_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xb_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.created_at >= '" . $start . "'");
            $this->beardb->where("tsd_projects.created_at <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XD')");
            $query = $this->beardb->get();

            //$str = $this->beardb->last_query();
            //var_dump($str);exit;

            $result = $query->row_array();


            $data['bear_tsd']['xd'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xd_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xd_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XE')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xe'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xe_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xe_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XI')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xi'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xi_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xi_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XM')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xm'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xm_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xm_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XO')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xo'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xo_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xo_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XP')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xp'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xp_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xp_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XR')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xr'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xr_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xr_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XS')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xs'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xs_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xs_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;

            $this->beardb->select('sum(sum_care_total) as cnt');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            $this->beardb->where("tsd_projects.ca_type IN ('XT')");
            $query = $this->beardb->get();
            $result = $query->row_array();

            $data['bear_tsd']['xt'] = $result['cnt'] ?? 0;
            $data['bear_tsd']['xt_co'] = $result['cnt'] == 0 ? 0 : $result['cnt'] * 0.0000005033;
            $data['bear_tsd']['xt_co_n'] = $result['cnt'] == 0 ? 0 : ($result['cnt'] * 0.0000005033) / 9;


            

            $this->beardb->select('ca_type');
            $this->beardb->from('tsd_projects');
            $this->beardb->where('tsd_projects.status', 'Y');
            $this->beardb->where("tsd_projects.start_date_time >= '" . $start . "'");
            $this->beardb->where("tsd_projects.start_date_time <= '" . $end . "'");
            $this->beardb->where('tsd_projects.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
            // $this->beardb->where("tsd_projects.ca_type IN ('AW, CD, CN, DG, MA, NR, PO, SC, XB, XD, XE, XT, XM, XO, XP, XR, XS, XT')");
            $query = $this->beardb->get();
            $result = $query->row_array();
            $data['bear_tsd']['corporate_action'] = $result;
            // $data['bear_tsd']['xd'] = str_replace(',', '',$tsd_projects['ca_type']);

        } else {
            $data['bear']['cf'] = '0';
            $data['bear']['tree'] = '0';
        }

        if (@$this->authen->member_data['carethewhale']['company_id'] > 0) {
            $this->whaledb->select("sum(total_weight) as total_weight, sum(total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where("company_sort_last.do_datetime >= '" . $start . "'");
            $this->whaledb->where("company_sort_last.do_datetime <= '" . $end . "'");
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['carethewhale']['company_id']);
            $query = $this->whaledb->get();
            $summary = $query->row_array();

            $data['whale']['weight'] = str_replace(',', '', number_format($summary['total_weight'], 0.00));
            $data['whale']['cf'] = str_replace(',', '', number_format($summary['total_cf'], 0.00));
            $data['whale']['tree'] = str_replace(',', '', number_format(($summary['total_cf'] / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + $summary['total_cf'];
            $data['total']['tree'] = $data['total']['tree'] + ($summary['total_cf'] / 9);

            $this->whaledb->select("garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->join('company_sort_last_detail', 'company_sort_last_detail.company_sort_last_id = company_sort_last.id');
            $this->whaledb->join('garbage_kind', 'company_sort_last_detail.garbage_kind_id = garbage_kind.id');
            $this->whaledb->join('garbage_category', 'garbage_kind.garbage_category_id = garbage_category.id');
            $this->whaledb->join('garbage_type', 'garbage_category.garbage_type_id = garbage_type.id');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where("company_sort_last.do_datetime >= '" . $start . "'");
            $this->whaledb->where("company_sort_last.do_datetime <= '" . $end . "'");
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['carethewhale']['company_id']);
            $this->whaledb->group_by('garbage_type.id');
            $query = $this->whaledb->get();
            $result = $query->result_array();

            $total = 0;
            foreach ($result as $key => $value) {
                $total = $total + $value['total_weight'];
            }

            foreach ($result as $key => $value) {
                $data['whale']['waste'][] = [
                    'txt' => $value['name'],
                    'value' => [
                        0 => str_replace(',', '', number_format($value['total_weight'], 2)),
                        1 => str_replace(',', '', number_format($value['total_cf'], 2)),
                        2 => str_replace(',', '', number_format(($value['total_cf'] / 9), 0))
                    ],
                    'percent' => str_replace(',', '', number_format((($value['total_weight'] / $total) * 100), 2))
                ];
            }

            if (count($data['whale']['waste']) == 0) {
                $this->whaledb->select("*", false);
                $this->whaledb->from('garbage_type');
                $this->whaledb->where('status', 'Y');
                $query = $this->whaledb->get();
                $result = $query->result_array();

                foreach ($result as $key => $value) {
                    $data['whale']['waste'][] = [
                        'txt' => $value['name'],
                        'value' => [
                            0 => 0.00,
                            1 => 0.00,
                            2 => 0.00
                        ],
                        'percent' => 0.00
                    ];
                }
            }

            $this->whaledb->select("garbage_disposal.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->join('company_sort_last_detail', 'company_sort_last_detail.company_sort_last_id = company_sort_last.id');
            $this->whaledb->join('garbage_disposal', 'company_sort_last_detail.garbage_disposal_id = garbage_disposal.id');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where("company_sort_last.do_datetime >= '" . $start . "'");
            $this->whaledb->where("company_sort_last.do_datetime <= '" . $end . "'");
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['carethewhale']['company_id']);
            $this->whaledb->group_by('garbage_disposal.id');
            $query = $this->whaledb->get();
            $result = $query->result_array();

            $total = 0;
            foreach ($result as $key => $value) {
                $total = $total + $value['total_weight'];
            }

            foreach ($result as $key => $value) {
                $data['whale']['wasteManage'][] = [
                    'txt' => $value['name'],
                    'value' => [
                        0 => str_replace(',', '', number_format((($value['total_weight'] / $total) * 100), 2)) . '%',
                        1 => str_replace(',', '', number_format($value['total_weight'], 2)),
                        2 => str_replace(',', '', number_format($value['total_cf'], 2)),
                        3 => str_replace(',', '', number_format(($value['total_cf'] / 9), 2))
                    ]
                ];
            }

            if (count($data['whale']['wasteManage']) == 0) {
                $this->whaledb->select("*", false);
                $this->whaledb->from('garbage_disposal');
                $this->whaledb->where('status', 'Y');
                $query = $this->whaledb->get();
                $result = $query->result_array();

                foreach ($result as $key => $value) {
                    $data['whale']['wasteManage'][] = [
                        'txt' => $value['name'],
                        'value' => [
                            0 => '0%',
                            1 => 0.00,
                            2 => 0.00,
                            3 => 0.00
                        ]
                    ];
                }
            }
        } else {
            $data['whale']['weight'] = '0';
            $data['whale']['cf'] = '0';
            $data['whale']['tree'] = '0';

            $this->whaledb->select("*", false);
            $this->whaledb->from('garbage_type');
            $this->whaledb->where('status', 'Y');
            $query = $this->whaledb->get();
            $result = $query->result_array();

            foreach ($result as $key => $value) {
                $data['whale']['waste'][] = [
                    'txt' => $value['name'],
                    'value' => [
                        0 => 0.00,
                        1 => 0.00,
                        2 => 0.00
                    ],
                    'percent' => 0.00
                ];
            }

            $this->whaledb->select("*", false);
            $this->whaledb->from('garbage_disposal');
            $this->whaledb->where('status', 'Y');
            $query = $this->whaledb->get();
            $result = $query->result_array();

            foreach ($result as $key => $value) {
                $data['whale']['wasteManage'][] = [
                    'txt' => $value['name'],
                    'value' => [
                        0 => '0%',
                        1 => 0.00,
                        2 => 0.00,
                        3 => 0.00
                    ]
                ];
            }
        }

        $data['total']['cf'] = str_replace(',', '', number_format($data['total']['cf'], 2));
        $data['total']['tree'] = str_replace(',', '', number_format($data['total']['tree'], 0));

        return $data;
    }

    function get_document_stats()
    {
        $this->beardb->select("*");
        $this->beardb->from('document');
        $this->beardb->where('status', 'Y');
        $query = $this->beardb->get();
        $summary = $query->result_array();

        return $summary;
    }

    function get_project_stats()
    {
        $this->beardb->select("*");
        $this->beardb->from('project');
        $this->beardb->where('status', 'Y');
        $this->beardb->where('member_id', $this->authen->member_data['carethebear']['id']);
        $this->beardb->order_by('id', 'DESC');
        $query = $this->beardb->get();
        $data = $query->result_array();

        return $data;
    }

    function get_activity_stats()
    {

        $this->beardb->select("*");
        $this->beardb->from('activity');
        $this->beardb->where('status', 'Y');
        $this->beardb->where('member_id', $this->authen->member_data['carethebear']['id']);
        $this->beardb->order_by('id', 'DESC');
        $query = $this->beardb->get();
        $data = $query->result_array();

        return $data;
    }

    function get_data_wild($id) {
        if(!empty($id)) {
            $this->climate->select('plot_wild.id_plot, Company_wild_donation.companyTh, plant_wild.name_wild, plant_wild.location_wild, plant_wild.status, plot_wild.link, (donation_wild.donation_amount * 200) / 9 as donation_amount');
            $this->climate->from('Company_wild_donation');
            $this->climate->join('donation_wild ', 'donation_wild.id_company_wild_donation = Company_wild_donation.id_company_wild_donation');
            $this->climate->join('plot_wild ', 'plot_wild.id_plot = donation_wild.id_plot');
            $this->climate->join('plant_wild ', 'plant_wild.id_plant = plot_wild.id_plant');
            $this->climate->where('Company_wild_donation.company_master_id', $id);
            $query = $this->climate->get();
            if($query->num_rows > 0) {
                $data = $query->result_array();
                if(!empty($data[0]['donation_amount'])) {
                    $data[0]['donation_amount'] = number_format($data[0]['donation_amount'], 2);
                }else{
                    $data[0]['donation_amount'] = '0.00';
                }
                return $data;
            }
        }
        return [];
    }

    function check_wild_from_bear_login($id) {
        if(!empty($id)) {
            $this->climate->select('*');
            $this->climate->from('Company_wild_donation');
            $this->climate->where('company_master_id', $id);
            $query = $this->climate->get();
            if($query->num_rows > 0) {
                return true;
            }
        }
        return false;
    }

    function check_whale_id_from_company_master($id) {
        if(!empty($id)) {
            $this->caremaster->select('*');
            $this->caremaster->from('company_masters');
            $this->caremaster->where('careTheWhaleld', $id); // TODO: check ld not Id In DB
            $query = $this->caremaster->get();
            if($query->num_rows > 0) {
                return ($query->result_array())[0];
            }
        }
        return [];
    }

    function get_tsd_stats()
    {
        $this->beardb->select("*");
        $this->beardb->from('tsd_projects');
        $this->beardb->where('company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
        $this->beardb->order_by('id', 'DESC');
        $query = $this->beardb->get();
        $tsdstats = $query->result_array();
        return $tsdstats;
    }

    function get_company_activity()
    {
        $this->beardb->select("activity.*");
        $this->beardb->from('activity');
        $this->beardb->join('member', 'member.id = activity.member_id');
        $this->beardb->where('activity.status', 'Y');
        $this->beardb->where('member.status', 'Y');
        $this->beardb->where('member.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
        $this->beardb->order_by('id', 'DESC');
        $query = $this->beardb->get();
        $data = $query->result_array();
        
        return $data;
    }
    

    function get_company_project()
    {
        $this->beardb->select("project.*");
        $this->beardb->from('project');
        $this->beardb->join('member', 'member.id = project.member_id');
        $this->beardb->where('project.status', 'Y');
        $this->beardb->where('member.status', 'Y');
        $this->beardb->where('member.company_master_id', $this->authen->member_data['carethebear']['company_master_id']);
        $this->beardb->order_by('id', 'DESC');
        $query = $this->beardb->get();
        $data = $query->result_array();

        return $data;
    }

}