<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class layout_model extends CI_Model {

    private $ci;
    private $beardb;
    private $whaledb;
    private $climate;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->beardb = $this->ci->load->database('bear', TRUE);
        $this->whaledb = $this->ci->load->database('whale', TRUE);
        $this->climate = $this->ci->load->database('default', TRUE);
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }
    

    
    function get_banner()
    {
        $this->beardb->select('*');
        $this->beardb->from('banner');
        $this->beardb->where('status', 'Y');
        $this->beardb->order_by('order_on', 'asc');
        $this->beardb->limit(5);
        $query = $this->beardb->get();
        return $query->result_array();
    }
}