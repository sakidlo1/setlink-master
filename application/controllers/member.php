<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('authen_member', NULL, 'authen');
        $this->smarty->assign('member', $this->authen->member_data);
        $this->smarty->assign('authen', $this->authen);

        if ($this->authen->controller != "") {
            $this->this_page = $this->authen->controller;
            $this->this_sub_page = $this->authen->function;
        } else {
            $this->this_page = "member";
            $this->this_sub_page = 'index';
        }

        $this->load->model('layout_model');

        $this->load->model('member_model', 'this_model');

        $this->smarty->assign('page', $this->this_page);
        $this->smarty->assign('sub_page', $this->this_sub_page);
        $this->smarty->assign('site_name', config_item('site_name'));
        $this->smarty->assign('company_name', config_item('company_name'));
        $this->smarty->assign('base_url', config_item('base_url'));
        $this->smarty->assign('image_url', config_item('image_url'));
        $this->smarty->assign('error_msg', '');
        $this->smarty->assign('success_msg', '');
    }

    public function login()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        $member = $this->this_model->login($data['email'], $data['password']);
        if (@$member['id'] > 0) {
            $_SESSION['climate_member'] = $member;

            if (@$member['carethebear']['id'] > 0) {
                $_SESSION['carethebear_member'] = $member['carethebear'];
            }

            if (@$member['carethewhale']['id'] > 0) {
                $_SESSION['member'] = $member['carethewhale'];
            }

            echo json_encode(['status' => 'success']);
        } else {
            //echo $member;
            echo json_encode(['status' => 'login_fail']);

        }
    }

    public function callback()
    {
        $code = $this->input->get('code');
        $state = $this->input->get('state');

        if (!empty($code)) {
            $url = config_item('oauth2_token');
            $data = array(
                'grant_type' => 'authorization_code',
                'code' => $code,
                "redirect_uri" => config_item('callback_url')
            );

            $headers = array(
                'Authorization: Basic U0VUTGluazpwYXNzd29yZDI=', // ส่วน Header Authorization ที่คุณได้รับ
                'Content-Type: application/x-www-form-urlencoded',
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);

            if ($response === false) {
                echo 'Error: ' . curl_error($ch);
                redirect('/');
            } else {
                $responseData = json_decode($response, true);

                $this->getProfile($responseData['access_token']);
            }

            curl_close($ch);
        } else {
            redirect('/');
        }
    }

    public function getProfile($token)
    {
        $apiUrl = config_item('get_profile');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $apiUrl);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
            //'Cookie: JSESSIONID=A5675BB176104432133EDDE2E1A282C2; JSESSIONID=32CB9476794D2AB71D5531ABF9F5BE83',
            'Authorization: Bearer ' . $token,
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        if ($response === false) {
            echo 'Error: ' . curl_error($ch);
        } else {
            $responseData = json_decode($response, true);
            $email = $responseData['E_EMAIL'];
            $this->checkEmail($email);
        }

        curl_close($ch);
    }

    public function checkEmail($email)
    {
        $email_exits = $this->this_model->check_email_exists($email);
        if ($email_exits) {
            $user = $this->this_model->getUserByEmail($email);
            if ($user) {
                $member = $this->this_model->loginByEmail($email);
                if (@$member['id'] > 0) {
                    $_SESSION['climate_member'] = $member;

                    if (@$member['carethebear']['id'] > 0) {
                        $_SESSION['carethebear_member'] = $member['carethebear'];
                    }

                    if (@$member['carethewhale']['id'] > 0) {
                        $_SESSION['member'] = $member['carethewhale'];
                    }

                    //echo json_encode(['status' => 'success']);
                    redirect('/dashboard');


                } else {
                    //echo $member;
                    echo json_encode(['status' => 'login_fail']);

                }
            }
        } else {
            //echo "ไม่มี";
            redirect('member/register');

        }
    }

    public function forget_password()
    {
        if ($this->authen->id > 0) {
            redirect('/');
        }

        $this->smarty->assign('page_name', 'ลืมรหัสผ่าน');

        if ($this->input->post('forget_password')) {
            $salt = $this->this_model->forget_password();
            if ($salt != false) {
                $email = $this->input->post('email');
                $reset_password_url = config_item('base_url') . $this->this_page . '/reset_password?email=' . $email . '&salt=' . $salt;
                $to = $email;
                $subject = 'ลืมรหัสผ่าน - ' . config_item('site_name');
                $message = $this->smarty->fetch('email/forget_password.tpl');
                $message = str_replace('[[link]]', $reset_password_url, $message);

                $this->load->library('mailer');
                $this->mailer->send($email, $subject, $message);

                redirect('/member/forget_password_success');
            } else {
                $this->smarty->assign('error_msg', 'ไม่พบอีเมลในระบบ กรุณาสมัครสมาชิก');
                $this->smarty->display('forget_password.tpl');
            }
        } else {
            $this->smarty->display('forget_password.tpl');
        }
    }

    public function forget_password_success()
    {
        $this->smarty->assign('page_name', 'ลืมรหัสผ่าน');
        $this->smarty->display('forget_password_success.tpl');
    }

    public function reset_password()
    {
        if ($this->authen->id > 0) {
            redirect('/');
        }

        $this->smarty->assign('page_name', 'ตั้งรหัสผ่านใหม่');

        if ($this->input->get('email') != "" && $this->input->get('salt') != "") {
            $salt = $this->this_model->get_salt($this->input->get('email'));
            if ($salt == false || $salt == "" || $salt != $this->input->get('salt')) {
                redirect('/');
            }
        } else {
            redirect('/');
        }

        if ($this->input->post('reset_password')) {
            $this->this_model->reset_password();
            redirect('/member/reset_password_success');
        } else {
            $this->smarty->display('reset_password.tpl');
        }
    }

    public function reset_password_success()
    {
        $this->smarty->assign('page_name', 'ตั้งรหัสผ่านใหม่');
        $this->smarty->display('reset_password_success.tpl');
    }

    public function member_profile()
    {
        if ($this->input->post()) {
         $this->this_model->update_profile();
        // $upload_info = $this->upload->data();
       
         $_id =  $this->authen->id;
            if (@$_FILES['customer_logo_0']['name'] != "") {
                if (!is_dir('./carethebear/images/upload/member')) {
                    mkdir('./carethebear/images/upload/member', 0777);
                }

                if (!is_dir('./carethebear/images/upload/member/' . ($_id % 4000))) {
                    mkdir('./carethebear/images/upload/member/' . ($_id % 4000), 0777);
                }

                if (!is_dir('./carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id)) {
                    mkdir('./carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id, 0777);
                }

                $config['upload_path'] = './carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id . '/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['max_size'] = 20480;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('customer_logo_0')) {
                    $upload_info = $this->upload->data();
                    $this->this_model->update_logo($_id, $upload_info['file_name']);
                }
            }
            exit();

        }

        $this->smarty->assign('item', $this->this_model->get_profile($this->authen->member_data['email']));
        $this->smarty->assign('page_name', 'Profile edit form');
        $this->smarty->assign('company_type', $this->this_model->get_company_type());
        $this->smarty->display('member_profile.tpl');
   
    }
    public function register()
    {
        if ($this->authen->member_data['is_bear'] == 'Y') {
            //redirect('/carethebear/member/activity');
            redirect('/dashboard/carethebear');
            
        }

        if ($this->input->post('company_name') != '') {
            if ($this->input->post('customer_select_project') == 'whale') {
                $check_email = $this->this_model->check_email_exists_whale($this->input->post('customer_email'));
                if ($check_email) {
                    echo 'email_exists';
                    exit();
                } else {
                    $salt = md5($this->input->post('customer_email') . '-' . $this->input->post('customer_first_name') . ' ' . $this->input->post('customer_last_name') . '-' . time());
                    $_id = $this->this_model->register($salt);
                    if (@$_FILES['customer_logo_0']['name'] != "") {
                        if (!is_dir('./carethebear/images/upload/member')) {
                            mkdir('./carethebear/images/upload/member', 0777);
                        }

                        if (!is_dir('./carethebear/images/upload/member/0')) {
                            mkdir('./carethebear/images/upload/member/0', 0777);
                        }

                        if (!is_dir('./carethebear/images/upload/member/0/0')) {
                            mkdir('./carethebear/images/upload/member/0/0', 0777);
                        }

                        $config['upload_path'] = './carethebear/images/upload/member/0/0/';
                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                        $config['max_size'] = 20480;
                        $config['overwrite'] = FALSE;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('customer_logo_0')) {
                            $upload_info = $this->upload->data();
                            $logo_url = config_item('base_url') . 'carethebear/images/upload/member/0/0/' . $upload_info['file_name'];
                        }
                    }

                    $_SESSION['tmp_register_carethewhale'] = [
                        'email' => $this->input->post('customer_email'),
                        'password' => md5($this->input->post('customer_account_password')),
                        'company' => $this->input->post('company_name'),
                        'logo' => $logo_url,
                        'name' => $this->input->post('customer_first_name') . ' ' . $this->input->post('customer_last_name'),
                        'address' => $this->input->post('customer_company_location'),
                        'tel' => $this->input->post('customer_phone'),
                        'mobile' => $this->input->post('customer_phone_partner')
                    ];

                    echo 'register_carethewhale';
                }

                exit();
            } else {
                $check_email = $this->this_model->check_email_exists($this->input->post('customer_email'));
                if ($check_email) {
                    echo 'email_exists';
                    exit();
                } else {
                    // $check_username = $this->this_model->check_username_exists($this->input->post('customer_account_name'));
                    // if ($check_username) {
                    //     echo 'username_exists';
                    //     exit();
                    // } else {
                        $salt = md5($this->input->post('customer_email') . '-' . $this->input->post('customer_first_name') . ' ' . $this->input->post('customer_last_name') . '-' . time());
                        $_id = $this->this_model->register($salt);

                        if (@$_FILES['customer_logo_0']['name'] != "") {
                            if (!is_dir('./carethebear/images/upload/member')) {
                                mkdir('./carethebear/images/upload/member', 0777);
                            }

                            if (!is_dir('./carethebear/images/upload/member/' . ($_id % 4000))) {
                                mkdir('./carethebear/images/upload/member/' . ($_id % 4000), 0777);
                            }

                            if (!is_dir('./carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id)) {
                                mkdir('./carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id, 0777);
                            }

                            $config['upload_path'] = './carethebear/images/upload/member/' . ($_id % 4000) . '/' . $_id . '/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['max_size'] = 20480;
                            $config['overwrite'] = FALSE;
                            $this->load->library('upload', $config);
                            if ($this->upload->do_upload('customer_logo_0')) {
                                $upload_info = $this->upload->data();
                                $this->this_model->update_logo($_id, $upload_info['file_name']);
                            }
                       // }

                        $email = 'socialdevelopmentdepartment@set.or.th';
                        $to = $email;
                        $subject = 'มีบริษัทสมัครสมาชิกเข้ามาใหม่ - ' . config_item('site_name');
                        $message = $this->smarty->fetch('email/register_for_staff.tpl');
                        $message = str_replace('[[company]]', $this->input->post('company_name'), $message);
                        $message = str_replace('[[name]]', $this->input->post('customer_first_name') . ' ' . $this->input->post('customer_last_name'), $message);
                        $message = str_replace('[[position]]', $this->input->post('customer_position'), $message);
                        $message = str_replace('[[department]]', $this->input->post('customer_department'), $message);
                        $message = str_replace('[[tel]]', $this->input->post('customer_phone'), $message);
                        $message = str_replace('[[mobile]]', $this->input->post('customer_phone_partner'), $message);
                        $message = str_replace('[[email]]', $this->input->post('customer_email'), $message);

                        $this->load->library('mailer');
                        $this->mailer->send($email, $subject, $message);

                        if ($this->input->post('customer_select_project') == 'bear,whale') {
                            $_SESSION['register_carethewhale'] = $this->input->post('customer_email');
                            echo 'register_carethewhale';
                        } else {
                            echo 'success';
                        }

                        exit();
                    }
                }
            }
        }
        
        $this->smarty->assign('page_name', 'Registration form');
        $this->smarty->assign('company_type', $this->this_model->get_company_type());
        $this->smarty->assign('company_lists', $this->this_model->get_list_company_master());
        $this->smarty->display('register.tpl');
    }

    public function register_carethewhale()
    {
        if ($this->authen->member_data['is_whale'] == 'Y') {
            //redirect('/carethewhale');
            redirect('/dashboard/carethewhale');
        }

        if ($this->input->post('simple_dropdown_building') != '') {
            if (@$_SESSION['tmp_register_carethewhale']['email'] != '') {
                $member_data = $_SESSION['tmp_register_carethewhale'];
            } else if (@$_SESSION['register_carethewhale'] != '') {
                $member_data = $this->this_model->get_bear_data_by_email($_SESSION['register_carethewhale']);
            } else {
                $member_data = $this->authen->member_data['carethebear'];
            }

            if ($this->this_model->register_carethewhale($member_data)) {
                $email = 'socialdevelopmentdepartment@set.or.th';
                $to = $email;
                $subject = 'มีบริษัทสมัครสมาชิกเข้ามาใหม่ - ' . config_item('site_name');
                $message = $this->smarty->fetch('email/register_for_staff_carethewhale.tpl');
                $message = str_replace('[[company]]', $member_data['company'], $message);
                $message = str_replace('[[name]]', $member_data['name'], $message);
                $message = str_replace('[[tel]]', $member_data['tel'], $message);
                $message = str_replace('[[mobile]]', $member_data['mobile'], $message);
                $message = str_replace('[[email]]', $member_data['email'], $message);

                $this->load->library('mailer');
                $this->mailer->send($email, $subject, $message);

                unset($_SESSION['register_carethewhale']);
                unset($_SESSION['tmp_register_carethewhale']);
                echo 'success';
            }

            exit();
        }

        $this->smarty->assign('page_name', 'Register โครงการ Care the Whale');
        $this->smarty->assign('company_type', $this->this_model->get_whale_company_type());
        $this->smarty->assign('zone', $this->this_model->get_zone());
        $this->smarty->assign('garbage_type', $this->this_model->get_garbage_type());
        $this->smarty->display('register_carethewhale.tpl');
    }

    public function register_success()
    {
        $this->smarty->assign('page_name', 'Register Complete');
        $this->smarty->display('register_success.tpl');
    }
}

