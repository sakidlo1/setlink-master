<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		
		if($this->authen->controller != "")
		{
			$this->this_page = $this->authen->controller;
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_page = "home";
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');

		$this->load->model('home_model', 'this_model');
		
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
        $this->smarty->assign('oauth2_authen_param', config_item('oauth2_authen_param'));
        $this->smarty->assign('callback_url', config_item('callback_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}
	
	public function index()
	{
		$this->smarty->assign('page_name', 'Home');
		$this->smarty->assign('news', $this->this_model->get_news());
		$this->smarty->assign('vdo', $this->this_model->get_vdo());
		$this->smarty->assign('wild_stats', $this->this_model->get_wild_stats());
		$this->smarty->assign('company_wild', $this->this_model->get_company_wild());
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->assign('whale_stats', $this->this_model->get_whale_stats());
		$this->smarty->assign('company_whale', $this->this_model->get_whale_company());
		$this->smarty->display($this->this_page.'.tpl');
	}

	public function care_the_bear()
	{
		$this->smarty->assign('page_name', 'Care the Bear');
		$this->smarty->assign('news', $this->this_model->get_news_bear());
		$this->smarty->assign('activity', $this->this_model->get_activity_bear());
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->assign('whale_stats', $this->this_model->get_whale_stats());
		$this->smarty->assign('company_whale', $this->this_model->get_whale_company());
		$this->smarty->display('care_the_bear.tpl');
	}
	
	public function get_summary_cf($from, $to)
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_summary_cf($from, $to);
		echo json_encode($data);
	}

	public function care_the_bear_about()
	{
		$this->smarty->assign('page_name', 'Care the Bear About');
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->display('care_the_bear_about.tpl');
		
	}

	public function care_the_bear_activity()
	{
		$this->smarty->assign('page_name', 'Care the Bear Activity');
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->display('care_the_bear_activity.tpl');
	}

	public function care_the_bear_project()
	{
		$this->smarty->assign('page_name', 'Care the Bear Project');
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->display('care_the_bear_project.tpl');
	}

	public function care_the_bear_article()
	{
		$this->smarty->assign('page_name', 'Care the Bear Article');
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->display('care_the_bear_article.tpl');
	}

	public function care_the_bear_news()
	{
		$this->smarty->assign('page_name', 'Care the Bear News');
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company_bear', $this->this_model->get_bear_company());
		$this->smarty->display('care_the_bear_news.tpl');
	}

	public function care_the_whale()
	{
		$this->smarty->assign('page_name', 'Care the Whale');
		$this->smarty->assign('news', $this->this_model->get_news_whale());
		$this->smarty->assign('tips', $this->this_model->get_tips_whale());
		$this->smarty->assign('whale_stats', $this->this_model->get_whale_stats());
		$this->smarty->assign('company_whale', $this->this_model->get_whale_company());
		$this->smarty->display('care_the_whale.tpl');
	}


	public function care_the_wild()
	{
		$this->smarty->assign('page_name', 'Care the Wild');
		$this->smarty->assign('wild_stats', $this->this_model->get_wild_stats());
		$this->smarty->assign('company_wild', $this->this_model->get_company_wild());
		$this->smarty->display('care_the_wild.tpl');
	}

	public function thank_you()
	{
		$this->smarty->assign('page_name', 'Thank you');
		$this->smarty->display('thank_you.tpl');
	}

	public function send_email()
	{
		$json = file_get_contents('php://input');
		$data = json_decode($json, true);

		if(count($data) > 0)
		{
			$email = 'climatecarePlatform@set.or.th';
			$subject = 'มีผู้สนใจลงทะเบียนกับโครงการ Care the wild - '.config_item('site_name');

			$message = '<html>';
			$message .= '  <head>';
			$message .= '    <meta charset="utf-8">';
			$message .= '  </head>';
			$message .= '  <body>';
			$message .= '	<div>';
			$message .= '		<br />';
			$message .= '		เรียนเจ้าหน้าที่,';
			$message .= '		<br />';
			$message .= '		<p style="padding-left: 30px;">';
			$message .= '			มีผู้สนใจลงทะเบียนกับโครงการ Care the wild ตามรายละเอียด ดังนี้';
			$message .= '			<br />';
			$message .= '			<br />';
			$message .= '			<table border="1" cellspacing="3" cellpadding="3">';
			$message .= '				<tr>';
			$message .= '					<th>ชื่อ - นามสกุล</th>';
			$message .= '					<td>'.$data['name'].'</td>';
			$message .= '				</tr>';
			$message .= '				<tr>';
			$message .= '					<th>ตำแหน่งงาน</th>';
			$message .= '					<td>'.$data['pos'].'</td>';
			$message .= '				</tr>';
			$message .= '				<tr>';
			$message .= '					<th>บริษัท / องค์กร</th>';
			$message .= '					<td>'.$data['org'].'</td>';
			$message .= '				</tr>';
			$message .= '				<tr>';
			$message .= '					<th>อีเมล</th>';
			$message .= '					<td>'.$data['email'].'</td>';
			$message .= '				</tr>';
			$message .= '				<tr>';
			$message .= '					<th>เบอร์ติดต่อ</th>';
			$message .= '					<td>'.$data['contact'].'</td>';
			$message .= '				</tr>';
			$message .= '				<tr>';
			$message .= '					<th>รายละเอียดเพิ่มเติม</th>';
			$message .= '					<td>'.$data['detail'].'</td>';
			$message .= '				</tr>';
			$message .= '			</table>';
			$message .= '		</p>';
			$message .= '		<br />';
			$message .= '		<br />';
			$message .= '		ขอบคุณค่ะ,';
			$message .= '		<br />';
			$message .= '		'.config_item('site_name');
			$message .= '		<br />';
			$message .= '		<br />';
			$message .= '	</div>';
			$message .= '  </body>';
			$message .= '</html>';

			$this->load->library('mailer');
			$this->mailer->send($email, $subject, $message);
		}
		
		header('Content-Type: application/json');
		echo json_encode(['status' => 'success']);
	}
	
	public function logout()
	{
		unset($_SESSION['climate_member']);
		unset($_SESSION['carethebear_member']);
		unset($_SESSION['member']);
		redirect('/');
	}
}