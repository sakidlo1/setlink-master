<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		
		if($this->authen->controller != "")
		{
			$this->this_page = $this->authen->controller;
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_page = "news";
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');

		$this->load->model('news_model', 'this_model');
		
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}

	public function index()
	{
		$this->smarty->assign('page_name', 'News');
		$this->smarty->display($this->this_page.'.tpl');
	}

	public function get($cat, $page)
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_all($cat, $page);
		echo json_encode($data);
	}

	public function detail($id = 0)
	{
		$data = $this->this_model->get_news_by_id($id);
		$this->smarty->assign('page_name', $data['name']);
		$this->smarty->assign('news_item', $data);
		$this->smarty->display($this->this_page.'_detail.tpl');
	}
}