<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {	
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		
		if($this->authen->controller != "")
		{
			$this->this_page = $this->authen->controller;
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_page = "dashboard";
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');

		$this->load->model('dashboard_model', 'this_model');
		
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		if($this->authen->id <= 0)
		{
			redirect('/');
		}
	}

	public function index()
	{
        $authen = $this->authen->member_data;
		$this->smarty->assign('page_name', 'Dashboard');
		$this->smarty->assign('stats', $this->this_model->get_dashboard_stats());
        $data_wild = [];
		if($authen['is_bear'] === 'Y' && $authen['carethebear']['company_master_id'] !== null) {
            $data_wild = $this->this_model->get_data_wild($authen['carethebear']['company_master_id']);
        }else if($authen['is_whale'] === 'Y' && $authen['carethewhale']['company_id'] !== null){
            $result = $this->this_model->check_whale_id_from_company_master($authen['carethewhale']['company_id']);
            if(!empty($result)) {
                $data_wild = $this->this_model->get_data_wild($result['id']);
            }
        }
		$this->smarty->assign('data_wild', $data_wild);
		$this->smarty->display($this->this_page.'.tpl');
	}

	public function outcome()
	{
		if($this->input->get('start') != '')
		{
			$start = $this->input->get('start');
		}
		else
		{
			$start = '2020-01-01';
		}

		if($this->input->get('end') != '')
		{
			$end = $this->input->get('end');
		}
		else
		{
			$end = date('Y-m-d');
		}
		
		$this->smarty->assign('page_name', 'Outcome');
		$this->smarty->assign('stats', $this->this_model->get_outcome_stats($start, $end));
		$this->smarty->display('outcome.tpl');
	}

	public function carethebear()
	{
		$this->smarty->assign('page_name', 'carethebear');
        $this->smarty->assign('list', $this->this_model->get_project_stats());
        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());

		$this->smarty->display($this->this_page.'_carethebear.tpl');
	}

	public function carethebear_activity()
	{
		$this->smarty->assign('page_name', 'carethebear activity');
        $this->smarty->assign('list', $this->this_model->get_activity_stats());
        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());
		$this->smarty->display($this->this_page.'_carethebear_activity.tpl');
	}

	public function carethebear_document()
	{
		$this->smarty->assign('page_name', 'carethebear document');
        $this->smarty->assign('document', $this->this_model->get_document_stats());
        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());

		$this->smarty->display($this->this_page.'_carethebear_document.tpl');
	}

	public function carethebear_report()
	{
		$this->smarty->assign('page_name', 'carethebear report');
		$this->smarty->assign('list_act', $this->this_model->get_activity_stats());
		$this->smarty->assign('list', $this->this_model->get_project_stats());
		$this->smarty->assign('list_tsd', $this->this_model->get_tsd_stats());
		$this->smarty->assign('list_comp_activity', $this->this_model->get_company_activity());
		$this->smarty->assign('list_comp_project', $this->this_model->get_company_project());
		
	
        // $this->smarty->assign('report', $this->this_model->get_report_stats());
        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());

		$this->smarty->display($this->this_page.'_carethebear_report.tpl');
	}

	public function carethewhale()
	{
		$this->smarty->assign('page_name', 'carethewhale');
		$this->smarty->display($this->this_page.'_carethewhale.tpl');
	}

	public function tgo()
	{
		$this->smarty->assign('page_name', 'tgo');
		$this->smarty->display($this->this_page.'_tgo.tpl');
	}

	public function carethewild()
	{
        $authen = $this->authen->member_data;
        $data_wild = [];
        if(!empty($authen['carethebear']['company_master_id'])) {
            $result = $this->this_model->check_wild_from_bear_login($authen['carethebear']['company_master_id']);
            if($result === true) {
                $data_wild = $this->this_model->get_data_wild($authen['carethebear']['company_master_id']);
            }
        }else{
            if($authen['is_whale'] === 'Y' && !empty($authen['carethewhale']['company_id'])) {
                $result = $this->this_model->check_whale_id_from_company_master($authen['carethewhale']['company_id']);
                if(!empty($result)) {
                    $data_wild = $this->this_model->get_data_wild($result['id']);
                }
            }
        }
        if(!empty($data_wild)) {
            $data_plant = [];
            foreach ($data_wild as $val) {
                $data_plant[] = [
                    'category' => 'successed',
                    'location' => $val['name_wild'],
                    'location_sub' => $val['location_wild'],
                    'icon_type' => 'forest1',
                    'box_img_bg' => 'https://climatecare.setsocialimpact.com/images/theme/default/public/images/wild/jungle-box-bg_section5.png', /* TODO: find constatnt or something like that (image path) */
                    'box_img_alt' => 'test',
                    'link' => $val['link']
                ];
            }
            $this->smarty->assign('page_name', 'carethewild');
            $this->smarty->assign('data_wild', $data_wild);
            $this->smarty->assign('data_plant', $data_plant);
            $this->smarty->display($this->this_page.'_carethewild.tpl');
        }else{
            $this->smarty->display('care_the_wild.tpl');
        }


	}
}