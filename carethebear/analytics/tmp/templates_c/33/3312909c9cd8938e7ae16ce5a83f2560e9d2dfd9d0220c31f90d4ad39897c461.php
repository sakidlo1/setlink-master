<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CorePluginsAdmin/macros.twig */
class __TwigTemplate_a33fa7aee70f098fc96c784e993b23bf871516ebfdede2b7ef7188d6bf431e48 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 116
        echo "
";
        // line 126
        echo "
";
        // line 147
        echo "
";
    }

    // line 2
    public function macro_tablePluginUpdates($__pluginsHavingUpdate__ = null, $__updateNonce__ = null, $__isMultiServerEnvironment__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "pluginsHavingUpdate" => $__pluginsHavingUpdate__,
            "updateNonce" => $__updateNonce__,
            "isMultiServerEnvironment" => $__isMultiServerEnvironment__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 3
            echo "    ";
            $macros["marketplaceMacro"] = $this->loadTemplate("@Marketplace/macros.twig", "@CorePluginsAdmin/macros.twig", 3)->unwrap();
            // line 4
            echo "
    <div>
        <a id=\"update-selected-plugins\" href=\"javascript:\" class=\"btn disabled\">";
            // line 6
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_UpdateSelected"]), "html", null, true);
            echo "</a>
    </div>
    <table piwik-content-table>
        <thead>
        <tr>
            <th>
                <span class=\"checkbox-container\">
                    <label>
                        <input type=\"checkbox\" id=\"select-plugin-all\"/>
                        <span></span>
                    </label>
                </span>
            </th>
            <th>";
            // line 19
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Plugin"]), "html", null, true);
            echo "</th>
            <th class=\"num\">";
            // line 20
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Version"]), "html", null, true);
            echo "</th>
            <th>";
            // line 21
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Description"]), "html", null, true);
            echo "</th>
            <th class=\"status\">";
            // line 22
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Status"]), "html", null, true);
            echo "</th>
            <th class=\"action-links\">";
            // line 23
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Action"]), "html", null, true);
            echo "</th>
        </tr>
        </thead>
        <tbody id=\"plugins\">
        ";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pluginsHavingUpdate"]) || array_key_exists("pluginsHavingUpdate", $context) ? $context["pluginsHavingUpdate"] : (function () { throw new RuntimeError('Variable "pluginsHavingUpdate" does not exist.', 27, $this->source); })()));
            foreach ($context['_seq'] as $context["name"] => $context["plugin"]) {
                // line 28
                echo "            <tr ";
                if (((twig_get_attribute($this->env, $this->source, $context["plugin"], "isActivated", [], "any", true, true, false, 28)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["plugin"], "isActivated", [], "any", false, false, false, 28), false)) : (false))) {
                    echo "class=\"active-plugin\"";
                } else {
                    echo "class=\"inactive-plugin\"";
                }
                echo ">
                <td class=\"select-cell\">
                    <span class=\"checkbox-container\">
                        <label>
                            <input type=\"checkbox\" id=\"select-plugin-";
                // line 32
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 32), "html_attr");
                echo "\" ";
                if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "isDownloadable", [], "any", true, true, false, 32) &&  !twig_get_attribute($this->env, $this->source, $context["plugin"], "isDownloadable", [], "any", false, false, false, 32))) {
                    echo "disabled=\"disabled\"";
                }
                echo " />
                            <span></span>
                        </label>
                    </span>
                </td>
                <td class=\"name\">
                    <a href=\"javascript:void(0);\" piwik-plugin-name=\"";
                // line 38
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 38), "html_attr");
                echo "\" class=\"plugin-details\">
                        ";
                // line 39
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 39), "html", null, true);
                echo "
                    </a>
                </td>
                <td class=\"vers\">
                    ";
                // line 43
                if ((((twig_get_attribute($this->env, $this->source, $context["plugin"], "changelog", [], "any", true, true, false, 43) && twig_get_attribute($this->env, $this->source, $context["plugin"], "changelog", [], "any", false, false, false, 43)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "changelog", [], "any", false, true, false, 43), "url", [], "any", true, true, false, 43)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "changelog", [], "any", false, false, false, 43), "url", [], "any", false, false, false, 43))) {
                    // line 44
                    echo "                        <a href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "changelog", [], "any", false, false, false, 44), "url", [], "any", false, false, false, 44), "html_attr");
                    echo "\" title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Changelog"]), "html", null, true);
                    echo "\"
                           target=\"_blank\" rel=\"noreferrer noopener\"
                        >";
                    // line 46
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "currentVersion", [], "any", false, false, false, 46), "html", null, true);
                    echo " => ";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "latestVersion", [], "any", false, false, false, 46), "html", null, true);
                    echo "</a>
                    ";
                } else {
                    // line 48
                    echo "                        ";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "currentVersion", [], "any", false, false, false, 48), "html", null, true);
                    echo " => ";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "latestVersion", [], "any", false, false, false, 48), "html", null, true);
                    echo "
                    ";
                }
                // line 50
                echo "                </td>
                <td class=\"desc\">
                    ";
                // line 52
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "description", [], "any", false, false, false, 52), "html", null, true);
                echo "
                    ";
                // line 53
                echo twig_call_macro($macros["marketplaceMacro"], "macro_missingRequirementsPleaseUpdateNotice", [$context["plugin"]], 53, $context, $this->getSourceContext());
                echo "
                </td>
                <td class=\"status\">
                    ";
                // line 56
                if (twig_get_attribute($this->env, $this->source, $context["plugin"], "isActivated", [], "any", false, false, false, 56)) {
                    // line 57
                    echo "                        ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Active"]), "html", null, true);
                    echo "
                    ";
                } else {
                    // line 59
                    echo "                        ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Inactive"]), "html", null, true);
                    echo "
                    ";
                }
                // line 61
                echo "                </td>
                <td class=\"togl action-links\">
                    ";
                // line 63
                if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "isDownloadable", [], "any", true, true, false, 63) &&  !twig_get_attribute($this->env, $this->source, $context["plugin"], "isDownloadable", [], "any", false, false, false, 63))) {
                    // line 64
                    echo "                        <span title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_PluginNotDownloadable"]), "html_attr");
                    echo " ";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "isPaid", [], "any", false, false, false, 64)) {
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_PluginNotDownloadablePaidReason"]), "html_attr");
                    }
                    echo "\"
                          >";
                    // line 65
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_NotDownloadable"]), "html_attr");
                    echo "</span>
                    ";
                } elseif (                // line 66
(isset($context["isMultiServerEnvironment"]) || array_key_exists("isMultiServerEnvironment", $context) ? $context["isMultiServerEnvironment"] : (function () { throw new RuntimeError('Variable "isMultiServerEnvironment" does not exist.', 66, $this->source); })())) {
                    // line 67
                    echo "                        <a onclick=\"\$(this).css('display', 'none')\" href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["action" => "download", "module" => "Marketplace", "pluginName" => twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 67), "nonce" => Piwik\Nonce::getNonce(twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 67))]]), "html", null, true);
                    echo "\">";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Download"]), "html", null, true);
                    echo "</a>
                    ";
                } elseif ((0 === twig_compare(0, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source,                 // line 68
$context["plugin"], "missingRequirements", [], "any", false, false, false, 68))))) {
                    // line 69
                    echo "                        <a href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["action" => "updatePlugin", "module" => "Marketplace", "pluginName" => twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 69), "nonce" => (isset($context["updateNonce"]) || array_key_exists("updateNonce", $context) ? $context["updateNonce"] : (function () { throw new RuntimeError('Variable "updateNonce" does not exist.', 69, $this->source); })())]]), "html", null, true);
                    echo "\">";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreUpdater_UpdateTitle"]), "html", null, true);
                    echo "</a>
                    ";
                } else {
                    // line 71
                    echo "                        -
                    ";
                }
                // line 73
                echo "                </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "        </tbody>
    </table>

    <script>
        \$(function () {
            \$('span.checkbox-container').on('change', function() {
                var isAtLeastOneChecked = \$('span.checkbox-container input:checked').length >= 1;
                isAtLeastOneChecked ? \$('#update-selected-plugins').removeClass('disabled') : \$('#update-selected-plugins').addClass('disabled');
            });

            \$('#update-selected-plugins').on('click', function () {
                var pluginsToUpdate = [];
                \$('tbody#plugins td.select-cell input').each(function () {
                    if (!this.checked) {
                        return;
                    }

                    var pluginName = \$(this).closest('tr').find('.name .plugin-details').attr('piwik-plugin-name');
                    pluginsToUpdate.push(pluginName);
                });

                var url = '";
            // line 97
            echo call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "updatePlugin", "nonce" => (isset($context["updateNonce"]) || array_key_exists("updateNonce", $context) ? $context["updateNonce"] : (function () { throw new RuntimeError('Variable "updateNonce" does not exist.', 97, $this->source); })())]]);
            echo "&pluginName=' + encodeURIComponent(pluginsToUpdate.join(','));
                window.location.href = url;

                \$(this).prop('disabled', true);
            });

            \$('#select-plugin-all').on('change', function () {
                var self = this;
                \$('tbody#plugins td.select-cell input[type=checkbox]').each(function () {
                    if (\$(this).prop('disabled')) {
                        return;
                    }
                    \$(this).prop('checked', self.checked);
                });
            });
        });
    </script>

";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 117
    public function macro_pluginActivateDeactivateAction($__name__ = null, $__isActivated__ = null, $__missingRequirements__ = null, $__deactivateNonce__ = null, $__activateNonce__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "name" => $__name__,
            "isActivated" => $__isActivated__,
            "missingRequirements" => $__missingRequirements__,
            "deactivateNonce" => $__deactivateNonce__,
            "activateNonce" => $__activateNonce__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 118
            if ((isset($context["isActivated"]) || array_key_exists("isActivated", $context) ? $context["isActivated"] : (function () { throw new RuntimeError('Variable "isActivated" does not exist.', 118, $this->source); })())) {
                // line 119
                echo "<a href='index.php?module=CorePluginsAdmin&action=deactivate&pluginName=";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 119, $this->source); })()), "html", null, true);
                echo "&nonce=";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["deactivateNonce"]) || array_key_exists("deactivateNonce", $context) ? $context["deactivateNonce"] : (function () { throw new RuntimeError('Variable "deactivateNonce" does not exist.', 119, $this->source); })()), "html", null, true);
                echo "&redirectTo=referrer'>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Deactivate"]), "html", null, true);
                echo "</a>";
            } elseif (            // line 120
(isset($context["missingRequirements"]) || array_key_exists("missingRequirements", $context) ? $context["missingRequirements"] : (function () { throw new RuntimeError('Variable "missingRequirements" does not exist.', 120, $this->source); })())) {
                // line 121
                echo "        -
    ";
            } else {
                // line 123
                echo "<a href='index.php?module=CorePluginsAdmin&action=activate&pluginName=";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["name"]) || array_key_exists("name", $context) ? $context["name"] : (function () { throw new RuntimeError('Variable "name" does not exist.', 123, $this->source); })()), "html", null, true);
                echo "&nonce=";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["activateNonce"]) || array_key_exists("activateNonce", $context) ? $context["activateNonce"] : (function () { throw new RuntimeError('Variable "activateNonce" does not exist.', 123, $this->source); })()), "html", null, true);
                echo "&redirectTo=referrer'>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Activate"]), "html", null, true);
                echo "</a>";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 127
    public function macro_pluginsFilter(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 128
            echo "
    <p class=\"row pluginsFilter\" piwik-plugin-filter>
        <span class=\"origin\">
            <strong>";
            // line 131
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Origin"]), "html", null, true);
            echo "</strong>
            <a data-filter-origin=\"all\" href=\"#\" class=\"active\">";
            // line 132
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_All"]), "html", null, true);
            echo "<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"core\" href=\"#\">";
            // line 133
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_OriginCore"]), "html", null, true);
            echo "<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"official\" href=\"#\">";
            // line 134
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_OriginOfficial"]), "html", null, true);
            echo "<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"thirdparty\" href=\"#\">";
            // line 135
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_OriginThirdParty"]), "html", null, true);
            echo "<span class=\"counter\"></span></a>
        </span>

        <span class=\"status\">
            <strong>";
            // line 139
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Status"]), "html", null, true);
            echo "</strong>
            <a data-filter-status=\"all\" href=\"#\" class=\"active\">";
            // line 140
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_All"]), "html", null, true);
            echo "<span class=\"counter\"></span></a> |
            <a data-filter-status=\"active\" href=\"#\">";
            // line 141
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Active"]), "html", null, true);
            echo "<span class=\"counter\"></span></a> |
            <a data-filter-status=\"inactive\" href=\"#\">";
            // line 142
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Inactive"]), "html", null, true);
            echo "<span class=\"counter\"></span></a>
        </span>
    </p>

";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 148
    public function macro_tablePlugins($__pluginsInfo__ = null, $__pluginNamesHavingSettings__ = null, $__activateNonce__ = null, $__deactivateNonce__ = null, $__uninstallNonce__ = null, $__isTheme__ = null, $__marketplacePluginNames__ = null, $__displayAdminLinks__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "pluginsInfo" => $__pluginsInfo__,
            "pluginNamesHavingSettings" => $__pluginNamesHavingSettings__,
            "activateNonce" => $__activateNonce__,
            "deactivateNonce" => $__deactivateNonce__,
            "uninstallNonce" => $__uninstallNonce__,
            "isTheme" => $__isTheme__,
            "marketplacePluginNames" => $__marketplacePluginNames__,
            "displayAdminLinks" => $__displayAdminLinks__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 149
            echo "
    <div id=\"confirmUninstallPlugin\" class=\"ui-confirm\">

        <h2 id=\"uninstallPluginConfirm\">";
            // line 152
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_UninstallConfirm"]), "html", null, true);
            echo "</h2>
        <input role=\"yes\" type=\"button\" value=\"";
            // line 153
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
            echo "\"/>
        <input role=\"no\" type=\"button\" value=\"";
            // line 154
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
            echo "\"/>

    </div>

    <table piwik-content-table>
        <thead>
        <tr>
            <th>";
            // line 161
            if ((isset($context["isTheme"]) || array_key_exists("isTheme", $context) ? $context["isTheme"] : (function () { throw new RuntimeError('Variable "isTheme" does not exist.', 161, $this->source); })())) {
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Theme"]), "html", null, true);
            } else {
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Plugin"]), "html", null, true);
            }
            echo "</th>
            <th>";
            // line 162
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Description"]), "html", null, true);
            echo "</th>
            <th class=\"status\">";
            // line 163
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Status"]), "html", null, true);
            echo "</th>
            ";
            // line 164
            if ((isset($context["displayAdminLinks"]) || array_key_exists("displayAdminLinks", $context) ? $context["displayAdminLinks"] : (function () { throw new RuntimeError('Variable "displayAdminLinks" does not exist.', 164, $this->source); })())) {
                // line 165
                echo "                <th class=\"action-links\">";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Action"]), "html", null, true);
                echo "</th>
            ";
            }
            // line 167
            echo "        </tr>
        </thead>
        <tbody id=\"plugins\">
        ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pluginsInfo"]) || array_key_exists("pluginsInfo", $context) ? $context["pluginsInfo"] : (function () { throw new RuntimeError('Variable "pluginsInfo" does not exist.', 170, $this->source); })()));
            foreach ($context['_seq'] as $context["name"] => $context["plugin"]) {
                // line 171
                echo "            ";
                $context["isDefaultTheme"] = ((isset($context["isTheme"]) || array_key_exists("isTheme", $context) ? $context["isTheme"] : (function () { throw new RuntimeError('Variable "isTheme" does not exist.', 171, $this->source); })()) && (0 === twig_compare($context["name"], "Morpheus")));
                // line 172
                echo "            ";
                if (((twig_get_attribute($this->env, $this->source, $context["plugin"], "alwaysActivated", [], "any", true, true, false, 172) &&  !twig_get_attribute($this->env, $this->source, $context["plugin"], "alwaysActivated", [], "any", false, false, false, 172)) || (isset($context["isTheme"]) || array_key_exists("isTheme", $context) ? $context["isTheme"] : (function () { throw new RuntimeError('Variable "isTheme" does not exist.', 172, $this->source); })()))) {
                    // line 173
                    echo "                <tr ";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "activated", [], "any", false, false, false, 173)) {
                        echo "class=\"active-plugin\"";
                    } else {
                        echo "class=\"inactive-plugin\"";
                    }
                    echo " data-filter-status=\"";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "activated", [], "any", false, false, false, 173)) {
                        echo "active";
                    } else {
                        echo "inactive";
                    }
                    echo "\" data-filter-origin=\"";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "isCorePlugin", [], "any", false, false, false, 173)) {
                        echo "core";
                    } elseif (twig_get_attribute($this->env, $this->source, $context["plugin"], "isOfficialPlugin", [], "any", false, false, false, 173)) {
                        echo "official";
                    } else {
                        echo "thirdparty";
                    }
                    echo "\">
                    <td class=\"name\">
                        <a name=\"";
                    // line 175
                    echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                    echo "\"></a>
                        ";
                    // line 176
                    if (( !twig_get_attribute($this->env, $this->source, $context["plugin"], "isCorePlugin", [], "any", false, false, false, 176) && twig_in_filter($context["name"], (isset($context["marketplacePluginNames"]) || array_key_exists("marketplacePluginNames", $context) ? $context["marketplacePluginNames"] : (function () { throw new RuntimeError('Variable "marketplacePluginNames" does not exist.', 176, $this->source); })())))) {
                        // line 177
                        echo "<a href=\"javascript:void(0);\"
                               piwik-plugin-name=\"";
                        // line 178
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                        echo "\"
                            >";
                        // line 179
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html", null, true);
                        echo "</a>";
                    } else {
                        // line 181
                        echo "                            ";
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html", null, true);
                        echo "
                        ";
                    }
                    // line 183
                    echo "                        <span class=\"plugin-version\" ";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "isCorePlugin", [], "any", false, false, false, 183)) {
                        echo "title=\"";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_CorePluginTooltip"]), "html", null, true);
                        echo "\"";
                    }
                    echo ">(";
                    if (twig_get_attribute($this->env, $this->source, $context["plugin"], "isCorePlugin", [], "any", false, false, false, 183)) {
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_OriginCore"]), "html", null, true);
                    } else {
                        echo "v";
                        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 183), "version", [], "any", false, false, false, 183), "html", null, true);
                    }
                    echo ")</span>

                        ";
                    // line 185
                    if (twig_in_filter($context["name"], (isset($context["pluginNamesHavingSettings"]) || array_key_exists("pluginNamesHavingSettings", $context) ? $context["pluginNamesHavingSettings"] : (function () { throw new RuntimeError('Variable "pluginNamesHavingSettings" does not exist.', 185, $this->source); })()))) {
                        // line 186
                        echo "                            <br /><br />
                            <a href=\"";
                        // line 187
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "CoreAdminHome", "action" => "generalSettings"]]), "html", null, true);
                        echo "#";
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                        echo "\" class=\"settingsLink\">";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Settings"]), "html", null, true);
                        echo "</a>
                        ";
                    }
                    // line 189
                    echo "                    </td>
                    <td class=\"desc\">
                        <div class=\"plugin-desc-missingrequirements\">
                            ";
                    // line 192
                    if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "missingRequirements", [], "any", true, true, false, 192) && twig_get_attribute($this->env, $this->source, $context["plugin"], "missingRequirements", [], "any", false, false, false, 192))) {
                        // line 193
                        echo "                                ";
                        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "missingRequirements", [], "any", false, false, false, 193), "html", null, true);
                        echo "
                                <br />
                            ";
                    }
                    // line 196
                    echo "                        </div>
                        <div class=\"plugin-desc-text\">

                            ";
                    // line 199
                    echo nl2br(\Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 199), "description", [], "any", false, false, false, 199), "html", null, true));
                    echo "

                            ";
                    // line 201
                    if (( !twig_test_empty(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 201), "homepage", [], "any", true, true, false, 201)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 201), "homepage", [], "any", false, false, false, 201))) : (""))) && !twig_in_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 201), "homepage", [], "any", false, false, false, 201), [0 => "http://piwik.org", 1 => "http://www.piwik.org", 2 => "http://piwik.org/", 3 => "http://www.piwik.org/", 4 => "https://piwik.org", 5 => "https://www.piwik.org", 6 => "https://piwik.org/", 7 => "https://www.piwik.org/", 8 => "http://matomo.org", 9 => "http://www.matomo.org", 10 => "http://matomo.org/", 11 => "http://www.matomo.org/", 12 => "https://matomo.org", 13 => "https://www.matomo.org", 14 => "https://matomo.org/", 15 => "https://www.matomo.org/"]))) {
                        // line 207
                        echo "                                <span class=\"plugin-homepage\">
                            <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"";
                        // line 208
                        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 208), "homepage", [], "any", false, false, false, 208), "html_attr");
                        echo "\">(";
                        echo twig_replace_filter(call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_PluginHomepage"]), [" " => "&nbsp;"]);
                        echo ")</a>
                        </span>
                            ";
                    }
                    // line 211
                    echo "
                            ";
                    // line 212
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 212), "donate", [], "any", true, true, false, 212) && twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 212), "donate", [], "any", false, false, false, 212)))) {
                        // line 213
                        echo "                                <div class=\"plugin-donation\">
                                    ";
                        // line 214
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_LikeThisPlugin"]), "html", null, true);
                        echo " <a href=\"javascript:;\" class=\"plugin-donation-link\" data-overlay-id=\"overlay-";
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                        echo "\">";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_ConsiderDonating"]), "html", null, true);
                        echo "</a>
                                    <div id=\"overlay-";
                        // line 215
                        echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                        echo "\" class=\"donation-overlay ui-confirm\" title=\"";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_LikeThisPlugin"]), "html", null, true);
                        echo "\">
                                        <p>";
                        // line 216
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_CommunityContributedPlugin"]), "html", null, true);
                        echo "</p>
                                        <p>";
                        // line 217
                        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_ConsiderDonatingCreatorOf", (("<b>" . $context["name"]) . "</b>")]);
                        echo "</p>
                                        <div class=\"donation-links\">
                                            ";
                        // line 219
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 219), "donate", [], "any", false, true, false, 219), "paypal", [], "any", true, true, false, 219) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 219), "donate", [], "any", false, false, false, 219), "paypal", [], "any", false, false, false, 219))) {
                            // line 220
                            echo "                                                <a class=\"donation-link paypal\" target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_donations&item_name=Matomo%20Plugin%20";
                            echo \Piwik\piwik_escape_filter($this->env, \Piwik\piwik_escape_filter($this->env, $context["name"], "url"), "html", null, true);
                            echo "&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted&business=";
                            echo \Piwik\piwik_escape_filter($this->env, \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 220), "donate", [], "any", false, false, false, 220), "paypal", [], "any", false, false, false, 220), "url"), "html", null, true);
                            echo "\"><img src=\"plugins/CorePluginsAdmin/images/paypal_donate.png\" height=\"30\"/></a>
                                            ";
                        }
                        // line 222
                        echo "                                            ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 222), "donate", [], "any", false, true, false, 222), "flattr", [], "any", true, true, false, 222) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 222), "donate", [], "any", false, false, false, 222), "flattr", [], "any", false, false, false, 222))) {
                            // line 223
                            echo "                                                <a class=\"donation-link flattr\" target=\"_blank\" rel=\"noreferrer noopener\" href=\"";
                            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 223), "donate", [], "any", false, false, false, 223), "flattr", [], "any", false, false, false, 223), "html", null, true);
                            echo "\"><img class=\"alignnone\" title=\"Flattr\" alt=\"\" src=\"plugins/CorePluginsAdmin/images/flattr.png\" height=\"29\" /></a>
                                            ";
                        }
                        // line 225
                        echo "                                            ";
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 225), "donate", [], "any", false, true, false, 225), "bitcoin", [], "any", true, true, false, 225) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 225), "donate", [], "any", false, false, false, 225), "bitcoin", [], "any", false, false, false, 225))) {
                            // line 226
                            echo "                                                <div class=\"donation-link bitcoin\">
                                                    <span>Donate Bitcoins to:</span>
                                                    <a href=\"bitcoin:";
                            // line 228
                            echo \Piwik\piwik_escape_filter($this->env, \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 228), "donate", [], "any", false, false, false, 228), "bitcoin", [], "any", false, false, false, 228), "url"), "html", null, true);
                            echo "\">";
                            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 228), "donate", [], "any", false, false, false, 228), "bitcoin", [], "any", false, false, false, 228), "html", null, true);
                            echo "</a>
                                                </div>
                                            ";
                        }
                        // line 231
                        echo "                                        </div>
                                        <input role=\"no\" type=\"button\" value=\"";
                        // line 232
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Close"]), "html", null, true);
                        echo "\"/>
                                    </div>
                                </div>
                            ";
                    }
                    // line 236
                    echo "                        </div>
                        ";
                    // line 237
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 237), "license", [], "any", true, true, false, 237)) {
                        // line 238
                        echo "                            <div class=\"plugin-license\">
                                ";
                        // line 239
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 239), "license_file", [], "any", true, true, false, 239)) {
                            echo "<a title=\"";
                            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_LicenseHomepage"]), "html", null, true);
                            echo "\" rel=\"noreferrer noopener\" target=\"_blank\" href=\"index.php?module=CorePluginsAdmin&action=showLicense&pluginName=";
                            echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html", null, true);
                            echo "\">";
                        }
                        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 239), "license", [], "any", false, false, false, 239), "html", null, true);
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 239), "license_file", [], "any", true, true, false, 239)) {
                            echo "</a>";
                        }
                        // line 240
                        echo "                            </div>
                        ";
                    }
                    // line 242
                    echo "                        ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 242), "authors", [], "any", true, true, false, 242)) {
                        // line 243
                        echo "                            <div class=\"plugin-author\">
                                By
                                    ";
                        // line 245
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, true, false, 245), "authors", [], "any", true, true, false, 245)) {
                            // line 246
                            ob_start();
                            // line 247
                            echo "                                        ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_array_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 247), "authors", [], "any", false, false, false, 247), function ($__author__) use ($context, $macros) { $context["author"] = $__author__; return twig_get_attribute($this->env, $this->source, $context["author"], "name", [], "any", false, false, false, 247); }));
                            $context['loop'] = [
                              'parent' => $context['_parent'],
                              'index0' => 0,
                              'index'  => 1,
                              'first'  => true,
                            ];
                            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                                $length = count($context['_seq']);
                                $context['loop']['revindex0'] = $length - 1;
                                $context['loop']['revindex'] = $length;
                                $context['loop']['length'] = $length;
                                $context['loop']['last'] = 1 === $length;
                            }
                            foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
                                // line 248
                                echo "                                            ";
                                if (twig_get_attribute($this->env, $this->source, $context["author"], "homepage", [], "any", true, true, false, 248)) {
                                    // line 249
                                    echo "                                                <a title=\"";
                                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_AuthorHomepage"]), "html", null, true);
                                    echo "\" href=\"";
                                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "homepage", [], "any", false, false, false, 249), "html", null, true);
                                    echo "\" rel=\"noreferrer noopener\" target=\"_blank\">";
                                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "name", [], "any", false, false, false, 249), "html", null, true);
                                    echo "</a>
                                            ";
                                } else {
                                    // line 251
                                    echo "                                                ";
                                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["author"], "name", [], "any", false, false, false, 251), "html", null, true);
                                    echo "
                                            ";
                                }
                                // line 253
                                echo "                                            ";
                                if ((-1 === twig_compare(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 253), twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["plugin"], "info", [], "any", false, false, false, 253), "authors", [], "any", false, false, false, 253))))) {
                                    // line 254
                                    echo "                                                ,
                                            ";
                                }
                                // line 256
                                echo "                                        ";
                                ++$context['loop']['index0'];
                                ++$context['loop']['index'];
                                $context['loop']['first'] = false;
                                if (isset($context['loop']['length'])) {
                                    --$context['loop']['revindex0'];
                                    --$context['loop']['revindex'];
                                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                                }
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 257
                            echo "                                    ";
                            $___internal_89dd2115bd33de829882b09e36fd85ad25f6dc21e8ecd982af45c2eeaaec2ed6_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                            // line 246
                            echo twig_spaceless($___internal_89dd2115bd33de829882b09e36fd85ad25f6dc21e8ecd982af45c2eeaaec2ed6_);
                        }
                        // line 258
                        echo ".
                            </div>
                        ";
                    }
                    // line 261
                    echo "                    </td>
                    <td class=\"status\" ";
                    // line 262
                    if ((isset($context["isDefaultTheme"]) || array_key_exists("isDefaultTheme", $context) ? $context["isDefaultTheme"] : (function () { throw new RuntimeError('Variable "isDefaultTheme" does not exist.', 262, $this->source); })())) {
                        echo "style=\"border-left-width:0px;\"";
                    }
                    echo ">
                        ";
                    // line 263
                    if ( !(isset($context["isDefaultTheme"]) || array_key_exists("isDefaultTheme", $context) ? $context["isDefaultTheme"] : (function () { throw new RuntimeError('Variable "isDefaultTheme" does not exist.', 263, $this->source); })())) {
                        // line 265
                        if (twig_get_attribute($this->env, $this->source, $context["plugin"], "activated", [], "any", false, false, false, 265)) {
                            // line 266
                            echo "                                ";
                            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Active"]), "html", null, true);
                            echo "
                            ";
                        } else {
                            // line 268
                            echo "                                ";
                            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Inactive"]), "html", null, true);
                            echo "
                                ";
                            // line 269
                            if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "uninstallable", [], "any", false, false, false, 269) && (isset($context["displayAdminLinks"]) || array_key_exists("displayAdminLinks", $context) ? $context["displayAdminLinks"] : (function () { throw new RuntimeError('Variable "displayAdminLinks" does not exist.', 269, $this->source); })()))) {
                                echo " <br/> - <a data-plugin-name=\"";
                                echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html_attr");
                                echo "\" class=\"uninstall\" href='index.php?module=CorePluginsAdmin&action=uninstall&pluginName=";
                                echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html", null, true);
                                echo "&nonce=";
                                echo \Piwik\piwik_escape_filter($this->env, (isset($context["uninstallNonce"]) || array_key_exists("uninstallNonce", $context) ? $context["uninstallNonce"] : (function () { throw new RuntimeError('Variable "uninstallNonce" does not exist.', 269, $this->source); })()), "html", null, true);
                                echo "'>";
                                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_ActionUninstall"]), "html", null, true);
                                echo "</a>";
                            }
                            // line 270
                            echo "                            ";
                        }
                    }
                    // line 273
                    echo "                    </td>

                    ";
                    // line 275
                    if ((isset($context["displayAdminLinks"]) || array_key_exists("displayAdminLinks", $context) ? $context["displayAdminLinks"] : (function () { throw new RuntimeError('Variable "displayAdminLinks" does not exist.', 275, $this->source); })())) {
                        // line 276
                        echo "                        <td class=\"togl action-links\" ";
                        if ((isset($context["isDefaultTheme"]) || array_key_exists("isDefaultTheme", $context) ? $context["isDefaultTheme"] : (function () { throw new RuntimeError('Variable "isDefaultTheme" does not exist.', 276, $this->source); })())) {
                            echo "style=\"border-left-width:0px;\"";
                        }
                        echo ">
                            ";
                        // line 277
                        if ( !(isset($context["isDefaultTheme"]) || array_key_exists("isDefaultTheme", $context) ? $context["isDefaultTheme"] : (function () { throw new RuntimeError('Variable "isDefaultTheme" does not exist.', 277, $this->source); })())) {
                            // line 279
                            if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "invalid", [], "any", true, true, false, 279) || twig_get_attribute($this->env, $this->source, $context["plugin"], "alwaysActivated", [], "any", false, false, false, 279))) {
                                // line 280
                                echo "                                    -
                                ";
                            } else {
                                // line 282
                                echo "                                    ";
                                echo twig_call_macro($macros["_self"], "macro_pluginActivateDeactivateAction", [$context["name"], twig_get_attribute($this->env, $this->source, $context["plugin"], "activated", [], "any", false, false, false, 282), twig_get_attribute($this->env, $this->source, $context["plugin"], "missingRequirements", [], "any", false, false, false, 282), (isset($context["deactivateNonce"]) || array_key_exists("deactivateNonce", $context) ? $context["deactivateNonce"] : (function () { throw new RuntimeError('Variable "deactivateNonce" does not exist.', 282, $this->source); })()), (isset($context["activateNonce"]) || array_key_exists("activateNonce", $context) ? $context["activateNonce"] : (function () { throw new RuntimeError('Variable "activateNonce" does not exist.', 282, $this->source); })())], 282, $context, $this->getSourceContext());
                                echo "
                                ";
                            }
                        }
                        // line 286
                        echo "                        </td>
                    ";
                    }
                    // line 288
                    echo "                </tr>
            ";
                }
                // line 290
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 291
            echo "        </tbody>
    </table>

    ";
            // line 294
            if ((isset($context["displayAdminLinks"]) || array_key_exists("displayAdminLinks", $context) ? $context["displayAdminLinks"] : (function () { throw new RuntimeError('Variable "displayAdminLinks" does not exist.', 294, $this->source); })())) {
                // line 295
                echo "    <div class=\"tableActionBar\">
        ";
                // line 296
                if ((isset($context["isTheme"]) || array_key_exists("isTheme", $context) ? $context["isTheme"] : (function () { throw new RuntimeError('Variable "isTheme" does not exist.', 296, $this->source); })())) {
                    // line 297
                    echo "            <a href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "overview", "sort" => "", "show" => "themes"]]), "html", null, true);
                    echo "\"><span class=\"icon-add\"></span> ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_InstallNewThemes"]), "html", null, true);
                    echo "</a>
        ";
                } else {
                    // line 299
                    echo "            <a href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "overview", "sort" => ""]]), "html", null, true);
                    echo "\"><span class=\"icon-add\"></span> ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_InstallNewPlugins"]), "html", null, true);
                    echo "</a>
        ";
                }
                // line 301
                echo "    </div>
    ";
            }
            // line 303
            echo "
    <div class=\"footer-message\">
        ";
            // line 305
            ob_start();
            // line 306
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pluginsInfo"]) || array_key_exists("pluginsInfo", $context) ? $context["pluginsInfo"] : (function () { throw new RuntimeError('Variable "pluginsInfo" does not exist.', 306, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["name"] => $context["plugin"]) {
                // line 307
                echo "                ";
                if ((twig_get_attribute($this->env, $this->source, $context["plugin"], "alwaysActivated", [], "any", true, true, false, 307) && twig_get_attribute($this->env, $this->source, $context["plugin"], "alwaysActivated", [], "any", false, false, false, 307))) {
                    // line 308
                    echo "                    ";
                    echo \Piwik\piwik_escape_filter($this->env, $context["name"], "html", null, true);
                    if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 308)) {
                        echo ", ";
                    }
                    // line 309
                    echo "                 ";
                }
                // line 310
                echo "            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 311
            echo "        ";
            $context["pluginsAlwaysActivated"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 312
            echo "        ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_AlwaysActivatedPluginsList", (isset($context["pluginsAlwaysActivated"]) || array_key_exists("pluginsAlwaysActivated", $context) ? $context["pluginsAlwaysActivated"] : (function () { throw new RuntimeError('Variable "pluginsAlwaysActivated" does not exist.', 312, $this->source); })())]), "html", null, true);
            echo "
    </div>

";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@CorePluginsAdmin/macros.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  965 => 312,  962 => 311,  948 => 310,  945 => 309,  939 => 308,  936 => 307,  918 => 306,  916 => 305,  912 => 303,  908 => 301,  900 => 299,  892 => 297,  890 => 296,  887 => 295,  885 => 294,  880 => 291,  874 => 290,  870 => 288,  866 => 286,  859 => 282,  855 => 280,  853 => 279,  851 => 277,  844 => 276,  842 => 275,  838 => 273,  834 => 270,  822 => 269,  817 => 268,  811 => 266,  809 => 265,  807 => 263,  801 => 262,  798 => 261,  793 => 258,  790 => 246,  787 => 257,  773 => 256,  769 => 254,  766 => 253,  760 => 251,  750 => 249,  747 => 248,  729 => 247,  727 => 246,  725 => 245,  721 => 243,  718 => 242,  714 => 240,  702 => 239,  699 => 238,  697 => 237,  694 => 236,  687 => 232,  684 => 231,  676 => 228,  672 => 226,  669 => 225,  663 => 223,  660 => 222,  652 => 220,  650 => 219,  645 => 217,  641 => 216,  635 => 215,  627 => 214,  624 => 213,  622 => 212,  619 => 211,  611 => 208,  608 => 207,  606 => 201,  601 => 199,  596 => 196,  589 => 193,  587 => 192,  582 => 189,  573 => 187,  570 => 186,  568 => 185,  551 => 183,  545 => 181,  541 => 179,  537 => 178,  534 => 177,  532 => 176,  528 => 175,  504 => 173,  501 => 172,  498 => 171,  494 => 170,  489 => 167,  483 => 165,  481 => 164,  477 => 163,  473 => 162,  465 => 161,  455 => 154,  451 => 153,  447 => 152,  442 => 149,  422 => 148,  408 => 142,  404 => 141,  400 => 140,  396 => 139,  389 => 135,  385 => 134,  381 => 133,  377 => 132,  373 => 131,  368 => 128,  356 => 127,  340 => 123,  336 => 121,  334 => 120,  326 => 119,  324 => 118,  307 => 117,  279 => 97,  256 => 76,  248 => 73,  244 => 71,  236 => 69,  234 => 68,  227 => 67,  225 => 66,  221 => 65,  212 => 64,  210 => 63,  206 => 61,  200 => 59,  194 => 57,  192 => 56,  186 => 53,  182 => 52,  178 => 50,  170 => 48,  163 => 46,  155 => 44,  153 => 43,  146 => 39,  142 => 38,  129 => 32,  117 => 28,  113 => 27,  106 => 23,  102 => 22,  98 => 21,  94 => 20,  90 => 19,  74 => 6,  70 => 4,  67 => 3,  52 => 2,  47 => 147,  44 => 126,  41 => 116,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{% macro tablePluginUpdates(pluginsHavingUpdate, updateNonce, isMultiServerEnvironment) %}
    {% import '@Marketplace/macros.twig' as marketplaceMacro %}

    <div>
        <a id=\"update-selected-plugins\" href=\"javascript:\" class=\"btn disabled\">{{ 'CorePluginsAdmin_UpdateSelected'|translate }}</a>
    </div>
    <table piwik-content-table>
        <thead>
        <tr>
            <th>
                <span class=\"checkbox-container\">
                    <label>
                        <input type=\"checkbox\" id=\"select-plugin-all\"/>
                        <span></span>
                    </label>
                </span>
            </th>
            <th>{{ 'General_Plugin'|translate }}</th>
            <th class=\"num\">{{ 'CorePluginsAdmin_Version'|translate }}</th>
            <th>{{ 'General_Description'|translate }}</th>
            <th class=\"status\">{{ 'CorePluginsAdmin_Status'|translate }}</th>
            <th class=\"action-links\">{{ 'General_Action'|translate }}</th>
        </tr>
        </thead>
        <tbody id=\"plugins\">
        {% for name,plugin in pluginsHavingUpdate %}
            <tr {% if plugin.isActivated|default(false) %}class=\"active-plugin\"{% else %}class=\"inactive-plugin\"{% endif %}>
                <td class=\"select-cell\">
                    <span class=\"checkbox-container\">
                        <label>
                            <input type=\"checkbox\" id=\"select-plugin-{{ plugin.name|e('html_attr') }}\" {% if plugin.isDownloadable is defined and not plugin.isDownloadable %}disabled=\"disabled\"{% endif %} />
                            <span></span>
                        </label>
                    </span>
                </td>
                <td class=\"name\">
                    <a href=\"javascript:void(0);\" piwik-plugin-name=\"{{ plugin.name|e('html_attr') }}\" class=\"plugin-details\">
                        {{ plugin.name }}
                    </a>
                </td>
                <td class=\"vers\">
                    {% if plugin.changelog is defined and plugin.changelog and plugin.changelog.url is defined and plugin.changelog.url %}
                        <a href=\"{{ plugin.changelog.url|e('html_attr') }}\" title=\"{{ 'CorePluginsAdmin_Changelog'|translate }}\"
                           target=\"_blank\" rel=\"noreferrer noopener\"
                        >{{ plugin.currentVersion }} => {{ plugin.latestVersion }}</a>
                    {% else %}
                        {{ plugin.currentVersion }} => {{ plugin.latestVersion }}
                    {% endif %}
                </td>
                <td class=\"desc\">
                    {{ plugin.description }}
                    {{ marketplaceMacro.missingRequirementsPleaseUpdateNotice(plugin) }}
                </td>
                <td class=\"status\">
                    {% if plugin.isActivated %}
                        {{ 'CorePluginsAdmin_Active'|translate }}
                    {% else %}
                        {{ 'CorePluginsAdmin_Inactive'|translate }}
                    {% endif %}
                </td>
                <td class=\"togl action-links\">
                    {% if plugin.isDownloadable is defined and not plugin.isDownloadable %}
                        <span title=\"{{ 'CorePluginsAdmin_PluginNotDownloadable'|translate|e('html_attr') }} {% if plugin.isPaid %}{{ 'CorePluginsAdmin_PluginNotDownloadablePaidReason'|translate|e('html_attr') }}{% endif %}\"
                          >{{ 'CorePluginsAdmin_NotDownloadable'|translate|e('html_attr') }}</span>
                    {% elseif isMultiServerEnvironment %}
                        <a onclick=\"\$(this).css('display', 'none')\" href=\"{{ linkTo({'action':'download', 'module': 'Marketplace', 'pluginName': plugin.name, 'nonce': (plugin.name|nonce)}) }}\">{{ 'General_Download'|translate }}</a>
                    {% elseif 0 == plugin.missingRequirements|length %}
                        <a href=\"{{ linkTo({'action':'updatePlugin', 'module': 'Marketplace', 'pluginName': plugin.name, 'nonce': updateNonce}) }}\">{{ 'CoreUpdater_UpdateTitle'|translate }}</a>
                    {% else %}
                        -
                    {% endif %}
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <script>
        \$(function () {
            \$('span.checkbox-container').on('change', function() {
                var isAtLeastOneChecked = \$('span.checkbox-container input:checked').length >= 1;
                isAtLeastOneChecked ? \$('#update-selected-plugins').removeClass('disabled') : \$('#update-selected-plugins').addClass('disabled');
            });

            \$('#update-selected-plugins').on('click', function () {
                var pluginsToUpdate = [];
                \$('tbody#plugins td.select-cell input').each(function () {
                    if (!this.checked) {
                        return;
                    }

                    var pluginName = \$(this).closest('tr').find('.name .plugin-details').attr('piwik-plugin-name');
                    pluginsToUpdate.push(pluginName);
                });

                var url = '{{ linkTo({'module':'Marketplace','action':'updatePlugin','nonce':updateNonce})|raw }}&pluginName=' + encodeURIComponent(pluginsToUpdate.join(','));
                window.location.href = url;

                \$(this).prop('disabled', true);
            });

            \$('#select-plugin-all').on('change', function () {
                var self = this;
                \$('tbody#plugins td.select-cell input[type=checkbox]').each(function () {
                    if (\$(this).prop('disabled')) {
                        return;
                    }
                    \$(this).prop('checked', self.checked);
                });
            });
        });
    </script>

{% endmacro %}

{% macro pluginActivateDeactivateAction(name, isActivated, missingRequirements, deactivateNonce, activateNonce) -%}
    {%- if isActivated -%}
        <a href='index.php?module=CorePluginsAdmin&action=deactivate&pluginName={{ name }}&nonce={{ deactivateNonce }}&redirectTo=referrer'>{{ 'CorePluginsAdmin_Deactivate'|translate }}</a>
    {%- elseif missingRequirements %}
        -
    {% else -%}
        <a href='index.php?module=CorePluginsAdmin&action=activate&pluginName={{ name }}&nonce={{  activateNonce }}&redirectTo=referrer'>{{ 'CorePluginsAdmin_Activate'|translate }}</a>
    {%- endif -%}
{%- endmacro %}

{% macro pluginsFilter() %}

    <p class=\"row pluginsFilter\" piwik-plugin-filter>
        <span class=\"origin\">
            <strong>{{ 'CorePluginsAdmin_Origin'|translate }}</strong>
            <a data-filter-origin=\"all\" href=\"#\" class=\"active\">{{ 'General_All'|translate }}<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"core\" href=\"#\">{{ 'CorePluginsAdmin_OriginCore'|translate }}<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"official\" href=\"#\">{{ 'CorePluginsAdmin_OriginOfficial'|translate }}<span class=\"counter\"></span></a> |
            <a data-filter-origin=\"thirdparty\" href=\"#\">{{ 'CorePluginsAdmin_OriginThirdParty'|translate }}<span class=\"counter\"></span></a>
        </span>

        <span class=\"status\">
            <strong>{{ 'CorePluginsAdmin_Status'|translate }}</strong>
            <a data-filter-status=\"all\" href=\"#\" class=\"active\">{{ 'General_All'|translate }}<span class=\"counter\"></span></a> |
            <a data-filter-status=\"active\" href=\"#\">{{ 'CorePluginsAdmin_Active'|translate }}<span class=\"counter\"></span></a> |
            <a data-filter-status=\"inactive\" href=\"#\">{{ 'CorePluginsAdmin_Inactive'|translate }}<span class=\"counter\"></span></a>
        </span>
    </p>

{% endmacro %}

{% macro tablePlugins(pluginsInfo, pluginNamesHavingSettings, activateNonce, deactivateNonce, uninstallNonce, isTheme, marketplacePluginNames, displayAdminLinks) %}

    <div id=\"confirmUninstallPlugin\" class=\"ui-confirm\">

        <h2 id=\"uninstallPluginConfirm\">{{ 'CorePluginsAdmin_UninstallConfirm'|translate }}</h2>
        <input role=\"yes\" type=\"button\" value=\"{{ 'General_Yes'|translate }}\"/>
        <input role=\"no\" type=\"button\" value=\"{{ 'General_No'|translate }}\"/>

    </div>

    <table piwik-content-table>
        <thead>
        <tr>
            <th>{% if isTheme %}{{ 'CorePluginsAdmin_Theme'|translate }}{% else %}{{ 'General_Plugin'|translate }}{% endif %}</th>
            <th>{{ 'General_Description'|translate }}</th>
            <th class=\"status\">{{ 'CorePluginsAdmin_Status'|translate }}</th>
            {% if (displayAdminLinks) %}
                <th class=\"action-links\">{{ 'General_Action'|translate }}</th>
            {% endif %}
        </tr>
        </thead>
        <tbody id=\"plugins\">
        {% for name,plugin in pluginsInfo %}
            {% set isDefaultTheme = isTheme and name == 'Morpheus' %}
            {% if (plugin.alwaysActivated is defined and not plugin.alwaysActivated) or isTheme %}
                <tr {% if plugin.activated %}class=\"active-plugin\"{% else %}class=\"inactive-plugin\"{% endif %} data-filter-status=\"{% if plugin.activated %}active{% else %}inactive{% endif %}\" data-filter-origin=\"{% if plugin.isCorePlugin %}core{% elseif plugin.isOfficialPlugin %}official{% else %}thirdparty{% endif %}\">
                    <td class=\"name\">
                        <a name=\"{{ name|e('html_attr') }}\"></a>
                        {% if not plugin.isCorePlugin and name in marketplacePluginNames -%}
                            <a href=\"javascript:void(0);\"
                               piwik-plugin-name=\"{{ name|e('html_attr') }}\"
                            >{{ name }}</a>
                        {%- else %}
                            {{ name }}
                        {% endif %}
                        <span class=\"plugin-version\" {% if plugin.isCorePlugin %}title=\"{{ 'CorePluginsAdmin_CorePluginTooltip'|translate }}\"{% endif %}>({% if plugin.isCorePlugin %}{{ 'CorePluginsAdmin_OriginCore'|translate }}{% else %}v{{ plugin.info.version }}{% endif %})</span>

                        {% if name in pluginNamesHavingSettings %}
                            <br /><br />
                            <a href=\"{{ linkTo({'module':'CoreAdminHome', 'action': 'generalSettings'}) }}#{{ name|e('html_attr') }}\" class=\"settingsLink\">{{ 'General_Settings'|translate }}</a>
                        {% endif %}
                    </td>
                    <td class=\"desc\">
                        <div class=\"plugin-desc-missingrequirements\">
                            {% if plugin.missingRequirements is defined and plugin.missingRequirements %}
                                {{  plugin.missingRequirements }}
                                <br />
                            {% endif %}
                        </div>
                        <div class=\"plugin-desc-text\">

                            {{ plugin.info.description|nl2br }}

                            {% if plugin.info.homepage|default is not empty and plugin.info.homepage not in [
                            'http://piwik.org', 'http://www.piwik.org', 'http://piwik.org/', 'http://www.piwik.org/',
                            'https://piwik.org', 'https://www.piwik.org', 'https://piwik.org/', 'https://www.piwik.org/',
                                'http://matomo.org', 'http://www.matomo.org', 'http://matomo.org/', 'http://www.matomo.org/',
                                'https://matomo.org', 'https://www.matomo.org', 'https://matomo.org/', 'https://www.matomo.org/'
                            ] %}
                                <span class=\"plugin-homepage\">
                            <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"{{ plugin.info.homepage|e('html_attr') }}\">({{ 'CorePluginsAdmin_PluginHomepage'|translate|replace({' ': '&nbsp;'})|raw }})</a>
                        </span>
                            {% endif %}

                            {% if plugin.info.donate is defined and plugin.info.donate|length %}
                                <div class=\"plugin-donation\">
                                    {{ 'CorePluginsAdmin_LikeThisPlugin'|translate }} <a href=\"javascript:;\" class=\"plugin-donation-link\" data-overlay-id=\"overlay-{{ name|escape('html_attr') }}\">{{ 'CorePluginsAdmin_ConsiderDonating'|translate }}</a>
                                    <div id=\"overlay-{{ name|escape('html_attr') }}\" class=\"donation-overlay ui-confirm\" title=\"{{ 'CorePluginsAdmin_LikeThisPlugin'|translate }}\">
                                        <p>{{ 'CorePluginsAdmin_CommunityContributedPlugin'|translate }}</p>
                                        <p>{{ 'CorePluginsAdmin_ConsiderDonatingCreatorOf'|translate(\"<b>\" ~ name ~ \"</b>\")|raw }}</p>
                                        <div class=\"donation-links\">
                                            {% if plugin.info.donate.paypal is defined and plugin.info.donate.paypal %}
                                                <a class=\"donation-link paypal\" target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_donations&item_name=Matomo%20Plugin%20{{ name|escape('url') }}&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted&business={{ plugin.info.donate.paypal|escape('url') }}\"><img src=\"plugins/CorePluginsAdmin/images/paypal_donate.png\" height=\"30\"/></a>
                                            {% endif %}
                                            {% if plugin.info.donate.flattr is defined and plugin.info.donate.flattr %}
                                                <a class=\"donation-link flattr\" target=\"_blank\" rel=\"noreferrer noopener\" href=\"{{ plugin.info.donate.flattr }}\"><img class=\"alignnone\" title=\"Flattr\" alt=\"\" src=\"plugins/CorePluginsAdmin/images/flattr.png\" height=\"29\" /></a>
                                            {% endif %}
                                            {% if plugin.info.donate.bitcoin is defined and plugin.info.donate.bitcoin %}
                                                <div class=\"donation-link bitcoin\">
                                                    <span>Donate Bitcoins to:</span>
                                                    <a href=\"bitcoin:{{ plugin.info.donate.bitcoin|escape('url') }}\">{{ plugin.info.donate.bitcoin }}</a>
                                                </div>
                                            {% endif %}
                                        </div>
                                        <input role=\"no\" type=\"button\" value=\"{{ 'General_Close'|translate }}\"/>
                                    </div>
                                </div>
                            {% endif %}
                        </div>
                        {% if plugin.info.license is defined %}
                            <div class=\"plugin-license\">
                                {% if plugin.info.license_file is defined %}<a title=\"{{ 'CorePluginsAdmin_LicenseHomepage'|translate }}\" rel=\"noreferrer noopener\" target=\"_blank\" href=\"index.php?module=CorePluginsAdmin&action=showLicense&pluginName={{ name }}\">{% endif %}{{ plugin.info.license }}{% if plugin.info.license_file is defined %}</a>{% endif %}
                            </div>
                        {% endif %}
                        {% if plugin.info.authors is defined %}
                            <div class=\"plugin-author\">
                                By
                                    {% if plugin.info.authors is defined -%}
                                    {% apply spaceless %}
                                        {% for author in plugin.info.authors|filter(author => author.name) %}
                                            {% if author.homepage is defined %}
                                                <a title=\"{{ 'CorePluginsAdmin_AuthorHomepage'|translate }}\" href=\"{{ author.homepage }}\" rel=\"noreferrer noopener\" target=\"_blank\">{{ author.name }}</a>
                                            {% else %}
                                                {{ author.name }}
                                            {% endif %}
                                            {% if loop.index < plugin.info.authors|length %}
                                                ,
                                            {% endif %}
                                        {% endfor %}
                                    {% endapply %}
                                    {%- endif %}.
                            </div>
                        {% endif %}
                    </td>
                    <td class=\"status\" {% if isDefaultTheme %}style=\"border-left-width:0px;\"{% endif %}>
                        {% if not isDefaultTheme -%}

                            {% if plugin.activated %}
                                {{ 'CorePluginsAdmin_Active'|translate }}
                            {% else %}
                                {{ 'CorePluginsAdmin_Inactive'|translate }}
                                {% if plugin.uninstallable and displayAdminLinks %} <br/> - <a data-plugin-name=\"{{ name|escape('html_attr') }}\" class=\"uninstall\" href='index.php?module=CorePluginsAdmin&action=uninstall&pluginName={{ name }}&nonce={{ uninstallNonce }}'>{{ 'CorePluginsAdmin_ActionUninstall'|translate }}</a>{% endif %}
                            {% endif %}

                        {%- endif %}
                    </td>

                    {% if displayAdminLinks %}
                        <td class=\"togl action-links\" {% if isDefaultTheme %}style=\"border-left-width:0px;\"{% endif %}>
                            {% if not isDefaultTheme -%}

                                {% if plugin.invalid is defined or plugin.alwaysActivated %}
                                    -
                                {% else %}
                                    {{ _self.pluginActivateDeactivateAction(name, plugin.activated, plugin.missingRequirements, deactivateNonce, activateNonce) }}
                                {% endif %}

                            {%- endif %}
                        </td>
                    {% endif %}
                </tr>
            {% endif %}
        {% endfor %}
        </tbody>
    </table>

    {% if displayAdminLinks %}
    <div class=\"tableActionBar\">
        {% if isTheme %}
            <a href=\"{{ linkTo({'module': 'Marketplace','action':'overview', 'sort': '', 'show': 'themes'}) }}\"><span class=\"icon-add\"></span> {{ 'CorePluginsAdmin_InstallNewThemes'|translate }}</a>
        {% else %}
            <a href=\"{{ linkTo({'module': 'Marketplace','action':'overview', 'sort': ''}) }}\"><span class=\"icon-add\"></span> {{ 'CorePluginsAdmin_InstallNewPlugins'|translate }}</a>
        {% endif %}
    </div>
    {% endif %}

    <div class=\"footer-message\">
        {% set pluginsAlwaysActivated %}
            {% for name,plugin in pluginsInfo %}
                {% if plugin.alwaysActivated is defined and plugin.alwaysActivated %}
                    {{ name }}{% if not loop.last %}, {% endif %}
                 {% endif %}
            {% endfor %}
        {% endset %}
        {{ 'CorePluginsAdmin_AlwaysActivatedPluginsList'|translate(pluginsAlwaysActivated) }}
    </div>

{% endmacro %}
", "@CorePluginsAdmin/macros.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/CorePluginsAdmin/templates/macros.twig");
    }
}
