<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Marketplace/licenseform.twig */
class __TwigTemplate_96722e898ab6dc0af17ed4f76a092743760a3b61e013d3bae2eb90df6811daeb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        ob_start();
        // line 2
        echo "        <div piwik-field uicontrol=\"text\" name=\"license_key\"
             class=\"valign licenseKeyText\"
             full-width=\"true\"
             ng-model=\"licenseController.licenseKey\"
             ng-change=\"licenseController.updatedLicenseKey()\"
             placeholder=\"";
        // line 7
        if ((isset($context["isValidConsumer"]) || array_key_exists("isValidConsumer", $context) ? $context["isValidConsumer"] : (function () { throw new RuntimeError('Variable "isValidConsumer" does not exist.', 7, $this->source); })())) {
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_LicenseKeyIsValidShort"]), "html", null, true);
        } else {
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_LicenseKey"]), "html_attr");
        }
        echo "\">
        </div>
        <div piwik-save-button
             class=\"valign\"
             onconfirm=\"licenseController.updateLicense()\"
             data-disabled=\"!licenseController.enableUpdate\"
             value=\"";
        // line 13
        if ((isset($context["hasLicenseKey"]) || array_key_exists("hasLicenseKey", $context) ? $context["hasLicenseKey"] : (function () { throw new RuntimeError('Variable "hasLicenseKey" does not exist.', 13, $this->source); })())) {
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreUpdater_UpdateTitle"]), "html_attr");
        } else {
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_ActivateLicenseKey"]), "html_attr");
        }
        echo "\"
             id=\"submit_license_key\"></div>
";
        $context["defaultLicenseKeyFields"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 16
        echo "
<div class=\"marketplace-max-width\" ng-controller=\"PiwikMarketplaceLicenseController as licenseController\">
    <div class=\"marketplace-paid-intro\">
    ";
        // line 19
        if ((isset($context["isValidConsumer"]) || array_key_exists("isValidConsumer", $context) ? $context["isValidConsumer"] : (function () { throw new RuntimeError('Variable "isValidConsumer" does not exist.', 19, $this->source); })())) {
            // line 20
            echo "        ";
            if ((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 20, $this->source); })())) {
                // line 21
                echo "            ";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PaidPluginsWithLicenseKeyIntro", ""]);
                echo "
            <br/>

            <div class=\"licenseToolbar valign-wrapper\">
                ";
                // line 25
                echo (isset($context["defaultLicenseKeyFields"]) || array_key_exists("defaultLicenseKeyFields", $context) ? $context["defaultLicenseKeyFields"] : (function () { throw new RuntimeError('Variable "defaultLicenseKeyFields" does not exist.', 25, $this->source); })());
                echo "

                <div piwik-save-button
                     class=\"valign\"
                     id=\"remove_license_key\"
                     onconfirm=\"licenseController.removeLicense()\"
                     value=\"";
                // line 31
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_RemoveLicenseKey"]), "html_attr");
                echo "\"
                ></div>

                <a href=\"";
                // line 34
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["action" => "subscriptionOverview"]]), "html", null, true);
                echo "\" class=\"btn valign\">
                    ";
                // line 35
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_ViewSubscriptions"]), "html", null, true);
                echo "
                </a>

                ";
                // line 38
                if ((((isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 38, $this->source); })()) && (isset($context["isPluginsAdminEnabled"]) || array_key_exists("isPluginsAdminEnabled", $context) ? $context["isPluginsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isPluginsAdminEnabled" does not exist.', 38, $this->source); })())) && twig_length_filter($this->env, (isset($context["paidPluginsToInstallAtOnce"]) || array_key_exists("paidPluginsToInstallAtOnce", $context) ? $context["paidPluginsToInstallAtOnce"] : (function () { throw new RuntimeError('Variable "paidPluginsToInstallAtOnce" does not exist.', 38, $this->source); })())))) {
                    // line 39
                    echo "                    <a href=\"javascript:;\" class=\"btn installAllPaidPlugins valign\">
                        ";
                    // line 40
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_InstallPurchasedPlugins"]), "html", null, true);
                    echo "
                    </a>
                    ";
                    // line 42
                    $this->loadTemplate("@Marketplace/paid-plugins-install-list.twig", "@Marketplace/licenseform.twig", 42)->display($context);
                    // line 43
                    echo "                ";
                }
                // line 44
                echo "
            </div>

            <div piwik-activity-indicator loading=\"licenseController.isUpdating\"></div>
        ";
            }
            // line 49
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            if ((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 51, $this->source); })())) {
                // line 52
                echo "            ";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PaidPluginsNoLicenseKeyIntro", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/recommends/premium-plugins/'>", "</a>"]);
                echo "

            <br/>

            <div class=\"licenseToolbar valign-wrapper\">
                ";
                // line 57
                echo (isset($context["defaultLicenseKeyFields"]) || array_key_exists("defaultLicenseKeyFields", $context) ? $context["defaultLicenseKeyFields"] : (function () { throw new RuntimeError('Variable "defaultLicenseKeyFields" does not exist.', 57, $this->source); })());
                echo "
            </div>

            <div piwik-activity-indicator loading=\"licenseController.isUpdating\"></div>

        ";
            } else {
                // line 63
                echo "            ";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PaidPluginsNoLicenseKeyIntroNoSuperUserAccess", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/recommends/premium-plugins/'>", "</a>"]);
                echo "
        ";
            }
            // line 65
            echo "
    ";
        }
        // line 67
        echo "    </div>
</div>


<div class=\"ui-confirm\" id=\"confirmRemoveLicense\">
    <h2>";
        // line 72
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_ConfirmRemoveLicense"]), "html", null, true);
        echo "</h2>
    <input role=\"yes\" type=\"button\" value=\"";
        // line 73
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
        echo "\"/>
    <input role=\"no\" type=\"button\" value=\"";
        // line 74
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
        echo "\"/>
</div>
";
    }

    public function getTemplateName()
    {
        return "@Marketplace/licenseform.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 74,  180 => 73,  176 => 72,  169 => 67,  165 => 65,  159 => 63,  150 => 57,  141 => 52,  138 => 51,  134 => 49,  127 => 44,  124 => 43,  122 => 42,  117 => 40,  114 => 39,  112 => 38,  106 => 35,  102 => 34,  96 => 31,  87 => 25,  79 => 21,  76 => 20,  74 => 19,  69 => 16,  59 => 13,  46 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set defaultLicenseKeyFields %}
        <div piwik-field uicontrol=\"text\" name=\"license_key\"
             class=\"valign licenseKeyText\"
             full-width=\"true\"
             ng-model=\"licenseController.licenseKey\"
             ng-change=\"licenseController.updatedLicenseKey()\"
             placeholder=\"{% if isValidConsumer %}{{ 'Marketplace_LicenseKeyIsValidShort'|translate }}{% else %}{{ 'Marketplace_LicenseKey'|translate|e('html_attr') }}{% endif %}\">
        </div>
        <div piwik-save-button
             class=\"valign\"
             onconfirm=\"licenseController.updateLicense()\"
             data-disabled=\"!licenseController.enableUpdate\"
             value=\"{% if hasLicenseKey %}{{ 'CoreUpdater_UpdateTitle'|translate|e('html_attr') }}{% else %}{{ 'Marketplace_ActivateLicenseKey'|translate|e('html_attr') }}{% endif %}\"
             id=\"submit_license_key\"></div>
{% endset %}

<div class=\"marketplace-max-width\" ng-controller=\"PiwikMarketplaceLicenseController as licenseController\">
    <div class=\"marketplace-paid-intro\">
    {% if isValidConsumer %}
        {% if isSuperUser %}
            {{ 'Marketplace_PaidPluginsWithLicenseKeyIntro'|translate('')|raw }}
            <br/>

            <div class=\"licenseToolbar valign-wrapper\">
                {{ defaultLicenseKeyFields|raw }}

                <div piwik-save-button
                     class=\"valign\"
                     id=\"remove_license_key\"
                     onconfirm=\"licenseController.removeLicense()\"
                     value=\"{{ 'Marketplace_RemoveLicenseKey'|translate|e('html_attr') }}\"
                ></div>

                <a href=\"{{ linkTo({'action': 'subscriptionOverview'}) }}\" class=\"btn valign\">
                    {{ 'Marketplace_ViewSubscriptions'|translate }}
                </a>

                {% if isAutoUpdatePossible and isPluginsAdminEnabled and paidPluginsToInstallAtOnce|length %}
                    <a href=\"javascript:;\" class=\"btn installAllPaidPlugins valign\">
                        {{ 'Marketplace_InstallPurchasedPlugins'|translate }}
                    </a>
                    {% include '@Marketplace/paid-plugins-install-list.twig' %}
                {% endif %}

            </div>

            <div piwik-activity-indicator loading=\"licenseController.isUpdating\"></div>
        {% endif %}

    {% else %}
        {% if isSuperUser %}
            {{ 'Marketplace_PaidPluginsNoLicenseKeyIntro'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/recommends/premium-plugins/'>\", \"</a>\")|raw }}

            <br/>

            <div class=\"licenseToolbar valign-wrapper\">
                {{ defaultLicenseKeyFields|raw }}
            </div>

            <div piwik-activity-indicator loading=\"licenseController.isUpdating\"></div>

        {% else %}
            {{ 'Marketplace_PaidPluginsNoLicenseKeyIntroNoSuperUserAccess'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/recommends/premium-plugins/'>\", \"</a>\")|raw }}
        {% endif %}

    {% endif %}
    </div>
</div>


<div class=\"ui-confirm\" id=\"confirmRemoveLicense\">
    <h2>{{ 'Marketplace_ConfirmRemoveLicense'|translate }}</h2>
    <input role=\"yes\" type=\"button\" value=\"{{ 'General_Yes'|translate }}\"/>
    <input role=\"no\" type=\"button\" value=\"{{ 'General_No'|translate }}\"/>
</div>
", "@Marketplace/licenseform.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/licenseform.twig");
    }
}
