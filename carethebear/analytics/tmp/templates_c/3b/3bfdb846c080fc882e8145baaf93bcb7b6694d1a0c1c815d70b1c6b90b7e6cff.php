<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Installation/getSystemCheckWidget.twig */
class __TwigTemplate_2fc7340b03ac969d4abaf47f3bed4c4cac4683c0229dc8f08366e9ea9490bf45 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widgetBody system-check\">
    ";
        // line 2
        if (( !(isset($context["numErrors"]) || array_key_exists("numErrors", $context) ? $context["numErrors"] : (function () { throw new RuntimeError('Variable "numErrors" does not exist.', 2, $this->source); })()) &&  !(isset($context["numWarnings"]) || array_key_exists("numWarnings", $context) ? $context["numWarnings"] : (function () { throw new RuntimeError('Variable "numWarnings" does not exist.', 2, $this->source); })()))) {
            // line 3
            echo "        <p class=\"system-success\"><span class=\"icon-ok\"></span> ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Installation_SystemCheckNoErrorsOrWarnings"]), "html", null, true);
            echo "</p>
    ";
        }
        // line 5
        echo "
    ";
        // line 6
        if ((isset($context["numErrors"]) || array_key_exists("numErrors", $context) ? $context["numErrors"] : (function () { throw new RuntimeError('Variable "numErrors" does not exist.', 6, $this->source); })())) {
            // line 7
            echo "        <ul>
            ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) || array_key_exists("errors", $context) ? $context["errors"] : (function () { throw new RuntimeError('Variable "errors" does not exist.', 8, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 9
                echo "                <li title=\"";
                echo \Piwik\piwik_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, $context["error"], "getLongErrorMessage", [], "any", true, true, false, 9)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["error"], "getLongErrorMessage", [], "any", false, false, false, 9), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["error"], "getItems", [], "method", false, false, false, 9), 0, [], "array", false, false, false, 9), "getComment", [], "method", false, false, false, 9))) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["error"], "getItems", [], "method", false, false, false, 9), 0, [], "array", false, false, false, 9), "getComment", [], "method", false, false, false, 9))), "html_attr");
                echo "\" class=\"system-check-widget-error\"><span class=\"icon-error\"></span> ";
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["error"], "getLabel", [], "any", false, false, false, 9), "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "        </ul>
    ";
        }
        // line 13
        echo "
    ";
        // line 14
        if ((isset($context["numWarnings"]) || array_key_exists("numWarnings", $context) ? $context["numWarnings"] : (function () { throw new RuntimeError('Variable "numWarnings" does not exist.', 14, $this->source); })())) {
            // line 15
            echo "        <ul>
            ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["warnings"]) || array_key_exists("warnings", $context) ? $context["warnings"] : (function () { throw new RuntimeError('Variable "warnings" does not exist.', 16, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["warning"]) {
                // line 17
                echo "                <li title=\"";
                echo \Piwik\piwik_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, $context["warning"], "getLongErrorMessage", [], "any", true, true, false, 17)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["warning"], "getLongErrorMessage", [], "any", false, false, false, 17), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["warning"], "getItems", [], "method", false, false, false, 17), 0, [], "array", false, false, false, 17), "getComment", [], "method", false, false, false, 17))) : (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["warning"], "getItems", [], "method", false, false, false, 17), 0, [], "array", false, false, false, 17), "getComment", [], "method", false, false, false, 17))), "html_attr");
                echo "\" class=\"system-check-widget-warning\"><span class=\"icon-warning\"></span> ";
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["warning"], "getLabel", [], "any", false, false, false, 17), "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['warning'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "        </ul>
    ";
        }
        // line 21
        echo "
    ";
        // line 22
        if (((isset($context["numErrors"]) || array_key_exists("numErrors", $context) ? $context["numErrors"] : (function () { throw new RuntimeError('Variable "numErrors" does not exist.', 22, $this->source); })()) || (isset($context["numWarnings"]) || array_key_exists("numWarnings", $context) ? $context["numWarnings"] : (function () { throw new RuntimeError('Variable "numWarnings" does not exist.', 22, $this->source); })()))) {
            // line 23
            echo "        <p>
            <br />
            <a href=\"";
            // line 25
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Installation", "action" => "systemCheckPage"]]), "html", null, true);
            echo "\"
            >";
            // line 26
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Installation_SystemCheckViewFullSystemCheck"]), "html", null, true);
            echo "</a>
        </p>
    ";
        }
        // line 29
        echo "
    <script>
        jQuery(function () {
            \$('.widgetBody.system-check').tooltip({
                track: true,
                content: function() {
                    var \$this = \$(this);
                    if (\$this.attr('piwik-field') === '') {
                        // do not show it for form fields
                        return '';
                    }

                    var title = \$(this).attr('title');
                    return piwikHelper.escape(title.replace(/\\n/g, '<br />'));
                },
                show: {delay: 200, duration: 100},
                hide: false
            });
        });
    </script>
</div>";
    }

    public function getTemplateName()
    {
        return "@Installation/getSystemCheckWidget.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 29,  115 => 26,  111 => 25,  107 => 23,  105 => 22,  102 => 21,  98 => 19,  87 => 17,  83 => 16,  80 => 15,  78 => 14,  75 => 13,  71 => 11,  60 => 9,  56 => 8,  53 => 7,  51 => 6,  48 => 5,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"widgetBody system-check\">
    {% if not numErrors and not numWarnings %}
        <p class=\"system-success\"><span class=\"icon-ok\"></span> {{ 'Installation_SystemCheckNoErrorsOrWarnings'|translate }}</p>
    {% endif %}

    {% if numErrors %}
        <ul>
            {% for error in errors %}
                <li title=\"{{ error.getLongErrorMessage|default(error.getItems()[0].getComment())|e('html_attr') }}\" class=\"system-check-widget-error\"><span class=\"icon-error\"></span> {{ error.getLabel }}</li>
            {% endfor %}
        </ul>
    {% endif %}

    {% if numWarnings %}
        <ul>
            {% for warning in warnings %}
                <li title=\"{{ warning.getLongErrorMessage|default(warning.getItems()[0].getComment())|e('html_attr') }}\" class=\"system-check-widget-warning\"><span class=\"icon-warning\"></span> {{ warning.getLabel }}</li>
            {% endfor %}
        </ul>
    {% endif %}

    {% if numErrors or numWarnings %}
        <p>
            <br />
            <a href=\"{{ linkTo({'module': 'Installation', 'action': 'systemCheckPage'}) }}\"
            >{{ 'Installation_SystemCheckViewFullSystemCheck'|translate }}</a>
        </p>
    {% endif %}

    <script>
        jQuery(function () {
            \$('.widgetBody.system-check').tooltip({
                track: true,
                content: function() {
                    var \$this = \$(this);
                    if (\$this.attr('piwik-field') === '') {
                        // do not show it for form fields
                        return '';
                    }

                    var title = \$(this).attr('title');
                    return piwikHelper.escape(title.replace(/\\n/g, '<br />'));
                },
                show: {delay: 200, duration: 100},
                hide: false
            });
        });
    </script>
</div>", "@Installation/getSystemCheckWidget.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Installation/templates/getSystemCheckWidget.twig");
    }
}
