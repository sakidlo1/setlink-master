<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Feedback/referBanner.twig */
class __TwigTemplate_b378f8d1a0319cb162ac378e46504535e9cb99da5a0c6b0a81b4c753c2b3c53d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div piwik-refer-banner show-refer-banner=\"";
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["showReferBanner"]) || array_key_exists("showReferBanner", $context) ? $context["showReferBanner"] : (function () { throw new RuntimeError('Variable "showReferBanner" does not exist.', 1, $this->source); })()), "html", null, true);
        echo "\"></div>
";
    }

    public function getTemplateName()
    {
        return "@Feedback/referBanner.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div piwik-refer-banner show-refer-banner=\"{{ showReferBanner }}\"></div>
", "@Feedback/referBanner.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Feedback/templates/referBanner.twig");
    }
}
