<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Marketplace/getPremiumFeatures.twig */
class __TwigTemplate_054916707ecf5fec18d4427cec19b930ef8a17f38e974af05044892c5405d745 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"getNewPlugins getPremiumFeatures widgetBody\">
    <div class=\"row\">
        <div class=\"col s12 m12\">
            <h3 style=\"margin-bottom: 28px;\">";
        // line 4
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_SupportMatomoThankYou"]), "html", null, true);
        echo "</h3></div>

        ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["plugins"]) || array_key_exists("plugins", $context) ? $context["plugins"] : (function () { throw new RuntimeError('Variable "plugins" does not exist.', 6, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["plugin"]) {
            // line 7
            echo "            <div class=\"col s12 m4\">

                <h3 class=\"pluginName\" piwik-plugin-name=\"";
            // line 9
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 9), "html_attr");
            echo "\">";
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "displayName", [], "any", false, false, false, 9), "html", null, true);
            echo "</h3>
                <span class=\"pluginBody\">
                    ";
            // line 11
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "description", [], "any", false, false, false, 11), "html", null, true);
            echo "
                    <br />
                    <a href=\"javascript:;\" class=\"pluginMoreDetails\" piwik-plugin-name=\"";
            // line 13
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plugin"], "name", [], "any", false, false, false, 13), "html_attr");
            echo "\">";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
            echo "</a>
                </span>
            </div>
            ";
            // line 16
            if ((0 === twig_compare((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 16) % 3), 0))) {
                // line 17
                echo "                </div><div class=\"row\">
            ";
            }
            // line 19
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['plugin'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "    </div>

    <div class=\"widgetBody\">
        <a href=\"";
        // line 23
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "overview", "show" => "premium"]]), "html", null, true);
        echo "\"
            >";
        // line 24
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_ViewAllMarketplacePlugins"]), "html", null, true);
        echo "</a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "@Marketplace/getPremiumFeatures.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 24,  113 => 23,  108 => 20,  94 => 19,  90 => 17,  88 => 16,  80 => 13,  75 => 11,  68 => 9,  64 => 7,  47 => 6,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"getNewPlugins getPremiumFeatures widgetBody\">
    <div class=\"row\">
        <div class=\"col s12 m12\">
            <h3 style=\"margin-bottom: 28px;\">{{ 'Marketplace_SupportMatomoThankYou'|translate }}</h3></div>

        {% for plugin in plugins %}
            <div class=\"col s12 m4\">

                <h3 class=\"pluginName\" piwik-plugin-name=\"{{ plugin.name|e('html_attr') }}\">{{ plugin.displayName }}</h3>
                <span class=\"pluginBody\">
                    {{ plugin.description }}
                    <br />
                    <a href=\"javascript:;\" class=\"pluginMoreDetails\" piwik-plugin-name=\"{{ plugin.name|e('html_attr') }}\">{{ 'General_MoreDetails'|translate }}</a>
                </span>
            </div>
            {% if loop.index % 3 == 0 %}
                </div><div class=\"row\">
            {% endif %}
        {% endfor %}
    </div>

    <div class=\"widgetBody\">
        <a href=\"{{ linkTo({'module': 'Marketplace', 'action': 'overview', 'show': 'premium'}) }}\"
            >{{ 'CorePluginsAdmin_ViewAllMarketplacePlugins'|translate }}</a>
    </div>
</div>", "@Marketplace/getPremiumFeatures.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/getPremiumFeatures.twig");
    }
}
