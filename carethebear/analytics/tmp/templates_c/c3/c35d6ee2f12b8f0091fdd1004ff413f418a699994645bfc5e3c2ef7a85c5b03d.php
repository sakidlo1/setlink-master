<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @MobileMessaging/index.twig */
class __TwigTemplate_d3e9e6d7b83f71e3ffb6fa9220b3b11a0f70302024b29f42b54305d73183738c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["macro"] = $this->macros["macro"] = $this->loadTemplate("@MobileMessaging/macros.twig", "@MobileMessaging/index.twig", 3)->unwrap();
        // line 5
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_SettingsMenu"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@MobileMessaging/index.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<div class=\"manageMobileMessagingSettings\">
    ";
        // line 9
        if ((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 9, $this->source); })())) {
            // line 10
            echo "    <div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 10, $this->source); })()), "html_attr");
            echo "\">
        <div ng-controller=\"DelegateMobileMessagingSettingsController as delegateManagement\">
            <div piwik-field uicontrol=\"radio\" name=\"delegatedManagement\"
                 options=\"";
            // line 13
            echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["delegateManagementOptions"]) || array_key_exists("delegateManagementOptions", $context) ? $context["delegateManagementOptions"] : (function () { throw new RuntimeError('Variable "delegateManagementOptions" does not exist.', 13, $this->source); })())), "html", null, true);
            echo "\"
                 full-width=\"true\"
                 ng-model=\"delegateManagement.enabled\"
                 data-title=\"";
            // line 16
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_LetUsersManageAPICredential"]), "html_attr");
            echo "\"
                 value=\"";
            // line 17
            if ((isset($context["delegatedManagement"]) || array_key_exists("delegatedManagement", $context) ? $context["delegatedManagement"] : (function () { throw new RuntimeError('Variable "delegatedManagement" does not exist.', 17, $this->source); })())) {
                echo "1";
            } else {
                echo "0";
            }
            echo "\">
            </div>
            <div piwik-save-button onconfirm=\"delegateManagement.save()\" saving=\"delegateManagement.isLoading\"></div>
        </div>
    </div>
    ";
        }
        // line 23
        echo "
    ";
        // line 24
        if ((isset($context["accountManagedByCurrentUser"]) || array_key_exists("accountManagedByCurrentUser", $context) ? $context["accountManagedByCurrentUser"] : (function () { throw new RuntimeError('Variable "accountManagedByCurrentUser" does not exist.', 24, $this->source); })())) {
            // line 25
            echo "        <div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_SMSProvider"]), "html_attr");
            echo "\" feature=\"true\">

            ";
            // line 27
            if (((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 27, $this->source); })()) && (isset($context["delegatedManagement"]) || array_key_exists("delegatedManagement", $context) ? $context["delegatedManagement"] : (function () { throw new RuntimeError('Variable "delegatedManagement" does not exist.', 27, $this->source); })()))) {
                // line 28
                echo "                <p>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_DelegatedSmsProviderOnlyAppliesToYou"]), "html", null, true);
                echo "</p>
            ";
            }
            // line 30
            echo "
            ";
            // line 31
            echo twig_call_macro($macros["macro"], "macro_manageSmsApi", [(isset($context["credentialSupplied"]) || array_key_exists("credentialSupplied", $context) ? $context["credentialSupplied"] : (function () { throw new RuntimeError('Variable "credentialSupplied" does not exist.', 31, $this->source); })()), (isset($context["credentialError"]) || array_key_exists("credentialError", $context) ? $context["credentialError"] : (function () { throw new RuntimeError('Variable "credentialError" does not exist.', 31, $this->source); })()), (isset($context["creditLeft"]) || array_key_exists("creditLeft", $context) ? $context["creditLeft"] : (function () { throw new RuntimeError('Variable "creditLeft" does not exist.', 31, $this->source); })()), (isset($context["smsProviderOptions"]) || array_key_exists("smsProviderOptions", $context) ? $context["smsProviderOptions"] : (function () { throw new RuntimeError('Variable "smsProviderOptions" does not exist.', 31, $this->source); })()), (isset($context["smsProviders"]) || array_key_exists("smsProviders", $context) ? $context["smsProviders"] : (function () { throw new RuntimeError('Variable "smsProviders" does not exist.', 31, $this->source); })()), (isset($context["provider"]) || array_key_exists("provider", $context) ? $context["provider"] : (function () { throw new RuntimeError('Variable "provider" does not exist.', 31, $this->source); })())], 31, $context, $this->getSourceContext());
            echo "
        </div>
    ";
        }
        // line 34
        echo "
    <div piwik-content-block content-title=\"";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_PhoneNumbers"]), "html_attr");
        echo "\">
        ";
        // line 36
        if ( !(isset($context["credentialSupplied"]) || array_key_exists("credentialSupplied", $context) ? $context["credentialSupplied"] : (function () { throw new RuntimeError('Variable "credentialSupplied" does not exist.', 36, $this->source); })())) {
            // line 37
            echo "            <p>
                ";
            // line 38
            if ((isset($context["accountManagedByCurrentUser"]) || array_key_exists("accountManagedByCurrentUser", $context) ? $context["accountManagedByCurrentUser"] : (function () { throw new RuntimeError('Variable "accountManagedByCurrentUser" does not exist.', 38, $this->source); })())) {
                // line 39
                echo "                    ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_CredentialNotProvided"]), "html", null, true);
                echo "
                ";
            } else {
                // line 41
                echo "                    ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_CredentialNotProvidedByAdmin"]), "html", null, true);
                echo "
                ";
            }
            // line 43
            echo "            </p>
        ";
        } else {
            // line 45
            echo "            <div ng-controller=\"ManageMobilePhoneNumbersController as managePhoneNumber\">

                <p>";
            // line 47
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_PhoneNumbers_Help"]), "html", null, true);
            echo "</p>

                ";
            // line 49
            if ((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 49, $this->source); })())) {
                // line 50
                echo "                    <p>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_DelegatedPhoneNumbersOnlyUsedByYou"]), "html", null, true);
                echo "</p>
                ";
            }
            // line 52
            echo "
                <div class=\"row\">
                    <h3 class=\"col s12\">";
            // line 54
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_PhoneNumbers_Add"]), "html", null, true);
            echo "</h3>
                </div>

                <div class=\"form-group row\">
                    <div class=\"col s12 m6\">
                        <div piwik-field uicontrol=\"select\" name=\"countryCodeSelect\"
                             value=\"";
            // line 60
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["defaultCallingCode"]) || array_key_exists("defaultCallingCode", $context) ? $context["defaultCallingCode"] : (function () { throw new RuntimeError('Variable "defaultCallingCode" does not exist.', 60, $this->source); })()), "html", null, true);
            echo "\"
                             ng-model=\"managePhoneNumber.countryCallingCode\"
                             full-width=\"true\"
                             data-title=\"";
            // line 63
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_SelectCountry"]), "html_attr");
            echo "\"
                             options='";
            // line 64
            echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["countries"]) || array_key_exists("countries", $context) ? $context["countries"] : (function () { throw new RuntimeError('Variable "countries" does not exist.', 64, $this->source); })())), "html", null, true);
            echo "'>
                        </div>
                    </div>
                    <div class=\"col s12 m6 form-help\">
                        ";
            // line 68
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_PhoneNumbers_CountryCode_Help"]), "html", null, true);
            echo "
                    </div>
                </div>

                <div class=\"form-group row addPhoneNumber\">
                    <div class=\"col s12 m6\">

                        <div class=\"countryCode left\">
                            <span class=\"countryCodeSymbol\">+</span>
                            <div piwik-field uicontrol=\"text\" name=\"countryCallingCode\"
                                 full-width=\"true\"
                                 ng-model=\"managePhoneNumber.countryCallingCode\"
                                 maxlength=\"4\"
                                 data-title=\"";
            // line 81
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_CountryCode"]), "html", null, true);
            echo "\">
                            </div>
                        </div>
                        <div class=\"phoneNumber left\">
                            <div piwik-field uicontrol=\"text\" name=\"newPhoneNumber\"
                                 ng-model=\"managePhoneNumber.newPhoneNumber\"
                                 ng-change=\"managePhoneNumber.validateNewPhoneNumberFormat()\"
                                 full-width=\"true\"
                                 maxlength=\"80\"
                                 data-title=\"";
            // line 90
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_PhoneNumber"]), "html", null, true);
            echo "\">
                            </div>
                        </div>
                        <div class=\"addNumber left valign-wrapper\">
                            <div piwik-save-button
                                 data-disabled=\"!managePhoneNumber.canAddNumber || managePhoneNumber.isAddingPhonenumber\"
                                 onconfirm=\"managePhoneNumber.addPhoneNumber()\"
                                 class=\"valign\" value='";
            // line 97
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Add"]), "html", null, true);
            echo "'></div>
                        </div>

                        <div piwik-alert=\"warning\"
                             id=\"suspiciousPhoneNumber\"
                             ng-show=\"managePhoneNumber.showSuspiciousPhoneNumber\">
                        ";
            // line 103
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_SuspiciousPhoneNumber", "54184032"]), "html", null, true);
            echo "
                        </div>

                    </div>
                    <div class=\"col s12 m6 form-help\">
                        ";
            // line 108
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["strHelpAddPhone"]) || array_key_exists("strHelpAddPhone", $context) ? $context["strHelpAddPhone"] : (function () { throw new RuntimeError('Variable "strHelpAddPhone" does not exist.', 108, $this->source); })()), "html", null, true);
            echo "
                    </div>
                </div>

                <div id=\"ajaxErrorAddPhoneNumber\"></div>
                <div piwik-activity-indicator loading=\"managePhoneNumber.isAddingPhonenumber\"></div>

                ";
            // line 115
            if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["phoneNumbers"]) || array_key_exists("phoneNumbers", $context) ? $context["phoneNumbers"] : (function () { throw new RuntimeError('Variable "phoneNumbers" does not exist.', 115, $this->source); })())), 0))) {
                // line 116
                echo "                    <div class=\"row\"><h3 class=\"col s12\">";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_ManagePhoneNumbers"]), "html", null, true);
                echo "</h3></div>
                ";
            }
            // line 118
            echo "
                ";
            // line 119
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["phoneNumbers"]) || array_key_exists("phoneNumbers", $context) ? $context["phoneNumbers"] : (function () { throw new RuntimeError('Variable "phoneNumbers" does not exist.', 119, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["phoneNumber"] => $context["validated"]) {
                // line 120
                echo "                    <div class=\"form-group row\">
                        <div class=\"col s12 m6\">
                            <span class='phoneNumber'>";
                // line 122
                echo \Piwik\piwik_escape_filter($this->env, $context["phoneNumber"], "html", null, true);
                echo "</span>

                            ";
                // line 124
                if ( !$context["validated"]) {
                    // line 125
                    echo "                                <input type=\"text\"
                                       ng-hide=\"managePhoneNumber.isActivated[";
                    // line 126
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 126), "html", null, true);
                    echo "]\"
                                       ng-model=\"managePhoneNumber.validationCode[";
                    // line 127
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 127), "html", null, true);
                    echo "]\"
                                       class='verificationCode'
                                       placeholder=\"";
                    // line 129
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_EnterActivationCode"]), "html_attr");
                    echo "\"/>
                                <div piwik-save-button
                                     ng-hide=\"managePhoneNumber.isActivated[";
                    // line 131
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 131), "html", null, true);
                    echo "]\"
                                     value='";
                    // line 132
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_ValidatePhoneNumber"]), "html", null, true);
                    echo "'
                                     data-disabled=\"!managePhoneNumber.validationCode[";
                    // line 133
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 133), "html", null, true);
                    echo "] || managePhoneNumber.isChangingPhoneNumber\"
                                     onconfirm='managePhoneNumber.validateActivationCode(";
                    // line 134
                    echo \Piwik\piwik_escape_filter($this->env, json_encode($context["phoneNumber"]), "html", null, true);
                    echo ", ";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 134), "html", null, true);
                    echo ")'
                                     ></div>
                            ";
                }
                // line 137
                echo "
                            <div piwik-save-button
                                 value='";
                // line 139
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Remove"]), "html", null, true);
                echo "'
                                 data-disabled=\"managePhoneNumber.isChangingPhoneNumber\"
                                 onconfirm=\"managePhoneNumber.removePhoneNumber(";
                // line 141
                echo \Piwik\piwik_escape_filter($this->env, json_encode($context["phoneNumber"]), "html", null, true);
                echo ")\"
                                 ></div>
                        </div>

                        ";
                // line 145
                if ( !$context["validated"]) {
                    // line 146
                    echo "                            <div class=\"form-help col s12 m6\">
                                <div ng-hide=\"managePhoneNumber.isActivated[";
                    // line 147
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 147), "html", null, true);
                    echo "]\">
                                    ";
                    // line 148
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_VerificationCodeJustSent"]), "html", null, true);
                    echo "
                                </div>
                                &nbsp;
                            </div>
                        ";
                }
                // line 153
                echo "                    </div>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['phoneNumber'], $context['validated'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 155
            echo "
                ";
            // line 156
            $macros["ajax"] = $this->loadTemplate("ajaxMacros.twig", "@MobileMessaging/index.twig", 156)->unwrap();
            // line 157
            echo "                ";
            echo twig_call_macro($macros["ajax"], "macro_errorDiv", ["invalidVerificationCodeAjaxError"], 157, $context, $this->getSourceContext());
            echo "

                <div piwik-activity-indicator loading=\"managePhoneNumber.isChangingPhoneNumber\"></div>

            </div>

        ";
        }
        // line 164
        echo "    </div>


    <div class='ui-confirm' id='confirmDeleteAccount'>
        <h2>";
        // line 168
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["MobileMessaging_Settings_DeleteAccountConfirm"]), "html", null, true);
        echo "</h2>
        <input role='yes' type='button' value='";
        // line 169
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
        echo "'/>
        <input role='no' type='button' value='";
        // line 170
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
        echo "'/>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@MobileMessaging/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 170,  412 => 169,  408 => 168,  402 => 164,  391 => 157,  389 => 156,  386 => 155,  371 => 153,  363 => 148,  359 => 147,  356 => 146,  354 => 145,  347 => 141,  342 => 139,  338 => 137,  330 => 134,  326 => 133,  322 => 132,  318 => 131,  313 => 129,  308 => 127,  304 => 126,  301 => 125,  299 => 124,  294 => 122,  290 => 120,  273 => 119,  270 => 118,  264 => 116,  262 => 115,  252 => 108,  244 => 103,  235 => 97,  225 => 90,  213 => 81,  197 => 68,  190 => 64,  186 => 63,  180 => 60,  171 => 54,  167 => 52,  161 => 50,  159 => 49,  154 => 47,  150 => 45,  146 => 43,  140 => 41,  134 => 39,  132 => 38,  129 => 37,  127 => 36,  123 => 35,  120 => 34,  114 => 31,  111 => 30,  105 => 28,  103 => 27,  97 => 25,  95 => 24,  92 => 23,  79 => 17,  75 => 16,  69 => 13,  62 => 10,  60 => 9,  57 => 8,  53 => 7,  48 => 1,  44 => 5,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% import '@MobileMessaging/macros.twig' as macro %}

{% set title %}{{ 'MobileMessaging_SettingsMenu'|translate }}{% endset %}

{% block content %}
<div class=\"manageMobileMessagingSettings\">
    {% if isSuperUser %}
    <div piwik-content-block content-title=\"{{ title|e('html_attr') }}\">
        <div ng-controller=\"DelegateMobileMessagingSettingsController as delegateManagement\">
            <div piwik-field uicontrol=\"radio\" name=\"delegatedManagement\"
                 options=\"{{ delegateManagementOptions|json_encode }}\"
                 full-width=\"true\"
                 ng-model=\"delegateManagement.enabled\"
                 data-title=\"{{ 'MobileMessaging_Settings_LetUsersManageAPICredential'|translate|e('html_attr') }}\"
                 value=\"{% if delegatedManagement %}1{% else %}0{% endif %}\">
            </div>
            <div piwik-save-button onconfirm=\"delegateManagement.save()\" saving=\"delegateManagement.isLoading\"></div>
        </div>
    </div>
    {% endif %}

    {% if accountManagedByCurrentUser %}
        <div piwik-content-block content-title=\"{{ 'MobileMessaging_Settings_SMSProvider'|translate|e('html_attr') }}\" feature=\"true\">

            {% if isSuperUser and delegatedManagement %}
                <p>{{ 'MobileMessaging_Settings_DelegatedSmsProviderOnlyAppliesToYou'|translate }}</p>
            {% endif %}

            {{ macro.manageSmsApi(credentialSupplied, credentialError, creditLeft, smsProviderOptions, smsProviders, provider) }}
        </div>
    {% endif %}

    <div piwik-content-block content-title=\"{{ 'MobileMessaging_PhoneNumbers'|translate|e('html_attr') }}\">
        {% if not credentialSupplied %}
            <p>
                {% if accountManagedByCurrentUser %}
                    {{ 'MobileMessaging_Settings_CredentialNotProvided'|translate }}
                {% else %}
                    {{ 'MobileMessaging_Settings_CredentialNotProvidedByAdmin'|translate }}
                {% endif %}
            </p>
        {% else %}
            <div ng-controller=\"ManageMobilePhoneNumbersController as managePhoneNumber\">

                <p>{{ 'MobileMessaging_Settings_PhoneNumbers_Help'|translate }}</p>

                {% if isSuperUser %}
                    <p>{{ 'MobileMessaging_Settings_DelegatedPhoneNumbersOnlyUsedByYou'|translate }}</p>
                {% endif %}

                <div class=\"row\">
                    <h3 class=\"col s12\">{{ 'MobileMessaging_Settings_PhoneNumbers_Add'|translate }}</h3>
                </div>

                <div class=\"form-group row\">
                    <div class=\"col s12 m6\">
                        <div piwik-field uicontrol=\"select\" name=\"countryCodeSelect\"
                             value=\"{{ defaultCallingCode }}\"
                             ng-model=\"managePhoneNumber.countryCallingCode\"
                             full-width=\"true\"
                             data-title=\"{{ 'MobileMessaging_Settings_SelectCountry'|translate|e('html_attr') }}\"
                             options='{{ countries|json_encode }}'>
                        </div>
                    </div>
                    <div class=\"col s12 m6 form-help\">
                        {{ 'MobileMessaging_Settings_PhoneNumbers_CountryCode_Help'|translate }}
                    </div>
                </div>

                <div class=\"form-group row addPhoneNumber\">
                    <div class=\"col s12 m6\">

                        <div class=\"countryCode left\">
                            <span class=\"countryCodeSymbol\">+</span>
                            <div piwik-field uicontrol=\"text\" name=\"countryCallingCode\"
                                 full-width=\"true\"
                                 ng-model=\"managePhoneNumber.countryCallingCode\"
                                 maxlength=\"4\"
                                 data-title=\"{{ 'MobileMessaging_Settings_CountryCode'|translate }}\">
                            </div>
                        </div>
                        <div class=\"phoneNumber left\">
                            <div piwik-field uicontrol=\"text\" name=\"newPhoneNumber\"
                                 ng-model=\"managePhoneNumber.newPhoneNumber\"
                                 ng-change=\"managePhoneNumber.validateNewPhoneNumberFormat()\"
                                 full-width=\"true\"
                                 maxlength=\"80\"
                                 data-title=\"{{ 'MobileMessaging_Settings_PhoneNumber'|translate }}\">
                            </div>
                        </div>
                        <div class=\"addNumber left valign-wrapper\">
                            <div piwik-save-button
                                 data-disabled=\"!managePhoneNumber.canAddNumber || managePhoneNumber.isAddingPhonenumber\"
                                 onconfirm=\"managePhoneNumber.addPhoneNumber()\"
                                 class=\"valign\" value='{{ 'General_Add'|translate }}'></div>
                        </div>

                        <div piwik-alert=\"warning\"
                             id=\"suspiciousPhoneNumber\"
                             ng-show=\"managePhoneNumber.showSuspiciousPhoneNumber\">
                        {{ 'MobileMessaging_Settings_SuspiciousPhoneNumber'|translate('54184032') }}
                        </div>

                    </div>
                    <div class=\"col s12 m6 form-help\">
                        {{ strHelpAddPhone }}
                    </div>
                </div>

                <div id=\"ajaxErrorAddPhoneNumber\"></div>
                <div piwik-activity-indicator loading=\"managePhoneNumber.isAddingPhonenumber\"></div>

                {% if phoneNumbers|length > 0 %}
                    <div class=\"row\"><h3 class=\"col s12\">{{ 'MobileMessaging_Settings_ManagePhoneNumbers'|translate }}</h3></div>
                {% endif %}

                {% for phoneNumber, validated in phoneNumbers %}
                    <div class=\"form-group row\">
                        <div class=\"col s12 m6\">
                            <span class='phoneNumber'>{{ phoneNumber }}</span>

                            {% if not validated %}
                                <input type=\"text\"
                                       ng-hide=\"managePhoneNumber.isActivated[{{ loop.index }}]\"
                                       ng-model=\"managePhoneNumber.validationCode[{{ loop.index }}]\"
                                       class='verificationCode'
                                       placeholder=\"{{ 'MobileMessaging_Settings_EnterActivationCode'|translate|e('html_attr') }}\"/>
                                <div piwik-save-button
                                     ng-hide=\"managePhoneNumber.isActivated[{{ loop.index }}]\"
                                     value='{{ 'MobileMessaging_Settings_ValidatePhoneNumber'|translate }}'
                                     data-disabled=\"!managePhoneNumber.validationCode[{{ loop.index }}] || managePhoneNumber.isChangingPhoneNumber\"
                                     onconfirm='managePhoneNumber.validateActivationCode({{ phoneNumber|json_encode }}, {{ loop.index }})'
                                     ></div>
                            {% endif %}

                            <div piwik-save-button
                                 value='{{ 'General_Remove'|translate }}'
                                 data-disabled=\"managePhoneNumber.isChangingPhoneNumber\"
                                 onconfirm=\"managePhoneNumber.removePhoneNumber({{ phoneNumber|json_encode }})\"
                                 ></div>
                        </div>

                        {% if not validated %}
                            <div class=\"form-help col s12 m6\">
                                <div ng-hide=\"managePhoneNumber.isActivated[{{ loop.index }}]\">
                                    {{ 'MobileMessaging_Settings_VerificationCodeJustSent'|translate }}
                                </div>
                                &nbsp;
                            </div>
                        {% endif %}
                    </div>
                {% endfor %}

                {% import 'ajaxMacros.twig' as ajax %}
                {{ ajax.errorDiv('invalidVerificationCodeAjaxError') }}

                <div piwik-activity-indicator loading=\"managePhoneNumber.isChangingPhoneNumber\"></div>

            </div>

        {% endif %}
    </div>


    <div class='ui-confirm' id='confirmDeleteAccount'>
        <h2>{{ 'MobileMessaging_Settings_DeleteAccountConfirm'|translate }}</h2>
        <input role='yes' type='button' value='{{ 'General_Yes'|translate }}'/>
        <input role='no' type='button' value='{{ 'General_No'|translate }}'/>
    </div>
</div>
{% endblock %}
", "@MobileMessaging/index.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/MobileMessaging/templates/index.twig");
    }
}
