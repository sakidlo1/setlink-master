<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Installation/cannotConnectToDb.twig */
class __TwigTemplate_13b23be861509021b57d39afba9d83b149bfa8be84bca60646803aaa790548a4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p>";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Installation_CannotConnectToDb"]), "html", null, true);
        echo ":</p>

<p><strong>";
        // line 3
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["exceptionMessage"]) || array_key_exists("exceptionMessage", $context) ? $context["exceptionMessage"] : (function () { throw new RuntimeError('Variable "exceptionMessage" does not exist.', 3, $this->source); })()), "html", null, true);
        echo "</strong></p>

<p>";
        // line 5
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Installation_CannotConnectToDbResolvingExplanation", "<a href=\"javascript:window.location.reload()\">", "</a>"]);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "@Installation/cannotConnectToDb.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 5,  43 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p>{{ 'Installation_CannotConnectToDb'|translate }}:</p>

<p><strong>{{ exceptionMessage }}</strong></p>

<p>{{ 'Installation_CannotConnectToDbResolvingExplanation'|translate('<a href=\"javascript:window.location.reload()\">', '</a>')|raw }}</p>", "@Installation/cannotConnectToDb.twig", "/opt/www/climate.setsocialimpact.com/public_html/carethebear/analytics/plugins/Installation/templates/cannotConnectToDb.twig");
    }
}
