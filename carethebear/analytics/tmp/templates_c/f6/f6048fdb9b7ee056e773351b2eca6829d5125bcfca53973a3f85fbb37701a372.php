<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Marketplace/uploadPluginDialog.twig */
class __TwigTemplate_912b755cc8fd32603771e37593bb1df784da1641403740c7132b73317ef32e8f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"ui-confirm\" id=\"installPluginByUpload\" piwik-plugin-upload>
    <h2>";
        // line 2
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_TeaserExtendPiwikByUpload"]), "html", null, true);
        echo "</h2>

    ";
        // line 4
        if ((isset($context["isPluginUploadEnabled"]) || array_key_exists("isPluginUploadEnabled", $context) ? $context["isPluginUploadEnabled"] : (function () { throw new RuntimeError('Variable "isPluginUploadEnabled" does not exist.', 4, $this->source); })())) {
            // line 5
            echo "        <p class=\"description\"> ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_AllowedUploadFormats"]), "html", null, true);
            echo " </p>

        <form enctype=\"multipart/form-data\" method=\"post\" id=\"uploadPluginForm\"
              action=\"";
            // line 8
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "CorePluginsAdmin", "action" => "uploadPlugin", "nonce" => (isset($context["installNonce"]) || array_key_exists("installNonce", $context) ? $context["installNonce"] : (function () { throw new RuntimeError('Variable "installNonce" does not exist.', 8, $this->source); })())]]), "html", null, true);
            echo "\">
            <input type=\"file\" name=\"pluginZip\" data-max-size=\"";
            // line 9
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["uploadLimit"]) || array_key_exists("uploadLimit", $context) ? $context["uploadLimit"] : (function () { throw new RuntimeError('Variable "uploadLimit" does not exist.', 9, $this->source); })()), "html", null, true);
            echo "\">
            <br />
            <div piwik-field uicontrol=\"password\" name=\"confirmPassword\" autocomplete=\"off\"
                 data-title=\"";
            // line 12
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Login_ConfirmPasswordToContinue"]), "html_attr");
            echo "\"
                 value=\"\">
            </div>

            <input class=\"startUpload btn\" type=\"submit\" value=\"";
            // line 16
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_UploadZipFile"]), "html", null, true);
            echo "\">
        </form>
    ";
        } else {
            // line 19
            echo "        <p class=\"description\"> ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PluginUploadDisabled"]);
            echo " </p>
        <pre>[General]
enable_plugin_upload = 1</pre>
        <input role=\"yes\" type=\"button\" value=\"";
            // line 22
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Ok"]), "html", null, true);
            echo "\"/>
    ";
        }
        // line 24
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "@Marketplace/uploadPluginDialog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 24,  84 => 22,  77 => 19,  71 => 16,  64 => 12,  58 => 9,  54 => 8,  47 => 5,  45 => 4,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ui-confirm\" id=\"installPluginByUpload\" piwik-plugin-upload>
    <h2>{{ 'Marketplace_TeaserExtendPiwikByUpload'|translate }}</h2>

    {% if isPluginUploadEnabled %}
        <p class=\"description\"> {{ 'Marketplace_AllowedUploadFormats'|translate }} </p>

        <form enctype=\"multipart/form-data\" method=\"post\" id=\"uploadPluginForm\"
              action=\"{{ linkTo({'module':'CorePluginsAdmin', 'action':'uploadPlugin', 'nonce': installNonce}) }}\">
            <input type=\"file\" name=\"pluginZip\" data-max-size=\"{{ uploadLimit }}\">
            <br />
            <div piwik-field uicontrol=\"password\" name=\"confirmPassword\" autocomplete=\"off\"
                 data-title=\"{{ 'Login_ConfirmPasswordToContinue'|translate|e('html_attr') }}\"
                 value=\"\">
            </div>

            <input class=\"startUpload btn\" type=\"submit\" value=\"{{ 'Marketplace_UploadZipFile'|translate }}\">
        </form>
    {% else %}
        <p class=\"description\"> {{ 'Marketplace_PluginUploadDisabled'|translate|raw }} </p>
        <pre>[General]
enable_plugin_upload = 1</pre>
        <input role=\"yes\" type=\"button\" value=\"{{ 'General_Ok'|translate }}\"/>
    {% endif %}
</div>", "@Marketplace/uploadPluginDialog.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/uploadPluginDialog.twig");
    }
}
