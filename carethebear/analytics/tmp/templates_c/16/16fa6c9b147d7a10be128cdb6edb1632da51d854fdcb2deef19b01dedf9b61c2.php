<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @API/listAllAPI.twig */
class __TwigTemplate_c270bc201f48950138babb339217e323fa87ad3a8a534992fe87adaf1f1de9ce extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'topcontrols' => [$this, 'block_topcontrols'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_ReportingApiReference"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@API/listAllAPI.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_topcontrols($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $this->loadTemplate("@CoreHome/_siteSelectHeader.twig", "@API/listAllAPI.twig", 6)->display($context);
        // line 7
        echo "    ";
        $this->loadTemplate("@CoreHome/_periodSelect.twig", "@API/listAllAPI.twig", 7)->display($context);
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "
<div class=\"api-list\">
    <div piwik-content-block content-title=\"";
        // line 13
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 13, $this->source); })()), "html", null, true);
        echo "\" rate=\"true\">
        <p>";
        // line 14
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_PluginDescription"]), "html", null, true);
        echo "</p>

        <p>
            ";
        // line 17
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_MoreInformation", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/analytics-api'>", "</a>", "<a target='_blank' rel='noreferrer' href='https://developer.matomo.org/api-reference/reporting-api'>", "</a>"]);
        echo "
        </p>
    </div>
    <div piwik-content-block content-title=\"";
        // line 20
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_UserAuthentication"]), "html_attr");
        echo "\">
        <p>
            ";
        // line 22
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["API_UsingTokenAuth", "", "", "<code>token_auth</code>"]);
        echo " <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/api-reference/reporting-api#authenticate-to-the-api-via-token_auth-parameter\">";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LearnMore"]), "html", null, true);
        echo "</a><br/>
            <br/>
            <a href=\"";
        // line 24
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "UsersManager", "action" => "userSecurity"]]), "html", null, true);
        // line 27
        echo "#/#authtokens\">You can manage your authentication tokens on your security page.</a>
        </p>
    </div>
    ";
        // line 30
        echo (isset($context["list_api_methods_with_links"]) || array_key_exists("list_api_methods_with_links", $context) ? $context["list_api_methods_with_links"] : (function () { throw new RuntimeError('Variable "list_api_methods_with_links" does not exist.', 30, $this->source); })());
        echo "
    <br/>
</div>
";
    }

    public function getTemplateName()
    {
        return "@API/listAllAPI.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 30,  102 => 27,  100 => 24,  93 => 22,  88 => 20,  82 => 17,  76 => 14,  72 => 13,  68 => 11,  64 => 10,  59 => 7,  56 => 6,  52 => 5,  47 => 1,  43 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'API_ReportingApiReference'|translate }}{% endset %}

{% block topcontrols %}
    {% include \"@CoreHome/_siteSelectHeader.twig\" %}
    {% include \"@CoreHome/_periodSelect.twig\" %}
{% endblock %}

{% block content %}

<div class=\"api-list\">
    <div piwik-content-block content-title=\"{{ title }}\" rate=\"true\">
        <p>{{ 'API_PluginDescription'|translate }}</p>

        <p>
            {{ 'API_MoreInformation'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/analytics-api'>\",\"</a>\",\"<a target='_blank' rel='noreferrer' href='https://developer.matomo.org/api-reference/reporting-api'>\",\"</a>\")|raw }}
        </p>
    </div>
    <div piwik-content-block content-title=\"{{ 'API_UserAuthentication'|translate|e('html_attr') }}\">
        <p>
            {{ 'API_UsingTokenAuth'|translate('','',\"<code>token_auth</code>\")|raw }} <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/api-reference/reporting-api#authenticate-to-the-api-via-token_auth-parameter\">{{ 'CoreAdminHome_LearnMore'|translate }}</a><br/>
            <br/>
            <a href=\"{{ linkTo({
                'module': 'UsersManager',
                'action': 'userSecurity',
            }) }}#/#authtokens\">You can manage your authentication tokens on your security page.</a>
        </p>
    </div>
    {{ list_api_methods_with_links|raw }}
    <br/>
</div>
{% endblock %}
", "@API/listAllAPI.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/API/templates/listAllAPI.twig");
    }
}
