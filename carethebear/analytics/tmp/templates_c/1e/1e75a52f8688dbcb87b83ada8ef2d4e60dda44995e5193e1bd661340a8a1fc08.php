<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Marketplace/plugin-list.twig */
class __TwigTemplate_72f1777c6cb0f9b4eb342181be41895dc9d2e5817d6d6621f52b08e753a58cd9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((1 === twig_compare(twig_length_filter($this->env, (isset($context["pluginsToShow"]) || array_key_exists("pluginsToShow", $context) ? $context["pluginsToShow"] : (function () { throw new RuntimeError('Variable "pluginsToShow" does not exist.', 1, $this->source); })())), 0))) {
            // line 2
            echo "    <div class=\"pluginListContainer row\">
        ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pluginsToShow"]) || array_key_exists("pluginsToShow", $context) ? $context["pluginsToShow"] : (function () { throw new RuntimeError('Variable "pluginsToShow" does not exist.', 3, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["plugin"]) {
                // line 4
                echo "            <div class=\"col s12 m6 l4\">
                ";
                // line 5
                $this->loadTemplate("@Marketplace/plugin-list.twig", "@Marketplace/plugin-list.twig", 5, "1831592346")->display(twig_array_merge($context, ["title" => ""]));
                // line 152
                echo "            </div>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['plugin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 154
            echo "    </div>
";
        }
        // line 156
        echo "
";
        // line 157
        if ((0 === twig_compare(twig_length_filter($this->env, (isset($context["pluginsToShow"]) || array_key_exists("pluginsToShow", $context) ? $context["pluginsToShow"] : (function () { throw new RuntimeError('Variable "pluginsToShow" does not exist.', 157, $this->source); })())), 0))) {
            // line 158
            echo "    <div piwik-content-block>
        ";
            // line 159
            if ((isset($context["showThemes"]) || array_key_exists("showThemes", $context) ? $context["showThemes"] : (function () { throw new RuntimeError('Variable "showThemes" does not exist.', 159, $this->source); })())) {
                // line 160
                echo "            ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NoThemesFound"]), "html", null, true);
                echo "
        ";
            } else {
                // line 162
                echo "            ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NoPluginsFound"]), "html", null, true);
                echo "
        ";
            }
            // line 164
            echo "    </div>
";
        }
        // line 166
        echo "
";
    }

    public function getTemplateName()
    {
        return "@Marketplace/plugin-list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 166,  105 => 164,  99 => 162,  93 => 160,  91 => 159,  88 => 158,  86 => 157,  83 => 156,  79 => 154,  64 => 152,  62 => 5,  59 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if pluginsToShow|length > 0 %}
    <div class=\"pluginListContainer row\">
        {% for plugin in pluginsToShow %}
            <div class=\"col s12 m6 l4\">
                {% embed 'contentBlock.twig' with {'title': ''} %}
                    {% block content %}
                        {% import '@Marketplace/macros.twig' as marketplaceMacro %}
                        {% import '@CorePluginsAdmin/macros.twig' as pluginsMacro %}
                        <div class=\"plugin\">
                            <h3 class=\"card-title\" title=\"{{ 'General_MoreDetails'|translate }}\">
                                <a href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\">{{ plugin.displayName }}</a>
                            </h3>

                            <p class=\"description\">
                                {{ plugin.description }}
                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">
                                    &rsaquo; {{ 'General_MoreLowerCase'|translate }}</a>
                            </p>

                            {% if showThemes %}
                                {# Screenshot for themes #}
                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\">
                                    <img title=\"{{ 'General_MoreDetails'|translate }}\"
                                         class=\"preview\" src=\"{{ plugin.screenshots|first }}?w=250&h=150\"/></a>
                            {% endif %}

                            <ul class=\"metadata\">
                                {% if plugin.isBundle is not defined or not plugin.isBundle %}
                                    <li>
                                        {% if plugin.latestVersion %}
                                            {{ 'CorePluginsAdmin_Version'|translate }}: {{ plugin.latestVersion }}
                                        {% endif %}

                                        {% if plugin.canBeUpdated %}
                                            <a class=\"update-available\"
                                                {% if plugin.changelog is defined and plugin.changelog and plugin.changelog.url is defined and plugin.changelog.url %}
                                                    target=\"_blank\" href=\"{{ plugin.changelog.url|e('html_attr') }}\"
                                                {% else %}
                                                    href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\"
                                                {% endif %}
                                               title=\"{{ 'Marketplace_PluginUpdateAvailable'|translate(plugin.currentVersion, plugin.latestVersion) }}\">
                                                {{ 'Marketplace_NewVersion'|translate }}</a>
                                        {% endif %}
                                    </li>
                                    {% if plugin.lastUpdated %}
                                        <li>{{ 'Marketplace_Updated'|translate }}: {{ plugin.lastUpdated }}</li>
                                    {% endif %}
                                    {% if plugin.numDownloads %}
                                        <li>{{ 'General_Downloads'|translate }}: {{ plugin.numDownloads }}</li>
                                    {% endif %}
                                    <li>{{ 'Marketplace_Developer'|translate }}: {{ marketplaceMacro.pluginDeveloper(plugin.owner) }}</li>
                                {% endif %}
                            </ul>

                            {% macro moreDetailsLink(plugin) %}
                                {% set canBePurchased = not plugin.isDownloadable and plugin.shop is defined and plugin.shop and plugin.shop.url %}
                                <a class=\"btn btn-block plugin-details {% if canBePurchased %}purchaseable{% endif %}\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">

                                    {% if canBePurchased and plugin.shop.variations %}
                                        {% set foundCheapest = 0 %}
                                        {% for variation in plugin.shop.variations %}
                                            {% if not foundCheapest and variation.cheapest is defined and variation.cheapest %}
                                                {% set foundCheapest = 1 %}
                                                {{ 'Marketplace_PriceFromPerPeriod'|translate(variation.prettyPrice, variation.period) }}
                                            {% endif %}
                                        {% endfor %}
                                        {% if not foundCheapest %}
                                            {{ 'Marketplace_PriceFromPerPeriod'|translate(plugin.shop.variations.0.prettyPrice, plugin.shop.variations.0.period) }}
                                        {% endif %}
                                    {% else %}
                                        {{ 'General_MoreDetails'|translate }}
                                    {% endif %}

                                </a>
                            {% endmacro %}


                            {% if isSuperUser %}
                                <div class=\"footer\">
                                    {% if plugin.isMissingLicense is defined and plugin.isMissingLicense %}

                                        <div class=\"alert alert-danger\" >
                                            {{ 'Marketplace_LicenseMissing'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>)</span>
                                        </div>

                                    {% elseif plugin.hasExceededLicense is defined and plugin.hasExceededLicense %}

                                        <div class=\"alert alert-danger\">
                                            {{ 'Marketplace_LicenseExceeded'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>)</span>
                                        </div>

                                    {% elseif plugin.canBeUpdated and 0 == plugin.missingRequirements|length and isAutoUpdatePossible %}
                                        <a class=\"btn btn-block\"
                                           href=\"{{ linkTo({'module': 'Marketplace', 'action':'updatePlugin', 'pluginName': plugin.name, 'nonce': updateNonce}) }}\">
                                            {{ 'CoreUpdater_UpdateTitle'|translate }}
                                        </a>
                                    {% elseif plugin.missingRequirements|length > 0 or not isAutoUpdatePossible %}

                                        {% macro downloadButton(showOr, plugin, isAutoUpdatePossible, showBrackets = false) -%}
                                            {%- if plugin.missingRequirements|length == 0 and plugin.isDownloadable and not isAutoUpdatePossible -%}
                                                {% if showBrackets %}({% endif %}<span onclick=\"\$(this).css('display', 'none')\">
                                                {%- if showOr %} {{ 'General_Or'|translate }} {% endif -%}
                                                <a class=\"plugin-details download\"
                                                   href=\"{{ linkTo({'module': 'Marketplace', 'action': 'download', 'pluginName': plugin.name, 'nonce': (plugin.name|nonce)}) }}\"
                                                >{{ 'General_Download'|translate }}</a></span>{% if showBrackets %}){% endif %}
                                            {%- endif -%}
                                        {%- endmacro %}

                                        {% if plugin.canBeUpdated and 0 == plugin.missingRequirements|length %}
                                            {{ 'Marketplace_CannotUpdate'|translate }}
                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>{{ _self.downloadButton(true, plugin, isAutoUpdatePossible)|raw }})</span>
                                        {% elseif plugin.isInstalled %}
                                            {{ 'General_Installed'|translate }}
                                            {{ _self.downloadButton(false, plugin, isAutoUpdatePossible, true)|raw }}
                                        {% elseif not plugin.isDownloadable %}
                                            {{ _self.moreDetailsLink(plugin)|raw }}
                                        {% else %}
                                            {{ 'Marketplace_CannotInstall'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>{{ _self.downloadButton(true, plugin, isAutoUpdatePossible)|raw }})</span>
                                        {% endif %}

                                    {% elseif plugin.isInstalled %}
                                        {{ 'General_Installed'|translate }}

                                        {% if not plugin.isInvalid and not isMultiServerEnvironment and isPluginsAdminEnabled %}
                                            ({{ pluginsMacro.pluginActivateDeactivateAction(plugin.name, plugin.isActivated, plugin.missingRequirements, deactivateNonce, activateNonce) }})
                                        {% endif %}

                                    {% elseif plugin.isPaid and not plugin.isDownloadable %}
                                        {{ _self.moreDetailsLink(plugin)|raw }}
                                    {% else %}
                                        <a href=\"{{ linkTo({'module': 'Marketplace', 'action': 'installPlugin', 'pluginName': plugin.name, 'nonce': installNonce}) }}\"
                                           class=\"btn\">
                                            {{ 'Marketplace_ActionInstall'|translate }}
                                        </a>
                                    {% endif %}
                                </div>
                            {% else %}
                                <div class=\"footer\">
                                    {{ _self.moreDetailsLink(plugin)|raw }}
                                </div>
                            {% endif %}

                        </div>
                    {% endblock %}
                {% endembed %}
            </div>
        {% endfor %}
    </div>
{% endif %}

{% if pluginsToShow|length == 0 %}
    <div piwik-content-block>
        {% if showThemes %}
            {{ 'Marketplace_NoThemesFound'|translate }}
        {% else %}
            {{ 'Marketplace_NoPluginsFound'|translate }}
        {% endif %}
    </div>
{% endif %}

", "@Marketplace/plugin-list.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/plugin-list.twig");
    }
}


/* @Marketplace/plugin-list.twig */
class __TwigTemplate_72f1777c6cb0f9b4eb342181be41895dc9d2e5817d6d6621f52b08e753a58cd9___1831592346 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        // line 0
        $macros["_self"] = $this->macros["_self"] = $this;
    }

    protected function doGetParent(array $context)
    {
        // line 5
        return "contentBlock.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("contentBlock.twig", "@Marketplace/plugin-list.twig", 5);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "                        ";
        $macros["marketplaceMacro"] = $this->loadTemplate("@Marketplace/macros.twig", "@Marketplace/plugin-list.twig", 7)->unwrap();
        // line 8
        echo "                        ";
        $macros["pluginsMacro"] = $this->loadTemplate("@CorePluginsAdmin/macros.twig", "@Marketplace/plugin-list.twig", 8)->unwrap();
        // line 9
        echo "                        <div class=\"plugin\">
                            <h3 class=\"card-title\" title=\"";
        // line 10
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
        echo "\">
                                <a href=\"#\" piwik-plugin-name=\"";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 11, $this->source); })()), "name", [], "any", false, false, false, 11), "html", null, true);
        echo "\">";
        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 11, $this->source); })()), "displayName", [], "any", false, false, false, 11), "html", null, true);
        echo "</a>
                            </h3>

                            <p class=\"description\">
                                ";
        // line 15
        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 15, $this->source); })()), "description", [], "any", false, false, false, 15), "html", null, true);
        echo "
                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"";
        // line 16
        echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 16, $this->source); })()), "name", [], "any", false, false, false, 16), "html", null, true);
        echo "\" title=\"";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
        echo "\">
                                    &rsaquo; ";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreLowerCase"]), "html", null, true);
        echo "</a>
                            </p>

                            ";
        // line 20
        if ((isset($context["showThemes"]) || array_key_exists("showThemes", $context) ? $context["showThemes"] : (function () { throw new RuntimeError('Variable "showThemes" does not exist.', 20, $this->source); })())) {
            // line 21
            echo "                                ";
            // line 22
            echo "                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"";
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 22, $this->source); })()), "name", [], "any", false, false, false, 22), "html", null, true);
            echo "\">
                                    <img title=\"";
            // line 23
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
            echo "\"
                                         class=\"preview\" src=\"";
            // line 24
            echo \Piwik\piwik_escape_filter($this->env, twig_first($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 24, $this->source); })()), "screenshots", [], "any", false, false, false, 24)), "html", null, true);
            echo "?w=250&h=150\"/></a>
                            ";
        }
        // line 26
        echo "
                            <ul class=\"metadata\">
                                ";
        // line 28
        if (( !twig_get_attribute($this->env, $this->source, ($context["plugin"] ?? null), "isBundle", [], "any", true, true, false, 28) ||  !twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 28, $this->source); })()), "isBundle", [], "any", false, false, false, 28))) {
            // line 29
            echo "                                    <li>
                                        ";
            // line 30
            if (twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 30, $this->source); })()), "latestVersion", [], "any", false, false, false, 30)) {
                // line 31
                echo "                                            ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Version"]), "html", null, true);
                echo ": ";
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 31, $this->source); })()), "latestVersion", [], "any", false, false, false, 31), "html", null, true);
                echo "
                                        ";
            }
            // line 33
            echo "
                                        ";
            // line 34
            if (twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 34, $this->source); })()), "canBeUpdated", [], "any", false, false, false, 34)) {
                // line 35
                echo "                                            <a class=\"update-available\"
                                                ";
                // line 36
                if ((((twig_get_attribute($this->env, $this->source, ($context["plugin"] ?? null), "changelog", [], "any", true, true, false, 36) && twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 36, $this->source); })()), "changelog", [], "any", false, false, false, 36)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["plugin"] ?? null), "changelog", [], "any", false, true, false, 36), "url", [], "any", true, true, false, 36)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 36, $this->source); })()), "changelog", [], "any", false, false, false, 36), "url", [], "any", false, false, false, 36))) {
                    // line 37
                    echo "                                                    target=\"_blank\" href=\"";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 37, $this->source); })()), "changelog", [], "any", false, false, false, 37), "url", [], "any", false, false, false, 37), "html_attr");
                    echo "\"
                                                ";
                } else {
                    // line 39
                    echo "                                                    href=\"#\" piwik-plugin-name=\"";
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 39, $this->source); })()), "name", [], "any", false, false, false, 39), "html", null, true);
                    echo "\"
                                                ";
                }
                // line 41
                echo "                                               title=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PluginUpdateAvailable", twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 41, $this->source); })()), "currentVersion", [], "any", false, false, false, 41), twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 41, $this->source); })()), "latestVersion", [], "any", false, false, false, 41)]), "html", null, true);
                echo "\">
                                                ";
                // line 42
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NewVersion"]), "html", null, true);
                echo "</a>
                                        ";
            }
            // line 44
            echo "                                    </li>
                                    ";
            // line 45
            if (twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 45, $this->source); })()), "lastUpdated", [], "any", false, false, false, 45)) {
                // line 46
                echo "                                        <li>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_Updated"]), "html", null, true);
                echo ": ";
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 46, $this->source); })()), "lastUpdated", [], "any", false, false, false, 46), "html", null, true);
                echo "</li>
                                    ";
            }
            // line 48
            echo "                                    ";
            if (twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 48, $this->source); })()), "numDownloads", [], "any", false, false, false, 48)) {
                // line 49
                echo "                                        <li>";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Downloads"]), "html", null, true);
                echo ": ";
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 49, $this->source); })()), "numDownloads", [], "any", false, false, false, 49), "html", null, true);
                echo "</li>
                                    ";
            }
            // line 51
            echo "                                    <li>";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_Developer"]), "html", null, true);
            echo ": ";
            echo twig_call_macro($macros["marketplaceMacro"], "macro_pluginDeveloper", [twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 51, $this->source); })()), "owner", [], "any", false, false, false, 51)], 51, $context, $this->getSourceContext());
            echo "</li>
                                ";
        }
        // line 53
        echo "                            </ul>

                            ";
        // line 76
        echo "

                            ";
        // line 78
        if ((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 78, $this->source); })())) {
            // line 79
            echo "                                <div class=\"footer\">
                                    ";
            // line 80
            if ((twig_get_attribute($this->env, $this->source, ($context["plugin"] ?? null), "isMissingLicense", [], "any", true, true, false, 80) && twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 80, $this->source); })()), "isMissingLicense", [], "any", false, false, false, 80))) {
                // line 81
                echo "
                                        <div class=\"alert alert-danger\" >
                                            ";
                // line 83
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_LicenseMissing"]), "html", null, true);
                echo "

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"";
                // line 85
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 85, $this->source); })()), "name", [], "any", false, false, false, 85), "html", null, true);
                echo "\" title=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
                echo "\">";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Help"]), "html", null, true);
                echo "</a>)</span>
                                        </div>

                                    ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 88
($context["plugin"] ?? null), "hasExceededLicense", [], "any", true, true, false, 88) && twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 88, $this->source); })()), "hasExceededLicense", [], "any", false, false, false, 88))) {
                // line 89
                echo "
                                        <div class=\"alert alert-danger\">
                                            ";
                // line 91
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_LicenseExceeded"]), "html", null, true);
                echo "

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"";
                // line 93
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 93, $this->source); })()), "name", [], "any", false, false, false, 93), "html", null, true);
                echo "\" title=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
                echo "\">";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Help"]), "html", null, true);
                echo "</a>)</span>
                                        </div>

                                    ";
            } elseif (((twig_get_attribute($this->env, $this->source,             // line 96
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 96, $this->source); })()), "canBeUpdated", [], "any", false, false, false, 96) && (0 === twig_compare(0, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 96, $this->source); })()), "missingRequirements", [], "any", false, false, false, 96))))) && (isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 96, $this->source); })()))) {
                // line 97
                echo "                                        <a class=\"btn btn-block\"
                                           href=\"";
                // line 98
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "updatePlugin", "pluginName" => twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 98, $this->source); })()), "name", [], "any", false, false, false, 98), "nonce" => (isset($context["updateNonce"]) || array_key_exists("updateNonce", $context) ? $context["updateNonce"] : (function () { throw new RuntimeError('Variable "updateNonce" does not exist.', 98, $this->source); })())]]), "html", null, true);
                echo "\">
                                            ";
                // line 99
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreUpdater_UpdateTitle"]), "html", null, true);
                echo "
                                        </a>
                                    ";
            } elseif (((1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source,             // line 101
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 101, $this->source); })()), "missingRequirements", [], "any", false, false, false, 101)), 0)) ||  !(isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 101, $this->source); })()))) {
                // line 102
                echo "
                                        ";
                // line 112
                echo "
                                        ";
                // line 113
                if ((twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 113, $this->source); })()), "canBeUpdated", [], "any", false, false, false, 113) && (0 === twig_compare(0, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 113, $this->source); })()), "missingRequirements", [], "any", false, false, false, 113)))))) {
                    // line 114
                    echo "                                            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_CannotUpdate"]), "html", null, true);
                    echo "
                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"";
                    // line 115
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 115, $this->source); })()), "name", [], "any", false, false, false, 115), "html", null, true);
                    echo "\" title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
                    echo "\">";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Help"]), "html", null, true);
                    echo "</a>";
                    echo twig_call_macro($macros["_self"], "macro_downloadButton", [true, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 115, $this->source); })()), (isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 115, $this->source); })())], 115, $context, $this->getSourceContext());
                    echo ")</span>
                                        ";
                } elseif (twig_get_attribute($this->env, $this->source,                 // line 116
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 116, $this->source); })()), "isInstalled", [], "any", false, false, false, 116)) {
                    // line 117
                    echo "                                            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Installed"]), "html", null, true);
                    echo "
                                            ";
                    // line 118
                    echo twig_call_macro($macros["_self"], "macro_downloadButton", [false, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 118, $this->source); })()), (isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 118, $this->source); })()), true], 118, $context, $this->getSourceContext());
                    echo "
                                        ";
                } elseif ( !twig_get_attribute($this->env, $this->source,                 // line 119
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 119, $this->source); })()), "isDownloadable", [], "any", false, false, false, 119)) {
                    // line 120
                    echo "                                            ";
                    echo twig_call_macro($macros["_self"], "macro_moreDetailsLink", [(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 120, $this->source); })())], 120, $context, $this->getSourceContext());
                    echo "
                                        ";
                } else {
                    // line 122
                    echo "                                            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_CannotInstall"]), "html", null, true);
                    echo "

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"";
                    // line 124
                    echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 124, $this->source); })()), "name", [], "any", false, false, false, 124), "html", null, true);
                    echo "\" title=\"";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
                    echo "\">";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Help"]), "html", null, true);
                    echo "</a>";
                    echo twig_call_macro($macros["_self"], "macro_downloadButton", [true, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 124, $this->source); })()), (isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 124, $this->source); })())], 124, $context, $this->getSourceContext());
                    echo ")</span>
                                        ";
                }
                // line 126
                echo "
                                    ";
            } elseif (twig_get_attribute($this->env, $this->source,             // line 127
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 127, $this->source); })()), "isInstalled", [], "any", false, false, false, 127)) {
                // line 128
                echo "                                        ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Installed"]), "html", null, true);
                echo "

                                        ";
                // line 130
                if ((( !twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 130, $this->source); })()), "isInvalid", [], "any", false, false, false, 130) &&  !(isset($context["isMultiServerEnvironment"]) || array_key_exists("isMultiServerEnvironment", $context) ? $context["isMultiServerEnvironment"] : (function () { throw new RuntimeError('Variable "isMultiServerEnvironment" does not exist.', 130, $this->source); })())) && (isset($context["isPluginsAdminEnabled"]) || array_key_exists("isPluginsAdminEnabled", $context) ? $context["isPluginsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isPluginsAdminEnabled" does not exist.', 130, $this->source); })()))) {
                    // line 131
                    echo "                                            (";
                    echo twig_call_macro($macros["pluginsMacro"], "macro_pluginActivateDeactivateAction", [twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 131, $this->source); })()), "name", [], "any", false, false, false, 131), twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 131, $this->source); })()), "isActivated", [], "any", false, false, false, 131), twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 131, $this->source); })()), "missingRequirements", [], "any", false, false, false, 131), (isset($context["deactivateNonce"]) || array_key_exists("deactivateNonce", $context) ? $context["deactivateNonce"] : (function () { throw new RuntimeError('Variable "deactivateNonce" does not exist.', 131, $this->source); })()), (isset($context["activateNonce"]) || array_key_exists("activateNonce", $context) ? $context["activateNonce"] : (function () { throw new RuntimeError('Variable "activateNonce" does not exist.', 131, $this->source); })())], 131, $context, $this->getSourceContext());
                    echo ")
                                        ";
                }
                // line 133
                echo "
                                    ";
            } elseif ((twig_get_attribute($this->env, $this->source,             // line 134
(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 134, $this->source); })()), "isPaid", [], "any", false, false, false, 134) &&  !twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 134, $this->source); })()), "isDownloadable", [], "any", false, false, false, 134))) {
                // line 135
                echo "                                        ";
                echo twig_call_macro($macros["_self"], "macro_moreDetailsLink", [(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 135, $this->source); })())], 135, $context, $this->getSourceContext());
                echo "
                                    ";
            } else {
                // line 137
                echo "                                        <a href=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "installPlugin", "pluginName" => twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 137, $this->source); })()), "name", [], "any", false, false, false, 137), "nonce" => (isset($context["installNonce"]) || array_key_exists("installNonce", $context) ? $context["installNonce"] : (function () { throw new RuntimeError('Variable "installNonce" does not exist.', 137, $this->source); })())]]), "html", null, true);
                echo "\"
                                           class=\"btn\">
                                            ";
                // line 139
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_ActionInstall"]), "html", null, true);
                echo "
                                        </a>
                                    ";
            }
            // line 142
            echo "                                </div>
                            ";
        } else {
            // line 144
            echo "                                <div class=\"footer\">
                                    ";
            // line 145
            echo twig_call_macro($macros["_self"], "macro_moreDetailsLink", [(isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 145, $this->source); })())], 145, $context, $this->getSourceContext());
            echo "
                                </div>
                            ";
        }
        // line 148
        echo "
                        </div>
                    ";
    }

    // line 55
    public function macro_moreDetailsLink($__plugin__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "plugin" => $__plugin__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 56
            echo "                                ";
            $context["canBePurchased"] = ((( !twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 56, $this->source); })()), "isDownloadable", [], "any", false, false, false, 56) && twig_get_attribute($this->env, $this->source, ($context["plugin"] ?? null), "shop", [], "any", true, true, false, 56)) && twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 56, $this->source); })()), "shop", [], "any", false, false, false, 56)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 56, $this->source); })()), "shop", [], "any", false, false, false, 56), "url", [], "any", false, false, false, 56));
            // line 57
            echo "                                <a class=\"btn btn-block plugin-details ";
            if ((isset($context["canBePurchased"]) || array_key_exists("canBePurchased", $context) ? $context["canBePurchased"] : (function () { throw new RuntimeError('Variable "canBePurchased" does not exist.', 57, $this->source); })())) {
                echo "purchaseable";
            }
            echo "\" href=\"#\" piwik-plugin-name=\"";
            echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 57, $this->source); })()), "name", [], "any", false, false, false, 57), "html", null, true);
            echo "\" title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
            echo "\">

                                    ";
            // line 59
            if (((isset($context["canBePurchased"]) || array_key_exists("canBePurchased", $context) ? $context["canBePurchased"] : (function () { throw new RuntimeError('Variable "canBePurchased" does not exist.', 59, $this->source); })()) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 59, $this->source); })()), "shop", [], "any", false, false, false, 59), "variations", [], "any", false, false, false, 59))) {
                // line 60
                echo "                                        ";
                $context["foundCheapest"] = 0;
                // line 61
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 61, $this->source); })()), "shop", [], "any", false, false, false, 61), "variations", [], "any", false, false, false, 61));
                foreach ($context['_seq'] as $context["_key"] => $context["variation"]) {
                    // line 62
                    echo "                                            ";
                    if ((( !(isset($context["foundCheapest"]) || array_key_exists("foundCheapest", $context) ? $context["foundCheapest"] : (function () { throw new RuntimeError('Variable "foundCheapest" does not exist.', 62, $this->source); })()) && twig_get_attribute($this->env, $this->source, $context["variation"], "cheapest", [], "any", true, true, false, 62)) && twig_get_attribute($this->env, $this->source, $context["variation"], "cheapest", [], "any", false, false, false, 62))) {
                        // line 63
                        echo "                                                ";
                        $context["foundCheapest"] = 1;
                        // line 64
                        echo "                                                ";
                        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PriceFromPerPeriod", twig_get_attribute($this->env, $this->source, $context["variation"], "prettyPrice", [], "any", false, false, false, 64), twig_get_attribute($this->env, $this->source, $context["variation"], "period", [], "any", false, false, false, 64)]), "html", null, true);
                        echo "
                                            ";
                    }
                    // line 66
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variation'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 67
                echo "                                        ";
                if ( !(isset($context["foundCheapest"]) || array_key_exists("foundCheapest", $context) ? $context["foundCheapest"] : (function () { throw new RuntimeError('Variable "foundCheapest" does not exist.', 67, $this->source); })())) {
                    // line 68
                    echo "                                            ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_PriceFromPerPeriod", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 68, $this->source); })()), "shop", [], "any", false, false, false, 68), "variations", [], "any", false, false, false, 68), 0, [], "any", false, false, false, 68), "prettyPrice", [], "any", false, false, false, 68), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 68, $this->source); })()), "shop", [], "any", false, false, false, 68), "variations", [], "any", false, false, false, 68), 0, [], "any", false, false, false, 68), "period", [], "any", false, false, false, 68)]), "html", null, true);
                    echo "
                                        ";
                }
                // line 70
                echo "                                    ";
            } else {
                // line 71
                echo "                                        ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MoreDetails"]), "html", null, true);
                echo "
                                    ";
            }
            // line 73
            echo "
                                </a>
                            ";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 103
    public function macro_downloadButton($__showOr__ = null, $__plugin__ = null, $__isAutoUpdatePossible__ = null, $__showBrackets__ = false, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "showOr" => $__showOr__,
            "plugin" => $__plugin__,
            "isAutoUpdatePossible" => $__isAutoUpdatePossible__,
            "showBrackets" => $__showBrackets__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 104
            if ((((0 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 104, $this->source); })()), "missingRequirements", [], "any", false, false, false, 104)), 0)) && twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 104, $this->source); })()), "isDownloadable", [], "any", false, false, false, 104)) &&  !(isset($context["isAutoUpdatePossible"]) || array_key_exists("isAutoUpdatePossible", $context) ? $context["isAutoUpdatePossible"] : (function () { throw new RuntimeError('Variable "isAutoUpdatePossible" does not exist.', 104, $this->source); })()))) {
                // line 105
                if ((isset($context["showBrackets"]) || array_key_exists("showBrackets", $context) ? $context["showBrackets"] : (function () { throw new RuntimeError('Variable "showBrackets" does not exist.', 105, $this->source); })())) {
                    echo "(";
                }
                echo "<span onclick=\"\$(this).css('display', 'none')\">";
                // line 106
                if ((isset($context["showOr"]) || array_key_exists("showOr", $context) ? $context["showOr"] : (function () { throw new RuntimeError('Variable "showOr" does not exist.', 106, $this->source); })())) {
                    echo " ";
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Or"]), "html", null, true);
                    echo " ";
                }
                // line 107
                echo "<a class=\"plugin-details download\"
                                                   href=\"";
                // line 108
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "Marketplace", "action" => "download", "pluginName" => twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 108, $this->source); })()), "name", [], "any", false, false, false, 108), "nonce" => Piwik\Nonce::getNonce(twig_get_attribute($this->env, $this->source, (isset($context["plugin"]) || array_key_exists("plugin", $context) ? $context["plugin"] : (function () { throw new RuntimeError('Variable "plugin" does not exist.', 108, $this->source); })()), "name", [], "any", false, false, false, 108))]]), "html", null, true);
                echo "\"
                                                >";
                // line 109
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Download"]), "html", null, true);
                echo "</a></span>";
                if ((isset($context["showBrackets"]) || array_key_exists("showBrackets", $context) ? $context["showBrackets"] : (function () { throw new RuntimeError('Variable "showBrackets" does not exist.', 109, $this->source); })())) {
                    echo ")";
                }
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@Marketplace/plugin-list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  789 => 109,  785 => 108,  782 => 107,  776 => 106,  771 => 105,  769 => 104,  753 => 103,  742 => 73,  736 => 71,  733 => 70,  727 => 68,  724 => 67,  718 => 66,  712 => 64,  709 => 63,  706 => 62,  701 => 61,  698 => 60,  696 => 59,  684 => 57,  681 => 56,  668 => 55,  662 => 148,  656 => 145,  653 => 144,  649 => 142,  643 => 139,  637 => 137,  631 => 135,  629 => 134,  626 => 133,  620 => 131,  618 => 130,  612 => 128,  610 => 127,  607 => 126,  596 => 124,  590 => 122,  584 => 120,  582 => 119,  578 => 118,  573 => 117,  571 => 116,  561 => 115,  556 => 114,  554 => 113,  551 => 112,  548 => 102,  546 => 101,  541 => 99,  537 => 98,  534 => 97,  532 => 96,  522 => 93,  517 => 91,  513 => 89,  511 => 88,  501 => 85,  496 => 83,  492 => 81,  490 => 80,  487 => 79,  485 => 78,  481 => 76,  477 => 53,  469 => 51,  461 => 49,  458 => 48,  450 => 46,  448 => 45,  445 => 44,  440 => 42,  435 => 41,  429 => 39,  423 => 37,  421 => 36,  418 => 35,  416 => 34,  413 => 33,  405 => 31,  403 => 30,  400 => 29,  398 => 28,  394 => 26,  389 => 24,  385 => 23,  380 => 22,  378 => 21,  376 => 20,  370 => 17,  364 => 16,  360 => 15,  351 => 11,  347 => 10,  344 => 9,  341 => 8,  338 => 7,  334 => 6,  323 => 5,  317 => 0,  109 => 166,  105 => 164,  99 => 162,  93 => 160,  91 => 159,  88 => 158,  86 => 157,  83 => 156,  79 => 154,  64 => 152,  62 => 5,  59 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if pluginsToShow|length > 0 %}
    <div class=\"pluginListContainer row\">
        {% for plugin in pluginsToShow %}
            <div class=\"col s12 m6 l4\">
                {% embed 'contentBlock.twig' with {'title': ''} %}
                    {% block content %}
                        {% import '@Marketplace/macros.twig' as marketplaceMacro %}
                        {% import '@CorePluginsAdmin/macros.twig' as pluginsMacro %}
                        <div class=\"plugin\">
                            <h3 class=\"card-title\" title=\"{{ 'General_MoreDetails'|translate }}\">
                                <a href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\">{{ plugin.displayName }}</a>
                            </h3>

                            <p class=\"description\">
                                {{ plugin.description }}
                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">
                                    &rsaquo; {{ 'General_MoreLowerCase'|translate }}</a>
                            </p>

                            {% if showThemes %}
                                {# Screenshot for themes #}
                                <a class=\"more\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\">
                                    <img title=\"{{ 'General_MoreDetails'|translate }}\"
                                         class=\"preview\" src=\"{{ plugin.screenshots|first }}?w=250&h=150\"/></a>
                            {% endif %}

                            <ul class=\"metadata\">
                                {% if plugin.isBundle is not defined or not plugin.isBundle %}
                                    <li>
                                        {% if plugin.latestVersion %}
                                            {{ 'CorePluginsAdmin_Version'|translate }}: {{ plugin.latestVersion }}
                                        {% endif %}

                                        {% if plugin.canBeUpdated %}
                                            <a class=\"update-available\"
                                                {% if plugin.changelog is defined and plugin.changelog and plugin.changelog.url is defined and plugin.changelog.url %}
                                                    target=\"_blank\" href=\"{{ plugin.changelog.url|e('html_attr') }}\"
                                                {% else %}
                                                    href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\"
                                                {% endif %}
                                               title=\"{{ 'Marketplace_PluginUpdateAvailable'|translate(plugin.currentVersion, plugin.latestVersion) }}\">
                                                {{ 'Marketplace_NewVersion'|translate }}</a>
                                        {% endif %}
                                    </li>
                                    {% if plugin.lastUpdated %}
                                        <li>{{ 'Marketplace_Updated'|translate }}: {{ plugin.lastUpdated }}</li>
                                    {% endif %}
                                    {% if plugin.numDownloads %}
                                        <li>{{ 'General_Downloads'|translate }}: {{ plugin.numDownloads }}</li>
                                    {% endif %}
                                    <li>{{ 'Marketplace_Developer'|translate }}: {{ marketplaceMacro.pluginDeveloper(plugin.owner) }}</li>
                                {% endif %}
                            </ul>

                            {% macro moreDetailsLink(plugin) %}
                                {% set canBePurchased = not plugin.isDownloadable and plugin.shop is defined and plugin.shop and plugin.shop.url %}
                                <a class=\"btn btn-block plugin-details {% if canBePurchased %}purchaseable{% endif %}\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">

                                    {% if canBePurchased and plugin.shop.variations %}
                                        {% set foundCheapest = 0 %}
                                        {% for variation in plugin.shop.variations %}
                                            {% if not foundCheapest and variation.cheapest is defined and variation.cheapest %}
                                                {% set foundCheapest = 1 %}
                                                {{ 'Marketplace_PriceFromPerPeriod'|translate(variation.prettyPrice, variation.period) }}
                                            {% endif %}
                                        {% endfor %}
                                        {% if not foundCheapest %}
                                            {{ 'Marketplace_PriceFromPerPeriod'|translate(plugin.shop.variations.0.prettyPrice, plugin.shop.variations.0.period) }}
                                        {% endif %}
                                    {% else %}
                                        {{ 'General_MoreDetails'|translate }}
                                    {% endif %}

                                </a>
                            {% endmacro %}


                            {% if isSuperUser %}
                                <div class=\"footer\">
                                    {% if plugin.isMissingLicense is defined and plugin.isMissingLicense %}

                                        <div class=\"alert alert-danger\" >
                                            {{ 'Marketplace_LicenseMissing'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>)</span>
                                        </div>

                                    {% elseif plugin.hasExceededLicense is defined and plugin.hasExceededLicense %}

                                        <div class=\"alert alert-danger\">
                                            {{ 'Marketplace_LicenseExceeded'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>)</span>
                                        </div>

                                    {% elseif plugin.canBeUpdated and 0 == plugin.missingRequirements|length and isAutoUpdatePossible %}
                                        <a class=\"btn btn-block\"
                                           href=\"{{ linkTo({'module': 'Marketplace', 'action':'updatePlugin', 'pluginName': plugin.name, 'nonce': updateNonce}) }}\">
                                            {{ 'CoreUpdater_UpdateTitle'|translate }}
                                        </a>
                                    {% elseif plugin.missingRequirements|length > 0 or not isAutoUpdatePossible %}

                                        {% macro downloadButton(showOr, plugin, isAutoUpdatePossible, showBrackets = false) -%}
                                            {%- if plugin.missingRequirements|length == 0 and plugin.isDownloadable and not isAutoUpdatePossible -%}
                                                {% if showBrackets %}({% endif %}<span onclick=\"\$(this).css('display', 'none')\">
                                                {%- if showOr %} {{ 'General_Or'|translate }} {% endif -%}
                                                <a class=\"plugin-details download\"
                                                   href=\"{{ linkTo({'module': 'Marketplace', 'action': 'download', 'pluginName': plugin.name, 'nonce': (plugin.name|nonce)}) }}\"
                                                >{{ 'General_Download'|translate }}</a></span>{% if showBrackets %}){% endif %}
                                            {%- endif -%}
                                        {%- endmacro %}

                                        {% if plugin.canBeUpdated and 0 == plugin.missingRequirements|length %}
                                            {{ 'Marketplace_CannotUpdate'|translate }}
                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>{{ _self.downloadButton(true, plugin, isAutoUpdatePossible)|raw }})</span>
                                        {% elseif plugin.isInstalled %}
                                            {{ 'General_Installed'|translate }}
                                            {{ _self.downloadButton(false, plugin, isAutoUpdatePossible, true)|raw }}
                                        {% elseif not plugin.isDownloadable %}
                                            {{ _self.moreDetailsLink(plugin)|raw }}
                                        {% else %}
                                            {{ 'Marketplace_CannotInstall'|translate }}

                                            <span style=\"white-space:nowrap\">(<a class=\"plugin-details\" href=\"#\" piwik-plugin-name=\"{{ plugin.name }}\" title=\"{{ 'General_MoreDetails'|translate }}\">{{ 'General_Help'|translate }}</a>{{ _self.downloadButton(true, plugin, isAutoUpdatePossible)|raw }})</span>
                                        {% endif %}

                                    {% elseif plugin.isInstalled %}
                                        {{ 'General_Installed'|translate }}

                                        {% if not plugin.isInvalid and not isMultiServerEnvironment and isPluginsAdminEnabled %}
                                            ({{ pluginsMacro.pluginActivateDeactivateAction(plugin.name, plugin.isActivated, plugin.missingRequirements, deactivateNonce, activateNonce) }})
                                        {% endif %}

                                    {% elseif plugin.isPaid and not plugin.isDownloadable %}
                                        {{ _self.moreDetailsLink(plugin)|raw }}
                                    {% else %}
                                        <a href=\"{{ linkTo({'module': 'Marketplace', 'action': 'installPlugin', 'pluginName': plugin.name, 'nonce': installNonce}) }}\"
                                           class=\"btn\">
                                            {{ 'Marketplace_ActionInstall'|translate }}
                                        </a>
                                    {% endif %}
                                </div>
                            {% else %}
                                <div class=\"footer\">
                                    {{ _self.moreDetailsLink(plugin)|raw }}
                                </div>
                            {% endif %}

                        </div>
                    {% endblock %}
                {% endembed %}
            </div>
        {% endfor %}
    </div>
{% endif %}

{% if pluginsToShow|length == 0 %}
    <div piwik-content-block>
        {% if showThemes %}
            {{ 'Marketplace_NoThemesFound'|translate }}
        {% else %}
            {{ 'Marketplace_NoPluginsFound'|translate }}
        {% endif %}
    </div>
{% endif %}

", "@Marketplace/plugin-list.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/plugin-list.twig");
    }
}
