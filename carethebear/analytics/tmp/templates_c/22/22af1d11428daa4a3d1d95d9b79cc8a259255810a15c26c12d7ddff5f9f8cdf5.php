<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Marketplace/overview.twig */
class __TwigTemplate_1d89acdace0b4b06a336af6aba42429496115612e721b81eba14ae4002e02666 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((((isset($context["inReportingMenu"]) || array_key_exists("inReportingMenu", $context) ? $context["inReportingMenu"] : (function () { throw new RuntimeError('Variable "inReportingMenu" does not exist.', 1, $this->source); })())) ? ("empty.twig") : ("admin.twig")), "@Marketplace/overview.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_Marketplace"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    <div class=\"marketplace\" piwik-marketplace>

        <div piwik-content-intro>
            <h2 piwik-enriched-headline feature-name=\"";
        // line 10
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Marketplace"]), "html", null, true);
        echo "\"
            >";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 11, $this->source); })()), "html_attr");
        echo "</h2>

            <p>
                ";
        // line 14
        if ( !(isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 14, $this->source); })())) {
            // line 15
            echo "                    ";
            if ((isset($context["showThemes"]) || array_key_exists("showThemes", $context) ? $context["showThemes"] : (function () { throw new RuntimeError('Variable "showThemes" does not exist.', 15, $this->source); })())) {
                // line 16
                echo "                        ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NotAllowedToBrowseMarketplaceThemes"]), "html", null, true);
                echo "
                    ";
            } else {
                // line 18
                echo "                        ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NotAllowedToBrowseMarketplacePlugins"]), "html", null, true);
                echo "
                    ";
            }
            // line 20
            echo "                ";
        } elseif ((isset($context["showThemes"]) || array_key_exists("showThemes", $context) ? $context["showThemes"] : (function () { throw new RuntimeError('Variable "showThemes" does not exist.', 20, $this->source); })())) {
            // line 21
            echo "                    ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_ThemesDescription"]), "html", null, true);
            echo "
                    ";
            // line 22
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_InstallingNewPluginViaMarketplaceOrUpload", call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Themes"]), "<a href=\"#\" class=\"uploadPlugin\">", call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_Theme"]), "</a>"]);
            echo "
                ";
        } else {
            // line 24
            echo "                    ";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CorePluginsAdmin_PluginsExtendPiwik"]), "html", null, true);
            echo "
                    ";
            // line 25
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_InstallingNewPluginViaMarketplaceOrUpload", call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Plugins"]), "<a href=\"#\" class=\"uploadPlugin\">", call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Plugin"]), "</a>"]);
            echo "
                ";
        }
        // line 27
        echo "                ";
        if (((isset($context["isSuperUser"]) || array_key_exists("isSuperUser", $context) ? $context["isSuperUser"] : (function () { throw new RuntimeError('Variable "isSuperUser" does not exist.', 27, $this->source); })()) && (isset($context["inReportingMenu"]) || array_key_exists("inReportingMenu", $context) ? $context["inReportingMenu"] : (function () { throw new RuntimeError('Variable "inReportingMenu" does not exist.', 27, $this->source); })()))) {
            // line 28
            echo "                    ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_NoticeRemoveMarketplaceFromReportingMenu", "<a href=\"#\" piwik-plugin-name=\"WhiteLabel\">", "</a>"]);
            echo "
                ";
        }
        // line 30
        echo "            </p>

            ";
        // line 32
        $this->loadTemplate("@Marketplace/licenseform.twig", "@Marketplace/overview.twig", 32)->display($context);
        // line 33
        echo "
            ";
        // line 34
        $this->loadTemplate("@Marketplace/uploadPluginDialog.twig", "@Marketplace/overview.twig", 34)->display($context);
        // line 35
        echo "
            <div class=\"row marketplaceActions\" ng-controller=\"PiwikMarketplaceController as marketplace\">
                <div piwik-field uicontrol=\"select\" name=\"plugin_type\"
                     class=\"col s12 m6 l4\"
                     ng-model=\"marketplace.pluginType\"
                     ng-change=\"marketplace.changePluginType()\"
                     data-title=\"";
        // line 41
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Show"]), "html_attr");
        echo "\"
                     value=\"";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["pluginType"]) || array_key_exists("pluginType", $context) ? $context["pluginType"] : (function () { throw new RuntimeError('Variable "pluginType" does not exist.', 42, $this->source); })()), "html", null, true);
        echo "\"
                     full-width=\"true\"
                     options=\"";
        // line 44
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["pluginTypeOptions"]) || array_key_exists("pluginTypeOptions", $context) ? $context["pluginTypeOptions"] : (function () { throw new RuntimeError('Variable "pluginTypeOptions" does not exist.', 44, $this->source); })())), "html", null, true);
        echo "\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"plugin_sort\"
                     data-title=\"";
        // line 48
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Sort"]), "html_attr");
        echo "\"
                     value=\"";
        // line 49
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["sort"]) || array_key_exists("sort", $context) ? $context["sort"] : (function () { throw new RuntimeError('Variable "sort" does not exist.', 49, $this->source); })()), "html", null, true);
        echo "\"
                     ng-model=\"marketplace.pluginSort\"
                     ng-change=\"marketplace.changePluginSort()\"
                     class=\"col s12 m6 l4\"
                     full-width=\"true\"
                     options=\"";
        // line 54
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["pluginSortOptions"]) || array_key_exists("pluginSortOptions", $context) ? $context["pluginSortOptions"] : (function () { throw new RuntimeError('Variable "pluginSortOptions" does not exist.', 54, $this->source); })())), "html", null, true);
        echo "\">
                </div>

                ";
        // line 58
        echo "                ";
        if (((1 === twig_compare(twig_length_filter($this->env, (isset($context["pluginsToShow"]) || array_key_exists("pluginsToShow", $context) ? $context["pluginsToShow"] : (function () { throw new RuntimeError('Variable "pluginsToShow" does not exist.', 58, $this->source); })())), 20)) || (isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new RuntimeError('Variable "query" does not exist.', 58, $this->source); })()))) {
            // line 59
            echo "                    <div class=\"col s12 m12 l4 \">
                        <form action=\"";
            // line 60
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["sort" => "", "embed" => "0"]]), "html", null, true);
            echo "\" method=\"post\" class=\"plugin-search\">
                            <div piwik-field uicontrol=\"text\" name=\"query\"
                                 data-title=\"";
            // line 62
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Search"]), "html", null, true);
            echo " ";
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["numAvailablePlugins"]) || array_key_exists("numAvailablePlugins", $context) ? $context["numAvailablePlugins"] : (function () { throw new RuntimeError('Variable "numAvailablePlugins" does not exist.', 62, $this->source); })()), "html", null, true);
            echo " ";
            echo \Piwik\piwik_escape_filter($this->env, lcfirst(call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Plugins"])), "html", null, true);
            echo "...\"
                                 value=\"";
            // line 63
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["query"]) || array_key_exists("query", $context) ? $context["query"] : (function () { throw new RuntimeError('Variable "query" does not exist.', 63, $this->source); })()), "html", null, true);
            echo "\"
                                 full-width=\"true\">
                            </div>
                            <span class=\"icon-search\" onclick=\"\$('form.plugin-search').submit();\"></span>
                        </form>
                    </div>
                ";
        }
        // line 70
        echo "            </div>
        </div>

        ";
        // line 73
        $this->loadTemplate("@Marketplace/plugin-list.twig", "@Marketplace/overview.twig", 73)->display($context);
        // line 74
        echo "
        <div class=\"footer-message center\">
            ";
        // line 76
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Marketplace_DevelopersLearnHowToDevelopPlugins", "<a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/develop\">", "</a>"]);
        echo "
            <br />
            <br />
            <br />
            <a rel=\"noreferrer noopener\" href=\"https://shop.matomo.org/faq/\" target=\"_blank\">FAQ</a> |
            <a rel=\"noreferrer noopener\" href=\"https://shop.matomo.org/terms-conditions/\" target=\"_blank\">Terms</a> |
            <a rel=\"noreferrer noopener\" href=\"https://matomo.org/privacy-policy/\" target=\"_blank\">Privacy</a> |
            <a rel=\"noreferrer noopener\" href=\"https://matomo.org/contact/\" target=\"_blank\">Contact</a>
        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "@Marketplace/overview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 76,  208 => 74,  206 => 73,  201 => 70,  191 => 63,  183 => 62,  178 => 60,  175 => 59,  172 => 58,  166 => 54,  158 => 49,  154 => 48,  147 => 44,  142 => 42,  138 => 41,  130 => 35,  128 => 34,  125 => 33,  123 => 32,  119 => 30,  113 => 28,  110 => 27,  105 => 25,  100 => 24,  95 => 22,  90 => 21,  87 => 20,  81 => 18,  75 => 16,  72 => 15,  70 => 14,  64 => 11,  60 => 10,  54 => 6,  50 => 5,  46 => 1,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends inReportingMenu ? \"empty.twig\" : \"admin.twig\" %}

{% set title %}{{ 'Marketplace_Marketplace'|translate }}{% endset %}

{% block content %}

    <div class=\"marketplace\" piwik-marketplace>

        <div piwik-content-intro>
            <h2 piwik-enriched-headline feature-name=\"{{ 'CorePluginsAdmin_Marketplace'|translate }}\"
            >{{ title|e('html_attr') }}</h2>

            <p>
                {% if not isSuperUser %}
                    {% if showThemes %}
                        {{ 'Marketplace_NotAllowedToBrowseMarketplaceThemes'|translate }}
                    {% else %}
                        {{ 'Marketplace_NotAllowedToBrowseMarketplacePlugins'|translate }}
                    {% endif %}
                {% elseif showThemes %}
                    {{ 'CorePluginsAdmin_ThemesDescription'|translate }}
                    {{ 'Marketplace_InstallingNewPluginViaMarketplaceOrUpload'|translate(('CorePluginsAdmin_Themes'|translate), '<a href=\"#\" class=\"uploadPlugin\">', ('CorePluginsAdmin_Theme'|translate), '</a>')|raw }}
                {% else %}
                    {{ 'CorePluginsAdmin_PluginsExtendPiwik'|translate }}
                    {{ 'Marketplace_InstallingNewPluginViaMarketplaceOrUpload'|translate(('General_Plugins'|translate), '<a href=\"#\" class=\"uploadPlugin\">', ('General_Plugin'|translate), '</a>')|raw }}
                {% endif %}
                {% if isSuperUser and inReportingMenu %}
                    {{ 'Marketplace_NoticeRemoveMarketplaceFromReportingMenu'|translate('<a href=\"#\" piwik-plugin-name=\"WhiteLabel\">', '</a>')|raw }}
                {% endif %}
            </p>

            {% include '@Marketplace/licenseform.twig' %}

            {% include '@Marketplace/uploadPluginDialog.twig' %}

            <div class=\"row marketplaceActions\" ng-controller=\"PiwikMarketplaceController as marketplace\">
                <div piwik-field uicontrol=\"select\" name=\"plugin_type\"
                     class=\"col s12 m6 l4\"
                     ng-model=\"marketplace.pluginType\"
                     ng-change=\"marketplace.changePluginType()\"
                     data-title=\"{{ 'Show'|translate|e('html_attr') }}\"
                     value=\"{{ pluginType }}\"
                     full-width=\"true\"
                     options=\"{{ pluginTypeOptions|json_encode }}\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"plugin_sort\"
                     data-title=\"{{ 'Sort'|translate|e('html_attr') }}\"
                     value=\"{{ sort }}\"
                     ng-model=\"marketplace.pluginSort\"
                     ng-change=\"marketplace.changePluginSort()\"
                     class=\"col s12 m6 l4\"
                     full-width=\"true\"
                     options=\"{{ pluginSortOptions|json_encode }}\">
                </div>

                {# Hide filters and search for themes because we don't have many of them #}
                {% if (pluginsToShow|length) > 20 or query %}
                    <div class=\"col s12 m12 l4 \">
                        <form action=\"{{ linkTo({'sort': '', 'embed': '0'}) }}\" method=\"post\" class=\"plugin-search\">
                            <div piwik-field uicontrol=\"text\" name=\"query\"
                                 data-title=\"{{ 'General_Search'|translate }} {{ numAvailablePlugins }} {{ 'General_Plugins'|translate|lcfirst }}...\"
                                 value=\"{{ query }}\"
                                 full-width=\"true\">
                            </div>
                            <span class=\"icon-search\" onclick=\"\$('form.plugin-search').submit();\"></span>
                        </form>
                    </div>
                {% endif %}
            </div>
        </div>

        {% include '@Marketplace/plugin-list.twig' %}

        <div class=\"footer-message center\">
            {{ 'Marketplace_DevelopersLearnHowToDevelopPlugins'|translate('<a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://developer.matomo.org/develop\">', '</a>')|raw }}
            <br />
            <br />
            <br />
            <a rel=\"noreferrer noopener\" href=\"https://shop.matomo.org/faq/\" target=\"_blank\">FAQ</a> |
            <a rel=\"noreferrer noopener\" href=\"https://shop.matomo.org/terms-conditions/\" target=\"_blank\">Terms</a> |
            <a rel=\"noreferrer noopener\" href=\"https://matomo.org/privacy-policy/\" target=\"_blank\">Privacy</a> |
            <a rel=\"noreferrer noopener\" href=\"https://matomo.org/contact/\" target=\"_blank\">Contact</a>
        </div>

    </div>

{% endblock %}
", "@Marketplace/overview.twig", "/opt/www/carethebear.com/public_html/analytics/plugins/Marketplace/templates/overview.twig");
    }
}
