-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 24, 2022 at 09:44 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `set_social_impact`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE IF NOT EXISTS `activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `activity_type_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `activity_date` date NOT NULL,
  `start_on` time NOT NULL,
  `end_on` time NOT NULL,
  `description` text NOT NULL,
  `image_1` varchar(255) NOT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  `vdo` text NOT NULL,
  `attendant` int(11) NOT NULL,
  `premium_1` int(11) NOT NULL,
  `premium_2` int(11) NOT NULL,
  `premium_3` int(11) NOT NULL,
  `premium_4` int(11) NOT NULL,
  `premium_5` int(11) NOT NULL,
  `premium_6` double NOT NULL DEFAULT 0,
  `food_no_foam_1` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `food_pax_1` int(11) NOT NULL,
  `food_no_foam_2` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `food_pax_2` int(11) NOT NULL,
  `food_no_foam_3` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `food_pax_3` int(11) NOT NULL,
  `food_no_foam_4` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `food_pax_4` int(11) NOT NULL,
  `food_no_foam_5` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `food_pax_5` int(11) NOT NULL,
  `waste_1` double NOT NULL,
  `waste_2` double NOT NULL,
  `waste_3` double NOT NULL,
  `waste_4` double NOT NULL,
  `waste_5` double NOT NULL,
  `waste_6` double NOT NULL,
  `wood_1` int(11) NOT NULL,
  `wood_2` int(11) NOT NULL,
  `wood_3` int(11) NOT NULL,
  `wood_4` int(11) NOT NULL,
  `wood_5` int(11) NOT NULL,
  `pp_1` int(11) NOT NULL,
  `pp_2` int(11) NOT NULL,
  `pp_3` int(11) NOT NULL,
  `pp_4` int(11) NOT NULL,
  `pp_5` int(11) NOT NULL,
  `pp_kg` double NOT NULL DEFAULT 0,
  `building_a_1` double NOT NULL,
  `building_a_2` double NOT NULL,
  `building_a_3` double NOT NULL,
  `building_a_4` double NOT NULL,
  `building_b_1` double NOT NULL,
  `building_b_2` double NOT NULL,
  `building_b_3` double NOT NULL,
  `building_b_4` double NOT NULL,
  `building_b_5` double NOT NULL,
  `building_b_6` double NOT NULL,
  `building_b_7` double NOT NULL,
  `building_b_8` double NOT NULL,
  `building_c_1` double NOT NULL,
  `led_bulb_watt_1` int(11) NOT NULL,
  `led_bulb_hour_1` double NOT NULL,
  `led_bulb_watt_2` int(11) NOT NULL,
  `led_bulb_hour_2` double NOT NULL,
  `led_bulb_watt_3` int(11) NOT NULL,
  `led_bulb_hour_3` double NOT NULL,
  `led_bulb_watt_4` int(11) NOT NULL,
  `led_bulb_hour_4` double NOT NULL,
  `led_bulb_watt_5` int(11) NOT NULL,
  `led_bulb_hour_5` double NOT NULL,
  `led_bulb_watt_6` int(11) NOT NULL,
  `led_bulb_hour_6` double NOT NULL,
  `led_bulb_watt_7` int(11) NOT NULL,
  `led_bulb_hour_7` double NOT NULL,
  `led_bulb_watt_8` int(11) NOT NULL,
  `led_bulb_hour_8` double NOT NULL,
  `led_bulb_watt_9` int(11) NOT NULL,
  `led_bulb_hour_9` double NOT NULL,
  `led_bulb_watt_10` int(11) NOT NULL,
  `led_bulb_hour_10` double NOT NULL,
  `led_bulb_watt_11` int(11) NOT NULL,
  `led_bulb_hour_11` double NOT NULL,
  `led_bulb_custom_watt_1` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_watt_2` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_watt_3` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_watt_4` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_watt_5` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_qty_1` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_qty_2` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_qty_3` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_qty_4` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_qty_5` int(11) NOT NULL DEFAULT 0,
  `led_bulb_custom_hour_1` double NOT NULL DEFAULT 0,
  `led_bulb_custom_hour_2` double NOT NULL DEFAULT 0,
  `led_bulb_custom_hour_3` double NOT NULL DEFAULT 0,
  `led_bulb_custom_hour_4` double NOT NULL DEFAULT 0,
  `led_bulb_custom_hour_5` double NOT NULL DEFAULT 0,
  `led_tube_watt_1` int(11) NOT NULL,
  `led_tube_hour_1` double NOT NULL,
  `led_tube_watt_2` int(11) NOT NULL,
  `led_tube_hour_2` double NOT NULL,
  `led_tube_watt_3` int(11) NOT NULL,
  `led_tube_hour_3` double NOT NULL,
  `led_tube_watt_4` int(11) NOT NULL,
  `led_tube_hour_4` double NOT NULL,
  `led_tube_watt_5` int(11) NOT NULL,
  `led_tube_hour_5` double NOT NULL,
  `led_tube_watt_6` int(11) NOT NULL,
  `led_tube_hour_6` double NOT NULL,
  `led_tube_watt_7` int(11) NOT NULL,
  `led_tube_hour_7` double NOT NULL,
  `led_tube_watt_8` int(11) NOT NULL,
  `led_tube_hour_8` double NOT NULL,
  `led_tube_watt_9` int(11) NOT NULL,
  `led_tube_hour_9` double NOT NULL,
  `led_tube_watt_10` int(11) NOT NULL,
  `led_tube_hour_10` double NOT NULL,
  `led_tube_watt_11` int(11) NOT NULL,
  `led_tube_hour_11` double NOT NULL,
  `led_tube_custom_watt_1` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_watt_2` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_watt_3` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_watt_4` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_watt_5` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_qty_1` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_qty_2` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_qty_3` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_qty_4` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_qty_5` int(11) NOT NULL DEFAULT 0,
  `led_tube_custom_hour_1` double NOT NULL DEFAULT 0,
  `led_tube_custom_hour_2` double NOT NULL DEFAULT 0,
  `led_tube_custom_hour_3` double NOT NULL DEFAULT 0,
  `led_tube_custom_hour_4` double NOT NULL DEFAULT 0,
  `led_tube_custom_hour_5` double NOT NULL DEFAULT 0,
  `cf_care_1` double NOT NULL,
  `cf_care_2` double NOT NULL,
  `cf_care_3` double NOT NULL,
  `cf_care_4` double NOT NULL,
  `cf_care_5` double NOT NULL,
  `cf_care_6` double NOT NULL,
  `cf_care_total` double NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT 0,
  `status` char(1) NOT NULL DEFAULT 'Y' COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`),
  KEY `updated_on` (`updated_on`),
  KEY `member_id` (`member_id`),
  KEY `activity_date` (`activity_date`),
  KEY `start_on` (`start_on`),
  KEY `end_on` (`end_on`),
  KEY `attendant` (`attendant`),
  KEY `name` (`name`),
  KEY `place` (`place`),
  KEY `status` (`status`),
  KEY `updated_by` (`updated_by`),
  KEY `view_count` (`view_count`),
  KEY `activity_type_id` (`activity_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `member_id`, `activity_type_id`, `name`, `place`, `activity_date`, `start_on`, `end_on`, `description`, `image_1`, `image_2`, `image_3`, `vdo`, `attendant`, `premium_1`, `premium_2`, `premium_3`, `premium_4`, `premium_5`, `premium_6`, `food_no_foam_1`, `food_pax_1`, `food_no_foam_2`, `food_pax_2`, `food_no_foam_3`, `food_pax_3`, `food_no_foam_4`, `food_pax_4`, `food_no_foam_5`, `food_pax_5`, `waste_1`, `waste_2`, `waste_3`, `waste_4`, `waste_5`, `waste_6`, `wood_1`, `wood_2`, `wood_3`, `wood_4`, `wood_5`, `pp_1`, `pp_2`, `pp_3`, `pp_4`, `pp_5`, `pp_kg`, `building_a_1`, `building_a_2`, `building_a_3`, `building_a_4`, `building_b_1`, `building_b_2`, `building_b_3`, `building_b_4`, `building_b_5`, `building_b_6`, `building_b_7`, `building_b_8`, `building_c_1`, `led_bulb_watt_1`, `led_bulb_hour_1`, `led_bulb_watt_2`, `led_bulb_hour_2`, `led_bulb_watt_3`, `led_bulb_hour_3`, `led_bulb_watt_4`, `led_bulb_hour_4`, `led_bulb_watt_5`, `led_bulb_hour_5`, `led_bulb_watt_6`, `led_bulb_hour_6`, `led_bulb_watt_7`, `led_bulb_hour_7`, `led_bulb_watt_8`, `led_bulb_hour_8`, `led_bulb_watt_9`, `led_bulb_hour_9`, `led_bulb_watt_10`, `led_bulb_hour_10`, `led_bulb_watt_11`, `led_bulb_hour_11`, `led_bulb_custom_watt_1`, `led_bulb_custom_watt_2`, `led_bulb_custom_watt_3`, `led_bulb_custom_watt_4`, `led_bulb_custom_watt_5`, `led_bulb_custom_qty_1`, `led_bulb_custom_qty_2`, `led_bulb_custom_qty_3`, `led_bulb_custom_qty_4`, `led_bulb_custom_qty_5`, `led_bulb_custom_hour_1`, `led_bulb_custom_hour_2`, `led_bulb_custom_hour_3`, `led_bulb_custom_hour_4`, `led_bulb_custom_hour_5`, `led_tube_watt_1`, `led_tube_hour_1`, `led_tube_watt_2`, `led_tube_hour_2`, `led_tube_watt_3`, `led_tube_hour_3`, `led_tube_watt_4`, `led_tube_hour_4`, `led_tube_watt_5`, `led_tube_hour_5`, `led_tube_watt_6`, `led_tube_hour_6`, `led_tube_watt_7`, `led_tube_hour_7`, `led_tube_watt_8`, `led_tube_hour_8`, `led_tube_watt_9`, `led_tube_hour_9`, `led_tube_watt_10`, `led_tube_hour_10`, `led_tube_watt_11`, `led_tube_hour_11`, `led_tube_custom_watt_1`, `led_tube_custom_watt_2`, `led_tube_custom_watt_3`, `led_tube_custom_watt_4`, `led_tube_custom_watt_5`, `led_tube_custom_qty_1`, `led_tube_custom_qty_2`, `led_tube_custom_qty_3`, `led_tube_custom_qty_4`, `led_tube_custom_qty_5`, `led_tube_custom_hour_1`, `led_tube_custom_hour_2`, `led_tube_custom_hour_3`, `led_tube_custom_hour_4`, `led_tube_custom_hour_5`, `cf_care_1`, `cf_care_2`, `cf_care_3`, `cf_care_4`, `cf_care_5`, `cf_care_6`, `cf_care_total`, `view_count`, `status`, `created_on`, `updated_on`, `updated_by`) VALUES
(1, 1, 2, 'กิจกรรม 1', 'สถานที่ 1', '2020-07-24', '12:30:00', '15:30:00', 'เส้นทางของศาลานา สู่ความยั่งยืน \r\n1. Smart Ecosystem สร้างระบบนิเวศการเรียนรู้กระตุ้นจากภูมิปัญญาและวัฒนธรรมของชุมชนท้องถิ่น เพราะระบบนิเวศแห่งการเรียนรู้ที่มีศักยภาพนั้น มีพื้นฐานมาจากการพัฒนาองค์ความรู้สู่วิธีคิดอย่างมีระบบ และเป็นการเรียนรู้แบบบูรณาการ ศาลานาจึงได้พัฒนาองค์ความรู้สู่การอบรมเชิงปฏิบัติการคือ “การอบรมหลักสูตรศาลานา” \r\n2. Smart Innovation สร้างศูนย์ปฏิบัติการ ค้นคว้า ทดลอง และเชื่อมโยงองค์กรต่าง ๆ เพื่อพัฒนานวัตกรรม เพื่อพัฒนาองค์ความรู้ รวมทั้งสินค้าและบริการจากการเกษตรกรรมวิถีธรรมชาติ ผลักดันแนวทางในการวิจัยเพื่อการเกษตร ภายใต้ผลิตผลทางการเกษตร วถีธรรมชาติ เสริมมุมมองความคิดสู่นวัตกรรม เพื่อให้วงการการเกษตร มีการเติบโตทางด้านเศรษฐกิจควบคู่ไปกับการเรียนรู้ สู่การขยายตัวของตลาดผู้บริโภค \r\n3. Smart Farmer Community สร้างชุมชน เชื่อมโยงเครือข่ายนักธุรกิจการเกษตรกรรมวิถีธรรมชาติรุ่นใหม่ที่เข้มแข็ง โดยเป็นหนึ่งในผู้เชื่อมโยงเครือข่ายเกษตรกรให้เกิดความเข้มแข็งภายในกลุ่ม ให้มีความพร้อมสู่การขยายตัวของเกษตรกรรมวิถีธรรมชาติ \r\n4. Smart Farming Economy ร่วมสร้างระบบเศรษฐกิจใหม่จากฐานนวัตกรรมการเกษตรกรรมวิถีธรรมชาติ หนึ่งในหลักสูตรการอบรมที่สำคัญ คือการเสริมทักษะด้านการเป็นผู้ประกอบการให้แก่เกษตรกร ซึ่งถือเป็นองค์ประกอบปลายทางจากการมีฐานความรู้ และการจัดการกลุ่มที่เข้มแข็ง ในขณะเดียวกัน ศาลานาจะเป็นอีกหนึ่งกำลัง ในการเชื่อมโยงตลาดสู่ผู้บริโภค เป้าหมายเพื่อสร้างการขยายตัวของเกษตรกรรมวิถีธรรมชาติ และผู้ประกอบการรุ่นใหม่ สู่ผู้บริโภค \r\n5. Smart Farming Network / Movement ร่วมสร้างความพร้อมในการรวมกลุ่มสู่เครือข่าย และเป็นส่วนหนึ่งของเครือข่ายสู่การขับเคลื่อน ภายใต้การสร้างความร่วมมือ ระหว่างกลุ่มที่มีจิตวิญญาณและความมุ่งมั่นเดียวกันสู่ความยั่งยืน โดยเช่ือมโยงเครือข่าย ขยายแนวร่วมให้แนวทางเกษตรกรรมวิถีธรรมชาติ เป็นหนึ่งของแนวทางหลักในการผลิต และ บริโภค มิใช่เป็นเพียงกระแส นาจะเป็นอีกหนึ่งกำลัง ในการเชื่อมโยงตลาดสู่ผู้บริโภค เป้าหมายเพื่อสร้างการขยายตัวของเกษตรกรรมวิถีธรรมชาติ และผู้ประกอบการรุ่นใหม่ สู่ผู้บริโภค ', 'http://localhost/set_social_impact/images/upload/member/1/1/activity.png', 'http://localhost/set_social_impact/images/upload/member/1/1/home-article.png', 'http://localhost/set_social_impact/images/upload/member/1/1/home-article1.png', '<iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"920\" height=\"600\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>', 20, 10, 10, 0, 0, 0, 2.5, 'N', 0, 'N', 0, 'Y', 10, 'Y', 20, 'N', 0, 0.3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 5, 0, 0, 0, 0, 1.5, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 6, 0, 0, 0, 0, 1.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1.5, 0, 0, 0, 0, 30.1, 12.37, 1.05, 1.04, 16.95, 81.88, 143.39, 13, 'Y', '2020-07-24 02:06:58', '2022-03-14 02:41:16', '-');

-- --------------------------------------------------------

--
-- Table structure for table `activity_register`
--

DROP TABLE IF EXISTS `activity_register`;
CREATE TABLE IF NOT EXISTS `activity_register` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_id` bigint(20) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `people` int(11) NOT NULL,
  `km` double NOT NULL,
  `cf_total` double NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `people` (`people`),
  KEY `km` (`km`),
  KEY `created_on` (`created_on`),
  KEY `cf_total` (`cf_total`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_register`
--

INSERT INTO `activity_register` (`id`, `activity_id`, `vehicle_id`, `people`, `km`, `cf_total`, `created_on`) VALUES
(1, 1, 1, 3, 25, 6.08, '2020-07-24 23:21:58'),
(2, 1, 5, 0, 15, 24.02, '2020-07-24 23:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `activity_type`
--

DROP TABLE IF EXISTS `activity_type`;
CREATE TABLE IF NOT EXISTS `activity_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_type`
--

INSERT INTO `activity_type` (`id`, `name`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'ประชุม (Online)', 1, 'Y', '2022-03-13 23:26:13', 'sa', '2022-03-13 23:26:13', '-'),
(2, 'ประชุม (Onsite)', 2, 'Y', '2022-03-13 23:26:23', 'sa', '2022-03-13 23:26:23', '-'),
(3, 'อบรม สัมมนา (Online)', 3, 'Y', '2022-03-13 23:26:34', 'sa', '2022-03-13 23:26:34', '-'),
(4, 'อบรม สัมมนา (Onsite)', 4, 'Y', '2022-03-13 23:26:45', 'sa', '2022-03-13 23:26:45', '-'),
(5, 'ประชุมผู้ถือหุ้น (Onsite)', 5, 'Y', '2022-03-13 23:26:57', 'sa', '2022-03-13 23:26:57', '-'),
(6, 'ประชุม ผู้ถือหุ้น e-AGM', 6, 'Y', '2022-03-13 23:27:06', 'sa', '2022-03-13 23:27:06', '-'),
(7, 'นักวิเคราะห์พบผู้ลงทุน / Opportunity Day', 7, 'Y', '2022-03-13 23:27:19', 'sa', '2022-03-13 23:27:19', '-'),
(8, 'แถลงข่าว', 8, 'Y', '2022-03-13 23:27:30', 'sa', '2022-03-13 23:27:37', 'sa'),
(9, 'กิจกรรม CSR', 9, 'Y', '2022-03-13 23:27:49', 'sa', '2022-03-13 23:27:49', '-'),
(10, 'กิจกรรมการท่องเที่ยว', 10, 'Y', '2022-03-13 23:28:00', 'sa', '2022-03-13 23:28:00', '-'),
(11, 'กิจกรรมมอบรางวัล / Awards ', 11, 'Y', '2022-03-13 23:28:12', 'sa', '2022-03-13 23:28:12', '-');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `thumbnail`, `name`, `description`, `content`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Articles/activity.png', 'Nature Our Classroom', 'ด้วยความเชื่อเรื่องการมีส่วนร่วมของสาธารณะจะส่งผลสำคัญต่อการอนุรักษ์ทรัพยากรธรรมชาติในระยะยาว EEC จึงถือกำเนิดขึ้นจากการคิดค้น การวิเคราะห์ และสร้างรูปแบบกิจกรรมร่วมสมัยที่เหมาะสมกับการนำพาสังคมไปสู่กระบวนการส่ิงแวดล้อมศึกษา ที่จะส่งผลต่อการรับรู้ ความเข้าใจ และก่อให้เกิดแรงบันดาลใจของผู้คนในการมีส่วนร่วมในการปกป้องทรัพยากรทางธรรมชาติ และการใช้ประโยชน์อย่างยั่งยืน ดังประโยครณรงค์หลักที่ว่า LET NATURE BE OUR CLASSROOM “ให้ธรรมชาติเป็นห้องเรียนของเรา”\r\n\r\nENVIRONMENTAL EDUCATION CENTRE THAILAND (EEC THAILAND) เป็นองค์กรที่ดำเนินงานด้านทางกระบวนการการจัดค่าย พาเด็กๆ', '<p><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-cover.png\" /></p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"row\">\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n</div>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"text-center\">\r\n<h2 class=\"title\">วิดีโอ</h2>\r\n<iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe></div>', 'Y', '2020-06-22 02:27:45', 'sa', '2020-07-24 23:45:23', 'sa');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `image`, `name`, `url`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Banner/banner.jpg', 'Banner 1', '', 1, 'Y', '2020-07-23 14:10:18', 'sa', '2020-07-23 14:10:18', '-'),
(2, 'http://localhost/set_social_impact/images/upload/editor/source/Banner/banner.jpg', 'Banner 2', '', 2, 'Y', '2020-07-23 14:10:28', 'sa', '2020-07-23 14:11:57', 'sa'),
(3, 'http://localhost/set_social_impact/images/upload/editor/source/Banner/banner.jpg', 'Banner 3', 'https://www.carethebear.com/', 3, 'Y', '2020-07-23 14:12:24', 'sa', '2020-07-23 14:12:24', '-');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `logo`, `name`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Company/home-logo2.png', 'Company 1', 1, 'Y', '2020-06-22 03:12:52', 'sa', '2020-06-22 03:14:11', 'sa'),
(2, 'http://localhost/set_social_impact/images/upload/editor/source/Company/home-logo2.png', 'Company 2', 2, 'Y', '2020-07-24 23:46:01', 'sa', '2020-07-24 23:46:01', '-');

-- --------------------------------------------------------

--
-- Table structure for table `company_group`
--

DROP TABLE IF EXISTS `company_group`;
CREATE TABLE IF NOT EXISTS `company_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_type_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`),
  KEY `status` (`status`),
  KEY `name` (`name`),
  KEY `company_type_id` (`company_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_group`
--

INSERT INTO `company_group` (`id`, `company_type_id`, `name`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 2, 'เกษตรและอุตสาหกรรมอาหาร', 1, 'Y', '2022-03-13 23:41:28', 'sa', '2022-03-13 23:41:28', '-'),
(2, 2, 'สินค้าอุปโภคบริโภค', 2, 'Y', '2022-03-13 23:42:42', 'sa', '2022-03-13 23:42:42', '-'),
(3, 2, 'ธุรกิจการเงิน', 3, 'Y', '2022-03-13 23:42:55', 'sa', '2022-03-13 23:42:55', '-'),
(4, 2, 'สินค้าอุตสาหกรรม', 4, 'Y', '2022-03-13 23:43:07', 'sa', '2022-03-13 23:43:07', '-'),
(5, 2, 'อสังหาริมทรัพย์และก่อสร้าง', 5, 'Y', '2022-03-13 23:43:21', 'sa', '2022-03-13 23:43:26', 'sa'),
(6, 2, 'ทรัพยากร', 6, 'Y', '2022-03-13 23:43:38', 'sa', '2022-03-13 23:43:38', '-'),
(7, 2, 'บริการ', 7, 'Y', '2022-03-13 23:43:49', 'sa', '2022-03-13 23:43:49', '-'),
(8, 2, 'เทคโนโลยี', 8, 'Y', '2022-03-13 23:44:04', 'sa', '2022-03-13 23:44:04', '-'),
(9, 3, 'เกษตรและอุตสาหกรรมอาหาร', 1, 'Y', '2022-03-13 23:41:28', 'sa', '2022-03-13 23:41:28', '-'),
(10, 3, 'สินค้าอุปโภคบริโภค', 2, 'Y', '2022-03-13 23:42:42', 'sa', '2022-03-13 23:42:42', '-'),
(11, 3, 'ธุรกิจการเงิน', 3, 'Y', '2022-03-13 23:42:55', 'sa', '2022-03-13 23:42:55', '-'),
(12, 3, 'สินค้าอุตสาหกรรม', 4, 'Y', '2022-03-13 23:43:07', 'sa', '2022-03-13 23:43:07', '-'),
(13, 3, 'อสังหาริมทรัพย์และก่อสร้าง', 5, 'Y', '2022-03-13 23:43:21', 'sa', '2022-03-13 23:43:26', 'sa'),
(14, 3, 'ทรัพยากร', 6, 'Y', '2022-03-13 23:43:38', 'sa', '2022-03-13 23:43:38', '-'),
(15, 3, 'บริการ', 7, 'Y', '2022-03-13 23:43:49', 'sa', '2022-03-13 23:43:49', '-'),
(16, 3, 'เทคโนโลยี', 8, 'Y', '2022-03-13 23:44:04', 'sa', '2022-03-13 23:44:04', '-'),
(17, 4, 'เกษตรและอุตสาหกรรมอาหาร', 1, 'Y', '2022-03-13 23:41:28', 'sa', '2022-03-13 23:41:28', '-'),
(18, 4, 'สินค้าอุปโภคบริโภค', 2, 'Y', '2022-03-13 23:42:42', 'sa', '2022-03-13 23:42:42', '-'),
(19, 4, 'ธุรกิจการเงิน', 3, 'Y', '2022-03-13 23:42:55', 'sa', '2022-03-13 23:42:55', '-'),
(20, 4, 'สินค้าอุตสาหกรรม', 4, 'Y', '2022-03-13 23:43:07', 'sa', '2022-03-13 23:43:07', '-'),
(21, 4, 'อสังหาริมทรัพย์และก่อสร้าง', 5, 'Y', '2022-03-13 23:43:21', 'sa', '2022-03-13 23:43:26', 'sa'),
(22, 4, 'ทรัพยากร', 6, 'Y', '2022-03-13 23:43:38', 'sa', '2022-03-13 23:43:38', '-'),
(23, 4, 'บริการ', 7, 'Y', '2022-03-13 23:43:49', 'sa', '2022-03-13 23:43:49', '-'),
(24, 4, 'เทคโนโลยี', 8, 'Y', '2022-03-13 23:44:04', 'sa', '2022-03-13 23:44:04', '-'),
(25, 7, 'โรงเรียน', 1, 'Y', '2022-03-13 23:47:14', 'sa', '2022-03-13 23:47:14', '-'),
(26, 7, 'มหาวิทยาลัย', 2, 'Y', '2022-03-13 23:47:27', 'sa', '2022-03-13 23:47:27', '-');

-- --------------------------------------------------------

--
-- Table structure for table `company_type`
--

DROP TABLE IF EXISTS `company_type`;
CREATE TABLE IF NOT EXISTS `company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_type`
--

INSERT INTO `company_type` (`id`, `name`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'ตลาดหลักทรัพย์แห่งประเทศไทย', 1, 'Y', '2022-03-13 23:21:36', 'sa', '2022-03-13 23:21:36', '-'),
(2, 'บริษัทจดทะเบียนในตลาดหลักทรัพย์แห่งประเทศไทย', 2, 'Y', '2022-03-13 23:21:53', 'sa', '2022-03-13 23:21:53', '-'),
(3, 'บริษัทจดทะเบียนในตลาดหลักทรัพย์ เอ็ม เอ ไอ', 3, 'Y', '2022-03-13 23:22:09', 'sa', '2022-03-13 23:22:09', '-'),
(4, 'บริษัทเอกชนที่ไม่ได้จดทะเบียนในตลาดหลักทรัพย์ฯ  ', 4, 'Y', '2022-03-13 23:22:24', 'sa', '2022-03-13 23:22:24', '-'),
(5, 'หน่วยงานภาครัฐ', 5, 'Y', '2022-03-13 23:22:38', 'sa', '2022-03-13 23:22:38', '-'),
(6, 'สมาคม / มูลนิธิ', 6, 'Y', '2022-03-13 23:22:50', 'sa', '2022-03-13 23:22:50', '-'),
(7, 'สถาบันการศึกษา', 7, 'Y', '2022-03-13 23:23:03', 'sa', '2022-03-13 23:23:03', '-'),
(8, 'สถานที่จัดการประชุม นิทรรศการ', 8, 'Y', '2022-03-13 23:23:14', 'sa', '2022-03-13 23:23:14', '-'),
(9, 'Social Enterprise (SE)', 9, 'Y', '2022-03-13 23:23:25', 'sa', '2022-03-13 23:23:25', '-');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `code` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `code`, `name`, `content`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'home', 'โครงการ Care the Bear', '<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนปปปปปปปปปปป ในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ปปปปปปปปป ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือ ปปปปปปป carbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; ปปปปปปป และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก carbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; ปปปปปปป และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก carbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo;</p>', 'Y', '2020-06-22 02:45:17', 'sa', '2020-06-22 02:45:23', 'sa'),
(2, 'about', 'โครงการ Care the Bear', '<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียน ในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือ carbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก <br /><br /> ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียน ในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือ carbon footprint จากกิจกรรมดังกล่าว ผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับ ก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วม ในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็น มิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือ carbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวน ต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก กลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือ <br /><br /></p>', 'Y', '2020-06-22 02:46:54', 'sa', '2020-06-22 02:46:54', '-'),
(4, 'care_1', 'การเดินทางที่เป็นมิตรกับสิ่งแวดล้อม', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care1.png\" alt=\"home-care1\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:51:10', 'sa', '2020-07-25 01:07:32', 'sa'),
(5, 'care_2', 'การลดใช้กระดาษและพลาสติก', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care2.png\" alt=\"home-care2\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:51:50', 'sa', '2020-07-25 01:07:55', 'sa'),
(6, 'care_3', 'งดการใช้โฟม', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care3.png\" alt=\"home-care3\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:52:13', 'sa', '2020-07-25 01:08:02', 'sa'),
(7, 'care_4', 'ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care1.png\" alt=\"home-care1\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:53:32', 'sa', '2020-07-25 01:08:10', 'sa'),
(8, 'care_5', 'ลดการเกิดขยะตักอาหารแต่พอดีและทานให้หมด', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care2.png\" alt=\"home-care2\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:54:26', 'sa', '2020-07-25 01:08:18', 'sa'),
(9, 'care_6', 'เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่ได้', '<p><img class=\"img-responsive\" src=\"http://localhost/set_social_impact/images/upload/editor/source/6_Care/home-care3.png\" alt=\"home-care3\" /></p>\r\n<p>&nbsp;</p>', 'Y', '2020-06-22 02:54:52', 'sa', '2020-07-25 01:08:29', 'sa'),
(3, 'about_vdo', 'รู้จัก โครงการ Care the Bear', '<p><iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"920\" height=\"600\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe></p>', 'Y', '2020-06-22 02:54:52', 'sa', '2020-07-23 14:29:02', 'sa'),
(10, 'term', 'ข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์', '', 'Y', '2022-03-24 16:37:36', 'sa', '2022-03-24 16:37:36', '-'),
(11, 'privacy', 'การคุ้มครองข้อมูลส่วนบุคคล', '', 'Y', '2022-03-24 16:38:51', 'sa', '2022-03-24 16:38:51', '-'),
(12, 'policy', 'นโยบายการใช้คุกกี้', '', 'Y', '2022-03-24 16:39:15', 'sa', '2022-03-24 16:39:15', '-');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_on` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `order_on` (`order_on`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `file`, `name`, `order_on`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Document/reportdownload.docx', 'Report Format', 1, 'Y', '2020-06-22 03:21:38', 'sa', '2020-06-22 03:21:38', '-');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `company` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `company_type_id` int(11) NOT NULL DEFAULT 0,
  `company_group_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `tel` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `is_set_staff` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `salt` varchar(100) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'Y = Yes, N = No',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `password` (`password`),
  KEY `created_on` (`created_on`),
  KEY `updated_on` (`updated_on`),
  KEY `last_login` (`last_login`),
  KEY `status` (`status`),
  KEY `salt` (`salt`),
  KEY `updated_by` (`updated_by`),
  KEY `company` (`company`),
  KEY `name` (`name`),
  KEY `position` (`position`),
  KEY `department` (`department`),
  KEY `tel` (`tel`),
  KEY `mobile` (`mobile`),
  KEY `is_set_staff` (`is_set_staff`),
  KEY `company_type_id` (`company_type_id`),
  KEY `company_group_id` (`company_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `email`, `username`, `password`, `company`, `logo`, `company_type_id`, `company_group_id`, `name`, `position`, `department`, `address`, `tel`, `mobile`, `is_set_staff`, `salt`, `status`, `created_on`, `updated_on`, `updated_by`, `last_login`) VALUES
(1, 'p-o-p-u-p@hotmail.com', 'popup', 'e10adc3949ba59abbe56e057f20f883e', 'ตลาดหลักทรัพย์แห่งประเทศไทย', 'http://localhost/set_social_impact/images/upload/member/1/1/crop_set-logo@2x.png', 2, 3, 'ชื่อ นามสกุล', 'ตำแหน่ง', 'ฝ่ายงาน', '93 ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพมหานคร 10400', '021234567', '0812345678', 'Y', '', 'Y', '2020-07-23 21:44:46', '2022-03-14 01:26:24', '-', '2022-03-21 23:21:36');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `thumbnail`, `name`, `description`, `content`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Articles/activity.png', 'Nature Our Classroom', 'ด้วยความเชื่อเรื่องการมีส่วนร่วมของสาธารณะจะส่งผลสำคัญต่อการอนุรักษ์ทรัพยากรธรรมชาติในระยะยาว EEC จึงถือกำเนิดขึ้นจากการคิดค้น การวิเคราะห์ และสร้างรูปแบบกิจกรรมร่วมสมัยที่เหมาะสมกับการนำพาสังคมไปสู่กระบวนการส่ิงแวดล้อมศึกษา ที่จะส่งผลต่อการรับรู้ ความเข้าใจ และก่อให้เกิดแรงบันดาลใจของผู้คนในการมีส่วนร่วมในการปกป้องทรัพยากรทางธรรมชาติ และการใช้ประโยชน์อย่างยั่งยืน ดังประโยครณรงค์หลักที่ว่า LET NATURE BE OUR CLASSROOM “ให้ธรรมชาติเป็นห้องเรียนของเรา”\r\n\r\nENVIRONMENTAL EDUCATION CENTRE THAILAND (EEC THAILAND) เป็นองค์กรที่ดำเนินงานด้านทางกระบวนการการจัดค่าย พาเด็กๆ', '<p><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-cover.png\" /></p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"row\">\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n</div>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"text-center\">\r\n<h2 class=\"title\">วิดีโอ</h2>\r\n<iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\" data-mce-fragment=\"1\"></iframe></div>', 'Y', '2022-03-14 03:21:28', 'sa', '2022-03-14 03:21:28', '-');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_on` time NOT NULL,
  `end_on` time NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vdo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attendant` int(11) NOT NULL,
  `is_care_1` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `is_care_2` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `is_care_3` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_2_1` int(11) NOT NULL,
  `care_2_2` double NOT NULL,
  `care_3_1` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_1_1` int(11) NOT NULL,
  `care_3_1_2` int(11) NOT NULL,
  `care_3_1_3` int(11) NOT NULL,
  `care_3_1_4` int(11) NOT NULL,
  `care_3_1_5` int(11) NOT NULL,
  `care_3_1_6` int(11) NOT NULL,
  `care_3_2` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_2_1` int(11) NOT NULL,
  `care_3_2_2` int(11) NOT NULL,
  `care_3_2_3` int(11) NOT NULL,
  `care_3_2_4` int(11) NOT NULL,
  `care_3_2_5` int(11) NOT NULL,
  `care_3_2_6` int(11) NOT NULL,
  `care_3_3` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_3_1` int(11) NOT NULL,
  `care_3_3_2` int(11) NOT NULL,
  `care_3_3_3` int(11) NOT NULL,
  `care_3_3_4` int(11) NOT NULL,
  `care_3_3_5` int(11) NOT NULL,
  `care_3_3_6` int(11) NOT NULL,
  `care_3_4` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_4_1` int(11) NOT NULL,
  `care_3_4_2` int(11) NOT NULL,
  `care_3_4_3` int(11) NOT NULL,
  `care_3_4_4` int(11) NOT NULL,
  `care_3_4_5` int(11) NOT NULL,
  `care_3_4_6` int(11) NOT NULL,
  `care_3_5` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_5_1` int(11) NOT NULL,
  `care_3_5_2` int(11) NOT NULL,
  `care_3_5_3` int(11) NOT NULL,
  `care_3_5_4` int(11) NOT NULL,
  `care_3_5_5` int(11) NOT NULL,
  `care_3_5_6` int(11) NOT NULL,
  `care_3_6` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No',
  `care_3_6_1` int(11) NOT NULL,
  `care_3_6_2` int(11) NOT NULL,
  `care_3_6_3` int(11) NOT NULL,
  `care_3_6_4` int(11) NOT NULL,
  `care_3_6_5` int(11) NOT NULL,
  `care_3_6_6` int(11) NOT NULL,
  `cf_care_1` double NOT NULL,
  `cf_care_2` double NOT NULL,
  `cf_care_3` double NOT NULL,
  `cf_care_total` double NOT NULL,
  `view_count` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  KEY `name` (`name`),
  KEY `place` (`place`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `start_on` (`start_on`),
  KEY `end_on` (`end_on`),
  KEY `attendant` (`attendant`),
  KEY `is_care_1` (`is_care_1`),
  KEY `is_care_2` (`is_care_2`),
  KEY `is_care_3` (`is_care_3`),
  KEY `view_count` (`view_count`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `member_id`, `name`, `place`, `start_date`, `end_date`, `start_on`, `end_on`, `description`, `image_1`, `image_2`, `image_3`, `vdo`, `attendant`, `is_care_1`, `is_care_2`, `is_care_3`, `care_2_1`, `care_2_2`, `care_3_1`, `care_3_1_1`, `care_3_1_2`, `care_3_1_3`, `care_3_1_4`, `care_3_1_5`, `care_3_1_6`, `care_3_2`, `care_3_2_1`, `care_3_2_2`, `care_3_2_3`, `care_3_2_4`, `care_3_2_5`, `care_3_2_6`, `care_3_3`, `care_3_3_1`, `care_3_3_2`, `care_3_3_3`, `care_3_3_4`, `care_3_3_5`, `care_3_3_6`, `care_3_4`, `care_3_4_1`, `care_3_4_2`, `care_3_4_3`, `care_3_4_4`, `care_3_4_5`, `care_3_4_6`, `care_3_5`, `care_3_5_1`, `care_3_5_2`, `care_3_5_3`, `care_3_5_4`, `care_3_5_5`, `care_3_5_6`, `care_3_6`, `care_3_6_1`, `care_3_6_2`, `care_3_6_3`, `care_3_6_4`, `care_3_6_5`, `care_3_6_6`, `cf_care_1`, `cf_care_2`, `cf_care_3`, `cf_care_total`, `view_count`, `status`, `created_on`, `updated_on`, `updated_by`) VALUES
(1, 1, 'โครงการ 1', 'สถานที่ 1', '2022-03-16', '2022-04-15', '08:00:00', '17:30:00', 'เส้นทางของศาลานา สู่ความยั่งยืน \r\n1. Smart Ecosystem สร้างระบบนิเวศการเรียนรู้กระตุ้นจากภูมิปัญญาและวัฒนธรรมของชุมชนท้องถิ่น เพราะระบบนิเวศแห่งการเรียนรู้ที่มีศักยภาพนั้น มีพื้นฐานมาจากการพัฒนาองค์ความรู้สู่วิธีคิดอย่างมีระบบ และเป็นการเรียนรู้แบบบูรณาการ ศาลานาจึงได้พัฒนาองค์ความรู้สู่การอบรมเชิงปฏิบัติการคือ “การอบรมหลักสูตรศาลานา” \r\n2. Smart Innovation สร้างศูนย์ปฏิบัติการ ค้นคว้า ทดลอง และเชื่อมโยงองค์กรต่าง ๆ เพื่อพัฒนานวัตกรรม เพื่อพัฒนาองค์ความรู้ รวมทั้งสินค้าและบริการจากการเกษตรกรรมวิถีธรรมชาติ ผลักดันแนวทางในการวิจัยเพื่อการเกษตร ภายใต้ผลิตผลทางการเกษตร วถีธรรมชาติ เสริมมุมมองความคิดสู่นวัตกรรม เพื่อให้วงการการเกษตร มีการเติบโตทางด้านเศรษฐกิจควบคู่ไปกับการเรียนรู้ สู่การขยายตัวของตลาดผู้บริโภค \r\n3. Smart Farmer Community สร้างชุมชน เชื่อมโยงเครือข่ายนักธุรกิจการเกษตรกรรมวิถีธรรมชาติรุ่นใหม่ที่เข้มแข็ง โดยเป็นหนึ่งในผู้เชื่อมโยงเครือข่ายเกษตรกรให้เกิดความเข้มแข็งภายในกลุ่ม ให้มีความพร้อมสู่การขยายตัวของเกษตรกรรมวิถีธรรมชาติ \r\n4. Smart Farming Economy ร่วมสร้างระบบเศรษฐกิจใหม่จากฐานนวัตกรรมการเกษตรกรรมวิถีธรรมชาติ หนึ่งในหลักสูตรการอบรมที่สำคัญ คือการเสริมทักษะด้านการเป็นผู้ประกอบการให้แก่เกษตรกร ซึ่งถือเป็นองค์ประกอบปลายทางจากการมีฐานความรู้ และการจัดการกลุ่มที่เข้มแข็ง ในขณะเดียวกัน ศาลานาจะเป็นอีกหนึ่งกำลัง ในการเชื่อมโยงตลาดสู่ผู้บริโภค เป้าหมายเพื่อสร้างการขยายตัวของเกษตรกรรมวิถีธรรมชาติ และผู้ประกอบการรุ่นใหม่ สู่ผู้บริโภค \r\n5. Smart Farming Network / Movement ร่วมสร้างความพร้อมในการรวมกลุ่มสู่เครือข่าย และเป็นส่วนหนึ่งของเครือข่ายสู่การขับเคลื่อน ภายใต้การสร้างความร่วมมือ ระหว่างกลุ่มที่มีจิตวิญญาณและความมุ่งมั่นเดียวกันสู่ความยั่งยืน โดยเช่ือมโยงเครือข่าย ขยายแนวร่วมให้แนวทางเกษตรกรรมวิถีธรรมชาติ เป็นหนึ่งของแนวทางหลักในการผลิต และ บริโภค มิใช่เป็นเพียงกระแส นาจะเป็นอีกหนึ่งกำลัง ในการเชื่อมโยงตลาดสู่ผู้บริโภค เป้าหมายเพื่อสร้างการขยายตัวของเกษตรกรรมวิถีธรรมชาติ และผู้ประกอบการรุ่นใหม่ สู่ผู้บริโภค ', 'http://localhost/set_social_impact/images/upload/member/1/1/activity1.png', 'http://localhost/set_social_impact/images/upload/member/1/1/home-article2.png', 'http://localhost/set_social_impact/images/upload/member/1/1/home-article3.png', '<iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"920\" height=\"600\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>', 200, 'Y', 'Y', 'Y', 200, 1.5, 'Y', 20, 0, 30, 0, 50, 0, 'N', 0, 0, 0, 0, 0, 0, 'Y', 0, 10, 0, 20, 0, 30, 'N', 0, 0, 0, 0, 0, 0, 'Y', 5, 0, 10, 0, 15, 0, 'N', 0, 0, 0, 0, 0, 0, 0, 12.62, 5.51, 18.13, 0, 'Y', '2022-03-22 00:39:19', '2022-03-22 00:39:19', '-');

-- --------------------------------------------------------

--
-- Table structure for table `project_register`
--

DROP TABLE IF EXISTS `project_register`;
CREATE TABLE IF NOT EXISTS `project_register` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `project_id` bigint(20) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `people` int(11) NOT NULL,
  `km` double NOT NULL,
  `cf_total` double NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_id` (`vehicle_id`),
  KEY `people` (`people`),
  KEY `km` (`km`),
  KEY `created_on` (`created_on`),
  KEY `cf_total` (`cf_total`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `priority` (`priority`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `priority`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'Developer', 1, 'Y', '2019-03-26 22:33:44', '-', '2019-03-26 22:33:44', '-'),
(2, 'Super Administrator', 2, 'Y', '2019-03-26 22:33:44', '-', '2019-03-26 22:33:44', '-'),
(3, 'Administrator', 3, 'Y', '2019-03-26 22:33:44', 'sa', '2019-03-26 22:33:44', 'sa');

-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

DROP TABLE IF EXISTS `tips`;
CREATE TABLE IF NOT EXISTS `tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tips`
--

INSERT INTO `tips` (`id`, `thumbnail`, `name`, `content`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'http://localhost/set_social_impact/images/upload/editor/source/Tips/400x400.png', 'Nature Our Classroom', '<p><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-cover.png\" /></p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"row\">\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n<div class=\"col-sm-6\"><img src=\"http://localhost/set_social_impact/images/theme/default/assets/images/article-img.png\" /></div>\r\n</div>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<p>ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก ร่วมสานพลังกลุ่มธุรกิจ และภาคีพันธมิตร เพื่อส่งเสริมให้บริษัทที่จดทะเบียนในตลาดหลักทรัพย์ มีส่วนร่วมในการแก้ไขปัญหาการเปลี่ยนแปลงสภาพภูมิอากาศ ด้วยการลดการปล่อยก๊าซเรือนกระจก จากการจัดประชุม สัมมนาที่เป็นมิตรกับสิ่งแวดล้อม &ldquo;Eco-Event&rdquo; ด้วย 6 กิจกรรมง่ายๆ โดยสามารถคำนวนการลดก๊าซเรือนกระจก หรือcarbon footprint จากกิจกรรมดังกล่าวผ่านทาง web application &ldquo;Eco Event Kit&rdquo; และเปรียบเทียมปริมาณก๊าซเรือนกระจกที่ลดได้กับจำนวนต้นไม้ที่ใช้ในการดูดซับก๊าซเรือนกระจก</p>\r\n<div class=\"text-center\">\r\n<h2 class=\"title\">วิดีโอ</h2>\r\n<iframe src=\"https://www.youtube.com/embed/JCSht-50arA\" width=\"300\" height=\"150\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe></div>', 'Y', '2020-06-22 02:37:03', 'sa', '2020-07-24 23:45:34', 'sa');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Y = Yes, N = No, D = Delete',
  `created_on` datetime NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_on` datetime NOT NULL,
  `updated_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `role_id` (`role_id`),
  KEY `password` (`password`),
  KEY `first_name` (`name`),
  KEY `email` (`email`),
  KEY `created_on` (`created_on`),
  KEY `created_by` (`created_by`),
  KEY `updated_on` (`updated_on`),
  KEY `updated_by` (`updated_by`),
  KEY `salt` (`salt`),
  KEY `status` (`status`),
  KEY `telephone` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `username`, `password`, `name`, `email`, `telephone`, `salt`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 1, 'developer', '8b5b7e646b1cd9fb86e0bd3a07374898', 'Anusorn Sillapaphadung', 'this.developer@gmail.com', '', '', 'Y', '2019-03-26 22:33:44', '-', '2019-03-26 22:33:44', '-'),
(2, 2, 'sa', 'c3f3494b9ad07e1ae58c5442826fed29', 'Super Administrator', 'p-o-p-u-p@hotmail.com', '', '', 'Y', '2019-03-26 22:33:44', '-', '2019-03-26 22:33:44', '-'),
(3, 3, 'admin', 'c3f3494b9ad07e1ae58c5442826fed29', 'System Administrator', 'bepopau@gmail.com', '', '', 'Y', '2019-03-26 22:33:44', 'sa', '2019-03-26 22:33:44', 'admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

ALTER TABLE `project_register` ADD `day` INT NOT NULL DEFAULT '1' AFTER `people`; 
ALTER TABLE `project_register` ADD INDEX(`day`); 
