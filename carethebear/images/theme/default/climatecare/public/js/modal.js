function alertBox(opts) {
   var self = this;
   this.options = $.extend({}, new modal.defaultsOption(), opts);
   this.dimID = 'fulldim' + (new Date().getTime());
   var html = $('html');
   var fullModal = document.createElement("div");
   fullModal.id = this.dimID;
   fullModal.className = "fulldim " + this.options.class;

   var _elem = '';
   _elem += '<div class="wrap-modal-box">';
   _elem += '<div class="wrap-center-block">';
   _elem += '<div class="wrapper-modal-text">';
   if (this.options.innerIcon) {
      _elem += this.htmlIconClose();
   }
   _elem += '<div class="wrapper-modal-inner">';
   _elem += '<div class="modal-content font-face">' + this.options.content + '</div>';
   _elem += '</div>';
   _elem += '</div>';
   _elem += '</div>';
   _elem += '</div>';
   if (!this.options.innerIcon) {
      _elem += this.htmlIconClose();
   }
   fullModal.innerHTML = _elem;
   document.body.appendChild(fullModal);

   this.elem = $('#' + this.dimID);

   $('body').append(self.elem);
   this.options.onInit(self.elem);
   this.closeDim = function () {
      self.options.onBeforeClose();
      if (self.elem.el[0].classList.contains('active')) {
         if (html.find('.fulldim.active').length <= 1) {
            html.removeClass('add-dim');
         }
         self.elem.removeClass('active');
      }
      setTimeout(function () {
         $('#' + self.dimID).remove();
         self.options.onClose();
         setBodyUnscrollable(false);
      }, 700);
   };
   this.elem.find('.close-dim').el.forEach(function (v) {
      $(v).on('click', function () {
         self.options.onCloseBtnClick();
         self.closeDim();
      });
   })
   this.elem.on('click', function () {
      self.closeDim();
   });
   this.elem.find('.wrapper-modal-text').on('click', function (event) {
      event.stopPropagation();
   });

   this.show = function () {
      html.addClass('add-dim');
      self.elem.addClass('active');
      self.options.onOpen();
      setBodyUnscrollable(true);
   };
   if (self.options.autoShow === true) setTimeout(self.show, 100);

   // check ios
   var ua = window.navigator.userAgent;
   var iOS = !!ua.match(/iPad|iPhone|iPod/i);
   var webkit = !!ua.match(/WebKit/i);
   var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
   if (iOSSafari) {
      html.addClass('dim-ios');
   }

   var setBodyUnscrollable = function (value) {
      if (!html.hasClass('dim-ios')) return;
      if (window.bodyScrollLock) {
         var lock = bodyScrollLock[value === true ? 'disableBodyScroll' : 'enableBodyScroll'];
         lock(window);
      }
   }

   self.setOption = function (key, value) {
      self.options[key] = value;
   };
   return this;
};
alertBox.prototype.htmlIconClose = function () {
   var html = '';
   html += '<div class="close-btn close-dim f_light">'
               + '<span class="icon icon-cross"></span>'
               + '</div>';
   return html;
}

var modal = {
   defaultsOption: function () {
      return {
         content: 'Thank you!',
         class: '',
         autoShow: true,
         innerIcon: true,
         onInit: function () { },
         onOpen: function () { },
         onClose: function () { },
         onCloseBtnClick: function () { },
         onBeforeClose: function () { },
      }
   },
   alert: function (text, _class) {
      var _text = text || ''
      var _class = _class || '';
      return new alertBox({ content: _text, class: 'general-alert ' + _class });
   },
   header: function (header, title, closeText, _class) {
      var html = '';
      html += '<div class="wrap-header-popup">';
      html += '<div class="-header">' + header + '</div>';
      html += '<div class="-title">' + title + '</div>';
      html += '<div class="box-btn">';
      html += '<a class="btn-style main-btn disabled-user-select close-dim header-button">' + closeText + '</a>';
      html += '</div>';
      html += '</div>';
      return new alertBox({ content: html, class: 'header-alert ' + _class });
   },
   confirm: function (opts) {
      var opts = $.extend({}, {
         ok: 'ตกลง',
         cancel: 'ยกเลิก',
         title: '',
         desc: '',
         innerIcon: true,
         onOpen: function () { },
         onOk: function () { },
         onCancel: function () { },
      }, opts);
      var template = function () {
         var html = '';
         html +=
            '<div class="wrap-confirm-alert">' +
            (opts.title !== '' ? '<div class="confirm-title f_bold">' + opts.title + '</div>' : '') +
            (opts.desc !== '' ? '<div class="confirm-text">' + opts.desc + '</div>' : '') +
            '<div class="wrap-general-button">' +
            (opts.cancel !== '' ? '<div class="cancel-btn general-cancel-button close-dim btn-gray font-face-bold bg-hover"><span class="animate-cicle white"></span><div class="button_inner elem"><span class="button_text f_bold">' + opts.cancel + '</span></div></div>' : '') +
            (opts.ok !== '' ? '<div class="confirm-btn general-ok-button font-face-bold bg-hover"><span class="animate-cicle"></span><div class="button_inner elem"><span class="button_text f_bold white">' + opts.ok + '</span></div></div>' : '') +
            '</div>' +
            '</div>';
         return html;
      };
      var _default = new modal.defaultsOption();
      _default.content = template();
      _default.class = 'confirm-alert';
      var _onInit = typeof opts.onInit === 'function' ? opts.onInit : function () { };
      opts.onInit = function (elem) {
         _onInit(elem);
         if (typeof (opts.onOk) === 'function') {
            elem.find('.wrap-confirm-alert .general-ok-button').on('click', opts.onOk);
         }
         if (typeof (opts.onCancel) === 'function') {
            elem.find('.wrap-confirm-alert .general-cancel-button').on('click', opts.onCancel);
         }
      };
      var options = $.extend({}, _default, opts);
      return new alertBox(options);
   },
};
