window.Promise = 'Promise' in window ? window.Promise : '';

function addComma (n) {
    n += '';
    return n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}
// function serialize(obj, prefix) {
//     var str = [];
//     Object.entries(obj).forEach(([key, value], index) => {
//         str.push((value !== null && typeof value === 'object') ? 
//             serialize(value, key) : [(prefix != '') ? prefix : ''] + encodeURIComponent(key) + '=' + encodeURIComponent(value));
//       });
//     return str.join('&');
// }
function run_number(elm, duration = 1) {
    var interval = 0;
    var internalCount = 0;
    var internalTime = 100;
    var runningNo = 0;
    var no = $(elm).data('no') == undefined ? '0' : $(elm).data('no');
    var digit = no.split('.')[0];
    var decimal = no.split('.')[1] ? ('.' + no.split('.')[1]) : '';
    var useNo = +digit;
    
    var beginningNo = useNo-(useNo*0.9);
    
    interval = window.setInterval(() => {
        if (runningNo < useNo) {
            var replace_no = easeOutQuad(internalCount, beginningNo, useNo, duration);
            internalCount+=internalTime/1000;
            runningNo = Math.floor(replace_no);
            
            var msg = runningNo > useNo ? addComma(useNo)+decimal : addComma(runningNo).toString()+decimal;
            $(elm).html(msg);
        } else {
            clearInterval(interval);
        }
    }, internalTime);
}
function easeOutQuad (t, b, c, d) {
    /**
     * 
     * 
        t = Time - Amount of time that has passed since the beginning of the animation. Usually starts at 0 and is slowly increased using a game loop or other update function.
        b = Beginning value - The starting point of the animation. Usually it's a static value, you can start at 0 for example.
        c = Change in value - The amount of change needed to go from starting point to end point. It's also usually a static value.
        d = Duration - Amount of time the animation will take. Usually a static value aswell.
    */
    return -c * (t /= d) * (t - 2) + b;
}

// function inviewSpy() {
//     const target = $(document).find('[inviewspy]');
//     function handleIntersection(entries) {
//         entries.map((entry) => {
//             if (entry.isIntersecting) {
//                 var delay = 0;
//                 var spy = $(entry.target).attr('inviewspy');
//                 if (spy) {
//                     delay = parseInt(spy);
//                 }
//                 $(entry.target).removeAttr('inviewspy');
//                 $(entry.target).addClass('inviewspy');
//                 setTimeout(() => {
//                     $(entry.target).addClass('inviewed');
//                     entry.target.observed = undefined;
//                     entry.target.observer.unobserve(entry.target);
//                 }, delay);
//             }
//         });
//     }
//     target.el.forEach(el => {
//         if (el.observed == undefined) {
//             addObseverToElm(el, handleIntersection, {threshold: 0.5});
//             el.observer.observe(el);
//         }
//     });
// }
$.fn('inview', function(callback, threshold = {}, once = true) {
    if (typeof callback == 'function') {
        const target = this;
        function handleIntersection(entries) {
            entries.map((entry) => {
                if (entry.isIntersecting) {
                    callback(entry.target);
                    if (once === true) entry.target.observer.unobserve(entry.target);
                }
            });
        }
        target.el.forEach( el => {
            addObseverToElm(el, handleIntersection, threshold);
            el.observer.observe(el);
        });
    }
});
$.fn('outview', function(callback) {
    if (typeof callback == 'function') {
        const target = this;
        function handleIntersection(entries) {
            entries.map((entry) => {
                if (!entry.isIntersecting) {
                    callback(entry.target);
                }
            });
        }
        target.el.forEach( el => {
            addObseverToElm(el, handleIntersection);
            el.observer.observe(el);
        });
    }
});
$.fn('lazy', function() {
    this.inview(function(e) {
        $(e).attr('src', $(e).data('src'));
        $(e).removeAttr('data-src');
        $(e).addClass('img-loaded');
    });
});

$.fn('loading', function(cmd = '') {
    if (cmd == '') {
        this.addClass('loading');
    } else if (cmd == 'reset') {
        this.removeClass('loading');
    }
});

let observeArr = [];
function addObseverToElm(elm, fn, inviewOpt = { threshold: 0}) {
    elm.observed = true;
    elm.observer = new IntersectionObserver(fn, inviewOpt);
}
$(document).ready(function() {
    loadCSS(image_url + 'theme/default/climatecare/public/css/form.css');
    //loadJS(image_url + 'theme/default/climatecare/public/js/form.js');
    loadCSS(image_url + 'theme/default/climatecare/public/css/header.css');
    loadJS(image_url + 'theme/default/climatecare/public/js/header.js');
    loadCSS(image_url + 'theme/default/climatecare/public/css/footer.css');
    loadJS(image_url + 'theme/default/climatecare/public/js/footer.js');

    // inviewSpy();

    // $('.lazy-img:not(.img-loaded)').lazy();
});
$.fn('goToAnchor', function (duration, otherTop) {
    if (typeof (otherTop) == 'undefined') otherTop = $(".header").outerHeight();
    // duration = 0 or more than 60 not bug
    return $(window).scrollTop(this.offset().top - otherTop, duration);
});
function serialize(obj, prefix) {
    var str = [], p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + '[' + p + ']' : p,
                v = obj[p];
            str.push((v !== null && typeof v === 'object') ?
                serialize(v, k) :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
    }
    return str.join('&');
}
function companyDetailBundle() {
    loadCSS(image_url + 'theme/default/public/css/company-detail.css');
    loadJS(image_url + 'theme/default/public/js/company-detail.js');
}
function sidebarMenuBundle() {
    loadCSS(image_url + 'theme/default/public/css/sidebar-menu.css');
    loadJS(image_url + 'theme/default/public/js/sidebar-menu.js?v=1.0');
}