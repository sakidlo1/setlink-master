
let title = '';
function changeBG(cate) {
    switch (cate) {
        case 'bear':
            setTitle('<div class="t_light_blue">Care the Bear</div><p class="t_30">โครงการลดก๊าซเรือนกระจก <br class="show-xs">จากการจัดงานหรือกิจกรรมขององค์กร</p>');
            break;
        case 'whale':
            setTitle('<div class="t_blue">Care the Whale</div><p class="t_30">โครงการลดก๊าซเรือนกระจก <br class="show-xs">จากการบริหารจัดการขยะตั้งแต่ต้นทางถึงปลายทาง</p>');
            break;
        case 'elephant':
            setTitle('<div class="t_green">Care the Wild</div><p class="t_30">โครงการปลูกป่าดูดซับก๊าซเรือนกระจก <br class="show-xs">สร้างสมดุลให้ระบบนิเวศ</p>');
            break;
        case 'all':
            setTitle('<div class="t_yellow">Climate Care Collaboration</div><p class="t_30">รวมพลังบวกกู้วิกฤตโลกร้อน</p>');
            break;
    }
}
function setTitle(msg) {
    title = msg;
}
function popupFull() {
    var html = '';
    html +='<div class="popup_fullscreen flex center">';
        html +='<div class="wrap_close">';
            html += '<div class="close_btn XL">';
                html += '<span class="icon icon-cross"></span>';
            html += '</div>';
        html +='</div>';
        html +='<div class="popup_box">';
            html +='<div class="vdo-player"></div>';
        html +='</div>';
    html +='</div>';
    return html;
}

function vdoSlideHTML(data) {
    var html = '';
    html += '<div class="swiper-slide wrap_slide" data-cate="'+data.cate+'" data-video="'+data.video+'">';
        html += '<div class="">';
            html += '<img class="slide_img lazy-img" data-src="'+data.thumbImg+'" alt="'+data.thumbImgAlt+'">';
        html += '</div>';
    html += '</div>';
    return html;
}
function bgSlideHTML(data) {
    var html = '';
    html += '<div class="bg_wrapper">';
        html += '<img class="lazy-img slider_bg bear hide-xs" data-src="'+data.bgImg+'" alt="'+data.bgImgAlt+'" data-cate="bear">';
        html += '<img class="lazy-img slider_bg bear show-xs" data-src="'+data.bgImgMobile+'" alt="'+data.bgImgMobileAlt+'" data-cate="bear">';
    html += '</div>';  
    return html; 
}

$(document).ready(function() {
    let swiper = '';
    let swiper2 = '';
    let swiper3 = '';
    let currentMedia = '';
    function mb_Swiper() {
        swiper = new Swiper('.swiper1', {
            loop: true,
            spaceBetween: 17,
            navigation: {
              nextEl: '.arrow.next',
              prevEl: '.arrow.back',
            }
        });
    }

    function dt_Swiper() {
        swiper = new Swiper('.swiper1', {
            effect: 'creative',
            creativeEffect: {
                prev: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
                next: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
            },
            loop: true,
            speed: 400,
            slidesPerView: 1,
            allowTouchMove: false,
            navigation: {
                nextEl: '.arrow.next',
                prevEl: '.arrow.back',
            }
        });
        swiper2 = new Swiper('.swiper2', {
            effect: 'creative',
            creativeEffect: {
                prev: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
                next: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
            },
            loop: true,
            speed: 400,
            slidesPerView: 1,
            allowTouchMove: false,
            initialSlide: 1,
            navigation: {
                nextEl: '.arrow.next',
                prevEl: '.arrow.back',
            }
        });
        
        swiper3 = new Swiper('.swiper3', {
            effect: 'creative',
            creativeEffect: {
                prev: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
                next: {
                    opacity: 0,
                    translate: [0, 0, -400],
                },
            },
            loop: true,
            speed: 400,
            slidesPerView: 1,
            initialSlide: 2,
            allowTouchMove: false,
            navigation: {
                nextEl: '.arrow.next',
                prevEl: '.arrow.back',
            }
        });
    }

    function initSwiper() {
        function getCate() {
            return $(swiper.slides[swiper.activeIndex]).data('cate');
        }
        function getVDO() {
            return $(swiper.slides[swiper.activeIndex]).data('video');
        }
        function changeSectionData() {
            var cate = getCate();
            changeBG(cate);
            var activeBG = $('.section_vdo_slider .wrap_bg').find('.bg_wrapper').removeClass('active').get(swiper.realIndex);
            $(activeBG).addClass('active');
            $('.section_vdo_slider .section-title').html(title);
            $('.section_vdo_slider .char').el.forEach(char => {
                if ($(char).get(0).classList.contains(cate)) {
                    $(char).addClass('active');
                } else {
                    $(char).removeClass('active');
                }
            });
        }
        function activeClassSlider(sw) {
            if (sw.destroyed && sw.destroyed === true) return;
            if (window.innerWidth < 767) {
                setTimeout(() => {
                    for (let i = 0; i < sw.slides.length; i++) {
                        let checkIndex = sw.activeIndex;
                        if (sw.slides == undefined) return;
                        if (sw.slides[i].classList.contains('swiper-slide-active')) {
                            sw.slides[i].classList.add('active');
                            sw.slides[i].classList.remove('prev');
                            sw.slides[i].classList.remove('next');
                        } else {
                            if (i < checkIndex) {
                                sw.slides[i].classList.add('prev');
                                sw.slides[i].classList.remove('active');
                                sw.slides[i].classList.remove('next');
                            } else {
                                sw.slides[i].classList.add('next');
                                sw.slides[i].classList.remove('active');
                                sw.slides[i].classList.remove('prev');
                            }
                        }
                    }
                }, 4);
            }
        }
        function destroySlides() {
            if (swiper != '') {
                swiper.destroy();
            };
            if (swiper2 != '') {
                swiper2.destroy();
            };
            if (swiper3 != '') {
                swiper3.destroy();
            };
        }
        if (window.innerWidth < 767) {
            if (currentMedia != 'mobile') {
                currentMedia = 'mobile';
                destroySlides();
                mb_Swiper();
            }
        } else {
            if (currentMedia != 'desktop') {
                currentMedia = 'desktop';
                destroySlides();
                dt_Swiper();
            }
        }

        $(swiper.navigation.$nextEl).off('click').on('click', function() {
            swiper.slideNext();
            if (swiper2 != '' && swiper2.destroyed && swiper2.destroyed !== true) swiper2.slideNext();
            if (swiper3 != '' && swiper3.destroyed && swiper3.destroyed !== true) swiper3.slideNext();
        });
        $(swiper.navigation.$prevEl).off('click').on('click', function() {
            swiper.slidePrev();
            if (swiper2 != '' && swiper2.destroyed && swiper2.destroyed !== true) swiper2.slidePrev();
            if (swiper3 != '' && swiper3.destroyed && swiper3.destroyed !== true) swiper3.slidePrev();
        });
        swiper.on('slideChange', function() {
            changeSectionData();
            activeClassSlider(swiper);
            inviewSpy();
            $('.lazy-img:not(.img-loaded)').lazy();
        });
        swiper.slides.forEach(slide => {
            $(slide).off('click').on('click', function() {
                var body = $('body');
                body.append(popupFull());
                body.addClass('dim');
                $('.popup_fullscreen .wrap_close').on('click', function() {
                    $('.popup_fullscreen').remove();
                    body.removeClass('dim');
                });
                function sizingMedia() {
                    var fullVideoWrap = body.find('.vdo-player').get(0);
                    var fullvideo = body.find('.video-element').get(0);
                    var ratio = 0.563;
                    var height = fullVideoWrap.clientWidth * ratio;
                    if (height > fullVideoWrap.clientHeight) {
                        height = fullVideoWrap.clientHeight;
                        var width = height / ratio;
                        $(fullvideo).attr('style', 'width: '+(width/fullVideoWrap.clientWidth) * 100 +'%; height: 100%;');
                    } else {
                        $(fullvideo).attr('style', 'height: '+(height/fullVideoWrap.clientHeight) * 100 +'%; width: 100%;');
                    }
                }
                new Player($('.popup_fullscreen .vdo-player'), getVDO(), {autoplay: true,  controls: true});

                window.addEventListener('resize', sizingMedia);
                sizingMedia();
            });
        });
        changeSectionData();
        activeClassSlider(swiper);
    }

    // TODO: get data vdo
    new Promise(resolve => {
        var slideHTML = '';
        var bgHTML = '';
        vdo_slider_data.forEach( data => {
            slideHTML += vdoSlideHTML(data);
            bgHTML += bgSlideHTML(data);
        });
        resolve({slide: slideHTML, bg: bgHTML});
    }).then((html) => {
        $('.section_vdo_slider .swiper1 .wrap_slides').html(html.slide);
        $('.section_vdo_slider .wrap_bg').html(html.bg);
        $('.lazy-img').lazy();
        if (vdo_slider_data.length > 1) $('.section_vdo_slider .swiper2 .wrap_slides').html(html.slide);
        if (vdo_slider_data.length > 2) $('.section_vdo_slider .swiper3 .wrap_slides').html(html.slide);
        initSwiper();
        $(window).on('resize', initSwiper);
    });
});