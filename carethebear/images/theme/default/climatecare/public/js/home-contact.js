$(document).ready(function() {
    if (typeof main_contact == 'function') {
        $('.section-contact').html(main_contact(false,[
            {
                icon:'icon-mail',
                desc:'SocialDevelopment<br>Department@set.or.th',
                link:'mailto:SocialDevelopmentDepartment@set.or.th'
            },
            {
                icon:'icon-headset',
                desc:'02 009 9999',
                link:'tel:020099999'
            },
            {
                icon:'icon-world',
                desc:'climatecare.setsocialimpact.com',
                link:'https://climatecare.setsocialimpact.com'
            },
            {
                icon:'icon-facebook1',
                desc:'SET Social impact',
                link:'https://www.facebook.com/SETsocialimpactofficial'
            },
        
        ]))
    }
});