// data -> [{title:'text',value:'value',selected:false},{title:'text2',value:'value2',selected:true}]
var objSimpleDropdown = {};
function SimpleDropdown(elem,data,opts,search) {
    if (typeof elem.nojquery === 'undefined') {
        if (typeof elem === 'string') {
            var snipId = 'simple_dropdown_';
            var id = elem.replace(snipId,'');
            elem = '#' + snipId + id;
        }
        elem = $(elem);
    }
    if (typeof opts === 'undefined') opts = {};
    if (typeof opts === 'undefined') search = true;
    this.main = elem;
    this.main.addClass('simple-dropdown');
    this.input = this.main.find('.input-value');
    this.name = this.input.attr('name');
    this.id = this.main.attr('id');
    this.textShow = this.main.find('.dropdown-show-selected .-text');
    this.ul = this.main.find('ul');
    this.elmMainSearch = this.main.find('.dropdown-section');    
    this.inpSearch = this.elmMainSearch.find('.inp-dd-search');       
    this.options = $.extend({}, {
        hideOutClick : true
    }, opts);
    this.initData = false;
    this.haveClass = 'have-data';
    this.selectedClass = 'selected';
    this.disabledClass = 'disabled';
    this.readonlyClass = 'readonly';
    if (typeof data === 'undefined') {
        // init data
        data = [];
        this.ul.children().el.forEach(function(ele) {
            if ($(ele).length <= 0) return;
            data.push({
                title : $(ele).find('.-text').get(0).innerHTML.trim(),
                value : $(ele).data('value').trim(),
                selected : $(ele).hasClass(this.selectedClass),
                disabled: $(ele).hasClass(this.disabledClass),
                readonly: $(ele).get(0).getAttribute('data-readonly') !== void 0
            });
        }.bind(this));
        this.initData = true;
    }
    this.data = !Array.isArray(data) ? [data] : data;
    this.init();
    if (this.name) {
        objSimpleDropdown[this.id] = this;
    }
    this.findBoxDropdown = this.main.find(".dropdown-section");  
};
SimpleDropdown.prototype.init = function(){
    var self = this;
    this._open = false;
    if (!this.initData) this.ul.empty();

    this.currentValue = '';
    this.currentItem = '';
    this.data.forEach(function(data,index) {
        var selected = data.selected===true;
        if (!self.initData) {
            self.ul.append(self.template(data,selected));
        }
        var li = self.ul.children().eq(index);
        if (selected) {
            self.currentValue = data.value;
            self.main.addClass(self.haveClass);
            self.textShow.html(data.title);
            self.input.val(data.value); // @remark first select
        }
        self._addItemEvent(li);
    })
    this.recalculate();
    this.ul.on('click',function(e) {
        e.stopPropagation();
    });
    this.main.on('click',function(e) {        
        if (!self.isOpen()) {                                    
            self.show();
            // return;
            if(self.main.hasClass("set-position")){
                self.elmMainSearch.removeClass("bottom");
                if (self.findBoxDropdown.get(0).style.removeProperty) {
                    self.findBoxDropdown.get(0).style.removeProperty('top');
                } else {
                    self.findBoxDropdown.get(0).style.removeAttribute('top');
                }
                self.dynamicBox();           
            }
            self.initScroll();
        }
    });
    this.main.addClass('inited');
};
SimpleDropdown.prototype._addItemEvent = function(li){
    var self = this;
    li.on('click',function() {
        var value = $(this).data('value');
        self.setValue(value,true);
        self.hide();
    });
};
SimpleDropdown.prototype._trigger = function(event,data){
    this.main.trigger(event,[data]);
};
SimpleDropdown.prototype.template = function(data,selected){
    var classList = [
        (selected ? this.selectedClass : ''),
        (data.readonly ? this.readonlyClass : void 0)
    ].join(' ');
    return '<li class="flex v-center h-justify f_bold' + classList + '" data-value="'+ data.value +'" data-disabled="' + data.disable + '">'
                + '<span class="-text">'+ data.title +'</span>'
                + '<span class="icon-Check"></span>'
            + '</li>';
};
SimpleDropdown.prototype.add = function(data){
    var data = !Array.isArray(data) ? [data] : data;
    data.forEach(function(item) {
        var li = $(this.template(item,false));
        this.ul.append(li);
        this._addItemEvent(li);
    }.bind(this));
    this.data = this.data.concat(data);
    this.recalculate();
};
SimpleDropdown.prototype.show = function(){
    var self = this;     
    self.main.addClass('open');
    self.validdateEvent('open');
    self._open = true;
    if(self.inpSearch.length > 0){
        self.filterFunction();
    } 
    if (self.options.hideOutClick) {
        setTimeout(function() {
            var off = $(document).on('click.simpledropdown',function(event) {                
                if (!$(event.target).closest('.inp-dd-search').length > 0) {
                    off();
                    self.hide();
                }
            });
        },10);
    }
    self._trigger('show');
};
SimpleDropdown.prototype.initScroll = function(){
    var self = this;
    var li = self.ul.find("li");
    $(document).on('keyup.filterOnKey',typing);  
    function typing(e, alreadyChecked) {
        var val = "";
        if (e.keyCode === 8 && val.length > 1) {
            val = val.split('').splice(0, val.length - 1).join('');
        } else if (e.key && e.key.length === 1) {
            val += e.key.toLowerCase();
        } else {
            return;
        }
        if (val.length === 0) {
            return;
        }
        var els = li.el.filter(function (idx) {
            var indexOfFirst = idx.innerText;                
            return indexOfFirst.toLowerCase().indexOf(val) === 0;
        });
        if (!els || els.length === 0) {
            val = '';
            if (alreadyChecked) {
                typing(e, true);
            }
            return;
        }  
        self.ul.get(0).scrollTop = $(els[0]).position().top - $(els[0]).height();        
    }
};
SimpleDropdown.prototype.dynamicBox = function(){
    var self = this;
    var heightScreen = document.documentElement.clientHeight;
    var elm = self.main.find(".dropdown-section").get(0).getBoundingClientRect();
    var getCurrentBottom = elm.bottom;
    var getCurrentHeight = - (elm.height + 10);
    if(getCurrentBottom > heightScreen){
        self.elmMainSearch.addClass("bottom");
        self.findBoxDropdown.get(0).style.top = getCurrentHeight + "px";        
    }else{
        //remove inline attr style
        self.elmMainSearch.removeClass("bottom");
        if (self.findBoxDropdown.get(0).style.removeProperty) {
            self.findBoxDropdown.get(0).style.removeProperty('top');
        } else {
            self.findBoxDropdown.get(0).style.removeAttribute('top');
        }
    }    
}
SimpleDropdown.prototype.hide = function(){
    var self = this;
    this.main.removeClass('open');
    this.validdateEvent('close');
    this._open = false;
    $(document).off('click.simpledropdown');
    $(document).off('keyup.filterOnKey');  
    this._trigger('hide');
    this.main.parents('.wrap-input-text').removeClass('focus');    
};
SimpleDropdown.prototype.isOpen = function(){
    return this._open;
};
SimpleDropdown.prototype.value = function(value){
    if (typeof value === 'undefined') { // get
        return this.getValue();
    } else {
        this.setValue(value);
    }
};
SimpleDropdown.prototype.getValue = function(){
    return this.currentValue;
}
SimpleDropdown.prototype.setValue = function(value,triggerEvent){
    if (typeof triggerEvent === 'undefined') triggerEvent = false;
    var index = -1;
    if (typeof value === 'number') {
        var filter = [this.data[value]];
        index = value;
    } else {
        var filter = this.data.filter(function(item,_index){
            var _return = item.value == value;
            if (_return) {
                index = _index;
            }
            return _return;
        });
    }
    if (filter.length > 0 && index != -1) {
        var title = filter[0].title;
        this.currentValue = filter[0].value;
        this.input.val(filter[0].value); // @remark init first value
        this.currentItem = filter[0];
        delete this.currentItem['selected'];
        this.main.addClass(this.haveClass);
        this.textShow.html(title.toString());
        this.item.removeClass(this.selectedClass);
        this.item.eq(index).addClass(this.selectedClass);
        if (triggerEvent === true) {
            this._trigger('change',filter[0]);
            this.main.parents('.wrap-input-text').removeClass('focus');
        }
    }
};
SimpleDropdown.prototype.getCurrentData = function(){
    return this.currentItem;
}
SimpleDropdown.prototype.recalculate = function(){
    this.item = this.ul.children();
}
SimpleDropdown.prototype.remove = function(){
    this.ul.empty();
    this.data = [];
    this.recalculate();
}
SimpleDropdown.prototype.clear = function(){
    this.currentValue = '';
    this.main.removeClass(this.haveClass);
    this.item.removeClass(this.selectedClass);
    this.textShow.get(0).innerHTML = '&nbsp;';
    this.input.val('');
    this.hide();
}
SimpleDropdown.prototype.enable = function () {
    this.main.removeClass('disabled');
}
SimpleDropdown.prototype.disable = function () {
    this.main.addClass('disabled');
}
function getSimpleDropdown(id) {
    if (typeof(id) === 'string' && id.trim().length > 0) {
        var snipId = 'simple_dropdown_';
        var id = id.replace(snipId,'');
        if (typeof(objSimpleDropdown[snipId + id]) !== 'undefined') {
            return objSimpleDropdown[snipId + id];
        }
    }
    return null;
}
SimpleDropdown.prototype.validdateEvent = function(type){    
    var self = this;
    let checkInvalidClass = self.main.parents('.wrap-input-text');
    if(checkInvalidClass.hasClass('invalid')){
        if (!self.isOpen()) {       
            checkInvalidClass.addClass('focus');           
        }else{
            checkInvalidClass.removeClass('focus');
        }
    }else{
        checkInvalidClass.removeClass('focus');
    } 
};
SimpleDropdown.prototype.filterFunction = function(){ 
    let self = this;
    let filter, li;
    self.inpSearch.get(0).value = '';
    self.elmMainSearch.find("li").attr('style',"");
    self.inpSearch.get(0).addEventListener('keyup',function(){
        let val = this.value;
        filter = val.toUpperCase();
        li = self.elmMainSearch.find("li");
        for (i = 0; i < li.length; i++) {
          txtValue = li.get(i).textContent || li.get(i).innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li.get(i).style.display = "";
          } else {
            li.get(i).style.display = "none";
          }
        }
    });
}

// $(document).ready(function () {
//     $('.simple-dropdown').el.forEach(function(elem) {
//         new SimpleDropdown(elem);
//     });
// });