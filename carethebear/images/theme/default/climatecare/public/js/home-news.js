function newsHTML(newData) {
    var html = '';
    html += '<div class="swiper-slide slide_news" style="cursor: url(' + image_url + 'theme/default/public/images/home/news/cursor.svg) 40 40, pointer">';
        html += '<a href="'+newData.link+'" class="link_slide">';
            html += '<div class="wrap_img">';
                html += '<img class="lazy-img" data-src="'+newData.image+'" alt="'+newData.imageAlt+'" />';
            html += '</div>';
            html += '<div class="wrap_detail">';
                html += '<div class="date">'+newData.date+'</div>';
                html += '<div class="news_title f_med">'+newData.title+'</div>';
            html += '</div>';
        html += '</a>';
    html += '</div>';
    return html;
}

$(document).ready(function() {
    let newsSwiper = "";
    let currentMedia = '';

    function destroySlide() {
        if (newsSwiper != '') {
            newsSwiper.destroy();
        };
    }
    function initSwiper() {
        if (window.innerWidth < 767) {
            if (currentMedia != 'mobile') {
                currentMedia = 'mobile';
                destroySlide();
                newsSwiper = new Swiper('.slider_news .swiper', {
                    loop:true,
                    speed: 400,
                    loopPreventsSlide:true,
                    allowTouchMove:true,
                    slidesPerView:"auto",
                    spaceBetween: 26,
                    navigation: {
                        nextEl: '.section-news .arrow.right',
                        prevEl: '.section-news .arrow.left'
                    }
                });
            }
        } else {
            if (currentMedia != 'desktop') {
                currentMedia = 'desktop';
                destroySlide();
                newsSwiper = new Swiper('.slider_news .swiper', {
                    loop:true,
                    speed: 400,
                    loopPreventsSlide:true,
                    allowTouchMove:true,
                    slidesPerView:"auto",
                    spaceBetween: 30,
                    navigation: {
                        nextEl: '.section-news .arrow.right',
                        prevEl: '.section-news .arrow.left'
                    }
                });
            }
        }
        activeClassSlider();
        newsSwiper.on('slideChange', function() {
            activeClassSlider();
            inviewSpy();
            $('.lazy-img:not(.img-loaded)').lazy();
        });
    }

    function activeClassSlider() {
        for (let i = 0; i < newsSwiper.slides.length; i++) {
            let checkIndex = newsSwiper.activeIndex;
            if ($(newsSwiper.slides[i]).hasClass('swiper-slide-active')) {
                $(newsSwiper.slides[i]).addClass('active');
                $(newsSwiper.slides[i]).removeClass('prev next');
            } else {
                if (i < checkIndex) {
                    $(newsSwiper.slides[i]).addClass('prev');
                    $(newsSwiper.slides[i]).removeClass('active next');
                } else {
                    $(newsSwiper.slides[i]).addClass('next');
                    $(newsSwiper.slides[i]).removeClass('active prev');
                }
            }
        }
    }

    // TODO: get news data
    var usedata__News = [...dataNews];
    var htmlNews = '';
    new Promise((resolve) => {
        usedata__News.forEach( (news, count) => {
            htmlNews += newsHTML(news);
            if (count+1 == usedata__News.length) resolve(htmlNews);
        });
    }).then((html) => {
        $('.slider_news .wrap_slides').html(html);
        initSwiper();
        $(window).on('resize', initSwiper);
    });
});