let listImgSwiper = [];
let info_items = [];
$(document).ready(function (){
    var body = $("body")

    $('.text-2').inview( function(e) {
        run_number(e);
    });
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    $('.slide-reciever').inview(function() {
        var html = main_slide({'side' : 'left', 'cate' : 'bear', 'icon': 'bear-icon'});
        $('.slide-reciever').html(html);
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
    
        $(window).on('resize', function (){
            mediaInitSlide('bear')
        });
        mediaInitSlide('bear');
    });

    var html_contact = main_contact("bear", info_items)
    $('.contact-html').html(html_contact)
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    $("#get-see-all").on('click', function(){
        body.append(append_modal(bear,"bear"));
        body.addClass('dim');
        setTimeout(() => {
            $('.main-modal-care .modal-content-absolute').addClass('show');
        }, 100);
        $(".btn-close-modal").on('click', function(){
            $('.main-modal-care .modal-content-absolute').removeClass('show');
            setTimeout(() => {
                $(".main-modal-care").remove();
                body.removeClass('dim');
            }, 600);
        })
        scroll_filter()
    });
})