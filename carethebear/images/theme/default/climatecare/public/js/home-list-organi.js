function slideWithImage(imageArr) {
    var html = '';
    html += '<div class="swiper-slide wrap_slide">';
    imageArr.forEach((logo, index) => {
        html += image(logo, 'logo-' + index);
    });
    html += '</div>';
    return html;
}
function image(img, imgAlt, cls='') {
    return '<img data-src="' + img + '" alt="' + imgAlt + '" class="lazy-img logo '+cls+'" style="width: ' + colWidth + '%" inviewspy>';
}
function calSlidePage(row) {
    let arrImg = [[]];
    let w = window.innerWidth;
    let _col = 0;
    let logo_size = checkMediaScreen() == 'desktop' ? 90 : 180;
    let min_col = checkMediaScreen() == 'desktop' ? 8 : 4;
    let max_col = checkMediaScreen() == 'desktop' ? 10 : 6;

    if (Math.ceil(w / logo_size) < min_col) {
        _col = min_col;
        colWidth = 100 / min_col;
    } else if (Math.ceil(w / logo_size) > max_col) {
        _col = max_col;
        colWidth = 100 / max_col;
    } else {
        _col = Math.ceil(w / logo_size);
        if (100 / _col > 25) colWidth = 25;
        else colWidth = 100 / _col;
    }
    colPerSlide = _col;
    var counter = 0;
    var addedImg = 0;

    dataOrganization.forEach((img) => {
        if (arrImg[counter] == undefined) arrImg[counter] = [];
        arrImg[counter].push(img);
        addedImg++;

        if (addedImg == (_col * row)) {
            counter++;
            addedImg = 0;
        }
    });
    arrImgOrgani = arrImg;
}

function checkMediaScreen() {
    return window.innerWidth < 767 ? 'mobile' : 'desktop';
}
function mediaInitSlide() {
    if (checkMediaScreen() == 'mobile') {
        calSlidePage(9);
    } else {
        calSlidePage(6);
    }
    appendSlide();
}
function appendSlide() {
    var slideHTML = '';
    arrImgOrgani.forEach((slide, index) => {
        slideHTML += slideWithImage(slide);
    });
    $('.slider_organization .swiper-wrapper').html(slideHTML);
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    if (listImgSwiper != '' && listImgSwiper.destroyed && listImgSwiper.destroyed !== true) listImgSwiper.destroy();
    listImgSwiper = new Swiper('#list-organization .slider_organization', { 
        loop: true, 
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        }, 
        speed: 1000, 
        allowTouchMove: true, 
        spaceBetween: 20,
        pagination: {
            el: '.swiper-pagination',
            clickable: true, 
            bulletClass: 'slide_bullet', 
            bulletActiveClass: 'active'
        }
    });

    listImgSwiper.on('slideChange', function() {
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
    });
}

function popupFilterHTML() {
    var html = '';
    html += '<div class="popup-filter">';
        html += '<div class="wrap_bg_head">';
            html += '<div class="wrap_head flex v-center h-justify">';
                html += '<div class="organi_title f_bold">';
                    html += 'องค์กร<br class="show-xs"/>ที่เข้าร่วมโครงการ';
                html += '</div>';
                html += '<div class="flex v-center">';
                    html += '<div class="wrap_dropdown">';
                        html += '<div class="simple-dropdown f_reg hide-placeholder " id="simple_dropdown_filter">';
                            html += '<input type="hidden" name="simple_dropdown_filter" value="" class="input-value" data-error="">';
                            html += '<div class="dropdown-show-selected flex flex-v-c flex-r f_med">';
                                html += '<span class="-text">ทั้งหมด</span>';
                                html += '<span class="-icon icon-arrow-down"></span>';
                            html += '</div>';
                            html += '<div class="dropdown-section hide-serch">';
                                html += '<ul>';
                                    choices.forEach(choice => {
                                        html += '<li class="flex '+(choice.value == 'all' ? 'selected' : '')+'" data-value="'+choice.value+'">';
                                            html += '<span class="-text">'+choice.label+'</span>';
                                        html += '</li>';
                                    });
                                html += '</ul>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                    html += '<div class="wrap_close">';
                        html += '<div class="close_btn black XL">';
                            html += '<span class="icon icon-cross"></span>';
                        html += '</div>';
                    html += '</div>';
                html += '</div>';
            html += '</div>';
        html += '</div>';
        html += '<div class="popup_box">';
            html += '<div class="wrap_list">';
                html += '<div class="max_width_list">';
                html += '</div>';
            html += '</div>';
        html += '</div>';
    html += '</div>';
    return html;
}

function getLogoFromCate(cate) {
    // check cate
    var arrImg = [];
    switch (cate) {
        case 'all':
            arrImg = dataOrganization;
            break;
        case 'bear':
            arrImg = bear;
            break;
        case 'whale':
            arrImg = whale;
            break;
        case 'elephant':
            arrImg = elephant;
            break;
        default:
            arrImg = dataOrganization;
            break;
    }
    var html = '';
    arrImg.forEach((data, key) => {
        html += image(data, 'logo-'+key, 'filtered_logo');
    });
    return html;
}

let colPerSlide = 1;
let colWidth = 0;
let currentMedia = '';
let arrImgOrgani = '';
let listImgSwiper = '';
let choices = [
    { value: "all", label: "ทั้งหมด" },
    { value: "bear", label: "Care the Bear" },
    { value: "whale", label: "Care the Whale" },
    { value: "wild", label: "Care the Wild" }
];

$(document).ready(function () {
    var body = $('body');
    $('#list-organization').inview(function() {
        $(window).on('resize', mediaInitSlide);
        mediaInitSlide();
    
        $('#list-organization .button').on('click', function() {
            body.append(popupFilterHTML());
            var filterDropdown = new SimpleDropdown('filter');
            body.addClass('dim');
            setTimeout(() => {
                $('.popup-filter').addClass('show');
                filterDropdown.main.on('change', function() {
                    $('.popup-filter .max_width_list').html(getLogoFromCate(filterDropdown.getValue()));
                    inviewSpy();
                    $('.lazy-img:not(.img-loaded)').lazy();
                });
                filterDropdown.main.trigger('change');
                $('.popup-filter .close_btn').on('click', function() {
                    $('.popup-filter').removeClass('show');
                    setTimeout(() => {
                        $('.popup-filter').remove();
                        body.removeClass('dim');
                    }, 700);
                });
            }, 50);
        });
    });
});