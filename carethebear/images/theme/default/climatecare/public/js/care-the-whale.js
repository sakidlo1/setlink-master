let listImgSwiper = [];
let info_items = [];
$(document).ready(function (){
    var body = $("body")
    $('#run-num').inview( function(e) {
        run_number(e);
    });

    $('.slide-wal').inview(function() {
        var html = main_slide({'side' : 'right', 'cate' : 'wal', 'icon': 'wal-icon'});
        $('.slide-wal').html(html);
        
        $(window).on('resize', function (){
            mediaInitSlide('wal')
        });
        mediaInitSlide('wal');
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
    });

    var html_contact = main_contact("wal", info_items)
    $('#contact-html').html(html_contact)
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    $("#get-see-all").on('click', function(){
        body.append(append_modal(whale, "wal"));
        body.addClass('dim');
        setTimeout(() => {
            $('.main-modal-care .modal-content-absolute').addClass('show');
        }, 100);
        $(".btn-close-modal").on('click', function(){
            $('.main-modal-care .modal-content-absolute').removeClass('show');
            setTimeout(() => {
                $(".main-modal-care").remove();
                body.removeClass('dim');
            }, 600);
        })
        scroll_filter()
    });
})