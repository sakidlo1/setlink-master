$(document).ready(function() {
    window.panelPosition = 'right';
    $('.section-register').inview(function() {
        $('.sticky_register').addClass('hide');
        window.panelPosition = 'center';
    }, {}, false);
    setTimeout(() => {
        $('.section-register').outview(function() {
            $('.sticky_register').removeClass('hide');
            window.panelPosition = 'right';
        });
    }, 1000);
    $('.section-register .button').on('click', function() {
        if (typeof showPanelRegister == 'function') showPanelRegister();
    });
});