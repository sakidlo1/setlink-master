function UploadFile(create, main) {
   if (typeof create === 'undefined' || typeof main === 'undefined' || !main) {
      return;
   }
   this.create = create;
   this.main = main;
   this.path = [];
   this.dropzone = this.main.find('.upload-wrap');
   this.uploadArea = this.main.find('.upload-area');
   this.uploadPreview = this.main.find('.upload-preview');
   this.inputUpload = this.main.find('.input_customer_logo');
   // console.log(this);
   this.init()
   this.initDropzone();
   this.initUploadFile(this.inputUpload);
   return this;
}
UploadFile.prototype.getExtension = function (filename) {
   return filename.split('.').pop().toLowerCase();
}
UploadFile.prototype.getType = function (path) {
   return path.split('/')[0];
}
UploadFile.prototype.randomId = function () {
   return Math.ceil(Math.random() * 100000);
};
UploadFile.prototype.getSize = function (fileSize) {
   var size = fileSize;
   var fileSizeExtension = ['Bytes', 'KB', 'MB', 'GB'];
   var i = 0; 
   while(size > 900){
      size /= 1024; i++;
   }
   var exactSize = (Math.round(size * 100) / 100) + ' ' + fileSizeExtension[i];
   return exactSize;
}
UploadFile.convertFileToBase64 = function (file) {
   return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
         resolve(reader.result);
      };
      reader.onerror = function (error) {
         reject(error);
      };
   });
};
UploadFile.prototype.checkExistFile = function () {
   // Remove exit file
};
UploadFile.prototype.init = function () {
   var self = this;
   this.uploadArea.on('click', function () {
      self.inputUpload.get(0).click();
  });
}
UploadFile.prototype.initDropzone = function () {
   var dropzone = this.dropzone;
   var self = this;
   var anyDragDrop = function (event) {
      event.preventDefault();
      event.stopPropagation();
   }
   var onDragOver = function () {
      dropzone.addClass('dragOver');
   }
   var onDragLeaveOrDrop = function () {
      dropzone.removeClass('dragOver');
   }
  
   dropzone.on('dragenter', anyDragDrop);
   dropzone.on('dragover', anyDragDrop);
   dropzone.on('dragleave', anyDragDrop);
   dropzone.on('drop', anyDragDrop);

   dropzone.on('dragenter', onDragOver);
   dropzone.on('dragover', onDragOver);

   dropzone.on('dragleave', onDragLeaveOrDrop);
   dropzone.on('drop', onDragLeaveOrDrop);

   dropzone.on('drop', function (event) {

      /** @todo check limit file upload*/

      // Allow one file
      if (event.dataTransfer.files.length > 1) {
         event.preventDefault();
         return false;
      }
      self.onUpload(event.dataTransfer.files);
  }, false);
};
UploadFile.prototype.initUploadFile = function (elementUpload) {
   if (typeof elementUpload === 'undefined') {
      return;
   }
   var self = this;
   elementUpload.on('change', function () {
      self.onUpload(this.files);
   });
};
UploadFile.prototype.onUpload = function (files) {
   var self = this;
   var isSupportFile = false;
   var isSupportSize = false;
   var support = 'jpg,jpeg,png'.split(',');
   var limitSize = 2;

   var fileObject = Object.keys(files).map(function (key) {
      return files[key];
   }).filter(function (file) {
      isSupportFile = true;
      isSupportSize = true;
      var ext = self.getExtension(file.name);
      var supportFiles = support.indexOf(ext) !== -1;
      var SET_LIMIT_SIZE = (limitSize !== undefined) ? limitSize : file.size;
      var supportSize = (file.size / 1000000) <= SET_LIMIT_SIZE;

      if (!supportFiles) {
         modal.alert('สามารถแนบไฟล์ประเภท ' + support + ' เท่านั้น');
         isSupportFile = false;
         return;
      }

      if (!supportSize) {
         modal.alert('ขนาดไฟล์เกินกำหนด');
         isSupportSize = false;
         return;
      }
      return isSupportFile && isSupportSize;
   }).map(function (file) {
      self.checkExistFile();
      var fileExtension = self.getExtension(file.name);
      var filetype = self.getType(file.type);
      var fileSize = self.getSize(file.size);
      var obj = {
         name: file.name,
         size: fileSize,
         file: file.name,
         ext: fileExtension,
         type: filetype,
         id: self.randomId()
      };
      self.path.push(obj);
      self.initPreviewImage(obj)
      return { file: file, fileInfo: obj };
   });
   this.main.trigger('uploaded', fileObject); // trigger current uploaded section.
};
UploadFile.prototype.initPreviewImage = function (fileInfo) {
   if (fileInfo === undefined) {
      return;
   }

   var self = this;
   var helper = this.helper();

   // this.uploadArea
   // this.uploadPreview
   this.uploadPreview.html('');
   var container = helper.$({
      class: 'preview-upload flex v-center h-justify'
   });
   var deleteImage =  helper.$({
      class: 'delete-upload flex center'
   });
   deleteImage.html('<div class="icon icon-cross">');

   var innerContent = '<div class="block--info flex center">'
                        + '<span class="block-icon icon-file"></span>'
                        + '<div class="block-info flex column">'
                           + '<div class="block-info--name f_med">' + fileInfo.name + '</div>'
                           + '<div class="block-info--size">' + fileInfo.size +'</div>'
                        + '</div>';
                        + '</div>';
   deleteImage.on('click', function () {
      var index = self.path.map(function(item){ return item.id == fileInfo.id }).indexOf(true);
      if (index !== -1) {
         self.main.trigger('delete', [index]);
         self.uploadPreview.html('');
         self.uploadPreview.attr('title', '');
         self.dropzone.removeClass('dragover active have-file');
         self.uploadArea.addClass('show');
         self.inputUpload.val('');
     }
   });

   container.html(innerContent);
   container.append(deleteImage);
   this.uploadPreview.attr('title', fileInfo.name);
   this.uploadPreview.append(container);
   this.dropzone.addClass('have-file');
   if (this.dropzone.hasClass('have-file')){
      this.dropzone.removeClass('invalid');
   }
  this.uploadArea.removeClass('show');
};
UploadFile.prototype.helper = function() {
   var isArray = Array.isArray;
   var isString = function (value) {
      typeof value === 'string'
   }
   var isObject = function (obj) {
      return obj !== null && typeof obj === 'object';
   }
   return {
      $: function (obj) {
         var _tag = obj.tag || 'div';
         var _class = isArray(obj.class) 
                        ? obj.class 
                        : isString(obj.class)
                           ? obj.class
                           : [obj.class]
         var _id = obj.id || '';
         var _dataSet = isObject(obj.dataset) ? obj.dataset : {};
         var _attr = isObject(obj.attr) ? obj.attr : {};
         var html = document.createElement(_tag);

         if (_id) {
            try {
               html.id = _id;
            } catch (_error) {
               console.log(_error);
            }
         }
         if (_attr) {
            for (var key in _attr) {
               try {
                  var value = _attr[key];
                  html.setAttribute(key,value);
               } catch (_error) {
                  console.log(_error);
               }
            }
         }
         if (_class.length) {
            try {
               html.className = _class.join(' ');
            } catch (_error) {
               console.log(_error);
            }
         }
         if (_dataSet) {
            for (var key in _dataSet) {
               try {
                  var value = _dataSet[key];
                  html.dataset[key] = value;
               } catch (_error) {
                  console.log(_error);
               }
            }
         }
         return $(html);
      }
   }
}
// function filePreview (src,previewElementImageId) {
//    var [file] = src.files;
//    if (file) {
//      previewElementImageId.src = URL.createObjectURL(file)
//    }
// }

// imgInp.onchange = evt => {
//    filePreview(imgInp, previewElementImageId);
// }