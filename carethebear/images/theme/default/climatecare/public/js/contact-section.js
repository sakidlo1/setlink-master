function main_contact(active, list){
    var arrCate = ['bear', 'wal', 'wild'];
    var html = "";
    let another_info = [
        {
            link: base_url + 'carethebear',
            bg: image_url + 'theme/default/public/images/care-the-bear/sec-6/content-1new.jpg',
            icon: image_url + 'theme/default/public/images/care-the-bear/sec-6/icon/img-1.svg',
            desc_1:'Care the Bear',
            desc_2:'“ลดโลกร้อน”',
            hover:'bear'
        },
        {
            link: base_url + 'care-the-whale',
            bg: image_url + 'theme/default/public/images/care-the-bear/sec-6/content-2.jpg',
            icon: image_url + 'theme/default/public/images/care-the-bear/sec-6/icon/img-2.svg',
            desc_1:'Care the Whale',
            desc_2:'“ขยะล่องหน”',
            hover:'whale'
        },
        {
            link: base_url + 'care-the-wild',
            bg: image_url + 'theme/default/public/images/care-the-bear/sec-6/content-3.jpg',
            icon: image_url + 'theme/default/public/images/care-the-bear/sec-6/icon/img-3.svg',
            desc_1:'Care the Wild',
            desc_2:'“ปลูกป้อง Plant & Protect”',
            hover:'wild'
        },
    ]
    html += '<div class="main-contact">'
        if (list.length > 0) {
            html += '<div class="top-contact">'
                html += '<div class="container">'
                    html += '<span class="title-contact"> ติดต่อฝ่ายพัฒนาเพื่อสังคม ตลาดหลักทรัพย์'
                        html += '<br class="d-sm-none"> แห่งประเทศไทย'
                        html += '<br class="d-lg-block">หรือดูรายละเอียดเพิ่มเติมได้ที่'
                    html += '</span>'
                    html += '<div class="box-contact">'
                        html +='<ul class="contact-list-row">'
                            list.forEach((item, index) => {
                                html += '<li inviewspy="'+((index+1)*200)+'">'
                                    html += '<div class="contact-list">'
                                        html += '<span class="contact-icon '+item.icon+'"></span>'
                                        html += '<a href="'+item.link+'" target="_blank" class="target-link text-contact text-'+active+' ">'+item.desc+''
                                        html += '</a>'
                                    html += '</div>'
                                html += '</li>'
                            });
                        html +='</ul>'
                    html += '</div>'
                html += '</div>'
            html += '</div>'
        }
        if (arrCate.indexOf(active) != -1) {
            html += '<span class="title-bot-contact" inviewspy="800">โครงการอื่นๆ'
            html += '</span>'
            html += '<div class="bot-contact">'
                html += '<div class="box-content">'
                    another_info.forEach((another_items, index) => {
                        let count = index+1;
                        let care_active = '';
                        if(active == 'bear'){
                            care_active = "active-bear"
                        }else if(active == 'wal'){
                            care_active = "active-whale"
                        }else if(active == 'wild'){
                            care_active = "active-wild"
                        }
                        html += '<a href="'+ another_items.link +'" class="'+care_active+' another '+another_items.hover+'-active filter-anoter-1" inviewspy='+(count*200)+'>'
                            html += '<img class="another-img lazy-img" data-src="'+another_items.bg+'">'
                            html += '<div class="another-content">'
                                html += '<div class="single-another-content">'
                                    html += '<img class="icon-'+count+' lazy-img" data-src="'+another_items.icon+'">'
                                    html += '<span class="text-another">'+another_items.desc_1+''
                                        html += '<br>'+another_items.desc_2+''
                                    html += '</span>'
                                html += '</div>'
                            html += '</div>'
                        html += '</a>'
                    })
                html += '</div>'
            html += '</div>'
        }
    html += '</div>'
    return html

}