function Register () {
   this.class = 'page-register';
   this.main = $('.' + this.class);
   if (!this.main.length) {
      return;
   }
   this.opts = Object.freeze({
      class: {
         invalid: 'invalid',
         warpInputText: 'wrap-input-text',
         focus: 'focus'
      }
   });
   this.formList = {};
   this.dropdowns = {};
   this.upload = {};
   this.btnSubmitForm = this.main.find('.btn-action.submit-form');
   this.formData = {};
   this.uploadFile = [];
   this.inputPassword = this.main.find('[name="customer_account_password"]');
   this.inputRePassword = this.main.find('[name="customer_account_confirm_password"]');
   this.phone = this.main.find('[name="customer_phone"]');
   this.phonePartner = this.main.find('[name="customer_phone_partner"]');
   this.init();
   // console.log(this); // ! TODO remove
   return this;
}
Register.prototype.init = function () {
   var self = this;
   if (!this.main.find('.common-form').length) {
      return null;
   }

   var addFormList = function(form) {
      return self.setForm(form);
   };

   var initialize = function () {
      self.upload = self.initUploadFile();
      self.initDropdown();
      self.initActionForm();
      self.watchUpload();
      self.watchInput();
   };

   Promise.all(
      this.main.find('.common-form').el.map(function(item) {
         return $(item).transformTag('form').then(addFormList);
      })
   ).then(function() {
      initialize();
   });
};
Register.prototype.setForm = function (element) {
   if (!element.length) {
      return;
   }
   var id = element.attr('id');
   return this.formList[id] = new CommonForm(element.get(0));
};
/**
 * init Dropdown
 * 
 * @returns null
 */
Register.prototype.initDropdown = function () {
   if (!SimpleDropdown || !this.main.find('.simple-dropdown').el.length) {
      return null;
   }
   var self = this;
   var snipId = 'simple_dropdown_';
   var watchEvent = function(objDropdown) {
      var dropdown = objDropdown.main;
      var parentDropdown = dropdown.parents('.' + self.opts.class.warpInputText);
      dropdown.on('change', function() {
         dropdown.removeClass(self.opts.class.invalid);
         parentDropdown.removeClass(self.opts.class.invalid);
      });
      dropdown.on('show', function() {
         parentDropdown.addClass(self.opts.class.focus);
      });
      dropdown.on('hide', function() {
         parentDropdown.removeClass(self.opts.class.focus);
      });
   }

   this.main.find('.simple-dropdown').el.forEach(function(element) {
      var id = $(element).attr('id').split(snipId)[1];
      if (id) {
         self.dropdowns[id] = new SimpleDropdown(id);
         watchEvent(self.dropdowns[id]);
      }
   });
};
Register.prototype.initActionForm = function () {
   var self = this;
   if (!this.btnSubmitForm.length) {
      return;
   }

   this.btnSubmitForm.on('click', function() {
      Promise.all([
         self.validateFormData(), 
         self.validatePassword()
      ]).then(function(values) {
         if (!values.length) {
            console.warn('no data');
            return;
         }
         var formData = values[0];
         var password = values[1];
         if (formData.valid && password.valid) {
            callback(formData);
         }
       });
   });

   var callback = function(data) {

      $('.wrap-action .form-display-error').remove();

      if (data.valid) {
         for (var key in data.inputs) {
            self.formData[key] = data.inputs[key];
         }

         var form = new FormData();
         for (var key in self.formData) {
            form.append(key, self.formData[key]);
            //console.log(key, self.formData[key]);
         }

         for(var i = 0; i < document.getElementById("customer_logo").files.length; i++)
         {
            form.append('customer_logo_' + i, document.getElementById("customer_logo").files[i]);
         }

         // Post Form Here
         var xhr = new XMLHttpRequest();
         var url = base_url + "member/register";
         xhr.open("POST", url, true);
         xhr.onload = function () {
             //console.log(this.responseText);

            if(this.responseText == 'email_exists')
            {
               $('.wrap-action').prepend('<label style="color: red; display: block; text-align: center; margin-bottom: 20px;" class="control-label form-display-error">อีเมลนี้มีอยู่ในระบบแล้ว กรุณาใช้อีเมลอื่น</label>');
            }
            else if(this.responseText == 'username_exists')
            {
               $('.wrap-action').prepend('<label style="color: red; display: block; text-align: center; margin-bottom: 20px;" class="control-label form-display-error">ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว กรุณาใช้ชื่อผู้ใช้งานอื่น</label>');
            }
            else if(this.responseText == 'register_carethewhale')
            {
               window.location = base_url + 'member/register_carethewhale';
            }
            else if(this.responseText == 'success')
            {
               window.location = base_url + 'member/register_success';
            }

         };
         xhr.send(form);

         //window.location.href = 'register-complete.html';
      }
      logged();
   }
   var logged = function () {
      // console.log(self);
   };
};
Register.prototype.validateFormData = function () {
   var self = this;
   var settings = this.opts;
   var forms = this.formList;
   var dropdowns = this.dropdowns;
   var upload = this.upload;
   var phone = {
      customer: self.phone
      ,partner: self.phonePartner
   }
   var formValid = true;
   var formInputs = {};
   var invalidList = [];
   return new Promise(function(resolve) {

      // Input
      for (var itemForm in forms) {
         if (!forms[itemForm].main) {
            continue;
         }
         var objForm = forms[itemForm].checkValidForm();
         if (!objForm.valid) {
            formValid = false;
            invalidList = invalidList.concat(objForm.list);
         }
         $.extend(formInputs, objForm.form);
      }

      // Dropdown
      for (var itemDropdown in dropdowns) {
         var dropdown = dropdowns[itemDropdown];
         if (!dropdown.getCurrentData()) {
            dropdown.main.addClass(settings.class.invalid);
            dropdown.main.parents('.' + settings.class.warpInputText).addClass(settings.class.invalid);
            formValid = false;
         }
      }

      // Upload
      if (upload.inputUpload) {

         if (!self.formData['file[]'] || !self.formData['file[]'].length) {
            upload.dropzone.addClass('invalid');
            formValid = false;
         } else {
            upload.dropzone.removeClass('invalid');
         }
      }

      /**
       * Format Phone
       * 
       * 1 Not leading zero
       * 2 Invalid format 9-10 character
       */
      if (!phone.customer.val().toString()[0]) {
         checkValidCustom(phone.customer, 0, false, true);
      } else if (phone.customer.val().toString()[0] !== '0') {
         checkValidCustom(phone.customer, 2, false, true);
         formValid = false;
      } else if (phone.customer.val().length < 9 || phone.customer.val().length > 10) {
         checkValidCustom(phone.customer, 1, false, true); 
         formValid = false;
      }
      if (!phone.partner.val().toString()[0]) {
         checkValidCustom(phone.partner, 0, false, true);
      } else if (phone.partner.val().toString()[0] !== '0') {
         checkValidCustom(phone.partner, 2, false, true);
         formValid = false;
      } else if (phone.partner.val().length < 9 || phone.partner.val().length > 10) {
         checkValidCustom(phone.partner, 1, false, true); 
         formValid = false;
      }

      return resolve({
         invalidList: invalidList
         ,inputs: formInputs
         ,valid: formValid
      });
   })
};
Register.prototype.watchUpload = function () {
   var self = this;
   var files = [];

   // Uploaded
   this.upload.main.on('uploaded', function(event) {
      if (event.detail) {
         event.detail.forEach(function(item) {
            if (item.file) {
               files.push(item.file);
            }
         });
         self.upload.dropzone.removeClass('invalid');
      }
      if (!self.formData['file[]']) {
         self.formData['file[]'] = [];
      }
      self.formData['file[]'] = files;
   });

   // Delete
   this.upload.main.on('delete', function (event) {
      if (event.detail) {
         var index = event.detail[0];
         if (index > -1) { 
            self.formData['file[]'].splice(index, 1); 
          }
      }
   });
};
Register.prototype.watchInput = function () {
   numericOnly(this.phone.get(0));
   numericOnly(this.phonePartner.get(0));
};
Register.prototype.validatePassword = function () {
   var valid = false;
   var self = this;
   var inputPassword = self.inputPassword.el ? self.inputPassword : $(self.inputPassword);
   var inputRePassword = self.inputRePassword.el ? self.inputRePassword : $(self.inputRePassword);
   return new Promise(resolve => {
      if (!inputPassword.length || !inputRePassword.length) {
         return resolve({ 
            valid: valid, 
            message: 'missing element' 
         });
      }

      if (inputPassword.length <= 0) {
         return resolve({ 
            valid: valid 
         });
      }
      if (inputPassword.val().length <= 0) {
         checkValidCustom(inputPassword, 0, false, true);
      } else if (inputPassword.val() != inputRePassword.val()) {
         checkValidCustom(inputRePassword, 1, false, true);
      } else {
         inputPassword.invalidText('reset');
         inputPassword.parents(".wrap-input-text").removeClass('focus');
         valid = true;
      }

      resolve({ 
         valid: valid 
      });
   });
};
/**
 * @package from "upload-file.js"
 */
Register.prototype.initUploadFile = function () {
   if (!UploadFile) {
      return;
   }
   return new UploadFile(this, this.formList.upload.main);
};

$(document).ready(function() {
   window.registerForm = new Register();
});