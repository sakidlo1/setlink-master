var planting_area = [
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านชัฎหนองยาว',
        location_sub: 'อ.ด้านช้าง จ.สุพรรณบุรี ',
        icon_type: 'forest1',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77688'
    },
    {
        category: 'successed',
        location: 'ป่าดอยอินทรีย์',
        location_sub: 'อ.เมือง จ.เชียงราย​',
        icon_type: 'forest1',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77332'
    },
    {
        category: 'successed',
        location: 'ป่าบ้านเขาหัวคน',
        location_sub: 'อ.เมือง จ.ราชบุรี​',
        icon_type: 'forest2',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77532'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านพุตูม',
        location_sub: 'อ.บ้านลาด จ.เพชรบุรี​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77269'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านใหม่',
        location_sub: 'อ.เมือง จ.เชียงราย​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77295'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านนาหวาย',
        location_sub: 'อ.นาหมื่น จ.น่าน​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77535'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนตำบลสหกรณ์นิคม',
        location_sub: 'อ.ทองผาภูมิ จ.กาญจนบุรี​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77469'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านหลังเขา',
        location_sub: 'อ.บ่อพลอย จ.กาญจนบุรี​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77470'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านอ้อย',
        location_sub: 'อ.ร้องกวาง จ.แพร่​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77474'
    },
    {
        category: 'successed',
        location: 'ป่าชุมชนบ้านบุญเริง',
        location_sub: 'อ.ร้องกวาง จ.แพร่​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77473'
    },
    {
        category: 'waited',
        location: 'ป่าชุมชนบ้านโคกพลวง',
        location_sub: 'อ.จักราช จ.นครราชสีมา​',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77291'
    },
    {
        category: 'waited',
        location: 'ป่าชุมชนบ้านหนองทิศสอน',
        location_sub: 'อ.นาเชือก จ.มหาสารคาม',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77270'
    },
    {
        category: 'waited',
        location: 'ป่าชุมชนบ้านหนองปลิง',
        location_sub: 'อ.เลาขวัญ จ.กาญจนบุรี',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77294'
    },
    {
        category: 'waited',
        location: 'ป่าชุมชนโคกโสกขี้หนู',
        location_sub: 'อ.ลำทะเมนชัย จ.นครราชสีมา',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77468'
    },
    {
        category: 'waited',
        location: 'ป่าชุมชนบ้านห้วยก้อด',
        location_sub: 'อ.วังเหนือ จ.ลำปาง ',
        icon_type: 'forest3',
        box_img_bg: image_url + 'theme/default/public/images/wild/jungle-box-bg_section5.png',
        box_img_alt: 'test',
        link: 'https://www.setsocialimpact.com/Article/Detail/77691'
    },
];

let info_items = [];

function slide(planting_area) {
    var path_icon = '';
    var html = '';

    if (planting_area.icon_type == 'forest1') {
        path_icon = image_url + 'theme/default/public/images/wild/forest-icon1_section5.png'
    }
    else if (planting_area.icon_type == 'forest2') {
        path_icon = image_url + 'theme/default/public/images/wild/forest-icon3_section5.png'
    }
    else if (planting_area.icon_type == 'forest3') {
        path_icon = image_url + 'theme/default/public/images/wild/forest-icon2_section5.png'
    }

    html += '<a href="' + planting_area.link + '" class="box-img-content swiper-slide" >';
        html += '<div class="box-detail" style="cursor: url(' + image_url + 'theme/default/public/images/wild/circle_cursor.svg) 40 40, pointer;">';
            html += '<img src="' + path_icon + '" alt="icon-forest">';
            html += '<div class="box-header-text f_med">' + planting_area.location + ' <br> <span class="box-text f_reg">' + planting_area.location_sub + '</span>';
            html += '</div>';
        html += '</div>';
        html += '<img data-src="' + planting_area.box_img_bg + '" class="forest lazy-img" alt="' + planting_area.box_img_alt + '">';
    html += '</a>';

    return html;

}


function loop_slide(type) {
    var Htmlslide = '';
    planting_area.forEach((area, index) => {
        if ((type == 'waited' && area.category == 'waited') || (type == 'processed' && area.category == 'successed')) {
            Htmlslide += slide(area);
        }
    });
    $('.container_section5 .swiper-wrapper').html(Htmlslide);
    
    setTimeout(() => {
        if (listImgSwiper != '') {
            listImgSwiper.destroy();
            listImgSwiper = '';
        }
        listImgSwiper = new Swiper('.container_section5 .swiper', {
            loop: true,
            spaceBetween: 20,
            slidesPerView: "auto"
        });
        
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
        
    }, 100);

}

function bg_contact_transparent(){
    document.querySelector(".top-contact").style.background="transparent"
}

function submit_form(){
    let findForm = $('#form_register');
    let objForm = new CommonForm(findForm.get(0)); 
    findForm.on('submit', function(){
        var objValid = objForm.checkValidForm();
        let dataForm = objValid.form;
        if(objValid.valid){

            var xhr = new XMLHttpRequest();
            var url = base_url + "home/send_email";
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    location.href = base_url + "thank_you"   
                }
            };
            var data = JSON.stringify(dataForm);
            xhr.send(data);
            
        }else{
            // do nothing
        }
    }); 
    

}

// let objForm = new CommonForm(findForm.get(0)); 
// let formMainElm = objForm.main;
// var objValid = objForm.checkValidForm(true);

var dropdown;
var dropdownValue = 'processed';
var listImgSwiper = '';
$(document).ready(function () {
    var body = $("body")
    dropdown = new SimpleDropdown('filter');
    dropdown.main.on('change', function () {
        dropdownValue = dropdown.getValue();
        $('.container_section5 .swiper-wrapper').html('');
        loop_slide(dropdownValue);
    });
    loop_slide(dropdownValue);

    $('#organize-html').inview(function() {
        var html = main_slide({'side' : 'left', 'cate' : 'chang', 'icon': 'chang-icon'});
        $('#organize-html').html(html);
        inviewSpy();
        $('.lazy-img:not(.img-loaded)').lazy();
    
        $(window).on('resize', function (){
            mediaInitSlide('elephant')
        });
        mediaInitSlide('elephant');
    })

    var html_contact = main_contact("wild", info_items)
    $('#contact-html').html(html_contact)
    // bg_contact_transparent()

    
    $("#get-see-all").on('click', function(){
        body.append(append_modal(elephant, "chang"));
        body.addClass('dim');
        setTimeout(() => {
            $('.main-modal-care .modal-content-absolute').addClass('show');
        }, 100);
        $(".btn-close-modal").on('click', function(){
            $('.main-modal-care .modal-content-absolute').removeClass('show');
            setTimeout(() => {
                $(".main-modal-care").remove();
                body.removeClass('dim');
            }, 600);
        })
        scroll_filter()
    });
    inviewSpy();
    $('.lazy-img:not(.img-loaded)').lazy();
    var form = new CommonForm('.common-form');
    submit_form();
    
});

$(document).ready(function (){
    var body = $("body")
    $('#run-num').inview( function(e) {
        run_number(e);
    });

})