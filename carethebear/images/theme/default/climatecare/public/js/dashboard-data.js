const dashboardData = {
    fullname: 'ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)',
    name: 'บจม. ธนาคารกรุงศรีอยุธยา',
    since: 'สิงหาคม 2565',
    logoImg: image_url + 'theme/default/public/images/dashboard/krungsri_logo.png',
    logoImgAlt: 'krungsri',
    global_warming_progress: {
        greenhouse_gas_reduced: '907249',
        tree_planted: '41947',
    },
    climate_progress: {
        bear: {
            climateMember: false,
            stats: {
                greenhouse_gas: '13209591',
                planting_trees: '1744215',
            }
        },
        whale: {
            climateMember: true,
            stats: {
                waste_manage: '5117705.95',
                greenhouse_gas: '15697657.95',
                planting_trees: '1744215',
            }
        },
        wild: {
            waste: '0',
            gas: '0',
            tree: '0',
            climateMember: true,
            stats: {
                greenhouse_gas: '0',
                greenhouse_absorb: '0',
                trees_planted: '0',
            }
        }
    }
};
const outcomeData = {
    result_climate: {
        greenhouse_gas_reduced: '200000',
        trees_planted: '2000',
        greenhouse_gas_absorb: '100000'
    }
}
let outcomeSummary = {
    bear: {
        cate: 'bear',
        climate_name: 'โครงการ Care the Bear',
        climate_desc: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการ<br class="show-xs">จัดงานหรือทุกกิจกรรมในรูปแบบ Online และ Onsite',
        year: '2565',
        company_name: 'บมจ. ธนาคารกรุงศรีอยุธยา',
        reduce_gas_activity: '10',
        meeting: '2',
        training: '3',
        give_awards: '5',
        climate_data: {
            greenhouse_gas_reduced: '1500',
            compare_tree_planted: '440'
        },
        progress_detail: [
            {
                txt: 'เดินทางโดยรถไฟฟ้า รถสาธารณะ หรือ Car pool หรือรถจักรยาน',
                value: [
                    500,
                    100
                ],
            },
            {
                txt: 'ลดการใช้กระดาษและพลาสติก',
                value: [
                    200,
                    70
                ]
            },
            {
                txt: 'เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่',
                value: [
                    100,
                    50
                ]
            },
            {
                txt: 'ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า',
                value: [
                    200,
                    70
                ]
            },
            {
                txt: 'งดการใช้โฟม',
                value: [
                    300,
                    80
                ]
            },
            {
                txt: 'ลดการเกิดขยะ ตักอาหารแต่พอดีและทานให้หมด',
                value: [
                    200,
                    70
                ]
            },
        ],
        tableTitle: 'ปริมาณการลดก๊าซเรือนกระจกการหลักการ 6 Cares',
        tableStructure: {
            head: [
                [
                    'หลักการ 6 Cares',
                ],
                [
                    'ปริมาณการลดก๊าซ<br>เรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                    'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                ]
            ],
            nested: [[50, []], [40, [45, 45]]]
        }
    },
    whale: {
        cate: 'whale',
        climate_name: 'โครงการ Care the Whale',
        climate_desc: 'โครงการลดการปล่อยก๊าซเรือนกระจกจากการบริหาร<br class="show-xs">จัดการขยะตั้งแต่ต้นทางถึงปลายทาง',
        company_name: 'บมจ. ธนาคารกรุงศรีอยุธยา',
        year: '2565',
        month: 'ม.ค. - มิ.ย.',
        climate_data: {
            greenhouse_gas_reduced: '1000',
            compare_tree_planted: '440'
        },
        progress_detail: {
            waste: [
                {
                    txt: 'ขยะ Recycle',
                    value: [
                        210516.12,
                        3577207.27,
                        397467
                    ],
                    percent: 14.6
                },
                {
                    txt: 'ขยะอินทรีย์',
                    value: [
                        73230.15,
                        2442862.95,
                        271429
                    ],
                    percent: 5.1
                },
                {
                    txt: 'ขยะทั่วไป',
                    value: [
                        1095917.00,
                        556135.27,
                        61793
                    ],
                    percent: 75.8
                },
                {
                    txt: 'ขยะอันตราย',
                    value: [
                        66431.94,
                        2188.27,
                        243
                    ],
                    percent: 4.6
                }
            ],
            wasteManage: [
                {
                    txt: 'ส่งหน่วยงานฝังกลบ',
                    value: [
                        '78%',
                        1023645.10,
                        153299.85,
                        397467
                    ],
                },
                {
                    txt: 'ขายเพื่อรีไซเคิล',
                    value: [
                        '11%',
                        153299.85,
                        308841.15,
                        34316.00
                    ],
                },
                {
                    txt: 'เผาทำลาย',
                    value: [
                        '5%',
                        67021.16,
                        1564.41,
                        174.00
                    ],
                },
                {
                    txt: 'เผา RDF',
                    value: [
                        '4%',
                        53259.77,
                        176618.34,
                        19,401.00
                    ],
                },
                {
                    txt: 'บริจาคเพื่อรีไซเคิล',
                    value: [
                        '3%',
                        44922.11,
                        2794420.15,
                        310490.00
                    ],
                },
                {
                    txt: 'ทำอาหารสัตว์',
                    value: [
                        '2%',
                        23260.93,
                        2022381.03,
                        224709.00
                    ],
                },
                {
                    txt: 'ทำปุ๋ยหมัก/ดิน',
                    value: [
                        '0%',
                        4205.02,
                        2720.00,
                        2720.00
                    ],
                },
            ]
        },
        wasteTableTitle: 'ประเภทขยะที่ได้จากการคัดแยกขยะ',
        wasteTableStructure: {
            head: [
                [
                    'ประเภทขยะ',
                ],
                [
                    'ปริมาณขยะ <span class="t_grey t_16">(กก.)</span>',
                    'ปริมาณการลดก๊าซเรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                    'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                ]
            ],
            nested: [[30, []], [65, [25, 25, 25]]]
        },
        wasteManageTableTitle: 'ประเภทการจัดการขยะ',
        wasteManageTableStructure: {
            head: [
                [
                    'วิธีการจัดการขยะ',
                ],
                [
                    'สัดส่วน',
                    'ปริมาณขยะ <span class="t_grey t_16">(กก.)</span>',
                    'ปริมาณการลดก๊าซเรือนกระจก <span class="t_grey t_16">(kg.CO<sub>2</sub>e)</span>',
                    'เทียบเท่ากับ<br>การปลูกต้นไม้ <span class="t_grey t_16">(ต้น)</span>'
                ]
            ],
            nested: [[35, []], [65, [20, 30, 25, 25]]]
        }
    },
    elephant: {
        cate: 'elephant',
        climate_name: 'โครงการ Care the Wild <br class="show-xs">“ปลูกป้อง Plant & Protect” ',
        climate_desc: 'โครงการความร่วมมือปลูกป่าเพื่อเพิ่มผืนป่า<br class="show-xs">และรักษาสมดุลระบบนิเวศตามธรรมชาติ',
        company_name: 'บมจ. ธนาคารกรุงศรีอยุธยา',
        climate_data: {
            greenhouse_gas_reduced: '500',
        },
        progress_detail: {
            houses: '500',
            locations: [
                {
                    area: '10 ไร่',
                    trees: ' 2000',
                    village: 'ป่าชุมชนบ้านพุตูม',
                    location: 'จังหวัดเพชรบุรี'
                },
                {
                    area: '5 ไร่',
                    trees: ' 1000',
                    village: 'ป่าชุมชนบ้านหลังเขา',
                    location: 'จังหวัดกาญจนบุรี'
                }
            ]
        },
    },
}

let outcomeExportData = {
    fullname: 'ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)',
    name: 'บมจ. ธนาคารกรุงศรีอยุธยา',
    year: '2565',
    logoImg: image_url + 'theme/default/public/images/dashboard/krungsri_logo.png',
    logoImgAlt: 'krungsri',
    result_climate: {
        greenhouse_gas_reduced: '200000',
        trees_planted: '2000',
        greenhouse_gas_absorb: '100000'
    },
    bear: {
        reduce_gas_activity: '10',
        meeting: '2',
        training: '3',
        give_awards: '5',
        climate_data: {
            greenhouse_gas_reduced: '1500',
            compare_tree_planted: '440'
        },
        progress_detail: [
            {
                txt: 'เดินทางโดยรถไฟฟ้า รถสาธารณะ หรือ Car pool หรือรถจักรยาน',
                value: [
                    500,
                    100
                ],
            },
            {
                txt: 'ลดการใช้กระดาษและพลาสติก',
                value: [
                    200,
                    70
                ]
            },
            {
                txt: 'เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่',
                value: [
                    100,
                    50
                ]
            },
            {
                txt: 'ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า',
                value: [
                    200,
                    70
                ]
            },
            {
                txt: 'งดการใช้โฟม',
                value: [
                    300,
                    80
                ]
            },
            {
                txt: 'ลดการเกิดขยะ ตักอาหารแต่พอดีและทานให้หมด',
                value: [
                    200,
                    70
                ]
            },
        ],
        tableTitle: 'ปริมาณการลดก๊าซเรือนกระจกการหลักการ 6 Cares',
        tableStructure: {
            head: [
                [
                    'หลักการ 6 Cares',
                ],
                [
                    'ปริมาณการลดก๊าซ\nเรือนกระจก (kg.CO2e)',
                    'เทียบเท่ากับ\nการปลูกต้นไม้ (ต้น)'
                ]
            ]
        }
    },
    whale: {
        year: '2565',
        month: 'ม.ค. - มิ.ย.',
        wasteTableTitle: 'ประเภทขยะที่ได้จากการคัดแยกขยะ',
        wasteManageTableTitle: 'ประเภทการจัดการขยะ',
        climate_data: {
            greenhouse_gas_reduced: '1000',
            compare_tree_planted: '440'
        },
        progress_detail: {
            waste: [
                {
                    txt: 'ขยะ Recycle',
                    value: [
                        210516.12,
                        3577207.27,
                        397467
                    ],
                    percent: 14.6
                },
                {
                    txt: 'ขยะอินทรีย์',
                    value: [
                        73230.15,
                        2442862.95,
                        271429
                    ],
                    percent: 5.1
                },
                {
                    txt: 'ขยะทั่วไป',
                    value: [
                        1095917.00,
                        556135.27,
                        61793
                    ],
                    percent: 75.8
                },
                {
                    txt: 'ขยะอันตราย',
                    value: [
                        66431.94,
                        2188.27,
                        243
                    ],
                    percent: 4.6
                }
            ],
            wasteManage: [
                {
                    txt: 'ส่งหน่วยงานฝังกลบ',
                    value: [
                        '78%',
                        1023645.10,
                        153299.85,
                        397467
                    ],
                },
                {
                    txt: 'ขายเพื่อรีไซเคิล',
                    value: [
                        '11%',
                        153299.85,
                        308841.15,
                        34316.00
                    ],
                },
                {
                    txt: 'เผาทำลาย',
                    value: [
                        '5%',
                        67021.16,
                        1564.41,
                        174.00
                    ],
                },
                {
                    txt: 'เผา RDF',
                    value: [
                        '4%',
                        53259.77,
                        176618.34,
                        19,401.00
                    ],
                },
                {
                    txt: 'บริจาคเพื่อรีไซเคิล',
                    value: [
                        '3%',
                        44922.11,
                        2794420.15,
                        310490.00
                    ],
                },
                {
                    txt: 'ทำอาหารสัตว์',
                    value: [
                        '2%',
                        23260.93,
                        2022381.03,
                        224709.00
                    ],
                },
                {
                    txt: 'ทำปุ๋ยหมัก/ดิน',
                    value: [
                        '0%',
                        4205.02,
                        2720.00,
                        2720.00
                    ],
                },
            ]
        },
    },
    elephant: {
        climate_name: 'โครงการ Care the Wild “ปลูกป้อง Plant & Protect” ',
        climate_desc: 'โครงการความร่วมมือปลูกป่าเพื่อเพิ่มผืนป่าและรักษาสมดุลระบบนิเวศตามธรรมชาติ',
        company_name: 'บมจ. ธนาคารกรุงศรีอยุธยา',
        climate_data: {
            greenhouse_gas_absorb: '500',
        },
        progress_detail: {
            houses: '500',
            locations: [
                {
                    area: '10 ไร่',
                    trees: ' 2,000',
                    village: 'ป่าชุมชนบ้านพุตูม',
                    location: 'จังหวัดเพชรบุรี'
                },
                {
                    area: '5 ไร่',
                    trees: ' 1,000',
                    village: 'ป่าชุมชนบ้านหลังเขา',
                    location: 'จังหวัดกาญจนบุรี'
                }
            ]
        },
    }
}