var form_register
function submit_dashboard_register() {
   var formValid = form_register.checkValidForm();
   return formValid;
}
function getObjectFileList(uploadFileObj) {
   var data = new DataTransfer();
   uploadFileObj.forEach(item => {
      data.items.add(item.obj);
   })
   return data.files;
}
function initDateInput(input) {
   var defaults = {
      pagination: '.swiper-pagination',
      slidesPerView: 3,
      freeMode: {
         enabled: true,
         sticky: true,
      },
      // loop: true,
      // loopAdditionalSlides: 1,
      direction: 'vertical',
      slideToClickedSlide: true,
      centeredSlides: true,
      slideChangeTransitionEnd: updateInput,
      mousewheel: {
         invert: true,
      },
   };

   var template = `<div class="picker">
                     <div class="swiper-container hours">
                        <div class="swiper-wrapper">
                           <div class="swiper-slide">00</div>
                           <div class="swiper-slide">01</div>
                           <div class="swiper-slide">02</div>
                           <div class="swiper-slide">03</div>
                           <div class="swiper-slide">04</div>
                           <div class="swiper-slide">05</div>
                           <div class="swiper-slide">06</div>
                           <div class="swiper-slide">07</div>
                           <div class="swiper-slide">08</div>
                           <div class="swiper-slide">09</div>
                           <div class="swiper-slide">10</div>
                           <div class="swiper-slide">11</div>
                           <div class="swiper-slide">12</div>
                           <div class="swiper-slide">13</div>
                           <div class="swiper-slide">14</div>
                           <div class="swiper-slide">15</div>
                           <div class="swiper-slide">16</div>
                           <div class="swiper-slide">17</div>
                           <div class="swiper-slide">18</div>
                           <div class="swiper-slide">19</div>
                           <div class="swiper-slide">20</div>
                           <div class="swiper-slide">21</div>
                           <div class="swiper-slide">22</div>
                           <div class="swiper-slide">23</div>
                        </div>
                     </div>
                     <div class="swiper-container minutes">
                        <div class="swiper-wrapper">
                           <div class="swiper-slide">00</div>
                           <div class="swiper-slide">01</div>
                           <div class="swiper-slide">02</div>
                           <div class="swiper-slide">03</div>
                           <div class="swiper-slide">04</div>
                           <div class="swiper-slide">05</div>
                           <div class="swiper-slide">06</div>
                           <div class="swiper-slide">07</div>
                           <div class="swiper-slide">08</div>
                           <div class="swiper-slide">09</div>
                           <div class="swiper-slide">10</div>
                           <div class="swiper-slide">11</div>
                           <div class="swiper-slide">12</div>
                           <div class="swiper-slide">13</div>
                           <div class="swiper-slide">14</div>
                           <div class="swiper-slide">15</div>
                           <div class="swiper-slide">16</div>
                           <div class="swiper-slide">17</div>
                           <div class="swiper-slide">18</div>
                           <div class="swiper-slide">19</div>
                           <div class="swiper-slide">20</div>
                           <div class="swiper-slide">21</div>
                           <div class="swiper-slide">22</div>
                           <div class="swiper-slide">23</div>
                           <div class="swiper-slide">24</div>
                           <div class="swiper-slide">25</div>
                           <div class="swiper-slide">26</div>
                           <div class="swiper-slide">27</div>
                           <div class="swiper-slide">28</div>
                           <div class="swiper-slide">29</div>
                           <div class="swiper-slide">30</div>
                           <div class="swiper-slide">31</div>
                           <div class="swiper-slide">32</div>
                           <div class="swiper-slide">33</div>
                           <div class="swiper-slide">34</div>
                           <div class="swiper-slide">35</div>
                           <div class="swiper-slide">36</div>
                           <div class="swiper-slide">37</div>
                           <div class="swiper-slide">38</div>
                           <div class="swiper-slide">39</div>
                           <div class="swiper-slide">40</div>
                           <div class="swiper-slide">41</div>
                           <div class="swiper-slide">42</div>
                           <div class="swiper-slide">43</div>
                           <div class="swiper-slide">44</div>
                           <div class="swiper-slide">45</div>
                           <div class="swiper-slide">46</div>
                           <div class="swiper-slide">47</div>
                           <div class="swiper-slide">48</div>
                           <div class="swiper-slide">49</div>
                           <div class="swiper-slide">50</div>
                           <div class="swiper-slide">51</div>
                           <div class="swiper-slide">52</div>
                           <div class="swiper-slide">53</div>
                           <div class="swiper-slide">54</div>
                           <div class="swiper-slide">55</div>
                           <div class="swiper-slide">56</div>
                           <div class="swiper-slide">57</div>
                           <div class="swiper-slide">58</div>
                           <div class="swiper-slide">59</div>
                        </div>
                     </div>
                     <div class="vizor"></div>
                  </div>`;
   
   var parent = $($('#' + input).parent());
   parent.append(template);
   var hours = new Swiper(
      parent.find('.swiper-container.hours').get(0),
      Object.assign({}, defaults, {
         initialSlide: 0
      })
   );

   var minutes = new Swiper(
      parent.find('.swiper-container.minutes').get(0),
      Object.assign({}, defaults, {
         initialSlide: 0
      })
   );
   hours.on('slideChange', updateInput)
   minutes.on('slideChange', updateInput)

   // var seconds = new Swiper('.swiper-container.seconds', defaults);

   var input = document.getElementById(input);
   input.addEventListener('focus', moveLeft);

   input.addEventListener('click', function (evt) {
      var start = input.selectionStart;
      var mod = start % 3;
      start -= mod;
      input.setSelectionRange(start, start + 2);
   });

   input.addEventListener('keydown', function (evt) {
      evt.preventDefault();
      switch (evt.keyCode) {
         case 38:
            change(true);
            break;
         case 40:
            change();
            break;
         case 37:
            moveLeft();
            break;
         case 39:
            moveRight();
            break;
      }
   });

   function moveLeft(e) {
      var start = input.selectionStart;
      var end = input.selectionEnd;

      var invalidSelection = (end - start !== 2) || (start % 3 !== 0);

      if (invalidSelection) {
         input.setSelectionRange(0, 2);
      } else if (start > 0) {
         start -= 3;
         input.setSelectionRange(start, start + 2);
      }
   }

   function moveRight() {
      var start = input.selectionStart;
      var end = input.selectionEnd;

      if (end - start !== 2) {
         input.setSelectionRange(0, 2);
      }

      if (end < input.value.length) {
         start += 3;
      }

      input.setSelectionRange(start, start + 2);
   }

   function findPart() {
      var start = input.selectionStart;
      var mod = start % 3;
      start -= mod;

      if (start === 3) {
         return minutes;
      }

      return hours;
   }

   function change(direction) {
      var part = findPart();
      direction ? part.slideNext(true, 50) : part.slidePrev(true, 50);
   }

   function updateInput() {
      if (!input) {
         return false;
      }

      var start = input.selectionStart;
      var end = input.selectionEnd;
      input.value = pad(hours.realIndex) + ':' + pad(minutes.realIndex);
      input.setSelectionRange(start, end);
   }

   function pad(v) {
      return v > 9 ? v : "0" + String(v);
   }
}

$(document).ready(function () {

   var dropdown_building = new SimpleDropdown('simple_dropdown_building')
   var dropdown_area = new SimpleDropdown('simple_dropdown_area')
   var submit_btn = $(".form_dashboard").find('[data-btn=submit_form]')
   // var get_dropdown_building_value = dropdown_building.getCurrentData()
   // let check_valid = true
   form_register = new CommonForm($('.form_dashboard.common-form').get(0));

   this.class = 'page_dashboard_register';
   this.main = $('.' + this.class);
   this.upload = {};
   this.opts = Object.freeze({
      class: {
         invalid: 'invalid',
         warpInputText: 'wrap-input-text',
         focus: 'focus'
      }
   });
   var settings = this.opts;

   // text area resize
   function input_auto_height(input_text) {
      input_text.on("input", (e) => {
         $(e.target).attr('style', 'height : ' + $(e.target).el[0].scrollHeight + 'px')
      })
   }
   var textareaInp = $('textarea.input-type-text')
   input_auto_height(textareaInp)
   function dropdown_valid() {
      let value_building = dropdown_building.getCurrentData()
      let value_area = dropdown_area.getCurrentData()
      var check_build = false;
      var check_area = false;
      if (!value_building) {
         $('#simple_dropdown_building').parents('.' + settings.class.warpInputText).addClass(settings.class.invalid)
      } else {
         $('#simple_dropdown_building').parents('.' + settings.class.warpInputText).removeClass(settings.class.invalid)
         check_build = true
      }
      if (!value_area) {
         $('#simple_dropdown_area').parents('.' + settings.class.warpInputText).addClass(settings.class.invalid)
      } else {
         $('#simple_dropdown_area').parents('.' + settings.class.warpInputText).removeClass(settings.class.invalid)
         check_area = true
      }
      return check_build && check_area;
   }
   function check_click_input(id) {
      $(id).on('click', () => {
         let delete_class = $(id).hasClass('active')
         if (delete_class) {
            setTimeout(() => {
               $(id).removeClass('active')
            }, 200);
         } else {
            setTimeout(() => {
               $(id).addClass('active')
            }, 100);
         }
      })
   }
   function check_upload_valid() {
      uploadResponsible.dropzone.removeClass('invalid')
      uploadPolicy.dropzone.removeClass('invalid')
      uploadActivity.dropzone.removeClass('invalid')
      uploadAdvertise.dropzone.removeClass('invalid')
      var policy = false
      var responsible = false
      var activity = false
      var advertise = false
      if (!uploadPolicy.path.length) {
         uploadPolicy.dropzone.addClass('invalid')
      } else {
         policy = true
      }
      if (!uploadResponsible.path.length) {
         uploadResponsible.dropzone.addClass('invalid')
      } else {
         responsible = true
      }
      if (!uploadActivity.path.length) {
         uploadActivity.dropzone.addClass('invalid')
      } else {
         activity = true
      }
      if (!uploadAdvertise.path.length) {
         uploadAdvertise.dropzone.addClass('invalid')
      } else {
         advertise = true
      }
      return policy && responsible && activity && advertise;
   }
   // event date
   check_click_input('#mon')
   check_click_input('#tue')
   check_click_input('#wed')
   check_click_input('#thu')
   check_click_input('#fri')
   check_click_input('#sat')
   check_click_input('#sun')
   initDateInput('time_open')
   initDateInput('time_off')

   // upload
   window.uploadPolicy = new UploadFile($('#upload_policy'));
   window.uploadResponsible = new UploadFile($('#upload_responsible'));
   window.uploadActivity = new UploadFile($('#activity'));
   window.uploadAdvertise = new UploadFile($('#advertise'));
   submit_btn.on('click', () => {
      var checkSubmit = submit_dashboard_register();
      var check_dropdown_list = dropdown_valid()
      var check_upload = check_upload_valid();

      if (checkSubmit.valid && check_dropdown_list && check_upload) {
         checkSubmit.form.upload_policy = getObjectFileList(window.uploadPolicy.path);
         checkSubmit.form.upload_responsible = getObjectFileList(window.uploadResponsible.path);
         checkSubmit.form.upload_activity = getObjectFileList(window.uploadActivity.path);
         checkSubmit.form.upload_advertise = getObjectFileList(window.uploadAdvertise.path);

         var dataForm = checkSubmit.form; // <<< use this object

         var form = new FormData();
         for (var key in dataForm) {
            form.append(key, dataForm[key]);
         }

         for(var i = 0; i < checkSubmit.form.upload_policy.length; i++)
         {
            form.append('upload_policy_' + i, checkSubmit.form.upload_policy[i]);
         }

         for(var i = 0; i < checkSubmit.form.upload_responsible.length; i++)
         {
            form.append('upload_responsible_' + i, checkSubmit.form.upload_responsible[i]);
         }

         for(var i = 0; i < checkSubmit.form.upload_activity.length; i++)
         {
            form.append('upload_activity_' + i, checkSubmit.form.upload_activity[i]);
         }

         for(var i = 0; i < checkSubmit.form.upload_advertise.length; i++)
         {
            form.append('upload_advertise_' + i, checkSubmit.form.upload_advertise[i]);
         }

         // Post Form Here
         var xhr = new XMLHttpRequest();
         var url = base_url + "member/register_carethewhale";
         xhr.open("POST", url, true);
         xhr.onload = function () {
             
            if(this.responseText == 'success')
            {
               window.location = base_url + 'member/register_success';
            }

         };
         xhr.send(form);

         //window.location.href = 'thank-you-dashboard-whale.html';
      }
   })
});