function sideBarTmpl() {
    var html = '';
    html += '<div class="sidebar-menu">';
        html += '<div class="wrap-menu flex column">';
            html += '<a href="' + base_url + 'dashboard" class="menu" data-menu="dashboard" inviewSpy="100">';
                html += '<span class="icon-grid"></span>';
                html += '<span class="menu-txt show-940 f_med">Dashboard</span>';
            html += '</a>';
            html += '<a href="' + base_url + 'member/register" class="menu" data-menu="carethebear" inviewSpy="150">';
                html += '<span class="icon-bear"></span>';
                html += '<span class="menu-txt show-940 f_med">Care The Bear</span>';
            html += '</a>';
            
            html += '<a href="' + base_url + 'member/register_carethewhale" class="menu" data-menu="carethewhale" inviewSpy="200">';
                html += '<span class="icon-whale"></span>';
                html += '<span class="menu-txt show-940 f_med">Care The Whale</span>';
            html += '</a>';
            html += '<a href="' + base_url + 'dashboard/carethewild" class="menu" data-menu="carethewild" inviewSpy="250">';
                html += '<span class="icon-elephant"></span>';
                html += '<span class="menu-txt show-940 f_med">Care The Wild</span>';
            html += '</a>';
            html += '<a href="' + base_url + 'dashboard/outcome" class="menu" data-menu="outcome" inviewSpy="300">';
                html += '<span class="icon-pie-chart"></span>';
                html += '<span class="menu-txt show-940 f_med">Summary</span>';
            html += '</a>';
            html += '<a href="#" class="menu" data-menu="crm" inviewSpy="350">';
                html += '<span class="icon-phone"></span>';
                html += '<span class="menu-txt show-940 f_med">CRM</span>';
            html += '</a>';
            html += '<a href="' + base_url + 'dashboard/tgo" class="menu" data-menu="tgo" inviewSpy="400">';
                html += '<span class="icon-world-tree"></span>';
                html += '<span class="menu-txt show-940 f_med">LESS Project</span>';
            html += '</a>';
        html += '</div>';
    html += '</div>';
    return html;
}

$(document).ready(function() {
    $('#content').prepend(sideBarTmpl());
    inviewSpy();
    var path = window.location.pathname;
    var menues = $('.sidebar-menu .menu');
    menues.el.forEach(menu => {
        $(menu).on('click', function() {
            menues.removeClass('active');
            $(this).addClass('active');
        });
        var pathSplitSlash = path.replace('/','').replace('.html','').split('/');
        if ($(menu).data('menu') == pathSplitSlash[pathSplitSlash.length-1]) {
            $(menu).addClass('active');
        }
    });
});