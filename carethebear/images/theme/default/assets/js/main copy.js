(function() {
    $('.banner').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        items: 1
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.slide-article').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        items: 1
    });

    $('.slide-activity').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                slideBy: 1
            },
            768: {
                items: 3,
                margin: 30,
                slideBy: 3
            }
        },
        onInitialized: function()
        {
            $('.slide-activity .owl-dot').each(function(){
                $(this).children('span').text($(this).index()+1);
            });
            $('.slide-activity .owl-nav').css('width', ($('.slide-activity .owl-dots').innerWidth()+30)+'px');
        }
    });

    $('.slide-activity').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                slideBy: 1
            },
            768: {
                items: 3,
                margin: 30,
                slideBy: 3
            }
        },
        onInitialized: function()
        {
            $('.slide-activity .owl-dot').each(function(){
                $(this).children('span').text($(this).index()+1);
            });
            $('.slide-activity .owl-nav').css('width', ($('.slide-activity .owl-dots').innerWidth()+30)+'px');
        }
    });

    $('.slide-home-activity').owlCarousel({
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            768: {
                items: 3,
                margin: 20
            }
        }
    });

    $('.navbar-toggle, #overlay').click(function(){
        $('body').toggleClass('show-menu');
    });

    $('.gallery').each(function(){
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
              enabled:true
            }
        });
    });

    AOS.init({
        duration: 800
    });

})();