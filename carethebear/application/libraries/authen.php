<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class authen {

    public $id;
	public $is_login;
    public $is_developer;
	public $is_admin;
	public $username;
	public $user_data;
	public $controller;
	public $function;

    public function __construct()
    {
        $CI =& get_instance();
		$CI->load->helper('url');
        
        if(@$_SESSION['set_social_impact_admin']['id'] > 0)
        {
            $this->id = $_SESSION['set_social_impact_admin']['id'];
            $this->username = $_SESSION['set_social_impact_admin']['username'];
            $this->user_data = $_SESSION['set_social_impact_admin'];

            $this->is_login = true;
            
            if($_SESSION['set_social_impact_admin']['role_id'] == 1)
            {
                $this->is_developer = true;
                $this->is_admin = true;
            }
            else if($_SESSION['set_social_impact_admin']['role_id'] == 2)
            {
                $this->is_developer = false;
                $this->is_admin = true;
            }
            else
            {
                $this->is_developer = false;
                $this->is_admin = false;
            }
        }
        else
        {
            $this->is_login = false;
            $this->is_developer = false;
            $this->is_admin = false;
        }

        $this->controller = $CI->uri->segment(2);
        $this->function = $CI->uri->segment(3);

        if($this->is_login == false)
        {
            if($this->controller != "user")
            {
                redirect('/backend/user/login');
            }
            else
            {
                if($this->function != "login" && $this->function != "forget_password" && $this->function != "reset_password")
                {
                    redirect('/backend/user/login');
                }
            }
        }
        else
        {
            if($this->controller == "user" && ($this->function == "login" || $this->function == "forget_password" || $this->function == "reset_password"))
            {
                redirect('/backend');
            }
        }
    }
}

?>