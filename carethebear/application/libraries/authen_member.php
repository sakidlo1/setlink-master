<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class authen_member {

    public $id;
    public $member_data;
    public $controller;
    public $function;
    public $is_login;
    public $lang;
    public $lang_code;
    
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->helper('url');
		
		if($CI->input->get('JWT') != '')
		{
			$token = $CI->input->get('JWT');
			$authorization = "Authorization: Bearer ".$token;

			if(config_item('base_url') == 'https://climatecare.setsocialimpact.com/carethebear/')
            {
                $url = 'https://www.setlink.set.or.th/api/user/profile';
            }
            else
            {
                $url = 'https://test.setlink.set.or.th/api/user/profile';
            }

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			$res = curl_exec($ch);
			curl_close($ch);
			$result = json_decode($res, true);
			
			if(@$result['id'] > 0)
			{
				$_SESSION['climate_member'] = $result;
			}
		}
		
		if(@$_SESSION['carethebear_member']['id'] <= 0 && $_SESSION['climate_member']['id'] > 0)
		{
			if($_SESSION['climate_member']['email'] != '')
			{
				$email = $_SESSION['climate_member']['email'];
				$CI->load->model('member_model');
				$data = $CI->member_model->auto_login_by_email($email);
				if(@$data['id'] > 0)
				{
					$_SESSION['carethebear_member'] = $data;
					redirect('/');
				}
			}
		}
        
        if(@$_SESSION['carethebear_member']['id'] > 0)
        {
            $this->id = $_SESSION['carethebear_member']['id'];
            $this->member_data = $_SESSION['carethebear_member'];
            $this->is_login = true;
        }
        else
        {
            $this->is_login = false;
        }
        
        $this->controller = $CI->uri->segment(1);
        $this->function = $CI->uri->segment(2);
    }
}

?>