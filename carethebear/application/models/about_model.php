<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class about_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_content_by_code($code)
    {
        $this->db->select('*');
        $this->db->from('content');
        $this->db->where('code', $code);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_company()
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }
}