<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class layout_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_banner()
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result_array();
    }
}