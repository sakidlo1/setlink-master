<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_bear_stats()
    {
        $json = file_get_contents(config_item('base_url').'api/get_stats');
        $json=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $json);
        return json_decode($json, true);
    }

    function get_company()
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_content_by_code($code)
    {
        $this->db->select('*');
        $this->db->from('content');
        $this->db->where('code', $code);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_article()
    {
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('status', 'Y');
        $this->db->order_by('created_on', 'desc');
        $this->db->limit(3);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_news()
    {
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('status', 'Y');
        $this->db->order_by('created_on', 'desc');
        $this->db->limit(3);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_tips()
    {
        $this->db->select('*');
        $this->db->from('tips');
        $this->db->where('status', 'Y');
        $this->db->order_by('created_on', 'desc');
        $this->db->limit(6);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_latest_activity()
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("CONCAT(activity.activity_date, ' ', activity.start_on) <= NOW()");
        $this->db->order_by('activity.activity_date desc, activity.start_on desc');
        $this->db->limit(6);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_year_activity()
    {
        $this->db->select('MIN(YEAR(activity.activity_date)) as min_year, MAX(YEAR(activity.activity_date)) as max_year');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("CONCAT(activity.activity_date, ' ', activity.start_on) <= NOW()");
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_summary_cf($from, $to)
    {
        $this->db->select('sum(activity.cf_care_total) as sum_cf');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where("YEAR(activity.activity_date) >= '".$from."'");
        $this->db->where("YEAR(activity.activity_date) <= '".$to."'");
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("CONCAT(activity.activity_date, ' ', activity.start_on) <= NOW()");
        $query = $this->db->get();
        $data = $query->row_array();

        $this->db->select('count(distinct member_id) as sum_company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where("YEAR(activity.activity_date) >= '".$from."'");
        $this->db->where("YEAR(activity.activity_date) <= '".$to."'");
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("CONCAT(activity.activity_date, ' ', activity.start_on) <= NOW()");
        $query = $this->db->get();
        $data2 = $query->row_array();

        return array('total_company' => $data2['sum_company'], 'total_cf' => $data['sum_cf'], 'total_tree' => number_format(($data['sum_cf'] / 9), 2));
    }

    function get_year_project()
    {
        $this->db->select('MIN(YEAR(project.start_date)) as min_year, MAX(YEAR(project.end_date)) as max_year');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("CONCAT(project.start_date, ' ', project.start_on) <= NOW()");
        $this->db->where("(CONCAT(project.end_date, ' ', project.end_on) <= NOW() or CONCAT(project.end_date, ' ', project.end_on) >= NOW())");
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_project_summary_cf($from, $to)
    {
        $this->db->select('sum(project.cf_care_total) as sum_cf');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where("((YEAR(project.start_date) <= '".$from."' and YEAR(project.end_date) >= '".$to."') or (YEAR(project.start_date) <= '".$from."' and YEAR(project.end_date) >= '".$from."' and YEAR(project.end_date) <= '".$to."') or (YEAR(project.start_date) >= '".$from."' and YEAR(project.end_date) <= '".$to."') or (YEAR(project.start_date) >= '".$from."' and YEAR(project.start_date) <= '".$to."' and YEAR(project.end_date) >= '".$to."'))");
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("CONCAT(project.start_date, ' ', project.start_on) <= NOW()");
        $this->db->where("(CONCAT(project.end_date, ' ', project.end_on) <= NOW() or CONCAT(project.end_date, ' ', project.end_on) >= NOW())");
        $query = $this->db->get();
        $data = $query->row_array();

        $this->db->select('count(*) as sum_project');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where("((YEAR(project.start_date) <= '".$from."' and YEAR(project.end_date) >= '".$to."') or (YEAR(project.start_date) <= '".$from."' and YEAR(project.end_date) >= '".$from."' and YEAR(project.end_date) <= '".$to."') or (YEAR(project.start_date) >= '".$from."' and YEAR(project.end_date) <= '".$to."') or (YEAR(project.start_date) >= '".$from."' and YEAR(project.start_date) <= '".$to."' and YEAR(project.end_date) >= '".$to."'))");
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("CONCAT(project.start_date, ' ', project.start_on) <= NOW()");
        $this->db->where("(CONCAT(project.end_date, ' ', project.end_on) <= NOW() or CONCAT(project.end_date, ' ', project.end_on) >= NOW())");
        $query = $this->db->get();
        $data2 = $query->row_array();

        return array('total_project' => $data2['sum_project'], 'total_cf' => $data['sum_cf'], 'total_tree' => number_format(($data['sum_cf'] / 9), 2));
    }
}