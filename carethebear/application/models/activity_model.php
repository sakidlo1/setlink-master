<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class activity_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_activity_type()
    {
        $this->db->select('*');
        $this->db->from('activity_type');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_today_activity()
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("activity.activity_date = DATE(NOW())");
        $this->db->order_by('activity.activity_date desc, activity.start_on desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_all_activity()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("activity.activity_date < DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('activity_type_id') != "")
        {
            $this->db->where('activity.activity_type_id', $this->input->get('activity_type_id'));
        }

        /*
        if($this->input->get('q') != "")
        {
            if($this->input->get('type') == 'company')
            {
                $this->db->where("member.company like '%".$this->input->get('q')."%'");
            }
            else if($this->input->get('type') == 'company')
            {
                $this->db->where("(activity.name like '%".$this->input->get('q')."%' or activity.description like '%".$this->input->get('q')."%')");
            }
            else
            {
                $this->db->where("(member.company like '%".$this->input->get('q')."%' or activity.name like '%".$this->input->get('q')."%' or activity.description like '%".$this->input->get('q')."%')");
            }
        }
        */

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_all_activity($limit,$page)
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where("activity.activity_date < DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('activity_type_id') != "")
        {
            $this->db->where('activity.activity_type_id', $this->input->get('activity_type_id'));
        }

        /*
        if($this->input->get('q') != "")
        {
            if($this->input->get('type') == 'company')
            {
                $this->db->where("member.company like '%".$this->input->get('q')."%'");
            }
            else if($this->input->get('type') == 'company')
            {
                $this->db->where("(activity.name like '%".$this->input->get('q')."%' or activity.description like '%".$this->input->get('q')."%')");
            }
            else
            {
                $this->db->where("(member.company like '%".$this->input->get('q')."%' or activity.name like '%".$this->input->get('q')."%' or activity.description like '%".$this->input->get('q')."%')");
            }
        }
        */

        $this->db->order_by('activity.activity_date desc, activity.start_on desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_activity_detail_by_id($id)
    {
        $this->db->set('view_count', 'view_count + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('activity');

        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('activity.id', $id);
        $query = $this->db->get();
        $data = $query->row_array();

        $data['start_date_time'] = $data['activity_date'].' '.$data['start_on'];

        return $data;
    }

    function register_activity($id)
    {
        $data['activity_id'] = $id;
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['people'] = (($this->input->post('people') > 1) ? $this->input->post('people') : 1);
        $data['km'] = $this->input->post('km');

        $cf_total = 0;

        if($data['vehicle_id'] == 1)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/14.763)* 2.2719);
        }
        else if($data['vehicle_id'] == 2)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.111) * 2.7406);
        }
        else if($data['vehicle_id'] == 3)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.905) * 2.2609);
        }
        else if($data['vehicle_id'] == 4)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/38.655) * 2.2719);
        }
        else if($data['vehicle_id'] == 5)
        {
            $cf_total = ((($data['km']*2)/ 2.85)/32 * 2.7406);
        }
        else if($data['vehicle_id'] == 6)
        {
            $cf_total = ((($data['km']*2)/ 10.204)/15 * 2.7406);
        }
        else if($data['vehicle_id'] == 7)
        {
            $cf_total = ((($data['km']*2)/ 38.655) * 2.2719);
        }
        else if($data['vehicle_id'] == 8)
        {
            $cf_total = ((($data['km']*2)*0.1021) * 0.4999);
        }
        else if($data['vehicle_id'] == 9)
        {
            $cf_total = (($data['km']*2) * 0.07);
        }
        else if($data['vehicle_id'] == 10)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 11)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 12)
        {
            $cf_total = (($data['km']*2) * 0.1733);
        }
        else if($data['vehicle_id'] == 13)
        {
            $cf_total = 0;
        }
		// Additional
		else if($data['vehicle_id'] == 14)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/14.763)* 2.2719);
        }
		else if($data['vehicle_id'] == 15)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.111) * 2.7406);
        }
        else if($data['vehicle_id'] == 16)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/38.655) * 2.2719);
        }
        else if($data['vehicle_id'] == 17)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 18)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.905) * 2.2609);
        }
        else if($data['vehicle_id'] == 19)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.16836632);
        }
        else if($data['vehicle_id'] == 20)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.31033792);
        }
        else if($data['vehicle_id'] == 21)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.03519296);
        }
        else if($data['vehicle_id'] == 22)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.20055988);
        }
        else if($data['vehicle_id'] == 23)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.26334732);
        }
		else if($data['vehicle_id'] == 24)
        {
            $cf_total = ((($data['km']*2)/ 2.85)/32 * 2.7406);
        }
		else if($data['vehicle_id'] == 25)
        {
            $cf_total = ((($data['km']*2) * 0.1021) * 0.4999);
        }
		else if($data['vehicle_id'] == 26)
        {
            $cf_total = ((($data['km']*2)/ 10.204)/15 * 2.7406);
        }
		else if($data['vehicle_id'] == 27)
        {
            $cf_total = ((($data['km']*2)/ 38.655) * 2.2719);
        }
		else if($data['vehicle_id'] == 28)
        {
            $cf_total = (($data['km']*2) * 0.07);
        }
		else if($data['vehicle_id'] == 29)
        {
            $cf_total = (($data['km']*2) * 0.04139172);
        }
		else if($data['vehicle_id'] == 30)
        {
            $cf_total = (($data['km']*2) * 0.07688462);
        }
		else if($data['vehicle_id'] == 31)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/14.763)* 2.2719);
        }
		else if($data['vehicle_id'] == 32)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.111) * 2.7406);
        }
        else if($data['vehicle_id'] == 33)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/38.655) * 2.2719);
        }
        else if($data['vehicle_id'] == 34)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 35)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.905) * 2.2609);
        }
		else if($data['vehicle_id'] == 36)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.16836632);
        }
        else if($data['vehicle_id'] == 37)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.31033792);
        }
        else if($data['vehicle_id'] == 38)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.03519296);
        }
        else if($data['vehicle_id'] == 39)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.20055988);
        }
        else if($data['vehicle_id'] == 40)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.26334732);
        }
		else if($data['vehicle_id'] == 41)
        {
            $cf_total = ((($data['km']*2)/ 2.85)/32 * 2.7406);
        }
		else if($data['vehicle_id'] == 42)
        {
            $cf_total = ((($data['km']*2) * 0.1021) * 0.4999);
        }
		else if($data['vehicle_id'] == 43)
        {
            $cf_total = ((($data['km']*2)/ 10.204)/15 * 2.7406);
        }
		else if($data['vehicle_id'] == 44)
        {
            $cf_total = ((($data['km']*2)/ 38.655) * 2.2719);
        }
		else if($data['vehicle_id'] == 45)
        {
            $cf_total = (($data['km']*2) * 0.07);
        }
		else if($data['vehicle_id'] == 46)
        {
            $cf_total = (($data['km']*2) * 0.04139172);
        }
		else if($data['vehicle_id'] == 47)
        {
            $cf_total = (($data['km']*2) * 0.07688462);
        }
        
        //$data['cf_total'] = str_replace(',', '', number_format($cf_total, 2));
        $data['cf_total'] = $cf_total;
        $data['created_on'] = date('Y-m-d H:i:s');
        $this->db->insert('activity_register', $data);

        $this->db->select('SUM(km) as sum_km, SUM(cf_total) as sum_cf_total');
        $this->db->from('activity_register');
        $this->db->where('activity_id', $id);
        $this->db->where("vehicle_id < 13");
        $query = $this->db->get();
        $activity_cf = $query->row_array();
        
        $data2['cf_care_1'] = (($activity_cf['sum_km']*2/14.763)* 2.2719 - ($activity_cf['sum_cf_total']));
		
		$this->db->select('SUM(km) as sum_km, SUM(cf_total) as sum_cf_total');
        $this->db->from('activity_register');
        $this->db->where('activity_id', $id);
        $this->db->where("vehicle_id >= 14");
        $this->db->where("vehicle_id <= 30");
        $query = $this->db->get();
        $activity_cf = $query->row_array();
        
        $data2['cf_care_1'] = $data2['cf_care_1'] + (($activity_cf['sum_km']*2/14.763)* 2.2719 - ($activity_cf['sum_cf_total']));
		
		$this->db->select('SUM(km) as sum_km, SUM(cf_total) as sum_cf_total');
        $this->db->from('activity_register');
        $this->db->where('activity_id', $id);
        $this->db->where("vehicle_id >= 31");
        $this->db->where("vehicle_id <= 47");
        $query = $this->db->get();
        $activity_cf = $query->row_array();
        
        $data2['cf_care_1'] = $data2['cf_care_1'] + $activity_cf['sum_cf_total'];

        if($data2['cf_care_1'] < 0)
        {
            $data2['cf_care_1'] = 0;
        }

        $this->db->where('id', $id);
        $this->db->update('activity', $data2);

        $this->db->set('cf_care_total', 'REPLACE(FORMAT((cf_care_1 + cf_care_2 + cf_care_3 + cf_care_4 + cf_care_5 + cf_care_6),2),\',\',\'\')', FALSE);
        $this->db->where('id', $id);
        $this->db->update('activity');
    }
}