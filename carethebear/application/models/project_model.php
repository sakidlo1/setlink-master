<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class project_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_today_project()
    {
        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("project.start_date = DATE(NOW())");
        $this->db->order_by('project.start_date desc, project.start_on desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_current_project()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("project.start_date < DATE(NOW())");
        $this->db->where("project.end_date >= DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('project_type_id') != "")
        {
            if($this->input->get('project_type_id') == '1')
            {
                $this->db->where('project.is_care_1', 'Y');
            }
            else if($this->input->get('project_type_id') == '2')
            {
                $this->db->where('project.is_care_2', 'Y');
            }
            else if($this->input->get('project_type_id') == '3')
            {
                $this->db->where('project.is_care_3', 'Y');
            }
        }

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_current_project($limit,$page)
    {
        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("project.start_date < DATE(NOW())");
        $this->db->where("project.end_date >= DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('project_type_id') != "")
        {
            if($this->input->get('project_type_id') == '1')
            {
                $this->db->where('project.is_care_1', 'Y');
            }
            else if($this->input->get('project_type_id') == '2')
            {
                $this->db->where('project.is_care_2', 'Y');
            }
            else if($this->input->get('project_type_id') == '3')
            {
                $this->db->where('project.is_care_3', 'Y');
            }
        }

        $this->db->order_by('project.start_date desc, project.start_on desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_past_project()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("project.end_date < DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('project_type_id') != "")
        {
            if($this->input->get('project_type_id') == '1')
            {
                $this->db->where('project.is_care_1', 'Y');
            }
            else if($this->input->get('project_type_id') == '2')
            {
                $this->db->where('project.is_care_2', 'Y');
            }
            else if($this->input->get('project_type_id') == '3')
            {
                $this->db->where('project.is_care_3', 'Y');
            }
        }

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function count_past_tsd()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('tsd_projects');
        $this->db->where('status', 'Y');
        $this->db->where("tsd_projects.end_date_time < DATE(NOW())");


        if($this->input->get('type') == 'tsd' && $this->input->get('project_tsd') != "")
        {
            if($this->input->get('project_tsd') == 'CD')
            {
                $this->db->where('tsd_projects.ca_type', 'CD');
            }
            else if($this->input->get('project_tsd') == 'CN')
            {
                $this->db->where('tsd_projects.ca_type', 'CN');
            }
            else if($this->input->get('project_tsd') == 'DG')
            {
                $this->db->where('tsd_projects.ca_type', 'DG');
            }
            else if($this->input->get('project_tsd') == 'MA')
            {
                $this->db->where('tsd_projects.ca_type', 'MA');
            }
            else if($this->input->get('project_tsd') == 'NR')
            {
                $this->db->where('tsd_projects.ca_type', 'NR');
            }
            else if($this->input->get('project_tsd') == 'PO')
            {
                $this->db->where('tsd_projects.ca_type', 'PO');
            }
            else if($this->input->get('project_tsd') == 'SC')
            {
                $this->db->where('tsd_projects.ca_type', 'SC');
            }
            else if($this->input->get('project_tsd') == 'XB')
            {
                $this->db->where('tsd_projects.ca_type', 'XB');
            }
            else if($this->input->get('project_tsd') == 'XD')
            {
                $this->db->where('tsd_projects.ca_type', 'XD');
            }
            else if($this->input->get('project_tsd') == 'XE')
            {
                $this->db->where('tsd_projects.ca_type', 'XE');
            }
            else if($this->input->get('project_tsd') == 'XI')
            {
                $this->db->where('tsd_projects.ca_type', 'XI');
            }
            else if($this->input->get('project_tsd') == 'XM')
            {
                $this->db->where('tsd_projects.ca_type', 'XM');
            }
            else if($this->input->get('project_tsd') == 'XO')
            {
                $this->db->where('tsd_projects.ca_type', 'XO');
            }
            else if($this->input->get('project_tsd') == 'XP')
            {
                $this->db->where('tsd_projects.ca_type', 'XP');
            }
            else if($this->input->get('project_tsd') == 'XR')
            {
                $this->db->where('tsd_projects.ca_type', 'XR');
            }
            else if($this->input->get('project_tsd') == 'XS')
            {
                $this->db->where('tsd_projects.ca_type', 'XS');
            }
            else if($this->input->get('project_tsd') == 'XT')
            {
                $this->db->where('tsd_projects.ca_type', 'XT');
            }
        }

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_past_project($limit,$page)
    {
        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where("project.end_date < DATE(NOW())");

        if($this->input->get('type') == 'company' && $this->input->get('q') != "")
        {
            $this->db->where("member.company like '%".$this->input->get('q')."%'");
        }
        else if($this->input->get('type') == 'type' && $this->input->get('project_type_id') != "")
        {
            if($this->input->get('project_type_id') == '1')
            {
                $this->db->where('project.is_care_1', 'Y');
            }
            else if($this->input->get('project_type_id') == '2')
            {
                $this->db->where('project.is_care_2', 'Y');
            }
            else if($this->input->get('project_type_id') == '3')
            {
                $this->db->where('project.is_care_3', 'Y');
            }
        }

        $this->db->order_by('project.start_date desc, project.start_on desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_past_tsd($limit,$page)
    {
        $this->db->select('tsd_projects.*');
        $this->db->from('tsd_projects');
        $this->db->where('status', 'Y');
        $this->db->where("tsd_projects.end_date_time < DATE(NOW())");

        if($this->input->get('type') == 'tsd' && $this->input->get('project_tsd') != "")
        {
            if($this->input->get('project_tsd') == 'CD')
            {
                $this->db->where('tsd_projects.ca_type', 'CD');
            }
            else if($this->input->get('project_tsd') == 'CN')
            {
                $this->db->where('tsd_projects.ca_type', 'CN');
            }
            else if($this->input->get('project_tsd') == 'DG')
            {
                $this->db->where('tsd_projects.ca_type', 'DG');
            }
            else if($this->input->get('project_tsd') == 'MA')
            {
                $this->db->where('tsd_projects.ca_type', 'MA');
            }
            else if($this->input->get('project_tsd') == 'NR')
            {
                $this->db->where('tsd_projects.ca_type', 'NR');
            }
            else if($this->input->get('project_tsd') == 'PO')
            {
                $this->db->where('tsd_projects.ca_type', 'PO');
            }
            else if($this->input->get('project_tsd') == 'SC')
            {
                $this->db->where('tsd_projects.ca_type', 'SC');
            }
            else if($this->input->get('project_tsd') == 'XB')
            {
                $this->db->where('tsd_projects.ca_type', 'XB');
            }
            else if($this->input->get('project_tsd') == 'XD')
            {
                $this->db->where('tsd_projects.ca_type', 'XD');
            }
            else if($this->input->get('project_tsd') == 'XE')
            {
                $this->db->where('tsd_projects.ca_type', 'XE');
            }
            else if($this->input->get('project_tsd') == 'XI')
            {
                $this->db->where('tsd_projects.ca_type', 'XI');
            }
            else if($this->input->get('project_tsd') == 'XM')
            {
                $this->db->where('tsd_projects.ca_type', 'XM');
            }
            else if($this->input->get('project_tsd') == 'XO')
            {
                $this->db->where('tsd_projects.ca_type', 'XO');
            }
            else if($this->input->get('project_tsd') == 'XP')
            {
                $this->db->where('tsd_projects.ca_type', 'XP');
            }
            else if($this->input->get('project_tsd') == 'XR')
            {
                $this->db->where('tsd_projects.ca_type', 'XR');
            }
            else if($this->input->get('project_tsd') == 'XS')
            {
                $this->db->where('tsd_projects.ca_type', 'XS');
            }
            else if($this->input->get('project_tsd') == 'XT')
            {
                $this->db->where('tsd_projects.ca_type', 'XT');
            }
        }

        $this->db->order_by('tsd_projects.start_date_time desc, tsd_projects.end_date_time desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_project_detail_by_id($id)
    {
        $this->db->set('view_count', 'view_count + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('project');

        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('project.id', $id);
        $query = $this->db->get();
        $data = $query->row_array();

        $data['start_date_time'] = $data['start_date'].' '.$data['start_on'];
        $data['end_date_time'] = $data['end_date'].' '.$data['end_on'];

        return $data;
    }

    function register_project($id)
    {
        $data['project_id'] = $id;
        $data['vehicle_id'] = $this->input->post('vehicle_id');
        $data['people'] = (($this->input->post('people') > 0) ? $this->input->post('people') : 0);
        $data['day'] = $this->input->post('day');
        $data['km'] = $this->input->post('km');

        $cf_total = 0;

        if($data['vehicle_id'] == 1)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/14.763)* 2.2719);
        }
        else if($data['vehicle_id'] == 2)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 11.111) * 2.7406);
        }
        else if($data['vehicle_id'] == 3)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 10.204) * 2.7406);
        }
        else if($data['vehicle_id'] == 4)
        {
            $cf_total = (((($data['km']*2)/$data['people'])/ 2.850) * 2.7406);
        }
        else if($data['vehicle_id'] == 5)
        {
            $cf_total = ((($data['km']*2) / 14.763) * 2.2719);
        }
        else if($data['vehicle_id'] == 6)
        {
            $cf_total = ((($data['km']*2) / 11.111) * 2.7406);
        }
        else if($data['vehicle_id'] == 7)
        {
            $cf_total = ((($data['km']*2) / 11.905) * 2.2609);
        }
        else if($data['vehicle_id'] == 8)
        {
            $cf_total = ((($data['km']*2) / 2.85) / 32 * 2.7406);
        }
        else if($data['vehicle_id'] == 9)
        {
            $cf_total = ((($data['km']*2) / 10.204) / 15 * 2.7406);
        }
        else if($data['vehicle_id'] == 10)
        {
            $cf_total = ((($data['km']*2) / 38.655) * 2.2719);
        }
        else if($data['vehicle_id'] == 11)
        {
            $cf_total = ((($data['km']*2) *0.1021) * 0.4999);
        }
        else if($data['vehicle_id'] == 12)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 13)
        {
            $cf_total = 0;
        }
        else if($data['vehicle_id'] == 14)
        {
            $cf_total = (($data['km']*2) * 0.07);
        }
        else if($data['vehicle_id'] == 15)
        {
            $cf_total = (($data['km']*2) * 0.1733);
        }
        else if($data['vehicle_id'] == 16)
        {
            $cf_total = ($data['km']* 0.1143);
        }
        else if($data['vehicle_id'] == 17)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.16836632);
        }
        else if($data['vehicle_id'] == 18)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.31033792);
        }
        else if($data['vehicle_id'] == 19)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.26334732);
        }
        else if($data['vehicle_id'] == 20)
        {
            $cf_total = ((($data['km']*2)/$data['people']) * 0.04139172);
        }
        
        //$data['cf_total'] = str_replace(',', '', number_format($cf_total, 2));
        $data['cf_total'] = ($cf_total * $data['day']);
        $data['created_on'] = date('Y-m-d H:i:s');
        $this->db->insert('project_register', $data);

        $this->db->select('SUM(km * day) as sum_km, SUM(cf_total) as sum_cf_total');
        $this->db->from('project_register');
        $this->db->where('project_id', $id);
        $this->db->where("((vehicle_id >= 1 and vehicle_id <= 4) or (vehicle_id >= 17 and vehicle_id <= 20))");
        $query = $this->db->get();
        $project_cf = $query->row_array();

        $this->db->select('SUM(cf_total) as sum_cf_total');
        $this->db->from('project_register');
        $this->db->where('project_id', $id);
        $this->db->where("(vehicle_id >= 5 and vehicle_id <= 16)");
        $query = $this->db->get();
        $project_cf2 = $query->row_array();
        
        $data2['cf_care_1'] = (($project_cf['sum_km']*2/14.763)* 2.2719 - ($project_cf['sum_cf_total'])) + $project_cf2['sum_cf_total'];

        if($data2['cf_care_1'] < 0)
        {
            $data2['cf_care_1'] = 0;
        }

        $this->db->where('id', $id);
        $this->db->update('project', $data2);

        $this->db->set('cf_care_total', 'REPLACE(FORMAT((cf_care_1 + cf_care_2 + cf_care_3),2),\',\',\'\')', FALSE);
        $this->db->where('id', $id);
        $this->db->update('project');
    }
}