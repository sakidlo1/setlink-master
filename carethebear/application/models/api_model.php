<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class api_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_summary_cf()
    {
        $this->db->select('sum(activity.cf_care_total) as sum_cf');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $query = $this->db->get();
        $activity = $query->row_array();
		
		$this->db->select('sum(project.cf_care_total) as sum_cf');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $query = $this->db->get();
        $project = $query->row_array();
		
		$this->db->select('count(*) as sum_company');
        $this->db->from('company');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $company = $query->row_array();
		
		return array('total_company' => number_format($company['sum_company'], 0), 'total_cf' => number_format((7712000 + $activity['sum_cf'] + $project['sum_cf']), 2), 'total_tree' => number_format(((7712000 + $activity['sum_cf'] + $project['sum_cf']) / 9), 2));
    }
	
	function get_company()
	{
		$this->db->select('logo, name');
        $this->db->from('company');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
	}
}