<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class care_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_content_by_code($code)
    {
        $this->db->select('*');
        $this->db->from('content');
        $this->db->where('code', $code);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }
}