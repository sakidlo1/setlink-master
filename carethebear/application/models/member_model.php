<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class member_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_activity_type()
    {
        $this->db->select('*');
        $this->db->from('activity_type');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_company_type()
    {
        $this->db->select('*');
        $this->db->from('company_type');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        $company_type = $query->result_array();

        $this->db->select('*');
        $this->db->from('company_group');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        $company_group = $query->result_array();

        $data = [];
        foreach($company_type as $key => $value)
        {
            $data[$key] = $value;
            $data[$key]['item'] = [];

            $_data = [];
            foreach($company_group as $key2 => $value2)
            {
                if($value['id'] == $value2['company_type_id'])
                {
                    $_data[] = $value2;
                }
            }

            if(count($_data) > 0)
            {
                $data[$key]['item'] = $_data;
            }
            else
            {
                $value['company_type_id'] = $value['id'];
                $value['id'] = 0;
                $data[$key]['item'][] = $value;
            }
        }

        return $data;
    }

    function check_email_exists($input)
    {
        if(@$input['email'] == "")
        {
            return true;
        }

        $this->db->select('count(*) as count_rec');
        $this->db->from('member');
        $this->db->where('email', $input['email']);
        $query = $this->db->get();

        $data = $query->row_array();
        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function check_username_exists($input)
    {
        if(@$input['username'] == "")
        {
            return true;
        }

        $this->db->select('count(*) as count_rec');
        $this->db->from('member');
        $this->db->where('username', $input['username']);
        $query = $this->db->get();

        $data = $query->row_array();
        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function register($salt)
    {
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['password'] = md5($this->input->post('password'));
        $data['company'] = $this->input->post('company');
        $data['logo'] = '';
        $data['company_type_id'] = explode('-', $this->input->post('company_type'))[0];
        $data['company_group_id'] = explode('-', $this->input->post('company_type'))[1];
        $data['name'] = $this->input->post('name');
        $data['position'] = $this->input->post('position');
        $data['department'] = $this->input->post('department');
        $data['address'] = $this->input->post('address');
        $data['tel'] = $this->input->post('tel');
        $data['mobile'] = $this->input->post('mobile');
        $data['is_set_staff'] = (($this->input->post('is_set_staff') == 'Y') ? 'Y' : 'N');
        $data['salt'] = $salt;
        $data['status'] = 'N';
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $data['last_login'] = date('Y-m-d H:i:s');
        $this->db->insert('member', $data);
        
        return $this->db->insert_id();
    }

    function update_logo($id, $logo)
    {
        $data['logo'] = config_item('image_url').'upload/member/'.($id%4000).'/'.$id.'/'.$logo;
        $this->db->where('id', $id);
        $this->db->update('member', $data);
    }

    function get_salt($email)
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            return $data['salt'];
        }
        else
        {
            return false;
        }
    }

    function verify_email($email)
    {
        $data['status'] = 'Y';
        $data['salt'] = '';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('email', $email);
        $this->db->where('status', 'N');
        $this->db->update('member', $data);
    }

    function get_dashboard_stats(){
        $data = [
            'total' => [
                'cf' => 0,
                'tree' => 0
            ],
            'bear' => [
                'cf' => '0.00',
                'tree' => '0'
            ],
            'whale' => [
                'weight' => '0.00',
                'cf' => '0.00',
                'tree' => '0'
            ]
        ];

        if (@$this->authen->member_data['id'] > 0) {
            $this->db->select('sum(activity.cf_care_total) as sum_cf');
            $this->db->from('activity');
            $this->db->where('activity.status', 'Y');
            $this->db->where('activity.member_id', $this->authen->member_data['id']);
            $query = $this->db->get();
            $activity = $query->row_array();

            $this->db->select('sum(project.cf_care_total) as sum_cf');
            $this->db->from('project');
            $this->db->where('project.status', 'Y');
            $this->db->where('project.member_id', $this->authen->member_data['id']);
            $query = $this->db->get();
            $project = $query->row_array();

            $this->db->select('sum(tsd_projects.cf_care_total) as sum_cf');
            $this->db->from('tsd_projects');
            $this->db->join('member', 'member.company_master_id = tsd_projects.company_master_id');
            $this->db->where('member.id', $this->authen->member_data['id']);
            $query = $this->db->get();
            $tsd_project = $query->row_array();

            $data['bear']['cf'] = str_replace(',', '', number_format(($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']), 2));
            $data['bear']['tree'] = str_replace(',', '', number_format((($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + ($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']);
            $data['total']['tree'] = $data['total']['tree'] + (($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9);
        } else {
            $data['bear']['cf'] = '0.00';
            $data['bear']['tree'] = '0';
        }

        if (@$this->authen->member_data['company_id'] > 0) {
            $this->whaledb->select("sum(total_weight) as total_weight, sum(total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['company_id']);
            $query = $this->whaledb->get();
            $summary = $query->row_array();

            $data['whale']['weight'] = str_replace(',', '', number_format($summary['total_weight'], 2));
            $data['whale']['cf'] = str_replace(',', '', number_format($summary['total_cf'], 2));
            $data['whale']['tree'] = str_replace(',', '', number_format(($summary['total_cf'] / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + $summary['total_cf'];
            $data['total']['tree'] = $data['total']['tree'] + ($summary['total_cf'] / 9);
        } else {
            $data['whale']['weight'] = '0.00';
            $data['whale']['cf'] = '0.00';
            $data['whale']['tree'] = '0';
        }

        $data['total']['cf'] = str_replace(',', '', number_format(($data['total']['cf'] ), 0));
        $data['total']['tree'] = str_replace(',', '', number_format($data['total']['tree'], 0));

        return $data;
    }
    function get_dashboard_stats_bk()
    {
        $data = [
            'total' => [
                'cf' => 0,
                'tree' => 0
            ],
            'bear' => [
                'cf' => '0.00',
                'tree' => '0'
            ],
            'whale' => [
                'weight' => '0.00',
                'cf' => '0.00',
                'tree' => '0'
            ]
        ];

        if (@$this->authen->member_data['carethebear']['id'] > 0) {
            $this->beardb->select('sum(activity.cf_care_total) as sum_cf');
            $this->beardb->from('activity');
            $this->beardb->where('activity.status', 'Y');
            $this->beardb->where('activity.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $activity = $query->row_array();

            $this->beardb->select('sum(project.cf_care_total) as sum_cf');
            $this->beardb->from('project');
            $this->beardb->where('project.status', 'Y');
            $this->beardb->where('project.member_id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $project = $query->row_array();

            $this->beardb->select('sum(tsd_projects.cf_care_total) as sum_cf');
            $this->beardb->from('tsd_projects');
            $this->beardb->join('member', 'member.company_master_id = tsd_projects.company_master_id');
            $this->beardb->where('member.id', $this->authen->member_data['carethebear']['id']);
            $query = $this->beardb->get();
            $tsd_project = $query->row_array();

            $data['bear']['cf'] = str_replace(',', '', number_format(($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']), 2));
            $data['bear']['tree'] = str_replace(',', '', number_format((($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + ($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']);
            $data['total']['tree'] = $data['total']['tree'] + (($activity['sum_cf'] + $project['sum_cf'] + $tsd_project['sum_cf']) / 9);
        } else {
            $data['bear']['cf'] = '0.00';
            $data['bear']['tree'] = '0';
        }

        if (@$this->authen->member_data['carethewhale']['company_id'] > 0) {
            $this->whaledb->select("sum(total_weight) as total_weight, sum(total_cf) as total_cf", false);
            $this->whaledb->from('company_sort_last');
            $this->whaledb->where('company_sort_last.status', 'Y');
            $this->whaledb->where('company_sort_last.company_id', $this->authen->member_data['carethewhale']['company_id']);
            $query = $this->whaledb->get();
            $summary = $query->row_array();

            $data['whale']['weight'] = str_replace(',', '', number_format($summary['total_weight'], 2));
            $data['whale']['cf'] = str_replace(',', '', number_format($summary['total_cf'], 2));
            $data['whale']['tree'] = str_replace(',', '', number_format(($summary['total_cf'] / 9), 0));

            $data['total']['cf'] = $data['total']['cf'] + $summary['total_cf'];
            $data['total']['tree'] = $data['total']['tree'] + ($summary['total_cf'] / 9);
        } else {
            $data['whale']['weight'] = '0.00';
            $data['whale']['cf'] = '0.00';
            $data['whale']['tree'] = '0';
        }

        $data['total']['cf'] = str_replace(',', '', number_format(($data['total']['cf'] ), 0));
        $data['total']['tree'] = str_replace(',', '', number_format($data['total']['tree'], 0));

        return $data;
    }
    function login()
    {
        $data = array();

        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id']>0)
        {
            $data2 = array();
            $data2['last_login'] = date('Y-m-d H:i:s');
            $this->db->where('id', $data['id']);
            $this->db->update('member', $data2);
        }
        
        return $data;
    }

    function auto_login_by_email($email)
    {
        $data = array();

        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('email', $email);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id']>0)
        {
            $data2 = array();
            $data2['last_login'] = date('Y-m-d H:i:s');
            $this->db->where('id', $data['id']);
            $this->db->update('member', $data2);
        }
        
        return $data;
    }

    function forget_password()
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('email', $this->input->post('email'));
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            $salt = md5($this->input->post('email').'-'.$data['name'].'-'.time());
            $data2['salt'] = $salt;
            $this->db->where('id', $data['id']);
            $this->db->update('member', $data2);

            return $salt;
        }
        else
        {
            return false;
        }
    }

    function reset_password()
    {
        $data['password'] = md5($this->input->post('password'));
        $data['salt'] = '';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('email', $this->input->get('email'));
        $this->db->update('member', $data);
    }

    function get_my_profile()
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('status', 'Y');
        $this->db->where('id', $this->authen->id);
        $query = $this->db->get();
        $data = $query->row_array();
        return $data;
    }

    function edit_profile()
    {
        if($this->input->post('password') != "")
        {
            $data['password'] = md5($this->input->post('password'));
        }
        
        $data['company'] = $this->input->post('company');
        $data['company_type_id'] = explode('-', $this->input->post('company_type'))[0];
        $data['company_group_id'] = explode('-', $this->input->post('company_type'))[1];
        $data['name'] = $this->input->post('name');
        $data['position'] = $this->input->post('position');
        $data['department'] = $this->input->post('department');
        $data['address'] = $this->input->post('address');
        $data['tel'] = $this->input->post('tel');
        $data['mobile'] = $this->input->post('mobile');
        $data['is_set_staff'] = (($this->input->post('is_set_staff') == 'Y') ? 'Y' : 'N');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('id', $this->authen->id);
        $this->db->update('member', $data);
    }

    function count_all_activity()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('activity');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_all_activity($limit,$page)
    {
        $this->db->select('*');
        $this->db->from('activity');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_activity()
    {
        $data['member_id'] = $this->authen->id;
        $data['activity_type_id'] = $this->input->post('activity_type_id');
        $data['name'] = $this->input->post('name');
        $data['place'] = $this->input->post('place');
        $data['activity_date'] = $this->input->post('activity_date');
        $data['start_on'] = $this->input->post('start_on').':00';
        $data['end_on'] = $this->input->post('end_on').':00';
        $data['description'] = $this->input->post('description');
        $data['image_1'] = '';
        $data['image_2'] = '';
        $data['image_3'] = '';
        $data['vdo'] = $this->input->post('vdo');
        $data['attendant'] = $this->input->post('attendant');
        $data['premium_1'] = (($this->input->post('premium_1') > 0) ? $this->input->post('premium_1') : 0);
        $data['premium_2'] = (($this->input->post('premium_2') > 0) ? $this->input->post('premium_2') : 0);
        $data['premium_3'] = (($this->input->post('premium_3') > 0) ? $this->input->post('premium_3') : 0);
        $data['premium_4'] = (($this->input->post('premium_4') > 0) ? $this->input->post('premium_4') : 0);
        $data['premium_5'] = (($this->input->post('premium_5') > 0) ? $this->input->post('premium_5') : 0);
        $data['premium_6'] = (($this->input->post('premium_6') > 0) ? $this->input->post('premium_6') : 0);
        $data['food_no_foam_1'] = (($this->input->post('food_no_foam_1') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_2'] = (($this->input->post('food_no_foam_2') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_3'] = (($this->input->post('food_no_foam_3') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_4'] = (($this->input->post('food_no_foam_4') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_5'] = (($this->input->post('food_no_foam_5') == 'Y') ? 'Y' : 'N');
        $data['food_pax_1'] = (($this->input->post('food_pax_1') > 0) ? $this->input->post('food_pax_1') : 0);
        $data['food_pax_2'] = (($this->input->post('food_pax_2') > 0) ? $this->input->post('food_pax_2') : 0);
        $data['food_pax_3'] = (($this->input->post('food_pax_3') > 0) ? $this->input->post('food_pax_3') : 0);
        $data['food_pax_4'] = (($this->input->post('food_pax_4') > 0) ? $this->input->post('food_pax_4') : 0);
        $data['food_pax_5'] = (($this->input->post('food_pax_5') > 0) ? $this->input->post('food_pax_5') : 0);
        $data['waste_1'] = (($this->input->post('waste_1') > 0) ? $this->input->post('waste_1') : 0);
        $data['waste_2'] = (($this->input->post('waste_2') > 0) ? $this->input->post('waste_2') : 0);
        $data['waste_3'] = (($this->input->post('waste_3') > 0) ? $this->input->post('waste_3') : 0);
        $data['waste_4'] = (($this->input->post('waste_4') > 0) ? $this->input->post('waste_4') : 0);
        $data['waste_5'] = (($this->input->post('waste_5') > 0) ? $this->input->post('waste_5') : 0);
        $data['waste_6'] = (($this->input->post('waste_6') > 0) ? $this->input->post('waste_6') : 0);
        $data['wood_1'] = (($this->input->post('wood_1') > 0) ? $this->input->post('wood_1') : 0);
        $data['wood_2'] = (($this->input->post('wood_2') > 0) ? $this->input->post('wood_2') : 0);
        $data['wood_3'] = (($this->input->post('wood_3') > 0) ? $this->input->post('wood_3') : 0);
        $data['wood_4'] = (($this->input->post('wood_4') > 0) ? $this->input->post('wood_4') : 0);
        $data['wood_5'] = (($this->input->post('wood_5') > 0) ? $this->input->post('wood_5') : 0);
        $data['pp_1'] = (($this->input->post('pp_1') > 0) ? $this->input->post('pp_1') : 0);
        $data['pp_2'] = (($this->input->post('pp_2') > 0) ? $this->input->post('pp_2') : 0);
        $data['pp_3'] = (($this->input->post('pp_3') > 0) ? $this->input->post('pp_3') : 0);
        $data['pp_4'] = (($this->input->post('pp_4') > 0) ? $this->input->post('pp_4') : 0);
        $data['pp_5'] = (($this->input->post('pp_5') > 0) ? $this->input->post('pp_5') : 0);
        $data['pp_kg'] = (($this->input->post('pp_kg') > 0) ? $this->input->post('pp_kg') : 0);
        $data['building_a_1'] = (($this->input->post('building_a_1') > 0) ? $this->input->post('building_a_1') : 0);
        $data['building_a_2'] = (($this->input->post('building_a_2') > 0) ? $this->input->post('building_a_2') : 0);
        $data['building_a_3'] = (($this->input->post('building_a_3') > 0) ? $this->input->post('building_a_3') : 0);
        $data['building_a_4'] = (($this->input->post('building_a_4') > 0) ? $this->input->post('building_a_4') : 0);
        $data['building_b_1'] = (($this->input->post('building_b_1') > 0) ? $this->input->post('building_b_1') : 0);
        $data['building_b_2'] = (($this->input->post('building_b_2') > 0) ? $this->input->post('building_b_2') : 0);
        $data['building_b_3'] = (($this->input->post('building_b_3') > 0) ? $this->input->post('building_b_3') : 0);
        $data['building_b_4'] = (($this->input->post('building_b_4') > 0) ? $this->input->post('building_b_4') : 0);
        $data['building_b_5'] = (($this->input->post('building_b_5') > 0) ? $this->input->post('building_b_5') : 0);
        $data['building_b_6'] = (($this->input->post('building_b_6') > 0) ? $this->input->post('building_b_6') : 0);
        $data['building_b_7'] = (($this->input->post('building_b_7') > 0) ? $this->input->post('building_b_7') : 0);
        $data['building_b_8'] = (($this->input->post('building_b_8') > 0) ? $this->input->post('building_b_8') : 0);
        $data['building_c_1'] = (($this->input->post('building_c_1') > 0) ? $this->input->post('building_c_1') : 0);
        $data['led_bulb_watt_1'] = (($this->input->post('led_bulb_watt_1') > 0) ? $this->input->post('led_bulb_watt_1') : 0);
        $data['led_bulb_watt_2'] = (($this->input->post('led_bulb_watt_2') > 0) ? $this->input->post('led_bulb_watt_2') : 0);
        $data['led_bulb_watt_3'] = (($this->input->post('led_bulb_watt_3') > 0) ? $this->input->post('led_bulb_watt_3') : 0);
        $data['led_bulb_watt_4'] = (($this->input->post('led_bulb_watt_4') > 0) ? $this->input->post('led_bulb_watt_4') : 0);
        $data['led_bulb_watt_5'] = (($this->input->post('led_bulb_watt_5') > 0) ? $this->input->post('led_bulb_watt_5') : 0);
        $data['led_bulb_watt_6'] = (($this->input->post('led_bulb_watt_6') > 0) ? $this->input->post('led_bulb_watt_6') : 0);
        $data['led_bulb_watt_7'] = (($this->input->post('led_bulb_watt_7') > 0) ? $this->input->post('led_bulb_watt_7') : 0);
        $data['led_bulb_watt_8'] = (($this->input->post('led_bulb_watt_8') > 0) ? $this->input->post('led_bulb_watt_8') : 0);
        $data['led_bulb_watt_9'] = (($this->input->post('led_bulb_watt_9') > 0) ? $this->input->post('led_bulb_watt_9') : 0);
        $data['led_bulb_watt_10'] = (($this->input->post('led_bulb_watt_10') > 0) ? $this->input->post('led_bulb_watt_10') : 0);
        $data['led_bulb_watt_11'] = (($this->input->post('led_bulb_watt_11') > 0) ? $this->input->post('led_bulb_watt_11') : 0);
        $data['led_bulb_hour_1'] = (($this->input->post('led_bulb_hour_1') > 0) ? $this->input->post('led_bulb_hour_1') : 0);
        $data['led_bulb_hour_2'] = (($this->input->post('led_bulb_hour_2') > 0) ? $this->input->post('led_bulb_hour_2') : 0);
        $data['led_bulb_hour_3'] = (($this->input->post('led_bulb_hour_3') > 0) ? $this->input->post('led_bulb_hour_3') : 0);
        $data['led_bulb_hour_4'] = (($this->input->post('led_bulb_hour_4') > 0) ? $this->input->post('led_bulb_hour_4') : 0);
        $data['led_bulb_hour_5'] = (($this->input->post('led_bulb_hour_5') > 0) ? $this->input->post('led_bulb_hour_5') : 0);
        $data['led_bulb_hour_6'] = (($this->input->post('led_bulb_hour_6') > 0) ? $this->input->post('led_bulb_hour_6') : 0);
        $data['led_bulb_hour_7'] = (($this->input->post('led_bulb_hour_7') > 0) ? $this->input->post('led_bulb_hour_7') : 0);
        $data['led_bulb_hour_8'] = (($this->input->post('led_bulb_hour_8') > 0) ? $this->input->post('led_bulb_hour_8') : 0);
        $data['led_bulb_hour_9'] = (($this->input->post('led_bulb_hour_9') > 0) ? $this->input->post('led_bulb_hour_9') : 0);
        $data['led_bulb_hour_10'] = (($this->input->post('led_bulb_hour_10') > 0) ? $this->input->post('led_bulb_hour_10') : 0);
        $data['led_bulb_hour_11'] = (($this->input->post('led_bulb_hour_11') > 0) ? $this->input->post('led_bulb_hour_11') : 0);
        $data['led_bulb_custom_watt_1'] = (($this->input->post('led_bulb_custom_watt_1') > 0) ? $this->input->post('led_bulb_custom_watt_1') : 0);
        $data['led_bulb_custom_watt_2'] = (($this->input->post('led_bulb_custom_watt_2') > 0) ? $this->input->post('led_bulb_custom_watt_2') : 0);
        $data['led_bulb_custom_watt_3'] = (($this->input->post('led_bulb_custom_watt_3') > 0) ? $this->input->post('led_bulb_custom_watt_3') : 0);
        $data['led_bulb_custom_watt_4'] = (($this->input->post('led_bulb_custom_watt_4') > 0) ? $this->input->post('led_bulb_custom_watt_4') : 0);
        $data['led_bulb_custom_watt_5'] = (($this->input->post('led_bulb_custom_watt_5') > 0) ? $this->input->post('led_bulb_custom_watt_5') : 0);
        $data['led_bulb_custom_qty_1'] = (($this->input->post('led_bulb_custom_qty_1') > 0) ? $this->input->post('led_bulb_custom_qty_1') : 0);
        $data['led_bulb_custom_qty_2'] = (($this->input->post('led_bulb_custom_qty_2') > 0) ? $this->input->post('led_bulb_custom_qty_2') : 0);
        $data['led_bulb_custom_qty_3'] = (($this->input->post('led_bulb_custom_qty_3') > 0) ? $this->input->post('led_bulb_custom_qty_3') : 0);
        $data['led_bulb_custom_qty_4'] = (($this->input->post('led_bulb_custom_qty_4') > 0) ? $this->input->post('led_bulb_custom_qty_4') : 0);
        $data['led_bulb_custom_qty_5'] = (($this->input->post('led_bulb_custom_qty_5') > 0) ? $this->input->post('led_bulb_custom_qty_5') : 0);
        $data['led_bulb_custom_hour_1'] = (($this->input->post('led_bulb_custom_hour_1') > 0) ? $this->input->post('led_bulb_custom_hour_1') : 0);
        $data['led_bulb_custom_hour_2'] = (($this->input->post('led_bulb_custom_hour_2') > 0) ? $this->input->post('led_bulb_custom_hour_2') : 0);
        $data['led_bulb_custom_hour_3'] = (($this->input->post('led_bulb_custom_hour_3') > 0) ? $this->input->post('led_bulb_custom_hour_3') : 0);
        $data['led_bulb_custom_hour_4'] = (($this->input->post('led_bulb_custom_hour_4') > 0) ? $this->input->post('led_bulb_custom_hour_4') : 0);
        $data['led_bulb_custom_hour_5'] = (($this->input->post('led_bulb_custom_hour_5') > 0) ? $this->input->post('led_bulb_custom_hour_5') : 0);
        $data['led_tube_watt_1'] = (($this->input->post('led_tube_watt_1') > 0) ? $this->input->post('led_tube_watt_1') : 0);
        $data['led_tube_watt_2'] = (($this->input->post('led_tube_watt_2') > 0) ? $this->input->post('led_tube_watt_2') : 0);
        $data['led_tube_watt_3'] = (($this->input->post('led_tube_watt_3') > 0) ? $this->input->post('led_tube_watt_3') : 0);
        $data['led_tube_watt_4'] = (($this->input->post('led_tube_watt_4') > 0) ? $this->input->post('led_tube_watt_4') : 0);
        $data['led_tube_watt_5'] = (($this->input->post('led_tube_watt_5') > 0) ? $this->input->post('led_tube_watt_5') : 0);
        $data['led_tube_watt_6'] = (($this->input->post('led_tube_watt_6') > 0) ? $this->input->post('led_tube_watt_6') : 0);
        $data['led_tube_watt_7'] = (($this->input->post('led_tube_watt_7') > 0) ? $this->input->post('led_tube_watt_7') : 0);
        $data['led_tube_watt_8'] = (($this->input->post('led_tube_watt_8') > 0) ? $this->input->post('led_tube_watt_8') : 0);
        $data['led_tube_watt_9'] = (($this->input->post('led_tube_watt_9') > 0) ? $this->input->post('led_tube_watt_9') : 0);
        $data['led_tube_watt_10'] = (($this->input->post('led_tube_watt_10') > 0) ? $this->input->post('led_tube_watt_10') : 0);
        $data['led_tube_watt_11'] = (($this->input->post('led_tube_watt_11') > 0) ? $this->input->post('led_tube_watt_11') : 0);
        $data['led_tube_hour_1'] = (($this->input->post('led_tube_hour_1') > 0) ? $this->input->post('led_tube_hour_1') : 0);
        $data['led_tube_hour_2'] = (($this->input->post('led_tube_hour_2') > 0) ? $this->input->post('led_tube_hour_2') : 0);
        $data['led_tube_hour_3'] = (($this->input->post('led_tube_hour_3') > 0) ? $this->input->post('led_tube_hour_3') : 0);
        $data['led_tube_hour_4'] = (($this->input->post('led_tube_hour_4') > 0) ? $this->input->post('led_tube_hour_4') : 0);
        $data['led_tube_hour_5'] = (($this->input->post('led_tube_hour_5') > 0) ? $this->input->post('led_tube_hour_5') : 0);
        $data['led_tube_hour_6'] = (($this->input->post('led_tube_hour_6') > 0) ? $this->input->post('led_tube_hour_6') : 0);
        $data['led_tube_hour_7'] = (($this->input->post('led_tube_hour_7') > 0) ? $this->input->post('led_tube_hour_7') : 0);
        $data['led_tube_hour_8'] = (($this->input->post('led_tube_hour_8') > 0) ? $this->input->post('led_tube_hour_8') : 0);
        $data['led_tube_hour_9'] = (($this->input->post('led_tube_hour_9') > 0) ? $this->input->post('led_tube_hour_9') : 0);
        $data['led_tube_hour_10'] = (($this->input->post('led_tube_hour_10') > 0) ? $this->input->post('led_tube_hour_10') : 0);
        $data['led_tube_hour_11'] = (($this->input->post('led_tube_hour_11') > 0) ? $this->input->post('led_tube_hour_11') : 0);
        $data['led_tube_custom_watt_1'] = (($this->input->post('led_tube_custom_watt_1') > 0) ? $this->input->post('led_tube_custom_watt_1') : 0);
        $data['led_tube_custom_watt_2'] = (($this->input->post('led_tube_custom_watt_2') > 0) ? $this->input->post('led_tube_custom_watt_2') : 0);
        $data['led_tube_custom_watt_3'] = (($this->input->post('led_tube_custom_watt_3') > 0) ? $this->input->post('led_tube_custom_watt_3') : 0);
        $data['led_tube_custom_watt_4'] = (($this->input->post('led_tube_custom_watt_4') > 0) ? $this->input->post('led_tube_custom_watt_4') : 0);
        $data['led_tube_custom_watt_5'] = (($this->input->post('led_tube_custom_watt_5') > 0) ? $this->input->post('led_tube_custom_watt_5') : 0);
        $data['led_tube_custom_qty_1'] = (($this->input->post('led_tube_custom_qty_1') > 0) ? $this->input->post('led_tube_custom_qty_1') : 0);
        $data['led_tube_custom_qty_2'] = (($this->input->post('led_tube_custom_qty_2') > 0) ? $this->input->post('led_tube_custom_qty_2') : 0);
        $data['led_tube_custom_qty_3'] = (($this->input->post('led_tube_custom_qty_3') > 0) ? $this->input->post('led_tube_custom_qty_3') : 0);
        $data['led_tube_custom_qty_4'] = (($this->input->post('led_tube_custom_qty_4') > 0) ? $this->input->post('led_tube_custom_qty_4') : 0);
        $data['led_tube_custom_qty_5'] = (($this->input->post('led_tube_custom_qty_5') > 0) ? $this->input->post('led_tube_custom_qty_5') : 0);
        $data['led_tube_custom_hour_1'] = (($this->input->post('led_tube_custom_hour_1') > 0) ? $this->input->post('led_tube_custom_hour_1') : 0);
        $data['led_tube_custom_hour_2'] = (($this->input->post('led_tube_custom_hour_2') > 0) ? $this->input->post('led_tube_custom_hour_2') : 0);
        $data['led_tube_custom_hour_3'] = (($this->input->post('led_tube_custom_hour_3') > 0) ? $this->input->post('led_tube_custom_hour_3') : 0);
        $data['led_tube_custom_hour_4'] = (($this->input->post('led_tube_custom_hour_4') > 0) ? $this->input->post('led_tube_custom_hour_4') : 0);
        $data['led_tube_custom_hour_5'] = (($this->input->post('led_tube_custom_hour_5') > 0) ? $this->input->post('led_tube_custom_hour_5') : 0);
        $data['cf_care_1'] = 0;
        $data['cf_care_2'] = 0;
        $data['cf_care_3'] = 0;
        $data['cf_care_4'] = 0;
        $data['cf_care_5'] = 0;
        $data['cf_care_6'] = 0;
        $data['cf_care_total'] = 0;
        $data['view_count'] = 0;
        $data['status'] = 'Y';
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->db->insert('activity', $data);
        
        $_id = $this->db->insert_id();

        $this->update_cf_activity($_id, $data);

        return $_id;
    }

    function update_image_1($id, $member_id, $image)
    {
        $data['image_1'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('activity', $data);
    }

    function update_image_2($id, $member_id, $image)
    {
        $data['image_2'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('activity', $data);
    }

    function update_image_3($id, $member_id, $image)
    {
        $data['image_3'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('activity', $data);
    }

    function get_activity_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('activity');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_activity($id)
    {
        $data['activity_type_id'] = $this->input->post('activity_type_id');
        $data['name'] = $this->input->post('name');
        $data['place'] = $this->input->post('place');
        $data['activity_date'] = $this->input->post('activity_date');
        $data['start_on'] = $this->input->post('start_on').':00';
        $data['end_on'] = $this->input->post('end_on').':00';
        $data['description'] = $this->input->post('description');
        $data['vdo'] = $this->input->post('vdo');
        $data['attendant'] = $this->input->post('attendant');
        $data['premium_1'] = (($this->input->post('premium_1') > 0) ? $this->input->post('premium_1') : 0);
        $data['premium_2'] = (($this->input->post('premium_2') > 0) ? $this->input->post('premium_2') : 0);
        $data['premium_3'] = (($this->input->post('premium_3') > 0) ? $this->input->post('premium_3') : 0);
        $data['premium_4'] = (($this->input->post('premium_4') > 0) ? $this->input->post('premium_4') : 0);
        $data['premium_5'] = (($this->input->post('premium_5') > 0) ? $this->input->post('premium_5') : 0);
        $data['premium_6'] = (($this->input->post('premium_6') > 0) ? $this->input->post('premium_6') : 0);
        $data['food_no_foam_1'] = (($this->input->post('food_no_foam_1') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_2'] = (($this->input->post('food_no_foam_2') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_3'] = (($this->input->post('food_no_foam_3') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_4'] = (($this->input->post('food_no_foam_4') == 'Y') ? 'Y' : 'N');
        $data['food_no_foam_5'] = (($this->input->post('food_no_foam_5') == 'Y') ? 'Y' : 'N');
        $data['food_pax_1'] = (($this->input->post('food_pax_1') > 0) ? $this->input->post('food_pax_1') : 0);
        $data['food_pax_2'] = (($this->input->post('food_pax_2') > 0) ? $this->input->post('food_pax_2') : 0);
        $data['food_pax_3'] = (($this->input->post('food_pax_3') > 0) ? $this->input->post('food_pax_3') : 0);
        $data['food_pax_4'] = (($this->input->post('food_pax_4') > 0) ? $this->input->post('food_pax_4') : 0);
        $data['food_pax_5'] = (($this->input->post('food_pax_5') > 0) ? $this->input->post('food_pax_5') : 0);
        $data['waste_1'] = (($this->input->post('waste_1') > 0) ? $this->input->post('waste_1') : 0);
        $data['waste_2'] = (($this->input->post('waste_2') > 0) ? $this->input->post('waste_2') : 0);
        $data['waste_3'] = (($this->input->post('waste_3') > 0) ? $this->input->post('waste_3') : 0);
        $data['waste_4'] = (($this->input->post('waste_4') > 0) ? $this->input->post('waste_4') : 0);
        $data['waste_5'] = (($this->input->post('waste_5') > 0) ? $this->input->post('waste_5') : 0);
        $data['waste_6'] = (($this->input->post('waste_6') > 0) ? $this->input->post('waste_6') : 0);
        $data['wood_1'] = (($this->input->post('wood_1') > 0) ? $this->input->post('wood_1') : 0);
        $data['wood_2'] = (($this->input->post('wood_2') > 0) ? $this->input->post('wood_2') : 0);
        $data['wood_3'] = (($this->input->post('wood_3') > 0) ? $this->input->post('wood_3') : 0);
        $data['wood_4'] = (($this->input->post('wood_4') > 0) ? $this->input->post('wood_4') : 0);
        $data['wood_5'] = (($this->input->post('wood_5') > 0) ? $this->input->post('wood_5') : 0);
        $data['pp_1'] = (($this->input->post('pp_1') > 0) ? $this->input->post('pp_1') : 0);
        $data['pp_2'] = (($this->input->post('pp_2') > 0) ? $this->input->post('pp_2') : 0);
        $data['pp_3'] = (($this->input->post('pp_3') > 0) ? $this->input->post('pp_3') : 0);
        $data['pp_4'] = (($this->input->post('pp_4') > 0) ? $this->input->post('pp_4') : 0);
        $data['pp_5'] = (($this->input->post('pp_5') > 0) ? $this->input->post('pp_5') : 0);
        $data['pp_kg'] = (($this->input->post('pp_kg') > 0) ? $this->input->post('pp_kg') : 0);
        $data['building_a_1'] = (($this->input->post('building_a_1') > 0) ? $this->input->post('building_a_1') : 0);
        $data['building_a_2'] = (($this->input->post('building_a_2') > 0) ? $this->input->post('building_a_2') : 0);
        $data['building_a_3'] = (($this->input->post('building_a_3') > 0) ? $this->input->post('building_a_3') : 0);
        $data['building_a_4'] = (($this->input->post('building_a_4') > 0) ? $this->input->post('building_a_4') : 0);
        $data['building_b_1'] = (($this->input->post('building_b_1') > 0) ? $this->input->post('building_b_1') : 0);
        $data['building_b_2'] = (($this->input->post('building_b_2') > 0) ? $this->input->post('building_b_2') : 0);
        $data['building_b_3'] = (($this->input->post('building_b_3') > 0) ? $this->input->post('building_b_3') : 0);
        $data['building_b_4'] = (($this->input->post('building_b_4') > 0) ? $this->input->post('building_b_4') : 0);
        $data['building_b_5'] = (($this->input->post('building_b_5') > 0) ? $this->input->post('building_b_5') : 0);
        $data['building_b_6'] = (($this->input->post('building_b_6') > 0) ? $this->input->post('building_b_6') : 0);
        $data['building_b_7'] = (($this->input->post('building_b_7') > 0) ? $this->input->post('building_b_7') : 0);
        $data['building_b_8'] = (($this->input->post('building_b_8') > 0) ? $this->input->post('building_b_8') : 0);
        $data['building_c_1'] = (($this->input->post('building_c_1') > 0) ? $this->input->post('building_c_1') : 0);
        $data['led_bulb_watt_1'] = (($this->input->post('led_bulb_watt_1') > 0) ? $this->input->post('led_bulb_watt_1') : 0);
        $data['led_bulb_watt_2'] = (($this->input->post('led_bulb_watt_2') > 0) ? $this->input->post('led_bulb_watt_2') : 0);
        $data['led_bulb_watt_3'] = (($this->input->post('led_bulb_watt_3') > 0) ? $this->input->post('led_bulb_watt_3') : 0);
        $data['led_bulb_watt_4'] = (($this->input->post('led_bulb_watt_4') > 0) ? $this->input->post('led_bulb_watt_4') : 0);
        $data['led_bulb_watt_5'] = (($this->input->post('led_bulb_watt_5') > 0) ? $this->input->post('led_bulb_watt_5') : 0);
        $data['led_bulb_watt_6'] = (($this->input->post('led_bulb_watt_6') > 0) ? $this->input->post('led_bulb_watt_6') : 0);
        $data['led_bulb_watt_7'] = (($this->input->post('led_bulb_watt_7') > 0) ? $this->input->post('led_bulb_watt_7') : 0);
        $data['led_bulb_watt_8'] = (($this->input->post('led_bulb_watt_8') > 0) ? $this->input->post('led_bulb_watt_8') : 0);
        $data['led_bulb_watt_9'] = (($this->input->post('led_bulb_watt_9') > 0) ? $this->input->post('led_bulb_watt_9') : 0);
        $data['led_bulb_watt_10'] = (($this->input->post('led_bulb_watt_10') > 0) ? $this->input->post('led_bulb_watt_10') : 0);
        $data['led_bulb_watt_11'] = (($this->input->post('led_bulb_watt_11') > 0) ? $this->input->post('led_bulb_watt_11') : 0);
        $data['led_bulb_hour_1'] = (($this->input->post('led_bulb_hour_1') > 0) ? $this->input->post('led_bulb_hour_1') : 0);
        $data['led_bulb_hour_2'] = (($this->input->post('led_bulb_hour_2') > 0) ? $this->input->post('led_bulb_hour_2') : 0);
        $data['led_bulb_hour_3'] = (($this->input->post('led_bulb_hour_3') > 0) ? $this->input->post('led_bulb_hour_3') : 0);
        $data['led_bulb_hour_4'] = (($this->input->post('led_bulb_hour_4') > 0) ? $this->input->post('led_bulb_hour_4') : 0);
        $data['led_bulb_hour_5'] = (($this->input->post('led_bulb_hour_5') > 0) ? $this->input->post('led_bulb_hour_5') : 0);
        $data['led_bulb_hour_6'] = (($this->input->post('led_bulb_hour_6') > 0) ? $this->input->post('led_bulb_hour_6') : 0);
        $data['led_bulb_hour_7'] = (($this->input->post('led_bulb_hour_7') > 0) ? $this->input->post('led_bulb_hour_7') : 0);
        $data['led_bulb_hour_8'] = (($this->input->post('led_bulb_hour_8') > 0) ? $this->input->post('led_bulb_hour_8') : 0);
        $data['led_bulb_hour_9'] = (($this->input->post('led_bulb_hour_9') > 0) ? $this->input->post('led_bulb_hour_9') : 0);
        $data['led_bulb_hour_10'] = (($this->input->post('led_bulb_hour_10') > 0) ? $this->input->post('led_bulb_hour_10') : 0);
        $data['led_bulb_hour_11'] = (($this->input->post('led_bulb_hour_11') > 0) ? $this->input->post('led_bulb_hour_11') : 0);
        $data['led_bulb_custom_watt_1'] = (($this->input->post('led_bulb_custom_watt_1') > 0) ? $this->input->post('led_bulb_custom_watt_1') : 0);
        $data['led_bulb_custom_watt_2'] = (($this->input->post('led_bulb_custom_watt_2') > 0) ? $this->input->post('led_bulb_custom_watt_2') : 0);
        $data['led_bulb_custom_watt_3'] = (($this->input->post('led_bulb_custom_watt_3') > 0) ? $this->input->post('led_bulb_custom_watt_3') : 0);
        $data['led_bulb_custom_watt_4'] = (($this->input->post('led_bulb_custom_watt_4') > 0) ? $this->input->post('led_bulb_custom_watt_4') : 0);
        $data['led_bulb_custom_watt_5'] = (($this->input->post('led_bulb_custom_watt_5') > 0) ? $this->input->post('led_bulb_custom_watt_5') : 0);
        $data['led_bulb_custom_qty_1'] = (($this->input->post('led_bulb_custom_qty_1') > 0) ? $this->input->post('led_bulb_custom_qty_1') : 0);
        $data['led_bulb_custom_qty_2'] = (($this->input->post('led_bulb_custom_qty_2') > 0) ? $this->input->post('led_bulb_custom_qty_2') : 0);
        $data['led_bulb_custom_qty_3'] = (($this->input->post('led_bulb_custom_qty_3') > 0) ? $this->input->post('led_bulb_custom_qty_3') : 0);
        $data['led_bulb_custom_qty_4'] = (($this->input->post('led_bulb_custom_qty_4') > 0) ? $this->input->post('led_bulb_custom_qty_4') : 0);
        $data['led_bulb_custom_qty_5'] = (($this->input->post('led_bulb_custom_qty_5') > 0) ? $this->input->post('led_bulb_custom_qty_5') : 0);
        $data['led_bulb_custom_hour_1'] = (($this->input->post('led_bulb_custom_hour_1') > 0) ? $this->input->post('led_bulb_custom_hour_1') : 0);
        $data['led_bulb_custom_hour_2'] = (($this->input->post('led_bulb_custom_hour_2') > 0) ? $this->input->post('led_bulb_custom_hour_2') : 0);
        $data['led_bulb_custom_hour_3'] = (($this->input->post('led_bulb_custom_hour_3') > 0) ? $this->input->post('led_bulb_custom_hour_3') : 0);
        $data['led_bulb_custom_hour_4'] = (($this->input->post('led_bulb_custom_hour_4') > 0) ? $this->input->post('led_bulb_custom_hour_4') : 0);
        $data['led_bulb_custom_hour_5'] = (($this->input->post('led_bulb_custom_hour_5') > 0) ? $this->input->post('led_bulb_custom_hour_5') : 0);
        $data['led_tube_watt_1'] = (($this->input->post('led_tube_watt_1') > 0) ? $this->input->post('led_tube_watt_1') : 0);
        $data['led_tube_watt_2'] = (($this->input->post('led_tube_watt_2') > 0) ? $this->input->post('led_tube_watt_2') : 0);
        $data['led_tube_watt_3'] = (($this->input->post('led_tube_watt_3') > 0) ? $this->input->post('led_tube_watt_3') : 0);
        $data['led_tube_watt_4'] = (($this->input->post('led_tube_watt_4') > 0) ? $this->input->post('led_tube_watt_4') : 0);
        $data['led_tube_watt_5'] = (($this->input->post('led_tube_watt_5') > 0) ? $this->input->post('led_tube_watt_5') : 0);
        $data['led_tube_watt_6'] = (($this->input->post('led_tube_watt_6') > 0) ? $this->input->post('led_tube_watt_6') : 0);
        $data['led_tube_watt_7'] = (($this->input->post('led_tube_watt_7') > 0) ? $this->input->post('led_tube_watt_7') : 0);
        $data['led_tube_watt_8'] = (($this->input->post('led_tube_watt_8') > 0) ? $this->input->post('led_tube_watt_8') : 0);
        $data['led_tube_watt_9'] = (($this->input->post('led_tube_watt_9') > 0) ? $this->input->post('led_tube_watt_9') : 0);
        $data['led_tube_watt_10'] = (($this->input->post('led_tube_watt_10') > 0) ? $this->input->post('led_tube_watt_10') : 0);
        $data['led_tube_watt_11'] = (($this->input->post('led_tube_watt_11') > 0) ? $this->input->post('led_tube_watt_11') : 0);
        $data['led_tube_hour_1'] = (($this->input->post('led_tube_hour_1') > 0) ? $this->input->post('led_tube_hour_1') : 0);
        $data['led_tube_hour_2'] = (($this->input->post('led_tube_hour_2') > 0) ? $this->input->post('led_tube_hour_2') : 0);
        $data['led_tube_hour_3'] = (($this->input->post('led_tube_hour_3') > 0) ? $this->input->post('led_tube_hour_3') : 0);
        $data['led_tube_hour_4'] = (($this->input->post('led_tube_hour_4') > 0) ? $this->input->post('led_tube_hour_4') : 0);
        $data['led_tube_hour_5'] = (($this->input->post('led_tube_hour_5') > 0) ? $this->input->post('led_tube_hour_5') : 0);
        $data['led_tube_hour_6'] = (($this->input->post('led_tube_hour_6') > 0) ? $this->input->post('led_tube_hour_6') : 0);
        $data['led_tube_hour_7'] = (($this->input->post('led_tube_hour_7') > 0) ? $this->input->post('led_tube_hour_7') : 0);
        $data['led_tube_hour_8'] = (($this->input->post('led_tube_hour_8') > 0) ? $this->input->post('led_tube_hour_8') : 0);
        $data['led_tube_hour_9'] = (($this->input->post('led_tube_hour_9') > 0) ? $this->input->post('led_tube_hour_9') : 0);
        $data['led_tube_hour_10'] = (($this->input->post('led_tube_hour_10') > 0) ? $this->input->post('led_tube_hour_10') : 0);
        $data['led_tube_hour_11'] = (($this->input->post('led_tube_hour_11') > 0) ? $this->input->post('led_tube_hour_11') : 0);
        $data['led_tube_custom_watt_1'] = (($this->input->post('led_tube_custom_watt_1') > 0) ? $this->input->post('led_tube_custom_watt_1') : 0);
        $data['led_tube_custom_watt_2'] = (($this->input->post('led_tube_custom_watt_2') > 0) ? $this->input->post('led_tube_custom_watt_2') : 0);
        $data['led_tube_custom_watt_3'] = (($this->input->post('led_tube_custom_watt_3') > 0) ? $this->input->post('led_tube_custom_watt_3') : 0);
        $data['led_tube_custom_watt_4'] = (($this->input->post('led_tube_custom_watt_4') > 0) ? $this->input->post('led_tube_custom_watt_4') : 0);
        $data['led_tube_custom_watt_5'] = (($this->input->post('led_tube_custom_watt_5') > 0) ? $this->input->post('led_tube_custom_watt_5') : 0);
        $data['led_tube_custom_qty_1'] = (($this->input->post('led_tube_custom_qty_1') > 0) ? $this->input->post('led_tube_custom_qty_1') : 0);
        $data['led_tube_custom_qty_2'] = (($this->input->post('led_tube_custom_qty_2') > 0) ? $this->input->post('led_tube_custom_qty_2') : 0);
        $data['led_tube_custom_qty_3'] = (($this->input->post('led_tube_custom_qty_3') > 0) ? $this->input->post('led_tube_custom_qty_3') : 0);
        $data['led_tube_custom_qty_4'] = (($this->input->post('led_tube_custom_qty_4') > 0) ? $this->input->post('led_tube_custom_qty_4') : 0);
        $data['led_tube_custom_qty_5'] = (($this->input->post('led_tube_custom_qty_5') > 0) ? $this->input->post('led_tube_custom_qty_5') : 0);
        $data['led_tube_custom_hour_1'] = (($this->input->post('led_tube_custom_hour_1') > 0) ? $this->input->post('led_tube_custom_hour_1') : 0);
        $data['led_tube_custom_hour_2'] = (($this->input->post('led_tube_custom_hour_2') > 0) ? $this->input->post('led_tube_custom_hour_2') : 0);
        $data['led_tube_custom_hour_3'] = (($this->input->post('led_tube_custom_hour_3') > 0) ? $this->input->post('led_tube_custom_hour_3') : 0);
        $data['led_tube_custom_hour_4'] = (($this->input->post('led_tube_custom_hour_4') > 0) ? $this->input->post('led_tube_custom_hour_4') : 0);
        $data['led_tube_custom_hour_5'] = (($this->input->post('led_tube_custom_hour_5') > 0) ? $this->input->post('led_tube_custom_hour_5') : 0);
        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('id', $id);
        $this->db->update('activity', $data);

        $this->update_cf_activity($id, $data);
    }

    function update_cf_activity($id, $data)
    {
        $cf_care_2 = 0;
        if($data['premium_1'] > 0)
        {
            $cf_care_2 += (($data['premium_1']*5/1000) * 2.1020 + ($data['premium_1']*5/1000) * 2.93);
        }
        if($data['premium_2'] > 0)
        {
            $cf_care_2 += (($data['premium_2']*23/1000) * 3.2673);
        }
        if($data['premium_3'] > 0)
        {
            $cf_care_2 += (($data['premium_3']*9.05/1000) * 3.2009);
        }
        if($data['premium_4'] > 0)
        {
            $cf_care_2 += (($data['premium_4']*3/1000) * 2.7406);
        }
        if($data['premium_5'] > 0)
        {
            $cf_care_2 += ($data['premium_5']*0.068052);
        }
        if($data['premium_6'] > 0)
        {
            $cf_care_2 += ($data['premium_6']*4.5624);
        }

        $cf_care_3 = 0;
        if($data['food_no_foam_1'] == 'Y')
        {
            $cf_care_3 += ($data['food_pax_1'] * 0.0348835);
        }
        if($data['food_no_foam_2'] == 'Y')
        {
            $cf_care_3 += ($data['food_pax_2'] * 0.0348835);
        }
        if($data['food_no_foam_3'] == 'Y')
        {
            $cf_care_3 += ($data['food_pax_3'] * 0.0348835);
        }
        if($data['food_no_foam_4'] == 'Y')
        {
            $cf_care_3 += ($data['food_pax_4'] * 0.0348835);
        }
        if($data['food_no_foam_5'] == 'Y')
        {
            $cf_care_3 += ($data['food_pax_5'] * 0.0348835);
        }

        $cf_care_4 = 0;
        if($data['building_a_1'] > 0)
        {
            $cf_care_4 += (1 * $data['building_a_1'] * 0.4999);
        }
        if($data['building_a_2'] > 0)
        {
            $cf_care_4 += (0.86 * $data['building_a_2'] * 0.4999);
        }
        if($data['building_a_3'] > 0)
        {
            $cf_care_4 += (0.86 * $data['building_a_3'] * 0.4999);
        }
        if($data['building_a_4'] > 0)
        {
            $cf_care_4 += (3.72 * $data['building_a_4'] * 0.4999);
        }
        if($data['building_b_1'] > 0)
        {
            $cf_care_4 += (25.02 * $data['building_b_1'] * 0.4999);
        }
        if($data['building_b_2'] > 0)
        {
            $cf_care_4 += (1.04 * $data['building_b_2'] * 0.4999);
        }
        if($data['building_b_3'] > 0)
        {
            $cf_care_4 += (0.8 * $data['building_b_3'] * 0.4999);
        }
        if($data['building_b_4'] > 0)
        {
            $cf_care_4 += (0.8 * $data['building_b_4'] * 0.4999);
        }
        if($data['building_b_5'] > 0)
        {
            $cf_care_4 += (1.04 * $data['building_b_5'] * 0.4999);
        }
        if($data['building_b_6'] > 0)
        {
            $cf_care_4 += (0.8 * $data['building_b_6'] * 0.4999);
        }
        if($data['building_b_7'] > 0)
        {
            $cf_care_4 += (0.8 * $data['building_b_7'] * 0.4999);
        }
        if($data['building_b_8'] > 0)
        {
            $cf_care_4 += (3.72 * $data['building_b_8'] * 0.4999);
        }
        if($data['building_c_1'] > 0)
        {
            $cf_care_4 += (9.16 * $data['building_c_1'] * 0.4999);
        }
        if($data['led_bulb_watt_1'] > 0 && $data['led_bulb_hour_1'] > 0)
        {
            $cf_care_4 += (((3* $data['led_bulb_watt_1'] * $data['led_bulb_hour_1'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_2'] > 0 && $data['led_bulb_hour_2'] > 0)
        {
            $cf_care_4 += (((4* $data['led_bulb_watt_2'] * $data['led_bulb_hour_2'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_3'] > 0 && $data['led_bulb_hour_3'] > 0)
        {
            $cf_care_4 += (((5* $data['led_bulb_watt_3'] * $data['led_bulb_hour_3'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_4'] > 0 && $data['led_bulb_hour_4'] > 0)
        {
            $cf_care_4 += (((6* $data['led_bulb_watt_4'] * $data['led_bulb_hour_4'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_5'] > 0 && $data['led_bulb_hour_5'] > 0)
        {
            $cf_care_4 += (((7* $data['led_bulb_watt_5'] * $data['led_bulb_hour_5'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_6'] > 0 && $data['led_bulb_hour_6'] > 0)
        {
            $cf_care_4 += (((9* $data['led_bulb_watt_6'] * $data['led_bulb_hour_6'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_7'] > 0 && $data['led_bulb_hour_7'] > 0)
        {
            $cf_care_4 += (((10* $data['led_bulb_watt_7'] * $data['led_bulb_hour_7'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_8'] > 0 && $data['led_bulb_hour_8'] > 0)
        {
            $cf_care_4 += (((12* $data['led_bulb_watt_8'] * $data['led_bulb_hour_8'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_9'] > 0 && $data['led_bulb_hour_9'] > 0)
        {
            $cf_care_4 += (((13* $data['led_bulb_watt_9'] * $data['led_bulb_hour_9'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_10'] > 0 && $data['led_bulb_hour_10'] > 0)
        {
            $cf_care_4 += (((15* $data['led_bulb_watt_10'] * $data['led_bulb_hour_10'])/1000) * 0.4999);
        }
        if($data['led_bulb_watt_11'] > 0 && $data['led_bulb_hour_11'] > 0)
        {
            $cf_care_4 += (((18* $data['led_bulb_watt_11'] * $data['led_bulb_hour_11'])/1000) * 0.4999);
        }
        if($data['led_bulb_custom_watt_1'] > 0 && $data['led_bulb_custom_qty_1'] > 0 && $data['led_bulb_custom_hour_1'] > 0)
        {
            $cf_care_4 += ((($data['led_bulb_custom_watt_1']* $data['led_bulb_custom_qty_1'] * $data['led_bulb_custom_hour_1'])/1000) * 0.4999);
        }
        if($data['led_bulb_custom_watt_2'] > 0 && $data['led_bulb_custom_qty_2'] > 0 && $data['led_bulb_custom_hour_2'] > 0)
        {
            $cf_care_4 += ((($data['led_bulb_custom_watt_2']* $data['led_bulb_custom_qty_2'] * $data['led_bulb_custom_hour_2'])/1000) * 0.4999);
        }
        if($data['led_bulb_custom_watt_3'] > 0 && $data['led_bulb_custom_qty_3'] > 0 && $data['led_bulb_custom_hour_3'] > 0)
        {
            $cf_care_4 += ((($data['led_bulb_custom_watt_3']* $data['led_bulb_custom_qty_3'] * $data['led_bulb_custom_hour_3'])/1000) * 0.4999);
        }
        if($data['led_bulb_custom_watt_4'] > 0 && $data['led_bulb_custom_qty_4'] > 0 && $data['led_bulb_custom_hour_4'] > 0)
        {
            $cf_care_4 += ((($data['led_bulb_custom_watt_4']* $data['led_bulb_custom_qty_4'] * $data['led_bulb_custom_hour_4'])/1000) * 0.4999);
        }
        if($data['led_bulb_custom_watt_5'] > 0 && $data['led_bulb_custom_qty_5'] > 0 && $data['led_bulb_custom_hour_5'] > 0)
        {
            $cf_care_4 += ((($data['led_bulb_custom_watt_5']* $data['led_bulb_custom_qty_5'] * $data['led_bulb_custom_hour_5'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_1'] > 0 && $data['led_tube_hour_1'] > 0)
        {
            $cf_care_4 += (((5* $data['led_tube_watt_1'] * $data['led_tube_hour_1'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_2'] > 0 && $data['led_tube_hour_2'] > 0)
        {
            $cf_care_4 += (((6* $data['led_tube_watt_2'] * $data['led_tube_hour_2'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_3'] > 0 && $data['led_tube_hour_3'] > 0)
        {
            $cf_care_4 += (((8* $data['led_tube_watt_3'] * $data['led_tube_hour_3'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_4'] > 0 && $data['led_tube_hour_4'] > 0)
        {
            $cf_care_4 += (((10* $data['led_tube_watt_4'] * $data['led_tube_hour_4'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_5'] > 0 && $data['led_tube_hour_5'] > 0)
        {
            $cf_care_4 += (((14* $data['led_tube_watt_5'] * $data['led_tube_hour_5'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_6'] > 0 && $data['led_tube_hour_6'] > 0)
        {
            $cf_care_4 += (((18* $data['led_tube_watt_6'] * $data['led_tube_hour_6'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_7'] > 0 && $data['led_tube_hour_7'] > 0)
        {
            $cf_care_4 += (((20* $data['led_tube_watt_7'] * $data['led_tube_hour_7'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_8'] > 0 && $data['led_tube_hour_8'] > 0)
        {
            $cf_care_4 += (((22* $data['led_tube_watt_8'] * $data['led_tube_hour_8'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_9'] > 0 && $data['led_tube_hour_9'] > 0)
        {
            $cf_care_4 += (((24* $data['led_tube_watt_9'] * $data['led_tube_hour_9'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_10'] > 0 && $data['led_tube_hour_10'] > 0)
        {
            $cf_care_4 += (((25* $data['led_tube_watt_10'] * $data['led_tube_hour_10'])/1000) * 0.4999);
        }
        if($data['led_tube_watt_11'] > 0 && $data['led_tube_hour_11'] > 0)
        {
            $cf_care_4 += (((36* $data['led_tube_watt_11'] * $data['led_tube_hour_11'])/1000) * 0.4999);
        }
        if($data['led_tube_custom_watt_1'] > 0 && $data['led_tube_custom_qty_1'] > 0 && $data['led_tube_custom_hour_1'] > 0)
        {
            $cf_care_4 += ((($data['led_tube_custom_watt_1']* $data['led_tube_custom_qty_1'] * $data['led_tube_custom_hour_1'])/1000) * 0.4999);
        }
        if($data['led_tube_custom_watt_2'] > 0 && $data['led_tube_custom_qty_2'] > 0 && $data['led_tube_custom_hour_2'] > 0)
        {
            $cf_care_4 += ((($data['led_tube_custom_watt_2']* $data['led_tube_custom_qty_2'] * $data['led_tube_custom_hour_2'])/1000) * 0.4999);
        }
        if($data['led_tube_custom_watt_3'] > 0 && $data['led_tube_custom_qty_3'] > 0 && $data['led_tube_custom_hour_3'] > 0)
        {
            $cf_care_4 += ((($data['led_tube_custom_watt_3']* $data['led_tube_custom_qty_3'] * $data['led_tube_custom_hour_3'])/1000) * 0.4999);
        }
        if($data['led_tube_custom_watt_4'] > 0 && $data['led_tube_custom_qty_4'] > 0 && $data['led_tube_custom_hour_4'] > 0)
        {
            $cf_care_4 += ((($data['led_tube_custom_watt_4']* $data['led_tube_custom_qty_4'] * $data['led_tube_custom_hour_4'])/1000) * 0.4999);
        }
        if($data['led_tube_custom_watt_5'] > 0 && $data['led_tube_custom_qty_5'] > 0 && $data['led_tube_custom_hour_5'] > 0)
        {
            $cf_care_4 += ((($data['led_tube_custom_watt_5']* $data['led_tube_custom_qty_5'] * $data['led_tube_custom_hour_5'])/1000) * 0.4999);
        }

        $cf_care_5 = 0;
        if($data['waste_1'] > 0)
        {
            $cf_care_5 += ((((0.3*$data['food_pax_1']) + (0.2*$data['food_pax_2']) + (0.3*$data['food_pax_3']) + (0.2*$data['food_pax_4']) + (0.3*$data['food_pax_5'])) * 2.53) - ($data['waste_1'] * 2.53));
        }
        if($data['waste_2'] > 0)
        {
            $cf_care_5 += ($data['waste_2']* 1.031);
        }
        if($data['waste_3'] > 0)
        {
            $cf_care_5 += ($data['waste_3']* 5.6735);
        }
        if($data['waste_4'] > 0)
        {
            $cf_care_5 += ($data['waste_4']* 9.127);
        }
        if($data['waste_5'] > 0)
        {
            $cf_care_5 += ($data['waste_5']* 1.832);
        }
        if($data['waste_6'] > 0)
        {
            $cf_care_5 += ($data['waste_6']* 0.276);
        }

        $cf_care_6 = 0;
        if($data['wood_1'] > 0)
        {
            $cf_care_6 += (($data['wood_1']*7.1349528)*3.333 + ($data['wood_1']*0.011891588)*621);
        }
        if($data['wood_2'] > 0)
        {
            $cf_care_6 += (($data['wood_2']* 10.7024292)*3.333 + ($data['wood_2']* 0.017837382)*621);
        }
        if($data['wood_3'] > 0)
        {
            $cf_care_6 += (($data['wood_3']* 14.2699056)*3.333 + ($data['wood_3']* 0.023783176)*621);
        }
        if($data['wood_4'] > 0)
        {
            $cf_care_6 += (($data['wood_4']* 17.837382)*3.333 + ($data['wood_4']* 0.02972897)*621);
        }
        if($data['wood_5'] > 0)
        {
            $cf_care_6 += (($data['wood_5']* 26.756073)*3.333 + ($data['wood_5']* 0.044593455)*621);
        }
        if($data['pp_1'] > 0)
        {
            $cf_care_6 += (($data['pp_1']* 1.340)* 2.4565);
        }
        if($data['pp_2'] > 0)
        {
            $cf_care_6 += (($data['pp_2']* 2.084)* 2.4565);
        }
        if($data['pp_3'] > 0)
        {
            $cf_care_6 += (($data['pp_3']* 3.572)* 2.4565);
        }
        if($data['pp_4'] > 0)
        {
            $cf_care_6 += (($data['pp_4']* 4.763)* 2.4565);
        }
        if($data['pp_5'] > 0)
        {
            $cf_care_6 += (($data['pp_5']* 5.954)* 2.4565);
        }
        if($data['pp_kg'] > 0)
        {
            $cf_care_6 += ($data['pp_kg']* 2.4565);
        }

        $update['cf_care_2'] = str_replace(',', '', number_format((($cf_care_2 > 0) ? $cf_care_2 : 0), 2));
        $update['cf_care_3'] = str_replace(',', '', number_format((($cf_care_3 > 0) ? $cf_care_3 : 0), 2));
        $update['cf_care_4'] = str_replace(',', '', number_format((($cf_care_4 > 0) ? $cf_care_4 : 0), 2));
        $update['cf_care_5'] = str_replace(',', '', number_format((($cf_care_5 > 0) ? $cf_care_5 : 0), 2));
        $update['cf_care_6'] = str_replace(',', '', number_format((($cf_care_6 > 0) ? $cf_care_6 : 0), 2));
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('id', $id);
        $this->db->update('activity', $update);

        $this->db->set('cf_care_total', 'REPLACE(FORMAT((cf_care_1 + cf_care_2 + cf_care_3 + cf_care_4 + cf_care_5 + cf_care_6),2),\',\',\'\')', FALSE);
        $this->db->where('id', $id);
        $this->db->update('activity');
    }

    function get_all_document()
    {
        $this->db->select('*');
        $this->db->from('document');
        $this->db->where('status', 'Y');
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_date_activity()
    {
        $this->db->select('MIN(activity.activity_date) as min_date, MAX(activity.activity_date) as max_date');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('member.id', $this->authen->id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_summary_cf($mode, $end_date, $from, $to, $month_year)
    {
        $this->db->select('sum(activity.cf_care_total) as sum_cf');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');

        if($mode == 'mode1')
        {
            $this->db->where("activity.activity_date <= '".$end_date."'");
        }
        else if($mode == 'mode2')
        {
            $this->db->where("activity.activity_date >= '".$from."'");
            $this->db->where("activity.activity_date <= '".$to."'");
        }
        else if($mode == 'mode3')
        {
            $from = $month_year.'-01';
            $to = $month_year.'-'.date('t', strtotime($month_year.'-01'));

            $this->db->where("activity.activity_date >= '".$from."'");
            $this->db->where("activity.activity_date <= '".$to."'");
        }
        
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('member.id', $this->authen->id);
        $query = $this->db->get();
        $activity = $query->row_array();

        if($activity['sum_cf'] == null)
        {
            $activity['sum_cf'] = '0';
        }

        $this->db->select('sum(project.cf_care_total) as sum_cf');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');

        if($mode == 'mode1')
        {
            $this->db->where("((project.start_date <= '".$end_date."' and project.end_date >= '".$end_date."') or (project.start_date <= '".$end_date."' and project.end_date <= '".$end_date."'))");
        }
        else if($mode == 'mode2')
        {
            $this->db->where("((project.start_date <= '".$from."' and project.end_date >= '".$to."') or (project.start_date <= '".$from."' and project.end_date >= '".$from."' and project.end_date <= '".$to."') or (project.start_date >= '".$from."' and project.end_date <= '".$to."') or (project.start_date >= '".$from."' and project.start_date <= '".$to."' and project.end_date >= '".$to."'))");
        }
        else if($mode == 'mode3')
        {
            $from = $month_year.'-01';
            $to = $month_year.'-'.date('t', strtotime($month_year.'-01'));

            $this->db->where("((project.start_date <= '".$from."' and project.end_date >= '".$to."') or (project.start_date <= '".$from."' and project.end_date >= '".$from."' and project.end_date <= '".$to."') or (project.start_date >= '".$from."' and project.end_date <= '".$to."') or (project.start_date >= '".$from."' and project.start_date <= '".$to."' and project.end_date >= '".$to."'))");
        }

        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('member.id', $this->authen->id);
        $query = $this->db->get();
        $project = $query->row_array();

        if($project['sum_cf'] == null)
        {
            $project['sum_cf'] = '0';
        }

        return array('total_cf' => ($activity['sum_cf'] + $project['sum_cf']), 'total_tree' => number_format((($activity['sum_cf'] + $project['sum_cf']) / 9), 2), 'total_activity_cf' => $activity['sum_cf'], 'total_activity_tree' => number_format(($activity['sum_cf'] / 9), 2), 'total_project_cf' => $project['sum_cf'], 'total_project_tree' => number_format(($project['sum_cf'] / 9), 2));
    }

    function count_all_project()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('project');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_all_project($limit,$page)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_project()
    {
        $data['member_id'] = $this->authen->id;
        $data['name'] = $this->input->post('name');
        $data['place'] = $this->input->post('place');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $data['start_on'] = $this->input->post('start_on').':00';
        $data['end_on'] = $this->input->post('end_on').':00';
        $data['description'] = $this->input->post('description');
        $data['image_1'] = '';
        $data['image_2'] = '';
        $data['image_3'] = '';
        $data['vdo'] = $this->input->post('vdo');
        $data['attendant'] = $this->input->post('attendant');
        $data['is_care_1'] = (($this->input->post('is_care_1') == 'Y') ? 'Y' : 'N');
        $data['is_care_2'] = (($this->input->post('is_care_2') == 'Y') ? 'Y' : 'N');
        $data['is_care_3'] = (($this->input->post('is_care_3') == 'Y') ? 'Y' : 'N');

        if($data['is_care_2'] == 'Y')
        {
            $data['care_2_1'] = (($this->input->post('care_2_1') > 0) ? $this->input->post('care_2_1') : 0);
            $data['care_2_2'] = (($this->input->post('care_2_2') > 0) ? $this->input->post('care_2_2') : 0);
        }
        else
        {
            $data['care_2_1'] = 0;
            $data['care_2_2'] = 0;
        }

        if($data['is_care_3'] == 'Y')
        {
            $data['care_3_1'] = (($this->input->post('care_3_1') == 'Y') ? 'Y' : 'N');
            if($data['care_3_1'] == 'Y')
            {
                $data['care_3_1_1'] = (($this->input->post('care_3_1_1') > 0) ? $this->input->post('care_3_1_1') : 0);
                $data['care_3_1_2'] = (($this->input->post('care_3_1_2') > 0) ? $this->input->post('care_3_1_2') : 0);
                $data['care_3_1_3'] = (($this->input->post('care_3_1_3') > 0) ? $this->input->post('care_3_1_3') : 0);
                $data['care_3_1_4'] = (($this->input->post('care_3_1_4') > 0) ? $this->input->post('care_3_1_4') : 0);
                $data['care_3_1_5'] = (($this->input->post('care_3_1_5') > 0) ? $this->input->post('care_3_1_5') : 0);
                $data['care_3_1_6'] = (($this->input->post('care_3_1_6') > 0) ? $this->input->post('care_3_1_6') : 0);
            }
            else
            {
                $data['care_3_1_1'] = 0;
                $data['care_3_1_2'] = 0;
                $data['care_3_1_3'] = 0;
                $data['care_3_1_4'] = 0;
                $data['care_3_1_5'] = 0;
                $data['care_3_1_6'] = 0;
            }

            $data['care_3_2'] = (($this->input->post('care_3_2') == 'Y') ? 'Y' : 'N');
            if($data['care_3_2'] == 'Y')
            {
                $data['care_3_2_1'] = (($this->input->post('care_3_2_1') > 0) ? $this->input->post('care_3_2_1') : 0);
                $data['care_3_2_2'] = (($this->input->post('care_3_2_2') > 0) ? $this->input->post('care_3_2_2') : 0);
                $data['care_3_2_3'] = (($this->input->post('care_3_2_3') > 0) ? $this->input->post('care_3_2_3') : 0);
                $data['care_3_2_4'] = (($this->input->post('care_3_2_4') > 0) ? $this->input->post('care_3_2_4') : 0);
                $data['care_3_2_5'] = (($this->input->post('care_3_2_5') > 0) ? $this->input->post('care_3_2_5') : 0);
                $data['care_3_2_6'] = (($this->input->post('care_3_2_6') > 0) ? $this->input->post('care_3_2_6') : 0);
            }
            else
            {
                $data['care_3_2_1'] = 0;
                $data['care_3_2_2'] = 0;
                $data['care_3_2_3'] = 0;
                $data['care_3_2_4'] = 0;
                $data['care_3_2_5'] = 0;
                $data['care_3_2_6'] = 0;
            }

            $data['care_3_3'] = (($this->input->post('care_3_3') == 'Y') ? 'Y' : 'N');
            if($data['care_3_3'] == 'Y')
            {
                $data['care_3_3_1'] = (($this->input->post('care_3_3_1') > 0) ? $this->input->post('care_3_3_1') : 0);
                $data['care_3_3_2'] = (($this->input->post('care_3_3_2') > 0) ? $this->input->post('care_3_3_2') : 0);
                $data['care_3_3_3'] = (($this->input->post('care_3_3_3') > 0) ? $this->input->post('care_3_3_3') : 0);
                $data['care_3_3_4'] = (($this->input->post('care_3_3_4') > 0) ? $this->input->post('care_3_3_4') : 0);
                $data['care_3_3_5'] = (($this->input->post('care_3_3_5') > 0) ? $this->input->post('care_3_3_5') : 0);
                $data['care_3_3_6'] = (($this->input->post('care_3_3_6') > 0) ? $this->input->post('care_3_3_6') : 0);
            }
            else
            {
                $data['care_3_3_1'] = 0;
                $data['care_3_3_2'] = 0;
                $data['care_3_3_3'] = 0;
                $data['care_3_3_4'] = 0;
                $data['care_3_3_5'] = 0;
                $data['care_3_3_6'] = 0;
            }

            $data['care_3_4'] = (($this->input->post('care_3_4') == 'Y') ? 'Y' : 'N');
            if($data['care_3_4'] == 'Y')
            {
                $data['care_3_4_1'] = (($this->input->post('care_3_4_1') > 0) ? $this->input->post('care_3_4_1') : 0);
                $data['care_3_4_2'] = (($this->input->post('care_3_4_2') > 0) ? $this->input->post('care_3_4_2') : 0);
                $data['care_3_4_3'] = (($this->input->post('care_3_4_3') > 0) ? $this->input->post('care_3_4_3') : 0);
                $data['care_3_4_4'] = (($this->input->post('care_3_4_4') > 0) ? $this->input->post('care_3_4_4') : 0);
                $data['care_3_4_5'] = (($this->input->post('care_3_4_5') > 0) ? $this->input->post('care_3_4_5') : 0);
                $data['care_3_4_6'] = (($this->input->post('care_3_4_6') > 0) ? $this->input->post('care_3_4_6') : 0);
            }
            else
            {
                $data['care_3_4_1'] = 0;
                $data['care_3_4_2'] = 0;
                $data['care_3_4_3'] = 0;
                $data['care_3_4_4'] = 0;
                $data['care_3_4_5'] = 0;
                $data['care_3_4_6'] = 0;
            }

            $data['care_3_5'] = (($this->input->post('care_3_5') == 'Y') ? 'Y' : 'N');
            if($data['care_3_5'] == 'Y')
            {
                $data['care_3_5_1'] = (($this->input->post('care_3_5_1') > 0) ? $this->input->post('care_3_5_1') : 0);
                $data['care_3_5_2'] = (($this->input->post('care_3_5_2') > 0) ? $this->input->post('care_3_5_2') : 0);
                $data['care_3_5_3'] = (($this->input->post('care_3_5_3') > 0) ? $this->input->post('care_3_5_3') : 0);
                $data['care_3_5_4'] = (($this->input->post('care_3_5_4') > 0) ? $this->input->post('care_3_5_4') : 0);
                $data['care_3_5_5'] = (($this->input->post('care_3_5_5') > 0) ? $this->input->post('care_3_5_5') : 0);
                $data['care_3_5_6'] = (($this->input->post('care_3_5_6') > 0) ? $this->input->post('care_3_5_6') : 0);
            }
            else
            {
                $data['care_3_5_1'] = 0;
                $data['care_3_5_2'] = 0;
                $data['care_3_5_3'] = 0;
                $data['care_3_5_4'] = 0;
                $data['care_3_5_5'] = 0;
                $data['care_3_5_6'] = 0;
            }

            $data['care_3_6'] = (($this->input->post('care_3_6') == 'Y') ? 'Y' : 'N');
            if($data['care_3_6'] == 'Y')
            {
                $data['care_3_6_1'] = (($this->input->post('care_3_6_1') > 0) ? $this->input->post('care_3_6_1') : 0);
                $data['care_3_6_2'] = (($this->input->post('care_3_6_2') > 0) ? $this->input->post('care_3_6_2') : 0);
                $data['care_3_6_3'] = (($this->input->post('care_3_6_3') > 0) ? $this->input->post('care_3_6_3') : 0);
                $data['care_3_6_4'] = (($this->input->post('care_3_6_4') > 0) ? $this->input->post('care_3_6_4') : 0);
                $data['care_3_6_5'] = (($this->input->post('care_3_6_5') > 0) ? $this->input->post('care_3_6_5') : 0);
                $data['care_3_6_6'] = (($this->input->post('care_3_6_6') > 0) ? $this->input->post('care_3_6_6') : 0);
            }
            else
            {
                $data['care_3_6_1'] = 0;
                $data['care_3_6_2'] = 0;
                $data['care_3_6_3'] = 0;
                $data['care_3_6_4'] = 0;
                $data['care_3_6_5'] = 0;
                $data['care_3_6_6'] = 0;
            }
        }
        else
        {
            $data['care_3_1'] = 'N';
            $data['care_3_1_1'] = 0;
            $data['care_3_1_2'] = 0;
            $data['care_3_1_3'] = 0;
            $data['care_3_1_4'] = 0;
            $data['care_3_1_5'] = 0;
            $data['care_3_1_6'] = 0;
            $data['care_3_2'] = 'N';
            $data['care_3_2_1'] = 0;
            $data['care_3_2_2'] = 0;
            $data['care_3_2_3'] = 0;
            $data['care_3_2_4'] = 0;
            $data['care_3_2_5'] = 0;
            $data['care_3_2_6'] = 0;
            $data['care_3_3'] = 'N';
            $data['care_3_3_1'] = 0;
            $data['care_3_3_2'] = 0;
            $data['care_3_3_3'] = 0;
            $data['care_3_3_4'] = 0;
            $data['care_3_3_5'] = 0;
            $data['care_3_3_6'] = 0;
            $data['care_3_4'] = 'N';
            $data['care_3_4_1'] = 0;
            $data['care_3_4_2'] = 0;
            $data['care_3_4_3'] = 0;
            $data['care_3_4_4'] = 0;
            $data['care_3_4_5'] = 0;
            $data['care_3_4_6'] = 0;
            $data['care_3_5'] = 'N';
            $data['care_3_5_1'] = 0;
            $data['care_3_5_2'] = 0;
            $data['care_3_5_3'] = 0;
            $data['care_3_5_4'] = 0;
            $data['care_3_5_5'] = 0;
            $data['care_3_5_6'] = 0;
            $data['care_3_6'] = 'N';
            $data['care_3_6_1'] = 0;
            $data['care_3_6_2'] = 0;
            $data['care_3_6_3'] = 0;
            $data['care_3_6_4'] = 0;
            $data['care_3_6_5'] = 0;
            $data['care_3_6_6'] = 0;
        }

        $data['cf_care_1'] = 0;
        $data['cf_care_2'] = 0;
        $data['cf_care_3'] = 0;
        $data['cf_care_total'] = 0;
        $data['view_count'] = 0;
        $data['status'] = 'Y';
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->db->insert('project', $data);
        
        $_id = $this->db->insert_id();

        $this->update_cf_project($_id, $data);

        return $_id;
    }

    function update_project_image_1($id, $member_id, $image)
    {
        $data['image_1'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('project', $data);
    }

    function update_project_image_2($id, $member_id, $image)
    {
        $data['image_2'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('project', $data);
    }

    function update_project_image_3($id, $member_id, $image)
    {
        $data['image_3'] = config_item('image_url').'upload/member/'.($member_id%4000).'/'.$member_id.'/'.$image;
        $this->db->where('id', $id);
        $this->db->update('project', $data);
    }

    function get_project_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('project');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('status', 'Y');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function update_project($id)
    {
        $data['name'] = $this->input->post('name');
        $data['place'] = $this->input->post('place');
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $data['start_on'] = $this->input->post('start_on').':00';
        $data['end_on'] = $this->input->post('end_on').':00';
        $data['description'] = $this->input->post('description');
        $data['vdo'] = $this->input->post('vdo');
        $data['attendant'] = $this->input->post('attendant');
        $data['is_care_1'] = (($this->input->post('is_care_1') == 'Y') ? 'Y' : 'N');
        $data['is_care_2'] = (($this->input->post('is_care_2') == 'Y') ? 'Y' : 'N');
        $data['is_care_3'] = (($this->input->post('is_care_3') == 'Y') ? 'Y' : 'N');

        if($data['is_care_2'] == 'Y')
        {
            $data['care_2_1'] = (($this->input->post('care_2_1') > 0) ? $this->input->post('care_2_1') : 0);
            $data['care_2_2'] = (($this->input->post('care_2_2') > 0) ? $this->input->post('care_2_2') : 0);
        }
        else
        {
            $data['care_2_1'] = 0;
            $data['care_2_2'] = 0;
        }

        if($data['is_care_3'] == 'Y')
        {
            $data['care_3_1'] = (($this->input->post('care_3_1') == 'Y') ? 'Y' : 'N');
            if($data['care_3_1'] == 'Y')
            {
                $data['care_3_1_1'] = (($this->input->post('care_3_1_1') > 0) ? $this->input->post('care_3_1_1') : 0);
                $data['care_3_1_2'] = (($this->input->post('care_3_1_2') > 0) ? $this->input->post('care_3_1_2') : 0);
                $data['care_3_1_3'] = (($this->input->post('care_3_1_3') > 0) ? $this->input->post('care_3_1_3') : 0);
                $data['care_3_1_4'] = (($this->input->post('care_3_1_4') > 0) ? $this->input->post('care_3_1_4') : 0);
                $data['care_3_1_5'] = (($this->input->post('care_3_1_5') > 0) ? $this->input->post('care_3_1_5') : 0);
                $data['care_3_1_6'] = (($this->input->post('care_3_1_6') > 0) ? $this->input->post('care_3_1_6') : 0);
            }
            else
            {
                $data['care_3_1_1'] = 0;
                $data['care_3_1_2'] = 0;
                $data['care_3_1_3'] = 0;
                $data['care_3_1_4'] = 0;
                $data['care_3_1_5'] = 0;
                $data['care_3_1_6'] = 0;
            }

            $data['care_3_2'] = (($this->input->post('care_3_2') == 'Y') ? 'Y' : 'N');
            if($data['care_3_2'] == 'Y')
            {
                $data['care_3_2_1'] = (($this->input->post('care_3_2_1') > 0) ? $this->input->post('care_3_2_1') : 0);
                $data['care_3_2_2'] = (($this->input->post('care_3_2_2') > 0) ? $this->input->post('care_3_2_2') : 0);
                $data['care_3_2_3'] = (($this->input->post('care_3_2_3') > 0) ? $this->input->post('care_3_2_3') : 0);
                $data['care_3_2_4'] = (($this->input->post('care_3_2_4') > 0) ? $this->input->post('care_3_2_4') : 0);
                $data['care_3_2_5'] = (($this->input->post('care_3_2_5') > 0) ? $this->input->post('care_3_2_5') : 0);
                $data['care_3_2_6'] = (($this->input->post('care_3_2_6') > 0) ? $this->input->post('care_3_2_6') : 0);
            }
            else
            {
                $data['care_3_2_1'] = 0;
                $data['care_3_2_2'] = 0;
                $data['care_3_2_3'] = 0;
                $data['care_3_2_4'] = 0;
                $data['care_3_2_5'] = 0;
                $data['care_3_2_6'] = 0;
            }

            $data['care_3_3'] = (($this->input->post('care_3_3') == 'Y') ? 'Y' : 'N');
            if($data['care_3_3'] == 'Y')
            {
                $data['care_3_3_1'] = (($this->input->post('care_3_3_1') > 0) ? $this->input->post('care_3_3_1') : 0);
                $data['care_3_3_2'] = (($this->input->post('care_3_3_2') > 0) ? $this->input->post('care_3_3_2') : 0);
                $data['care_3_3_3'] = (($this->input->post('care_3_3_3') > 0) ? $this->input->post('care_3_3_3') : 0);
                $data['care_3_3_4'] = (($this->input->post('care_3_3_4') > 0) ? $this->input->post('care_3_3_4') : 0);
                $data['care_3_3_5'] = (($this->input->post('care_3_3_5') > 0) ? $this->input->post('care_3_3_5') : 0);
                $data['care_3_3_6'] = (($this->input->post('care_3_3_6') > 0) ? $this->input->post('care_3_3_6') : 0);
            }
            else
            {
                $data['care_3_3_1'] = 0;
                $data['care_3_3_2'] = 0;
                $data['care_3_3_3'] = 0;
                $data['care_3_3_4'] = 0;
                $data['care_3_3_5'] = 0;
                $data['care_3_3_6'] = 0;
            }

            $data['care_3_4'] = (($this->input->post('care_3_4') == 'Y') ? 'Y' : 'N');
            if($data['care_3_4'] == 'Y')
            {
                $data['care_3_4_1'] = (($this->input->post('care_3_4_1') > 0) ? $this->input->post('care_3_4_1') : 0);
                $data['care_3_4_2'] = (($this->input->post('care_3_4_2') > 0) ? $this->input->post('care_3_4_2') : 0);
                $data['care_3_4_3'] = (($this->input->post('care_3_4_3') > 0) ? $this->input->post('care_3_4_3') : 0);
                $data['care_3_4_4'] = (($this->input->post('care_3_4_4') > 0) ? $this->input->post('care_3_4_4') : 0);
                $data['care_3_4_5'] = (($this->input->post('care_3_4_5') > 0) ? $this->input->post('care_3_4_5') : 0);
                $data['care_3_4_6'] = (($this->input->post('care_3_4_6') > 0) ? $this->input->post('care_3_4_6') : 0);
            }
            else
            {
                $data['care_3_4_1'] = 0;
                $data['care_3_4_2'] = 0;
                $data['care_3_4_3'] = 0;
                $data['care_3_4_4'] = 0;
                $data['care_3_4_5'] = 0;
                $data['care_3_4_6'] = 0;
            }

            $data['care_3_5'] = (($this->input->post('care_3_5') == 'Y') ? 'Y' : 'N');
            if($data['care_3_5'] == 'Y')
            {
                $data['care_3_5_1'] = (($this->input->post('care_3_5_1') > 0) ? $this->input->post('care_3_5_1') : 0);
                $data['care_3_5_2'] = (($this->input->post('care_3_5_2') > 0) ? $this->input->post('care_3_5_2') : 0);
                $data['care_3_5_3'] = (($this->input->post('care_3_5_3') > 0) ? $this->input->post('care_3_5_3') : 0);
                $data['care_3_5_4'] = (($this->input->post('care_3_5_4') > 0) ? $this->input->post('care_3_5_4') : 0);
                $data['care_3_5_5'] = (($this->input->post('care_3_5_5') > 0) ? $this->input->post('care_3_5_5') : 0);
                $data['care_3_5_6'] = (($this->input->post('care_3_5_6') > 0) ? $this->input->post('care_3_5_6') : 0);
            }
            else
            {
                $data['care_3_5_1'] = 0;
                $data['care_3_5_2'] = 0;
                $data['care_3_5_3'] = 0;
                $data['care_3_5_4'] = 0;
                $data['care_3_5_5'] = 0;
                $data['care_3_5_6'] = 0;
            }

            $data['care_3_6'] = (($this->input->post('care_3_6') == 'Y') ? 'Y' : 'N');
            if($data['care_3_6'] == 'Y')
            {
                $data['care_3_6_1'] = (($this->input->post('care_3_6_1') > 0) ? $this->input->post('care_3_6_1') : 0);
                $data['care_3_6_2'] = (($this->input->post('care_3_6_2') > 0) ? $this->input->post('care_3_6_2') : 0);
                $data['care_3_6_3'] = (($this->input->post('care_3_6_3') > 0) ? $this->input->post('care_3_6_3') : 0);
                $data['care_3_6_4'] = (($this->input->post('care_3_6_4') > 0) ? $this->input->post('care_3_6_4') : 0);
                $data['care_3_6_5'] = (($this->input->post('care_3_6_5') > 0) ? $this->input->post('care_3_6_5') : 0);
                $data['care_3_6_6'] = (($this->input->post('care_3_6_6') > 0) ? $this->input->post('care_3_6_6') : 0);
            }
            else
            {
                $data['care_3_6_1'] = 0;
                $data['care_3_6_2'] = 0;
                $data['care_3_6_3'] = 0;
                $data['care_3_6_4'] = 0;
                $data['care_3_6_5'] = 0;
                $data['care_3_6_6'] = 0;
            }
        }
        else
        {
            $data['care_3_1'] = 'N';
            $data['care_3_1_1'] = 0;
            $data['care_3_1_2'] = 0;
            $data['care_3_1_3'] = 0;
            $data['care_3_1_4'] = 0;
            $data['care_3_1_5'] = 0;
            $data['care_3_1_6'] = 0;
            $data['care_3_2'] = 'N';
            $data['care_3_2_1'] = 0;
            $data['care_3_2_2'] = 0;
            $data['care_3_2_3'] = 0;
            $data['care_3_2_4'] = 0;
            $data['care_3_2_5'] = 0;
            $data['care_3_2_6'] = 0;
            $data['care_3_3'] = 'N';
            $data['care_3_3_1'] = 0;
            $data['care_3_3_2'] = 0;
            $data['care_3_3_3'] = 0;
            $data['care_3_3_4'] = 0;
            $data['care_3_3_5'] = 0;
            $data['care_3_3_6'] = 0;
            $data['care_3_4'] = 'N';
            $data['care_3_4_1'] = 0;
            $data['care_3_4_2'] = 0;
            $data['care_3_4_3'] = 0;
            $data['care_3_4_4'] = 0;
            $data['care_3_4_5'] = 0;
            $data['care_3_4_6'] = 0;
            $data['care_3_5'] = 'N';
            $data['care_3_5_1'] = 0;
            $data['care_3_5_2'] = 0;
            $data['care_3_5_3'] = 0;
            $data['care_3_5_4'] = 0;
            $data['care_3_5_5'] = 0;
            $data['care_3_5_6'] = 0;
            $data['care_3_6'] = 'N';
            $data['care_3_6_1'] = 0;
            $data['care_3_6_2'] = 0;
            $data['care_3_6_3'] = 0;
            $data['care_3_6_4'] = 0;
            $data['care_3_6_5'] = 0;
            $data['care_3_6_6'] = 0;
        }

        $data['updated_on'] = date('Y-m-d H:i:s');
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('id', $id);
        $this->db->update('project', $data);

        $this->update_cf_project($id, $data);
    }

    function update_cf_project($id, $data)
    {
        $cf_care_2 = 0;
        if($data['is_care_2'] == 'Y')
        {
            if($data['care_2_1'] > 0)
            {
                $cf_care_2 += ($data['care_2_1'] * 0.028968);
            }

            if($data['care_2_2'] > 0)
            {
                $cf_care_2 += ($data['care_2_2'] * 4.5484);
            }
        }

        $cf_care_3 = 0;
        if($data['is_care_3'] == 'Y')
        {
            if($data['care_3_1'] == 'Y')
            {
                if($data['care_3_1_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 55 * $data['care_3_1_1']) * 0.0000005032);
                }

                if($data['care_3_1_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 55 * $data['care_3_1_2']) * 0.0000005032);
                }

                if($data['care_3_1_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 55 * $data['care_3_1_3']) * 0.0000005032);
                }

                if($data['care_3_1_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 55 * $data['care_3_1_4']) * 0.0000005032);
                }

                if($data['care_3_1_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 55 * $data['care_3_1_5']) * 0.0000005032);
                }

                if($data['care_3_1_6'] > 0)
                {
                    $cf_care_3 += ((440 * 55 * $data['care_3_1_6']) * 0.0000005032);
                }
            }

            if($data['care_3_2'] == 'Y')
            {
                if($data['care_3_2_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 70 * $data['care_3_2_1']) * 0.0000005032);
                }

                if($data['care_3_2_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 70 * $data['care_3_2_2']) * 0.0000005032);
                }

                if($data['care_3_2_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 70 * $data['care_3_2_3']) * 0.0000005032);
                }

                if($data['care_3_2_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 70 * $data['care_3_2_4']) * 0.0000005032);
                }

                if($data['care_3_2_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 70 * $data['care_3_2_5']) * 0.0000005032);
                }

                if($data['care_3_2_6'] > 0)
                {
                    $cf_care_3 += ((440 * 70 * $data['care_3_2_6']) * 0.0000005032);
                }
            }

            if($data['care_3_3'] == 'Y')
            {
                if($data['care_3_3_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 80 * $data['care_3_3_1']) * 0.0000005032);
                }

                if($data['care_3_3_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 80 * $data['care_3_3_2']) * 0.0000005032);
                }

                if($data['care_3_3_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 80 * $data['care_3_3_3']) * 0.0000005032);
                }

                if($data['care_3_3_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 80 * $data['care_3_3_4']) * 0.0000005032);
                }

                if($data['care_3_3_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 80 * $data['care_3_3_5']) * 0.0000005032);
                }

                if($data['care_3_3_6'] > 0)
                {
                    $cf_care_3 += ((440 * 80 * $data['care_3_3_6']) * 0.0000005032);
                }
            }

            if($data['care_3_4'] == 'Y')
            {
                if($data['care_3_4_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 90 * $data['care_3_4_1']) * 0.0000005032);
                }

                if($data['care_3_4_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 90 * $data['care_3_4_2']) * 0.0000005032);
                }

                if($data['care_3_4_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 90 * $data['care_3_4_3']) * 0.0000005032);
                }

                if($data['care_3_4_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 90 * $data['care_3_4_4']) * 0.0000005032);
                }

                if($data['care_3_4_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 90 * $data['care_3_4_5']) * 0.0000005032);
                }

                if($data['care_3_4_6'] > 0)
                {
                    $cf_care_3 += ((440 * 90 * $data['care_3_4_6']) * 0.0000005032);
                }
            }

            if($data['care_3_5'] == 'Y')
            {
                if($data['care_3_5_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 100 * $data['care_3_5_1']) * 0.0000005032);
                }

                if($data['care_3_5_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 100 * $data['care_3_5_2']) * 0.0000005032);
                }

                if($data['care_3_5_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 100 * $data['care_3_5_3']) * 0.0000005032);
                }

                if($data['care_3_5_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 100 * $data['care_3_5_4']) * 0.0000005032);
                }

                if($data['care_3_5_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 100 * $data['care_3_5_5']) * 0.0000005032);
                }

                if($data['care_3_5_6'] > 0)
                {
                    $cf_care_3 += ((440 * 100 * $data['care_3_5_6']) * 0.0000005032);
                }
            }

            if($data['care_3_6'] == 'Y')
            {
                if($data['care_3_6_1'] > 0)
                {
                    $cf_care_3 += ((1247.4 * 120 * $data['care_3_6_1']) * 0.0000005032);
                }

                if($data['care_3_6_2'] > 0)
                {
                    $cf_care_3 += ((623.7 * 120 * $data['care_3_6_2']) * 0.0000005032);
                }

                if($data['care_3_6_3'] > 0)
                {
                    $cf_care_3 += ((310.8 * 120 * $data['care_3_6_3']) * 0.0000005032);
                }

                if($data['care_3_6_4'] > 0)
                {
                    $cf_care_3 += ((1765 * 120 * $data['care_3_6_4']) * 0.0000005032);
                }

                if($data['care_3_6_5'] > 0)
                {
                    $cf_care_3 += ((882.5 * 120 * $data['care_3_6_5']) * 0.0000005032);
                }

                if($data['care_3_6_6'] > 0)
                {
                    $cf_care_3 += ((440 * 120 * $data['care_3_6_6']) * 0.0000005032);
                }
            }
        }

        $update['cf_care_2'] = str_replace(',', '', number_format((($cf_care_2 > 0) ? $cf_care_2 : 0), 2));
        $update['cf_care_3'] = str_replace(',', '', number_format((($cf_care_3 > 0) ? $cf_care_3 : 0), 2));
        $this->db->where('member_id', $this->authen->id);
        $this->db->where('id', $id);
        $this->db->update('project', $update);

        $this->db->set('cf_care_total', 'REPLACE(FORMAT((cf_care_1 + cf_care_2 + cf_care_3),2),\',\',\'\')', FALSE);
        $this->db->where('id', $id);
        $this->db->update('project');
    }
}