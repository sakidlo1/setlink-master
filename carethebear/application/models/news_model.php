<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class news_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function count_all_news()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('news');
        $this->db->where('status', 'Y');

        if($this->input->get('q') != "")
        {
            $this->db->where("(name like '%".$this->input->get('q')."%' or description like '%".$this->input->get('q')."%')");
        }

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }
    
    function get_all_news($limit,$page)
    {
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('status', 'Y');

        if($this->input->get('q') != "")
        {
            $this->db->where("(name like '%".$this->input->get('q')."%' or description like '%".$this->input->get('q')."%')");
        }

        $this->db->order_by('id desc');
        $this->db->limit($limit, ($limit*$page)-$limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_news_detail_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('news');
        $this->db->where('status', 'Y');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }
}