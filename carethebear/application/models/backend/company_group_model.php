<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class company_group_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_company_type()
    {
        $this->db->select('*');
        $this->db->from('company_type');
        $this->db->where("status <> 'D'");
        $this->db->order_by('order_on', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('company_group');
        $this->db->join('company_type', 'company_type.id = company_group.company_type_id');

        $fType = @$this->input->post('columns')[0]['search']['value'];
        if($fType != "")
        {
            $this->db->where('company_group.company_type_id', $fType);
        }

        $fName = @$this->input->post('columns')[1]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company_group.name LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('company_group.status', $fStatus);
        }

        $this->db->where("company_group.status <> 'D'");

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company_group.*, company_type.name as company_type');
        $this->db->from('company_group');
        $this->db->join('company_type', 'company_type.id = company_group.company_type_id');

        $fType = @$this->input->post('columns')[0]['search']['value'];
        if($fType != "")
        {
            $this->db->where('company_group.company_type_id', $fType);
        }

        $fName = @$this->input->post('columns')[1]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company_group.name LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('company_group.status', $fStatus);
        }

        $this->db->where("company_group.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('company_group.order_on asc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->db->select('company_group.*');
        $this->db->from('company_group');
        $this->db->where('company_group.id', $this->uri->segment(4));
        $this->db->where("company_group.status <> 'D'");
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function insert()
    {
        $data['company_type_id'] = $this->input->post('company_type_id');
        $data['name'] = $this->input->post('name');
        $data['order_on'] = $this->input->post('order_on');
        $data['status'] = $this->input->post('status');
        $data['created_on'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->authen->username;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = '-';
        $this->db->insert('company_group', $data);
    }
    
    function update()
    {
        $data['company_type_id'] = $this->input->post('company_type_id');
        $data['name'] = $this->input->post('name');
        $data['order_on'] = $this->input->post('order_on');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('company_group', $data);
    }
    
    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where("company_group.status <> 'D'");
        $this->db->update('company_group', $data);
    }
}