<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class role_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('role');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("role.name LIKE '%".$fName."%'");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('role.status', $fStatus);
        }

        $this->db->where("role.priority > ".$this->authen->user_data['role_priority']);
        $this->db->where("role.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('role.*');
		$this->db->from('role');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("role.name LIKE '%".$fName."%'");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('role.status', $fStatus);
        }

        $this->db->where("role.priority > ".$this->authen->user_data['role_priority']);
        $this->db->where("role.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('priority asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id()
    {
        $this->db->select('*');
		$this->db->from('role');
		$this->db->where('id', $this->uri->segment(4));
        $this->db->where("role.priority > ".$this->authen->user_data['role_priority']);
        $this->db->where("role.status <> 'D'");
		$query = $this->db->get();
        return $query->row_array();
    }
	
    function insert()
    {
        if($this->input->post('priority') > $this->authen->user_data['role_priority'])
        {
    		$data['name'] = $this->input->post('name');
        	$data['priority'] = $this->input->post('priority');
    		$data['status'] = $this->input->post('status');
    		$data['created_on'] = date('Y-m-d H:i:s');
    		$data['created_by'] = $this->authen->username;
    		$data['updated_on'] = date('Y-m-d H:i:s');
    		$data['updated_by'] = '-';
            $this->db->insert('role', $data);
        }
    }
	
	function update()
    {
        if($this->input->post('priority') > $this->authen->user_data['role_priority'])
        {
        	$data['name'] = $this->input->post('name');
        	$data['priority'] = $this->input->post('priority');
    		$data['status'] = $this->input->post('status');
    		$data['updated_on'] = date('Y-m-d H:i:s');
    		$data['updated_by'] = $this->authen->username;
    		$this->db->where('id', $this->uri->segment(4));
            $this->db->where("role.priority > ".$this->authen->user_data['role_priority']);
            $this->db->where("role.status <> 'D'");
    		$this->db->update('role', $data);
        }
    }
	
	function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
    	$this->db->where('id', $this->uri->segment(4));
        $this->db->where("role.priority > ".$this->authen->user_data['role_priority']);
        $this->db->where("role.status <> 'D'");
        $this->db->update('role', $data);
    }
}