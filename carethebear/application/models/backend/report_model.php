<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_active() 
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('last_login');
        $query = $this->db->get();
        $data = $query->row_array();
        
        if (!empty($data)) {
            $lastLogin = strtotime($data['last_login']);
            $sixtyDays = strtotime($data['last_login'] . '-60 days');
            
            if ($lastLogin < $sixtyDays) {
                echo 'Yes';
            } else {
                echo 'No';
            }
        } 
    }

    /*
    function get_active() 
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('last_login');
        $query = $this->db->get();
        $data = $query->row_array();
        
        if ($data['last_login'] = date('Y-m-d H:i:s' < strtotime('-60 days'))) {

            echo 'Yes';
        } else {
            echo 'No';
        }
    }
    */
    function get_summary_cf()
    {
        $this->db->select('YEAR(activity.activity_date) as year, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->group_by('YEAR(activity.activity_date)');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_summary_cf_quarter($year)
    {
        $data = [];

        $this->db->select("'ไตรมาส 1' as quarter, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total", false);
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('YEAR(activity.activity_date)', $year);
        $this->db->where("MONTH(activity.activity_date) >= 1");
        $this->db->where("MONTH(activity.activity_date) <= 3");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 2' as quarter, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total", false);
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('YEAR(activity.activity_date)', $year);
        $this->db->where("MONTH(activity.activity_date) >= 4");
        $this->db->where("MONTH(activity.activity_date) <= 6");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 3' as quarter, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total", false);
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('YEAR(activity.activity_date)', $year);
        $this->db->where("MONTH(activity.activity_date) >= 7");
        $this->db->where("MONTH(activity.activity_date) <= 9");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 4' as quarter, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total", false);
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('activity.status', 'Y');
        $this->db->where('YEAR(activity.activity_date)', $year);
        $this->db->where("MONTH(activity.activity_date) >= 10");
        $this->db->where("MONTH(activity.activity_date) <= 12");
        $query = $this->db->get();
        $data[] = $query->row_array();

        return $data;
    }

    function get_summary_cf_month($year)
    {
        $data = [];

        $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

        foreach($month as $key => $value)
        {
            $this->db->select("'".$value."' as month, sum(activity.cf_care_1) as cf_care_1, sum(activity.cf_care_2) as cf_care_2, sum(activity.cf_care_3) as cf_care_3, sum(activity.cf_care_4) as cf_care_4, sum(activity.cf_care_5) as cf_care_5, sum(activity.cf_care_6) as cf_care_6, sum(activity.cf_care_total) as cf_care_total", false);
            $this->db->from('activity');
            $this->db->join('member', 'member.id = activity.member_id');
            $this->db->where('member.status', 'Y');
            $this->db->where('activity.status', 'Y');
            $this->db->where('YEAR(activity.activity_date)', $year);
            $this->db->where('MONTH(activity.activity_date)', ($key + 1));
            $query = $this->db->get();
            $data[] = $query->row_array();
        }

        return $data;
    }

    function get_project_summary_cf()
    {
        $this->db->select('YEAR(project.start_date) as year, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->group_by('YEAR(project.start_date)');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_project_summary_cf_quarter($year)
    {
        $data = [];

        $this->db->select("'ไตรมาส 1' as quarter, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total", false);
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('YEAR(project.start_date)', $year);
        $this->db->where("MONTH(project.start_date) >= 1");
        $this->db->where("MONTH(project.start_date) <= 3");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 2' as quarter, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total", false);
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('YEAR(project.start_date)', $year);
        $this->db->where("MONTH(project.start_date) >= 4");
        $this->db->where("MONTH(project.start_date) <= 6");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 3' as quarter, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total", false);
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('YEAR(project.start_date)', $year);
        $this->db->where("MONTH(project.start_date) >= 7");
        $this->db->where("MONTH(project.start_date) <= 9");
        $query = $this->db->get();
        $data[] = $query->row_array();

        $this->db->select("'ไตรมาส 4' as quarter, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total", false);
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('member.status', 'Y');
        $this->db->where('project.status', 'Y');
        $this->db->where('YEAR(project.start_date)', $year);
        $this->db->where("MONTH(project.start_date) >= 10");
        $this->db->where("MONTH(project.start_date) <= 12");
        $query = $this->db->get();
        $data[] = $query->row_array();

        return $data;
    }

    function get_project_summary_cf_month($year)
    {
        $data = [];

        $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

        foreach($month as $key => $value)
        {
            $this->db->select("'".$value."' as month, sum(project.cf_care_1) as cf_care_1, sum(project.cf_care_2) as cf_care_2, sum(project.cf_care_3) as cf_care_3, sum(project.cf_care_total) as cf_care_total", false);
            $this->db->from('project');
            $this->db->join('member', 'member.id = project.member_id');
            $this->db->where('member.status', 'Y');
            $this->db->where('project.status', 'Y');
            $this->db->where('YEAR(project.start_date)', $year);
            $this->db->where('MONTH(project.start_date)', ($key + 1));
            $query = $this->db->get();
            $data[] = $query->row_array();
        }

        return $data;
    }

    function count_all_member()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('member');

        $fName = @$this->input->post('columns')[0]['search']['value'];

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fName != "")
        {
            $this->db->where("(member.email LIKE '%".$fName."%' or member.username LIKE '%".$fName."%' or member.company LIKE '%".$fName."%' or member.position LIKE '%".$fName."%' or member.department LIKE '%".$fName."%' or member.address LIKE '%".$fName."%' or member.tel LIKE '%".$fName."%' or member.mobile LIKE '%".$fName."%')");
        }

        $this->db->where('member.status', 'Y');

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all_member($start = 0, $limit = 0)
    {
        $this->db->select('member.*, (select count(*) from activity where member_id = member.id and status = \'Y\') as activity_count, (select count(*) from project where member_id = member.id and status = \'Y\') as project_count, (select sum(cf_care_total) from activity where member_id = member.id and status = \'Y\') as cf_care_total, (select sum(cf_care_total) from project where member_id = member.id and status = \'Y\') as project_cf_care_total, ((select sum(cf_care_total) from activity where member_id = member.id and status = \'Y\') + (select sum(cf_care_total) from project where member_id = member.id and status = \'Y\')) as total, company_type.name as company_type, company_group.name as company_group');
        $this->db->from('member');
        $this->db->join('company_type', 'company_type.id = member.company_type_id', 'left');
        $this->db->join('company_group', 'company_group.id = member.company_group_id', 'left');

        $fName = @$this->input->post('columns')[0]['search']['value'];

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fName != "")
        {
            $this->db->where("(member.email LIKE '%".$fName."%' or member.username LIKE '%".$fName."%' or member.company LIKE '%".$fName."%' or member.position LIKE '%".$fName."%' or member.department LIKE '%".$fName."%' or member.address LIKE '%".$fName."%' or member.tel LIKE '%".$fName."%' or member.mobile LIKE '%".$fName."%')");
        }

        $this->db->where('member.status', 'Y');

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('member.id desc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_member_by_id()
    {
        $this->db->select('member.*, (select count(*) from activity where member_id = member.id and status = \'Y\') as activity_count, (select count(*) from project where member_id = member.id and status = \'Y\') as project_count, (select sum(cf_care_1) from activity where member_id = member.id and status = \'Y\') as cf_care_1, (select sum(cf_care_2) from activity where member_id = member.id and status = \'Y\') as cf_care_2, (select sum(cf_care_3) from activity where member_id = member.id and status = \'Y\') as cf_care_3, (select sum(cf_care_4) from activity where member_id = member.id and status = \'Y\') as cf_care_4, (select sum(cf_care_5) from activity where member_id = member.id and status = \'Y\') as cf_care_5, (select sum(cf_care_6) from activity where member_id = member.id and status = \'Y\') as cf_care_6, (select sum(cf_care_total) from activity where member_id = member.id and status = \'Y\') as cf_care_total, (select sum(cf_care_1) from project where member_id = member.id and status = \'Y\') as project_cf_care_1, (select sum(cf_care_2) from project where member_id = member.id and status = \'Y\') as project_cf_care_2, (select sum(cf_care_3) from project where member_id = member.id and status = \'Y\') as project_cf_care_3, (select sum(cf_care_total) from project where member_id = member.id and status = \'Y\') as project_cf_care_total, company_type.name as company_type, company_group.name as company_group');
        $this->db->from('member');
        $this->db->join('company_type', 'company_type.id = member.company_type_id', 'left');
        $this->db->join('company_group', 'company_group.id = member.company_group_id', 'left');
        $this->db->where('member.id', $this->uri->segment(4));
        $this->db->where('member.status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        $this->db->select('activity.*');
        $this->db->from('activity');
        $this->db->where('activity.member_id', $this->uri->segment(4));
        $this->db->where('activity.status', 'Y');
        $this->db->order_by('activity.activity_date asc, activity.start_on asc');
        $query = $this->db->get();
        $data['activity'] = $query->result_array();

        $this->db->select('project.*');
        $this->db->from('project');
        $this->db->where('project.member_id', $this->uri->segment(4));
        $this->db->where('project.status', 'Y');
        $this->db->order_by('project.start_date asc, project.start_on asc');
        $query = $this->db->get();
        $data['project'] = $query->result_array();

        return $data;
    }

    function count_all_activity()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');

        $fFrom = @$this->input->post('columns')[0]['search']['value'];
        $fTo = @$this->input->post('columns')[1]['search']['value'];
        $fName = @$this->input->post('columns')[2]['search']['value'];

        if(@$this->input->get('fFrom') != '')
        {
            $fFrom = $this->input->get('fFrom');
        }

        if(@$this->input->get('fTo') != '')
        {
            $fTo = $this->input->get('fTo');
        }

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fFrom != "")
        {
            $this->db->where("activity.activity_date >= '".$fFrom."'");
        }

        if($fTo != "")
        {
            $this->db->where("activity.activity_date <= '".$fTo."'");
        }

        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or activity.name LIKE '%".$fName."%' or activity.place LIKE '%".$fName."%' or activity.description LIKE '%".$fName."%')");
        }

        $this->db->where('activity.status', 'Y');

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all_activity($start = 0, $limit = 0)
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');

        $fFrom = @$this->input->post('columns')[0]['search']['value'];
        $fTo = @$this->input->post('columns')[1]['search']['value'];
        $fName = @$this->input->post('columns')[2]['search']['value'];

        if(@$this->input->get('fFrom') != '')
        {
            $fFrom = $this->input->get('fFrom');
        }

        if(@$this->input->get('fTo') != '')
        {
            $fTo = $this->input->get('fTo');
        }

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fFrom != "")
        {
            $this->db->where("activity.activity_date >= '".$fFrom."'");
        }

        if($fTo != "")
        {
            $this->db->where("activity.activity_date <= '".$fTo."'");
        }

        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or activity.name LIKE '%".$fName."%' or activity.place LIKE '%".$fName."%' or activity.description LIKE '%".$fName."%')");
        }

        $this->db->where('activity.status', 'Y');

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('activity.activity_date asc, activity.start_on asc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_activity_by_id()
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('activity.id', $this->uri->segment(4));
        $this->db->where('activity.status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }

    function count_all_project()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');

        $fStart = @$this->input->post('columns')[0]['search']['value'];
        $fEnd = @$this->input->post('columns')[1]['search']['value'];
        $fName = @$this->input->post('columns')[2]['search']['value'];

        if(@$this->input->get('fFrom') != '')
        {
            $fStart = $this->input->get('fFrom');
        }

        if(@$this->input->get('fTo') != '')
        {
            $fEnd = $this->input->get('fTo');
        }

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fStart != '' && $fEnd != '')
        {
            $this->db->where("((project.start_date >= '".$fStart."' and project.end_date <= '".$fEnd."') or (project.start_date <= '".$fStart."' and project.end_date >= '".$fStart."' and project.end_date <= '".$fEnd."') or (project.start_date >= '".$fStart."' and project.start_date <= '".$fEnd."' and project.end_date >= '".$fEnd."') or (project.start_date <= '".$fStart."' and project.end_date >= '".$fEnd."'))");
        }
        else if($fStart != '' && $fEnd == '')
        {
            $this->db->where("((project.start_date <= '".$fStart."' and project.end_date >= '".$fStart."') or (project.start_date >= '".$fStart."' and project.end_date >= '".$fStart."'))");
        }
        else if($fStart == '' && $fEnd != '')
        {
            $this->db->where("((project.start_date <= '".$fEnd."' and project.end_date >= '".$fEnd."') or (project.start_date <= '".$fEnd."' and project.end_date <= '".$fEnd."'))");
        }

        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or project.name LIKE '%".$fName."%' or project.place LIKE '%".$fName."%' or project.description LIKE '%".$fName."%')");
        }

        $this->db->where('project.status', 'Y');

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all_project($start = 0, $limit = 0)
    {
        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');

        $fStart = @$this->input->post('columns')[0]['search']['value'];
        $fEnd = @$this->input->post('columns')[1]['search']['value'];
        $fName = @$this->input->post('columns')[2]['search']['value'];

        if(@$this->input->get('fFrom') != '')
        {
            $fStart = $this->input->get('fFrom');
        }

        if(@$this->input->get('fTo') != '')
        {
            $fEnd = $this->input->get('fTo');
        }

        if(@$this->input->get('fName') != '')
        {
            $fName = $this->input->get('fName');
        }

        if($fStart != '' && $fEnd != '')
        {
            $this->db->where("((project.start_date >= '".$fStart."' and project.end_date <= '".$fEnd."') or (project.start_date <= '".$fStart."' and project.end_date >= '".$fStart."' and project.end_date <= '".$fEnd."') or (project.start_date >= '".$fStart."' and project.start_date <= '".$fEnd."' and project.end_date >= '".$fEnd."') or (project.start_date <= '".$fStart."' and project.end_date >= '".$fEnd."'))");
        }
        else if($fStart != '' && $fEnd == '')
        {
            $this->db->where("((project.start_date <= '".$fStart."' and project.end_date >= '".$fStart."') or (project.start_date >= '".$fStart."' and project.end_date >= '".$fStart."'))");
        }
        else if($fStart == '' && $fEnd != '')
        {
            $this->db->where("((project.start_date <= '".$fEnd."' and project.end_date >= '".$fEnd."') or (project.start_date <= '".$fEnd."' and project.end_date <= '".$fEnd."'))");
        }

        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or project.name LIKE '%".$fName."%' or project.place LIKE '%".$fName."%' or project.description LIKE '%".$fName."%')");
        }

        $this->db->where('project.status', 'Y');

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('project.start_date asc, project.start_on asc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_project_by_id()
    {
        $this->db->select('project.*, member.company');
        $this->db->from('project');
        $this->db->join('member', 'member.id = project.member_id');
        $this->db->where('project.id', $this->uri->segment(4));
        $this->db->where('project.status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }
}