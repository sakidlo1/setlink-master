<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class member_model extends CI_Model {

    private $ci;
    private $caremaster;

    function __construct()
    {
        parent::__construct();

        $this->ci =& get_instance();
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function get_company()
    {
        $this->caremaster->select('*');
        $this->caremaster->from('company_masters');
        $this->caremaster->where("deleted_at is null");
        $this->caremaster->order_by('companyTh', 'asc');
        $query = $this->caremaster->get();
        return $query->result_array();
    }

    function get_active() 
    {
        $this->db->select('*');
        $this->db->from('member');
        $this->db->where('last_login');
        $query = $this->db->get();
        $data = $query->row_array();
        
        if ($data) {
            $last_login = strtotime($data['last_login']);
            $cutoff_date = strtotime('-60 days');

            if ($last_login < $cutoff_date) {
                return 'Yes';
            } else {
                return 'No';
            }
        }
    }


    function count_all()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('member');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(member.email LIKE '%".$fName."%' or member.username LIKE '%".$fName."%' or member.company LIKE '%".$fName."%' or member.position LIKE '%".$fName."%' or member.department LIKE '%".$fName."%' or member.address LIKE '%".$fName."%' or member.tel LIKE '%".$fName."%' or member.mobile LIKE '%".$fName."%')");
        }

        $fCompany = @$this->input->post('columns')[1]['search']['value'];
        if($fCompany != "")
        {
            $this->db->where('member.company_master_id', $fCompany);
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('member.status', $fStatus);
        }

        $this->db->where("member.status <> 'D'");

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('member.*, company_type.name as company_type, company_group.name as company_group');
        $this->db->from('member');
        $this->db->join('company_type', 'company_type.id = member.company_type_id', 'left');
        $this->db->join('company_group', 'company_group.id = member.company_group_id', 'left');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(member.email LIKE '%".$fName."%' or member.username LIKE '%".$fName."%' or member.company LIKE '%".$fName."%' or member.position LIKE '%".$fName."%' or member.department LIKE '%".$fName."%' or member.address LIKE '%".$fName."%' or member.tel LIKE '%".$fName."%' or member.mobile LIKE '%".$fName."%')");
        }

        $fCompany = @$this->input->post('columns')[1]['search']['value'];
        if($fCompany != "")
        {
            $this->db->where('member.company_master_id', $fCompany);
        }

        $fStatus = @$this->input->post('columns')[2]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('member.status', $fStatus);
        }

        $this->db->where("member.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('member.id desc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->db->select('member.*, company_type.name as company_type, company_group.name as company_group');
        $this->db->from('member');
        $this->db->join('company_type', 'company_type.id = member.company_type_id', 'left');
        $this->db->join('company_group', 'company_group.id = member.company_group_id', 'left');
        $this->db->where('member.id', $this->uri->segment(4));
        $this->db->where("member.status <> 'D'");
        $query = $this->db->get();
        return $query->row_array();
    }

    function check_email_exists($id, $email)
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('member');
        $this->db->where('email',$email);
        $this->db->where("id <> ".$id);
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        $data = $query->row_array();

        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function check_username_exists($id, $username)
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('member');
        $this->db->where('username',$username);
        $this->db->where("id <> ".$id);
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        $data = $query->row_array();
        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function update()
    {
        $data['name'] = $this->input->post('name');
        $data['position'] = $this->input->post('position');
        $data['department'] = $this->input->post('department');
        $data['address'] = $this->input->post('address');
        $data['tel'] = $this->input->post('tel');
        $data['mobile'] = $this->input->post('mobile');
        $data['email'] = $this->input->post('email');
        $data['username'] = $this->input->post('username');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('member', $data);
    }

    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where("member.status <> 'D'");
        $this->db->update('member', $data);
    }

    function approve() {
        $data['company_other'] = null;
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where("member.company_other = '1'");
        $this->db->update('member', $data);
    }

    function get_list_company_master() {
        $this->caremaster->select('id,companyTh as name_th,companyEn as name_en,companyCode as code');
        $this->caremaster->from('company_masters');
        $query = $this->caremaster->get();
        return $query->result_array();
    }
}