<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class activity_model extends CI_Model {

    private $ci;
    private $caremaster;


    function __construct()
    {
        /*
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        */
        parent::__construct();

        $this->ci =& get_instance();
        $this->caremaster = $this->ci->load->database('caremaster', TRUE);
    }

    function get_company()
    {
        $this->caremaster->select('*');
        $this->caremaster->from('company_masters');
        $this->caremaster->where("deleted_at is null");
        $this->caremaster->order_by('companyTh', 'asc');
        $query = $this->caremaster->get();
        return $query->result_array();
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');

        $fFrom = @$this->input->post('columns')[0]['search']['value'];
        if($fFrom != "")
        {
            $this->db->where("activity.activity_date >= '".$fFrom."'");
        }

        $fTo = @$this->input->post('columns')[1]['search']['value'];
        if($fTo != "")
        {
            $this->db->where("activity.activity_date <= '".$fTo."'");
        }

        $fName = @$this->input->post('columns')[2]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or activity.name LIKE '%".$fName."%' or activity.place LIKE '%".$fName."%' or activity.description LIKE '%".$fName."%')");
        }

        $fCompany = @$this->input->post('columns')[3]['search']['value'];
        if($fCompany != "")
        {
            $this->db->where('member.company_master_id', $fCompany);

        }

        $fStatus = @$this->input->post('columns')[4]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('activity.status', $fStatus);
        }

        $this->db->where("activity.status <> 'D'");

        $query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');

        $fFrom = @$this->input->post('columns')[0]['search']['value'];
        if($fFrom != "")
        {
            $this->db->where("activity.activity_date >= '".$fFrom."'");
        }

        $fTo = @$this->input->post('columns')[1]['search']['value'];
        if($fTo != "")
        {
            $this->db->where("activity.activity_date <= '".$fTo."'");
        }

        $fName = @$this->input->post('columns')[2]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(member.company LIKE '%".$fName."%' or activity.name LIKE '%".$fName."%' or activity.place LIKE '%".$fName."%' or activity.description LIKE '%".$fName."%')");
        }

        $fCompany = @$this->input->post('columns')[3]['search']['value'];
        if($fCompany != "")
        {
            $this->db->where('member.company_master_id', $fCompany);
        }

        $fStatus = @$this->input->post('columns')[4]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('activity.status', $fStatus);
        }

        $this->db->where("activity.status <> 'D'");

        if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
        {
            $this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
        }
        else
        {
            $this->db->order_by('activity.id desc');
        }

        if($limit > 0)
        {
            $this->db->limit($limit, $start);   
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_by_id()
    {
        $this->db->select('activity.*, member.company');
        $this->db->from('activity');
        $this->db->join('member', 'member.id = activity.member_id');
        $this->db->where('activity.id', $this->uri->segment(4));
        $this->db->where("activity.status <> 'D'");
        $query = $this->db->get();
        return $query->row_array();
    }

    function update()
    {
        //$data['member_id'] = $this->input->post('member_id');
        $data['name'] = $this->input->post('name');
        $data['place'] = $this->input->post('place');
        $data['attendant'] = $this->input->post('attendant');
        $data['description'] = $this->input->post('description');
        $data['vdo'] = $this->input->post('vdo');
        $data['status'] = $this->input->post('status');
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->update('activity', $data);
    }

    function delete()
    {
        $data['status'] = 'D';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->authen->username;
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where("activity.status <> 'D'");
        $this->db->update('activity', $data);
    }
}