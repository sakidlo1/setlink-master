<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}
		
		$this->smarty->assign('page_name', 'Report');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('file_manager_url', config_item('file_manager_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
    }

    public function index()
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Event');
		$list = $this->this_model->get_summary_cf();
		
		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_activity_by_year.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้กระดาษ/พลาสติก', 'Care 3 : ลดโฟม', 'Care 4 : ลดใช้พลังงาน', 'Care 5 : ขยะจากงาน', 'Care 6 : ตกแต่ง', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($item['year'] + 543), number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_4'], 2), number_format($item['cf_care_5'], 2), number_format($item['cf_care_6'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

    public function quarter($year)
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Event (Quarter)');
		$list = $this->this_model->get_summary_cf_quarter($year);

		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_activity_by_quarter.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Quarter', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้กระดาษ/พลาสติก', 'Care 3 : ลดโฟม', 'Care 4 : ลดใช้พลังงาน', 'Care 5 : ขยะจากงาน', 'Care 6 : ตกแต่ง', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($year + 543), $item['quarter'], number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_4'], 2), number_format($item['cf_care_5'], 2), number_format($item['cf_care_6'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->assign('year', $year);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

    public function month($year)
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Event (Month)');
		$list = $this->this_model->get_summary_cf_month($year);

		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_activity_by_month.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Month', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้กระดาษ/พลาสติก', 'Care 3 : ลดโฟม', 'Care 4 : ลดใช้พลังงาน', 'Care 5 : ขยะจากงาน', 'Care 6 : ตกแต่ง', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($year + 543), $item['month'], number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_4'], 2), number_format($item['cf_care_5'], 2), number_format($item['cf_care_6'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->assign('year', $year);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function all_project()
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Project');
		$list = $this->this_model->get_project_summary_cf();
		
		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_project_by_year.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ', 'Care 3 : ลดใช้กระดาษ', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($item['year'] + 543), number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

    public function all_project_quarter($year)
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Project (Quarter)');
		$list = $this->this_model->get_project_summary_cf_quarter($year);

		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_project_by_quarter.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Quarter', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ', 'Care 3 : ลดใช้กระดาษ', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($year + 543), $item['quarter'], number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->assign('year', $year);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

    public function all_project_month($year)
	{	
		$this->smarty->assign('page_name', 'Report CF by All for Project (Month)');
		$list = $this->this_model->get_project_summary_cf_month($year);

		if($this->input->get('export'))
		{
			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_cf_by_all_project_by_month.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Year', 'Month', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ', 'Care 3 : ลดใช้กระดาษ', 'Total', 'Tree']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [($year + 543), $item['month'], number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_total'], 2), number_format(($item['cf_care_total'] / 9))]);
		    }

			exit();
		}

		$this->smarty->assign('list', $list);
		$this->smarty->assign('year', $year);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

    public function member()
	{	
		$this->smarty->assign('page_name', 'Report CF by Member');

		if($this->input->get('export'))
		{
			$list = $this->this_model->get_all_member();

			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_by_member.csv";');

		    $f = fopen('php://output', 'w');

		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		    fputcsv($f, ['Company', 'Type', 'Group', 'Name', 'Activity', 'Total', 'Project', 'Total', 'Summary']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [$item['company'], $item['company_type'], $item['company_group'], $item['name'], number_format($item['activity_count']), number_format($item['cf_care_total'], 2), number_format($item['project_count']), number_format($item['project_cf_care_total'], 2), number_format($item['total'], 2)]);
		    }

			exit();
		}
		$this->smarty->assign('active', $this->this_model->get_active());
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function member_load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all_member();
		$data = $this->this_model->get_all_member($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}

	public function member_detail()
	{
		$this->smarty->assign('page_name', 'Report CF by Member Detail');

		$this->smarty->assign('item', $this->this_model->get_member_by_id());
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function activity()
	{	
		$this->smarty->assign('page_name', 'Report CF by Activity');

		if($this->input->get('export'))
		{
			$list = $this->this_model->get_all_activity();

			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_by_activity.csv";');

		    $f = fopen('php://output', 'w');
		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Date', 'Start', 'End', 'Name', 'Company', 'Attendant', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้กระดาษ/พลาสติก', 'Care 3 : ลดโฟม', 'Care 4 : ลดใช้พลังงาน', 'Care 5 : ขยะจากงาน', 'Care 6 : ตกแต่ง', 'Total']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [$item['activity_date'], $item['start_on'], $item['end_on'], $item['name'], $item['company'], number_format($item['attendant']), number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_4'], 2), number_format($item['cf_care_5'], 2), number_format($item['cf_care_6'], 2), number_format($item['cf_care_total'], 2)]);
		    }

			exit();
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function activity_load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all_activity();
		$data = $this->this_model->get_all_activity($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}

	public function activity_detail()
	{
		$this->smarty->assign('page_name', 'Report CF by Activity Detail');

		$this->smarty->assign('item', $this->this_model->get_activity_by_id());
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function project()
	{	
		$this->smarty->assign('page_name', 'Report CF by Project');

		if($this->input->get('export'))
		{
			$list = $this->this_model->get_all_project();

			header('Content-Type: application/csv');
		    header('Content-Disposition: attachment; filename="report_by_project.csv";');

		    $f = fopen('php://output', 'w');
		    fputs($f, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

		    fputcsv($f, ['Start', 'End', 'Name', 'Company', 'Attendant', 'Care 1 : เดินทาง', 'Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ', 'Care 3 : ลดใช้กระดาษ', 'Total']);

		    foreach($list as $item)
		    {
		    	fputcsv($f, [$item['start_date'].' '.$item['start_on'], $item['start_date'].' '.$item['end_on'], $item['name'], $item['company'], number_format($item['attendant']), number_format($item['cf_care_1'], 2), number_format($item['cf_care_2'], 2), number_format($item['cf_care_3'], 2), number_format($item['cf_care_total'], 2)]);
		    }

			exit();
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function project_load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all_project();
		$data = $this->this_model->get_all_project($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}

	public function project_detail()
	{
		$this->smarty->assign('page_name', 'Report CF by Project Detail');

		$this->smarty->assign('item', $this->this_model->get_project_by_id());
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}