<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}
		
		$this->smarty->assign('page_name', 'Member');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('file_manager_url', config_item('file_manager_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
    }

	public function index()
	{	
		$this->load->library('user_agent');
		if(strstr($this->agent->referrer(),$this->this_page.'/add') && $this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'Insert data successful.');
		}
		else if(strstr($this->agent->referrer(),$this->this_page.'/edit') && $this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'Update data successful.');
		}
		else if(strstr($this->agent->referrer(),$this->this_page.'/delete'))
		{
			$this->smarty->assign('success_msg', 'Delete data successful.');
		}
        else if(strstr($this->agent->referrer(),$this->this_page.'/approve'))
        {
            $this->smarty->assign('success_msg', 'Approve data successful.');
        }
		
		
		$this->smarty->assign('company', $this->this_model->get_company());
		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function active() {
        $this->load->model('get_active');
        $data['active_status'] = $this->get_active->get_active();
        $this->smarty->display('backend/'.$this->this_page.'.tpl');
    }

	public function check_email_exists($id)
    {

    	header('Content-Type: application/json');
		
		$email=$this->input->post('email');

		$output = array(
			"status" => (($this->this_model->check_email_exists($id, $email) == false) ? false : true)
		);

		echo json_encode($output);
    }

	public function check_username_exists($id)
	{
		
		header('Content-Type: application/json');

		$username=$this->input->post('username');

		$output = array(
			"status" => (($this->this_model->check_username_exists($id, $username) == false) ? false : true)
		);

		echo json_encode($output);
	}

	public function load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all();
		$data = $this->this_model->get_all($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}

	public function edit()
	{ 
		$item = $this->this_model->get_by_id();
		if($this->input->post('save'))
		{
            $this->this_model->update();
			
            if($item['status'] == 'N' && $this->input->post('status') == 'Y')
            {
            	$email = $item['email'];
				$subject = 'การเข้าใช้งาน Care the Bear';

				$message = $this->smarty->fetch('email/approve_member.tpl');
				$message = str_replace('[[company_name]]', $item['company'], $message);

				$this->load->library('mailer');
				$this->mailer->send($email, $subject, $message);
            }

			redirect('/backend/'.$this->this_page.'?update=1');
		}
		else
		{
			$this->smarty->assign('item', $item);
            $this->smarty->assign('company_lists', $this->this_model->get_list_company_master());
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function delete()
	{
		$this->this_model->delete();
		$this->smarty->assign('location', 'backend/'.$this->this_page);
		$this->smarty->display('backend/do_complete.tpl');
	}

    public function approve()
    {
        $this->this_model->approve();
        $this->smarty->assign('location', 'backend/'.$this->this_page);
        $this->smarty->display('backend/do_complete.tpl');
    }
}