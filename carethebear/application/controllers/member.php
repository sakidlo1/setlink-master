<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
        $this->this_page_climatrcare = config_item('master_base_url');
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');
		$this->smarty->assign('banner', $this->layout_model->get_banner());

		$this->load->model($this->this_page.'_model', 'this_model');
		
		$this->smarty->assign('page_name', 'Member');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('master_base_url', config_item('master_base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}

	public function check_email_exists()
	{
		header('Content-Type: application/json');

    	$inputJSON = file_get_contents('php://input');
		$input = json_decode($inputJSON, TRUE);

		$output = array(
			"status" => (($this->this_model->check_email_exists($input) == false) ? false : true)
		);

		echo json_encode($output);
	}

	public function check_username_exists()
	{
		header('Content-Type: application/json');

    	$inputJSON = file_get_contents('php://input');
		$input = json_decode($inputJSON, TRUE);

		$output = array(
			"status" => (($this->this_model->check_username_exists($input) == false) ? false : true)
		);

		echo json_encode($output);
	}

	public function register()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		if(@$_SESSION['climate_member']['id'] > 0)
		{
			$this->smarty->assign('climate_member', $_SESSION['climate_member']);
		}

		$this->smarty->assign('page_name', 'สมัครสมาชิก');
		
		if($this->input->post('email') != "")
		{
			$salt = md5($this->input->post('email').'-'.$this->input->post('name').'-'.time());
			$_id = $this->this_model->register($salt);

			if(@$_FILES['logo']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_id%4000)))
				{
					mkdir('./images/upload/member/'.($_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_id%4000).'/'.$_id))
				{
					mkdir('./images/upload/member/'.($_id%4000).'/'.$_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_id%4000).'/'.$_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('logo'))
				{
					$upload_info = $this->upload->data();
					$this->this_model->update_logo($_id, $upload_info['file_name']);
				}
			}

			/*
			$email = $this->input->post('email');
			$verify_email_url = config_item('base_url').$this->this_page.'/verify_email?email='.$email.'&salt='.$salt;
			$to = $email;
			$subject = 'กรุณายืนยันการสมัครสมาชิก - '.config_item('site_name');
			$message = $this->smarty->fetch('email/register.tpl');
			$message = str_replace('[[link]]', $verify_email_url, $message);
			*/

			$email = 'climatecareplatform@set.or.th';
			$to = $email;
			$subject = 'มีบริษัทสมัครสมาชิกเข้ามาใหม่ - '.config_item('site_name');
			$message = $this->smarty->fetch('email/register_for_staff.tpl');
			$message = str_replace('[[company]]', $this->input->post('company'), $message);
			$message = str_replace('[[name]]', $this->input->post('name'), $message);
			$message = str_replace('[[position]]', $this->input->post('position'), $message);
			$message = str_replace('[[department]]', $this->input->post('department'), $message);
			$message = str_replace('[[tel]]', $this->input->post('tel'), $message);
			$message = str_replace('[[mobile]]', $this->input->post('mobile'), $message);
			$message = str_replace('[[email]]', $this->input->post('email'), $message);

			$this->load->library('mailer');
			$this->mailer->send($email, $subject, $message);

			redirect('/'.$this->this_page.'/login?register=1');
		}
		else
		{
			$this->smarty->assign('company_type', $this->this_model->get_company_type());
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function verify_email()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		$this->smarty->assign('page_name', 'ยืนยันอีเมล');

		if($this->input->get('email') != "" && $this->input->get('salt') != "")
		{
			$salt = $this->this_model->get_salt($this->input->get('email'));
			if($salt == false || $salt == "" || $salt != $this->input->get('salt'))
			{
				redirect('/');
			}
			else
			{
				$this->this_model->verify_email($this->input->get('email'));
				redirect('/'.$this->this_page.'/login?verify=1');
			}
		}
		else
		{
			redirect('/');
		}
	}

	public function login()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'เข้าสู่ระบบ');

		if($this->input->post('login'))
		{
			$data = $this->this_model->login();
			if(@$data['id']>0)
			{
				$_SESSION['carethebear_member'] = $data;
				redirect('/member/activity');
			}
			else
			{
				$this->smarty->assign('error_msg', 'รหัสผู้ใช้งาน และรหัสผ่านไม่ถูกต้อง');
				$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
		}
		else
		{
			if($this->input->get('register') == "1")
			{
				//$this->smarty->assign('success_msg', 'คุณได้สมัครสมาชิกเรียบร้อยแล้ว กรุณายืนยันอีเมลก่อนเข้าสู่ระบบ');
				$this->smarty->assign('success_msg', 'คุณได้สมัครสมาชิกเรียบร้อยแล้ว กรุณารอเจ้าหน้าที่ทำการอนุมัติก่อนเข้าสู่ระบบ');
			}
			else if($this->input->get('verify') == "1")
			{
				$this->smarty->assign('success_msg', 'คุณได้ทำการยืนยันอีเมลเรียบร้อยแล้ว กรุณาเข้าสู่ระบบ');
			}
			else if($this->input->get('reset') == "1")
			{
				$this->smarty->assign('success_msg', 'ตั้งรหัสผ่านใหม่เรียบร้อยแล้ว กรุณาเข้าสู่ระบบด้วยรหัสผ่านใหม่');
			}

			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function forget_password()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		$this->smarty->assign('page_name', 'ลืมรหัสผ่าน');
		
		if($this->input->post('forget_password'))
		{
			$salt = $this->this_model->forget_password();
			if($salt != false)
			{
				$email = $this->input->post('email');
				$reset_password_url = config_item('base_url').$this->this_page.'/reset_password?email='.$email.'&salt='.$salt;
				$to = $email;
				$subject = 'ลืมรหัสผ่าน - '.config_item('site_name');
				$message = $this->smarty->fetch('email/forget_password.tpl');
				$message = str_replace('[[link]]', $reset_password_url, $message);

				$this->load->library('mailer');
				$this->mailer->send($email, $subject, $message);

				$this->smarty->assign('success_msg', 'แจ้งลืมรหัสผ่านสำเร็จแล้ว กรุณาตรวจสอบอีเมลเพื่อตั้งรหัสผ่านใหม่');
				$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
			else
			{
				$this->smarty->assign('error_msg', 'ไม่พบอีเมลในระบบ กรุณาสมัครสมาชิก');
				$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
		}
		else
		{
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function reset_password()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		$this->smarty->assign('page_name', 'ตั้งรหัสผ่านใหม่');
		
		if($this->input->get('email') != "" && $this->input->get('salt') != "")
		{
			$salt = $this->this_model->get_salt($this->input->get('email'));
			if($salt == false || $salt == "" || $salt != $this->input->get('salt'))
			{
				redirect('/');
			}
		}
		else
		{
			redirect('/');
		}
		
		if($this->input->post('reset_password'))
		{
			$this->this_model->reset_password();
			redirect('/'.$this->this_page.'/login?reset=1');
		}
		else
		{
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function logout()
	{
		unset($_SESSION['climate_member']);
		unset($_SESSION['carethebear_member']);
		unset($_SESSION['member']);
		redirect('/');
	}

	public function profile()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'แก้ไขรายละเอียดสมาชิก');
		
		if($this->input->post('name') != "")
		{
			$this->this_model->edit_profile();
			$_id = $this->authen->id;
			
			if(@$_FILES['logo']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_id%4000)))
				{
					mkdir('./images/upload/member/'.($_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_id%4000).'/'.$_id))
				{
					mkdir('./images/upload/member/'.($_id%4000).'/'.$_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_id%4000).'/'.$_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('logo'))
				{
					$upload_info = $this->upload->data();
					$this->this_model->update_logo($_id, $upload_info['file_name']);
				}
			}
			
			$_SESSION['carethebear_member'] = $this->this_model->get_my_profile();
			
			redirect('/'.$this->this_page.'/profile?update=1');
		}
		else
		{
			if($this->input->get('update') == "1")
			{
				$this->smarty->assign('success_msg', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
			}

			$this->smarty->assign('company_type', $this->this_model->get_company_type());
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function activity()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'กิจกรรมของคุณ');
		
		if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}

		$perpage = 20;
		
		if($this->input->get('page')>0)
		{
			$page = $this->input->get('page');
		}
		else
		{
			$page = 1;
		}
		
		$url = config_item('base_url').$this->this_page.'/'.$this->this_sub_page.'?page=';
		$get = '';
		
		$total = $this->this_model->count_all_activity();
		
		$this->smarty->assign('url', $url);
		$this->smarty->assign('get', $get);
		$this->smarty->assign('total_page', ceil($total/$perpage));
		$this->smarty->assign('current_page', $page);
		$this->smarty->assign('first_page', $url.'1'.$get);
		$this->smarty->assign('back_page', $url.(($page>1) ? ($page-1) : '1').$get);
		$this->smarty->assign('next_page', $url.(($page<ceil($total/$perpage)) ? ($page+1) : ceil($total/$perpage)).$get);
		$this->smarty->assign('last_page', $url.ceil($total/$perpage).$get);
		
		$data = $this->this_model->get_all_activity($perpage,$page);
		$this->smarty->assign('list', $data);

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function activity_add()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'เพิ่มกิจกรรม');
		
		if($this->input->post('save') != "")
		{
			$_id = $this->this_model->add_activity();
			$_member_id = $this->authen->id;
			
			if(@$_FILES['image_1']['name']!="" || $_FILES['image_2']['name']!="" || $_FILES['image_3']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_member_id%4000)))
				{
					mkdir('./images/upload/member/'.($_member_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id))
				{
					mkdir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_member_id%4000).'/'.$_member_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);

				if(@$_FILES['image_1']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_1'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_1($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_2']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_2'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_2($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_3']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_3'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_3($_id, $_member_id, $upload_info['file_name']);
					}
                }
			}
			
			//redirect('/'.$this->this_page.'/activity?update=1');
            redirect($this->this_page_climatrcare.'dashboard/carethebear_activity');
		}
		else
		{
			$this->smarty->assign('activity_type', $this->this_model->get_activity_type());
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function activity_edit($id = 0)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'แก้ไขกิจกรรม');
		
		if($this->input->post('save') != "")
		{
			$this->this_model->update_activity($id);
			$_id = $id;
			$_member_id = $this->authen->id;
			
			if(@$_FILES['image_1']['name']!="" || $_FILES['image_2']['name']!="" || $_FILES['image_3']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_member_id%4000)))
				{
					mkdir('./images/upload/member/'.($_member_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id))
				{
					mkdir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_member_id%4000).'/'.$_member_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);

				if(@$_FILES['image_1']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_1'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_1($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_2']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_2'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_2($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_3']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_3'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_image_3($_id, $_member_id, $upload_info['file_name']);
					}
                }
			}
			
			//redirect('/'.$this->this_page.'/activity?update=1');
            redirect($this->this_page_climatrcare.'dashboard/carethebear_activity');
		}
		else
		{
			$this->smarty->assign('activity_type', $this->this_model->get_activity_type());
			$this->smarty->assign('activity', $this->this_model->get_activity_by_id($id));
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function activity_report($id)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());
		$activity_detail = $this->this_model->get_activity_by_id($id);
		$this->smarty->assign('activity', $activity_detail);
		//$this->smarty->assign('activity_date', $this->this_model->get_date_activity());
		$this->smarty->assign('today', date('Y-m-d'));
		$this->smarty->assign('month', date('Y-m'));

		$this->smarty->assign('page_name', 'รายงานกิจกรรม : '.$activity_detail['name']);

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function document()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'เอกสารเผยแพร่โครงการ Care the Bear');
		$this->smarty->assign('document', $this->this_model->get_all_document());

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function get_summary_cf($mode, $end_date, $from, $to, $month_year)
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_summary_cf($mode, $end_date, $from, $to, $month_year);
		echo json_encode($data);
	}

	public function activity_pdf($id)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$activity_detail = $this->this_model->get_activity_by_id($id);

		$this->load->library('excel');
		$sheet = $this->excel->getActiveSheet();
		$sheet->setCellValue('A1', 'รายงานการลดคาร์บอนฟุตพริ้นท์');

		$sheet->setCellValue('A2', 'บริษัท');
		$sheet->setCellValue('B2', $this->authen->member_data['company']);

		$sheet->setCellValue('A3', 'ฝ่าย');
		$sheet->setCellValue('B3', $this->authen->member_data['department']);

		$sheet->setCellValue('A4', 'ชื่อกิจกรรม');
		$sheet->setCellValue('B4', $activity_detail['name']);

		$sheet->setCellValue('A5', 'สถานที่');
		$sheet->setCellValue('B5', $activity_detail['place']);

		$sheet->setCellValue('A6', 'วันที่');
		$sheet->setCellValue('B6', $activity_detail['activity_date']);

		$sheet->setCellValue('A8', 'จำนวนผู้มาเข้าร่วมประชุม');
		$sheet->setCellValue('B8', $activity_detail['attendant']);
		$sheet->setCellValue('C8', 'คน');

		$sheet->setCellValue('A10', 'การจัดการของแจกให้ผู้ร่วมงาน');

		$sheet->setCellValue('B11', 'เอกสารแจก');
		$sheet->setCellValue('C11', $activity_detail['premium_1']);
		$sheet->setCellValue('D11', 'แผ่น');

		$sheet->setCellValue('B12', 'แฟ้มพลาสติก');
		$sheet->setCellValue('C12', $activity_detail['premium_2']);
		$sheet->setCellValue('D12', 'แฟ้ม');

		$sheet->setCellValue('B13', 'ถุงพลาสติก');
		$sheet->setCellValue('C13', $activity_detail['premium_3']);
		$sheet->setCellValue('D13', 'ใบ');

		$sheet->setCellValue('B14', 'แก้วพลาสติก');
		$sheet->setCellValue('C14', $activity_detail['premium_4']);
		$sheet->setCellValue('D14', 'ใบ');

		$sheet->setCellValue('B15', 'น้ำดื่มขวด PET');
		$sheet->setCellValue('C15', $activity_detail['premium_5']);
		$sheet->setCellValue('D15', 'ขวด');

		$sheet->setCellValue('A16', 'การเลี้ยงอาหารและเครื่องดื่ม');

		$sheet->setCellValue('B17', 'อาหารเช้า');
		$sheet->setCellValue('C17', $activity_detail['food_pax_1']);
		$sheet->setCellValue('D17', 'แพค');

		$sheet->setCellValue('B18', 'เบรกเช้า');
		$sheet->setCellValue('C18', $activity_detail['food_pax_2']);
		$sheet->setCellValue('D18', 'แพค');

		$sheet->setCellValue('B19', 'อาหารกลางวัน');
		$sheet->setCellValue('C19', $activity_detail['food_pax_3']);
		$sheet->setCellValue('D19', 'แพค');

		$sheet->setCellValue('B20', 'เบรกบ่าย');
		$sheet->setCellValue('C20', $activity_detail['food_pax_4']);
		$sheet->setCellValue('D20', 'แพค');

		$sheet->setCellValue('B21', 'อาหารเย็น');
		$sheet->setCellValue('C21', $activity_detail['food_pax_5']);
		$sheet->setCellValue('D21', 'แพค');

		$sheet->setCellValue('A22', 'การจัดการของเสียหลังจากการจัดงาน');

		$sheet->setCellValue('B23', 'เศษอาหาร');
		$sheet->setCellValue('C23', $activity_detail['waste_1']);
		$sheet->setCellValue('D23', 'ก.ก.');

		$sheet->setCellValue('B24', 'พลาสติก');
		$sheet->setCellValue('C24', $activity_detail['waste_2']);
		$sheet->setCellValue('D24', 'ก.ก.');

		$sheet->setCellValue('B25', 'กระดาษ/กระดาษกล่อง');
		$sheet->setCellValue('C25', $activity_detail['waste_3']);
		$sheet->setCellValue('D25', 'ก.ก.');

		$sheet->setCellValue('B26', 'อลูมิเนียม');
		$sheet->setCellValue('C26', $activity_detail['waste_4']);
		$sheet->setCellValue('D26', 'ก.ก.');

		$sheet->setCellValue('B27', 'เหล็ก');
		$sheet->setCellValue('C27', $activity_detail['waste_5']);
		$sheet->setCellValue('D27', 'ก.ก.');

		$sheet->setCellValue('B28', 'แก้ว');
		$sheet->setCellValue('C28', $activity_detail['waste_6']);
		$sheet->setCellValue('D28', 'ก.ก.');

        $sheet->setCellValue('A29', 'การลดการใช้อุปกรณ์ตกแต่ง');

        $sheet->setCellValue('B30', 'ไม้อัด ขนาดมาตรฐาน 4x8 ฟุต');

        $sheet->setCellValue('C31', '4 มม.');
		$sheet->setCellValue('D31', $activity_detail['wood_1']);
		$sheet->setCellValue('E31', 'แผ่น');

        $sheet->setCellValue('C32', '6 มม.');
		$sheet->setCellValue('D32', $activity_detail['wood_2']);
		$sheet->setCellValue('E32', 'แผ่น');

        $sheet->setCellValue('C33', '8 มม.');
		$sheet->setCellValue('D33', $activity_detail['wood_3']);
		$sheet->setCellValue('E33', 'แผ่น');

        $sheet->setCellValue('C34', '10 มม.');
		$sheet->setCellValue('D34', $activity_detail['wood_4']);
		$sheet->setCellValue('E34', 'แผ่น');

        $sheet->setCellValue('C35', '15 มม.');
		$sheet->setCellValue('D35', $activity_detail['wood_5']);
		$sheet->setCellValue('E35', 'แผ่น');

		$sheet->setCellValue('B36', 'ฟิวเจอร์บอร์ด PP Board ขนาดมาตรฐาน 130x145 ซม.');

        $sheet->setCellValue('C37', '2 มม.');
		$sheet->setCellValue('D37', $activity_detail['pp_1']);
		$sheet->setCellValue('E37', 'แผ่น');

        $sheet->setCellValue('C38', '4 มม.');
		$sheet->setCellValue('D38', $activity_detail['pp_2']);
		$sheet->setCellValue('E38', 'แผ่น');

        $sheet->setCellValue('C39', '6 มม.');
		$sheet->setCellValue('D39', $activity_detail['pp_3']);
		$sheet->setCellValue('E39', 'แผ่น');

        $sheet->setCellValue('C40', '8 มม.');
		$sheet->setCellValue('D40', $activity_detail['pp_4']);
		$sheet->setCellValue('E40', 'แผ่น');

        $sheet->setCellValue('C41', '10 มม.');
		$sheet->setCellValue('D41', $activity_detail['pp_5']);
		$sheet->setCellValue('E41', 'แผ่น');

		$sheet->setCellValue('A42', 'อุปกรณ์แสงสว่างในห้องประชุม');

		if($this->authen->member_data['is_set_staff'] == true)
		{
			$sheet->setCellValue('B43', 'ห้องประชุม');
			
			$sheet->setCellValue('C44', 'ตึก A');

			$sheet->setCellValue('D45', 'ชั้น 5 ห้อง 502 จำนวน');
			$sheet->setCellValue('E45', $activity_detail['building_a_1']);
			$sheet->setCellValue('F45', 'ชม.');

			$sheet->setCellValue('D46', 'ชั้น 5 ห้อง 503 จำนวน');
			$sheet->setCellValue('E46', $activity_detail['building_a_2']);
			$sheet->setCellValue('F46', 'ชม.');

			$sheet->setCellValue('D47', 'ชั้น 5 ห้อง 504 จำนวน');
			$sheet->setCellValue('E47', $activity_detail['building_a_3']);
			$sheet->setCellValue('F47', 'ชม.');

			$sheet->setCellValue('D48', 'ชั้น 5 ห้อง 505 จำนวน');
			$sheet->setCellValue('E48', $activity_detail['building_a_4']);
			$sheet->setCellValue('F48', 'ชม.');

			$sheet->setCellValue('C49', 'ตึก B');

			$sheet->setCellValue('D50', 'ชั้น 3 ห้อง สุกรีย์ จำนวน');
			$sheet->setCellValue('E50', $activity_detail['building_b_1']);
			$sheet->setCellValue('F50', 'ชม.');

			$sheet->setCellValue('D51', 'ชั้น 6 ห้อง 601 จำนวน');
			$sheet->setCellValue('E51', $activity_detail['building_b_2']);
			$sheet->setCellValue('F51', 'ชม.');

			$sheet->setCellValue('D52', 'ชั้น 6 ห้อง 602 จำนวน');
			$sheet->setCellValue('E52', $activity_detail['building_b_3']);
			$sheet->setCellValue('F52', 'ชม.');

			$sheet->setCellValue('D53', 'ชั้น 6 ห้อง 603 จำนวน');
			$sheet->setCellValue('E53', $activity_detail['building_b_4']);
			$sheet->setCellValue('F53', 'ชม.');

			$sheet->setCellValue('D54', 'ชั้น 7 ห้อง 701 จำนวน');
			$sheet->setCellValue('E54', $activity_detail['building_b_5']);
			$sheet->setCellValue('F54', 'ชม.');

			$sheet->setCellValue('D55', 'ชั้น 7 ห้อง 702 จำนวน');
			$sheet->setCellValue('E55', $activity_detail['building_b_6']);
			$sheet->setCellValue('F55', 'ชม.');

			$sheet->setCellValue('D56', 'ชั้น 7 ห้อง 703 จำนวน');
			$sheet->setCellValue('E56', $activity_detail['building_b_7']);
			$sheet->setCellValue('F56', 'ชม.');

			$sheet->setCellValue('D57', 'ชั้น 7 ห้อง 704 จำนวน');
			$sheet->setCellValue('E57', $activity_detail['building_b_8']);
			$sheet->setCellValue('F57', 'ชม.');

			$sheet->setCellValue('C58', 'ตึก C');

			$sheet->setCellValue('D59', 'ชั้น 7 ห้อง ศ.สังเวียน จำนวน');
			$sheet->setCellValue('E59', $activity_detail['building_c_1']);
			$sheet->setCellValue('F59', 'ชม.');
			
            $sheet->setCellValue('B60', 'ห้องประชุมอื่นๆ');

            $sheet->setCellValue('C61', 'อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED Bulb');

			$sheet->setCellValue('D62', 'ขนาด 3 วัตต์ จำนวน');
			$sheet->setCellValue('E62', $activity_detail['led_bulb_watt_1']);
			$sheet->setCellValue('F62', 'หลอด');
			$sheet->setCellValue('G62', $activity_detail['led_bulb_hour_1']);
			$sheet->setCellValue('H62', 'ชม.');

			$sheet->setCellValue('D63', 'ขนาด 4 วัตต์ จำนวน');
			$sheet->setCellValue('E63', $activity_detail['led_bulb_watt_2']);
			$sheet->setCellValue('F63', 'หลอด');
			$sheet->setCellValue('G63', $activity_detail['led_bulb_hour_2']);
			$sheet->setCellValue('H63', 'ชม.');

			$sheet->setCellValue('D64', 'ขนาด 5 วัตต์ จำนวน');
			$sheet->setCellValue('E64', $activity_detail['led_bulb_watt_3']);
			$sheet->setCellValue('F64', 'หลอด');
			$sheet->setCellValue('G64', $activity_detail['led_bulb_hour_3']);
			$sheet->setCellValue('H64', 'ชม.');

			$sheet->setCellValue('D65', 'ขนาด 6 วัตต์ จำนวน');
			$sheet->setCellValue('E65', $activity_detail['led_bulb_watt_4']);
			$sheet->setCellValue('F65', 'หลอด');
			$sheet->setCellValue('G65', $activity_detail['led_bulb_hour_4']);
			$sheet->setCellValue('H65', 'ชม.');

			$sheet->setCellValue('D66', 'ขนาด 7 วัตต์ จำนวน');
			$sheet->setCellValue('E66', $activity_detail['led_bulb_watt_5']);
			$sheet->setCellValue('F66', 'หลอด');
			$sheet->setCellValue('G66', $activity_detail['led_bulb_hour_5']);
			$sheet->setCellValue('H66', 'ชม.');

			$sheet->setCellValue('D67', 'ขนาด 9 วัตต์ จำนวน');
			$sheet->setCellValue('E67', $activity_detail['led_bulb_watt_6']);
			$sheet->setCellValue('F67', 'หลอด');
			$sheet->setCellValue('G67', $activity_detail['led_bulb_hour_6']);
			$sheet->setCellValue('H67', 'ชม.');

			$sheet->setCellValue('D68', 'ขนาด 10 วัตต์ จำนวน');
			$sheet->setCellValue('E68', $activity_detail['led_bulb_watt_7']);
			$sheet->setCellValue('F68', 'หลอด');
			$sheet->setCellValue('G68', $activity_detail['led_bulb_hour_7']);
			$sheet->setCellValue('H68', 'ชม.');

			$sheet->setCellValue('D69', 'ขนาด 12 วัตต์ จำนวน');
			$sheet->setCellValue('E69', $activity_detail['led_bulb_watt_8']);
			$sheet->setCellValue('F69', 'หลอด');
			$sheet->setCellValue('G69', $activity_detail['led_bulb_hour_8']);
			$sheet->setCellValue('H69', 'ชม.');

			$sheet->setCellValue('D70', 'ขนาด 13 วัตต์ จำนวน');
			$sheet->setCellValue('E70', $activity_detail['led_bulb_watt_9']);
			$sheet->setCellValue('F70', 'หลอด');
			$sheet->setCellValue('G70', $activity_detail['led_bulb_hour_9']);
			$sheet->setCellValue('H70', 'ชม.');

			$sheet->setCellValue('D71', 'ขนาด 15 วัตต์ จำนวน');
			$sheet->setCellValue('E71', $activity_detail['led_bulb_watt_10']);
			$sheet->setCellValue('F71', 'หลอด');
			$sheet->setCellValue('G71', $activity_detail['led_bulb_hour_10']);
			$sheet->setCellValue('H71', 'ชม.');

			$sheet->setCellValue('D72', 'ขนาด 18 วัตต์ จำนวน');
			$sheet->setCellValue('E72', $activity_detail['led_bulb_watt_11']);
			$sheet->setCellValue('F72', 'หลอด');
			$sheet->setCellValue('G72', $activity_detail['led_bulb_hour_11']);
			$sheet->setCellValue('H72', 'ชม.');

            $sheet->setCellValue('C73', 'อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED TUBE');

			$sheet->setCellValue('D74', 'ขนาด 5 วัตต์ จำนวน');
			$sheet->setCellValue('E74', $activity_detail['led_tube_watt_1']);
			$sheet->setCellValue('F74', 'หลอด');
			$sheet->setCellValue('G74', $activity_detail['led_tube_hour_1']);
			$sheet->setCellValue('H74', 'ชม.');

			$sheet->setCellValue('D75', 'ขนาด 6 วัตต์ จำนวน');
			$sheet->setCellValue('E75', $activity_detail['led_tube_watt_2']);
			$sheet->setCellValue('F75', 'หลอด');
			$sheet->setCellValue('G75', $activity_detail['led_tube_hour_2']);
			$sheet->setCellValue('H75', 'ชม.');

			$sheet->setCellValue('D76', 'ขนาด 8 วัตต์ จำนวน');
			$sheet->setCellValue('E76', $activity_detail['led_tube_watt_3']);
			$sheet->setCellValue('F76', 'หลอด');
			$sheet->setCellValue('G76', $activity_detail['led_tube_hour_3']);
			$sheet->setCellValue('H76', 'ชม.');

			$sheet->setCellValue('D77', 'ขนาด 10 วัตต์ จำนวน');
			$sheet->setCellValue('E77', $activity_detail['led_tube_watt_4']);
			$sheet->setCellValue('F77', 'หลอด');
			$sheet->setCellValue('G77', $activity_detail['led_tube_hour_4']);
			$sheet->setCellValue('H77', 'ชม.');

			$sheet->setCellValue('D78', 'ขนาด 14 วัตต์ จำนวน');
			$sheet->setCellValue('E78', $activity_detail['led_tube_watt_5']);
			$sheet->setCellValue('F78', 'หลอด');
			$sheet->setCellValue('G78', $activity_detail['led_tube_hour_5']);
			$sheet->setCellValue('H78', 'ชม.');

			$sheet->setCellValue('D79', 'ขนาด 18 วัตต์ จำนวน');
			$sheet->setCellValue('E79', $activity_detail['led_tube_watt_6']);
			$sheet->setCellValue('F79', 'หลอด');
			$sheet->setCellValue('G79', $activity_detail['led_tube_hour_6']);
			$sheet->setCellValue('H79', 'ชม.');

			$sheet->setCellValue('D80', 'ขนาด 20 วัตต์ จำนวน');
			$sheet->setCellValue('E80', $activity_detail['led_tube_watt_7']);
			$sheet->setCellValue('F80', 'หลอด');
			$sheet->setCellValue('G80', $activity_detail['led_tube_hour_7']);
			$sheet->setCellValue('H80', 'ชม.');

			$sheet->setCellValue('D81', 'ขนาด 22 วัตต์ จำนวน');
			$sheet->setCellValue('E81', $activity_detail['led_tube_watt_8']);
			$sheet->setCellValue('F81', 'หลอด');
			$sheet->setCellValue('G81', $activity_detail['led_tube_hour_8']);
			$sheet->setCellValue('H81', 'ชม.');

			$sheet->setCellValue('D82', 'ขนาด 24 วัตต์ จำนวน');
			$sheet->setCellValue('E82', $activity_detail['led_tube_watt_9']);
			$sheet->setCellValue('F82', 'หลอด');
			$sheet->setCellValue('G82', $activity_detail['led_tube_hour_9']);
			$sheet->setCellValue('H82', 'ชม.');

			$sheet->setCellValue('D83', 'ขนาด 25 วัตต์ จำนวน');
			$sheet->setCellValue('E83', $activity_detail['led_tube_watt_10']);
			$sheet->setCellValue('F83', 'หลอด');
			$sheet->setCellValue('G83', $activity_detail['led_tube_hour_10']);
			$sheet->setCellValue('H83', 'ชม.');

			$sheet->setCellValue('D84', 'ขนาด 36 วัตต์ จำนวน');
			$sheet->setCellValue('E84', $activity_detail['led_tube_watt_11']);
			$sheet->setCellValue('F84', 'หลอด');
			$sheet->setCellValue('G84', $activity_detail['led_tube_hour_11']);
			$sheet->setCellValue('H84', 'ชม.');

			$sheet->setCellValue('A86', 'ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้');
			$sheet->setCellValue('B86', $activity_detail['cf_care_total']);
			$sheet->setCellValue('C86', 'kgCO2e เทียบเท่าการดูดซับ CO2/ปี ของต้นไม้');
			$sheet->setCellValue('D86', number_format(($activity_detail['cf_care_total']/9), 0, '.', ''));
			$sheet->setCellValue('E86', 'ต้น');
		}
		else
		{
			$sheet->setCellValue('B43', 'อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED Bulb');

			$sheet->setCellValue('C44', 'ขนาด 3 วัตต์ จำนวน');
			$sheet->setCellValue('D44', $activity_detail['led_bulb_watt_1']);
			$sheet->setCellValue('E44', 'หลอด');
			$sheet->setCellValue('F44', $activity_detail['led_bulb_hour_1']);
			$sheet->setCellValue('G44', 'ชม.');

			$sheet->setCellValue('C45', 'ขนาด 4 วัตต์ จำนวน');
			$sheet->setCellValue('D45', $activity_detail['led_bulb_watt_2']);
			$sheet->setCellValue('E45', 'หลอด');
			$sheet->setCellValue('F45', $activity_detail['led_bulb_hour_2']);
			$sheet->setCellValue('G45', 'ชม.');

			$sheet->setCellValue('C46', 'ขนาด 5 วัตต์ จำนวน');
			$sheet->setCellValue('D46', $activity_detail['led_bulb_watt_3']);
			$sheet->setCellValue('E46', 'หลอด');
			$sheet->setCellValue('F46', $activity_detail['led_bulb_hour_3']);
			$sheet->setCellValue('G46', 'ชม.');

			$sheet->setCellValue('C47', 'ขนาด 6 วัตต์ จำนวน');
			$sheet->setCellValue('D47', $activity_detail['led_bulb_watt_4']);
			$sheet->setCellValue('E47', 'หลอด');
			$sheet->setCellValue('F47', $activity_detail['led_bulb_hour_4']);
			$sheet->setCellValue('G47', 'ชม.');

			$sheet->setCellValue('C48', 'ขนาด 7 วัตต์ จำนวน');
			$sheet->setCellValue('D48', $activity_detail['led_bulb_watt_5']);
			$sheet->setCellValue('E48', 'หลอด');
			$sheet->setCellValue('F48', $activity_detail['led_bulb_hour_5']);
			$sheet->setCellValue('G48', 'ชม.');

			$sheet->setCellValue('C49', 'ขนาด 9 วัตต์ จำนวน');
			$sheet->setCellValue('D49', $activity_detail['led_bulb_watt_6']);
			$sheet->setCellValue('E49', 'หลอด');
			$sheet->setCellValue('F49', $activity_detail['led_bulb_hour_6']);
			$sheet->setCellValue('G49', 'ชม.');

			$sheet->setCellValue('C50', 'ขนาด 10 วัตต์ จำนวน');
			$sheet->setCellValue('D50', $activity_detail['led_bulb_watt_7']);
			$sheet->setCellValue('E50', 'หลอด');
			$sheet->setCellValue('F50', $activity_detail['led_bulb_hour_7']);
			$sheet->setCellValue('G50', 'ชม.');

			$sheet->setCellValue('C51', 'ขนาด 12 วัตต์ จำนวน');
			$sheet->setCellValue('D51', $activity_detail['led_bulb_watt_8']);
			$sheet->setCellValue('E51', 'หลอด');
			$sheet->setCellValue('F51', $activity_detail['led_bulb_hour_8']);
			$sheet->setCellValue('G51', 'ชม.');

			$sheet->setCellValue('C52', 'ขนาด 13 วัตต์ จำนวน');
			$sheet->setCellValue('D52', $activity_detail['led_bulb_watt_9']);
			$sheet->setCellValue('E52', 'หลอด');
			$sheet->setCellValue('F52', $activity_detail['led_bulb_hour_9']);
			$sheet->setCellValue('G52', 'ชม.');

			$sheet->setCellValue('C53', 'ขนาด 15 วัตต์ จำนวน');
			$sheet->setCellValue('D53', $activity_detail['led_bulb_watt_10']);
			$sheet->setCellValue('E53', 'หลอด');
			$sheet->setCellValue('F53', $activity_detail['led_bulb_hour_10']);
			$sheet->setCellValue('G53', 'ชม.');

			$sheet->setCellValue('C54', 'ขนาด 18 วัตต์ จำนวน');
			$sheet->setCellValue('D54', $activity_detail['led_bulb_watt_11']);
			$sheet->setCellValue('E54', 'หลอด');
			$sheet->setCellValue('F54', $activity_detail['led_bulb_hour_11']);
			$sheet->setCellValue('G54', 'ชม.');

            $sheet->setCellValue('B55', 'อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED TUBE');

			$sheet->setCellValue('C56', 'ขนาด 5 วัตต์ จำนวน');
			$sheet->setCellValue('D56', $activity_detail['led_tube_watt_1']);
			$sheet->setCellValue('E56', 'หลอด');
			$sheet->setCellValue('F56', $activity_detail['led_tube_hour_1']);
			$sheet->setCellValue('G56', 'ชม.');

			$sheet->setCellValue('C57', 'ขนาด 6 วัตต์ จำนวน');
			$sheet->setCellValue('D57', $activity_detail['led_tube_watt_2']);
			$sheet->setCellValue('E57', 'หลอด');
			$sheet->setCellValue('F57', $activity_detail['led_tube_hour_2']);
			$sheet->setCellValue('G57', 'ชม.');

			$sheet->setCellValue('C58', 'ขนาด 8 วัตต์ จำนวน');
			$sheet->setCellValue('D58', $activity_detail['led_tube_watt_3']);
			$sheet->setCellValue('E58', 'หลอด');
			$sheet->setCellValue('F58', $activity_detail['led_tube_hour_3']);
			$sheet->setCellValue('G58', 'ชม.');

			$sheet->setCellValue('C59', 'ขนาด 10 วัตต์ จำนวน');
			$sheet->setCellValue('D59', $activity_detail['led_tube_watt_4']);
			$sheet->setCellValue('E59', 'หลอด');
			$sheet->setCellValue('F59', $activity_detail['led_tube_hour_4']);
			$sheet->setCellValue('G59', 'ชม.');

			$sheet->setCellValue('C60', 'ขนาด 14 วัตต์ จำนวน');
			$sheet->setCellValue('D60', $activity_detail['led_tube_watt_5']);
			$sheet->setCellValue('E60', 'หลอด');
			$sheet->setCellValue('F60', $activity_detail['led_tube_hour_5']);
			$sheet->setCellValue('G60', 'ชม.');

			$sheet->setCellValue('C61', 'ขนาด 18 วัตต์ จำนวน');
			$sheet->setCellValue('D61', $activity_detail['led_tube_watt_6']);
			$sheet->setCellValue('E61', 'หลอด');
			$sheet->setCellValue('F61', $activity_detail['led_tube_hour_6']);
			$sheet->setCellValue('G61', 'ชม.');

			$sheet->setCellValue('C62', 'ขนาด 20 วัตต์ จำนวน');
			$sheet->setCellValue('D62', $activity_detail['led_tube_watt_7']);
			$sheet->setCellValue('E62', 'หลอด');
			$sheet->setCellValue('F62', $activity_detail['led_tube_hour_7']);
			$sheet->setCellValue('G62', 'ชม.');

			$sheet->setCellValue('C63', 'ขนาด 22 วัตต์ จำนวน');
			$sheet->setCellValue('D63', $activity_detail['led_tube_watt_8']);
			$sheet->setCellValue('E63', 'หลอด');
			$sheet->setCellValue('F63', $activity_detail['led_tube_hour_8']);
			$sheet->setCellValue('G63', 'ชม.');

			$sheet->setCellValue('C64', 'ขนาด 24 วัตต์ จำนวน');
			$sheet->setCellValue('D64', $activity_detail['led_tube_watt_9']);
			$sheet->setCellValue('E64', 'หลอด');
			$sheet->setCellValue('F64', $activity_detail['led_tube_hour_9']);
			$sheet->setCellValue('G64', 'ชม.');

			$sheet->setCellValue('C65', 'ขนาด 25 วัตต์ จำนวน');
			$sheet->setCellValue('D65', $activity_detail['led_tube_watt_10']);
			$sheet->setCellValue('E65', 'หลอด');
			$sheet->setCellValue('F65', $activity_detail['led_tube_hour_10']);
			$sheet->setCellValue('G65', 'ชม.');

			$sheet->setCellValue('C66', 'ขนาด 36 วัตต์ จำนวน');
			$sheet->setCellValue('D66', $activity_detail['led_tube_watt_11']);
			$sheet->setCellValue('E66', 'หลอด');
			$sheet->setCellValue('F66', $activity_detail['led_tube_hour_11']);
			$sheet->setCellValue('G66', 'ชม.');

			$sheet->setCellValue('A68', 'ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้');
			$sheet->setCellValue('B68', $activity_detail['cf_care_total']);
			$sheet->setCellValue('C68', 'kgCO2e เทียบเท่าการดูดซับ CO2/ปี ของต้นไม้');
			$sheet->setCellValue('D68', number_format(($activity_detail['cf_care_total']/9), 0, '.', ''));
			$sheet->setCellValue('E68', 'ต้น');
		}

		$this->excel->save($this->excel, 'activity_report.xlsx');

		/*
		$this->smarty->assign('activity', $activity_detail);

		$html = $this->smarty->fetch($this->this_page.'_'.$this->this_sub_page.'.tpl');
		$this->load->library('pdf', $html);
        $this->pdf->WriteHTML($html);
        $this->pdf->Output('CF_Report.pdf','D');
        */
	}

	public function project()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'โครงการของคุณ');
		
		if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}

		$perpage = 20;
		
		if($this->input->get('page')>0)
		{
			$page = $this->input->get('page');
		}
		else
		{
			$page = 1;
		}
		
		$url = config_item('base_url').$this->this_page.'/'.$this->this_sub_page.'?page=';
		$get = '';
		
		$total = $this->this_model->count_all_project();
		
		$this->smarty->assign('url', $url);
		$this->smarty->assign('get', $get);
		$this->smarty->assign('total_page', ceil($total/$perpage));
		$this->smarty->assign('current_page', $page);
		$this->smarty->assign('first_page', $url.'1'.$get);
		$this->smarty->assign('back_page', $url.(($page>1) ? ($page-1) : '1').$get);
		$this->smarty->assign('next_page', $url.(($page<ceil($total/$perpage)) ? ($page+1) : ceil($total/$perpage)).$get);
		$this->smarty->assign('last_page', $url.ceil($total/$perpage).$get);
        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());
		
		$data = $this->this_model->get_all_project($perpage,$page);
		$this->smarty->assign('list', $data);

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function project_add()
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'เพิ่มโครงการ');
		
		if($this->input->post('save') != "")
		{
			$_id = $this->this_model->add_project();
			$_member_id = $this->authen->id;
			
			if(@$_FILES['image_1']['name']!="" || $_FILES['image_2']['name']!="" || $_FILES['image_3']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_member_id%4000)))
				{
					mkdir('./images/upload/member/'.($_member_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id))
				{
					mkdir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_member_id%4000).'/'.$_member_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);

				if(@$_FILES['image_1']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_1'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_1($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_2']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_2'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_2($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_3']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_3'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_3($_id, $_member_id, $upload_info['file_name']);
					}
                }
			}
			//dashboard/carethebear
			//redirect('/'.$this->this_page.'/project?update=1');
            redirect($this->this_page_climatrcare.'dashboard/carethebear');
		}
		else
		{
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function project_edit($id = 0)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$this->smarty->assign('page_name', 'แก้ไขโครงการ');
		
		if($this->input->post('save') != "")
		{
			$this->this_model->update_project($id);
			$_id = $id;
			$_member_id = $this->authen->id;
			
			if(@$_FILES['image_1']['name']!="" || $_FILES['image_2']['name']!="" || $_FILES['image_3']['name']!="")
			{
				if(!is_dir('./images/upload/member'))
				{
					mkdir('./images/upload/member',0777);
				}

				if(!is_dir('./images/upload/member/'.($_member_id%4000)))
				{
					mkdir('./images/upload/member/'.($_member_id%4000),0777);
				}
				
				if(!is_dir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id))
				{
					mkdir('./images/upload/member/'.($_member_id%4000).'/'.$_member_id,0777);
				}
				
				$config['upload_path'] = './images/upload/member/'.($_member_id%4000).'/'.$_member_id.'/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']    = 20480;
				$config['overwrite']    = FALSE;
				$this->load->library('upload', $config);

				if(@$_FILES['image_1']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_1'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_1($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_2']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_2'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_2($_id, $_member_id, $upload_info['file_name']);
					}
                }

                if(@$_FILES['image_3']['name']!="")
				{
					$this->upload->initialize($config);
					if($this->upload->do_upload('image_3'))
					{
						$upload_info = $this->upload->data();
						$this->this_model->update_project_image_3($_id, $_member_id, $upload_info['file_name']);
					}
                }
			}
			
			//redirect('/'.$this->this_page.'/project?update=1');
            redirect($this->this_page_climatrcare.'dashboard/carethebear');
		}
		else
		{
			$this->smarty->assign('project', $this->this_model->get_project_by_id($id));
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function project_report($id)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

        $this->smarty->assign('stats', $this->this_model->get_dashboard_stats());
		$project_detail = $this->this_model->get_project_by_id($id);
		$this->smarty->assign('project', $project_detail);
		$this->smarty->assign('today', date('Y-m-d'));
		$this->smarty->assign('month', date('Y-m'));

		$this->smarty->assign('page_name', 'รายงานโครงการ : '.$project_detail['name']);

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function project_pdf($id)
	{
		if($this->authen->id <= 0)
		{
			redirect('/member/login');
		}

		$project_detail = $this->this_model->get_project_by_id($id);

		$this->load->library('excel');
		$sheet = $this->excel->getActiveSheet();
		$sheet->setCellValue('A1', 'รายงานการลดคาร์บอนฟุตพริ้นท์');

		$sheet->setCellValue('A2', 'บริษัท');
		$sheet->setCellValue('B2', $this->authen->member_data['company']);

		$sheet->setCellValue('A3', 'ฝ่าย');
		$sheet->setCellValue('B3', $this->authen->member_data['department']);

		$sheet->setCellValue('A4', 'ชื่อโครงการ');
		$sheet->setCellValue('B4', $project_detail['name']);

		$sheet->setCellValue('A5', 'สถานที่');
		$sheet->setCellValue('B5', $project_detail['place']);

		$sheet->setCellValue('A6', 'วันที่');
		$sheet->setCellValue('B6', $project_detail['start_date']);
		$sheet->setCellValue('C6', '-');
		$sheet->setCellValue('D6', $project_detail['end_date']);

		$sheet->setCellValue('A8', 'จำนวนผู้มาเข้าร่วมประชุม');
		$sheet->setCellValue('B8', $project_detail['attendant']);
		$sheet->setCellValue('C8', 'คน');

		$sheet->setCellValue('A10', 'การจัดการของแจกให้กับผู้ร่วมงาน');

		$sheet->setCellValue('B11', 'ถุงพลาสติก');
		$sheet->setCellValue('C11', $project_detail['care_2_1']);
		$sheet->setCellValue('D11', 'ใบ');

		$sheet->setCellValue('B12', 'ถุงกระดาษคราฟท์');
		$sheet->setCellValue('C12', $project_detail['care_2_2']);
		$sheet->setCellValue('D12', 'กก.');

		$sheet->setCellValue('A14', 'การจัดการของเสียหลังการจัดงาน');

		$sheet->setCellValue('B15', 'กระดาษ 55 แกรม');

		$sheet->setCellValue('C16', 'A3');
		$sheet->setCellValue('D16', $project_detail['care_3_1_1']);
		$sheet->setCellValue('E16', 'แผ่น');

		$sheet->setCellValue('C17', 'A4');
		$sheet->setCellValue('D17', $project_detail['care_3_1_2']);
		$sheet->setCellValue('E17', 'แผ่น');

		$sheet->setCellValue('C18', 'A5');
		$sheet->setCellValue('D18', $project_detail['care_3_1_3']);
		$sheet->setCellValue('E18', 'แผ่น');

		$sheet->setCellValue('C19', 'B3');
		$sheet->setCellValue('D19', $project_detail['care_3_1_4']);
		$sheet->setCellValue('E19', 'แผ่น');

		$sheet->setCellValue('C20', 'B4');
		$sheet->setCellValue('D20', $project_detail['care_3_1_5']);
		$sheet->setCellValue('E20', 'แผ่น');

		$sheet->setCellValue('C21', 'B5');
		$sheet->setCellValue('D21', $project_detail['care_3_1_6']);
		$sheet->setCellValue('E21', 'แผ่น');

		$sheet->setCellValue('B22', 'กระดาษ 70 แกรม');

		$sheet->setCellValue('C23', 'A3');
		$sheet->setCellValue('D23', $project_detail['care_3_2_1']);
		$sheet->setCellValue('E23', 'แผ่น');

		$sheet->setCellValue('C24', 'A4');
		$sheet->setCellValue('D24', $project_detail['care_3_2_2']);
		$sheet->setCellValue('E24', 'แผ่น');

		$sheet->setCellValue('C25', 'A5');
		$sheet->setCellValue('D25', $project_detail['care_3_2_3']);
		$sheet->setCellValue('E25', 'แผ่น');

		$sheet->setCellValue('C26', 'B3');
		$sheet->setCellValue('D26', $project_detail['care_3_2_4']);
		$sheet->setCellValue('E26', 'แผ่น');

		$sheet->setCellValue('C27', 'B4');
		$sheet->setCellValue('D27', $project_detail['care_3_2_5']);
		$sheet->setCellValue('E27', 'แผ่น');

		$sheet->setCellValue('C28', 'B5');
		$sheet->setCellValue('D28', $project_detail['care_3_2_6']);
		$sheet->setCellValue('E28', 'แผ่น');

		$sheet->setCellValue('B29', 'กระดาษ 80 แกรม');

		$sheet->setCellValue('C30', 'A3');
		$sheet->setCellValue('D30', $project_detail['care_3_3_1']);
		$sheet->setCellValue('E30', 'แผ่น');

		$sheet->setCellValue('C31', 'A4');
		$sheet->setCellValue('D31', $project_detail['care_3_3_2']);
		$sheet->setCellValue('E31', 'แผ่น');

		$sheet->setCellValue('C32', 'A5');
		$sheet->setCellValue('D32', $project_detail['care_3_3_3']);
		$sheet->setCellValue('E32', 'แผ่น');

		$sheet->setCellValue('C33', 'B3');
		$sheet->setCellValue('D33', $project_detail['care_3_3_4']);
		$sheet->setCellValue('E33', 'แผ่น');

		$sheet->setCellValue('C34', 'B4');
		$sheet->setCellValue('D34', $project_detail['care_3_3_5']);
		$sheet->setCellValue('E34', 'แผ่น');

		$sheet->setCellValue('C35', 'B5');
		$sheet->setCellValue('D35', $project_detail['care_3_3_6']);
		$sheet->setCellValue('E35', 'แผ่น');

		$sheet->setCellValue('B36', 'กระดาษ 90 แกรม');

		$sheet->setCellValue('C37', 'A3');
		$sheet->setCellValue('D37', $project_detail['care_3_4_1']);
		$sheet->setCellValue('E37', 'แผ่น');

		$sheet->setCellValue('C38', 'A4');
		$sheet->setCellValue('D38', $project_detail['care_3_4_2']);
		$sheet->setCellValue('E38', 'แผ่น');

		$sheet->setCellValue('C39', 'A5');
		$sheet->setCellValue('D39', $project_detail['care_3_4_3']);
		$sheet->setCellValue('E39', 'แผ่น');

		$sheet->setCellValue('C40', 'B3');
		$sheet->setCellValue('D40', $project_detail['care_3_4_4']);
		$sheet->setCellValue('E40', 'แผ่น');

		$sheet->setCellValue('C41', 'B4');
		$sheet->setCellValue('D41', $project_detail['care_3_4_5']);
		$sheet->setCellValue('E41', 'แผ่น');

		$sheet->setCellValue('C42', 'B5');
		$sheet->setCellValue('D42', $project_detail['care_3_4_6']);
		$sheet->setCellValue('E42', 'แผ่น');

		$sheet->setCellValue('B43', 'กระดาษ 100 แกรม');

		$sheet->setCellValue('C44', 'A3');
		$sheet->setCellValue('D44', $project_detail['care_3_5_1']);
		$sheet->setCellValue('E44', 'แผ่น');

		$sheet->setCellValue('C45', 'A4');
		$sheet->setCellValue('D45', $project_detail['care_3_5_2']);
		$sheet->setCellValue('E45', 'แผ่น');

		$sheet->setCellValue('C46', 'A5');
		$sheet->setCellValue('D46', $project_detail['care_3_5_3']);
		$sheet->setCellValue('E46', 'แผ่น');

		$sheet->setCellValue('C47', 'B3');
		$sheet->setCellValue('D47', $project_detail['care_3_5_4']);
		$sheet->setCellValue('E47', 'แผ่น');

		$sheet->setCellValue('C48', 'B4');
		$sheet->setCellValue('D48', $project_detail['care_3_5_5']);
		$sheet->setCellValue('E48', 'แผ่น');

		$sheet->setCellValue('C49', 'B5');
		$sheet->setCellValue('D49', $project_detail['care_3_5_6']);
		$sheet->setCellValue('E49', 'แผ่น');

		$sheet->setCellValue('B50', 'กระดาษ 120 แกรม');

		$sheet->setCellValue('C51', 'A3');
		$sheet->setCellValue('D51', $project_detail['care_3_6_1']);
		$sheet->setCellValue('E51', 'แผ่น');

		$sheet->setCellValue('C52', 'A4');
		$sheet->setCellValue('D52', $project_detail['care_3_6_2']);
		$sheet->setCellValue('E52', 'แผ่น');

		$sheet->setCellValue('C53', 'A5');
		$sheet->setCellValue('D53', $project_detail['care_3_6_3']);
		$sheet->setCellValue('E53', 'แผ่น');

		$sheet->setCellValue('C54', 'B3');
		$sheet->setCellValue('D54', $project_detail['care_3_6_4']);
		$sheet->setCellValue('E54', 'แผ่น');

		$sheet->setCellValue('C55', 'B4');
		$sheet->setCellValue('D55', $project_detail['care_3_6_5']);
		$sheet->setCellValue('E55', 'แผ่น');

		$sheet->setCellValue('C56', 'B5');
		$sheet->setCellValue('D56', $project_detail['care_3_6_6']);
		$sheet->setCellValue('E56', 'แผ่น');


		$sheet->setCellValue('A58', 'ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้');
		$sheet->setCellValue('B58', $project_detail['cf_care_total']);
		$sheet->setCellValue('C58', 'kgCO2e เทียบเท่าการดูดซับ CO2/ปี ของต้นไม้');
		$sheet->setCellValue('D58', number_format(($project_detail['cf_care_total']/9), 0, '.', ''));
		$sheet->setCellValue('E58', 'ต้น');

		$this->excel->save($this->excel, 'project_report.xlsx');

		/*
		$this->smarty->assign('project', $project_detail);

		$html = $this->smarty->fetch($this->this_page.'_'.$this->this_sub_page.'.tpl');
		$this->load->library('pdf', $html);
        $this->pdf->WriteHTML($html);
        $this->pdf->Output('CF_Report_Project.pdf','D');
        */
	}
}
