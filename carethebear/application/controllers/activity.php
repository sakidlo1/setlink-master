<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');
		$this->smarty->assign('banner', $this->layout_model->get_banner());

		$this->load->model($this->this_page.'_model', 'this_model');
		
		$this->smarty->assign('page_name', 'Activity');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('master_base_url', config_item('master_base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}

	public function index()
	{
		$this->smarty->assign('page_name', 'กิจกรรม');

		$perpage = 6;
		
		if($this->input->get('page')>0)
		{
			$page = $this->input->get('page');
		}
		else
		{
			$page = 1;
		}

		$this->smarty->assign('today_activity', $this->this_model->get_today_activity());
		
		$url = config_item('base_url').$this->this_page.'?page=';
		$get = (($this->input->get('type') != '') ? '&type='.$this->input->get('type') : '').(($this->input->get('q') != '') ? '&q='.$this->input->get('q') : '').(($this->input->get('activity_type_id') != '') ? '&activity_type_id='.$this->input->get('activity_type_id') : '');
		
		$total = $this->this_model->count_all_activity();
		
		$this->smarty->assign('url', $url);
		$this->smarty->assign('get', $get);
		$this->smarty->assign('total_page', ceil($total/$perpage));
		$this->smarty->assign('current_page', $page);
		$this->smarty->assign('first_page', $url.'1'.$get);
		$this->smarty->assign('back_page', $url.(($page>1) ? ($page-1) : '1').$get);
		$this->smarty->assign('next_page', $url.(($page<ceil($total/$perpage)) ? ($page+1) : ceil($total/$perpage)).$get);
		$this->smarty->assign('last_page', $url.ceil($total/$perpage).$get);
		
		$data = $this->this_model->get_all_activity($perpage,$page);
		$this->smarty->assign('list', $data);

		$this->smarty->assign('activity_type', $this->this_model->get_activity_type());
		$this->smarty->display($this->this_page.'.tpl');
	}

	public function detail($id)
	{
		$activity_detail = $this->this_model->get_activity_detail_by_id($id);
		$this->smarty->assign('activity_detail', $activity_detail);
		$this->smarty->assign('today_date', date('Y-m-d'));
		$this->smarty->assign('now_time', date('H:i:s'));
		$this->smarty->assign('start_time_minus_2_hours', date('H:i:s', strtotime($activity_detail['activity_date'].' '.$activity_detail['start_on'].' -2 hours')));

		$this->smarty->assign('page_name', $activity_detail['name'].' - '.$activity_detail['company']);
		
		if($this->input->post('save') != "")
		{
			$this->this_model->register_activity($id);
			redirect('/'.$this->this_page.'/detail/'.$id.'?update=1');
		}
		else
		{
			if($this->input->get('update') == "1")
			{
				$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว ขอบคุณที่ตอบแบบสอบถามค่ะ');
			}

			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}
}