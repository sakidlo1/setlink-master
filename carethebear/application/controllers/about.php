<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');
		$this->smarty->assign('banner', $this->layout_model->get_banner());

		$this->load->model($this->this_page.'_model', 'this_model');
		
		$this->smarty->assign('page_name', 'About');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('master_base_url', config_item('master_base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}
	
	public function index()
	{
		$this->smarty->assign('about_content', $this->this_model->get_content_by_code('about'));
		$this->smarty->assign('about_vdo', $this->this_model->get_content_by_code('about_vdo'));
		$this->smarty->assign('company', $this->this_model->get_company());
		$this->smarty->display($this->this_page.'.tpl');
	}
}