<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Home extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		
		if($this->authen->controller != "")
		{
			$this->this_page = $this->authen->controller;
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_page = "home";
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');
		$this->smarty->assign('banner', $this->layout_model->get_banner());

		$this->load->model($this->this_page.'_model', 'this_model');
		
		$this->smarty->assign('page_name', 'Home');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
        $this->smarty->assign('oauth2_authen_param', config_item('oauth2_authen_param'));
        $this->smarty->assign('callback_url', config_item('callback_url'));
		$this->smarty->assign('master_base_url', config_item('master_base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}
	
	public function index()
	{
		$this->smarty->assign('home_content', $this->this_model->get_content_by_code('home'));
		$this->smarty->assign('latest_activity', $this->this_model->get_latest_activity());
		$this->smarty->assign('year_activity', $this->this_model->get_year_activity());
		$this->smarty->assign('year_project', $this->this_model->get_year_project());
		$this->smarty->assign('article', $this->this_model->get_article());
		$this->smarty->assign('news', $this->this_model->get_news());
		$this->smarty->assign('tips', $this->this_model->get_tips());
		$this->smarty->assign('bear_stats', $this->this_model->get_bear_stats());
		$this->smarty->assign('company', $this->this_model->get_company());
		$this->smarty->display($this->this_page.'.tpl');
	}

	public function get_summary_cf($from, $to)
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_summary_cf($from, $to);
		echo json_encode($data);
	}

	public function get_project_summary_cf($from, $to)
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_project_summary_cf($from, $to);
		echo json_encode($data);
	}
}