<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('member', $this->authen->member_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->load->model('layout_model');
		$this->smarty->assign('banner', $this->layout_model->get_banner());

		$this->load->model($this->this_page.'_model', 'this_model');
		
		$this->smarty->assign('page_name', 'News');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('master_base_url', config_item('master_base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');
	}

	public function index()
	{
		$this->smarty->assign('page_name', 'ข่าวและประชาสัมพันธ์ (News)');

		$perpage = 3;
		
		if($this->input->get('page')>0)
		{
			$page = $this->input->get('page');
		}
		else
		{
			$page = 1;
		}

		$url = config_item('base_url').$this->this_page.'?page=';
		$get = (($this->input->get('q') != '') ? '&q='.$this->input->get('q') : '');
		
		$total = $this->this_model->count_all_news();
		
		$this->smarty->assign('url', $url);
		$this->smarty->assign('get', $get);
		$this->smarty->assign('total_page', ceil($total/$perpage));
		$this->smarty->assign('current_page', $page);
		$this->smarty->assign('first_page', $url.'1'.$get);
		$this->smarty->assign('back_page', $url.(($page>1) ? ($page-1) : '1').$get);
		$this->smarty->assign('next_page', $url.(($page<ceil($total/$perpage)) ? ($page+1) : ceil($total/$perpage)).$get);
		$this->smarty->assign('last_page', $url.ceil($total/$perpage).$get);
		
		$data = $this->this_model->get_all_news($perpage,$page);
		$this->smarty->assign('list', $data);

		$this->smarty->display($this->this_page.'.tpl');
	}

	public function detail($id)
	{
		$news_detail = $this->this_model->get_news_detail_by_id($id);
		$this->smarty->assign('news_detail', $news_detail);

		$this->smarty->assign('page_name', $news_detail['name']);

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}