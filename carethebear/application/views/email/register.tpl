<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div>
	    <a href="{$base_url}">
			<img src="{$image_url}theme/default/assets/images/logo.png" alt="{$site_name}" width="120px">
		</a>
		<br />
		<br />
	</div>
	<div>
		<br />
		สวัสดีค่ะ สมาชิก,
		<br />
		<p style="padding-left: 30px;">
			คุณได้ทำการสมัครสมาชิกเรียบร้อยแล้ว กรุณากดลิงค์ด้านล่างเพื่อทำการยืนยันอีเมล์
			<br />
			<br />
			<a href="[[link]]" target="_blank">ยืนยันอีเมล์</a>
		</p>
		<br />
		ขอต้อนรับสู่ SET Social Impact ค่ะ
		<br />
		ทีมงาน SET Social Impact
	</div>
  </body>
</html>