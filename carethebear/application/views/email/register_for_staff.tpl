<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div>
	    <a href="{$base_url}">
			<img src="{$image_url}theme/default/assets/images/logo.png" alt="{$site_name}" width="120px">
		</a>
		<br />
		<br />
	</div>
	<div>
		<br />
		เรียนเจ้าหน้าที่,
		<br />
		<p style="padding-left: 30px;">
			มีบริษัทสมัครสมาชิกเข้ามาใหม่ ตามรายละเอียดด้านล่าง
			<br />
			<br />
			<table border="1" cellpadding="2" cellspacing="2">
				<tr>
					<th>บริษัท/องค์กร</th>
					<td>[[company]]</td>
				</tr>
				<tr>
					<th>ชื่อ นามสกุล</th>
					<td>[[name]]</td>
				</tr>
				<tr>
					<th>ตำแหน่ง</th>
					<td>[[position]]</td>
				</tr>
				<tr>
					<th>ฝ่ายงาน</th>
					<td>[[department]]</td>
				</tr>
				<tr>
					<th>โทรศัพท์</th>
					<td>[[tel]]</td>
				</tr>
				<tr>
					<th>โทรศัพท์มือถือ</th>
					<td>[[mobile]]</td>
				</tr>
				<tr>
					<th>อีเมล</th>
					<td>[[email]]</td>
				</tr>
			</table>
			<br />
			<br />
			<a href="{$base_url}backend/member" target="_blank">กรุณาเข้าระบบเพื่ออนุมัติ</a>
		</p>
		<br />
		ขอบคุณค่ะ
		<br />
		ทีมงาน SET Social Impact
	</div>
  </body>
</html>