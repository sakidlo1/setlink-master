{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        <img class="img-cover is-small" src="{$image_url}theme/default/assets/images/add-cover.png">
                        <h3 class="activity-head">
                            <img src="{$image_url}theme/default/assets/images/icon-calendar.png">
                            <span>กิจกรรมของคุณ</span>
                        </h3>
                        <div class="activity-add">แก้ไขกิจกรรม <img src="{$image_url}theme/default/assets/images/icon-add.png"></div>
                        <script>
                            function check_data()
                            {
                                $('#name_req').hide();
                                $('#place_req').hide();
                                $('#activity_date_req').hide();
                                $('#time_req').hide();
                                $('#description_req').hide();
                                $('#attendant_req').hide();
                                $('#food_pax_1_req').hide();
                                $('#food_pax_2_req').hide();
                                $('#food_pax_3_req').hide();
                                $('#food_pax_4_req').hide();
                                $('#food_pax_5_req').hide();
                                $('.has-error').removeClass('has-error');
                                
                                with(document.add_edit)
                                {
                                    if(name.value=="")
                                    {
                                        $('#name_req').show();
                                        $('#name_req').parent('div').addClass('has-error');
                                        $('#name').focus();
                                        return false;
                                    }
                                    else if(place.value=="")
                                    {
                                        $('#place_req').show();
                                        $('#place_req').parent('div').addClass('has-error');
                                        $('#place').focus();
                                        return false;
                                    }
                                    else if(activity_date.value=="")
                                    {
                                        $('#activity_date_req').show();
                                        $('#activity_date_req').parent('div').addClass('has-error');
                                        $('#activity_date').focus();
                                        return false;
                                    }
                                    else if(start_on.value=="" || end_on.value=="")
                                    {
                                        $('#time_req').show();
                                        $('#time_req').parent('div').addClass('has-error');
                                        $('#start_on').focus();
                                        return false;
                                    }
                                    else if(description.value=="")
                                    {
                                        $('#description_req').show();
                                        $('#description_req').parent('div').addClass('has-error');
                                        $('#description').focus();
                                        return false;
                                    }
                                    else if(attendant.value=="")
                                    {
                                        $('#attendant_req').show();
                                        $('#attendant_req').parent('div').addClass('has-error');
                                        $('#attendant').focus();
                                        return false;
                                    }
                                    else if($('#food_no_foam_1').prop("checked") == true && food_pax_1.value=="")
                                    {
                                        $('#food_pax_1_req').show();
                                        $('#food_pax_1_req').parent('div').addClass('has-error');
                                        $('#food_pax_1').focus();
                                        return false;
                                    }
                                    else if($('#food_no_foam_2').prop("checked") == true && food_pax_2.value=="")
                                    {
                                        $('#food_pax_2_req').show();
                                        $('#food_pax_2_req').parent('div').addClass('has-error');
                                        $('#food_pax_2').focus();
                                        return false;
                                    }
                                    else if($('#food_no_foam_3').prop("checked") == true && food_pax_3.value=="")
                                    {
                                        $('#food_pax_3_req').show();
                                        $('#food_pax_3_req').parent('div').addClass('has-error');
                                        $('#food_pax_3').focus();
                                        return false;
                                    }
                                    else if($('#food_no_foam_4').prop("checked") == true && food_pax_4.value=="")
                                    {
                                        $('#food_pax_4_req').show();
                                        $('#food_pax_4_req').parent('div').addClass('has-error');
                                        $('#food_pax_4').focus();
                                        return false;
                                    }
                                    else if($('#food_no_foam_5').prop("checked") == true && food_pax_5.value=="")
                                    {
                                        $('#food_pax_5_req').show();
                                        $('#food_pax_5_req').parent('div').addClass('has-error');
                                        $('#food_pax_5').focus();
                                        return false;
                                    }
                                }
                            }
                        </script>
                        <form name="add_edit" class="form-project form-inline" method="post" onsubmit="return check_data();" enctype="multipart/form-data">
                            <div class="activity-control">
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label>ชื่อกิจกรรม</label>
                                            <input type="text" id="name" name="name" value="{$activity.name}" class="form-control">
                                            <p id="name_req" class="required">กรุณากรอกชื่อกิจกรรม</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group label-small">
                                            <label>วันที่</label>
                                            <input type="date" id="activity_date" name="activity_date" value="{$activity.activity_date}" class="form-control">
                                            <p id="activity_date_req" class="required">กรุณาเลือกวันที่</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group ">
                                            <label>สถานที่</label>
                                            <input type="text" id="place" name="place" value="{$activity.place}" class="form-control">
                                            <p id="place_req" class="required">กรุณากรอกสถานที่</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group label-small">
                                            <label>เวลา</label>
                                            <div class="form-inline">
                                                <input type="time" id="start_on" name="start_on" value="{$activity.start_on|substr:0:5}">
                                                -
                                                <input type="time" id="end_on" name="end_on" value="{$activity.end_on|substr:0:5}">
                                            </div>
                                            <p id="time_req" class="required">กรุณาระบุเวลา</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea rows="8" id="description" name="description" class="form-control is-full">{$activity.description}</textarea>
                                            <p id="description_req" class="required">กรุณากรอกข้อมูล</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    {if $activity.image_1 != '' || $activity.image_2 != '' || $activity.image_3 != ''}
                                        <div class="col-sm-7">
                                            <div class="form-group has-label-upload">
                                                {if $activity.image_1 != ''}
                                                    <img src="{$activity.image_1}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                                {if $activity.image_2 != ''}
                                                    <img src="{$activity.image_2}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                                {if $activity.image_3 != ''}
                                                    <img src="{$activity.image_3}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                            </div>
                                        </div>
                                    {/if}
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>อัพโหลดไฟล์ภาพ <img src="{$image_url}theme/default/assets/images/icon-upload.png"></label>
                                            <input type="file" id="image_1" name="image_1" accept="image/gif, image/jpeg, image/png"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>&nbsp;</label>
                                            <input type="file" id="image_2" name="image_2" accept="image/gif, image/jpeg, image/png"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>&nbsp;</label>
                                            <input type="file" id="image_3" name="image_3" accept="image/gif, image/jpeg, image/png"  class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>ไฟล์วิดีโอ Embed Youtube</label>
                                            <textarea id="vdo" name="vdo" class="form-control">{$activity.vdo}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="activity-control">
                                <div class="form-group people">
                                    <img src="{$image_url}theme/default/assets/images/add-bg1.png">
                                    <p>กรุณาระบุข้อมูลการจัดกิจกรรม</p>
                                    <p>
                                        <b>จำนวนผู้มาเข้าร่วมประชุม</b>
                                        <input type="text" id="attendant" name="attendant" value="{$activity.attendant}" class="form-control">
                                        <b>คน</b>
                                    </p>
                                    <p id="attendant_req" class="required">กรุณากรอกจำนวนผู้มาเข้าร่วมประชุม</p>
                                </div>
                                <div class="row">
                                    <img src="{$image_url}theme/default/assets/images/add-bg2.png" class="bg-2">
                                    <div class="col col-sm-6">
                                        <div class="input-box">
                                            <div class="head">
                                                <span>ของแจกผู้ร่วมงานที่ลดได้ในการจัดกิจกรรม</span>
                                                <img src="{$image_url}theme/default/assets/images/icon-add2.png">
                                            </div>
                                            <div class="body">
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add11.png">
                                                        <span>เอกสารแจก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="premium_1" name="premium_1" value="{if $activity.premium_1 > 0}{$activity.premium_1}{/if}" class="form-control">
                                                        <span>แผ่น</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add12.png">
                                                        <span>แฟ้มพลาสติก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="premium_2" name="premium_2" value="{if $activity.premium_2 > 0}{$activity.premium_2}{/if}" class="form-control">
                                                        <span>แฟ้ม</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add13.png">
                                                        <span>ถุงพลาสติก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="premium_3" name="premium_3" value="{if $activity.premium_3 > 0}{$activity.premium_3}{/if}" class="form-control">
                                                        <span>ใบ</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add14.png">
                                                        <span>แก้วพลาสติก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="premium_4" name="premium_4" value="{if $activity.premium_4 > 0}{$activity.premium_4}{/if}" class="form-control">
                                                        <span>ใบ</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add15.png">
                                                        <span>น้ำดื่มขวด PET</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="premium_5" name="premium_5" value="{if $activity.premium_5 > 0}{$activity.premium_5}{/if}" class="form-control">
                                                        <span>ขวด</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-6">
                                        <div class="input-box">
                                            <div class="head">
                                                <span>การเลี้ยงอาหารและเครื่องดื่ม</span>
                                                <img src="{$image_url}theme/default/assets/images/icon-add2.png">
                                            </div>
                                            <div class="body is-green">
                                                <div class="form-group">
                                                    <div class="mask">ไม่ใช้โฟม</div>
                                                    <div class="radio">
                                                        <input type="checkbox" id="food_no_foam_1" name="food_no_foam_1" value="Y"{if $activity.food_no_foam_1 == 'Y'} checked="checked"{/if}> 
                                                        <label for="food_no_foam_1">อาหารเช้า</label>
                                                    </div>
                                                    <div class="unit">
                                                        <input type="text" id="food_pax_1" name="food_pax_1" value="{if $activity.food_pax_1 > 0}{$activity.food_pax_1}{/if}" class="form-control">
                                                        <span>PAX</span>
                                                        <p id="food_pax_1_req" class="required">กรุณากรอกข้อมูล</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <input type="checkbox" id="food_no_foam_2" name="food_no_foam_2" value="Y"{if $activity.food_no_foam_2 == 'Y'} checked="checked"{/if}> 
                                                        <label for="food_no_foam_2">เบรกเช้า</label>
                                                    </div>
                                                    <div class="unit">
                                                        <input type="text" id="food_pax_2" name="food_pax_2" value="{if $activity.food_pax_2 > 0}{$activity.food_pax_2}{/if}" class="form-control">
                                                        <span>PAX</span>
                                                        <p id="food_pax_2_req" class="required">กรุณากรอกข้อมูล</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <input type="checkbox" id="food_no_foam_3" name="food_no_foam_3" value="Y"{if $activity.food_no_foam_3 == 'Y'} checked="checked"{/if}> 
                                                        <label for="food_no_foam_3">อาหารกลางวัน</label>
                                                    </div>
                                                    <div class="unit">
                                                        <input type="text" id="food_pax_3" name="food_pax_3" value="{if $activity.food_pax_3 > 0}{$activity.food_pax_3}{/if}" class="form-control">
                                                        <span>PAX</span>
                                                        <p id="food_pax_3_req" class="required">กรุณากรอกข้อมูล</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <input type="checkbox" id="food_no_foam_4" name="food_no_foam_4" value="Y"{if $activity.food_no_foam_4 == 'Y'} checked="checked"{/if}> 
                                                        <label for="food_no_foam_4">เบรกบ่าย</label>
                                                    </div>
                                                    <div class="unit">
                                                        <input type="text" id="food_pax_4" name="food_pax_4" value="{if $activity.food_pax_4 > 0}{$activity.food_pax_4}{/if}" class="form-control">
                                                        <span>PAX</span>
                                                        <p id="food_pax_4_req" class="required">กรุณากรอกข้อมูล</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <input type="checkbox" id="food_no_foam_5" name="food_no_foam_5" value="Y"{if $activity.food_no_foam_5 == 'Y'} checked="checked"{/if}> 
                                                        <label for="food_no_foam_5">อาหารเย็น</label>
                                                    </div>
                                                    <div class="unit">
                                                        <input type="text" id="food_pax_5" name="food_pax_5" value="{if $activity.food_pax_5 > 0}{$activity.food_pax_5}{/if}" class="form-control">
                                                        <span>PAX</span>
                                                        <p id="food_pax_5_req" class="required">กรุณากรอกข้อมูล</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <img src="{$image_url}theme/default/assets/images/add-bg3.png" class="bg-3">
                                    <div class="col col-sm-6">
                                        <div class="input-box">
                                            <div class="head">
                                                <span>การจัดการของเสียหลังการจัดงาน</span>
                                                <img src="{$image_url}theme/default/assets/images/icon-add3.png">
                                            </div>
                                            <div class="body is-green">
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add31.png">
                                                        <span>เศษอาหาร</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_1" name="waste_1" value="{if $activity.waste_1 > 0}{$activity.waste_1}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add32.png">
                                                        <span>พลาสติก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_2" name="waste_2" value="{if $activity.waste_2 > 0}{$activity.waste_2}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add33.png">
                                                        <span>กระดาษ/กระดาษกล่อง</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_3" name="waste_3" value="{if $activity.waste_3 > 0}{$activity.waste_3}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add34.png">
                                                        <span>อลูมิเนียม</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_4" name="waste_4" value="{if $activity.waste_4 > 0}{$activity.waste_4}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add35.png">
                                                        <span>เหล็ก</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_5" name="waste_5" value="{if $activity.waste_5 > 0}{$activity.waste_5}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>
                                                        <img src="{$image_url}theme/default/assets/images/icon-add36.png">
                                                        <span>แก้ว</span>
                                                    </label>
                                                    <div class="unit">
                                                        <input type="text" id="waste_6" name="waste_6" value="{if $activity.waste_6 > 0}{$activity.waste_6}{/if}" class="form-control">
                                                        <span>กก.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-6">
                                        <div class="input-box">
                                            <div class="head">
                                                <span>การลดการใช้อุปกรณ์ตกแต่ง</span>
                                                <img src="{$image_url}theme/default/assets/images/icon-add4.png">
                                            </div>
                                            <div class="body">
                                                <div class="form-group is-half">
                                                    <img src="{$image_url}theme/default/assets/images/icon-add41.png">
                                                    <p class="txt">ไม้อัด<br/>ขนาดมาตรฐาน 4 x 8 ฟุต</p>
                                                    <div class="unit">
                                                        <div class="col">
                                                            <p>4 มม.</p>
                                                            <input type="text" id="wood_1" name="wood_1" value="{if $activity.wood_1 > 0}{$activity.wood_1}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>6 มม.</p>
                                                            <input type="text" id="wood_2" name="wood_2" value="{if $activity.wood_2 > 0}{$activity.wood_2}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>8 มม.</p>
                                                            <input type="text" id="wood_3" name="wood_3" value="{if $activity.wood_3 > 0}{$activity.wood_3}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>10 มม.</p>
                                                            <input type="text" id="wood_4" name="wood_4" value="{if $activity.wood_4 > 0}{$activity.wood_4}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>15 มม.</p>
                                                            <input type="text" id="wood_5" name="wood_5" value="{if $activity.wood_5 > 0}{$activity.wood_5}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <span>แผ่น</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group is-half">
                                                    <img src="{$image_url}theme/default/assets/images/icon-add42.png">
                                                    <p class="txt">ฟิวเจอร์บอร์ด PP Board<br/>ขนาดมาตรฐาน 130X145 cm.</p>
                                                    <div class="unit">
                                                        <div class="col">
                                                            <p>2 มม.</p>
                                                            <input type="text" id="pp_1" name="pp_1" value="{if $activity.pp_1 > 0}{$activity.pp_1}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>4 มม.</p>
                                                            <input type="text" id="pp_2" name="pp_2" value="{if $activity.pp_2 > 0}{$activity.pp_2}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>6 มม.</p>
                                                            <input type="text" id="pp_3" name="pp_3" value="{if $activity.pp_3 > 0}{$activity.pp_3}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>8 มม.</p>
                                                            <input type="text" id="pp_4" name="pp_4" value="{if $activity.pp_4 > 0}{$activity.pp_4}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <p>10 มม.</p>
                                                            <input type="text" id="pp_5" name="pp_5" value="{if $activity.pp_5 > 0}{$activity.pp_5}{/if}" class="form-control">
                                                        </div>
                                                        <div class="col">
                                                            <span>แผ่น</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {if $member.is_set_staff == 'Y'}
                                    <img src="{$image_url}theme/default/assets/images/add-bg5.png" class="bg-5">
                                    <div class="row floor">
                                        <div class="col col-sm-4 no-padding">
                                            <div class="input-box no-margin-top">
                                                <div class="head text-center">
                                                    <span>ตึก A</span>
                                                    <img src="{$image_url}theme/default/assets/images/icon-add5.png">
                                                </div>
                                                <div class="body">
                                                    <div class="form-group is-half is-full">
                                                        <p>ชั้น 5  ห้อง</p>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>502</p>
                                                                <input type="text" id="building_a_1" name="building_a_1" value="{if $activity.building_a_1 > 0}{$activity.building_a_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>503</p>
                                                                <input type="text" id="building_a_2" name="building_a_2" value="{if $activity.building_a_2 > 0}{$activity.building_a_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>504</p>
                                                                <input type="text" id="building_a_3" name="building_a_3" value="{if $activity.building_a_3 > 0}{$activity.building_a_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>505</p>
                                                                <input type="text" id="building_a_4" name="building_a_4" value="{if $activity.building_a_4 > 0}{$activity.building_a_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <span>ชม.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-sm-4 no-padding">
                                            <div class="input-box no-margin-top">
                                                <div class="head text-center">
                                                    <span>ตึก B</span>
                                                    <img src="{$image_url}theme/default/assets/images/icon-add5.png">
                                                </div>
                                                <div class="body">
                                                    <div class="form-group is-half is-full">
                                                        <p>ชั้น 3  ห้อง</p>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p class="w100">สุกรีย์</p>
                                                                <input type="text" id="building_b_1" name="building_b_1" value="{if $activity.building_b_1 > 0}{$activity.building_b_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <span>ชม.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group is-half is-full">
                                                        <p>ชั้น 6  ห้อง</p>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>601</p>
                                                                <input type="text" id="building_b_2" name="building_b_2" value="{if $activity.building_b_2 > 0}{$activity.building_b_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>602</p>
                                                                <input type="text" id="building_b_3" name="building_b_3" value="{if $activity.building_b_3 > 0}{$activity.building_b_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>603</p>
                                                                <input type="text" id="building_b_4" name="building_b_4" value="{if $activity.building_b_4 > 0}{$activity.building_b_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <span>ชม.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group is-half is-full">
                                                        <p>ชั้น 7 ห้อง</p>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>701</p>
                                                                <input type="text" id="building_b_5" name="building_b_5" value="{if $activity.building_b_5 > 0}{$activity.building_b_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>702</p>
                                                                <input type="text" id="building_b_6" name="building_b_6" value="{if $activity.building_b_6 > 0}{$activity.building_b_6}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>703</p>
                                                                <input type="text" id="building_b_7" name="building_b_7" value="{if $activity.building_b_7 > 0}{$activity.building_b_7}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>704</p>
                                                                <input type="text" id="building_b_8" name="building_b_8" value="{if $activity.building_b_8 > 0}{$activity.building_b_8}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <span>ชม.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-sm-4 no-padding">
                                            <div class="input-box no-margin-top">
                                                <div class="head text-center">
                                                    <span>ตึก C</span>
                                                    <img src="{$image_url}theme/default/assets/images/icon-add5.png">
                                                </div>
                                                <div class="body">
                                                    <div class="form-group is-half is-full">
                                                        <p>ชั้น 7  ห้อง</p>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p class="w100">ศ.สังเวียน</p>
                                                                <input type="text" id="building_c_1" name="building_c_1" value="{if $activity.building_c_1 > 0}{$activity.building_c_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <span>ชม.</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="other">กรณีจัดประชุมในสถานที่อื่นๆ </p>
                                {else}
                                    <img src="{$image_url}theme/default/assets/images/add-bg4.png" class="bg-4">
                                {/if}
                                <div class="row">
                                    <div class="col col-sm-12">
                                        <div class="input-box no-margin-top">
                                            <div class="head text-center">
                                                <span>อุปกรณ์แสงสว่างในห้องประชุม</span>
                                                <img src="{$image_url}theme/default/assets/images/icon-add5.png">
                                            </div>
                                            <div class="body">
                                                <div class="form-group no-border">
                                                    <div class="table-head">
                                                        <img src="{$image_url}theme/default/assets/images/icon-add51.png">
                                                        <span>อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED Bulb</span>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 110px"></th>
                                                                    <th>3</th>
                                                                    <th>4</th>
                                                                    <th>5</th>
                                                                    <th>6</th>
                                                                    <th>7</th>
                                                                    <th>9</th>
                                                                    <th>10</th>
                                                                    <th>12</th>
                                                                    <th>13</th>
                                                                    <th>15</th>
                                                                    <th>18</th>
                                                                    <th style="width: 50px">วัตต์</th>
                                                                </tr>                      
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 110px">จำนวนหลอดที่ใช้</td>
                                                                    <td><input type="text" id="led_bulb_watt_1" name="led_bulb_watt_1" value="{if $activity.led_bulb_watt_1 > 0}{$activity.led_bulb_watt_1}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_2" name="led_bulb_watt_2" value="{if $activity.led_bulb_watt_2 > 0}{$activity.led_bulb_watt_2}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_3" name="led_bulb_watt_3" value="{if $activity.led_bulb_watt_3 > 0}{$activity.led_bulb_watt_3}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_4" name="led_bulb_watt_4" value="{if $activity.led_bulb_watt_4 > 0}{$activity.led_bulb_watt_4}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_5" name="led_bulb_watt_5" value="{if $activity.led_bulb_watt_5 > 0}{$activity.led_bulb_watt_5}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_6" name="led_bulb_watt_6" value="{if $activity.led_bulb_watt_6 > 0}{$activity.led_bulb_watt_6}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_7" name="led_bulb_watt_7" value="{if $activity.led_bulb_watt_7 > 0}{$activity.led_bulb_watt_7}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_8" name="led_bulb_watt_8" value="{if $activity.led_bulb_watt_8 > 0}{$activity.led_bulb_watt_8}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_9" name="led_bulb_watt_9" value="{if $activity.led_bulb_watt_9 > 0}{$activity.led_bulb_watt_9}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_10" name="led_bulb_watt_10" value="{if $activity.led_bulb_watt_10 > 0}{$activity.led_bulb_watt_10}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_watt_11" name="led_bulb_watt_11" value="{if $activity.led_bulb_watt_11 > 0}{$activity.led_bulb_watt_11}{/if}" class="form-control"></td>
                                                                    <td style="width: 50px">หลอด</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 110px">ชั่วโมงการใช้งาน</td>
                                                                    <td><input type="text" id="led_bulb_hour_1" name="led_bulb_hour_1" value="{if $activity.led_bulb_hour_1 > 0}{$activity.led_bulb_hour_1}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_2" name="led_bulb_hour_2" value="{if $activity.led_bulb_hour_2 > 0}{$activity.led_bulb_hour_2}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_3" name="led_bulb_hour_3" value="{if $activity.led_bulb_hour_3 > 0}{$activity.led_bulb_hour_3}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_4" name="led_bulb_hour_4" value="{if $activity.led_bulb_hour_4 > 0}{$activity.led_bulb_hour_4}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_5" name="led_bulb_hour_5" value="{if $activity.led_bulb_hour_5 > 0}{$activity.led_bulb_hour_5}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_6" name="led_bulb_hour_6" value="{if $activity.led_bulb_hour_6 > 0}{$activity.led_bulb_hour_6}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_7" name="led_bulb_hour_7" value="{if $activity.led_bulb_hour_7 > 0}{$activity.led_bulb_hour_7}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_8" name="led_bulb_hour_8" value="{if $activity.led_bulb_hour_8 > 0}{$activity.led_bulb_hour_8}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_9" name="led_bulb_hour_9" value="{if $activity.led_bulb_hour_9 > 0}{$activity.led_bulb_hour_9}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_10" name="led_bulb_hour_10" value="{if $activity.led_bulb_hour_10 > 0}{$activity.led_bulb_hour_10}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_bulb_hour_11" name="led_bulb_hour_11" value="{if $activity.led_bulb_hour_11 > 0}{$activity.led_bulb_hour_11}{/if}" class="form-control"></td>
                                                                    <td style="width: 50px">ชั่วโมง</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="form-group no-border">
                                                    <div class="table-head">
                                                        <img src="{$image_url}theme/default/assets/images/icon-add52.png">
                                                        <span>อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED TUBE </span>
                                                    </div>
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 110px"></th>
                                                                    <th>5</th>
                                                                    <th>6</th>
                                                                    <th>8</th>
                                                                    <th>10</th>
                                                                    <th>14</th>
                                                                    <th>18</th>
                                                                    <th>20</th>
                                                                    <th>22</th>
                                                                    <th>24</th>
                                                                    <th>25</th>
                                                                    <th>36</th>
                                                                    <th style="width: 50px">วัตต์</th>
                                                                </tr>                      
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 110px">จำนวนหลอดที่ใช้</td>
                                                                    <td><input type="text" id="led_tube_watt_1" name="led_tube_watt_1" value="{if $activity.led_tube_watt_1 > 0}{$activity.led_tube_watt_1}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_2" name="led_tube_watt_2" value="{if $activity.led_tube_watt_2 > 0}{$activity.led_tube_watt_2}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_3" name="led_tube_watt_3" value="{if $activity.led_tube_watt_3 > 0}{$activity.led_tube_watt_3}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_4" name="led_tube_watt_4" value="{if $activity.led_tube_watt_4 > 0}{$activity.led_tube_watt_4}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_5" name="led_tube_watt_5" value="{if $activity.led_tube_watt_5 > 0}{$activity.led_tube_watt_5}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_6" name="led_tube_watt_6" value="{if $activity.led_tube_watt_6 > 0}{$activity.led_tube_watt_6}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_7" name="led_tube_watt_7" value="{if $activity.led_tube_watt_7 > 0}{$activity.led_tube_watt_7}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_8" name="led_tube_watt_8" value="{if $activity.led_tube_watt_8 > 0}{$activity.led_tube_watt_8}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_9" name="led_tube_watt_9" value="{if $activity.led_tube_watt_9 > 0}{$activity.led_tube_watt_9}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_10" name="led_tube_watt_10" value="{if $activity.led_tube_watt_10 > 0}{$activity.led_tube_watt_10}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_watt_11" name="led_tube_watt_11" value="{if $activity.led_tube_watt_11 > 0}{$activity.led_tube_watt_11}{/if}" class="form-control"></td>
                                                                    <td style="width: 50px">หลอด</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 110px">ชั่วโมงการใช้งาน</td>
                                                                    <td><input type="text" id="led_tube_hour_1" name="led_tube_hour_1" value="{if $activity.led_tube_hour_1 > 0}{$activity.led_tube_hour_1}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_2" name="led_tube_hour_2" value="{if $activity.led_tube_hour_2 > 0}{$activity.led_tube_hour_2}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_3" name="led_tube_hour_3" value="{if $activity.led_tube_hour_3 > 0}{$activity.led_tube_hour_3}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_4" name="led_tube_hour_4" value="{if $activity.led_tube_hour_4 > 0}{$activity.led_tube_hour_4}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_5" name="led_tube_hour_5" value="{if $activity.led_tube_hour_5 > 0}{$activity.led_tube_hour_5}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_6" name="led_tube_hour_6" value="{if $activity.led_tube_hour_6 > 0}{$activity.led_tube_hour_6}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_7" name="led_tube_hour_7" value="{if $activity.led_tube_hour_7 > 0}{$activity.led_tube_hour_7}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_8" name="led_tube_hour_8" value="{if $activity.led_tube_hour_8 > 0}{$activity.led_tube_hour_8}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_9" name="led_tube_hour_9" value="{if $activity.led_tube_hour_9 > 0}{$activity.led_tube_hour_9}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_10" name="led_tube_hour_10" value="{if $activity.led_tube_hour_10 > 0}{$activity.led_tube_hour_10}{/if}" class="form-control"></td>
                                                                    <td><input type="text" id="led_tube_hour_11" name="led_tube_hour_11" value="{if $activity.led_tube_hour_11 > 0}{$activity.led_tube_hour_11}{/if}" class="form-control"></td>
                                                                    <td style="width: 50px">ชั่วโมง</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <br/><br/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="btn-box text-right">
                                    <button type="submit" name="save" value="save" class="btn btn-yellow">Save</button>
                                    <button type="button" class="btn btn-grey" onclick="window.history.back();">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}