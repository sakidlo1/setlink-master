<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{block name=meta_title}{$page} - {$site_name}{/block}</title>
        <link rel="apple-touch-icon" sizes="180x180" href="{$image_url}theme/default/climatecare/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="{$image_url}theme/default/climatecare/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="{$image_url}theme/default/climatecare/favicon-16x16.png" />
        <link rel="manifest" href="{$image_url}theme/default/climatecare/site.webmanifest" />
        <link rel="mask-icon" href="{$image_url}theme/default/climatecare/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
        {block name=css}{/block}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/header.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/footer.css">
        {* //CSS Carethebear// *}
        {* <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css"> *}
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/fontawesome.min.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap-timepicker.min.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/aos.css">
        {* <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/fonts.css"> *}
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/main.css?ver={$smarty.now}">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/responsive.css?ver={$smarty.now}">

        <script>
            var base_url = '{$base_url}';
            var master_base_url = '{$master_base_url}';
            var image_url = '{$image_url}';
            var is_login = {if $member.id > 0}true{else}false{/if};

            const oauth2_authen_param = `{$oauth2_authen_param}{$callback_url}`;
        </script>

        <script src="{$image_url}theme/default/climatecare/public/js/utility.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/common.js?v=1.1"></script>
        <script>
            function menuModal() {
                var html = '';
                html += '<div class="popup_cate_menu">';
                    html += '<div class="dim show"></div>';
                    html += '<div class="wrap_box">';
                        html += '<div class="box_cate_menu">';
                            html += '<a href="{$master_base_url}carethebear" class="cate_menu bear flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/climatecare/public/images/header/login_bear.svg" alt="bear" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Bear';
                                html += '</div>';
                            html += '</a>';
                            html += '<a href="{$master_base_url}care-the-whale" class="cate_menu whale flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/climatecare/public/images/header/login_whale.svg" alt="whale" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Whale';
                                html += '</div>';
                            html += '</a>';
                            html += '<a href="{$master_base_url}care-the-wild" class="cate_menu elephant flex v-center">';
                                html += '<div class="wrap_character">';
                                    html += '<img src="{$image_url}theme/default/climatecare/public/images/header/menu_elephant.svg" alt="elephant" />';
                                html += '</div>';
                                html += '<div class="text f_med">';
                                    html += 'Care the Wild';
                                html += '</div>';
                            html += '</a>';

                            {if $member.id > 0}
                                html += '<div class="menu login_menu flex show-940">';
                                    html += '<div class="icon-user">';
                                        html += '<a href="{$master_base_url}dashboard"><span class="user icon-profile"></span></a>';
                                    html += '</div>';
                                    html += '<div class="flex column">';
                                        html += '<span class="txt-log f_med"><a href="{$master_base_url}dashboard" style="color: #000;">{$member.name} {$member.surname}</a><br> <span class="txt-sub f_reg"><a href="{$base_url}logout">แก้ไขข้อมูลสมาชิก</a></span><span class="f_bold" style="color: #FFA400;font-size: 20px;"> | </span><span class="txt-sub f_reg"><a href="{$base_url}logout">ออกจากระบบ</a></span></span>';
                                    html += '</div>';
                                html += '</div>';
                            {else}
                                html += '<div class="menu login_menu flex show-940">';
                                    html += '<span class="icon icon-profile"></span>';
                                    html += '<span class="txt f_med">เข้าสู่ระบบ</span>';
                                html += '</div>';
                            {/if}

                        html += '</div>';
                    html += '</div>';
                html += '</div>';
                return html;
            }
        </script>
        {block name=js}{/block}
        {literal}
        <script>
            function loadCSS(e,n,o,t){"use strict";if(e.trim().length<=0){return;}var d=window.document.createElement("link"),i=n||window.document.getElementsByTagName("script")[0],s=window.document.styleSheets;return d.rel="stylesheet",d.href=e,d.media="only x",t&&(d.onload=t),i.parentNode.insertBefore(d,i),d.onloadcssdefined=function(n){for(var o,t=0;t<s.length;t++)s[t].href&&s[t].href.indexOf(e)>-1&&(o=!0);o?n():setTimeout(function(){d.onloadcssdefined(n)})},d.onloadcssdefined(function(){d.media=o||"all"}),d};
            function loadJS(e, a) { if (e.length <= 0) { return; } var t = document.getElementsByTagName("head")[0], n = document.createElement("script"); n.type = "text/javascript", n.src = e, n.async = !0, n.onreadystatechange = a, n.onload = a, t.appendChild(n) }
        </script>
        {/literal}
    </head>
    <body>
        <header class="header flex h-justify v-center">
            <div class="constraint_header flex h-justify v-center">
                <a href="{$master_base_url}" class="logo flex center">
                    <img src="{$image_url}theme/default/climatecare/public/images/header/logo.svg" alt="Set Climate logo" />
                </a>
                <div class="header_menu flex h-justify v-center">
                    {if $member.id > 0}
                        <div class="menu flex center hide-940" data-btn="login">
                            <div class="icon-user">
                                <a href="{$master_base_url}dashboard"><span class="user icon-profile"></span></a>
                            </div>
                            <div class="flex column">
                                <span class="txt f_med"><a href="{$master_base_url}dashboard" style="color: #000;">{$member.name} {$member.surname}</a><br> 
                                <span class="txt-sub f_reg"><a href="{$master_base_url}member/member_profile">แก้ไขข้อมูลสมาชิก</a></span>
                                <span class="line f_bold"> | </span>
                                <span class="txt-sub f_reg"><a href="{$master_base_url}logout">ออกจากระบบ</a></span>
                                </span>
                            </div>
                            {* <div class="icon-user-settings">
                                <a href="{$base_url}member/register"><img src="{$image_url}theme/default/public/images/header/user-settings.svg"/></a>
                            </div> *}
                        </div>
                    {else}
                        <div class="menu flex center hide-940" data-btn="login">
                            <span class="icon icon-profile"></span>
                            <span class="txt f_med">เข้าสู่ระบบ</span>
                        </div>
                    {/if}
                    <div class="menu flex center" data-btn="menu">
                        <span class="icon menu icon-menu"></span>
                        <span class="txt f_med">เมนู</span>
                    </div>
                    <div class="menu flex center" data-btn="close">
                        <span class="icon close icon-cross"></span>
                    </div>
                </div>
            </div>
        </header>
        <div class="menu-bear">
            <div class="wrap-menu">
                <a {if $page == 'home'} {/if}><a class="menu-link f_med" href="{$base_url}">Care the bear</a></a>
                <a {if $page == 'about'} {/if}><a class="menu-link f_med" href="{$base_url}about">What’s Care the Bear</a></a>
                <a {if $page == 'activity'} {/if}><a class="menu-link f_med" href="{$base_url}activity">Activities & Events</a></a>
                <a {if $page == 'project'} {/if}><a class="menu-link f_med" href="{$base_url}project">Climate Action Project</a></a>
                <a {if $page == 'article' || $page == 'tips'} {/if}><a class="menu-link f_med" href="{$base_url}article/all">Articles</a></a>
                <a {if $page == 'news'} {/if}><a class="menu-link f_med" href="{$base_url}news">News</a></a>
            </div>
        </div>

        {* //Wrap CARETHEBEAR// *}
        <div id="wrapper">
            <section class="cover">
                <div class="banner owl-carousel">
                    {foreach $banner as $banner_item}
                        <div class="item">
                            {if $banner_item.url != ''}
                                <a href="{$banner_item.url}" target="_blank" title="{$banner_item.name}">
                                    <img src="{$banner_item.image}" alt="{$banner_item.name}">
                                </a>
                            {else}
                                <img src="{$banner_item.image}" alt="{$banner_item.name}">
                            {/if}
                        </div>
                    {/foreach}
                </div>
            </section>
            {block name=body}{/block}
        </div>
        {* //JSCARETHEBEAR// *}
<div id="overlay"></div>
<script src="{$image_url}theme/default/assets/js/jquery.min.js"></script>
<script src="{$image_url}theme/default/assets/js/bootstrap.min.js"></script>
<script src="{$image_url}theme/default/assets/js/owl.carousel.min.js"></script>
<script src="{$image_url}theme/default/assets/js/bootstrap-datepicker.min.js"></script>
<script src="{$image_url}theme/default/assets/js/bootstrap-timepicker.min.js"></script>
<script src="{$image_url}theme/default/assets/js/aos.js"></script>
<script src="{$image_url}theme/default/assets/js/main.js?ver={$smarty.now}"></script>
<script>
    function setCookie(name,value,days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }
    function cookieConsent() {
      if (!getCookie('cookies-consent')) {
        $('.cookies').show();
      }
    }

    window.onload = function() { cookieConsent(); };
</script>
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//climatecare.setsocialimpact.com/analytics/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
        {* {block name=body}{/block} *}
        {block name=script}{/block}
    </body>
</html>
