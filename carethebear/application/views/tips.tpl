{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section id="tips" data-aos="fade" class="no-padding">
        <div class="container">
            <br /><br />
            <h2 class="title no-margin-top">Tips <i class="icon-circle"></i></h2>
            <div class="row tip-box">
                {if $list|count > 0}
                    {foreach $list as $tips_item}
                        <div class="col col-sm-4">
                            <a href="{$base_url}tips/detail/{$tips_item.id}">
                                <div class="tips_item">
                                    <div class="img">
                                        <img src="{$tips_item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                    </div>
                                </div>
                            </a>
                        </div>
                    {/foreach}
                {else}
                    <div class="inner">
                        <br/><br/><br/>
                        <center>ไม่มีข้อมูล</center>
                        <br/><br/><br/>
                    </div>
                {/if}
            </div>
            <hr/>
            {include file='paging.tpl'}
        </div>
    </section>
    <br/>
    <br class="hidden-xs"/>
{/block}