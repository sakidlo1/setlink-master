{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<style>
		@page {
			size: A4;
			margin: 0.5cm;
		}

		@media print {
	        header, footer, .cover, .title, .member-actions, #btn-print {
	            visibility: hidden;
	            display: none;
	        }
	        .report {
	        	zoom: 0.7;
	        	margin-top: -50px;
	        }
        }
	</style>
    <section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    {include file='member_nav.tpl'}
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        <div class="report">
                        	<button id="btn-print" class="pull-right" onclick="window.print()" style="border: 0px;background: transparent;"><span class="glyphicon glyphicon-print"></span></button>
                            <div class="project">
                                <h3>{$activity.name}</h3>
                                <p>{$activity.activity_date|date_thai:"%e %B %Y"}</p>
                                <img src="{$activity.image_1}">
                            </div>
                            <div class="carbon">
                                <div class="head">
                                    <img src="{$member.logo}">
                                    <h3>Event นี้ช่วยลดปริมาณคาร์บอนฟุตพริ้นท์</h3>
                                    <img src="{$image_url}theme/default/assets/images/img-report2.png">
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="col col-sm-6">
                                            <div class="inner">
                                                <div class="box">ลดการใช้พลังงานจากการเดินทาง</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle1.png">
                                                    <span>{$activity.cf_care_1|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">งดการใช้โฟม</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle3.png">
                                                    <span>{$activity.cf_care_3|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">ลดการเกิดขยะจากการจัดงาน (เก็บข้อมูลหลังจบงาน)</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle5.png">
                                                    <span>{$activity.cf_care_5|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-sm-6">
                                            <div class="inner">
                                                <div class="box">ลดการใช้กระดาษและพลาสติก</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle2.png">
                                                    <span>{$activity.cf_care_2|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle4.png">
                                                    <span>{$activity.cf_care_4|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">เลือกวัสดุตกแต่งที่นำกลับมาใช้ใหม่ได้</div>
                                                <div class="box">
                                                    <img src="{$image_url}theme/default/assets/images/home-circle6.png">
                                                    <span>{$activity.cf_care_6|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="carbon-avg">
                                <p><span>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้</span> <big>{$activity.cf_care_total|number_format:2:".":","}</big> <span>kgCO<sub>2</sub>e</span></p>
                                <div class="clearfix"></div>
                                <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png"> เทียบเท่ากับการปลูกต้นไม้</span> <big class="bg-green">{($activity.cf_care_total/9)|number_format:0:".":","}</big> <span>ต้น</span></p>
                            </div>
                            {*
                            <div class="carbon-date">
                                <span>บริษัทคุณช่วยลดปริมาณคาร์บอนฟุตพริ้นท์ ระยะเวลา</span>
                                <div class="date">
                                    <input type="date" id="from" value="{$activity_date.min_date}" onchange="get_summary_cf();" class="form-control" >
                                    <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                </div>
                                <span>ถึง</span>
                                <div class="date">
                                    <input type="date" id="to" value="{$activity_date.max_date}" onchange="get_summary_cf();" class="form-control" >
                                    <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                </div>
                            </div>
                            <div class="carbon-avg has-bg">
                                <p><span>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้</span> <big id="total-cf"></big> <span>kgCO<sub>2</sub>e</span></p>
                                <div class="clearfix"></div>
                                <p><span>เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี ของต้นไม้</span> <big id="total-tree" class="bg-green"></big> <span>ต้น</span></p>
                            </div>
                            *}
                            <script>
                                function select_mode(value)
                                {
                                    $('.mode-action').hide();
                                    $('.mode-action[data-mode="'+value+'"]').show();
                                    get_summary_cf();
                                }
                            </script>
                            <div class="mode-filter carbon-date">
                                <h3>บริษัทคุณช่วยลดปริมาณคาร์บอนฟุตพริ้นท์</h3>
                                <div class="form-group no-margin">
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode1" id="mode1" checked onchange="select_mode(this.value)"> 
                                        <label for="mode1">ตั้งแต่เริ่ม</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode2" id="mode2" onchange="select_mode(this.value)"> 
                                        <label for="mode2">ช่วงเวลา</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode3" id="mode3" onchange="select_mode(this.value)"> 
                                        <label for="mode3">ช่วงเดือน</label>
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode1" style="display: block;">
                                    <span>ตั้งแต่เริ่ม จนถึง</span>
                                    <div class="date">
                                        <input type="date" id="end_date" name="end_date" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode2">
                                    <span>ระยะเวลา</span>
                                    <div class="date">
                                        <input type="date" id="from" name="from" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                    <span>ถึง</span>
                                    <div class="date">
                                        <input type="date" id="to" name="to" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode3">
                                    <span>เดือน / ปี</span>
                                    <div class="date">
                                        <input type="month" id="month_year" name="month_year" value="{$month}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                            </div>
                            <div class="mode-result">
                                <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดกิจกรรม</h3></p>
                                </div>                                
                                <div class="carbon-avg has-bg">
                                    <p><span class="two-row">ปริมาณคาร์บอนฟุตพริ้นที่ลดได้จากการจัดกิจกรรม</span> <big id="total-activity-cf"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์นเทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span> <big id="total-activity-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                               <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดโครงการ</h3></p>
                                </div>                                
                                <div class="carbon-avg has-bg">
                                    <p><span class="two-row">ปริมาณคาร์บอนฟุตพริ้นที่ลดได้จากการจัดโครงการ</span> <big id="total-project-cf" class="bg-whit"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span> <big id="total-project-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                                </div>
                                <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดกิจกรรมและโครงการ</h3></p>
                                    <div class="carbon-avg has-bg">
                                    <p><span>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้รวม</span> <big id="total-cf"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span><big id="total-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script>

    		
        {literal}
        function input_format(value, dec)
        {
            value = value.toString().replace(/\,/g,"");
            if(value != "")
            {
                if(dec == 0)
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                }
                else
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                }
            }
            else
            {
                return '';
            }
        }
        {/literal}

        function get_summary_cf()
        {
            $.get('{$base_url}{$page}/get_summary_cf/' + $('input[name="mode"]:checked').val() + '/' + $('#end_date').val() + '/' + $('#from').val() + '/' + $('#to').val() + '/' + $('#month_year').val(), function(data) {
                $('#total-cf').html(input_format(data.total_cf, 2));
                $('#total-tree').html(input_format(data.total_tree, 0));
                $('#total-activity-cf').html(input_format(data.total_activity_cf, 2));
                $('#total-activity-tree').html(input_format(data.total_activity_tree, 0));
                $('#total-project-cf').html(input_format(data.total_project_cf, 2));
                $('#total-project-tree').html(input_format(data.total_project_tree, 0));
            });
        }

        $(document).ready(function () {

            get_summary_cf();

        });
    </script>
{/block}