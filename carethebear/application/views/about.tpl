{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
	<link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
{/block}
{block name=js}
	<script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
    <script>
    const bear = [
        {foreach $company_bear as $company_bear_item}
            '{$company_bear_item.logo}',
        {/foreach}
    ];
    const dataOrganization = [
        ...bear
    ];
    </script>
    <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
    <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
    <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
{/block}
{block name=body}
	<div>
    <div id="banner_html"></div>
        <div data-aos="fade" class="banner-care-the-bear">
            <div class="content-banner-left">
                <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/banner/logo-bear.png" alt="" >
                    <span class="title-banner">โครงการ<br class="d-xl-block"> Care the Bear</span>
                    <span class="tiny-banner">ปัญหาสิ่งแวดล้อมเป็นปัญหาใหญ่ของโลกใบนี้ ซึ่งมีความรุนแรงและ
                        ส่งผลกระทบต่อประเทศไทยเป็นอย่างมาก เห็นได้จากความถี่และความรุนแรงของภัยธรรมชาติที่เกิดขึ้น
                        ซึ่งส่งผลกระทบต่อทั้งเศรษฐกิจ สังคมและสิ่งแวดล้อมอย่างหนัก จึงเป็นวาระเร่งด่วนของประเทศทั่วโลกที่ต้องเร่งแก้ปัญหา
                        <br class="d-xl-block">
                        <br class="d-lg-block">
                        จากข้อมูลของ Climate Watch Data ซึ่งได้รายงานเมื่อ 10 มีนาคม 2565 ที่ผ่านมานี้ ได้สรุปข้อมูลการปล่อยก๊าซ
                        เรือนกระจกในปี 2560 ว่าทุกประเทศทั่วโลกได้มีการปล่อยก๊าซเรือนกระจกมากถึง &nbsp; 48,939.71 &nbsp; ล้านตัน
                        คาร์บอนไดออกไซด์เทียบเท่า (MtCO<sub>2</sub>e) และประเทศไทยเป็นประเทศที่ปล่อยก๊าซเรือนกระจกมากถึง 432.22 
                        MtCO<sub>2</sub>e คิดเป็นอันดับที่ 20 ของโลกมีสัดส่วน 0.88% ของทั้งโลก และเป็นลำดับที่ 2 ของอาเซียนรองจากอินโดนีเซีย ซึ่ง
                        ประเทศที่ปล่อยก๊าซเรือนกระจกมากที่สุดในโลก ได้แก่ จีน (11,705.81 MtCO<sub>2</sub>e) รองลงมาเป็นสหรัฐอเมริกา
                        (5,794.35 MtCO<sub>2</sub>e)  ซึ่งทั้ง 2 ประเทศนี้มีสัดส่วนมากกว่า 35.76% ของทั้งโลก สำหรับในประเทศไทยภาคที่ปล่อย
                        ก๊าซเรือนกระจกมากที่สุดคือภาคพลังงาน ซึ่งมีสัดส่วนมากที่สุดถึง 61.11% รองลงมาคือภาคอุตสาหกรรม 16.67%
                        ภาคการเกษตร 15.96% และภาคป่าไม้และการใช้ประโยชน์ที่ดิน 3.31% และการจัดการของเสีย 2.95%และในปี 
                        พ.ศ. 2560 ประเทศไทย มีการปล่อยก๊าซเรือนกระจกเฉลี่ยต่อคนที่ 6.21 ตันคาร์บอนไดออกไซด์เทียบเท่า (tonCO<sub>2</sub>e) 
                        ซึ่งต่ำกว่าค่าเฉลี่ยมาตรฐานของประชากรโลก²เพียง 3.57% ซึ่งปล่อยอยู่ที่ 6.44 tonCO<sub>2</sub>e นับได้ว่าเป็นต้นเหตุ
                        ของสภาวะโลกร้อน ซึ่งการแก้ปัญหานี้จะเห็นผลอย่างเป็นรูปธรรมต้องมีการประเมินและวัดผลได้ ดังนั้นการทราบถึงปริมาณ
                        คาร์บอนฟุตพริ้นท์ที่ปล่อยออกมาจากกิจกรรมต่าง ๆ ของมนุษย์อย่างต่อเนื่อง ทั้งการใช้พลังงาน การเกษตร การพัฒนาและ
                        การขยายตัวของภาคอุตสาหกรรม การขนส่ง การตัดไม้ทำลายป่า หรือแม้กระทั่งการจัดงานอีเว้นท์ที่ต้องมีการใช้พลังงานไฟฟ้า
                        ทั้งส่วนของการจัดงานและการพักแรม การเดินทางของผู้เข้าร่วมงาน การใช้พลังงานในการปรุงอาหาร สิ่งเหลือทิ้งจากการจัดงาน 
                        ล้วนเป็นเหตุสำคัญของการเกิดภาวะโลกร้อน ซึ่งส่งผลกระทบต่อวิถีการดำรงชีวิตของมนุษย์ สิ่งมีชีวิตและนับวันปัญหาดังกล่าว
                        ก็ยิ่งทวีความรุนแรงมากขึ้น
                    </span>
                <div class="img-content-tiny">
                <img data-aos="fade-up" src="{$base_url}images/upload/editor/source/Articles/ctb.jpg" alt="">
                </div>
            <span class="tiny-banner" >
                ตลาดหลักทรัพย์แห่งประเทศไทย จึงริเริ่มโครงการ “Care the Bear” ภายใต้แนวคิด “Change the Climate Change” 
                ตั้งแต่ปี 2561 โดยร่วมกับพันธมิตร ทั้งภาคเอกชน ภาครัฐ และธุรกิจเพื่อสังคม ช่วยกันขับเคลื่อนการลดภาวะโลกร้อน
                ด้วยการลดการปล่อยก๊าซเรือนกระจกจากการจัดกิจกรรมขององค์กร ไม่ว่าจะเป็นกิจกรรม onsite หรือ online เช่น 
                การประชุม การอบรม การจัดงาน event  งานมอบรางวัล การประชุมผู้ถือหุ้น กิจกรรม CSR  เป็นต้น โดยใช้หลักการ 6 Cares  
                มุ่งเปลี่ยนแปลงในมิติของผู้บริโภคให้มีส่วนช่วยลดโลกร้อน ซึ่งสอดคล้องกับ Sustainable Development Goals (SDGs) 
                ข้อที่ 13 “Climate Action”
            </span>
            <div class="img-content-tiny">
            <img data-aos="fade-up" src="{$base_url}images/upload/editor/source/Articles/unnamed-11_1.jpg" alt="" >
            </div>
            <span class="tiny-banner">
                ปัจจุบันโครงการ Care the Bear มีองค์กรพันธมิตรกว่า 240 องค์กร (ข้อมูล ณ มีค.2565)&nbsp; และได้ร่วมกันลดการปล่อย
                ก๊าซเรือนกระจกแล้ว13,325 ตันคาร์บอนไดออกไซด์เทียบเท่า ซึ่งเทียบเท่าการดูดซับ CO<sub>2</sub> / ปีของต้นไม้จำนวน 
                1,480,657 ต้น (ข้อมูล ณ มีค.2565)&nbsp; โดยมีองค์กรภาคีหลัก คือ องค์การบริหารจัดการก๊าซเรือนกระจก (องค์การมหาชน) 
                และพันธมิตรอื่นร่วมขับเคลื่อน ไม่ว่าจะเป็นบริษัทจดทะเบียน บริษัทจำกัด หน่วยงานภาครัฐ สมาคม สถาบันการศึกษา โรงแรม 
                สถานที่จัดการประชุม และกิจการเพื่อสังคม
                <br class="d-xl-block">
                <br class="d-lg-block">
                โครงการ Care the Bear จะเป็นเครื่องมือที่สามารถช่วยสร้างจิตสำนึก ปรับเปลี่ยนพฤติกรรมของสมาชิกในองค์กร หรือชุมชน 
                เพื่อลดการปล่อยก๊าซเรือนกระจก โดยผลที่ได้จากการคำนวณการลดก๊าซเรือนกระจกนี้สามารถเปิดเผยในรายงานประจำปี 
                และรายงานความยั่งยืนขององค์กรได้ ซึ่งหลักการคำนวณการลดก๊าซเรือนกระจกเป็นไปตามมาตรฐานองค์การบริหารจัดการก๊าซเรือนกระจก 
                (องค์การมหาชน)
                <br class="d-xl-block">
                <strong>*&nbsp;</strong>องค์กรใดที่ยังไม่ได้เข้าร่วมโครงการจะไม่สามารถเข้าไปกรอกข้อมูลในสูตรการคำนวณการลดก๊าซเรือนกระจกได้
            </span>
            </div>
        </div>
        {* <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">โครงการ Care the Bear <i class="icon-circle"></i></h2>
                    <div class="about-detail">
                        <img class="img-bear" src="{$image_url}theme/default/assets/images/home-bare.png" style="float:right;margin-bottom: 20px;width: 300px; max-width: 100%;">
                        <br class="hidden-sm hidden-md hidden-md hidden-lg"/>
                        {$about_content.content}
                    </div>
                </div>
            </div>
        </div> *}
    </div>
    <section data-aos="fade-up" class="no-padding-top">
        <div class="container">
            <h2 class="title">ภาคธุรกิจที่เข้าร่วมโครงการ <i class="icon-circle"></i></h2>
            <div class="row">
                <div class="col-md-12">
                    <div class="home-grid-logo owl-carousel">
                        {foreach array_chunk($company, 50) as $items}
                            <div class="item">
                                {foreach $items as $item}
                                <div class="box">
                                    <img src="{$item.logo}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                </div>
                                {/foreach}
                            </div>
                        {/foreach}
                        {*
                        {foreach $company as $item}
                            <div class="box">
                                <img src="{$item.logo}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            </div>
                        {/foreach}
                        *}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section data-aos="fade" class="no-padding-top">
        <div class="container">
            <h2 class="title text-center"><b>รู้จัก โครงการ Care the Bear</b></h2>
            <div class="about-iframe">
                {$about_vdo.content|replace:'<p>':''|replace:'</p>':''}
            </div>
            <div class="about-iframe">
            <iframe width="560" height="315" 
            src="https://www.youtube.com/embed/zix9njWkfPc" 
            title="YouTube video player" 
            frameborder="0" a
            llow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowfullscreen></iframe>
            </div>
        </div>
    </section>
    {* <section data-aos="fade" class="no-padding-top">
        <div class="container">
            <h2 class="title">รู้จัก 6 Cares <i class="icon-circle"></i></h2>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="title font-dark text-center no-margin-top-mobile"><big>ทุกคนสามารถลดโลกร้อนด้วย</big></h2>
                    <div class="home-circle">
                        <img class="img-center" src="{$image_url}theme/default/assets/images/home-circle.png">
                        <a href="{$base_url}care#care-1" class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle1.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานเดินทางมาโดยรถสาธารณะ หรือ เดินทางมาด้วยกัน  หรือ Conference Call เพื่อลดการใช้พลังงานจากการเดินทาง">
                                <span>1</span>
                                <span>ประชาสัมพันธ์ให้ผู้ร่วมงานเดินทางโดยรถสาธารณะ</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-2"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle2.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานนำกระบอกน้ำส่วนตัว และถุงผ้ามาด้วย รวมถึงส่งเสริมการใช้เทคโนโลยีในการประชุม เพื่อลดการแจกเอกสารและกระดาษต่างๆ ในงาน">
                                <span>2</span>
                                <span>ลดการใช้กระดาษและพลาสติก</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-3"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle3.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="ส่งเสริมนโยบายในการใช้อุปกรณ์ตกแต่งที่นำกลับมาใช้ใหม่ได้ และงดการใช้โฟม เป็นบรรจุภัณฑ์และตกแต่งในงาน">
                                <span>3</span>
                                <span>งดการใช้โฟม</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-4"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle4.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้เลือกใช้อุปกรณ์ไฟฟ้าในการจัดงานที่มีฉลากประหยัดไฟ หรือมีประสิทธิภาพสูงสุดในการประหยัดพลังงาน">
                                <span>4</span>
                                <span>ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-5"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle6.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="วางแผน ออกแบบในการใช้วัสดุตกแต่งที่สามารนำมา Reuse / Recycle หรือ ส่งต่อให้ใช้ประโยชน์ได้สูงสุด">
                                <span>5</span>
                                <span>เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่ได้</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-6"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle5.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานตักอาหารแต่พอดี และทานให้หมด และส่งเสริมให้มีการคัดแยกขยะที่เกิดขึ้นจากการจัดงาน">
                                <span>6</span>
                                <span>ลดการเกิดขยะ ตักอาหารแต่พอดี และทานให้หมด</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section> *}
    <div class="main-sec-2">
	        <div class="wrap_care">
	            <div class="max_width_care flex center hide-xs">
	                <div data-aos="fade-up" class="left">
	                    <div class="cycle">
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/circle.svg" alt="ring" class="ring"/>
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" class="world"/>

	                        <div class="wrap_6">
	                            <a href="{$base_url}care#care-1" target="_blank" class="care">
                                    <img class="no_1" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                                <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-2" target="_blank" class="care">
	                                <img class="no_2" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                                <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-3" target="_blank" class="care">
	                                <img class="no_3" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                                <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-4" target="_blank" class="care">
	                                <img class="no_4" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                                <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-5" target="_blank" class="care">
	                                <img class="no_5" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                                <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-6" target="_blank" class="care">
	                                <img class="no_6" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                                <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <div data-aos="fade-up" class="right">
	                    <div class="care_txt">
	                        <div class="title f_bold">
	                            ทุกกิจกรรมสามารถใช้
	                            <p class="hilight f_bold hide-940">6 ปฏิบัติการ</p>
	                            <span class="hilight f_bold show-940"> 6 ปฏิบัติการ </span>
	                            ของ Care the Bear 
	                        </div>
	                        <div class="detail">
	                            มาออกแบบเพื่อประเมินวัดผลเป็นค่า
	                            การลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน 
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="show-xs">
	                <div class="flex center">
	                    <div class="left">
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" />
	                    </div>
	                    <div class="right">
	                        <div class="title f_bold">ทุกกิจกรรมสามารถใช้<p class="hilight f_bold hide-940">หลักการ 6 Cares</p><span class="hilight f_bold show-940"> หลักการ 6 Cares </span>ของ Care the Bear </div>
	                        <div class="detail">มาออกแบบเพื่อประเมินวัดผลเป็นค่าการลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน</div>
	                    </div>
	                </div>
	                <div class="wrap_6 flex h-justify wrap">
                        <a href="{$base_url}care#care-1" target="_blank" class="care">
                        <img class="no_1" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/01.svg" alt="1" />
                        <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
                        </a>
                        <a href="{$base_url}care#care-2" target="_blank" class="care">
                        <img class="no_2" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/02.svg" alt="2" />
                        <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
                        </a>
                        <a href="{$base_url}care#care-3" target="_blank" class="care">
                        <img class="no_3" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/03.svg" alt="3" />
                        <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
                        </a>
                        <a href="{$base_url}care#care-4" target="_blank" class="care">
                        <img class="no_4" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/04.svg" alt="4" />
                        <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
                        </a>
                        <a href="{$base_url}care#care-5" target="_blank" class="care">
                        <img class="no_5" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/05.svg" alt="5" />
                        <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
                        </a>
                        <a href="{$base_url}care#care-6" target="_blank" class="care">
                        <img class="no_6" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/06.svg" alt="6" />
                        <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
                        </a>
	                </div>
	            </div>
	        </div>
	    </div>
{/block}