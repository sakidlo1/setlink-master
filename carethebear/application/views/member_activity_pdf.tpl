<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>รายงานการลดคาร์บอนฟุตพริ้นท์</title>
        <style>
            @page {
                margin: 10mm;
                margin-header: 0mm;
                margin-footer: 0mm;
            }
        </style>
    </head>
    <body style="padding: 0px; margin: 0px; font-family: 'sarabun'; font-size: 18px;">
        <table cellpadding="0" cellspacing="5" width="90%" align="center">
            <tr>
                <td rowspan="7" style="width: 200px;">
                    <img style="height: 150px;" src="{$image_url}theme/default/assets/images/home-bare.png">
                </td>
                <td style="width: 80px;">&nbsp;</td>
                <td style="width: 50px;">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>บริษัท</td>
                <td>&nbsp;</td>
                <td colspan="2">{$member.company}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>ฝ่าย</td>
                <td>&nbsp;</td>
                <td colspan="2">{$member.department}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>ชื่อกิจกรรม</td>
                <td>&nbsp;</td>
                <td colspan="2">{$activity.name}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>สถานที่</td>
                <td>&nbsp;</td>
                <td colspan="2">{$activity.place}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>วันที่</td>
                <td>&nbsp;</td>
                <td colspan="2">{$activity.activity_date|date_thai:"%e %B %Y"}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6"><hr /></td>
            </tr>
            <tr>
                <td colspan="3">จำนวนผู้มาเข้าร่วมประชุม</td>
                <td style="text-align: right;">{$activity.attendant|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>คน</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การจัดการของแจกให้ผู้ร่วมงาน
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - เอกสารแจก
                </td>
                <td style="text-align: right;">{$activity.premium_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - แฟ้มพลาสติก
                </td>
                <td style="text-align: right;">{$activity.premium_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แฟ้ม</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - ถุงพลาสติก
                </td>
                <td style="text-align: right;">{$activity.premium_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>ใบ</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - แก้วพลาสติก
                </td>
                <td style="text-align: right;">{$activity.premium_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>ใบ</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - น้ำดื่มขวด PET
                </td>
                <td style="text-align: right;">{$activity.premium_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>ขวด</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การเลี้ยงอาหารและเครื่องดื่ม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - อาหารเช้า
                </td>
                <td style="text-align: right;">{$activity.food_pax_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แพค</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - เบรกเช้า
                </td>
                <td style="text-align: right;">{$activity.food_pax_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แพค</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - อาหารกลางวัน
                </td>
                <td style="text-align: right;">{$activity.food_pax_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แพค</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - เบรกบ่าย
                </td>
                <td style="text-align: right;">{$activity.food_pax_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แพค</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - อาหารเย็น
                </td>
                <td style="text-align: right;">{$activity.food_pax_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แพค</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การจัดการของเสียหลังจากการจัดงาน
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - เศษอาหาร
                </td>
                <td style="text-align: right;">{$activity.waste_1|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - พลาสติก
                </td>
                <td style="text-align: right;">{$activity.waste_2|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ/กระดาษกล่อง
                </td>
                <td style="text-align: right;">{$activity.waste_3|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - อลูมิเนียม
                </td>
                <td style="text-align: right;">{$activity.waste_4|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - เหล็ก
                </td>
                <td style="text-align: right;">{$activity.waste_5|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - แก้ว
                </td>
                <td style="text-align: right;">{$activity.waste_6|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>ก.ก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การลดการใช้อุปกรณ์ตกแต่ง
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - ไม้อัด ขนาดมาตรฐาน 4x8 ฟุต
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 4 มม.
                </td>
                <td style="text-align: right;">{$activity.wood_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 6 มม.
                </td>
                <td style="text-align: right;">{$activity.wood_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 8 มม.
                </td>
                <td style="text-align: right;">{$activity.wood_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 10 มม.
                </td>
                <td style="text-align: right;">{$activity.wood_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 15 มม.
                </td>
                <td style="text-align: right;">{$activity.wood_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - ฟิวเจอร์บอร์ด PP Board ขนาดมาตรฐาน 130x145 ซม.
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 2 มม.
                </td>
                <td style="text-align: right;">{$activity.pp_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 4 มม.
                </td>
                <td style="text-align: right;">{$activity.pp_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 6 มม.
                </td>
                <td style="text-align: right;">{$activity.pp_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 8 มม.
                </td>
                <td style="text-align: right;">{$activity.pp_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - 10 มม.
                </td>
                <td style="text-align: right;">{$activity.pp_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • อุปกรณ์แสงสว่างในห้องประชุม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            {if $member.is_set_staff == true}
                <tr>
                    <td colspan="3" style="padding-left: 40px;">
                        - ห้องประชุม
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ตึก A
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 5 ห้อง 502 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_a_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 5 ห้อง 503 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_a_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 5 ห้อง 504 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_a_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 5 ห้อง 505 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_a_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ตึก B
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 3 ห้อง สุกรีย์ จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 6 ห้อง 601 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 6 ห้อง 602 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 6 ห้อง 603 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 7 ห้อง 701 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_5|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 7 ห้อง 702 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_6|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 7 ห้อง 703 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_7|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 7 ห้อง 704 จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_b_8|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ตึก C
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ชั้น 7 ห้อง ศ.สังเวียน จำนวน
                    </td>
                    <td style="text-align: right;">{$activity.building_c_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 40px;">
                        - ห้องประชุมอื่นๆ
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED Bulb
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 3 วัตต์ จำนวน {$activity.led_bulb_watt_1|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 4 วัตต์ จำนวน {$activity.led_bulb_watt_2|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 5 วัตต์ จำนวน {$activity.led_bulb_watt_3|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 6 วัตต์ จำนวน {$activity.led_bulb_watt_4|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 7 วัตต์ จำนวน {$activity.led_bulb_watt_5|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_5|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 9 วัตต์ จำนวน {$activity.led_bulb_watt_6|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_6|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 10 วัตต์ จำนวน {$activity.led_bulb_watt_7|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_7|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 12 วัตต์ จำนวน {$activity.led_bulb_watt_8|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_8|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 13 วัตต์ จำนวน {$activity.led_bulb_watt_9|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_9|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 15 วัตต์ จำนวน {$activity.led_bulb_watt_10|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_10|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 18 วัตต์ จำนวน {$activity.led_bulb_watt_11|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_11|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED TUBE
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 5 วัตต์ จำนวน {$activity.led_tube_watt_1|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 6 วัตต์ จำนวน {$activity.led_tube_watt_2|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 8 วัตต์ จำนวน {$activity.led_tube_watt_3|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 10 วัตต์ จำนวน {$activity.led_tube_watt_4|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 14 วัตต์ จำนวน {$activity.led_tube_watt_5|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_5|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 18 วัตต์ จำนวน {$activity.led_tube_watt_6|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_6|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 20 วัตต์ จำนวน {$activity.led_tube_watt_7|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_7|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 22 วัตต์ จำนวน {$activity.led_tube_watt_8|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_8|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 24 วัตต์ จำนวน {$activity.led_tube_watt_9|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_9|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 25 วัตต์ จำนวน {$activity.led_tube_watt_10|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_10|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 80px;">
                        - ขนาด 36 วัตต์ จำนวน {$activity.led_tube_watt_11|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_11|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
            {else}
                <tr>
                    <td colspan="3" style="padding-left: 40px;">
                        - อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED Bulb
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 3 วัตต์ จำนวน {$activity.led_bulb_watt_1|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 4 วัตต์ จำนวน {$activity.led_bulb_watt_2|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 5 วัตต์ จำนวน {$activity.led_bulb_watt_3|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 6 วัตต์ จำนวน {$activity.led_bulb_watt_4|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 7 วัตต์ จำนวน {$activity.led_bulb_watt_5|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_5|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 9 วัตต์ จำนวน {$activity.led_bulb_watt_6|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_6|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 10 วัตต์ จำนวน {$activity.led_bulb_watt_7|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_7|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 12 วัตต์ จำนวน {$activity.led_bulb_watt_8|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_8|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 13 วัตต์ จำนวน {$activity.led_bulb_watt_9|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_9|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 15 วัตต์ จำนวน {$activity.led_bulb_watt_10|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_10|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 18 วัตต์ จำนวน {$activity.led_bulb_watt_11|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_bulb_hour_11|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 40px;">
                        - อุปกรณ์ประหยัดพลังงาน หลอดไฟ LED TUBE
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 5 วัตต์ จำนวน {$activity.led_tube_watt_1|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_1|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 6 วัตต์ จำนวน {$activity.led_tube_watt_2|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_2|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 8 วัตต์ จำนวน {$activity.led_tube_watt_3|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_3|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 10 วัตต์ จำนวน {$activity.led_tube_watt_4|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_4|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 14 วัตต์ จำนวน {$activity.led_tube_watt_5|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_5|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 18 วัตต์ จำนวน {$activity.led_tube_watt_6|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_6|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 20 วัตต์ จำนวน {$activity.led_tube_watt_7|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_7|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 22 วัตต์ จำนวน {$activity.led_tube_watt_8|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_8|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 24 วัตต์ จำนวน {$activity.led_tube_watt_9|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_9|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 25 วัตต์ จำนวน {$activity.led_tube_watt_10|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_10|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 60px;">
                        - ขนาด 36 วัตต์ จำนวน {$activity.led_tube_watt_11|number_format:0:".":","} หลอด
                    </td>
                    <td style="text-align: right;">{$activity.led_tube_hour_11|number_format:2:".":","}</td>
                    <td>&nbsp;</td>
                    <td>ชม.</td>
                </tr>
            {/if}
            <tr>
                <td colspan="6"><hr /></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">
                    ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$activity.cf_care_total|number_format:2:".":","} kgCO<sub>2</sub>e เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี ของต้นไม้ {($activity.cf_care_total/9)|number_format:0:".":","} ต้น
                </td>
            </tr>
        </table>
    </body>
</html>