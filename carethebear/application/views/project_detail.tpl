{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    {if $project_detail.end_date < $today_date}
        <section>
            <div class="container">
                <h2 class="title">โครงการที่ผ่านมา <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อโครงการ : {$project_detail.name}<br/>
                        วันที่ : {$project_detail.start_date|date_thai:"%e %B %Y"} - {$project_detail.end_date|date_thai:"%e %B %Y"} ({$project_detail.start_on|substr:0:5} - {$project_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$project_detail.company}<br/>
                        สถานที่ : {$project_detail.place}<br/>
                        ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$project_detail.cf_care_total|number_format:2:".":","} kgCO<sub>2</sub>e เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี ของต้นไม้  {($project_detail.cf_care_total/9)|number_format:0:".":","} ต้น
                        <br />
                    </div>
                    <div class="img"><img src="{$project_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$project_detail.description|nl2br}</p>
                    </div>
                    {if $project_detail.image_2 != '' || $project_detail.image_3 != ''}
                        <div class="gallery">
                            {if $project_detail.image_2 != ''}
                                <a href="{$project_detail.image_2}" class="inner"><img src="{$project_detail.image_2}"></a>
                            {/if}
                            {if $project_detail.image_3 != ''}
                                <a href="{$project_detail.image_3}" class="inner"><img src="{$project_detail.image_3}"></a>
                            {/if}
                        </div>
                    {/if}
                    {if $project_detail.vdo != ''}
                        <div class="vdo">
                            <h1 class="title">วิดีโอ</h1>
                            {$project_detail.vdo}
                        </div>
                    {/if}
                </div>
            </div>
        </section>
    {elseif $project_detail.start_date > $today_date}
            <div class="container">
                <h2 class="title">โครงการที่กำลังจะมาถึง <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อโครงการ : {$project_detail.name}<br/>
                        วันที่ : {$project_detail.start_date|date_thai:"%e %B %Y"} - {$project_detail.end_date|date_thai:"%e %B %Y"} ({$project_detail.start_on|substr:0:5} - {$project_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$project_detail.company}<br/>
                        สถานที่ : {$project_detail.place}<br/>
                    </div>
                    <div class="img"><img src="{$project_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$project_detail.description|nl2br}</p>
                    </div>
                </div>
            </div>
        </section>
    {else}
    	<section>
            <div class="container">
                <h2 class="title">โครงการที่ดำเนินการ <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อโครงการ : {$project_detail.name}<br/>
                        วันที่ : {$project_detail.start_date|date_thai:"%e %B %Y"} - {$project_detail.end_date|date_thai:"%e %B %Y"} ({$project_detail.start_on|substr:0:5} - {$project_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$project_detail.company}<br/>
                        สถานที่ : {$project_detail.place}<br/>
                    </div>
                    <div class="img"><img src="{$project_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$project_detail.description|nl2br}</p>
                    </div>
                </div>
            </div>
        </section>
        {if $project_detail.is_care_1 == 'Y' && (($start_time_minus_2_hours <= $now_time && $project_detail.start_date == $today_date) || ($project_detail.start_date < $today_date && $project_detail.end_date >= $today_date))}
            <section>
                <div class="container">
                    {if $success_msg != ""}
                    <div class="alert alert-success">
                        {$success_msg}
                    </div>
                    {/if}
                    {if $error_msg != ""}
                    <div class="alert alert-danger">
                        {$error_msg}
                    </div>
                    {/if}
                    <script>
                        function check_data()
                        {
                            $('.vehicle_id_req').hide();
                            $('#people_req').hide();
                            $('#day_req').hide();
                            $('#km_req').hide();
                            $('.has-error').removeClass('has-error');

                            with(document.add_edit)
                            {
                                if(people.value=='')
                                {
                                    $('#people_req').show();
                                    $('#people_req').parent('div').addClass('has-error');
                                    $('#people').focus();
                                    return false;
                                }
                                else if(day.value=='')
                                {
                                    $('#day_req').show();
                                    $('#day_req').parent('div').addClass('has-error');
                                    $('#day').focus();
                                    return false;
                                }
                                else if(km.value=='')
                                {
                                    $('#km_req').show();
                                    $('#km_req').parent('div').addClass('has-error');
                                    $('#km').focus();
                                    return false;
                                }
                                else if($('input[name="vehicle_id"]:checked').length == 0)
                                {
                                    $('.vehicle_id_req').show();
                                    $('.vehicle_id_req').parent('div').addClass('has-error');
                                    return false;
                                }
                            }
                        }
                    </script>
                    <form class="activity-form for-project" name="add_edit" method="post" onsubmit="return check_data();">
                        <div class="activity-form-head">
                            <img class="img" src="{$image_url}theme/default/assets/images/project-form-img1.png">
                            <h2 class="title">แบบสอบถามโครงการลดก๊าซเรือนกระจก ประเภทการเดินทาง</h2>
                            <p>(เพื่อเก็บค่าการลดการปล่อย Carbon Footprint) * Required</p>
                            <p class="form-mask">ท่านเดินทางมาร่วมงานโดย<br/><span class="font-red">(โปรดเลือกการเดินทางเพียงแบบเดียว)</span></p>
                        </div>
                        <div class="form">
                            <div class="box">
                                <div class="row">
                                    <div class="col col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span>กรุณากรอกจำนวนผู้ร่วมเดินทาง&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <input type="number" class="form-control input-people" id="people" name="people">
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;คน</span>
                                                <p id="people_req" class="required">กรุณาระบุจำนวนผู้ร่วมเดินทาง</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span>จำนวนวันที่เดินทาง&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <input type="number" class="form-control input-people" id="day" name="day">
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;วัน</span>
                                                <p id="day_req" class="required">กรุณาระบุจำนวนวันที่เดินทาง</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span>ระบุระยะทางในการเดินทางมาประชุมหรือคำนวณจาก&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <a href="https://www.google.com/maps/" target="_blank">    
                                                    <img src="{$image_url}theme/default/assets/images/project-form-map.png" width="80" class="icon-map">
                                                </a>
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;ที่นี่</span>
                                            </div>
                                            <div class="km">
                                                <img src="{$image_url}theme/default/assets/images/project-form-km.png">
                                                <input type="number" min="0" placeholder="0" class="form-control" id="km" name="km" maxlength="7">
                                                <p id="km_req" class="required text-center">กรุณาระบุระยะทาง</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12">
                                        <div class="choice">
                                            <div class="img-bg">
                                                <img src="{$image_url}theme/default/assets/images/project-form-img2.png">
                                                <img src="{$image_url}theme/default/assets/images/project-form-img3.png">
                                            </div>
                                            <div class="form-group left-style1">
                                                <label>แบบ Onsite คุณเดินทางมาอย่างไร</label>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox11" name="vehicle_id" value="1"> 
                                                    <label for="checkbox11">รถยนต์ส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox12" name="vehicle_id" value="2"> 
                                                    <label for="checkbox12">รถกระบะส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox13" name="vehicle_id" value="3"> 
                                                    <label for="checkbox13">รถตู้โดยสารส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox14" name="vehicle_id" value="4"> 
                                                    <label for="checkbox14">รถบัสโดยสารส่วนบุคคล</label>
                                                </div>
                                            </div>
                                            <div class="form-group left-style1">
                                                <label>แบบ Online ตามปกติคุณเดินทางมาอย่างไร</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox21" name="vehicle_id" value="5"> 
                                                            <label for="checkbox21">รถยนต์ส่วนบุคคล</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox22" name="vehicle_id" value="6"> 
                                                            <label for="checkbox22">รถกระบะส่วนบุคคล</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox23" name="vehicle_id" value="7"> 
                                                            <label for="checkbox23">รถแท็กซี่</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox24" name="vehicle_id" value="8"> 
                                                            <label for="checkbox24">รถโดยสารประจำทาง</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox25" name="vehicle_id" value="9"> 
                                                            <label for="checkbox25">รถตู้โดยสาร</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox26" name="vehicle_id" value="10"> 
                                                            <label for="checkbox26">รถจักรยานยนต์</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox27" name="vehicle_id" value="11"> 
                                                            <label for="checkbox27">รถไฟฟ้า</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox28" name="vehicle_id" value="12"> 
                                                            <label for="checkbox28">เดิน</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox29" name="vehicle_id" value="13"> 
                                                            <label for="checkbox29">จักรยาน</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox210" name="vehicle_id" value="14"> 
                                                            <label for="checkbox210">เรือยนต์</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox211" name="vehicle_id" value="15"> 
                                                            <label for="checkbox211">เที่ยวบินในประเทศ</label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" id="checkbox212" name="vehicle_id" value="16"> 
                                                            <label for="checkbox212">เที่ยวบินระหว่างประเทศ</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group left-style1">
                                                <label>แบบ Onsite คุณเดินทางด้วยรถ EV ประเภทไหน</label>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox31" name="vehicle_id" value="17"> 
                                                    <label for="checkbox31">รถยนต์ส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox32" name="vehicle_id" value="18"> 
                                                    <label for="checkbox32">รถกระบะส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox33" name="vehicle_id" value="19"> 
                                                    <label for="checkbox33">รถตู้โดยสารส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox34" name="vehicle_id" value="20"> 
                                                    <label for="checkbox34">รถบัสโดยสารส่วนบุคคล</label>
                                                </div>
                                            </div>
                                            <p class="required vehicle_id_req">กรุณาเลือกข้อมูลการเดินทาง</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-box">
                                <button type="submit" name="save" value="save" class="btn btn-yellow" >Save</button>
                                <button type="button" class="btn btn-grey" onclick="window.history.back();">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        {/if}
    {/if}
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}