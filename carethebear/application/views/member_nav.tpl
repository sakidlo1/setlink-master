<h2 class="title">สมาชิก Care the Bear <i class="icon-circle"></i></h2>
<div class="member-actions">
    <a href="{$base_url}member/project" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true" title="การปล่อยก๊าซเรือนกระจก (Greenhouse Gases : GHGS)<br/>จากการดำเนินโครงการต่างๆ ขององค์กร โดยนำหลักการของ 6 Care<br/>มาใช้ในการปฏิบัติ มีระยะเวลาในการดำเนินโครงการตั้งแต่ 1 เดือนขึ้นไป">
        <img src="{$image_url}theme/default/assets/images/icon-myproject.png">
        <span>โครงการของคุณ
        </span>
    </a>
    <a href="{$base_url}member/activity" class="inner" data-toggle="tooltip" data-placement="bottom" data-html="true" title="การปล่อยก๊าซเรือนกระจก (Greenhouse Gases : GHGS)<br/>จากกิจกรรมต่างๆ ขององค์กร โดยนำหลักการของ 6 Care<br/>มาใช้ในการปฏิบัติ มีระยะเวลาในการดำเนินกิจกรรม 1 วัน">
        <img src="{$image_url}theme/default/assets/images/icon-calendar.png">
        <span>กิจกรรมของคุณ
        </span>
    </a>
    <a href="{$base_url}member/document" class="inner">
        <img src="{$image_url}theme/default/assets/images/icon-project.png">
        <span>สื่อของโครงการ</span>
    </a>
    <a href="{$base_url}member/profile" class="inner">
        <img src="{$image_url}theme/default/assets/images/icon-edit.png">
        <span>แก้ไขข้อมูลสมาชิก</span>
    </a>
</div>