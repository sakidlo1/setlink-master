{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        {* <img class="img-cover is-small" src="{$image_url}theme/default/assets/images/add-cover.png">
                        <h3 class="activity-head">
                            <img src="{$image_url}theme/default/assets/images/icon-calendar.png">
                            <span>โครงการของคุณ</span>
                        </h3>
                        <div class="activity-add">แก้ไขโครงการ <img src="{$image_url}theme/default/assets/images/icon-add.png"></div> *}
                        <script>
                            function check_data()
                            {
                                $('#name_req').hide();
                                $('#place_req').hide();
                                $('#date_req').hide();
                                $('#time_req').hide();
                                $('#description_req').hide();
                                $('#attendant_req').hide();
                                $('.has-error').removeClass('has-error');
                                
                                with(document.add_edit)
                                {
                                    if(name.value=="")
                                    {
                                        $('#name_req').show();
                                        $('#name_req').parent('div').addClass('has-error');
                                        $('#name').focus();
                                        return false;
                                    }
                                    else if(place.value=="")
                                    {
                                        $('#place_req').show();
                                        $('#place_req').parent('div').addClass('has-error');
                                        $('#place').focus();
                                        return false;
                                    }
                                    else if(start_date.value=="" || end_date.value=="")
                                    {
                                        $('#date_req').show();
                                        $('#date_req').parents('.form-group').addClass('has-error');
                                        $('#start_date').focus();
                                        return false;
                                    }
                                    else if(start_on.value=="" || end_on.value=="")
                                    {
                                        $('#time_req').show();
                                        $('#time_req').parents('.form-group').addClass('has-error');
                                        $('#start_on').focus();
                                        return false;
                                    }
                                    else if(description.value=="")
                                    {
                                        $('#description_req').show();
                                        $('#description_req').parent('div').addClass('has-error');
                                        $('#description').focus();
                                        return false;
                                    }
                                    else if(activity_type_id.value=="")
                                    {
                                        $('#activity_type_id_req').show();
                                        $('#activity_type_id_req').parent('div').addClass('has-error');
                                        $('#activity_type_id').focus();
                                        return false;
                                    }
                                    else if(attendant.value=="")
                                    {
                                        $('#attendant_req').show();
                                        $('#attendant_req').parent('div').addClass('has-error');
                                        $('#attendant').focus();
                                        return false;
                                    }
                                }
                            }
                        </script>
                        <form name="add_edit" class="form-project for-project" method="post" onsubmit="return check_data();" enctype="multipart/form-data">
                            <div class="activity-control">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>ชื่อโครงการ</label>
                                            <input type="text" id="name" name="name" value="{$project.name}" class="form-control">
                                            <p id="name_req" class="required">กรุณากรอกชื่อโครงการ</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group label-small ">
                                            <div class="date-range">
                                                <input type="text" class="form-control datepicker" name="start_date" id="start_date" value="{$project.start_date}" placeholder="yyyy-mm-dd">
                                                <label>ถึง</label>
                                                <input type="text" class="form-control datepicker" name="end_date" id="end_date" value="{$project.end_date}" placeholder="yyyy-mm-dd">
                                            </div>
                                            <p id="date_req" class="required">กรุณาเลือกวันที่</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group ">
                                            <label>สถานที่</label>
                                            <input type="text" id="place" name="place" value="{$project.place}" class="form-control">
                                            <p id="place_req" class="required">กรุณากรอกสถานที่</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group label-small">
                                            <label>เวลา</label>
                                            <div class="form-inline">
                                                <div class="rang-box">
                                                    <div class="picker-box">
                                                        <input type="text" class="form-control timepicker" name="start_on" id="start_on" value="{$project.start_on|substr:0:5}" placeholder="HH:mm">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                    <span>-</span>
                                                    <div class="picker-box">
                                                        <input type="text" class="form-control timepicker" name="end_on" id="end_on" value="{$project.end_on|substr:0:5}" placeholder="HH:mm">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <p id="time_req" class="required">กรุณาระบุเวลา</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea rows="8" id="description" name="description" class="form-control is-full" placeholder="รายละเอียดของโครงการ">{$project.description}</textarea>
                                            <p id="description_req" class="required">กรุณากรอกข้อมูล</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    {if $project.image_1 != '' || $project.image_2 != '' || $project.image_3 != ''}
                                        <div class="col-sm-7">
                                            <div class="form-group has-label-upload">
                                                {if $project.image_1 != ''}
                                                    <img src="{$project.image_1}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                                {if $project.image_2 != ''}
                                                    <img src="{$project.image_2}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                                {if $project.image_3 != ''}
                                                    <img src="{$project.image_3}" style="max-height: 150px;">
                                                    <br /><br />
                                                {/if}
                                            </div>
                                        </div>
                                    {/if}
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>อัพโหลดไฟล์ภาพ <img src="{$image_url}theme/default/assets/images/icon-upload.png"><br/><small style="color:#ff0000;display: inline-block;line-height: 20px;">สกุลขอลภาพเป็น png<br/>ขนาด 800 x 533</small></label>
                                            <input type="file" id="image_1" name="image_1" accept="image/png"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-7" style="margin-top: -35px;">
                                        <div class="form-group has-label-upload">
                                            <label>&nbsp;</label>
                                            <input type="file" id="image_2" name="image_2" accept="image/png"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>&nbsp;</label>
                                            <input type="file" id="image_3" name="image_3" accept="image/png"  class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group has-label-upload">
                                            <label>ไฟล์วิดีโอ Embed Youtube</label>
                                            <textarea id="vdo" name="vdo" class="form-control">{$project.vdo}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="activity-control">
                                <div class="people">
                                    <img src="{$image_url}theme/default/assets/images/add-bg1.png">
                                    <div class="form-group">
                                        <p class="no-margin">
                                            กรุณาระบุข้อมูลการจัดโครงการ<br/>
                                            จำนวนผู้มาเข้าร่วมประชุม
                                            <input type="text" id="attendant" name="attendant" value="{$project.attendant}" class="form-control input-people">
                                            คน
                                        </p>
                                        <p id="attendant_req" class="required">กรุณากรอกจำนวนผู้เข้าร่วมโครงการ / ประชุม</p>
                                    </div>
                                </div>
                                <div class="title-type">
                                    ประเภทของโครงการ
                                </div>
                                <div class="row">
                                    <div class="col col-sm-12">
                                        <div class="input-box">
                                            <div class="head">
                                                <div class="checkbox">
                                                    <input type="checkbox" name="is_care_1" id="is_care_1" value="Y"{if $project.is_care_1 == 'Y'} checked="checked"{/if}> 
                                                    <label for="is_care_1">
                                                        <div class="text-icon is-img">
                                                            <img src="{$image_url}theme/default/assets/images/icon-type1.png">
                                                            <span>การเดินทางแบบ Car Pool</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="project-text-icon">
                                                    <img src="{$image_url}theme/default/assets/images/img-type1.png">
                                                    <span>เป็นการสร้างโครงการเพื่อให้ผู้เข้าร่วมโครงการกรอกรายละเอียดบนเว็บไซต์</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12">
                                        <div class="input-box">
                                            <div class="head">
                                                <div class="checkbox">
                                                    <input type="checkbox" name="is_care_2" id="is_care_2" value="Y"{if $project.is_care_2 == 'Y'} checked="checked"{/if}> 
                                                    <label for="is_care_2">
                                                        <div class="text-icon is-img">
                                                            <img src="{$image_url}theme/default/assets/images/icon-type2.png">
                                                            <span>Care 2 การจัดการของแจกให้กับผู้ร่วมงาน</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="body is-green">
                                                <div class="project-input">
                                                    <img src="{$image_url}theme/default/assets/images/img-type2.png" class="img-type">
                                                    <div class="form-group">
                                                        <label>
                                                            <img src="{$image_url}theme/default/assets/images/img-bag.png">
                                                            <span>ถุงพลาสติก</span>
                                                        </label>
                                                        <div class="unit">
                                                            <input type="text" id="care_2_1" name="care_2_1" value="{if $project.care_2_1 > 0}{$project.care_2_1}{/if}" class="form-control">
                                                            <span>ใบ</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>
                                                            <img src="{$image_url}theme/default/assets/images/img-bag.png">
                                                            <span>ถุงกระดาษคราฟท์</span>
                                                        </label>
                                                        <div class="unit">
                                                            <input type="text" id="care_2_2" name="care_2_2" value="{if $project.care_2_2 > 0}{$project.care_2_2}{/if}" class="form-control">
                                                            <span>กก.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-sm-12">
                                        <div class="input-box">
                                            <div class="head">
                                                <div class="checkbox">
                                                    <input type="checkbox" name="is_care_3" id="is_care_3" value="Y"{if $project.is_care_3 == 'Y'} checked="checked"{/if}> 
                                                    <label for="is_care_3">
                                                        <div class="text-icon is-img">
                                                            <img src="{$image_url}theme/default/assets/images/icon-type3.png">
                                                            <span>Care 3 การจัดการของเสียหลังการจัดงาน</span>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="project-table">
                                                    <img src="{$image_url}theme/default/assets/images/img-type3.png" class="img-type">
                                                    <label class="label-amount">จำนวนกระดาษที่ใช้</label>
                                                    <label class="label-thickness no-margin">ความหนา (แกรม)</label>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_1" id="care_3_1" value="Y"{if $project.care_3_1 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_1">55</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_1_1" name="care_3_1_1" value="{if $project.care_3_1_1 > 0}{$project.care_3_1_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_1_2" name="care_3_1_2" value="{if $project.care_3_1_2 > 0}{$project.care_3_1_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_1_3" name="care_3_1_3" value="{if $project.care_3_1_3 > 0}{$project.care_3_1_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_1_4" name="care_3_1_4" value="{if $project.care_3_1_4 > 0}{$project.care_3_1_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_1_5" name="care_3_1_5" value="{if $project.care_3_1_5 > 0}{$project.care_3_1_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_1_6" name="care_3_1_6" value="{if $project.care_3_1_6 > 0}{$project.care_3_1_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_2" id="care_3_2" value="Y"{if $project.care_3_2 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_2">70</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_2_1" name="care_3_2_1" value="{if $project.care_3_2_1 > 0}{$project.care_3_2_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_2_2" name="care_3_2_2" value="{if $project.care_3_2_2 > 0}{$project.care_3_2_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_2_3" name="care_3_2_3" value="{if $project.care_3_2_3 > 0}{$project.care_3_2_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_2_4" name="care_3_2_4" value="{if $project.care_3_2_4 > 0}{$project.care_3_2_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_2_5" name="care_3_2_5" value="{if $project.care_3_2_5 > 0}{$project.care_3_2_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_2_6" name="care_3_2_6" value="{if $project.care_3_2_6 > 0}{$project.care_3_2_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_3" id="care_3_3" value="Y"{if $project.care_3_3 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_3">80</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_3_1" name="care_3_3_1" value="{if $project.care_3_3_1 > 0}{$project.care_3_3_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_3_2" name="care_3_3_2" value="{if $project.care_3_3_2 > 0}{$project.care_3_3_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_3_3" name="care_3_3_3" value="{if $project.care_3_3_3 > 0}{$project.care_3_3_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_3_4" name="care_3_3_4" value="{if $project.care_3_3_4 > 0}{$project.care_3_3_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_3_5" name="care_3_3_5" value="{if $project.care_3_3_5 > 0}{$project.care_3_3_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_3_6" name="care_3_3_6" value="{if $project.care_3_3_6 > 0}{$project.care_3_3_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_4" id="care_3_4" value="Y"{if $project.care_3_4 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_4">90</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_4_1" name="care_3_4_1" value="{if $project.care_3_4_1 > 0}{$project.care_3_4_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_4_2" name="care_3_4_2" value="{if $project.care_3_4_2 > 0}{$project.care_3_4_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_4_3" name="care_3_4_3" value="{if $project.care_3_4_3 > 0}{$project.care_3_4_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_4_4" name="care_3_4_4" value="{if $project.care_3_4_4 > 0}{$project.care_3_4_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_4_5" name="care_3_4_5" value="{if $project.care_3_4_5 > 0}{$project.care_3_4_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_4_6" name="care_3_4_6" value="{if $project.care_3_4_6 > 0}{$project.care_3_4_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_5" id="care_3_5" value="Y"{if $project.care_3_5 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_5">100</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_5_1" name="care_3_5_1" value="{if $project.care_3_5_1 > 0}{$project.care_3_5_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_5_2" name="care_3_5_2" value="{if $project.care_3_5_2 > 0}{$project.care_3_5_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_5_3" name="care_3_5_3" value="{if $project.care_3_5_3 > 0}{$project.care_3_5_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_5_4" name="care_3_5_4" value="{if $project.care_3_5_4 > 0}{$project.care_3_5_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_5_5" name="care_3_5_5" value="{if $project.care_3_5_5 > 0}{$project.care_3_5_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_5_6" name="care_3_5_6" value="{if $project.care_3_5_6 > 0}{$project.care_3_5_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="care_3_6" id="care_3_6" value="Y"{if $project.care_3_6 == 'Y'} checked="checked"{/if}> 
                                                                <label for="care_3_6">120</label>
                                                            </div>
                                                        </label>
                                                        <div class="unit">
                                                            <div class="col">
                                                                <p>A3</p>
                                                                <input type="text" id="care_3_6_1" name="care_3_6_1" value="{if $project.care_3_6_1 > 0}{$project.care_3_6_1}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A4</p>
                                                                <input type="text" id="care_3_6_2" name="care_3_6_2" value="{if $project.care_3_6_2 > 0}{$project.care_3_6_2}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>A5</p>
                                                                <input type="text" id="care_3_6_3" name="care_3_6_3" value="{if $project.care_3_6_3 > 0}{$project.care_3_6_3}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B3</p>
                                                                <input type="text" id="care_3_6_4" name="care_3_6_4" value="{if $project.care_3_6_4 > 0}{$project.care_3_6_4}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B4</p>
                                                                <input type="text" id="care_3_6_5" name="care_3_6_5" value="{if $project.care_3_6_5 > 0}{$project.care_3_6_5}{/if}" class="form-control">
                                                            </div>
                                                            <div class="col">
                                                                <p>B5</p>
                                                                <input type="text" id="care_3_6_6" name="care_3_6_6" value="{if $project.care_3_6_6 > 0}{$project.care_3_6_6}{/if}" class="form-control">
                                                            </div>
                                                        </div>
                                                        <p class="required">กรุณากรอกข้อมูลให้ครบ</p>
                                                    </div>
                                                    <p class="required">กรุณาเลือกความหนา</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="btn-box text-right">
                                    <button type="submit" name="save" value="save" class="btn btn-yellow">Save</button>
                                    <button type="button" class="btn btn-grey" onclick="window.history.back();">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}