{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
	{block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">สมัครเข้าร่วมโครงการ <i class="icon-circle"></i></h2>
                    <div class="card-login">
                        <h2 class="name">Care the Bear</h2>
                        <div class="box">
                            <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/login-bg.png');"></div>
                            <script>
		                        var email_exists = true;
		                        var username_exists = true;
		                        
		                        function check_email_exists()
		                        {
		                            if($('#email').val() != "") {
		                                $.ajax({
		                                    type: 'POST',
		                                    url: '{$base_url}member/check_email_exists',
		                                    data: '{literal}{"email":"' + $('#email').val() + '"}{/literal}',
		                                    success: function(data) { 
		                                        if(data.status == false) {
		                                            email_exists = false;
		                                            check_username_exists();
		                                        } else {
		                                            return check_data();
		                                        }
		                                    },
		                                    contentType: "application/json",
		                                    dataType: 'json'
		                                });
		                                return false;
		                            } else {
		                                return check_data();
		                            }
		                        }

		                        function check_username_exists()
		                        {
		                            if($('#username').val() != "") {
		                                $.ajax({
		                                    type: 'POST',
		                                    url: '{$base_url}member/check_username_exists',
		                                    data: '{literal}{"username":"' + $('#username').val() + '"}{/literal}',
		                                    success: function(data) { 
		                                        if(data.status == false) {
		                                            username_exists = false;
		                                            if(check_data() != false) {
		                                                document.add_edit.submit();
		                                            }
		                                        } else {
		                                            return check_data();
		                                        }
		                                    },
		                                    contentType: "application/json",
		                                    dataType: 'json'
		                                });
		                                return false;
		                            } else {
		                                return check_data();
		                            }
		                        }

		                        function check_data()
		                        {
		                        	$('#company_req').hide();
		                        	$('#logo_req').hide();
		                        	$('#company_type_req').hide();
		                        	$('#name_req').hide();
		                        	$('#position_req').hide();
		                        	$('#department_req').hide();
		                        	$('#address_req').hide();
		                        	$('#tel_req').hide();
		                        	$('#mobile_req').hide();
		                            $('#email_req').hide();
		                            $('#email_exists').hide();
		                            $('#username_req').hide();
		                            $('#username_exists').hide();
		                            $('#password_req').hide();
		                            $('#confirm_password_req').hide();
		                            $('#confirm_password_inc').hide();
		                            $('#is_accept_req').hide();
		                            $('.has-error').removeClass('has-error');
		                            
		                            with(document.add_edit)
		                            {

		                                if(company.value=="")
		                                {
		                                    $('#company_req').show();
		                                    $('#company_req').parent('div').addClass('has-error');
		                                    $('#company').focus();
		                                    return false;
		                                }
		                                else if(logo.value=="")
		                                {
		                                    $('#logo_req').show();
		                                    $('#logo_req').parent('div').addClass('has-error');
		                                    $('#logo').focus();
		                                    return false;
		                                }
		                                else if(company_type.value=="")
		                                {
		                                    $('#company_type_req').show();
		                                    $('#company_type_req').parent('div').addClass('has-error');
		                                    $('#company_type').focus();
		                                    return false;
		                                }
		                                else if(name.value=="")
		                                {
		                                    $('#name_req').show();
		                                    $('#name_req').parent('div').addClass('has-error');
		                                    $('#name').focus();
		                                    return false;
		                                }
		                                else if(position.value=="")
		                                {
		                                    $('#position_req').show();
		                                    $('#position_req').parent('div').addClass('has-error');
		                                    $('#position').focus();
		                                    return false;
		                                }
		                                else if(department.value=="")
		                                {
		                                    $('#department_req').show();
		                                    $('#department_req').parent('div').addClass('has-error');
		                                    $('#department').focus();
		                                    return false;
		                                }
		                                else if(address.value=="")
		                                {
		                                    $('#address_req').show();
		                                    $('#address_req').parent('div').addClass('has-error');
		                                    $('#address').focus();
		                                    return false;
		                                }
		                                else if(tel.value=="")
		                                {
		                                    $('#tel_req').show();
		                                    $('#tel_req').parent('div').addClass('has-error');
		                                    $('#tel').focus();
		                                    return false;
		                                }
		                                else if(mobile.value=="")
		                                {
		                                    $('#mobile_req').show();
		                                    $('#mobile_req').parent('div').addClass('has-error');
		                                    $('#mobile').focus();
		                                    return false;
		                                }
		                                else if(email.value=="")
		                                {
		                                    $('#email_req').show();
		                                    $('#email_req').parent('div').addClass('has-error');
		                                    $('#email').focus();
		                                    return false;
		                                }
		                                else if(email_exists == true)
		                                {
		                                    $('#email_exists').show();
		                                    $('#email_exists').parent('div').addClass('has-error');
		                                    $('#email').focus();
		                                    return false;
		                                }
		                                else if(username.value=="")
		                                {
		                                    $('#username_req').show();
		                                    $('#username_req').parent('div').addClass('has-error');
		                                    $('#username').focus();
		                                    return false;
		                                }
		                                else if(username_exists == true)
		                                {
		                                    $('#username_exists').show();
		                                    $('#username_exists').parent('div').addClass('has-error');
		                                    $('#username').focus();
		                                    return false;
		                                }
		                                else if(password.value=="")
		                                {
		                                    $('#password_req').show();
		                                    $('#password_req').parent('div').addClass('has-error');
		                                    $('#password').focus();
		                                    return false;
		                                }
		                                else if(cpassword.value=="")
		                                {
		                                    $('#confirm_password_req').show();
		                                    $('#confirm_password_req').parent('div').addClass('has-error');
		                                    $('#cpassword').focus();
		                                    return false;
		                                }
		                                else if(password.value!=cpassword.value)
		                                {
		                                    $('#confirm_password_inc').show();
		                                    $('#confirm_password_inc').parent('div').addClass('has-error');
		                                    $('#cpassword').focus();
		                                    return false;
		                                }
		                                else if(is_accept.checked == false)
                                        {
                                            $('#is_accept_req').show();
                                            $('#is_accept_req').parent('div').addClass('has-error');
                                            $('#is_accept').focus();
                                            return false;
                                        }
		                            }
		                        }
		                    </script>
                            <form name="add_edit" onsubmit="return check_email_exists();" method="post" enctype="multipart/form-data">
                                <h3 class="text-center"><img src="{$image_url}theme/default/assets/images/icon-register.png" width="60"> สมัครเข้าร่วมโครงการ</h3>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="company" name="company" class="form-control" placeholder="บริษัท/องค์กร*" value="{if $climate_member.company.name != ''}{$climate_member.company.name}{/if}">
                                    <label>บริษัท/องค์กร*</label>
                                    <p id="company_req" class="required">กรุณากรอกชื่อบริษัท/องค์กร</p>
                                </div>
                                <div class="form-group form-project no-padding has-label-upload">
                                    <label>อัพโหลดโลโก้ <img src="{$image_url}theme/default/assets/images/icon-upload.png">*</label>
                                    <input type="file" id="logo" name="logo" accept="image/gif, image/jpeg, image/png"  class="form-control">
                                    <p id="logo_req" class="required">กรุณาอัพโหลดโลโก้</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <label>ประเภทองค์กร*</label>
                                    <select id="company_type" name="company_type" class="form-control">
                                        <option value="">ประเภทองค์กร*</option>
                                        {foreach $company_type as $company_type_item}
                                        	<optgroup label="{$company_type_item.name}">
	                                        	{foreach $company_type_item.item as $company_group_item}
	                                        		<option value="{$company_group_item.company_type_id}-{$company_group_item.id}">{$company_group_item.name}</option>
	                                        	{/foreach}
                                        	</optgroup>
                                        {/foreach}
                                    </select>
                                    <p id="company_type_req" class="required">กรุณาเลือกประเภทองค์กร</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="ผู้สมัคร ชื่อ นามสกุล*" value="{if $climate_member.name != ''}{$climate_member.name}{/if}{if $climate_member.surname != ''} {$climate_member.surname}{/if}">
                                    <label>ผู้สมัคร ชื่อ นามสกุล*</label>
                                    <p id="name_req" class="required">กรุณากรอกชื่อ นามสกุล</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="position" name="position" class="form-control" placeholder="ตำแหน่ง*" value="{if $climate_member.position != ''}{$climate_member.position}{/if}">
                                    <label>ตำแหน่ง*</label>
                                    <p id="position_req" class="required is-hide-label">กรุณากรอกตำแหน่ง</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" name="department" id="department" class="form-control" placeholder="ฝ่ายงาน*" value="{if $climate_member.department != ''}{$climate_member.department}{/if}">
                                    <label>ฝ่ายงาน*</label>
                                    <p id="department_req" class="required">กรุณากรอกฝ่ายงาน</p>
                                </div>
                                <div class="form-group is-hide-label">
                                	<input type="text" name="address" id="address" class="form-control" placeholder="ที่อยู่*" value="{if $climate_member.company.address != ''}{$climate_member.company.address}{/if}">
                                    <label>ที่อยู่*</label>
                                    <p id="address_req" class="required">กรุณากรอกที่อยู่</p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group is-hide-label">
                                            <input type="text" id="tel" name="tel" class="form-control" placeholder="โทรศัพท์*" value="{if $climate_member.telephone != ''}{$climate_member.telephone}{/if}">
                                            <label>โทรศัพท์*</label>
                                            <p id="tel_req" class="required">กรุณากรอกหมายเลขโทรศัพท์</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group is-hide-label">
                                            <input type="text" id="mobile" name="mobile" class="form-control" placeholder="โทรศัพท์มือถือ*" value="{if $climate_member.mobile != ''}{$climate_member.mobile}{/if}">
                                            <label>โทรศัพท์มือถือ*</label>
                                            <p id="mobile_req" class="required">กรุณากรอกหมายเลขโทรศัพท์มือถือ</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="อีเมล*" value="{if $climate_member.email != ''}{$climate_member.email}{/if}">
                                    <label>อีเมล*</label>
                                    <p id="email_req" class="required">กรุณากรอกอีเมล</p>
                                    <p id="email_exists" class="required">อีเมลนี้มีอยู่ในระบบแล้ว กรุณาใช้อีเมลอื่น</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="username" name="username" class="form-control" placeholder="ชื่อผู้ใช้งาน (Username)*" value="{if $climate_member.username != ''}{$climate_member.username}{/if}">
                                    <label>ชื่อผู้ใช้งาน (Username)*</label>
                                    <p id="username_req" class="required">กรุณากรอกชื่อผู้ใช้งาน</p>
                                    <p id="username_exists" class="required">ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว กรุณาใช้ชื่อผู้ใช้งานอื่น</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="รหัสผ่าน (Password)*">
                                    <label>รหัสผ่าน (Password)*</label>
                                    <p id="password_req" class="required">กรุณากรอกรหัสผ่าน</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="ยืนยันรหัสผ่าน*">
                                    <label>ยืนยันรหัสผ่าน*</label>
                                    <p id="confirm_password_req" class="required">กรุณายืนยันรหัสผ่าน</p>
                                    <p id="confirm_password_inc" class="required">กรุณายืนยันรหัสผ่านให้ถูกต้อง</p>
                                </div>
                                <br/><br/>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" id="input-remember" name="is_set_staff" value="Y"> 
                                        <label for="input-remember">คุณเป็นพนักงานของตลาดหลักทรัพย์แห่งประเทศไทย</label>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" id="input-accept" name="is_accept" value="Y"> 
                                        <label for="input-accept">ข้าพเจ้าขอยืนยันว่าข้าพเจ้าได้อ่านและเข้าใจข้อตกลงในการใช้บริการเว็บไซต์ carethebear.com <a href="{$base_url}term" class="font-orange" target="_blank">อ่านข้อตกลงที่นี่</a></label>
                                    </div>
                                    <p class="required" id="is_accept_req"><small>กรุณายอมรับเงื่อนไข</small></p>
                                </div>
                                <br/><br/>
                                <div class="form-group text-center">
                                    <button type="submit" name="register" value="register" class="btn btn-yellow">สมัครสมาชิก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}