{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">ตั้งรหัสผ่านใหม่ <i class="icon-circle"></i></h2>
                    <div class="card-login">
                        <h2 class="name">Care the Bear</h2>
                        <div class="box">
                            <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/login-bg.png');"></div>
                            <script>
                                function check_data()
                                {
                                    $('#password_req').hide();
                                    $('#confirm_password_req').hide();
                                    $('#confirm_password_inc').hide();
                                    $('.has-error').removeClass('has-error');
                                    
                                    with(document.add_edit)
                                    {

                                        if(password.value=="")
                                        {
                                            $('#password_req').show();
                                            $('#password_req').parent('div').addClass('has-error');
                                            $('#password').focus();
                                            return false;
                                        }
                                        else if(cpassword.value=="")
                                        {
                                            $('#confirm_password_req').show();
                                            $('#confirm_password_req').parent('div').addClass('has-error');
                                            $('#cpassword').focus();
                                            return false;
                                        }
                                        else if(password.value!=cpassword.value)
                                        {
                                            $('#confirm_password_inc').show();
                                            $('#confirm_password_inc').parent('div').addClass('has-error');
                                            $('#cpassword').focus();
                                            return false;
                                        }
                                    }
                                }
                            </script>
                            <form name="add_edit" onsubmit="return check_data();" method="post">
                                <h2 class="font-cond">สำหรับสมาชิก ตั้งรหัสผ่านใหม่</h2>
                                <div class="form-group">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="รหัสผ่านใหม่">
                                    <label>รหัสผ่านใหม่</label>
                                    <p id="password_req" class="required">กรุณากรอกรหัสผ่านใหม่</p>
                                </div>
                                <div class="form-group">
                                    <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="ยืนยันรหัสผ่าน">
                                    <label>ยืนยันรหัสผ่าน</label>
                                    <p id="confirm_password_req" class="required">กรุณายืนยันรหัสผ่าน</p>
                                    <p id="confirm_password_inc" class="required">กรุณายืนยันรหัสผ่านให้ถูกต้อง</p>
                                </div>
                                <br/><br/>
                                <div class="form-group text-center">
                                    <button type="submit" name="reset_password" value="reset_password" class="btn btn-yellow">ตั้งรหัสผ่านใหม่</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}