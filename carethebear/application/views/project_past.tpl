{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section class="no-padding">
        <div class="container">
            <br/>
            <h2 class="title no-margin-top inline-block">{if $smarty.get.q != ''}ผลการค้นหา{else}โครงการที่ดำเนินการเสร็จสิ้น <i class="icon-circle"></i>{/if}</h2>
                <form method="get" class="activity-search form-inline">
                <script>
                    function select(value)
                    {
                        if(value == 'type')
                        {
                            $('.act-search').hide(); 
                            $('.act-search[data-mode="dropdown"]').show();
                            $('.act-search[data-mode="dropdown2"]').hide();
                        }
                        else if(value == 'tsd')
                        {
                            $('.act-search').hide(); 
                            $('.act-search[data-mode="dropdown"]').hide();
                            $('.act-search[data-mode="dropdown2"]').show();
                        }
                        else
                        {
                            $('.act-search').hide(); 
                            $('.act-search[data-mode="text"]').show();
                        }
                    }
                </script>
                <select name="type" class="form-control" onchange="select(this.value)">
                    <option value="">กรุณาเลือก</option>
                    <option value="company"{if $smarty.get.type == 'company'} selected="selected"{/if}>ชื่อบริษัท/องค์กร</option>
                    <option value="type"{if $smarty.get.type == 'type'} selected="selected"{/if}>ประเภทโครงการ</option>
                    <option value="tsd"{if $smarty.get.type == 'tsd'} selected="selected"{/if}>TSD Projects</option>
                </select>
                <select name="project_type_id" class="form-control act-search" data-mode="dropdown"{if $smarty.get.type != 'type'} style="display:none;"{/if}>
                    <option value="">กรุณาเลือก</option>
                    <option value="1"{if $smarty.get.project_type_id == '1'} selected="selected"{/if}>โครงการ Car Pool</option>
                    <option value="2"{if $smarty.get.project_type_id == '2'} selected="selected"{/if}>โครงการงดรับถุงกระดาษ</option>
                    <option value="3"{if $smarty.get.project_type_id == '3'} selected="selected"{/if}>โครงการงดรับถุงพลาสติก</option>
                </select>
                <select name="project_tsd" class="form-control act-search" data-mode="dropdown2"{if $smarty.get.type != 'tsd'} style="display:none;"{/if}>
                    <option value="">กรุณาเลือก</option>
                    <option value="CD"{if $smarty.get.project_tsd == 'CD'} selected="selected"{/if}>CD</option>
                    <option value="CN"{if $smarty.get.project_tsd == 'CN'} selected="selected"{/if}>CN</option>
                    <option value="DG"{if $smarty.get.project_tsd == 'DG'} selected="selected"{/if}>DG</option>
                    <option value="MA"{if $smarty.get.project_tsd == 'MA'} selected="selected"{/if}>MA</option>
                    <option value="NR"{if $smarty.get.project_tsd == 'NR'} selected="selected"{/if}>NR</option>
                    <option value="PO"{if $smarty.get.project_tsd == 'PO'} selected="selected"{/if}>PO</option>
                    <option value="SC"{if $smarty.get.project_tsd == 'SC'} selected="selected"{/if}>SC</option>
                    <option value="XB"{if $smarty.get.project_tsd == 'XB'} selected="selected"{/if}>XB</option>
                    <option value="XD"{if $smarty.get.project_tsd == 'XD'} selected="selected"{/if}>XD</option>
                    <option value="XE"{if $smarty.get.project_tsd == 'XE'} selected="selected"{/if}>XE</option>
                    <option value="XI"{if $smarty.get.project_tsd == 'XI'} selected="selected"{/if}>XI</option>
                    <option value="XM"{if $smarty.get.project_tsd == 'XM'} selected="selected"{/if}>XM</option>
                    <option value="XO"{if $smarty.get.project_tsd == 'XO'} selected="selected"{/if}>XO</option>
                    <option value="XP"{if $smarty.get.project_tsd == 'XP'} selected="selected"{/if}>XP</option>
                    <option value="XR"{if $smarty.get.project_tsd == 'XR'} selected="selected"{/if}>XR</option>
                    <option value="XS"{if $smarty.get.project_tsd == 'XS'} selected="selected"{/if}>XS</option>
                    <option value="XT"{if $smarty.get.project_tsd == 'XT'} selected="selected"{/if}>XT</option>
                </select>
                <input type="text" name="q" class="form-control act-search" data-mode="text" value="{$smarty.get.q}" placeholder="ระบุคำค้นหา"{if $smarty.get.type == 'type'} style="display:none;"{/if}
                {if $smarty.get.type == 'tsd'} style="display:none;"{/if}>
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            <div class="row activity-past">
                {if $list|count > 0}
                    {foreach $list as $key => $list_item}
                        <div class="col-sm-4">
                            <a href="{$base_url}project/detail/{$list_item.id}" class="activity item">

                            {if $smarty.get.type == ''}
                                <div class="img">
                                    <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                </div>
                                <div class="text">
                                    <h3 class="no-margin">{$list_item.name}</h3>
                                    <h3>จัดโดย {$list_item.company}</h3>
                                    <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                    <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                </div>
                            {elseif $smarty.get.type == 'company'}
                                <div class="img">
                                    <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                </div>
                                <div class="text">
                                    <h3 class="no-margin">{$list_item.name}</h3>
                                    <h3>จัดโดย {$list_item.company}</h3>
                                    <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                    <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                </div>
                            {elseif $smarty.get.type == 'type'}
                                <div class="img">
                                    <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                </div>
                                <div class="text">
                                    <h3 class="no-margin">{$list_item.name}</h3>
                                    <h3>จัดโดย {$list_item.company}</h3>
                                    <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                    <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                </div>
                            {elseif $smarty.get.type == 'tsd'}
                                <div class="img">
                                    <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/tsd_project_default.png')"></div>
                                </div>
                                <div class="text">
                                    <h3 class="no-margin">{$list_item.project_name}</h3>
                                    <h3>จัดโดย {$list_item.company_name_th}</h3>
                                    <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                </div>
                            {/if}
                            

                                {* {if $smarty.get.type == ''}
                                    <div class="img">
                                        <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                    </div>
                                    <div class="text">
                                        <h3 class="no-margin">{$list_item.name}</h3>
                                        <h3>จัดโดย {$list_item.company}</h3>
                                        <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                        <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                    </div>
                                {/if}
                            
                                {if $smarty.get.type == 'company'}
                                    <div class="img">
                                        <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                    </div>
                                    <div class="text">
                                        <h3 class="no-margin">{$list_item.name}</h3>
                                        <h3>จัดโดย {$list_item.company}</h3>
                                        <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                        <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                    </div>
                                {/if}

                                {if $smarty.get.type == 'type'}
                                    <div class="img">
                                        <div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                    </div>
                                    <div class="text">
                                        <h3 class="no-margin">{$list_item.name}</h3>
                                        <h3>จัดโดย {$list_item.company}</h3>
                                        <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                        <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                </div>
                                {/if}
                                {if $smarty.get.type == 'tsd'}
                                    <div class="img">
                                        <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/tsd_project_default.png')"></div>
                                    </div>
                                    <div class="text">
                                        <h3 class="no-margin">{$list_item.project_name}</h3>
                                        <h3>จัดโดย {$list_item.company_name_th}</h3>
                                        <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                        
                                    </div>
                                {/if} *}

                                {* <div class="img">
                                	<div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                </div>
                                <div class="text">
                                    <h3 class="no-margin">{$list_item.name}</h3>
                                    <h3>จัดโดย {$list_item.company}</h3>
                                    <div class="by">ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนเทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                    <div class="detail">{$list_item.description|truncate:500|nl2br}</div>
                                </div> *}
                            </a>
                        </div>
                        {if ($key + 1) % 3 == 0}
                        <div class="clearfix"></div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="inner">
                        <br/><br/><br/>
                        <center>ไม่มีข้อมูล</center>
                        <br/><br/><br/>
                    </div>
                {/if}
            </div>
            {include file='paging.tpl'}
        </div>
    </section>
{/block}