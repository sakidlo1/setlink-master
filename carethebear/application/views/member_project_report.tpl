{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/dashboard-carethebear.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section style="padding: 0px 0px;">
    <div class="global-warming-detail inviewspy inviewed">
                <div class="title f_bold">ผลรวมลดปริมาณก๊าซเรือนกระจก <br class="show-xs">และเทียบเท่าการปลูกต้นไม้
                </div>
                <div class="detail-wrapper flex column-xs">
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/climatecare/public/images/home/summary-progress/air-pollution.png" alt="air-pollution">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">ลดปริมาณก๊าซเรือนกระจกได้</p>
                            <span>
                                <span class="stats-value f_bold" data-global="gas" data-no="">{$stats.total.cf}</span><br class="show-xs">
                                <span class="f_reg">Kg CO<sub>2</sub>e</span>
                            </span>
                        </div>
                    </div>
                    <div class="detail flex center wrap">
                        <div class="wrap-img">
                            <img class="IMG" src="{$image_url}theme/default/climatecare/public/images/home/summary-progress/trees.png" alt="trees">
                        </div>
                        <div class="wrap-stats">
                            <p class="f_bold">เทียบเท่าการปลูกต้นไม้ใหญ่ อายุ 10 ปี</p>
                            <span>
                                <span class="stats-value f_bold" data-global="tree" data-no="">{$stats.total.tree}</span><br class="show-xs">
                                <span class="f_reg">ต้น</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section style="padding: 0px 0px;" data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        <div class="report for-project" id="report">
                            <button id="btn-print" class="pull-right" onclick="print()" style="border: 0px;background: transparent;"><span class="glyphicon glyphicon-print"></span></button>
                            <div class="project">
                                <h3>{$project.name}</h3>
                                <p>ระยะเวลาของโครงการ วันที่ {$project.start_date|date_thai:"%e %B %Y"} ถึง วันที่ {$project.end_date|date_thai:"%e %B %Y"}</p>
                                <img src="{$project.image_1}">
                            </div>
                            <div class="carbon">
                                <div class="head">
                                    <img src="{$member.logo}">
                                    <h3>โครงการนี้ช่วยลดปริมาณคาร์บอนฟุตพริ้นท์</h3>
                                    <img src="{$image_url}theme/default/assets/images/img-report2.png">
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="col col-sm-7">
                                            <div class="inner">
                                                <div class="box">ลดการใช้พลังงานจากการเดินทาง</div>
                                                <div class="box">
                                                    <span>{$project.cf_care_1|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">ลดการใช้ถุงพลาสติกและถุงกระดาษ</div>
                                                <div class="box">
                                                    <span>{$project.cf_care_2|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                            <div class="inner">
                                                <div class="box">ลดการใช้กระดาษ</div>
                                                <div class="box">
                                                    <span>{$project.cf_care_3|number_format:2:".":","}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="carbon-avg">
                                <p><span>ปริมาณคาร์บอนฟุตพริ้นที่ลดได้รวม</span> <big class="bg-blue">{$project.cf_care_total|number_format:2:".":","}</big> <span>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า</span></p>
                                <div class="clearfix"></div>
                                <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png"> เทียบเท่ากับการปลูกต้นไม้</span> <big class="bg-green">{($project.cf_care_total/9)|number_format:0:".":","}</big> <span>ต้น</span></p>
                            </div>
                            <script>
                                function select_mode(value)
                                {
                                    $('.mode-action').hide();
                                    $('.mode-action[data-mode="'+value+'"]').show();
                                    get_summary_cf();
                                }
                            </script>
                            <div class="mode-filter carbon-date">
                                <h3>บริษัทคุณช่วยลดปริมาณคาร์บอนฟุตพริ้นท์</h3>
                                <div class="form-group no-margin">
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode1" id="mode1" checked onchange="select_mode(this.value)"> 
                                        <label for="mode1">ตั้งแต่เริ่ม</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode2" id="mode2" onchange="select_mode(this.value)"> 
                                        <label for="mode2">ช่วงเวลา</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="mode" value="mode3" id="mode3" onchange="select_mode(this.value)"> 
                                        <label for="mode3">ช่วงเดือน</label>
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode1" style="display: block;">
                                    <span>ตั้งแต่เริ่ม จนถึง</span>
                                    <div class="date">
                                        <input type="date" id="end_date" name="end_date" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode2">
                                    <span>ระยะเวลา</span>
                                    <div class="date">
                                        <input type="date" id="from" name="from" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                    <span>ถึง</span>
                                    <div class="date">
                                        <input type="date" id="to" name="to" value="{$today}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                                <div class="form-group mode-action" data-mode="mode3">
                                    <span>เดือน / ปี</span>
                                    <div class="date">
                                        <input type="month" id="month_year" name="month_year" value="{$month}" class="form-control" onchange="get_summary_cf();" >
                                        <img src="{$image_url}theme/default/assets/images/icon-date.png">
                                    </div>
                                </div>
                            </div>
                            <div class="mode-result">
                                <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดกิจกรรม</h3></p>
                                </div>                                
                                <div class="carbon-avg has-bg">
                                    <p><span class="two-row">ปริมาณคาร์บอนฟุตพริ้นที่ลดได้จากการจัดกิจกรรม</span> <big id="total-activity-cf"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์นเทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span> <big id="total-activity-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                               <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดโครงการ</h3></p>
                                </div>                                
                                <div class="carbon-avg has-bg">
                                    <p><span class="two-row">ปริมาณคาร์บอนฟุตพริ้นที่ลดได้จากการจัดโครงการ</span> <big id="total-project-cf" class="bg-whit"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span> <big id="total-project-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                                </div>
                                <div class="carbon-date">
                                    <p><h3>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้จากการจัดกิจกรรมและโครงการ</h3></p>
                                    <div class="carbon-avg has-bg">
                                    <p><span>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้รวม</span> <big id="total-cf"></big> <span>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า</span></p>
                                    <div class="clearfix"></div>
                                    <p><span><img class="icon-tree" src="{$image_url}theme/default/assets/images/icon-tree.png">เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้</span><big id="total-tree" class="bg-green"></big> <span>ต้น</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script src="{$image_url}theme/default/assets/js/html2canvas.min.js"></script>
    <script>
        var myTimeout = null;
        function print()
        {
            $('#btn-print').hide();
            var viewportwidth = document.documentElement.clientWidth;
            var viewportheight = document.documentElement.clientHeight;
            var printWindow = window.open('','PrintWindow', 'width=750,height=450,left='+(viewportwidth-750)+',top=0');
            html2canvas($('#report')[0]).then(function (canvas) {
                $('#btn-print').show();
                var doc = printWindow.document;
                var img = doc.createElement('img');
                img.src = canvas.toDataURL('image/png');
                img.style.width = '100%';
                img.style.marginTop = '55px';
                doc.body.appendChild(img);
                myTimeout = setTimeout(function () {
                    printWindow.print();
                    printWindow.close();
                    clearTimeout(myTimeout);
                }, 300);
            });
        }
        
        {literal}
        function input_format(value, dec)
        {
            value = value.toString().replace(/\,/g,"");
            if(value != "")
            {
                if(dec == 0)
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                }
                else
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                }
            }
            else
            {
                return '';
            }
        }
        {/literal}

        function get_summary_cf()
        {
            $.get('{$base_url}{$page}/get_summary_cf/' + $('input[name="mode"]:checked').val() + '/' + $('#end_date').val() + '/' + $('#from').val() + '/' + $('#to').val() + '/' + $('#month_year').val(), function(data) {
                $('#total-cf').html(input_format(data.total_cf, 2));
                $('#total-tree').html(input_format(data.total_tree, 0));
                $('#total-activity-cf').html(input_format(data.total_activity_cf, 2));
                $('#total-activity-tree').html(input_format(data.total_activity_tree, 0));
                $('#total-project-cf').html(input_format(data.total_project_cf, 2));
                $('#total-project-tree').html(input_format(data.total_project_tree, 0));
            });
        }

        $(document).ready(function () {

            get_summary_cf();

        });
    </script>
{/block}