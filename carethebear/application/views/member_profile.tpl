{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    {include file='member_nav.tpl'}
                    <div class="card-login">
                        <h2 class="name"></h2>
                        {if $success_msg != ""}
                        <div class="alert alert-success">
                            {$success_msg}
                        </div>
                        {/if}
                        {if $error_msg != ""}
                        <div class="alert alert-danger">
                            {$error_msg}
                        </div>
                        {/if}
                        <div class="box">
                            <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/login-bg.png');"></div>
                            <script>
		                        function check_data()
		                        {
		                        	$('#company_req').hide();
                                    $('#company_type_req').hide();
		                        	$('#name_req').hide();
		                        	$('#position_req').hide();
		                        	$('#department_req').hide();
		                        	$('#address_req').hide();
		                        	$('#tel_req').hide();
		                        	$('#mobile_req').hide();
		                            $('#confirm_password_req').hide();
		                            $('#confirm_password_inc').hide();
		                            $('.has-error').removeClass('has-error');
		                            
		                            with(document.add_edit)
		                            {

		                                if(company.value=="")
		                                {
		                                    $('#company_req').show();
		                                    $('#company_req').parent('div').addClass('has-error');
		                                    $('#company').focus();
		                                    return false;
		                                }
                                        else if(company_type.value=="")
                                        {
                                            $('#company_type_req').show();
                                            $('#company_type_req').parent('div').addClass('has-error');
                                            $('#company_type').focus();
                                            return false;
                                        }
		                                else if(name.value=="")
		                                {
		                                    $('#name_req').show();
		                                    $('#name_req').parent('div').addClass('has-error');
		                                    $('#name').focus();
		                                    return false;
		                                }
		                                else if(position.value=="")
		                                {
		                                    $('#position_req').show();
		                                    $('#position_req').parent('div').addClass('has-error');
		                                    $('#position').focus();
		                                    return false;
		                                }
		                                else if(department.value=="")
		                                {
		                                    $('#department_req').show();
		                                    $('#department_req').parent('div').addClass('has-error');
		                                    $('#department').focus();
		                                    return false;
		                                }
		                                else if(address.value=="")
		                                {
		                                    $('#address_req').show();
		                                    $('#address_req').parent('div').addClass('has-error');
		                                    $('#address').focus();
		                                    return false;
		                                }
		                                else if(tel.value=="")
		                                {
		                                    $('#tel_req').show();
		                                    $('#tel_req').parent('div').addClass('has-error');
		                                    $('#tel').focus();
		                                    return false;
		                                }
		                                else if(mobile.value=="")
		                                {
		                                    $('#mobile_req').show();
		                                    $('#mobile_req').parent('div').addClass('has-error');
		                                    $('#mobile').focus();
		                                    return false;
		                                }
		                                else if(password.value!="" && cpassword.value=="")
		                                {
		                                    $('#confirm_password_req').show();
		                                    $('#confirm_password_req').parent('div').addClass('has-error');
		                                    $('#cpassword').focus();
		                                    return false;
		                                }
		                                else if(password.value!=cpassword.value)
		                                {
		                                    $('#confirm_password_inc').show();
		                                    $('#confirm_password_inc').parent('div').addClass('has-error');
		                                    $('#cpassword').focus();
		                                    return false;
		                                }
		                            }
		                        }
		                    </script>
                            <form name="add_edit" onsubmit="return check_data();" method="post" enctype="multipart/form-data">
                                <h3 class="text-center"><img src="{$image_url}theme/default/assets/images/icon-profile.png" width="60"> แก้ไขรายละเอียดสมาชิก</h3>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="company" name="company" class="form-control" placeholder="บริษัท/องค์กร*" value="{$member.company}">
                                    <label>บริษัท/องค์กร*</label>
                                    <p id="company_req" class="required">กรุณากรอกชื่อบริษัท/องค์กร</p>
                                </div>
                                <div class="form-group form-project no-padding has-label-upload">
                                    <img src="{$member.logo}" style="max-height: 100px;">
                                </div>
                                <div class="form-group form-project no-padding has-label-upload">
                                    <label>อัพโหลดโลโก้* <img src="{$image_url}theme/default/assets/images/icon-upload.png"></label>
                                    <input type="file" id="logo" name="logo" accept="image/gif, image/jpeg, image/png"  class="form-control">
                                    <p id="logo_req" class="required">กรุณาอัพโหลดโลโก้</p>
                                </div>
                                {$member_company_type_group="`$member.company_type_id`-`$member.company_group_id`"}
                                <div class="form-group is-hide-label">
                                    <label>ประเภทองค์กร*</label>
                                    <select id="company_type" name="company_type" class="form-control">
                                        <option value="">ประเภทองค์กร*</option>
                                        {foreach $company_type as $company_type_item}
                                            <optgroup label="{$company_type_item.name}">
                                                {foreach $company_type_item.item as $company_group_item}
                                                    {$company_type_group="`$company_group_item.company_type_id`-`$company_group_item.id`"}
                                                    <option value="{$company_group_item.company_type_id}-{$company_group_item.id}"{if $member_company_type_group == $company_type_group} selected="selected"{/if}>{$company_group_item.name}</option>
                                                {/foreach}
                                            </optgroup>
                                        {/foreach}
                                    </select>
                                    <p id="company_type_req" class="required">กรุณาเลือกประเภทองค์กร</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="ผู้สมัคร ชื่อ นามสกุล*" value="{$member.name}">
                                    <label>ผู้สมัคร ชื่อ นามสกุล*</label>
                                    <p id="name_req" class="required">กรุณากรอกชื่อ นามสกุล</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" id="position" name="position" class="form-control" placeholder="ตำแหน่ง*" value="{$member.position}">
                                    <label>ตำแหน่ง*</label>
                                    <p id="position_req" class="required is-hide-label">กรุณากรอกตำแหน่ง</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" name="department" id="department" class="form-control" placeholder="ฝ่ายงาน*" value="{$member.department}">
                                    <label>ฝ่ายงาน*</label>
                                    <p id="department_req" class="required">กรุณากรอกฝ่ายงาน</p>
                                </div>
                                <div class="form-group is-hide-label">
                                	<input type="text" name="address" id="address" class="form-control" placeholder="ที่อยู่*" value="{$member.address}">
                                    <label>ที่อยู่*</label>
                                    <p id="address_req" class="required">กรุณากรอกที่อยู่</p>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group is-hide-label">
                                            <input type="text" id="tel" name="tel" class="form-control" placeholder="โทรศัพท์*" value="{$member.tel}">
                                            <label>โทรศัพท์*</label>
                                            <p id="tel_req" class="required">กรุณากรอกหมายเลขโทรศัพท์</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group is-hide-label">
                                            <input type="text" id="mobile" name="mobile" class="form-control" placeholder="โทรศัพท์มือถือ*" value="{$member.mobile}">
                                            <label>โทรศัพท์มือถือ*</label>
                                            <p id="mobile_req" class="required">กรุณากรอกหมายเลขโทรศัพท์มือถือ</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" class="form-control" placeholder="อีเมล*" readonly="readonly" value="{$member.email}">
                                    <label>อีเมล*</label>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="text" class="form-control" placeholder="ชื่อผู้ใช้งาน (Username)*" readonly="readonly" value="{$member.username}">
                                    <label>ชื่อผู้ใช้งาน (Username)*</label>
                                </div>
                                <code>หากต้องการเปลี่ยนรหัสผ่าน กรุณากรอกรหัสผ่านและยืนยันรหัสผ่าน</code>
                                <div class="form-group is-hide-label">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="รหัสผ่าน (Password)">
                                    <label>รหัสผ่าน (Password)</label>
                                    <p id="password_req" class="required">กรุณากรอกรหัสผ่าน</p>
                                </div>
                                <div class="form-group is-hide-label">
                                    <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="ยืนยันรหัสผ่าน">
                                    <label>ยืนยันรหัสผ่าน</label>
                                    <p id="confirm_password_req" class="required">กรุณายืนยันรหัสผ่าน</p>
                                    <p id="confirm_password_inc" class="required">กรุณายืนยันรหัสผ่านให้ถูกต้อง</p>
                                </div>
                                <br/><br/>
                                <div class="form-group text-center">
                                    <div class="checkbox">
                                        <input type="checkbox" id="input-remember" name="is_set_staff" value="Y"{if $member.is_set_staff == 'Y'} checked="checked"{/if}> 
                                        <label for="input-remember">คุณเป็นพนักงานของตลาดหลักทรัพย์แห่งประเทศไทย</label>
                                    </div>
                                </div>
                                <br/><br/>
                                <div class="form-group ">
                                    <div class="btn-box text-center">
                                        <button type="submit" class="btn btn-yellow">Save</button>
                                        <button type="button" class="btn btn-grey" onclick="window.history.back();">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}