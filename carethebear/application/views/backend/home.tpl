{extends file="backend/layout.tpl"}
{block name=meta_title}Home - {$site_name} : {$company_name}{/block}
{block name=body}
    <section class="content-header">
        <h1>
            {$site_name}
            <small>{$company_name}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
            <li class="active">Home</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-flag"></i>&nbsp; Member & Activity / Project</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/activity" class="btn btn-app">
                            <i class="fa fa-flag"></i> Activity
                        </a>
                        <a href="{$base_url}backend/project" class="btn btn-app">
                            <i class="fa fa-calendar"></i> Project
                        </a>
                        <a href="{$base_url}backend/member" class="btn btn-app">
                            <i class="fa fa-user"></i> Member
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp; Content</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/banner" class="btn btn-app">
                            <i class="fa fa-image"></i> Banner
                        </a>
                        <a href="{$base_url}backend/news" class="btn btn-app">
                            <i class="fa fa-newspaper-o"></i> News
                        </a>
                        <a href="{$base_url}backend/article" class="btn btn-app">
                            <i class="fa fa-file-text-o"></i> Article
                        </a>
                        <a href="{$base_url}backend/tips" class="btn btn-app">
                            <i class="fa fa-file-text-o"></i> Tips
                        </a>
                        <a href="{$base_url}backend/content" class="btn btn-app">
                            <i class="fa fa-file-text"></i> Content
                        </a>
                        <a href="{$base_url}backend/company" class="btn btn-app">
                            <i class="fa fa-building"></i> Company
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-file-archive-o"></i>&nbsp; Document</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/document" class="btn btn-app">
                            <i class="fa fa-file-archive-o"></i> Document
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-pie-chart"></i>&nbsp; Report</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/report" class="btn btn-app">
                            <i class="fa fa-pie-chart"></i> CF by All for Event
                        </a>
                        <a href="{$base_url}backend/report/all_project" class="btn btn-app">
                            <i class="fa fa-pie-chart"></i> CF by All for Project
                        </a>
                        <a href="{$base_url}backend/report/member" class="btn btn-app">
                            <i class="fa fa-user"></i> CF by Member
                        </a>
                        <a href="{$base_url}backend/report/activity" class="btn btn-app">
                            <i class="fa fa-flag"></i> CF by Activity
                        </a>
                        <a href="{$base_url}backend/report/project" class="btn btn-app">
                            <i class="fa fa-calendar"></i> CF by Project
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-gears"></i>&nbsp; Settings</h3>
                    </div>
                    <div class="box-body">
                        <a href="{$base_url}backend/company_type" class="btn btn-app">
                            <i class="fa fa-th-large"></i> Company Type
                        </a>
                        <a href="{$base_url}backend/company_group" class="btn btn-app">
                            <i class="fa fa-th"></i> Company Group
                        </a>
                        <a href="{$base_url}backend/activity_type" class="btn btn-app">
                            <i class="fa fa-th-list"></i> Activity Type
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {if $authen->is_developer == true || $authen->is_admin == true}
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-users"></i>&nbsp; Users</h3>
                        </div>
                        <div class="box-body">
                            <a href="{$base_url}backend/user" class="btn btn-app">
                                <i class="fa fa-users"></i> Users
                            </a>
                            {if $authen->is_developer == true}
                                <a href="{$base_url}backend/role" class="btn btn-app">
                                    <i class="fa fa-shield"></i> Role
                                </a>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    </section>
{/block}