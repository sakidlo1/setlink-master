{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<script src="{$image_url}tinymce/tinymce.min.js"></script>
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">{$page_name}</h3>
						<form role="form" class="form-horizontal" name="add_edit" method="post" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">Logo <font color="red">*</font></label>
									<div class="col-sm-6">
										<img id="image_display" src="{$item.logo}" class="img-responsive" style="width: 200px;" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Company <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.company|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Type <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.company_type}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Group <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.company_group}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Name <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.name|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Position <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.position|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Department <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.department|escape}">
									</div>
								</div>
								<hr />
								<h4 class="box-title">Activity</h4>
								<hr />
								<div class="form-group">
									<label class="col-sm-3 control-label">Activity <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.activity_count|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 1 : เดินทาง </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_1|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 2 : ลดใช้กระดาษ/พลาสติก </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_2|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 3 : ลดโฟม </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_3|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 4 : ลดใช้พลังงาน </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_4|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 5 : ขยะจากงาน </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_5|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 6 : ตกแต่ง </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_6|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Total </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_total|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tree </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{($item.cf_care_total/9)|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<hr />
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<div class="table-responsive">
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Date / Time</th>
														<th>Name</th>
														<th>Attendant</th>
				                                        <th>Care 1 : เดินทาง</th>
				                                        <th>Care 2 : ลดใช้กระดาษ/พลาสติก</th>
				                                        <th>Care 3 : ลดโฟม</th>
				                                        <th>Care 4 : ลดใช้พลังงาน</th>
				                                        <th>Care 5 : ขยะจากงาน</th>
				                                        <th>Care 6 : ตกแต่ง</th>
				                                        <th>Total</th>
														<th>Detail</th>
													</tr>
												</thead>
												<tbody>
													{if $item.activity|count > 0}
														{foreach $item.activity as $activity_item}
														<tr>
															<td>{$activity_item.activity_date|date_format:"%e %B %Y"} ({$activity_item.start_on|substr:0:5} - {$activity_item.end_on|substr:0:5})</td>
															<td>{$activity_item.name}</td>
															<td>{$activity_item.attendant|number_format:0:".":","}</td>
															<td>{$activity_item.cf_care_1|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_2|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_3|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_4|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_5|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_6|number_format:2:".":","}</td>
															<td>{$activity_item.cf_care_total|number_format:2:".":","}</td>
															<td><a href="{$base_url}backend/{$page}/activity_detail/{$activity_item.id}"><i class="fa fa-search"></i></a></td>
														</tr>
														{/foreach}
													{else}
														<tr>
															<td class="text-center" colspan="11">No Activity</td>
														</tr>
													{/if}
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<hr />
								<h4 class="box-title">Project</h4>
								<hr />
								<div class="form-group">
									<label class="col-sm-3 control-label">Project <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.project_count|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 1 : เดินทาง </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.project_cf_care_1|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.project_cf_care_2|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 3 : ลดใช้กระดาษ </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.project_cf_care_3|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Total </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.project_cf_care_total|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tree </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{($item.project_cf_care_total/9)|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<hr />
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<div class="table-responsive">
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Start</th>
														<th>End</th>
														<th>Name</th>
														<th>Attendant</th>
				                                        <th>Care 1 : เดินทาง</th>
				                                        <th>Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ</th>
				                                        <th>Care 3 : ลดใช้กระดาษ</th>
				                                        <th>Total</th>
														<th>Detail</th>
													</tr>
												</thead>
												<tbody>
													{if $item.project|count > 0}
														{foreach $item.project as $project_item}
														<tr>
															<td>{$project_item.start_date|date_format:"%e %B %Y"} {$project_item.start_on|substr:0:5}</td>
															<td>{$project_item.end_date|date_format:"%e %B %Y"} {$project_item.end_on|substr:0:5}</td>
															<td>{$project_item.name}</td>
															<td>{$project_item.attendant|number_format:0:".":","}</td>
															<td>{$project_item.cf_care_1|number_format:2:".":","}</td>
															<td>{$project_item.cf_care_2|number_format:2:".":","}</td>
															<td>{$project_item.cf_care_3|number_format:2:".":","}</td>
															<td>{$project_item.cf_care_total|number_format:2:".":","}</td>
															<td><a href="{$base_url}backend/{$page}/project_detail/{$project_item.id}"><i class="fa fa-search"></i></a></td>
														</tr>
														{/foreach}
													{else}
														<tr>
															<td class="text-center" colspan="9">No Project</td>
														</tr>
													{/if}
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="form-group">
									<hr />
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">&nbsp;</label>
									<div class="col-sm-6">
										<a href="javascript:history.back();" class="btn btn-default">Back</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}