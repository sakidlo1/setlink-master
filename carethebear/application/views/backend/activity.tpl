{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				{if $success_msg != ""}
				<div class="alert alert-success">
					{$success_msg}
				</div>
				{/if}
				{if $error_msg != ""}
				<div class="alert alert-danger">
					{$error_msg}
				</div>
				{/if}
				<div class="box">
		            <div class="box-header">
		            	
		            </div>
		            <div class="box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-inline">
		                        	<div class="form-group">
                                        <label for="fFrom">From : </label>
                                        <input class="form-control" id="fFrom" type="text" size="10" />
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="fTo">To : </label>
                                        <input class="form-control" id="fTo" type="text" size="10" />
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
		                            <div class="form-group">
		                                <label for="fName">Keyword : </label>
		                                <input class="form-control" id="fName" type="text" size="10" />
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
                                    <div class="form-group">
		                                <label for="fCompany">Company : </label>
		                                <select class="form-control" id="fCompany">
		                                    <option value="">[ Company ]</option>
		                                    {foreach $company as $company_item}	
		                                    	<option value="{$company_item.id}">{$company_item.companyTh}</option>
		                                    {/foreach}
		                                </select>
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
		                            <div class="form-group">
		                                <label for="fStatus">Status : </label>
		                                <select class="form-control" id="fStatus">
		                                    <option value="">[ Status ]</option>
		                                    <option value="Y">Open</option>
		                                    <option value="N">Close</option>
		                                </select>
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
		                            <div class="form-group">
		                                <input class="btn btn-default" type="button" value="Search" id="fSearch" />
		                            </div>
		                        </div>
		                    </div>
		                </div>
						<div class="table-responsive">
							<table id="data-tables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Date</th>
										<th>Start</th>
										<th>End</th>
										<th>Image</th>
										<th>Name</th>
										<th>Place</th>
										<th>Company</th>
										<th>Attendant</th>
										<th>Status</th>
										<th>Edit</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}
{block name="script"}
	 <script>

	 	{literal}
            function nl2br (str, is_xhtml) {   
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
            }

            function input_format(value, dec)
            {
                value = value.replace(/\,/g,"");
                if(value != "")
                {
                    if(dec == 0)
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                    }
                    else
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    }
                }
                else
                {
                    return '';
                }
            }
        {/literal}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "desc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                'dom': '<"top"i>rt<"bottom"lp><"clear">',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/load_data{$get_params}',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'activity_date',
                        'name': 'Date',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(data).format('D MMM YYYY');
                        }
                    },
                    {
                        'data': 'start_on',
                        'name': 'Start',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(full.activity_date + ' ' + data).format('HH:mm');
                        }
                    },
                    {
                        'data': 'end_on',
                        'name': 'End',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(full.activity_date + ' ' + data).format('HH:mm');
                        }
                    },
                    {
                        'data': 'image_1',
                        'name': 'Image',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<img src="' + data + '" onerror="this.src=\'{$image_url}theme/default/no_img.png\';" style="max-width: 150px; max-height: 150px;" />';
                        }
                    },
                    {
                        'data': 'name',
                        'name': 'Name',
                        'autoWidth': true
                    },
                    {
                        'data': 'place',
                        'name': 'Place',
                        'autoWidth': true
                    },
                    {
                        'data': 'company',
                        'name': 'Company',
                        'autoWidth': true
                    },
                    {
                        'data': 'attendant',
                        'name': 'Attendant',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 0);
                        }
                    },
                    {
                        'data': 'status',
                        'name': 'Status',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return ((data == 'Y') ? '<span class="label label-success">Open</span>' : '<span class="label label-warning">Close</span>');
                        }
                    },
                    {
                        'data': 'id',
                        'name': 'Edit',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}backend/{$page}/edit/' + data + '{$get_params}"><i class="fa fa-edit"></i></a> / <a href="{$base_url}backend/{$page}/delete/' + data + '{$get_params}" onclick="return confirm(\'Do you want to delete data ?\');"><i class="fa fa-trash"></i></a>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fFrom').val().trim());
                oTable.columns(1).search($('#fTo').val().trim());
                oTable.columns(2).search($('#fName').val().trim());
                oTable.columns(3).search($('#fCompany').val().trim());
                oTable.columns(4).search($('#fStatus').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}