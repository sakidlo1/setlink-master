{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
		            <div class="box-header">
		            	
		            </div>
		            <div class="box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-inline">
		                        	<div class="form-group">
                                        <label for="fFrom">From : </label>
                                        <input class="form-control" id="fFrom" type="text" size="10" />
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="form-group">
                                        <label for="fTo">To : </label>
                                        <input class="form-control" id="fTo" type="text" size="10" />
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
		                            <div class="form-group">
		                                <label for="fName">Keyword : </label>
		                                <input class="form-control" id="fName" type="text" size="10" />
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
		                            <div class="form-group">
		                                <input class="btn btn-default" type="button" value="Search" id="fSearch" />
                                        <input class="btn btn-success" type="button" value="Export" onclick="window.location='{$base_url}backend/{$page}/{$sub_page}?fFrom='+$('#fFrom').val()+'&fTo='+$('#fTo').val()+'&fName='+$('#fName').val()+'&export=1';" />
		                            </div>
		                        </div>
		                    </div>
		                </div>
						<div class="table-responsive">
							<table id="data-tables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Start</th>
										<th>End</th>
										<th>Name</th>
										<th>Company</th>
										<th>Attendant</th>
                                        <th>Care 1 : เดินทาง</th>
                                        <th>Care 2 : ลดใช้ถุงพลาสติก/ถุงกระดาษ</th>
                                        <th>Care 3 : ลดใช้กระดาษ</th>
                                        <th>Total</th>
										<th>Detail</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}
{block name="script"}
	 <script>

	 	{literal}
            function nl2br (str, is_xhtml) {   
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
            }

            function input_format(value, dec)
            {
                value = value.replace(/\,/g,"");
                if(value != "")
                {
                    if(dec == 0)
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                    }
                    else
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    }
                }
                else
                {
                    return '';
                }
            }
        {/literal}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "desc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                'dom': '<"top"i>rt<"bottom"lp><"clear">',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/project_load_data{$get_params}',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'start_date',
                        'name': 'Start',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(data).format('D MMM YYYY') + ' ' + moment(data + ' ' + full.start_on).format('HH:mm');
                        }
                    },
                    {
                        'data': 'end_date',
                        'name': 'End',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(data).format('D MMM YYYY') + ' ' + moment(data + ' ' + full.end_on).format('HH:mm');
                        }
                    },
                    {
                        'data': 'name',
                        'name': 'Name',
                        'autoWidth': true
                    },
                    {
                        'data': 'company',
                        'name': 'Company',
                        'autoWidth': true
                    },
                    {
                        'data': 'attendant',
                        'name': 'Attendant',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 0);
                        }
                    },
                    {
                        'data': 'cf_care_1',
                        'name': 'Care 1',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'cf_care_2',
                        'name': 'Care 2',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'cf_care_3',
                        'name': 'Care 3',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'cf_care_total',
                        'name': 'Total',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'id',
                        'name': 'Detail',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}backend/{$page}/project_detail/' + data + '{$get_params}"><i class="fa fa-search"></i></a>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fFrom').val().trim());
                oTable.columns(1).search($('#fTo').val().trim());
                oTable.columns(2).search($('#fName').val().trim());
                oTable.draw();
            });

            $('#fFrom').datepicker({
                format: 'yyyy-mm-dd'
            });

            $('#fTo').datepicker({
                format: 'yyyy-mm-dd'
            });

        });
    </script>
{/block}