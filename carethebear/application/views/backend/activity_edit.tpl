{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<script src="{$image_url}tinymce/tinymce.min.js"></script>
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Edit {$page_name}</h3>
						<script>
							function check_data()
							{
								$('#status_req').hide();
								$('.has-error').removeClass('has-error');
								
								with(document.add_edit)
								{
									if(status[0].checked==false && status[1].checked==false)
									{
										$('#status_req').show();
										$('#status_req').parent('div').addClass('has-error');
										return false;
									}
								}
							}
						</script>
						<form role="form" class="form-horizontal" name="add_edit" method="post" onsubmit="return check_data();" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">Company <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.company|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Name <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="name" id="name" value="{$item.name|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Place <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="place" id="place" value="{$item.place|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Date / Time <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.activity_date|date_format:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Attendant <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="attendant" id="attendant" value="{$item.attendant|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Description <font color="red">*</font></label>
									<div class="col-sm-6">
										<textarea class="form-control" rows="10" name="description" id="description">{$item.description}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Image 1 <font color="red">*</font></label>
									<div class="col-sm-6">
										<img id="image_display" src="{$item.image_1}" class="img-responsive" style="width: 200px;" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Image 2 </label>
									<div class="col-sm-6">
										<img id="image_display" src="{$item.image_2}" class="img-responsive" style="width: 200px;" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Image 3 </label>
									<div class="col-sm-6">
										<img id="image_display" src="{$item.image_3}" class="img-responsive" style="width: 200px;" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Video Embed </label>
									<div class="col-sm-6">
										<textarea class="form-control" name="vdo" id="vdo">{$item.vdo}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Status <font color="red">*</font></label>
									<div class="col-sm-6">
										<label class="radio-inline">
											<input type="radio" value="Y" id="status" name="status"{if $item.status == 'Y'} checked="checked"{/if}> Open
										</label>
										<label class="radio-inline">
											<input type="radio" value="N" id="status" name="status"{if $item.status == 'N'} checked="checked"{/if}> Close
										</label>
										<br />
										<label id="status_req" for="inputError" style="display: none;" class="control-label">Please select Status.</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">&nbsp;</label>
									<div class="col-sm-6">
										<button class="btn btn-success" type="submit" name="save" id="save" value="save">Save</button>
										<a href="javascript:history.back();" class="btn btn-default">Cancel</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}