{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<script src="{$image_url}tinymce/tinymce.min.js"></script>
	<script src="{$image_url}theme/default/assets/js/chosen.jquery.min.js"></script>

	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Edit {$page_name}</h3>
						<script>
							var email_exists = true;
		                    var username_exists = true;


								function check_email_exists() {
									if ($('#email').val() !== "") {
										$.ajax({
											dataType: 'json',
											type: 'POST',
											url: '{$base_url}backend/member/check_email_exists/{$item.id}',
											data: {
												email: $('#email').val(),
												id: {$item.id}
											},
											success: function(data) { 
		                                        if(data.status == false) {
		                                            email_exists = false;
		                                            check_username_exists();
		                                        } else {
		                                            return check_data();
		                                        }
		                                    },
										});
										return false;
									} else {
										return check_data();
									}
								}

								function check_username_exists() {
		                            if($('#username').val() != "") {
		                                $.ajax({
		                                    dataType: 'json',
		                                    type: 'POST',
		                                    url: '{$base_url}backend/member/check_username_exists/{$item.id}',
											data: {
												username: $('#username').val(),
												id: {$item.id}
											},
		                                    success: function(data) { 
		                                        if(data.status == false) {
		                                            username_exists = false;
		                                            if(check_data() != false) {
		                                                document.add_edit.submit();
		                                            }
		                                        } else {
		                                            return check_data();
		                                        }
		                                    },
		                                });
		                                return false;
		                            } else {
		                                return check_data();
		                            }
		                        }
		                        
							function check_data()
							{
								$('#email_exists').hide();
								$('#username_exists').hide();
								$('#status_req').hide();
								$('.has-error').removeClass('has-error');
								
								with(document.add_edit)
								{
									if(email_exists == true)
									{
										$('#email_exists').show();
										$('#email_exists').parent('div').addClass('has-error');
										$('#email').focus();
										return false;
									}
									else if(username_exists == true)
									{
										$('#username_exists').show();
		                                $('#username_exists').parent('div').addClass('has-error');
		                                $('#username').focus();
		                                return false;
									}
									else if(status[0].checked==false && status[1].checked==false)
									{
										$('#status_req').show();
										$('#status_req').parent('div').addClass('has-error');
										return false;
									}
								}
							}

							jQuery(function ($) {
								$('.chosen-select').chosen({
									width: "100%"
								});

								$('.chosen-select').on('change',function () {
									var name = $(this).find(':selected').val();
									console.log(name);
									var nameEn = $(this).find(':selected').data('en');
									var code = $(this).find(':selected').data('code');
									if(nameEn === '' || nameEn === undefined) {
										nameEn = "ไม่ระบุ"
									}
									if(code === '' || code === undefined) {
										code = "ไม่ระบุ"
									}
									$('#company_name').val(name);
									$('#company_name_en').val(nameEn);

									$('#company_name_en').trigger('updated');

									$('#company_short_name').val(code);
								})
							});



						</script>
						<form role="form" class="form-horizontal" name="add_edit" method="post" onsubmit="return check_email_exists();" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">Logo <font color="red">*</font></label>
									<div class="col-sm-6">
										<img id="image_display" src="{$item.logo}" class="img-responsive" style="width: 200px;" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Company Chosen <font color="red">*</font></label>
									<div class="col-sm-6">
										<select name="company_name_chosen" class="chosen-select" id="company_name_chosen" readonly="">
											<option value="">--เลือก--</option>
											{foreach $company_lists as $company_item}
												<option class="" data-code="{$company_item['code']}" data-en="{$company_item['name_en']}" value="{$company_item['name_th']}">
													<span class="-text">{$company_item['name_th']}</span>
												</option>
											{/foreach}
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Company <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="company_name" value="{$item.company|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Company En <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="company_name_en" value="{$item.company_en|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Company Code (Symbol) <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="company_short_name" value="{$item.company_code|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Type <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" value="{$item.company_type}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Group <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" value="{$item.company_group}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Name <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="name" id="name" value="{$item.name|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Position <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="position" id="position" value="{$item.position|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Department <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="department" id="department" value="{$item.department|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Address <font color="red">*</font></label>
									<div class="col-sm-6">
										<textarea class="form-control" name="address" id="address">{$item.address}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tel <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="tel" id="tel" value="{$item.tel|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Mobile <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="mobile" id="mobile" value="{$item.mobile|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="email" id="email" value="{$item.email|escape}">
										<label id="email_exists" for="inputError" style="display: none;" class="control-label">อีเมลนี้มีอยู่ในระบบแล้ว กรุณาใช้อีเมลอื่น</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Username <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" name="username" id="username" value="{$item.username|escape}">
										<label id="username_exists" for="inputError" style="display: none;" class="control-label">ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว กรุณาใช้ชื่อผู้ใช้งานอื่น</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">is SET Staff ?</label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{if $item.is_set_staff == 'Y'}Yes{else}No{/if}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Status <font color="red">*</font></label>
									<div class="col-sm-6">
										<label class="radio-inline">
											<input type="radio" value="Y" id="status" name="status"{if $item.status == 'Y'} checked="checked"{/if}> Open
										</label>
										<label class="radio-inline">
											<input type="radio" value="N" id="status" name="status"{if $item.status == 'N'} checked="checked"{/if}> Close
										</label>
										<br />
										<label id="status_req" for="inputError" style="display: none;" class="control-label">Please select Status.</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">&nbsp;</label>
									<div class="col-sm-6">
										<input type="hidden" name="save" value="save">
										<button class="btn btn-success" type="submit" id="save">Save</button>
										<a href="javascript:history.back();" class="btn btn-default">Cancel</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}