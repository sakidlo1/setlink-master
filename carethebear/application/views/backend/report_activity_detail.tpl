{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<script src="{$image_url}tinymce/tinymce.min.js"></script>
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">{$page_name}</h3>
						<form role="form" class="form-horizontal" name="add_edit" method="post" enctype="multipart/form-data">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">Company <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.company|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Name <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.name|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Place <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.place|escape}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Date / Time <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.activity_date|date_format:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Attendant <font color="red">*</font></label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.attendant|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 1 : เดินทาง </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_1|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 2 : ลดใช้กระดาษ/พลาสติก </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_2|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 3 : ลดโฟม </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_3|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 4 : ลดใช้พลังงาน </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_4|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 5 : ขยะจากงาน </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_5|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Care 6 : ตกแต่ง </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_6|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Total </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{$item.cf_care_total|number_format:2:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tree </label>
									<div class="col-sm-6">
										<input class="form-control" type="text" readonly="readonly" value="{($item.cf_care_total/9)|number_format:0:".":","}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">&nbsp;</label>
									<div class="col-sm-6">
										<a href="javascript:history.back();" class="btn btn-default">Back</a>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}