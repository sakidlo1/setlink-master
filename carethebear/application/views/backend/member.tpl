{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				{if $success_msg != ""}
				<div class="alert alert-success">
					{$success_msg}
				</div>
				{/if}
				{if $error_msg != ""}
				<div class="alert alert-danger">
					{$error_msg}
				</div>
				{/if}
				<div class="box">
		            <div class="box-header">
		            	
		            </div>
		            <div class="box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-inline">
		                            <div class="form-group">
		                                <label for="fName">Keyword : </label>
		                                <input class="form-control" id="fName" type="text" size="10" />
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
                                    <select class="form-control" id="fCompany">
		                                    <option value="">[ Company ]</option>
		                                    {foreach $company as $company_item}	
		                                    	<option value="{$company_item.id}">{$company_item.companyTh}</option>
		                                    {/foreach}
		                                </select>
		                            <div class="form-group">
		                                <label for="fStatus">Status : </label>
		                                <select class="form-control" id="fStatus">
		                                    <option value="">[ Status ]</option>
		                                    <option value="Y">Open</option>
		                                    <option value="N">Close</option>
		                                </select>
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
		                            <div class="form-group">
		                                <input class="btn btn-default" type="button" value="Search" id="fSearch" />
		                            </div>
		                        </div>
		                    </div>
		                </div>
						<div class="table-responsive">
							<table id="data-tables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Date / Time</th>
										<th>Logo</th>
										<th>Name</th>
										<th>Company</th>
                                        <th>Company En</th>
                                        <th>Company Code</th>
                                        <th>Type</th>
                                        <th>Group</th>
										<th>Username</th>
										<th>Email</th>
										<th>Tel</th>
										<th>Mobile</th>
										<th>Status</th>
                                        <th>Active</th>
										<th>Edit</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}
{block name="script"}
	 <script>
        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "desc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                'dom': '<"top"i>rt<"bottom"lp><"clear">',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/load_data{$get_params}',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                	{
                        'data': 'created_on',
                        'name': 'Date / Time',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return moment(data).format('D MMM YYYY<br />(HH:mm)');
                        }
                    },
                    {
                        'data': 'logo',
                        'name': 'Logo',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<img src="' + data + '" onerror="this.src=\'{$image_url}theme/default/no_img.png\';" style="max-width: 150px; max-height: 150px;" />';
                        }
                    },
                    {
                        'data': 'name',
                        'name': 'Name',
                        'autoWidth': true
                    },
                    {
                        'data': 'company',
                        'name': 'Company',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_en',
                        'name': 'Company En',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_code',
                        'name': 'Code',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_type',
                        'name': 'Type',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_group',
                        'name': 'Group',
                        'autoWidth': true
                    },
                    {
                        'data': 'username',
                        'name': 'Username',
                        'autoWidth': true
                    },
                    {
                        'data': 'email',
                        'name': 'Email',
                        'autoWidth': true
                    },
                    {
                        'data': 'tel',
                        'name': 'Tel',
                        'autoWidth': true
                    },
                    {
                        'data': 'mobile',
                        'name': 'Mobile',
                        'autoWidth': true
                    },
                    {
                        'data': 'status',
                        'name': 'Status',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            var str  = ((data == 'Y') ? '<span class="label label-success">Open</span>' : '<span class="label label-warning">Close</span>');
                            if(full.company_other === '1') {
                                str = str + '<span class="label label-danger">Other</span>'
                            }
                            return str;
                        }
                    },
                    {
                        'data': 'active_status',
                        'name': 'Active',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return ((data == 'Yes') ? '<span class="label label-success">Is Active</span>' : '<span class="label label-warning">No Active</span>');
                        }
                    },
                    {
                        'data': 'id',
                        'name': 'Edit',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            var str = '<a href="{$base_url}backend/{$page}/edit/' + data + '{$get_params}"><i class="fa fa-edit"></i></a> / <a href="{$base_url}backend/{$page}/delete/' + data + '{$get_params}" onclick="return confirm(\'Do you want to delete data ?\');"><i class="fa fa-trash"></i></a>';
                            if(full.company_other === '1') {
                                str = str + ' / <a href="{$base_url}backend/{$page}/approve/' + data + '{$get_params}" onclick="return confirm(\'คุณต้องการอนุมัติบริษัทนี้ใช่ไหม ?\');">Approve</a>'
                            }
                            return str;
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.columns(1).search($('#fCompany').val().trim());
                oTable.columns(2).search($('#fStatus').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}