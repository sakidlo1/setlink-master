{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
		            <div class="box-header">
		            	
		            </div>
		            <div class="box-body">
		                <div class="row">
		                    <div class="col-md-12">
		                        <div class="form-inline">
		                            <div class="form-group">
		                                <label for="fName">Keyword : </label>
		                                <input class="form-control" id="fName" type="text" size="10" />
		                                &nbsp;&nbsp;&nbsp;
		                            </div>
		                            <div class="form-group">
		                                <input class="btn btn-default" type="button" value="Search" id="fSearch" />
                                        <input class="btn btn-success" type="button" value="Export" onclick="window.location='{$base_url}backend/{$page}/{$sub_page}?fName='+$('#fName').val()+'&export=1';" />
		                            </div>
		                        </div>
		                    </div>
		                </div>
						<div class="table-responsive">
							<table id="data-tables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>ID</th>
										<th>Logo</th>
                                        <th>Company</th>
                                        <th>Type</th>
                                        <th>Group</th>
                                        <th>Name</th>
                                        <th>Activity</th>
                                        <th>Total</th>
                                        <th>Project</th>
                                        <th>Total</th>
                                        <th>Summary</th>
                                        <th>Active</th>
										<th>Detail</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}
{block name="script"}
	 <script>

	 	{literal}
            function nl2br (str, is_xhtml) {   
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
            }

            function input_format(value, dec)
            {
				if(value == null)
				{
					return '';
				}
                value = value.replace(/\,/g,"");
                if(value != "")
                {
                    if(dec == 0)
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                    }
                    else
                    {
                        return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                    }
                }
                else
                {
                    return '';
                }
            }
        {/literal}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "desc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                    { orderable: false, targets: -1 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                'dom': '<"top"i>rt<"bottom"lp><"clear">',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/member_load_data{$get_params}',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'id',
                        'name': 'ID',
                        'autoWidth': true
                    },
                    {
                        'data': 'logo',
                        'name': 'Logo',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<img src="' + data + '" onerror="this.src=\'{$image_url}theme/default/no_img.png\';" style="max-width: 150px; max-height: 150px;" />';
                        }
                    },
                    {
                        'data': 'company',
                        'name': 'Company',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_type',
                        'name': 'Type',
                        'autoWidth': true
                    },
                    {
                        'data': 'company_group',
                        'name': 'Group',
                        'autoWidth': true
                    },
                    {
                        'data': 'name',
                        'name': 'Name',
                        'autoWidth': true
                    },
                    {
                        'data': 'activity_count',
                        'name': 'Activity',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 0);
                        }
                    },
                    {
                        'data': 'cf_care_total',
                        'name': 'Activity Total',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'project_count',
                        'name': 'Project',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 0);
                        }
                    },
                    {
                        'data': 'project_cf_care_total',
                        'name': 'Project Total',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'total',
                        'name': 'Total',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return input_format(data, 2);
                        }
                    },
                    {
                        'data': 'active',
                        'name': 'Active',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return ((data == 'Yes') ? '<span class="label label-success">Is Active</span>' : '<span class="label label-warning">No Active</span>');
                        }
                    },
                    {
                        'data': 'id',
                        'name': 'Detail',
                        'autoWidth': true,
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}backend/{$page}/member_detail/' + data + '{$get_params}"><i class="fa fa-search"></i></a>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}