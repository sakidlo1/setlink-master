{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} Management - {$site_name}{/block}
{block name=body}
	<section class="content-header">
		<h1>
			{$page_name}
			<small>{$site_name}</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{$base_url}backend"><i class="fa fa-home"></i> {$site_name}</a></li>
			<li class="active">{$page_name}</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
		            <div class="box-header">
		            	<input class="btn btn-success pull-right" type="button" value="Export" onclick="window.location='{$base_url}backend/{$page}?export=1';" />
		            </div>
		            <div class="box-body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Year</th>
										<th>Care 1 : เดินทาง</th>
                                        <th>Care 2 : ลดใช้กระดาษ/พลาสติก</th>
                                        <th>Care 3 : ลดโฟม</th>
                                        <th>Care 4 : ลดใช้พลังงาน</th>
                                        <th>Care 5 : ขยะจากงาน</th>
                                        <th>Care 6 : ตกแต่ง</th>
                                        <th>Total</th>
                                        <th>Tree</th>
                                        <th>by quarter</th>
                                        <th>by Month</th>
									</tr>
								</thead>
                                <tbody>
                                    {if $list|count > 0}
                                        {foreach $list as $item}
                                        <tr>
                                            <td>{($item.year + 543)}</td>
                                            <td>{$item.cf_care_1|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_2|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_3|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_4|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_5|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_6|number_format:2:".":","}</td>
                                            <td>{$item.cf_care_total|number_format:2:".":","}</td>
                                            <td>{($item.cf_care_total/9)|number_format:0:".":","}</td>
                                            <td><a href="{$base_url}backend/{$page}/quarter/{$item.year}"><i class="fa fa-search"></i></a></td>
                                            <td><a href="{$base_url}backend/{$page}/month/{$item.year}"><i class="fa fa-search"></i></a></td>
                                        </tr>
                                        {/foreach}
                                    {else}
                                        <tr>
                                            <td class="text-center" colspan="11">No Data</td>
                                        </tr>
                                    {/if}
                                </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
{/block}