{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section>
        <div class="container">
            <h2 class="title">{$article_detail.name} <i class="icon-circle"></i></h2>
            <div class="article-content">
                {$article_detail.content}
            </div>
        </div>
    </section>
{/block}