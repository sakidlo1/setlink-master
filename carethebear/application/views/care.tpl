{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section class="care-page" data-aos="fade-up">
        <div class="container">
            <h2 class="title">รู้จัก 6 Cares <i class="icon-circle"></i></h2>
            <a id="care-1">
            {$care_1.content}
            <br /><br />
            <a id="care-2">
            {$care_2.content}
            <br /><br />
            <a id="care-3">
            {$care_3.content}
            <br /><br />
            <a id="care-4">
            {$care_4.content}
            <br /><br />
            <a id="care-5">
            {$care_5.content}
            <br /><br />
            <a id="care-6">
            {$care_6.content}
            <br /><br />
        </div>
    </section>
{/block}