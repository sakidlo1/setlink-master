{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=css}
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
    <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        
{/block}
{block name=js}
    <script src="{$image_url}theme/default/assets/js/jquery.min.js"></script>
	<script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
    
    

    <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
    <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
	<script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
    <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
	{* <script src="{$image_url}theme/default/climatecare/public/js/home-news.js"></script> *}
{/block}
{block name=body}
	{* <section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-7" >
                    <h2 class="title">{$home_content.name} <i class="icon-circle"></i></h2>
                    <div class="justify">
                        {$home_content.content}
                    </div>
                    <br/>
                    <div class="see-all">
                        <a href="{$base_url}about"><button type="button" class="btn btn-white font-black">ดูเพิ่มเติม</button></a>
                    </div>
                </div>
                <div class="col-sm-5">
                    <img class="home-img-bear" src="{$image_url}theme/default/assets/images/home-bare.png">
                </div>
            </div>
        </div>
    </section> *}
    <div id="banner_html"></div>
        <div data-aos="fade" class="banner-care-the-bear">
	        <div class="content-banner-left">
	            <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/banner/logo-bear.png" alt="" >
	            <span class="title-banner" >Care the Bear<br class="d-xl-block"> Change the Climate Change</span>
	            <span class="desc-banner" >ตลาดหลักทรัพย์ฯ สนับสนุนให้บริษัทจดทะเบียน<br class="d-xl-block">
	                และองค์กรที่สนใจร่วมปรับพฤติกรรมลดการปล่อย<br class="d-xl-block">
	                ก๊าซเรือนกระจกจากการจัดงานหรือทุกกิจกรรม<br class="d-xl-block">
	                ในรูปแบบ Online และ Onsite เช่น การประชุมผู้ถือหุ้น<br class="d-xl-block"> 
	                การประชุมผู้ถือหุ้นแบบ e-AGM การจัดงานอีเว้นต์ต่างๆ<br class="d-xl-block">
	                การจัดประชุมออนไลน์ การจัดกิจกรรมท่องเที่ยว<br class="d-xl-block">
	                การจัดงานมอบรางวัล การจัดงาน CSR เป็นต้น </span>
	        </div>
	        <div class="img-in-banner d-lg-block" >
	            <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/banner/img-in-desktop-banner.png" alt="">
	        </div>
	        <div class="img-in-banner d-lg-none" >
	            <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/banner/img-in-banner.png" alt="">
	        </div>
	    </div>

        <div class="main-sec-2">
	        <div class="wrap_care">
	            <div class="max_width_care flex center hide-xs">
	                <div class="left">
	                    <div class="cycle">
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/circle.svg" alt="ring" class="ring"/>
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" class="world"/>

	                        <div class="wrap_6">
	                            <a href="{$base_url}care#care-1" target="_blank" class="care">
                                    <img class="no_1" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/01.svg" alt="1" />
	                                <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-2" target="_blank" class="care">
	                                <img class="no_2" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/02.svg" alt="2" />
	                                <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-3" target="_blank" class="care">
	                                <img class="no_3" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/03.svg" alt="3" />
	                                <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-4" target="_blank" class="care">
	                                <img class="no_4" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/04.svg" alt="4" />
	                                <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-5" target="_blank" class="care">
	                                <img class="no_5" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/05.svg" alt="5" />
	                                <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
	                            </a>
	                            <a href="{$base_url}care#care-6" target="_blank" class="care">
	                                <img class="no_6" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/06.svg" alt="6" />
	                                <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	                <div class="right">
	                    <div class="care_txt">
	                        <div class="title f_bold">
	                            ทุกกิจกรรมสามารถใช้
	                            <p class="hilight f_bold hide-940">6 ปฏิบัติการ</p>
	                            <span class="hilight f_bold show-940"> 6 ปฏิบัติการ </span>
	                            ของ Care the Bear 
	                        </div>
	                        <div class="detail">
	                            มาออกแบบเพื่อประเมินวัดผลเป็นค่า
	                            การลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน 
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="show-xs">
	                <div class="flex center">
	                    <div class="left">
	                        <img src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/world_tha.svg" alt="world" />
	                    </div>
	                    <div class="right">
	                        <div class="title f_bold">ทุกกิจกรรมสามารถใช้<p class="hilight f_bold hide-940">หลักการ 6 Cares</p><span class="hilight f_bold show-940"> หลักการ 6 Cares </span>ของ Care the Bear </div>
	                        <div class="detail">มาออกแบบเพื่อประเมินวัดผลเป็นค่าการลดก๊าซเรือนกระจกและสร้างพฤติกรรมใหม่ให้กับพนักงานในองค์กรอย่างยั่งยืน</div>
	                    </div>
	                </div>
	                <div class="wrap_6 flex h-justify wrap">
                        <a href="{$base_url}care#care-1" target="_blank" class="care">
                        <img class="no_1" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/01.svg" alt="1" />
                        <div class="label"><p class="f_med no">01</p><span class="label_txt">รณรงค์ให้เดินทาง<br class="hide-xs">โดยรถสาธารณะ<br class="hide-xs">หรือเดินทางมาด้วยกัน</span></div>
                        </a>
                        <a href="{$base_url}care#care-2" target="_blank" class="care">
                        <img class="no_2" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/02.svg" alt="2" />
                        <div class="label"><p class="f_med no">02</p><span class="label_txt">ลดการใช้กระดาษ<br class="hide-xs">และพลาสติก</span></div>
                        </a>
                        <a href="{$base_url}care#care-3" target="_blank" class="care">
                        <img class="no_3" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/03.svg" alt="3" />
                        <div class="label"><p class="f_med no">03</p><span class="label_txt">งดการใช้โฟมจากบรรจุภัณฑ์<br class="hide-xs">หรือโฟมเพื่อตกแต่ง</span></div>
                        </a>
                        <a href="{$base_url}care#care-4" target="_blank" class="care">
                        <img class="no_4" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/04.svg" alt="4" />
                        <div class="label"><p class="f_med no">04</p><span class="label_txt">ลดการใช้พลังงานจาก<br class="hide-xs">อุปกรณ์ไฟฟ้า หรือเปลี่ยนไปใช้<br class="hide-xs">อุปกรณ์ประหยัดพลังงาน</span></div>
                        </a>
                        <a href="{$base_url}care#care-5" target="_blank" class="care">
                        <img class="no_5" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/05.svg" alt="5" />
                        <div class="label"><p class="f_med no">05</p><span class="label_txt">ออกแบบโดยใช้<br class="hide-xs">วัสดุตกแต่งที่นำ<br class="hide-xs">กลับมาใช้ใหม่ได้</span></div>
                        </a>
                        <a href="{$base_url}care#care-6" target="_blank" class="care">
                        <img class="no_6" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-2/06.svg" alt="6" />
                        <div class="label"><p class="f_med no">06</p><span class="label_txt">ลดขยะจากอาหาร<br class="hide-xs">เหลือทิ้งในงาน</span></div>
                        </a>
	                </div>
	            </div>
	        </div>
	    </div>

        <div class="main-care-the-bear">
	        <div class="container">
	            <span class="title-sec-3">สิทธิประโยชน์ใน<br class="d-md-none">การเข้าร่วม Care the Bear</span>
	            <div class="box-content">
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-1.png" alt="">
	                    <span class="text-content">คำนวณ วัดผล ปริมาณการ<br class="d-md-block"> ลดก๊าซเรือนกระจกได้ทันที </span>
	                </div>
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-2.png" alt="">
	                    <span class="text-content">ผลการดำเนินงานนำไปแสดงใน<br class="d-md-block"> Annual Report หรือ<br class="d-md-block">  56-1 One Report ได้          </span>
	                </div>
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-3.png" alt="">
	                    <span class="text-content">นำผลการดำเนินงานยื่นขอใบ<br class="d-md-block"> ประกาศเกียรติคุณ LESS จาก<br class="d-md-block"> องค์การบริหารจัดการก๊าซเรือน<br class="d-md-block"> กระจก (องค์การมหาชน) </span>
	                </div>
	            </div>
	            <div class="box-content">
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-4.png" alt="">
	                    <span class="text-content">เกิดภาพลักษณ์ที่ดีต่อองค์กรใน<br class="d-md-block"> ด้านการบริหารจัดการสิ่งแวดล้อม<br class="d-md-block"> อย่างเป็นรูปธรรม</span>
	                </div>
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-5.png" alt="">
	                    <span class="text-content">เกิดการสร้างจิตสำนึก การมีส่วน<br class="d-md-block"> ร่วมในการดูแลด้านสิ่งแวดล้อม<br class="d-md-block"> ของพนักงานในองค์กร</span>
	                </div>
	                <div class="single-content">
	                    <img class="img-content lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-3/content-6.png" alt="">
	                    <span class="text-content">องค์กรสามารถออกแบบแผน<br class="d-md-block"> การลดการใช้งบประมาณ เช่น<br class="d-md-block">ลดค่าไฟฟ้า ค่ากระดาษ</span>
	                </div>
	            </div>
	        </div>
	    </div>

        <div class="main-performance">
	        <div class="container">
	            <span class="title-performance">ผลการดำเนินงานโครงการ</span>
	            <span class="year-performance">ตั้งแต่ปี 2018 - ปัจจุบัน</span>
	            <div class="box-content-performance">
	                <div class="left-content">
	                    <div class="list-items">
	                        <span class="icon-building"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">จำนวนพันธมิตร</span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_company|replace:',':''}">{number_format(str_replace(",","",$bear_stats.total_company))}</span>
	                                <span class="text-3">องค์กร</span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items">
	                        <span class="icon-cloud"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">ลดปริมาณก๊าซเรือนกระจกได้</span>
	                            <div>
	                                <span class="text-2" data-no="{(($bear_stats.summary_cf|replace:',':''))|number_format:2:".":""}">{number_format(str_replace(",","",$bear_stats.total_cf),2)}</span>
	                                <span class="text-3 ">Kg 
	                                    <span class="hanging-co2-1">CO</span>
	                                    <span class="text-indent-eq">eq</span>
	                                </span>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="list-items">
	                        <span class="icon-tree"></span>
	                        <div class="text-list-items">
	                            <span class="text-1">เทียบเท่าการดูดซับ 
	                                <span class="hanging-co2-2">CO</span> 
	                                <span class="text-indent-per-year">/ ปี ของต้นไม้</span>
	                            </span>
	                            <div>
	                                <span class="text-2" data-no="{$bear_stats.total_tree|replace:',':''}">{number_format(str_replace(",","",$bear_stats.total_tree),2)}</span>
	                                <span class="text-3">ต้น</span>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="right-content">
	                    <img class="lazy-img" src="{$image_url}theme/default/climatecare/public/images/care-the-bear/sec-4/content-1.png" alt="">
	                </div>
	            </div>
	        </div>
	    </div>

        <section data-aos="fade" class="no-padding-top">
            <div class="container">
                <h2 class="title">ข่าวและประชาสัมพันธ์ (News) <i class="icon-circle"></i></h2>
                <div class="slide-article owl-carousel">
                    {foreach $news as $item}
                        <div class="article item">
                            <div class="head">
                                <span>{$item.name}</span>
                                <a href="{$base_url}news/detail/{$item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            <div class="text">
                                {$item.description|nl2br}
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="see-all">
                    <a href="{$base_url}news" class="btn btn-white font-black">ดูทั้งหมด</a>
                </div>
            </div>
        </section>
    
        <section data-aos="fade">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12" >
                        <h2 class="title">ประมวลภาพกิจกรรม <i class="icon-circle"></i></h2>
                        <div class="slide-home-activity owl-carousel">
                            {foreach $latest_activity as $activity_item}
                            <div class="item">
                                <a href="{$base_url}activity/detail/{$activity_item.id}">
                                    <div class="bg" style="background-image: url('{$activity_item.image_1}'); background-size: cover;"></div>
                                    <div class="text">
                                        <h4>{$activity_item.company}</h4>
                                    </div>
                                </a>
                            </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-aos="fade" class="no-padding-top">
            <div class="container">
                <div class="home-highlight">
                    <h2 class="title text-center"><big>ความสำเร็จในการแก้ปัญหาสิ่งแวดล้อม</big></h2>
                    <h2 class="title text-center no-margin-top">การจัดกิจกรรมขององค์กร <i class="icon-circle icon-white"></i></h2>
                    <div class="subtitle text-center">
                        ผลรวมตั้งแต่ปี พ.ศ. 
                        <br class="hidden-lg hidden-md"/>
                        <select id="summary_from" onchange="get_summary_cf();">
                            {section name=foo start={$year_activity.min_year} loop={($year_activity.max_year + 1)} step=1}
                                <option value="{$smarty.section.foo.index}"{if $year_activity.max_year == $smarty.section.foo.index} selected="selected"{/if}>{($smarty.section.foo.index + 543)}</option>
                            {/section}
                        </select>
                        - 
                        <select id="summary_to" onchange="get_summary_cf();">
                            {section name=foo start={$year_activity.min_year} loop={($year_activity.max_year + 1)} step=1}
                                <option value="{$smarty.section.foo.index}"{if $year_activity.max_year == $smarty.section.foo.index} selected="selected"{/if}>{($smarty.section.foo.index + 543)}</option>
                            {/section}
                        </select>
                    </div>
                    <div class="home-icon">
                        <div class="row">
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success1.png')"></div>
                                    <div class="text"> </br>จำนวนองค์กรที่เข้าร่วม
                                        </br><big id="home-company-count" class="counter"></big> องค์กร
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success2.png')"></div>
                                    <div class="text"><br/>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้
                                        <big id="home-cf-count" class="counter"></big><br/>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success3.png')"></div>
                                    <div class="text"></br>เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้
                                        <big id="home-tree-count" class="counter"></big> ต้น
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="hr-white"/>

                    <h2 class="title text-center no-margin-top">การจัดโครงการขององค์กร <i class="icon-circle icon-white"></i></h2>
                    <div class="subtitle text-center">
                        ผลรวมตั้งแต่ปี พ.ศ. 
                        <br class="hidden-lg hidden-md"/>
                        <select id="project_summary_from" onchange="get_project_summary_cf();">
                            {section name=foo start={$year_project.min_year} loop={($year_project.max_year + 1)} step=1}
                                <option value="{$smarty.section.foo.index}"{if $year_project.max_year == $smarty.section.foo.index} selected="selected"{/if}>{($smarty.section.foo.index + 543)}</option>
                            {/section}
                        </select>
                        - 
                        <select id="project_summary_to" onchange="get_project_summary_cf();">
                            {section name=foo start={$year_project.min_year} loop={($year_project.max_year + 1)} step=1}
                                <option value="{$smarty.section.foo.index}"{if $year_project.max_year == $smarty.section.foo.index} selected="selected"{/if}>{($smarty.section.foo.index + 543)}</option>
                            {/section}
                        </select>
                    </div>
                    <div class="home-icon for-project">
                        <div class="row">
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success4.png')"></div>
                                    <div class="text"></br>จำนวนโครงการ </br>
                                        <big id="home-project-count" class="counter"></big> โครงการ
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success5.png')"></div>
                                    <div class="text"></br>ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้
                                        <big id="home-project-cf-count" class="counter"></big><br/>กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่า
                                    </div>
                                </div>
                            </div>
                            <div class="col col-sm-4">
                                <div class="box">
                                    <div class="icon" style="background-image: url('{$image_url}theme/default/assets/images/home-success6.png')"></div>
                                    <div class="text"></br>เทียบเท่าการดูดซับ CO<sub>2</sub>/ปี ของต้นไม้
                                        <big id="home-project-tree-count" class="counter"></big> ต้น
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {* <section data-aos="fade" class="no-padding-top">
        <div class="container">
            <h2 class="title">รู้จัก 6 Cares <i class="icon-circle"></i></h2>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="title font-dark text-center no-margin-top-mobile"><big>ทุกคนสามารถลดโลกร้อนด้วย</big></h2>
                    <div class="home-circle">
                        <img class="img-center" src="{$image_url}theme/default/assets/images/home-circle.png">
                        <a href="{$base_url}care#care-1" class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle1.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานเดินทางมาโดยรถสาธารณะ หรือ เดินทางมาด้วยกัน  หรือ Conference Call เพื่อลดการใช้พลังงานจากการเดินทาง">
                                <span>1</span>
                                <span>ประชาสัมพันธ์ให้ผู้ร่วมงานเดินทางโดยรถสาธารณะ</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-2"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle2.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานนำกระบอกน้ำส่วนตัว และถุงผ้ามาด้วย รวมถึงส่งเสริมการใช้เทคโนโลยีในการประชุม เพื่อลดการแจกเอกสารและกระดาษต่างๆ ในงาน">
                                <span>2</span>
                                <span>ลดการใช้กระดาษและพลาสติก</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-3"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle3.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="ส่งเสริมนโยบายในการใช้อุปกรณ์ตกแต่งที่นำกลับมาใช้ใหม่ได้ และงดการใช้โฟม เป็นบรรจุภัณฑ์และตกแต่งในงาน">
                                <span>3</span>
                                <span>งดการใช้โฟม</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-4"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle4.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้เลือกใช้อุปกรณ์ไฟฟ้าในการจัดงานที่มีฉลากประหยัดไฟ หรือมีประสิทธิภาพสูงสุดในการประหยัดพลังงาน">
                                <span>4</span>
                                <span>ลดการใช้พลังงานจากอุปกรณ์ไฟฟ้า</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-5"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle6.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="วางแผน ออกแบบในการใช้วัสดุตกแต่งที่สามารนำมา Reuse / Recycle หรือ ส่งต่อให้ใช้ประโยชน์ได้สูงสุด">
                                <span>5</span>
                                <span>เลือกใช้วัสดุตกแต่งที่นำกลับมาใช้ใหม่ได้</span>
                            </div>
                        </a>
                        <a href="{$base_url}care#care-6"  class="inner">
                            <img src="{$image_url}theme/default/assets/images/home-circle5.png">
                            <div class="text" data-toggle="tooltip" data-placement="top" title="รณรงค์ให้ผู้ร่วมงานตักอาหารแต่พอดี และทานให้หมด และส่งเสริมให้มีการคัดแยกขยะที่เกิดขึ้นจากการจัดงาน">
                                <span>6</span>
                                <span>ลดการเกิดขยะ ตักอาหารแต่พอดี และทานให้หมด</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </section> *}
        <section data-aos="fade">
            <div class="container">
                <h2 class="title">หมีเล่าเรื่อง <i class="icon-circle"></i></h2>
                <div class="slide-article owl-carousel">
                    {foreach $article as $item}
                        <div class="article item">
                            <div class="head">
                                <span>{$item.name}</span>
                                <a href="{$base_url}article/detail/{$item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            <div class="text">
                                {$item.description|nl2br}
                            </div>
                        </div>
                    {/foreach}
                </div>
                <div class="see-all">
                    <a href="{$base_url}article" class="btn btn-white font-black">ดูทั้งหมด</a>
                </div>
            </div>
        </section>
        <section data-aos="fade" class="no-padding-top">
            <div class="container">
                <h2 class="title">Tips <i class="icon-circle"></i></h2>
                <div class="slide-home-tips owl-carousel">
                    {foreach $tips as $item}
                        <a href="{$base_url}tips/detail/{$item.id}">
                            <div class="item">
                                <div class="img">
                                    <img src="{$item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                </div>
                            </div>
                        </a>
                    {/foreach}
                </div>
                <br/>
                <div class="see-all">
                    <a href="{$base_url}tips" class="btn btn-white font-black">ดูทั้งหมด</a>
                </div>
            </div>
        </section>

        <section>
        <div class="slide-reciever"><div class="main-organize bg-bear  ">
            <div class="container"><div class="left-content  ">
                <span class="title-organize"> องค์กร<br> ที่เข้าร่วมโครงการ</span>
                <br><br><br><br>
                    <img class="img-left-content" 
                    src="{$base_url_master}/images/theme/default/public/images/bg-sec-slide/bear-icon.png">
                </div>
                <div class="swiper main-slide-wrapper">
                    <div class="col-md-12">
                        <div class="home-grid-logo owl-carousel" style="height:370px;">
                            {foreach array_chunk($company, 50) as $items}
                                <div class="item">
                                    {foreach $items as $item}
                                    <div class="box">
                                        <img src="{$item.logo}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                    </div>
                                    {/foreach}
                                </div>
                            {/foreach}
                            {*
                            {foreach $company as $item}
                                <div class="box">
                                    <img src="{$item.logo}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                </div>
                            {/foreach}
                            *}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
        
        <section>
        <div class="contact-html">
            <div class="main-contact">
                <span class="title-bot-contact inviewspy inviewed">โครงการอื่นๆ</span>
                <div class="bot-contact">
                <div class="box-content">
                    <a href="{$base_url_master}/care-the-bear" 
                        class="active-bear another bear-active filter-anoter-1 inviewspy inviewed">
                        <img class="another-img lazy-img img-loaded" 
                        src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/content-1new.jpg">
                        
                        <div class="another-content">
                            <div class="single-another-content">
                                <img class="icon-1 lazy-img img-loaded" 
                                src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/icon/img-1.svg">
                                <span class="text-another">Care the Bear<br>“ลดโลกร้อน”</span>
                            </div>
                        </div>
                    </a>
                    <a href="{$base_url_master}/care-the-whale" 
                    class="active-bear another whale-active filter-anoter-1 inviewspy inviewed">
                        <img class="another-img lazy-img img-loaded" 
                        src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/content-2.jpg">
                            <div class="another-content">
                                <div class="single-another-content">
                                    <img class="icon-2 lazy-img img-loaded" src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/icon/img-2.svg">
                                    <span class="text-another">Care the Whale<br>“ขยะล่องหน”</span>
                                </div>
                            </div>
                    </a>
                    <a href="{$base_url_master}/care-the-wild" class="active-bear another wild-active filter-anoter-1 inviewspy inviewed">
                        <img class="another-img lazy-img img-loaded" 
                        src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/content-3.jpg">
                        <div class="another-content">
                            <div class="single-another-content">
                                <img class="icon-3 lazy-img img-loaded" 
                                src="{$base_url_master}/images/theme/default/public/images/care-the-bear/sec-6/icon/img-3.svg">
                                <span class="text-another">Care the Wild<br>“ปลูกป้อง Plant &amp; Protect”</span>
                            </div>
                        </div>
                    </a>
             </div>
        </div>
        </div>
        </div>
        </section>

    {* <div class="slide-reciever"></div>
    	<div class="contact-html"></div> *}

{/block}
{block name=script}
    <script src="{$image_url}theme/default/assets/js/jquery.waypoints.min.js"></script>
    <script src="{$image_url}theme/default/assets/js/jquery.counterup.min.js"></script>
    <script>
        {literal}
        function input_format(value, dec)
        {
            value = value.replace(/\,/g,"");
            if(value != "")
            {
                if(dec == 0)
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+$)/g,"$1,");
                }
                else
                {
                    return parseFloat(value).toFixed(dec).replace(/(\d)(?=(\d{3})+\.\d\d$)/g,"$1,");
                }
            }
            else
            {
                return '';
            }
        }
        {/literal}

        function get_summary_cf()
        {
            $.get('{$base_url}{$page}/get_summary_cf/' + $('#summary_from').val() + '/' + $('#summary_to').val(), function(data) {
                $('#home-company-count').html(input_format(((($('#summary_from').val() == '2019') ? 50 : 0) + parseInt(data.total_company)).toString(), 0));
                $('#home-cf-count').html(input_format(((($('#summary_from').val() == '2019') ? 7712000 : 0) + parseInt(((data.total_cf == null) ? 0 : data.total_cf))).toString(), 0));
                $('#home-tree-count').html(input_format(((((($('#summary_from').val() == '2019') ? 7712000 : 0) + parseInt(((data.total_cf == null) ? 0 : data.total_cf)))) / 9).toString(), 0));
            });
        }

        function get_project_summary_cf()
        {
            $.get('{$base_url}{$page}/get_project_summary_cf/' + $('#project_summary_from').val() + '/' + $('#project_summary_to').val(), function(data) {
                $('#home-project-count').html(input_format((parseInt(data.total_project)).toString(), 0));
                $('#home-project-cf-count').html(input_format((parseInt(((data.total_cf == null) ? 0 : data.total_cf))).toString(), 0));
                $('#home-project-tree-count').html(input_format((((parseInt(((data.total_cf == null) ? 0 : data.total_cf)))) / 9).toString(), 0));
            });
        }

        $(document).ready(function () {

            get_summary_cf();
            get_project_summary_cf();

            $('.counter').hide();
            setTimeout(function(){ 
                $('.counter').show().counterUp({
                    delay: 100,
                    time: 1200
                });
            }, 500);
        });
    </script>
    {* <script src="{$image_url}theme/default/climatecare/public/js/care-the-bear.js"></script> *}
{/block}