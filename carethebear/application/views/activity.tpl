{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    {if $today_activity|count > 0}
    	<section class="no-padding-bottom">
            <div class="container">
                <h2 class="title">กิจกรรมวันนี้ <i class="icon-circle"></i></h2>
                <div class="slide-activity owl-carousel">
                    {foreach $today_activity as $today_activity_item}
                        <div class="item article is-half">
                            <div class="head">
                                <span>{$today_activity_item.name}</span>
                                <a href="{$base_url}activity/detail/{$today_activity_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$today_activity_item.image_1}">
                            <div class="text">
                                <h3>วันที่ {$today_activity_item.activity_date|date_thai:"%e %B %Y"}<br/>จัดโดย  {$today_activity_item.company}</h3>
                                <p>{$today_activity_item.description|truncate:500|nl2br}</p>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </section>
    {/if}
    <section class="no-padding">
        <div class="container">
            {if $today_activity|count > 0}
            <hr class="border-orange"/>
            {/if}
            <br/>
            <h2 class="title no-margin-top inline-block">{if $smarty.get.q != ''}ผลการค้นหา{else}กิจกรรมที่ผ่านมา <i class="icon-circle"></i>{/if}</h2>
            <form method="get" class="activity-search form-inline">
                <script>
                    function select(value)
                    {
                        if(value == 'type')
                        {
                            $('.act-search').hide(); 
                            $('.act-search[data-mode="dropdown"]').show();
                        }
                        else
                        {
                            $('.act-search').hide(); 
                            $('.act-search[data-mode="text"]').show();
                        }
                    }
                </script>
                <select name="type" class="form-control" onchange="select(this.value)">
                    <option value="">กรุณาเลือก</option>
                    <option value="company"{if $smarty.get.type == 'company'} selected="selected"{/if}>ชื่อบริษัท/องค์กร</option>
                    <option value="type"{if $smarty.get.type == 'type'} selected="selected"{/if}>ประเภทกิจกรรม</option>
                </select>
                <select name="activity_type_id" class="form-control act-search" data-mode="dropdown"{if $smarty.get.type != 'type'} style="display:none;"{/if}>
                    <option value="">กรุณาเลือก</option>
                    {foreach $activity_type as $activity_type_item}
                        <option value="{$activity_type_item.id}"{if $activity_type_item.id == $smarty.get.activity_type_id} selected="selected"{/if}>{$activity_type_item.name}</option>
                    {/foreach}
                </select>
                <input type="text" name="q" class="form-control act-search" data-mode="text" value="{$smarty.get.q}" placeholder="ระบุคำค้นหา"{if $smarty.get.type == 'type'} style="display:none;"{/if}>
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            <div class="row activity-past">
                {if $list|count > 0}
                    {foreach $list as $key => $list_item}
                        <div class="col-sm-4">
                            <a href="{$base_url}activity/detail/{$list_item.id}" class="activity item">
                                <div class="img">
                                	<div class="bg" style="background-image: url('{$list_item.image_1}')"></div>
                                </div>
                                <div class="text">
                                    <h3>{$list_item.name}</h3>
                                    <div class="by">จัดโดย  {$list_item.company}<br/>ปริมาณคาร์บอนฟุตพริ้นท์ท์ที่ลดได้้ {$list_item.cf_care_total|number_format:2:".":","} กิโลกรัมคาร์บอนไดออกไซด์เทียบเท่ากับการปลูกต้นไม้ {($list_item.cf_care_total/9)|number_format:0:".":","} ต้น</div>
                                    <div class="detail">{$list_item.description|truncate:100|nl2br}</div>
                                    <div class="date">วันที่ {$list_item.activity_date|date_thai:"%e %B %Y"}<br/>ผู้เข้าชม  {$list_item.view_count|number_format:0:".":","} คน</div>
                                </div>
                            </a>
                        </div>
                        {if ($key + 1) % 3 == 0}
                        <div class="clearfix"></div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="inner">
                        <br/><br/><br/>
                        <center>ไม่มีข้อมูล</center>
                        <br/><br/><br/>
                    </div>
                {/if}
            </div>
            {include file='paging.tpl'}
        </div>
    </section>
{/block}