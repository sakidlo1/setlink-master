{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">เข้าสู่ระบบ <i class="icon-circle"></i></h2>
                    <div class="card-login">
                        <h2 class="name">Care the Bear</h2>
                        {if $success_msg != ""}
                        <div class="alert alert-success">
                            {$success_msg}
                        </div>
                        {/if}
                        {if $error_msg != ""}
                        <div class="alert alert-danger">
                            {$error_msg}
                        </div>
                        {/if}
                        <div class="box">
                            <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/login-bg.png');"></div>
                            <script>
                                function check_data()
                                {
                                    $('#username_req').hide();
                                    $('#password_req').hide();
                                    $('.has-error').removeClass('has-error');
                                    
                                    with(document.add_edit)
                                    {

                                        if(username.value=="")
                                        {
                                            $('#username_req').show();
                                            $('#username_req').parent('div').addClass('has-error');
                                            $('#username').focus();
                                            return false;
                                        }
                                        else if(password.value=="")
                                        {
                                            $('#password_req').show();
                                            $('#password_req').parent('div').addClass('has-error');
                                            $('#password').focus();
                                            return false;
                                        }
                                    }
                                }
                            </script>
                            <form name="add_edit" onsubmit="return check_data();" method="post">
                                <h2 class="font-cond">สำหรับสมาชิก Log in</h2>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon icon-user"></i></div>
                                        <input type="text" id="username" name="username" class="form-control" placeholder="รหัสผู้ใช้งาน">
                                    </div>
                                    <p id="username_req" class="required">กรุณากรอกรหัสผู้ใช้งาน</p>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="icon icon-password"></i></div>
                                        <input type="password" id="password" name="password" class="form-control" placeholder="รหัสผ่าน">
                                    </div>
                                    <p id="password_req" class="required">กรุณากรอกรหัสผ่าน</p>
                                </div>
                                <div class="forget-link">
                                    <a href="{$base_url}member/forget_password">ลืมรหัสผ่าน</a>
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" name="login" value="login" class="btn btn-yellow">เข้าสู่ระบบ</button>
                                </div>
                                <div class="form-group">
                                    <div class="other font-cond">คุณมีบัญชีผู้ใช้หรือไม่ ? 
                                        <a href="{$base_url}member/register" class="font-yellow">ลงทะเบียนสมาชิกที่นี่</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}