{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	{if $activity_detail.activity_date < $today_date}
        <section>
            <div class="container">
                <h2 class="title">กิจกรรมที่ผ่านมา <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อกิจกรรม : {$activity_detail.name}<br/>
                        วันที่ : {$activity_detail.activity_date|date_thai:"%e %B %Y"} ({$activity_detail.start_on|substr:0:5} - {$activity_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$activity_detail.company}<br/>
                        สถานที่ : {$activity_detail.place}<br/>
                        ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$activity_detail.cf_care_total|number_format:2:".":","} kgCO<sub>2</sub>e เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี ของต้นไม้  {($activity_detail.cf_care_total/9)|number_format:0:".":","} ต้น
                        <br />
                    </div>
                    <div class="img"><img src="{$activity_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$activity_detail.description|nl2br}</p>
                    </div>
                    {if $activity_detail.image_2 != '' || $activity_detail.image_3 != ''}
                        <div class="gallery">
                            {if $activity_detail.image_2 != ''}
                                <a href="{$activity_detail.image_2}" class="inner"><img src="{$activity_detail.image_2}"></a>
                            {/if}
                            {if $activity_detail.image_3 != ''}
                                <a href="{$activity_detail.image_3}" class="inner"><img src="{$activity_detail.image_3}"></a>
                            {/if}
                        </div>
                    {/if}
                    {if $activity_detail.vdo != ''}
                        <div class="vdo">
                            <h1 class="title">วิดีโอ</h1>
                            {$activity_detail.vdo}
                        </div>
                    {/if}
                </div>
            </div>
        </section>
    {elseif $activity_detail.activity_date > $today_date}
            <div class="container">
                <h2 class="title">กิจกรรมที่กำลังจะมาถึง <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อกิจกรรม : {$activity_detail.name}<br/>
                        วันที่ : {$activity_detail.activity_date|date_thai:"%e %B %Y"} ({$activity_detail.start_on|substr:0:5} - {$activity_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$activity_detail.company}<br/>
                        สถานที่ : {$activity_detail.place}<br/>
                    </div>
                    <div class="img"><img src="{$activity_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$activity_detail.description|nl2br}</p>
                    </div>
                </div>
            </div>
        </section>
    {else}
    	<section>
            <div class="container">
                <h2 class="title">กิจกรรมวันนี้ <i class="icon-circle"></i></h2>
                <div class="activity-detail">
                    <div class="name">
                        ชื่อกิจกรรม : {$activity_detail.name}<br/>
                        วันที่ : {$activity_detail.activity_date|date_thai:"%e %B %Y"} ({$activity_detail.start_on|substr:0:5} - {$activity_detail.end_on|substr:0:5})<br/>
                        จัดโดย : {$activity_detail.company}<br/>
                        สถานที่ : {$activity_detail.place}<br/>
                    </div>
                    <div class="img"><img src="{$activity_detail.image_1}" class="img-responsive"></div>
                    <div class="detail">
                        <p>{$activity_detail.description|nl2br}</p>
                    </div>
                </div>
            </div>
        </section>
        {if $start_time_minus_2_hours <= $now_time && $activity_detail.activity_date == $today_date}
            <section>
                <div class="container">
                    {if $success_msg != ""}
                    <div class="alert alert-success">
                        {$success_msg}
                    </div>
                    {/if}
                    {if $error_msg != ""}
                    <div class="alert alert-danger">
                        {$error_msg}
                    </div>
                    {/if}
                    <script>
                        function check_data()
                        {
                            $('.vehicle_id_req').hide();
                            $('#people_req').hide();
                            $('#km_req').hide();
                            $('.has-error').removeClass('has-error');

                            with(document.add_edit)
                            {
                                if($('input[name="vehicle_id"]:checked').length == 0)
                                {
                                    $('.vehicle_id_req').show();
                                    $('.vehicle_id_req').parent('div').addClass('has-error');
                                    return false;
                                }
                                else if(($('input[name="vehicle_id"]:eq(0)').prop("checked") == true || $('input[name="vehicle_id"]:eq(1)').prop("checked") == true || $('input[name="vehicle_id"]:eq(2)').prop("checked") == true || $('input[name="vehicle_id"]:eq(3)').prop("checked") == true || $('input[name="vehicle_id"]:eq(4)').prop("checked") == true || $('input[name="vehicle_id"]:eq(5)').prop("checked") == true || $('input[name="vehicle_id"]:eq(6)').prop("checked") == true || $('input[name="vehicle_id"]:eq(7)').prop("checked") == true || $('input[name="vehicle_id"]:eq(8)').prop("checked") == true || $('input[name="vehicle_id"]:eq(9)').prop("checked") == true) && people.value=='')
                                {
                                    $('#people_req').show();
                                    $('#people_req').parent('div').addClass('has-error');
                                    $('#people').focus();
                                    return false;
                                }
                                else if(km.value=='')
                                {
                                    $('#km_req').show();
                                    $('#km_req').parent('div').addClass('has-error');
                                    $('#km').focus();
                                    return false;
                                }
                            }
                        }
                    </script>
                    <form class="activity-form" name="add_edit" method="post" onsubmit="return check_data();">
                        <div class="activity-form-head">
                            <img class="img" src="{$image_url}theme/default/assets/images/activity-form-img1.png">
                            <h2 class="title">แบบสอบถามการเดินทางมาร่วมกิจกรรม</h2>
                            <p>(เพื่อเก็บค่าการลดการปล่อย Carbon Footprint) * Required</p>
                            <p class="form-mask">ท่านเดินทางมาร่วมงานโดย<br/><span class="font-red">(โปรดเลือกการเดินทางเพียงแบบเดียว)</span></p>
                        </div>
                        <div class="form">
                            <div class="box">
                                <div class="row">
                                    <div class="col col-sm-6">
                                        <div class="form-group">
                                            <label>คุณเดินทางมาอย่างไร<br/>แบบที่ 1 ประเภทส่วนบุคคล</label>
                                            {*
                                                <div class="radio">
                                                    <input type="radio" id="checkbox11" name="vehicle_id" value="1"> 
                                                    <label for="checkbox11">รถยนต์ส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox12" name="vehicle_id" value="2"> 
                                                    <label for="checkbox12">รถกระบะส่วนบุคคล</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox13" name="vehicle_id" value="3"> 
                                                    <label for="checkbox13">แท๊กซี่</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox14" name="vehicle_id" value="4"> 
                                                    <label for="checkbox14">จักรยานยนต์</label>
                                                </div>
                                            *}
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_1" value="14"> 
                                                <label for="onsite_person_1">รถยนต์</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_2" value="15"> 
                                                <label for="onsite_person_2">รถกระบะ</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_3" value="16"> 
                                                <label for="onsite_person_3">รถจักรยานยนต์</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_4" value="17"> 
                                                <label for="onsite_person_4">จักรยาน/เดิน</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_5" value="18"> 
                                                <label for="onsite_person_5">แท็กซี่</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_6" value="19"> 
                                                <label for="onsite_person_6">รถยนต์ไฟฟ้า (EV)</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_7" value="20"> 
                                                <label for="onsite_person_7">รถกระบะไฟฟ้า (EV)</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_8" value="21"> 
                                                <label for="onsite_person_8">มอเตอร์ไซด์ไฟฟ้า (EV)</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_9" value="22"> 
                                                <label for="onsite_person_9">รถ SUV ไฟฟ้า (EV)</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_person_10" value="23"> 
                                                <label for="onsite_person_10">รถตู้ไฟฟ้า (EV)</label>
                                            </div>
                                            <p class="required vehicle_id_req">กรุณาเลือกข้อมูลการเดินทาง</p>
                                        </div>
                                        <div class="form-group">
                                            <label><small>สำหรับแบบที่ 1 โปรดกรอกจำนวนผู้ร่วมเดินทาง</small></label>
                                            <input type="text" id="people" name="people" maxlength="3" placeholder="จำนวน">
                                            <p id="people_req" class="required">กรุณาระบุจำนวนผู้ร่วมเดินทาง</p>
                                        </div>
                                    </div>
                                    <div class="col col-sm-6">
                                        <div class="img-bg">
                                            <img src="{$image_url}theme/default/assets/images/activity-form-img2.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="row">
                                    <div class="col col-sm-6">
                                        <div class="form-group">
                                            <label>แบบที่ 2 ประเภทสาธารณะ</label>
                                            {*
                                                <div class="radio">
                                                    <input type="radio" id="checkbox31" name="vehicle_id" value="5"> 
                                                    <label for="checkbox31">รถโดยสารประจำทาง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox32" name="vehicle_id" value="6"> 
                                                    <label for="checkbox32">รถตู้ประจำทาง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox33" name="vehicle_id" value="7"> 
                                                    <label for="checkbox33">จักรยานยนต์รับจ้าง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox34" name="vehicle_id" value="8"> 
                                                    <label for="checkbox34">รถไฟฟ้า</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox35" name="vehicle_id" value="9"> 
                                                    <label for="checkbox35">เรือยนต์</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox37" name="vehicle_id" value="10"> 
                                                    <label for="checkbox37">จักรยาน / เดิน</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox38" name="vehicle_id" value="11"> 
                                                    <label for="checkbox38">ประชุมออนไลน์</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox39" name="vehicle_id" value="12"> 
                                                    <label for="checkbox39">เที่ยวบินในประเทศ</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" id="checkbox40" name="vehicle_id" value="13"> 
                                                    <label for="checkbox40">เที่ยวบินระหว่างประเทศ</label>
                                                </div>
                                            *}
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_1" value="24"> 
                                                <label for="onsite_public_1">รถโดยสารประจำทาง</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_2" value="25"> 
                                                <label for="onsite_public_2">รถไฟฟ้า/รถไฟใต้ดิน</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_3" value="26"> 
                                                <label for="onsite_public_3">รถตู้ประจำทาง</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_4" value="27"> 
                                                <label for="onsite_public_4">รถจักรยานยนต์รับจ้าง</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_5" value="28"> 
                                                <label for="onsite_public_5">เรือด่วนเจ้าพระยา</label>
                                            </div>
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_6" value="29"> 
                                                <label for="onsite_public_6">รถบัสไฟฟ้า (EV)</label>
                                            </div> 
                                            <div class="radio">
                                                <input type="radio" name="vehicle_id" id="onsite_public_7" value="30"> 
                                                <label for="onsite_public_7">เรือด่วนเจ้าพระยาไฟฟ้า (EV)</label>
                                            </div> 
                                            <p class="required vehicle_id_req">กรุณาเลือกข้อมูลการเดินทาง</p>
                                        </div>
                                    </div>
                                    <div class="col col-sm-6">
                                        <div class="form-group">
                                            <div class="img-bg">
                                                <a href="https://www.google.com/maps/" target="_blank">
                                                    <img src="{$image_url}theme/default/assets/images/activity-form-img3.png">
                                                </a>
                                                <input type="text" id="km" name="km" maxlength="7" placeholder="0">
                                                <p id="km_req" class="required text-center">กรุณาระบุระยะทาง</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="form-group">
                                    <h3>แบบที่ 3 กรณีจัดกิจกรรม Online หากคุณต้องเดินทางมายังสถานที่จัดงานจริง คุณจะเลือกเดินทางโดยวิธีใด</h3>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="list-bullet">
                                                <label><small>&#9679;</small> ประเภทส่วนบุคคล</label>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_1" value="31"> 
                                                    <label for="online_person_1">รถยนต์</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_2" value="32"> 
                                                    <label for="online_person_2">รถกระบะ</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_3" value="33"> 
                                                    <label for="online_person_3">รถจักรยานยนต์</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_4" value="34"> 
                                                    <label for="online_person_4">จักรยาน/เดิน</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_5" value="35"> 
                                                    <label for="online_person_5">แท็กซี่</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_6" value="36"> 
                                                    <label for="online_person_6">รถยนต์ไฟฟ้า (EV)</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_7" value="37"> 
                                                    <label for="online_person_7">รถกระบะไฟฟ้า (EV)</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_8" value="38"> 
                                                    <label for="online_person_8">มอเตอร์ไซด์ไฟฟ้า (EV)</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_9" value="39"> 
                                                    <label for="online_person_9">รถ SUV ไฟฟ้า (EV)</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_person_10" value="40"> 
                                                    <label for="online_person_10">รถตู้ไฟฟ้า (EV)</label>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="list-bullet">
                                                <label><small>&#9679;</small> ประเภทสาธารณะ</label>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_1" value="41"> 
                                                    <label for="online_public_1">รถโดยสารประจำทาง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_2" value="42"> 
                                                    <label for="online_public_2">รถไฟฟ้า / รถไฟใต้ดิน</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_3" value="43"> 
                                                    <label for="online_public_3">รถตู้ประจำทาง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_4" value="44"> 
                                                    <label for="online_public_4">รถจักรยานยนต์รับจ้าง</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_5" value="45"> 
                                                    <label for="online_public_5">เรือด่วนเจ้าพระยา</label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_6" value="46">
                                                    <label for="online_public_6">รถบัสไฟฟ้า (EV)</label>
                                                </div> 
                                                <div class="radio">
                                                    <input type="radio" name="vehicle_id" id="online_public_7" value="47">
                                                    <label for="online_public_7">เรือด่วนเจ้าพระยาไฟฟ้า (EV)</label>
                                                </div> 
                                            </div>    
                                        </div>
                                    </div>
                                    <p class="required vehicle_id_req">กรุณาเลือกข้อมูลการเดินทาง</p>  
                                </div>
                            </div>
                            <div class="btn-box">
                                <button type="submit" name="save" value="save" class="btn btn-yellow" >Save</button>
                                <button type="button" class="btn btn-grey" onclick="window.history.back();">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        {/if}
    {/if}
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}