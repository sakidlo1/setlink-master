<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>รายงานการลดคาร์บอนฟุตพริ้นท์</title>
        <style>
            @page {
                margin: 10mm;
                margin-header: 0mm;
                margin-footer: 0mm;
            }
        </style>
    </head>
    <body style="padding: 0px; margin: 0px; font-family: 'sarabun'; font-size: 18px;">
        <table cellpadding="0" cellspacing="5" width="90%" align="center">
            <tr>
                <td rowspan="7" style="width: 200px;">
                    <img style="height: 150px;" src="{$image_url}theme/default/assets/images/home-bare.png">
                </td>
                <td style="width: 80px;">&nbsp;</td>
                <td style="width: 50px;">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>บริษัท</td>
                <td>&nbsp;</td>
                <td colspan="2">{$member.company}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>ฝ่าย</td>
                <td>&nbsp;</td>
                <td colspan="2">{$member.department}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>ชื่อกิจกรรม</td>
                <td>&nbsp;</td>
                <td colspan="2">{$project.name}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>สถานที่</td>
                <td>&nbsp;</td>
                <td colspan="2">{$project.place}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>วันที่</td>
                <td>&nbsp;</td>
                <td colspan="2">{$project.start_date|date_thai:"%e %B %Y"} - {$project.end_date|date_thai:"%e %B %Y"}</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6"><hr /></td>
            </tr>
            <tr>
                <td colspan="3">จำนวนผู้มาเข้าร่วมประชุม</td>
                <td style="text-align: right;">{$project.attendant|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>คน</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การจัดการของแจกให้กับผู้ร่วมงาน
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - ถุงพลาสติก
                </td>
                <td style="text-align: right;">{$project.care_2_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>ใบ</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - ถุงกระดาษคราฟท์
                </td>
                <td style="text-align: right;">{$project.care_2_2|number_format:2:".":","}</td>
                <td>&nbsp;</td>
                <td>กก.</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 20px;">
                    • การจัดการของเสียหลังการจัดงาน
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 55 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_1_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_1_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_1_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_1_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_1_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_1_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 70 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_2_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_2_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_2_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_2_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_2_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_2_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 80 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_3_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_3_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_3_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_3_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_3_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_3_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 90 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_4_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_4_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_4_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_4_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_4_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_4_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 100 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_5_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_5_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_5_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_5_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_5_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_5_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 40px;">
                    - กระดาษ 120 แกรม
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A3
                </td>
                <td style="text-align: right;">{$project.care_3_6_1|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A4
                </td>
                <td style="text-align: right;">{$project.care_3_6_2|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - A5
                </td>
                <td style="text-align: right;">{$project.care_3_6_3|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B3
                </td>
                <td style="text-align: right;">{$project.care_3_6_4|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B4
                </td>
                <td style="text-align: right;">{$project.care_3_6_5|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="3" style="padding-left: 60px;">
                    - B5
                </td>
                <td style="text-align: right;">{$project.care_3_6_6|number_format:0:".":","}</td>
                <td>&nbsp;</td>
                <td>แผ่น</td>
            </tr>
            <tr>
                <td colspan="6"><hr /></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">
                    ปริมาณคาร์บอนฟุตพริ้นท์ที่ลดได้ {$project.cf_care_total|number_format:2:".":","} kgCO<sub>2</sub>e เทียบเท่าการดูดซับ CO<sub>2</sub> /ปี ของต้นไม้ {($project.cf_care_total/9)|number_format:0:".":","} ต้น
                </td>
            </tr>
        </table>
    </body>
</html>