{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=body}
	<section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">{$content.name} <i class="icon-circle"></i></h2>
                    <div class="content-detail">
                        {$content.content}
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}