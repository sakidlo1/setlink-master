{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 class="title">ลืมรหัสผ่าน <i class="icon-circle"></i></h2>
                    <div class="card-login">
                        <h2 class="name">Care the Bear</h2>
                        {if $success_msg != ""}
                        <div class="alert alert-success">
                            {$success_msg}
                        </div>
                        {/if}
                        {if $error_msg != ""}
                        <div class="alert alert-danger">
                            {$error_msg}
                        </div>
                        {/if}
                        <div class="box">
                            <div class="bg" style="background-image: url('{$image_url}theme/default/assets/images/login-bg.png');"></div>
                            <script>
                                function check_data()
                                {
                                    $('#email_req').hide();
                                    $('.has-error').removeClass('has-error');
                                    
                                    with(document.add_edit)
                                    {

                                        if(email.value=="")
                                        {
                                            $('#email_req').show();
                                            $('#email_req').parent('div').addClass('has-error');
                                            $('#email').focus();
                                            return false;
                                        }
                                    }
                                }
                            </script>
                            <form name="add_edit" onsubmit="return check_data();" method="post">
                                <h2 class="font-cond">สำหรับสมาชิก ลืมรหัสผ่าน</h2>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                        <input type="text" id="email" name="email" class="form-control" placeholder="ระบุอีเมล">
                                    </div>
                                    <p id="email_req" class="required">กรุณากรอกอีเมล</p>
                                </div>
                                <br/><br/>
                                <div class="form-group text-center">
                                    <button type="submit" name="forget_password" value="forget_password" class="btn btn-yellow">ส่ง</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}