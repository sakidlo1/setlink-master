{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    {if $today_project|count > 0}
    	<section class="no-padding-bottom">
            <div class="container">
                <h2 class="title">โครงการวันนี้ <i class="icon-circle"></i></h2>
                <div class="slide-activity owl-carousel">
                    {foreach $today_project as $today_project_item}
                        <div class="item article is-half">
                            <div class="head">
                                <span>{$today_project_item.name}</span>
                                <a href="{$base_url}project/detail/{$today_project_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$today_project_item.image_1}">
                            <div class="text">
                                <h3>วันที่ {$today_project_item.start_date|date_thai:"%e %B %Y"} - {$today_project_item.end_date|date_thai:"%e %B %Y"}<br/>จัดโดย  {$today_project_item.company}</h3>
                                <p>{$today_project_item.description|truncate:500|nl2br}</p>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </section>
    {/if}
    {if $current_project|count > 0}
        <section class="no-padding-bottom">
            <div class="container">
                <h2 class="title">โครงการที่ดำเนินการ <i class="icon-circle"></i></h2>
                <div class="slide-activity owl-carousel">
                    {foreach $current_project as $current_project_item}
                        <div class="item article is-half">
                            <div class="head">
                                <span>{$current_project_item.name}</span>
                                <a href="{$base_url}project/detail/{$current_project_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$current_project_item.image_1}">
                            <div class="text">
                                <h3>วันที่ {$current_project_item.start_date|date_thai:"%e %B %Y"} - {$current_project_item.end_date|date_thai:"%e %B %Y"}<br/>จัดโดย  {$current_project_item.company}</h3>
                                <p>{$current_project_item.description|truncate:500|nl2br}</p>
                            </div>
                        </div>
                    {/foreach}
                </div>
                <br/>
                <div class="see-all" style="text-align: center;">
                    <a href="{$base_url}project/current" class="btn btn-white font-black">ดูทั้งหมด</a>
                </div>
            </div>
        </section>
    {/if}
    {if $past_project|count > 0}
        <section class="no-padding-bottom">
            <div class="container">
                <h2 class="title">โครงการที่ดำเนินการเสร็จสิ้น <i class="icon-circle"></i></h2>
                <div class="slide-activity owl-carousel">
                    {foreach $past_project as $past_project_item}
                        <div class="item article is-half">
                            <div class="head">
                                <span>{$past_project_item.name}</span>
                                <a href="{$base_url}project/detail/{$past_project_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$past_project_item.image_1}">
                            <div class="text">
                                <h3>วันที่ {$past_project_item.start_date|date_thai:"%e %B %Y"} - {$past_project_item.end_date|date_thai:"%e %B %Y"}<br/>จัดโดย  {$past_project_item.company}</h3>
                                <p>{$past_project_item.description|truncate:500|nl2br}</p>
                            </div>
                        </div>
                    {/foreach}
                </div>
                <br/>
                <div class="see-all" style="text-align: center;">
                    <a href="{$base_url}project/past" class="btn btn-white font-black">ดูทั้งหมด</a>
                </div>
            </div>
        </section>
    {/if}
{/block}