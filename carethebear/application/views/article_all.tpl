{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
        <link rel="stylesheet" type="text/css" href="{$image_url}theme/default/assets/css/bootstrap.min.css">
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        <script>
        const bear = [
            {foreach $company_bear as $company_bear_item}
                '{$company_bear_item.logo}',
            {/foreach}
        ];
        const dataOrganization = [
            ...bear
        ];
        </script>
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
	<section id="article">
        <div class="container">
            <h2 class="title inline-block">หมีเล่าเรื่อง <i class="icon-circle"></i></h2>
            <form action="{$base_url}article" method="get" class="activity-search form-inline mt-20">
                <input type="text" id="q" name="q" value="{$smarty.get.q}" class="form-control mobile-full" placeholder="ระบุคำค้นหา">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            <div class="row">
                {foreach $article as $article_item}
                    <div class="col-sm-12">
                        <div class="item article is-half">
                            <div class="head">
                                <span>{$article_item.name}</span>
                                <a href="{$base_url}article/detail/{$article_item.id}" class="btn btn-border btn-sm">รายละเอียด</a>
                            </div>
                            <img class="img" src="{$article_item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                            <div class="text">
                                {$article_item.description|nl2br}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
            <hr/>
            <div class="see-all">
                <a href="{$base_url}article" class="btn btn-white font-black">ดูทั้งหมด</a>
            </div>
        </div>
    </section>
    <section id="tips" data-aos="fade" class="no-padding">
        <div class="container">
            <h2 class="title no-margin-top">Tips <i class="icon-circle"></i></h2>
            <div class="row tip-box">
                {foreach $tips as $tips_item}
                    <div class="col col-sm-4">
                        <a href="{$base_url}tips/detail/{$tips_item.id}">
                            <div class="tips_item">
                                <div class="img">
                                    <img src="{$tips_item.thumbnail}" onerror="this.src='{$image_url}theme/default/no_img.png';">
                                </div>
                            </div>
                        </a>
                    </div>
                {/foreach}
            </div>
            <hr/>
            <div class="see-all">
                <a href="{$base_url}tips" class="btn btn-white font-black">ดูทั้งหมด</a>
            </div>
        </div>
    </section>
    <br/>
    <br class="hidden-xs"/>
{/block}