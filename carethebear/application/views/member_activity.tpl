{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
    {block name=css}
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-the-bear.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/slide-organize.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/layout.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/care-6.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/contact-section.css"> 
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/banner.css">
        <link rel="stylesheet" href="{$image_url}theme/default/climatecare/public/css/menu-bear.css">
            
    {/block}
    {block name=js}
        <script src="{$image_url}theme/default/climatecare/public/js/swiper.js"></script>
        
    
        <script src="{$image_url}theme/default/climatecare/public/js/slide-organize.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/contact-section.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/player.js"></script>
        <script src="{$image_url}theme/default/climatecare/public/js/simpledropdown.js"></script>
    {/block}
{block name=body}
    <section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    {include file='member_nav.tpl'}
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        {if $success_msg != ""}
                        <div class="alert alert-success">
                            {$success_msg}
                        </div>
                        {/if}
                        {if $error_msg != ""}
                        <div class="alert alert-danger">
                            {$error_msg}
                        </div>
                        {/if}
                        <img class="img-cover is-small" src="{$image_url}theme/default/assets/images/member-activity.png">
                        <div class="activity-control">
                            <h3 class="activity-head">
                                <img src="{$image_url}theme/default/assets/images/icon-calendar.png">
                                <span>กิจกรรมของคุณ </span>
                            </h3>
                            <a href="{$base_url}member/activity_add" class="activity-add">เพิ่มกิจกรรม <img src="{$image_url}theme/default/assets/images/icon-add.png"></a>
                            <div class="clearfix"></div>
                            <div class="activity-list">
                                {if $list|count > 0}
                                    {foreach $list as $key => $item}
                                        <div class="inner">
                                            <div class="pull-right">
                                                <a class="action" href="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={$base_url}activity/detail/{$item.id}" target="_blank" data-toggle="tooltip" data-placement="left" data-html="true" title="QR Code สำหรับนำไป SCAN<br/>เพื่อประชาสัมพันธ์ให้ตอบแบบสอบถามการเดินทาง (Care 1)" style="    padding-left: 10px;"><i class="fa fa-qrcode" ></i></a>
                                            </div>
                                            <p><span style="width: 40px; display: inline-block; text-align: right; margin-right: 10px;">{($key + 1)}.</span>{$item.name}</p>
                                            <p style="padding-left: 50px;">{$item.activity_date|date_thai:"%e %B %Y"} ({$item.start_on|substr:0:5} - {$item.end_on|substr:0:5})</p>
                                            <div class="actions" style="padding-left: 50px;">
                                                <a class="action" href="{$base_url}member/activity_report/{$item.id}"><img src="{$image_url}theme/default/assets/images/icon-activity.png"> รายงานการลดคาร์บอนฟุตพริ้นท์แต่ละกิจกรรม</a>
                                                <a class="action" href="{$base_url}member/activity_edit/{$item.id}"><img src="{$image_url}theme/default/assets/images/icon-edit2.png"> แก้ไขข้อมูล</a>
                                                <a class="action" href="{$base_url}member/activity_pdf/{$item.id}" download><img src="{$image_url}theme/default/assets/images/icon-download2.png"> ดาวโหลดรายงาน</a>
                                            </div>
                                        </div>
                                    {/foreach}
                                {else}
                                    <div class="inner">
                                        <center>ไม่มีข้อมูล</center>
                                    </div>
                                {/if}
                            </div>
                            {include file='paging.tpl'}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            {if $success_msg != ""}
                $(window).scrollTop($('.alert-success').offset().top - 50);
            {elseif $error_msg != ""}
                $(window).scrollTop($('.alert-danger').offset().top - 50);
            {/if}
        });
    </script>
{/block}