{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name}{/block}
{block name=body}
	<section data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-sm-12" >
                    {include file='member_nav.tpl'}
                    <div class="member-box">
                        <h2 class="title">{$member.company}</h2>
                        <img class="img-cover" src="{$image_url}theme/default/assets/images/media.png">
                        <div class="media-control">
                            <h3>เอกสารเผยแพร่โครงการ Care the Bear</h3>
                            {foreach $document as $document_item}
                                <div class="media-box">
                                    <div class="col">
                                        <a class="name" href="{$document_item.file}" download>{$document_item.name}</a>
                                    </div>
                                    <div class="col">
                                        <a class="detail" href="{$document_item.file}" download>รายละเอียดไฟล์</a>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}