<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
รายงานตามบริษัทสมาชิก
<br />
<br />
<br />
<br />
<table style="width: 100%; border: 1px #000000 solid;" border="1">
    <th>
      ลำดับที่
    </th>
    <th>
      ชื่อบริษัท
    </th>
    <th>
      รายการขยะ
    </th>
    <th>
      ประเภทขยะ
    </th>
    <th>
      ผลรวมปริมาณขยะ (กก.)
    </th>
    <th>
      kgCO<sub>2</sub>eq
    </th>
    <th>
      จำนวนต้นไม้
    </th>
    {foreach $company_data as $row}
        <tr>
          <td>{$row.company_name}</td>
          <td>{$row.garbage_kind_name}</td>
          <td>{$row.garbage_type_name}</td>
          <td>{$row.total}</td>
          <td>{$row.total_reduce}</td>
          <td>{$row.total_cf}</td>
        </tr>
      {/foreach}
</table>
<br />
<br />

</body>
</html>