{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>รายละเอียดการคำนวณ</h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}backend"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">ตั้งค่าคำนวณ </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">รายละเอียดการคำนวณ </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <form name="add_edit" method="post">
            <table class="table-calculate">
              <thead>
                <tr>
                  <td style="width: 150px;">รายละเอียดขยะ</td>
                  {foreach $garbage_disposal as $garbage_disposal_item}
                  <td>{$garbage_disposal_item.name}</td>
                  {/foreach}
                </tr>
              </thead>
              <tbody>
                    <tr>
                        <td colspan="{($garbage_disposal|count + 1)}"><hr /></td>
                    </tr>
                  {foreach $garbage_kind as $garbage_kind_item}
                    <tr>
                      <td rowspan="2">{$garbage_kind_item.name}</td>
                      {foreach $garbage_disposal as $garbage_disposal_item}
                        {$value = ''}
                        {foreach $calc as $calc_item}
                            {if $calc_item.garbage_kind_id == $garbage_kind_item.id && $calc_item.garbage_disposal_id == $garbage_disposal_item.id && $calc_item.value1 > 0}
                                {$value = $calc_item.value1}
                            {/if}
                        {/foreach}
                        <td><input type="text" class="form-control" name="value1_{$garbage_kind_item.id}_{$garbage_disposal_item.id}" value="{$value}" /></td>
                      {/foreach}
                    </tr>
                    <tr>
                      {foreach $garbage_disposal as $garbage_disposal_item}
                        {$value = ''}
                        {foreach $calc as $calc_item}
                            {if $calc_item.garbage_kind_id == $garbage_kind_item.id && $calc_item.garbage_disposal_id == $garbage_disposal_item.id && $calc_item.value2 > 0}
                                {$value = $calc_item.value2}
                            {/if}
                        {/foreach}
                        <td><input type="text" class="form-control" name="value2_{$garbage_kind_item.id}_{$garbage_disposal_item.id}" value="{$value}" /></td>
                      {/foreach}
                    </tr>
                    <tr>
                        <td colspan="{($garbage_disposal|count + 1)}"><hr /></td>
                    </tr>
                {/foreach}
              </tbody>
            </table>
            <div class="boxed-action">
              <button type="submit" name="save" value="save" class="btn btn-primary btn-save">
                <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
                บันทึก
              </button>
            </div>
        </form>
{/block}
{block name=script}
	<script>
        $(document).ready(function () {

            
        });
    </script>
{/block}