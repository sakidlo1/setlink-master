<form name="add_edit" action="{$base_url}backend/{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_data();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มหน่วยงานที่รับจัดการขยะ
          {elseif $action == 'edit'}
            แก้ไขหน่วยงานที่รับจัดการขยะ
          {elseif $action == 'delete'}
            ยืนยันการลบหน่วยงานที่รับจัดการขยะ
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form">
          <div class="wrap-input">
            <label>ชื่อการหน่วยงานที่รับจัดการขยะ </label>
            <input
              value="{$item.name}"
              type="text"
              class="form-control"
              name="name" id="name"
            />
          </div>
          <label id="name_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุชื่อ</small>
            </label>
          <div class="wrap-input">
            <label>รายละเอียด </label>
            <textarea name="description" id="description" class="form-control">{$item.description}</textarea>
          </div>
          <div class="wrap-input">
            <label>วิธีการจัดการขยะ </label>
            <ul>
              {foreach $garbage_disposal as $garbage_disposal_item}
                {$check = false}
                {foreach $item.detail as $detail_item}
                  {if $detail_item.garbage_disposal_id == $garbage_disposal_item.id}
                    {$check = true}
                  {/if}
                {/foreach}
                <li style="list-style: none;">
                  <input type="checkbox" class="form-control" name="garbage_disposal_id[]" value="{$garbage_disposal_item.id}" id="garbage_disposal_{$garbage_disposal_item.id}" style="height: 15px; display: inline; width: 30px;"{if $check == true} checked="checked"{/if}>
                  <label class="" for="garbage_disposal_{$garbage_disposal_item.id}">{$garbage_disposal_item.name}</label>
                </li>
              {/foreach}
            </ul>
          </div>
          <label id="garbage_disposal_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกวิธีการจัดการขยะ</small>
            </label>
          <div class="wrap-input">
              <label>สถานะ</label>
              <select name="status" id="status" class="form-control">
                <option value="">[ เลือกสถานะ ]</option>
                <option value="Y"{if $item.status == 'Y'} selected="selected"{/if}>เปิดใช้งาน</option>
                <option value="N"{if $item.status == 'N'} selected="selected"{/if}>ปิดใช้งาน</option>
              </select>
            </div>
          <label id="status_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกสถานะ</small>
            </label>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>