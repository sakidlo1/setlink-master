{extends file="backend/layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <link rel="stylesheet" href="{$image_url}theme/default/css/report.css" />
    <div class="main-content">
          <div>
            <div class="wrap-header-txt left">
              <h1>รายงาน</h1>
            </div>
            <div class="wrap-header-txt float-right">
              <!-- <h1>รายงานที่ 20 ลูกบ้าน</h1> -->
            </div>
          </div>

          <div class="breadcrumb-section clearfix">
            <div class="nav-section-left">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend"><img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend/report">รายงาน</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <a href="#">รายงานการจัดการขยะต้นทาง</a>
                  </li>
                </ol>
              </nav>
            </div>
            <div class="clear-fix"></div>
          </div>
          <div class="dashboard-section">
            <div class="report-boxed">
              <form name="report" method="get">
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">รายงานการจัดการขยะต้นทาง </span>
                </div>
                <div class="right text-right">
                  <button type="button" onclick="window.print();">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Print.png" />
                  </button>
                  <button type="submit" name="export" value="Y">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Import.png" />
                  </button>
                </div>
              </div>
              <div class="section-filter-date-action clearfix">
                <div class="float-right">
                  <select class="form-control quarter-boxed" name="type" onchange="document.report.submit();">
                    <option value="year"{if $smarty.get.type == 'year' || $smarty.get.type == ''} selected="selected"{/if}>รายปี</option>
                    <option value="quater"{if $smarty.get.type == 'quater'} selected="selected"{/if}>รายไตรมาส</option>
                    <option value="month"{if $smarty.get.type == 'month'} selected="selected"{/if}>รายเดือน</option>
                    <option value="range"{if $smarty.get.type == 'range'} selected="selected"{/if}>Year to Date</option>
                  </select>
                  <div hidden class="wrap-date-picker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                  </div>
                  {if $smarty.get.type == 'year' || $smarty.get.type == ''}
                  <div id="div-year" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year_start" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_start == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                    <span>-</span>
                    <select class="form-control quarter-boxed" name="year_end" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_end == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'quater' || $smarty.get.type == 'month'}
                  <div id="div-month" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'range'}
                  <div class="wrap-date-picker" id="div-datepicker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                    <input class="form-control daterange" name="dates" value="{$smarty.get.dates}" />
                  </div>
                  {/if}
                </div>
              </div>
            </form>
            </div>
            {foreach $data as $key => $item}
                <div class="report-boxed">
                  <div class="section-filter-date-action clearfix">
                    <div class="left">
                      <span class="header-font-size">{$item.text}</span>
                    </div>
                      <div class="right text-right">
                        (หน่วย : กก./ชิ้น)
                      </div>
                  </div>
                  <div class="section-main-tree">
                    <div class="inner-wrap">
                      <div class="row">
                        <div class="col-9 align-self-center">
                          <div class="chartdiv" id="chartdiv_{$key}"></div>
                        </div>
                        <div class="col-3 align-self-center edit-padding-table-10px">
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            {/foreach}
            <!-- section custome table -->
            <hr />
            <div class="section-table-report">
              <span class="header-font-size">รายงานการจัดขยะต้นทาง </span>
              <br />
              {foreach $data as $key => $item}
                <span class="header-font-size">{$item.text} </span>
                  <table>
                    <thead>
                      <th>
                        ลำดับที่
                      </th>
                      <th>
                        รายการขยะ
                      </th>
                      <th>
                        ประเภทขยะ
                      </th>
                      <th>
                        ผลรวมปริมาณขยะ
                      </th>
                      <th>
                        หน่วย (กก./ชิ้น)
                      </th>
                    </thead>
                    <tbody>
                      {$total_1 = 0}
                      {foreach $item.data as $no => $value}
                          <tr>
                            <td>{($no + 1)}</td>
                            <td>{$value.name}</td>
                            <td>{$value.type}</td>
                            <td>{$value.total_weight|number_format:2:".":","}</td>
                            <td>{if $value.unit == 'W'}ก.ก.{else}ชิ้น{/if}</td>
                          </tr>
                          {$total_1 = $total_1 + $value.total_weight}
                      {/foreach}
                        <tr>
						<td colspan="3">รวม</td>
						<td>{$total_1|number_format:2:".":","}</td>
						<td>ก.ก. / ชิ้น</td>
					  </tr>
                    </tbody>
                  </table>
              {/foreach}
            </div>
            <!-- END REPORT SECTION -->
          </div>
        </div>
{/block}
{block name="script"}
  <script>
        $('.daterange').daterangepicker({
          locale: {
          format: 'YYYY-MM-DD'
        },
          {if $smarty.get.dates == ''}
              startDate: moment().startOf('month'),
              endDate: moment().endOf('month')
          {/if}
        }).on('apply.daterangepicker', function(ev, picker) {
            document.report.submit();
        });

        am4core.ready(function () {

          {foreach $data as $key => $item}

              am4core.useTheme(am4themes_animated);
              // Themes end

              var chart = am4core.create("chartdiv_{$key}", am4charts.XYChart);
              chart.padding(40, 40, 40, 40);
              chart.rtl = true;

              var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
              categoryAxis.renderer.grid.template.location = 0;
              categoryAxis.dataFields.category = "network";
              categoryAxis.renderer.minGridDistance = 1;
              categoryAxis.renderer.inversed = true;
              categoryAxis.renderer.grid.template.disabled = true;
              categoryAxis.padding(0, 30, 0, 30)


              var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
              valueAxis.min = 0;

              var series = chart.series.push(new am4charts.ColumnSeries());
              series.dataFields.categoryY = "network";
              series.dataFields.valueX = "MAU";
              series.tooltipText = "{literal}{valueX.value}{/literal}"
              series.columns.template.strokeOpacity = 0;
              series.columns.template.column.cornerRadiusBottomRight = 5;
              series.columns.template.column.cornerRadiusTopRight = 5;

              var labelBullet = series.bullets.push(new am4charts.LabelBullet())
              labelBullet.label.horizontalCenter = "left";
              labelBullet.label.text = "{literal}{values.valueX.workingValue.formatNumber('#.0as')}{/literal}";
              labelBullet.label.truncate = false;
              labelBullet.label.hideOversized = false;
              labelBullet.label.dx = 10;
              labelBullet.locationX = 1;

              // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
              series.columns.template.adapter.add("fill", function (fill, target) {
                return chart.colors.getIndex(target.dataItem.index);
              });

              categoryAxis.sortBySeries = series;
              
              chart.data = [
                {foreach $item.data as $no => $value} 
                    {if $no < 10}
                    {
                      "network": `{$value.name}`,
                      "MAU": {$value.total_weight}
                    },
                    {/if}
                {/foreach}

              ]

            {/foreach}

        });

  </script>
  <style>
    .chartdiv {
      width: 100%;
      height: 500px;
    }
  </style>
{/block}