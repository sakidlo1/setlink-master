{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<div class="detail-page-top-section">
      <h1>{$item.id} : {$item.name}</h1>
      <div class="breadcrumb-section clearfix">
        <div class="nav-section-left">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{$base_url}backend"
                  ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                /></a>
              </li>
              <li class="breadcrumb-item">
                <a href="#">บริหารจัดการขยะ </a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                <a href="#">หน่วยงานที่รับจัดการขยะ </a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="clear-fix"></div>
      </div>
    </div>
    <div class="detail-section">
      <div class="row">
        <div class="col">
          <label>รหัสหน่วยงานที่รับจัดการขยะ</label>
          <p>{$item.id}</p>
        </div>
        <div class="col">
          <label>ชื่อหน่วยงานที่รับจัดการขยะ</label>
          <p>{$item.name}</p>
        </div>
      </div>
      <div class="detail">
        <div class="row">
          <div class="col">
            <label>รายละเอียด</label>
            <p>{if $item.description != ''}{$item.description|nl2br}{else}-{/if}</p>
          </div>
          <div class="col">
            <label>วิธีการจัดการขยะ</label>
            <p>
              <ul>
                  {foreach $garbage_disposal as $garbage_disposal_item}
                    {foreach $item.detail as $detail_item}
                      {if $detail_item.garbage_disposal_id == $garbage_disposal_item.id}
                        <li>
                          {$garbage_disposal_item.name}
                        </li>
                      {/if}
                    {/foreach}
                  {/foreach}
                </ul>
            </p>
          </div>
        </div>
        <label>วันที่ </label>
        <p>{$item.updated_on|date_format:"%d.%m.%Y %H:%M"}</p>
        <label>สถานะ </label>
        <p class="status-color">
          {if $item.status == 'Y'}
            <span style="background-color: #09b66d"></span> เปิดใช้งาน
          {elseif $item.status == 'N'}
            <span style="background-color: #BF3A0D"></span> ปิดใช้งาน
          {/if}
        </p>
      </div>
    </div>
{/block}
{block name=script}

{/block}