{extends file="backend/layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <link rel="stylesheet" href="{$image_url}theme/default/css/report.css" />
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <div class="main-content">
          <div>
            <div class="wrap-header-txt left">
              <h1>รายงาน</h1>
            </div>
            <div class="wrap-header-txt float-right">
              <!-- <h1>รายงานที่ 20 ลูกบ้าน</h1> -->
            </div>
          </div>

          <div class="breadcrumb-section clearfix">
            <div class="nav-section-left">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend">
                        <img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend/report">รายงาน</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <a href="#">รายงานตามบริษัท รายการขยะ ประเภทขยะ</a>
                  </li>
                </ol>
              </nav>
            </div>
            <div class="clear-fix"></div>
          </div>
          <div class="dashboard-section">
            <div class="report-boxed">
              
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">รายงานตามบริษัท รายการขยะ ประเภทขยะ </span>
                </div>

              </div>
              <div class="section-filter-date-action clearfix">
                <div class="float-left">
                    <form id="filter-form" action="{$base_url}backend/report/company_name" method="get">
                      <div class="form-inline">

                      <div class="form-group">
                        <label for="company_id" style="margin-right: 10px">เลือกบริษัท</label>
                        <select class="form-control"
                                style="width: 200px; margin-right: 20px"
                                name="company_id"
                                id="company_id">
                            <option value="">บริษัทสมาชิก</option>
                            {foreach $company as $company_item}
                              <option value="{$company_item.id}"
                                      {if $filter['company_id'] == $company_item.id}
                                          selected
                                      {/if}
                              >
                                  {$company_item.name}
                              </option>
                          {/foreach}
                        </select>
                      </div>
                      
                      <div class="form-group">
                          <label for="garbage_kind_id" style="margin-right: 10px">เลือกรายการขยะ</label>
                          <select class="form-control" style="width: 150px; margin-right: 20px" name="garbage_kind_id" id="garbage_kind_id">
                              <option value="">เลือกรายการขยะ</option>
                              {foreach $garbage_kind as $garbage_kind_item}
                                <option value="{$garbage_kind_item.id}"
                                        {if $filter['garbage_kind_id'] == $garbage_kind_item.id}
                                            selected
                                        {/if}
                                >{$garbage_kind_item.name}</option>
                              {/foreach}
                          </select>
                      </div>
                      
                      <div class="form-group">
                          <label for="garbage_type_id" style="margin-right: 10px">เลือกประเภทขยะ</label>
                          <select class="form-control" style="margin-right: 20px" name="garbage_type_id" id="garbage_type_id">
                              <option value="">เลือกประเภทขยะ</option>
                              {foreach $garbage_type as $garbage_type_item}
                                <option value="{$garbage_type_item.id}"
                                        {if $filter['garbage_type_id'] == $garbage_type_item.id}
                                            selected
                                        {/if}
                                >{$garbage_type_item.name}</option>
                              {/foreach}
                          </select>
                      </div>

                        {* <div class="form-group">
                              <label for="garbage_type_id" style="margin-right: 10px">วันที่</label>
                              <input class="form-control daterange" style="margin-right: 20px" name="dates" value="{$smarty.get.dates}" />
                        </div> *}

                          <div class="form-group" style="margin-top: 20px">
                              <label for="start_date" style="margin-right: 10px">วันที่</label>
                              <input class="form-control" type="text" id="start_date" name="start_date" value="{$filter['start_date']}" />
       
                              <label for="end_date" style="margin-right: 10px; margin-left: 10px">ถึงวันที่</label>
                              <input class="form-control" type="text" id="end_date" name="end_date" value="{$filter['end_date']}" />
                          </div>
                        
                        <div class="form-group" style="margin-top: 20px; margin-left: 10px">
                          <input class="btn btn-primary" id="submit-button" name="submit-button" type="submit" value="Search">
                        </div>
                
                      </div>
                    </form>
                </div>
              </div>
            
            </div>
            
            <hr />
            <div class="section-table-report">
              <span class="header-font-size">รายงานตามบริษัท รายการขยะ ประเภทขยะ </span>
              <br />
              {* <span class="header-font-size">{$filter['start_date']} - {$filter['end_date']} </span> *}
                  <table id="data-table" class="display" style="width:100%">
                    <thead>
                      <th>
                        รหัสบริษัท
                      </th>
                      <th>
                        วันที่
                      </th>
                      <th>
                        ชื่อบริษัท
                      </th>
                      <th>
                        รายการขยะ
                      </th>
                      <th>
                        ประเภทขยะ
                      </th>
                      <th>
                        ปริมาณขยะ (กก.)
                      </th>
                      <th>
                        ค่าเปรียบเทียบ (Baseline)
                      </th>
                      <th>
                        ปริมาณขยะที่ลดได้ (กก.)
                      </th>
                      <th>
                        kgCO<sub>2</sub>eq
                      </th>
                    </thead>
                    <tbody>
                        
                    {foreach $company_data as $row}
                      <tr>
                        <td>{$row.company_id}</td>
                        <td>{$row.do_date}</td>
                        <td>{$row.company_name}</td>
                        <td>{$row.garbage_kind_name}</td>
                        <td>{$row.garbage_type_name}</td>
                        <td>{$row.total}</td>
                        <td>{$row.baseline}</td>
                        <td>{$row.total_reduce}</td>
                        <td>{$row.total_cf}</td>
                      </tr>
                    {/foreach}
                     
                    </tbody>
                  </table>
              
            </div>
            
          </div>
        </div>
{/block}
{block name="script"}
  <script>

      $('#start_date').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "minYear": 2000,
        "maxYear": 2024,
        "autoApply": true,
        "autoUpdateInput": true,
        "locale": {
          "format": 'YYYY-MM-DD'
        },
        function(start, end, label) {
          console.log(start.format("YYYY-MM-DD"));
        }
      });

      $('#end_date').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "minYear": 2000,
        "maxYear": 2024,
        "autoApply": true,
        "autoUpdateInput": true,
        "locale": {
          "format": 'YYYY-MM-DD'
        },
        function(start, end, label) {
          console.log(start.format("YYYY-MM-DD"));
        }
      });

      $(document).ready(function() {

        $('#data-table').DataTable({
            responsive: true,
            bPaginate: true,
            bLengthChange: true,
            bAutoWidth: true,
            info: true,
            ordering: false,
            dom: 'Blfrtip',
            buttons: ['colvis', 'csv', 'excel', 'print'],
        });
      });
  </script>

  <style>
    div.dt-buttons{
      position:relative;
      float:right;
      padding-left: 20px;
    }
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>
{/block}