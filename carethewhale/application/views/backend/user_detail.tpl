{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<div class="detail-page-top-section">
      <h1>{$item.id} : {$item.name}</h1>
      <div class="breadcrumb-section clearfix">
        <div class="nav-section-left">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{$base_url}backend"
                  ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                /></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                <a href="#">ผู้ใช้งาน </a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="clear-fix"></div>
      </div>
    </div>
    <div class="detail-section">
      <div class="row">
        <div class="col">
          <label>รหัสผู้ใช้</label>
          <p>{$item.id}</p>
        </div>
        <div class="col">
          <label>ชื่อ - นามสกุล</label>
          <p>{$item.name}</p>
        </div>
      </div>
      <div class="detail">
        <label>Email</label>
        <p>{$item.email}</p>
        <label>วันที่ใช้งานล่าสุด </label>
        <p>{if $item.last_login != ''}{$item.last_login|date_format:"%d.%m.%Y %H:%M"}{else}-{/if}</p>
        <label>Role </label>
        <p>
        	{if $item.type == 'A'}
        		Administrator
        	{elseif $item.type == 'V'}
        		Viewer
        	{/if}
        </p>
      </div>
    </div>
{/block}
{block name=script}

{/block}