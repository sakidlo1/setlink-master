<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
รายงานตามประเภทอาคาร
<br />
<br />
{foreach $data as $key => $item}
{$item.text}
<br />
<br />
<table style="width: 100%; border: 1px #000000 solid;" border="1">
  <th>
    ลำดับที่
  </th>
  <th>
    ประเภทอาคาร
  </th>
  <th>
    ผลรวมขยะ
  </th>
  <th>
    Kg.CO<sub>2</sub>eq
  </th>
  <th>
    จำนวนต้นไม้
  </th>
  {$check_no = 1}
  {$check = []}
  {foreach $item.data as $no => $value}
    {if $value.name|in_array:$check}

    {else}
      {$check[] = $value.name}
      {$total_weight = 0}
      {$total_cf = 0}
      {foreach $item.data as $value2}
        {if $value.company_type_id == $value2.company_type_id}
          {$total_weight = $total_weight + $value2.total_weight}
          {$total_cf = $total_cf + $value2.total_cf}
        {/if}
      {/foreach}
      <tr>
        <td>{($check_no)}</td>
        <td>{$value.name}</td>
        <td>{$total_weight|number_format:2:".":","}</td>
        <td>{$total_cf|number_format:2:".":","}</td>
        <td>{($total_cf/9)|number_format:0:".":","}</td>
      </tr>
      {$check_no = $check_no + 1}
    {/if}
  {/foreach}
</table>
<br />
<br />
{/foreach}
</body>
</html>