{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>บริษัทสมาชิก</h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}backend"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">จัดการบริษัท </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">บริษัทสมาชิก </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <div class="company-detail">
          <img
            src="{$item.logo}"
            alt="{$item.name}"
            style="max-width: 120px; max-height: 120px;"
          />
          <div class="detail-nav">
            <div class="row">
              <div class="col-6">
                <h2 class="corp-name">{$item.name}</h2>
              </div>
              <div class="col-6 text-right">

              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <button class="code">รหัส {$item.id}</button>
              </div>
              <div class="col-6 text-right">
                <div class="wrap-right-action">
                  <button onclick="window.print();">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Print.png" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="company-detail-wrap">
              <div class="boxed">
                <div class="row">
                  <div class="col">
                    <h2>รายละเอียดของบริษัท</h2>
                  </div>
                  <div class="col text-right">
                    <button onclick="window.location='{$base_url}backend/company/edit/{$item.id}';">
                      <img src="{$image_url}theme/default/assets/images/company-detail/Edit.png" />
                    </button>
                  </div>
                </div>
                <div class="form-detail">
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/User.png" />
                    <input
                      type="text"
                      class="form-control"
                      placeholder="{$item.user.name}"
                    />
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" class="form-control" placeholder="{$item.tel1}" />
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" class="form-control" placeholder="{$item.tel2}" />
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Message.png" />
                    <input
                      type="email"
                      class="form-control"
                      placeholder="{$item.user.email}"
                    />
                  </div>
                  
                </div> <div class="row address">
                    <div class="col">
                      <h2>ที่ตั้งสำนักงาน</h2>
                      <p>
                        <span>ที่อยู่</span> {$item.address}
                      </p>
                    </div>
                <div class="form-select">
                  <table class="table-date-open">
                    <tr>
                      <td><h2>ประเภทอาคาร</h2></td>
                      <td><h2>Zone</h2></td>
                      <td style="width: 320px">
                        <div class="wrap-input date-open">
                          <div class="row align-items-center">
                            <div class="col">
                              <label>วันเปิดทำการ</label>
                            </div>
                            <div class="col">
                              <div class="wrap-checkbox">
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox1"
                                    value="option1"
                                    {if $item.open_mon == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox1"
                                    >จ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox2"
                                    value="option2"
                                    {if $item.open_tue == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox2"
                                    >อ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox3"
                                    value="option2"
                                    {if $item.open_wed == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox3"
                                    >พ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox4"
                                    value="option2"
                                    {if $item.open_thu == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox4"
                                    >พฤ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox5"
                                    value="option2"
                                    {if $item.open_fri == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox5"
                                    >ศ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox6"
                                    value="option2"
                                    {if $item.open_sat == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox6"
                                    >ส</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="inlineCheckbox7"
                                    value="option2"
                                    {if $item.open_sun == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="inlineCheckbox7"
                                    >อา</label
                                  >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>จำนวน<br />ผู้มาติดต่อ</td>
                      <td>
                        <input type="text" class="form-control" placeholder="{$item.guest_qty|number_format:0:".":","}" />
                        <span>คน/เดือน</span>
                      </td>
                      <td>ขนาดพื้นที่</td>
                      <td>
                        <input type="text" class="form-control" placeholder="{$item.size|number_format:2:".":","}" />
                        <span>ตร.ม.</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <select class="form-control">
                          <option value="1">{$item.company_type}</option>
                        </select>
                      </td>
                      <td>
                        <select class="form-control">
                          <option value="1">{$item.zone}</option>
                        </select>
                      </td>
                      <td>
                        <div class="row">
                          <div class="col">
                            <div class="wrap-input time-picker">
                              <input
                                placeholder="{$item.open_start|substr:0:5}"
                                class="form-control"
                                id="timepicker"
                              />
                            </div>
                          </div>
                          <div class="col">
                            <div class="wrap-input time-picker">
                              <input
                                placeholder="{$item.open_end|substr:0:5}"
                                class="form-control"
                                id="timepicker1"
                              />
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>จำนวนพนักงาน<br />ในบริษัท</td>
                      <td>
                        <input type="text" class="form-control" placeholder="{$item.staff_qty|number_format:0:".":","}" /><span>
                          คน</span>
                      </td>
                        <td><div class="wrap-input">
                    <div class="wrap-right-action">
                      <span style="margin-right: 15px">สถานะ </span>
                      <div class="custom-switch pl-0">
                        <input
                          class="custom-switch-input"
                          id="example_01"
                          type="checkbox"
                          {if $item.status == 'Y'} checked="checked"{/if}
                        />
                        <label
                          class="custom-switch-btn"
                          for="example_01"
                        ></label>
                      </div>
                    </div>
                  </div></td>
                      <td colspan="2" class="text-right"></td>
                </tr>
                  </table>
                   
      
                  </div>
                </div>
              </div>
              <div class="year-detail">
                <div class="year">ข้อมูลปี {$item.year}</div>
                <div class="group-btn">
                  {if $item.prev_year != ''}
                  <button onclick="window.location='{$base_url}backend/company/detail/{$item.id}/{$item.prev_year}';">
                    <img src="{$image_url}theme/default/assets/images/icon/arrow-left.png" alt="" />
                  </button>
                  {/if}
                  {if $item.next_year != ''}
                  <button onclick="window.location='{$base_url}backend/company/detail/{$item.id}/{$item.next_year}';">
                    <img src="{$image_url}theme/default/assets/images/icon/arrow-right.png" alt="" />
                  </button>
                  {/if}
                </div>
              </div>
              <div class="boxed">
                <div class="trash-type">
                  <h2>ประเภทขยะของบริษัท</h2>
                  <div>
                    {foreach $item.garbage_type as $child_item}
                        {$child_item.description|nl2br}
                    {/foreach}
                  </div>
                </div>
              </div>
              <div class="boxed">
                <div class="text-center"><h2>การจัดการด้านสิ่งแวดล้อม</h2></div>
                <div class="news-group">
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.policy_image[0].image != ''}
                      <img src="{$item.policy_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.policy_name}</p>
                      <p>{$item.manage.policy_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.policy_image[1].image != ''}
                          <img src="{$item.policy_image[1].image}" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.policy_image[2].image != ''}
                          <img src="{$item.policy_image[2].image}" />
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.person_image[0].image != ''}
                      <img src="{$item.person_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.person_name}</p>
                      <p>{$item.manage.person_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.person_image[1].image != ''}
                          <img src="{$item.person_image[1].image}" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.person_image[2].image != ''}
                          <img src="{$item.person_image[2].image}" />
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.model_image[0].image != ''}
                      <img src="{$item.model_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.model_name}</p>
                      <p>{$item.manage.model_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.model_image[1].image != ''}
                          <img src="{$item.model_image[1].image}" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.model_image[2].image != ''}
                          <img src="{$item.model_image[2].image}" />
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.activity_image[0].image != ''}
                      <img src="{$item.activity_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.activity_name}</p>
                      <p>{$item.manage.activity_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.activity_image[1].image != ''}
                          <img src="{$item.activity_image[1].image}" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.activity_image[2].image != ''}
                          <img src="{$item.activity_image[2].image}" />
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                    {if $item.public_image[0].image != ''}
                      <img src="{$item.public_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.public_name}</p>
                      <p>{$item.manage.public_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.public_image[1].image != ''}
                          <img src="{$item.public_image[1].image}" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.public_image[2].image != ''}
                          <img src="{$item.public_image[2].image}" />
                        {/if}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
{/block}
{block name=script}
	<script>
		
        $(document).ready(function () {

            

        });
    </script>
{/block}