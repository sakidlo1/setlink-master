{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>บริษัทสมาชิก</h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}backend"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">จัดการบริษัท </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">บริษัทสมาชิก </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <div class="company-detail">
          <img
            src="{$item.logo}"
            alt="{$item.name}"
            style="max-width: 120px; max-height: 120px;"
          />
          <div class="detail-nav">
            <div class="row">
              <div class="col-6">
                <h2 class="corp-name">{$item.name}</h2>
              </div>
              <div class="col-6 text-right">

              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <button class="code">รหัส {$item.id}</button>
              </div>
              <div class="col-6 text-right">
                <div class="wrap-right-action">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="company-detail-wrap">
              <div class="boxed">
                <script>
                var email_exists = true;
                function check_email_exists()
                {
                  if($('#email').val() != "") {
                    $.post(
                      '{$base_url}backend/{$page}/check_email_exists/{$item.id}',
                      { email: $('#email').val() },
                      function( data ) {
                        if(data.status == false) {
                            email_exists = false;
                            if(check_data() != false) {
                              document.add_edit.submit();
                            }
                          } else {
                            return check_data();
                          }
                      }, 
                      "json"
                    );
                    return false;
                  } else {
                    return check_data();
                  }
                }

                function check_data()
                {
                  $('#contact_name_req').hide();
                  $('#tel1_req').hide();
                  $('#email_req').hide();
                  $('#email_exists').hide();
                  $('#address_req').hide();
                  $('#company_type_id_req').hide();
                  $('#zone_id_req').hide();
                  $('#open_day_req').hide();
                  $('#open_start_req').hide();
                  $('#open_end_req').hide();
                  $('#guest_qty_req').hide();
                  $('#staff_qty_req').hide();
                  $('#size_req').hide();
                  $('.has-error').removeClass('has-error');
                  
                  with(document.add_edit)
                  {
                    if(contact_name.value=="")
                    {
                      $('#contact_name_req').show();
                      $('#contact_name_req').parent('div').addClass('has-error');
                      $('#contact_name').focus();
                      return false;
                    }
                    else if(tel1.value=="")
                    {
                      $('#tel1_req').show();
                      $('#tel1_req').parent('div').addClass('has-error');
                      $('#tel1').focus();
                      return false;
                    }
                    else if(email.value=="")
                    {
                      $('#email_req').show();
                      $('#email_req').parent('div').addClass('has-error');
                      $('#email').focus();
                      return false;
                    }
                    else if(email_exists == true)
                    {
                      $('#email_exists').show();
                      $('#email_exists').parent('div').addClass('has-error');
                      $('#email').focus();
                      return false;
                    }
                    else if(address.value=="")
                    {
                      $('#address_req').show();
                      $('#address_req').parent('div').addClass('has-error');
                      $('#address').focus();
                      return false;
                    }
                    else if(company_type_id.value=="")
                    {
                      $('#company_type_id_req').show();
                      $('#company_type_id_req').parent('div').addClass('has-error');
                      $('#company_type_id').focus();
                      return false;
                    }
                    else if(zone_id.value=="")
                    {
                      $('#zone_id_req').show();
                      $('#zone_id_req').parent('div').addClass('has-error');
                      $('#zone_id').focus();
                      return false;
                    }
                    else if($('.form-check-input:checked').length == 0)
                    {
                      $('#open_day_req').show();
                      $('#open_day_req').parent('div').addClass('has-error');
                      $('.form-check-input')[0].focus();

                      return false;
                    }
                    else if(open_start.value=="")
                    {
                      $('#open_start_req').show();
                      $('#open_start_req').parent('div').addClass('has-error');
                      $('#open_start').focus();
                      return false;
                    }
                    else if(open_end.value=="")
                    {
                      $('#open_end_req').show();
                      $('#open_end_req').parent('div').addClass('has-error');
                      $('#open_end').focus();
                      return false;
                    }
                    else if(guest_qty.value=="")
                    {
                      $('#guest_qty_req').show();
                      $('#guest_qty_req').parent('div').addClass('has-error');
                      $('#guest_qty').focus();
                      return false;
                    }
                    else if(staff_qty.value=="")
                    {
                      $('#staff_qty_req').show();
                      $('#staff_qty_req').parent('div').addClass('has-error');
                      $('#staff_qty').focus();

                      return false;
                    }
                    else if(size.value=="")
                    {
                      $('#size_req').show();
                      $('#size_req').parent('div').addClass('has-error');
                      $('#size').focus();
                      return false;
                    }
                  }
                }
              </script>
                <form name="add_edit" method="post" onsubmit="return check_email_exists();">
                <div class="row">
                  <div class="col">
                    <h2>รายละเอียดของบริษัท</h2>
                  </div>
                  <div class="col text-right">
                    <button onclick="window.location='{$base_url}backend/company/edit/{$item.id}';">
                      <img src="{$image_url}theme/default/assets/images/company-detail/Edit.png" />
                    </button>
                  </div>
                </div>
                <div class="form-detail">
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/User.png" />
                    <input
                      type="text"
                      name="contact_name" id="contact_name"
                      class="form-control"
                      placeholder="ชื่อ - นามสกุล"
                      value="{$item.user.name}"
                    />
                    <label id="contact_name_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาระบุชื่อ - นามสกุล</small>
                    </label>
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" name="tel1" id="tel1" class="form-control" value="{$item.tel1}" placeholder="โทร" />
                    <label id="tel1_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเบอร์โทรศัพท์</small>
                      </label>
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" name="tel2" id="tel2" class="form-control" value="{$item.tel2}" placeholder="โทร" />
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Message.png" />
                    <input
                      type="email"
                      name="email" id="email"
                      class="form-control"
                      value="{$item.user.email}"
                      placeholder="example@mail.com"
                    />
                     <label id="email_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุอีเมล</small>
                      </label>
                        <label id="email_exists" style="display: none;" class="text-danger col-12 text-right">
                          <small>อีเมลนี้มีอยู่ในระบบแล้ว</small>
                      </label>
                  </div>
                  
                </div> <div class="row address">
                    <div class="col">
                      <h2>ที่ตั้งสำนักงาน</h2>
                      <p>
                        <span>ที่อยู่</span> 
                        <textarea name="address" rows="2" cols="70" id="address">{$item.address}</textarea>
                      </p><label id="address_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุที่ตั้งสำนักงาน</small>
                      </label>
                    </div>
                <div class="form-select">
                  <table class="table-date-open">
                    <tr>
                      <td><h2>ประเภทอาคาร</h2></td>
                      <td><h2>Zone</h2></td>
                      <td style="width: 320px">
                        <div class="wrap-input date-open">
                          <div class="row align-items-center">
                            <div class="col">
                              <label>วันเปิดทำการ</label>
                            </div>
                            <div class="col">
                              <div class="wrap-checkbox">
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    name="open_mon"
                                    id="open_mon"
                                    value="Y"
                                    {if $item.open_mon == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_mon"
                                    >จ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_tue"
                                    name="open_tue"
                                    value="Y"
                                    {if $item.open_tue == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_tue"
                                    >อ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_wed"
                                    name="open_wed"
                                    value="Y"
                                    {if $item.open_wed == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_wed"
                                    >พ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_thu"
                                    name="open_thu"
                                    value="Y"
                                    {if $item.open_thu == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_thu"
                                    >พฤ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_fri"
                                    name="open_fri"
                                    value="Y"
                                    {if $item.open_fri == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_fri"
                                    >ศ</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_sat"
                                    name="open_sat"
                                    value="Y"
                                    {if $item.open_sat == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_sat"
                                    >ส</label
                                  >
                                </div>
                                <div class="form-check">
                                  <input
                                    class="form-check-input"
                                    type="checkbox"
                                    id="open_sun"
                                    name="open_sun"
                                    value="Y"
                                    {if $item.open_sun == 'Y'} checked="checked"{/if}
                                  />
                                  <label
                                    class="form-check-label"
                                    for="open_sun"
                                    >อา</label
                                  >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <label id="open_day_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุวันเปิดทำการ</small>
                        </label>
                      </td>
                      <td>จำนวน<br />ผู้มาติดต่อ</td>
                      <td width="150">
                        <input type="number" style="width: 60px"
                         min="0" step="1" name="guest_qty" id="guest_qty" class="form-control" value="{$item.guest_qty}" placeholder="จำนวนผู้มาติดต่อ" />
                        <small>คน/เดือน</small>
                        <label id="guest_qty_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุจำนวนผู้มาติดต่อ (คน/เดือน)</small>
                      </label>
                      </td>
                      <td>ขนาดพื้นที่</td>
                      <td width="150">
                        <input type="number" style="width: 60px"
                         min="0" step="0.01" name="size" id="size" class="form-control" value="{$item.size}" placeholder="ขนาดพื้นที่" />
                        <span>ตร.ม.</span>
                        <label id="size_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุขนาดพื้นที่ (ตร.ม.)</small>
                      </label>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <select name="company_type_id" id="company_type_id" 
                        class="form-control"
                        aria-placeholder="ประเภทอาคาร
                      "
                      >
                        <option value="">ประเภทอาคาร</option>
                        {foreach $company_type as $child_item}
                          <option value="{$child_item.id}"{if $item.company_type_id == $child_item.id} selected="selected" {/if}>{$child_item.name}</option>
                        {/foreach}
                      </select>
                    <label id="company_type_id_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาเลือกประเภทอาคาร</small>
                    </label>
                      </td>
                      <td>
                        <select name="zone_id" id="zone_id" 
                          class="form-control"
                          aria-placeholder="โซน
                        "
                        >
                          <option value="">โซน</option>
                          {foreach $zone as $child_item}
                            <option value="{$child_item.id}"{if $item.zone_id == $child_item.id} selected="selected" {/if}>{$child_item.name}</option>
                          {/foreach}
                        </select>
                    <label id="zone_id_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาเลือกโซน</small>
                    </label>
                      </td>
                      <td>
                        <div class="row">
                          <div class="col">
                            <div class="wrap-input time-picker">
                              <input
                                value="{$item.open_start|substr:0:5}"
                                name="open_start"
                                placeholder="เวลาเปิด"
                                class="form-control"
                                id="open_start"
                              />
                            </div>
                        <label id="open_start_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเวลาเปิดทำการ</small>
                      </label>
                          </div>
                          <div class="col">
                            <div class="wrap-input time-picker">
                              <input
                                value="{$item.open_end|substr:0:5}"
                                name="open_end"
                                placeholder="เวลาปิด"
                                class="form-control"
                                id="open_end"
                              />
                            </div>
                        <label id="open_end_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเวลาปิดทำการ</small>
                      </label>
                          </div>
                        </div>
                      </td>
                      <td>จำนวนพนักงาน<br />ในบริษัท</td>
                      <td width="150">
                        <input type="number" style="width:60px;"
                         min="0" step="1" name="staff_qty" id="staff_qty" class="form-control" value="{$item.staff_qty}" placeholder="จำนวนพนักงานในบริษัท" />
                         <small>คน</small>
                        <label id="staff_qty_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุจำนวนพนักงานในบริษัท (คน)</small>
                      </label>
                      </td>
                        <td><div class="wrap-input">
                    <div class="wrap-right-action">
                      <span style="margin-right: 15px">สถานะ </span>
                      <div class="custom-switch pl-0">
                        <input
                          class="custom-switch-input"
                          id="status"
                          name="status"
                          type="checkbox"
                          value="Y"
                          {if $item.status == 'Y'} checked="checked"{/if}
                        />
                        <label
                          class="custom-switch-btn"
                          for="status"
                        ></label>
                      </div>
                    </div>
                  </div></td>
                      <td colspan="2" class="text-right"></td>
                </tr>
                  </table>
                   
                   <div class="col text-right">
                      <div class="btn-right-group">
                        <button class="btn-save" type="submit" name="save" value="save">
                          <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
                          บันทึก
                        </button>
                        <button  onclick="window.location='{$base_url}backend/company/detail/{$item.id}';" class="btn-cancle">ยกเลิก</button>
                      </div>
                    </div>
      
                  </div>
                </div>
              </form>
              </div>
              <div class="year-detail">
                <div class="year">ข้อมูลปี {$item.year}</div>
                <div class="group-btn">
                  {if $item.prev_year != ''}
                  <button onclick="window.location='{$base_url}backend/company/detail/{$item.id}/{$item.prev_year}';">
                    <img src="{$image_url}theme/default/assets/images/icon/arrow-left.png" alt="" />
                  </button>
                  {/if}
                  {if $item.next_year != ''}
                  <button onclick="window.location='{$base_url}backend/company/detail/{$item.id}/{$item.next_year}';">
                    <img src="{$image_url}theme/default/assets/images/icon/arrow-right.png" alt="" />
                  </button>
                  {/if}
                </div>
              </div>
              <div class="boxed">
                <div class="trash-type">
                  <h2>ประเภทขยะของบริษัท</h2>
                  {foreach $item.garbage_type as $child_item}
                      {$child_item.description|nl2br}
                  {/foreach}
                  
                  {*
                  {foreach $item.garbage_type as $child_item}
                      <img src="{$image_url}theme/default/assets/images/company-detail/bin-{$child_item.color}.png" />
                      <span>{$child_item.name}</span>
                      <span class="val">{$child_item.weight|number_format:0:".":","}</span>
                  {/foreach}
                  <span>(หน่วย : กิโลกรัม)</span>
                  *}
                </div>
              </div>
              <div class="boxed">
                <div class="text-center"><h2>การจัดการด้านสิ่งแวดล้อม</h2></div>
                <div class="news-group">
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.policy_image[0].image != ''}
                      <img src="{$item.policy_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.policy_name}</p>
                      <p>{$item.manage.policy_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.policy_image[1].image != ''}
                          <img src="{$item.policy_image[1].image}"style="max-width: 180px; max-height: 180px;" />
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.policy_image[2].image != ''}
                          <img src="{$item.policy_image[2].image}"style="max-width: 180px; max-height: 180px;" />
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.person_image[0].image != ''}
                      <img src="{$item.person_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.person_name}</p>
                      <p>{$item.manage.person_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.person_image[1].image != ''}
                          <img src="{$item.person_image[1].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.person_image[2].image != ''}
                          <img src="{$item.person_image[2].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.model_image[0].image != ''}
                      <img src="{$item.model_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.model_name}</p>
                      <p>{$item.manage.model_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.model_image[1].image != ''}
                          <img src="{$item.model_image[1].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.model_image[2].image != ''}
                          <img src="{$item.model_image[2].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                      {if $item.activity_image[0].image != ''}
                      <img src="{$item.activity_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.activity_name}</p>
                      <p>{$item.manage.activity_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.activity_image[1].image != ''}
                          <img src="{$item.activity_image[1].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.activity_image[2].image != ''}
                          <img src="{$item.activity_image[2].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                    </div>
                  </div>
                  <div class="news">
                    <div class="thumbnail">
                    {if $item.public_image[0].image != ''}
                      <img src="{$item.public_image[0].image}" style="max-width: 180px; max-height: 180px;" />
                    {/if}
                    </div>
                    <div class="detail-new">
                      <p>{$item.manage.public_name}</p>
                      <p>{$item.manage.public_description|nl2br}</p>
                    </div>
                    <div class="detail-img">
                      <div class="thumbnail">
                        {if $item.public_image[1].image != ''}
                          <img src="{$item.public_image[1].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                      <div class="thumbnail">
                        {if $item.public_image[2].image != ''}
                          <img src="{$item.public_image[2].image}" style="max-width: 180px; max-height: 180px;"/>
                        {/if}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
{/block}
{block name=script}
	<script>
		
        $(document).ready(function () {

            $("#open_start").timepicker();
            $("#open_end").timepicker();

        });
    </script>
{/block}