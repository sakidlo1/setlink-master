<form name="add_edit" action="{$base_url}backend/{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_data();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
  <input type="hidden" name="item_garbage_type_id" id="item_garbage_type_id" value="{$item.garbage_type_id}">
  <input type="hidden" name="item_garbage_category_id" id="item_garbage_category_id" value="{$item.garbage_category_id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มรายละเอียดของขยะ
          {elseif $action == 'edit'}
            แก้ไขรายละเอียดของขยะ
          {elseif $action == 'delete'}
            ยืนยันการลบรายละเอียดของขยะ
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form">
          <div class="wrap-input">
            <label>ชื่อรายละเอียดของขยะ </label>
            <input
              value="{$item.name}"
              type="text"
              class="form-control"
              name="name" id="name"
            />
          </div>
          <label id="name_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุชื่อ</small>
            </label>
          <div class="wrap-input">
            <label>คำอธิบาย </label>
            <textarea name="description" id="description" class="form-control">{$item.description}</textarea>
          </div>
          <div class="wrap-input">
            <label>ประเภทขยะ </label>
            <select name="garbage_type_id" id="garbage_type_id" class="form-control">
              <option value="">[ เลือกประเภทขยะ ]</option>
              {foreach $garbage_type as $garbage_type_item}
                <option value="{$garbage_type_item.id}"{if $item.garbage_type_id == $garbage_type_item.id} selected="selected"{/if}>{$garbage_type_item.name}</option>
              {/foreach}
            </select>
          </div>
          <label id="garbage_type_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกประเภทขยะ</small>
            </label>
          <div class="wrap-input">
            <label>หมวดหมู่ขยะ </label>
            <select name="garbage_category_id" id="garbage_category_id" class="form-control">
              <option value="">[ เลือกหมวดหมู่ขยะ ]</option>
            </select>
          </div>
          <label id="garbage_category_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกหมวดหมู่ขยะ</small>
            </label>
          <div class="wrap-input">
            <label>ภาพตัวอย่าง</label>
            <div
              class="upload-wrap upload-mulitiple-wrap upload-modal-group"
            >
              <div class="upload-content">
                <div class="uploadpreview 21{if $item.image[0].image != ''} active-image{/if}"{if $item.image[0].image != ''} style="background-image:url('{$item.image[0].image}');"{/if}></div>
                <div class="delete-img 21{if $item.image[0].image != ''} show{/if}">X</div>
                <input type="hidden" name="file_image[]" id="file_21" value="{if $item.image[0].image != ''}{$item.image[0].image}{/if}">
                <input
                  id="21"
                  type="file"
                  accept="image/*"
                  style="display: none"
                />
                <label
                  for="21"
                  style="
                    background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                  "
                ></label>
              </div>
              <div class="upload-content">
                <div class="uploadpreview 20{if $item.image[1].image != ''} active-image{/if}"{if $item.image[1].image != ''} style="background-image:url('{$item.image[1].image}');"{/if}></div>
                <div class="delete-img 20{if $item.image[1].image != ''} show{/if}">X</div>
                <input type="hidden" name="file_image[]" id="file_20" value="{if $item.image[1].image != ''}{$item.image[1].image}{/if}">
                <input
                  id="20"
                  type="file"
                  accept="image/*"
                  style="display: none"
                />
                <label
                  for="20"
                  style="
                    background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                  "
                ></label>
              </div>
              <div class="upload-content">
                <div class="uploadpreview 19{if $item.image[2].image != ''} active-image{/if}"{if $item.image[2].image != ''} style="background-image:url('{$item.image[2].image}');"{/if}></div>
                <div class="delete-img 19{if $item.image[2].image != ''} show{/if}">X</div>
                <input type="hidden" name="file_image[]" id="file_19" value="{if $item.image[2].image != ''}{$item.image[2].image}{/if}">
                <input
                  id="19"
                  type="file"
                  accept="image/*"
                  style="display: none"
                />
                <label
                  for="19"
                  style="
                    background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                  "
                ></label>
              </div>
            </div>
          </div>
          <div class="wrap-input dpflex-special">
            <label>จำนวน</label>
            <input name="kg_qty" id="kg_qty" value="{$item.kg_qty}" type="number" class="form-control" />
            <label>ชิ้น เท่ากับ 1 ก.ก.</label>
          </div>
          <label id="kg_qty_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุจำนวนชิ้น ต่อ 1 ก.ก.</small>
            </label>
          <div class="wrap-input">
              <label>สถานะ</label>
              <select name="status" id="status" class="form-control">
                <option value="">[ เลือกสถานะ ]</option>
                <option value="Y"{if $item.status == 'Y'} selected="selected"{/if}>เปิดใช้งาน</option>
                <option value="N"{if $item.status == 'N'} selected="selected"{/if}>ปิดใช้งาน</option>
              </select>
            </div>
          <label id="status_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกสถานะ</small>
            </label>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>