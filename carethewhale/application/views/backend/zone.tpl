{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>โซน</h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}backend"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">จัดการบริษัท </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">โซน </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <div class="filter-section clearfix">
          <div class="filter-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="fName">ค้นหา : &nbsp;&nbsp;&nbsp;</label>
                        <input class="form-control" id="fName" type="text" size="10" />
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <label for="fStatus">สถานะ : &nbsp;&nbsp;&nbsp;</label>
                        <select class="form-control" id="fStatus">
                            <option value="">[ เลือกสถานะ ]</option>
                            <option value="Y">เปิดใช้งาน</option>
                            <option value="N">ปิดใช้งาน</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="button" value="Search" id="fSearch" />
                    </div>
                </div>
            </div>
          <div class="filter-right">
            <button type="button" onclick="call_modal('add');">
              <img src="{$image_url}theme/default/assets/images/register/add.svg" />
            </button>
          </div>
        </div>
        <div class="table-section">
          	<table id="data-tables" class="display table-button-no-style" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="80">รหัสโซน</th>
						<th width="200">ชื่อโซน</th>
						<th>รายละเอียดโซน</th>
						<th width="100">Rating</th>
						<th>วันที่</th>
						<th width="100">สถานะ</th>
						<th width="150"></th>
					</tr>
				</thead>
			</table>
        </div>
        <div class="modal fade modal-with-form" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	      <div class="modal-dialog modal-dialog-scrollable" role="document">
	        <div class="modal-content">
	          
	        </div>
	      </div>
	    </div>
{/block}
{block name=script}
	<script>
		function call_modal(action, id)
		{
			if(typeof id == 'undefined') {
				id = 0
			}

			if(action == 'delete') {
				$('#modalAction').addClass('modal-delete');
			} else {
				$('#modalAction').removeClass('modal-delete');
			}

			$.get('{$base_url}backend/{$page}/form/' + action + '/' + id, function( data ) {
			  $('#modalAction .modal-content').html(data);
			  $('#modalAction').modal('show');
			});
		}

		function check_data()
		{
			$('#name_req').hide();
			$('#rating_req').hide();
			$('#status_req').hide();
			$('.has-error').removeClass('has-error');
			
			with(document.add_edit)
			{
				if(name.value=="")
				{
					$('#name_req').show();
					$('#name_req').parent('div').addClass('has-error');
					$('#name').focus();
					return false;
				}
				else if(rating[0].checked==false && rating[1].checked==false && rating[2].checked==false && rating[3].checked==false && rating[4].checked==false)
				{
					$('#rating_req').show();
					$('#rating_req').parent('div').addClass('has-error');
					$('input[name="rating"]')[0].focus();
					return false;
				}
				else if(status.value=="")
				{
					$('#status_req').show();
					$('#status_req').parent('div').addClass('has-error');
					$('#status').focus();
					return false;
				}
			}
		}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "asc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                	{ orderable: false, targets: -1 },
                	{  className: "status", targets: -2 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                responsive: true,
				bPaginate: true,
				bLengthChange: true,
				bInfo: true,
				bAutoWidth: false,
				language: {
				paginate: {
				  previous: "<img src='{$image_url}theme/default/assets/images/template/previous.png'>",
				  next: "<img src='{$image_url}theme/default/assets/images/template/next.png' />",
				},
				},
                'dom': '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/load_data',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'id'
                    },
                    {
                        'data': 'name'
                    },
                    {
                        'data': 'description'
                    },
                    {
                        'data': 'rating',
                        'render': function (data, type, full, meta) {
                            
                            var html = '';

                            for(var i = 1; i <= data; i++)
                            {
                            	html += '<img src="{$image_url}theme/default/assets/images/template/Star.png" />';
                            }

                            return html;
                        }
                    },
                    {
                        'data': 'updated_on',
                        'render': function (data, type, full, meta) {
                            return ((data != '' && data != null) ? moment(data).format('DD.MM.YYYY HH.mm') : '-');
                        }
                    },
                    {
                        'data': 'status',
                        'render': function (data, type, full, meta) {
                            return ((data == 'Y') ? '<span style="background: #09b66d"></span> เปิดใช้งาน' : '<span style="background: #BF3A0D"></span> ปิดใช้งาน');
                        }
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}backend/{$page}/detail/' + data + '"><img src="{$image_url}theme/default/assets/images/template/dots-icon.png" /></a><button type="button" onclick="call_modal(\'edit\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Edit.png" /></button><button type="button" onclick="call_modal(\'delete\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Delete.png" /></button>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.columns(1).search($('#fStatus').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}