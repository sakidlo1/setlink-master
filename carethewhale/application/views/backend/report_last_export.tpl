<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
รายงานการจัดการปลายทาง
<br />
<br />
{foreach $data as $key => $item}
{$item.text}
<br />
<br />
<table style="width: 100%; border: 1px #000000 solid;" border="1">
  <th>
    ลำดับที่
  </th>
  <th>
    รายการขยะ
  </th>
  <th>
    วิธีการจัดการ
  </th>
  <th>
    บริษัท/หน่วยงาน
  </th>
  <th>
    ปริมาณขยะ (กก.)
  </th>
  <th>
    Kg.CO<sub>2</sub>eq
  </th>
  <th>
    จำนวนต้นไม้
  </th>
  {foreach $item.data as $no => $value}
      <tr>
        <td>{($no + 1)}</td>
        <td>{$value.name}</td>
        <td>{$value.disposal}</td>
        <td>{$value.disposal_company}</td>
        <td>{$value.total_weight|number_format:2:".":","}</td>
        <td>{$value.total_cf|number_format:2:".":","}</td>
        <td>{($value.total_cf/9)|number_format:0:".":","}</td>
      </tr>
  {/foreach}
</table>
<br />
<br />
{/foreach}
</body>
</html>