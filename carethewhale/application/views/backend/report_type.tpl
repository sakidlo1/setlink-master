{extends file="backend/layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <link rel="stylesheet" href="{$image_url}theme/default/css/report.css" />
    <div class="main-content">
          <div>
            <div class="wrap-header-txt left">
              <h1>รายงาน</h1>
            </div>
            <div class="wrap-header-txt float-right">
              <!-- <h1>รายงานที่ 20 ลูกบ้าน</h1> -->
            </div>
          </div>

          <div class="breadcrumb-section clearfix">
            <div class="nav-section-left">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend"><img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="{$base_url}backend/report">รายงาน</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <a href="#">รายงานตามประเภทขยะ</a>
                  </li>
                </ol>
              </nav>
            </div>
            <div class="clear-fix"></div>
          </div>
          <div class="dashboard-section">
            <div class="report-boxed">
              <form name="report" method="get">
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">รายงานผลรวมขยะ </span>
                </div>
                <div class="right text-right">
                  <select class="form-control category-gerbarg-boxed" name="garbage" onchange="document.report.submit();">
                    <option value="non_recycle"{if $smarty.get.garbage == 'non_recycle' || $smarty.get.garbage == ''} selected="selected"{/if}>Non Recycle</option>
                    <option value="recycle"{if $smarty.get.garbage == 'recycle'} selected="selected"{/if}>ขยะรีไซเคิล</option>
                    <option value="plastic"{if $smarty.get.garbage == 'plastic'} selected="selected"{/if}>ขยะพลาสติก</option>
                  </select>
                  <button type="button" onclick="window.print();">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Print.png" />
                  </button>
                  <button type="submit" name="export" value="Y">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Import.png" />
                  </button>
                </div>
              </div>
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">
                    {if $smarty.get.garbage == 'non_recycle' || $smarty.get.garbage == ''}
                      Non Recycle 
                    {elseif $smarty.get.garbage == 'recycle'}
                      ขยะรีไซเคิล
                    {elseif $smarty.get.garbage == 'plastic'}
                      ขยะพลาสติก
                    {/if}
                  </span>
                </div>
                <div class="float-right">
                  <select class="form-control quarter-boxed" name="type" onchange="document.report.submit();">
                    <option value="year"{if $smarty.get.type == 'year' || $smarty.get.type == ''} selected="selected"{/if}>รายปี</option>
                    <option value="quater"{if $smarty.get.type == 'quater'} selected="selected"{/if}>รายไตรมาส</option>
                    <option value="month"{if $smarty.get.type == 'month'} selected="selected"{/if}>รายเดือน</option>
                    <option value="range"{if $smarty.get.type == 'range'} selected="selected"{/if}>Year to Date</option>
                  </select>
                  <div hidden class="wrap-date-picker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                  </div>
                  {if $smarty.get.type == 'year' || $smarty.get.type == ''}
                  <div id="div-year" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year_start" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_start == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                    <span>-</span>
                    <select class="form-control quarter-boxed" name="year_end" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_end == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'quater' || $smarty.get.type == 'month'}
                  <div id="div-month" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'range'}
                  <div class="wrap-date-picker" id="div-datepicker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                    <input class="form-control daterange" name="dates" value="{$smarty.get.dates}" />
                  </div>
                  {/if}
                </div>
              </div>
            </form>
            </div>
            {foreach $data as $key => $item}
                <div class="report-boxed">
                  <div class="section-filter-date-action clearfix">
                    <div class="left">
                      <span class="header-font-size">{$item.text}</span>
                    </div>
                    <div class="right text-right">

                    </div>
                  </div>
                  <div class="section-main-tree">
                    <div class="inner-wrap">
                      <div class="row">
                        <div class="col-7 align-self-center">
                          <div class="chartdiv" id="chartdiv_{$key}"></div>
                        </div>
                        <div class="col-5 align-self-center edit-padding-table-10px">
                          <table>
                            <tbody>
                              <tr>
                                <td width="250">
                                  <p style="margin-top: 75px;">รายการขยะ</p>
                                </td>
                                <td>
                                  <img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png" alt="co2-icon">
                                  <p>Kg.CO<sub>2</sub>eq</p>
                                </td>
                                <td>
                                  <img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png" alt="tree-icon">
                                  <p>จำนวน<br />ต้นไม้</p>
                                </td>
                              </tr>
                              {foreach $item.data as $no => $value}
                                {if $no < 10}
                                <tr>
                                  <td>
                                    <p><small>{$value.name}</small></p>
                                  </td>
                                  <td>
                                    <p>{$value.total_cf|number_format:2:".":","}</p>
                                  </td>
                                  <td>
                                    <p>{($value.total_cf/9)|number_format:0:".":","}</p>
                                  </td>
                                </tr>
                                {/if}
                              {/foreach}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            {/foreach}
            <!-- section custome table -->
            <hr />
            <div class="section-table-report">
              <span class="header-font-size">รายงานผลรวมขยะ </span>
              <span class="header-font-size"> 
                {if $smarty.get.garbage == 'non_recycle' || $smarty.get.garbage == ''}
                  Non Recycle 
                {elseif $smarty.get.garbage == 'recycle'}
                  ขยะรีไซเคิล
                {elseif $smarty.get.garbage == 'plastic'}
                  ขยะพลาสติก
                {/if}
              </span>
              <br />
              <br />
              {foreach $data as $key => $item}
                <span class="header-font-size">{$item.text} </span>
                  <table>
                    <thead>
                      <th>
                        ลำดับที่
                      </th>
                      <th>
                        รายการขยะ
                      </th>
                      <th>
                        ประเภทขยะ
                      </th>
                      <th>
                        ปริมาณขยะ (กก.)
                      </th>
                      <th>
                        Kg.CO<sub>2</sub>eq
                      </th>
                      <th>
                        จำนวนต้นไม้
                      </th>
                    </thead>
                    <tbody>
                        {$total_1 = 0}
						{$total_2 = 0}
						{$total_3 = 0}
                      {foreach $item.data as $no => $value}
                          <tr>
                            <td>{($no + 1)}</td>
                            <td>{$value.name}</td>
                            <td>{$value.type}</td>
                            <td>{$value.total_weight|number_format:2:".":","}</td>
                            <td>{$value.total_cf|number_format:2:".":","}</td>
                            <td>{($value.total_cf/9)|number_format:0:".":","}</td>
                          </tr>
                        {$total_1 = $total_1 + $value.total_weight}
						{$total_2 = $total_2 + $value.total_cf}
						{$total_3 = $total_3 + ($value.total_cf/9)}
                      {/foreach}
                      <tr>
						<td colspan="3">รวม</td>
						<td>{$total_1|number_format:2:".":","}</td>
						<td>{$total_2|number_format:2:".":","}</td>
						<td>{$total_3|number_format:0:".":","}</td>
					  </tr>
                    </tbody>
                  </table>
              {/foreach}
            </div>
            <!-- END REPORT SECTION -->
          </div>
        </div>
{/block}
{block name="script"}
  <style>
    .chartdiv {
      width: 100%;
      height: 450px;
    }
    .report-boxed .section-main-tree .edit-padding-table-10px table tr td {
      padding: 5px 10px;
    }
  </style>
  <script>
        $('.daterange').daterangepicker({
          locale: {
          format: 'YYYY-MM-DD'
        },
          {if $smarty.get.dates == ''}
              startDate: moment().startOf('month'),
              endDate: moment().endOf('month')
          {/if}
        }).on('apply.daterangepicker', function(ev, picker) {
            document.report.submit();
        });

        am4core.ready(function () {

          {foreach $data as $key => $item}
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("chartdiv_{$key}", am4charts.PieChart);

            // Add data
            chart.data = [
              {foreach $item.data as $no => $value} 
                {if $no < 10}
                  {
                    "country": "{$value.name}",
                    "litres": {$value.total_weight}
                  },
                {/if}
              {/foreach}
            ];

            // Set inner radius
            chart.innerRadius = am4core.percent(30);

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "litres";
            pieSeries.dataFields.category = "country";
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;
            pieSeries.alignLabels = true;
            pieSeries.labels.template.maxWidth = 120;
            pieSeries.labels.template.wrap = true;
            pieSeries.labels.template.padding(1, 1, 1, 1);
            pieSeries.labels.template.fontSize = 16;
		    //pieSeries.alignLabels.template.fontsize = 18;

          {/foreach}

        });

  </script>
{/block}