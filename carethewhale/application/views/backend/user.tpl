{extends file="backend/layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>ผู้ใช้งาน</h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}backend"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">ผู้ใช้งาน </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <div class="filter-section clearfix">
          <div class="filter-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="fName">ค้นหา : &nbsp;&nbsp;&nbsp;</label>
                        <input class="form-control" id="fName" type="text" size="10" />
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <label for="fStatus">สถานะ : &nbsp;&nbsp;&nbsp;</label>
                        <select class="form-control" id="fStatus">
                            <option value="">[ เลือกสถานะ ]</option>
                            <option value="Y">เปิดใช้งาน</option>
                            <option value="N">ปิดใช้งาน</option>
                        </select>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="button" value="Search" id="fSearch" />
                    </div>
                </div>
            </div>
          <div class="filter-right">
            <button type="button" onclick="call_modal('add');">
              <img src="{$image_url}theme/default/assets/images/register/add.svg" />
            </button>
          </div>
        </div>
        <div class="table-section">
          	<table id="data-tables" class="display table-button-no-style" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>รหัส</th>
						<th>ชื่อ - นามสกุล</th>
						<th>อีเมล</th>
						<th>วันที่ใช้งานล่าสุด</th>
						<th>Role</th>
						<th></th>
					</tr>
				</thead>
			</table>
        </div>
        <div class="modal fade modal-with-form" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	      <div class="modal-dialog modal-dialog-scrollable" role="document">
	        <div class="modal-content">
	          
	        </div>
	      </div>
	    </div>
{/block}
{block name=script}
	<script>
		function call_modal(action, id)
		{
			if(typeof id == 'undefined') {
				id = 0
			}

			if(action == 'delete') {
				$('#modalAction').addClass('modal-delete');
			} else {
				$('#modalAction').removeClass('modal-delete');
			}

			$.get('{$base_url}backend/{$page}/form/' + action + '/' + id, function( data ) {
			  $('#modalAction .modal-content').html(data);
			  $('#modalAction').modal('show');
			});
		}

		var email_exists = true;
		function check_email_exists()
		{
			if($('#action').val() == 'delete')
			{
				document.add_edit.submit();
			}
			else if($('#email').val() != "") {
				$.post(
					'{$base_url}backend/{$page}/check_email_exists/' + $('#id').val(),
					{ email: $('#email').val() },
					function( data ) {
						if(data.status == false) {
				    		email_exists = false;
				    		if(check_data() != false) {
				    			document.add_edit.submit();
				    		}
				    	} else {
				    		return check_data();
				    	}
					}, 
					"json"
				);
				return false;
			} else {
				return check_data();
			}
		}

		function check_data()
		{
			$('#name_req').hide();
			$('#email_req').hide();
			$('#email_exists').hide();
			$('#password_req').hide();
			$('#type_req').hide();
			$('.has-error').removeClass('has-error');
			
			with(document.add_edit)
			{
				if(name.value=="")
				{
					$('#name_req').show();
					$('#name_req').parent('div').addClass('has-error');
					$('#name').focus();
					return false;
				}
				else if(email.value=="")
				{
					$('#email_req').show();
					$('#email_req').parent('div').addClass('has-error');
					$('#email').focus();
					return false;
				}
				else if(email_exists == true)
				{
					$('#email_exists').show();
					$('#email_exists').parent('div').addClass('has-error');
					$('#email').focus();
					return false;
				}
				else if($('#action').val() == 'add' && password.value=="")
				{
					$('#password_req').show();
					$('#password_req').parent('div').addClass('has-error');
					$('#password').focus();
					return false;
				}
				else if(type.value=="")
				{
					$('#type_req').show();
					$('#type_req').parent('div').addClass('has-error');
					$('#type').focus();
					return false;
				}
			}
		}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "asc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                	{ orderable: false, targets: -1 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                responsive: true,
				bPaginate: true,
				bLengthChange: true,
				bInfo: true,
				bAutoWidth: false,
				language: {
				paginate: {
				  previous: "<img src='{$image_url}theme/default/assets/images/template/previous.png'>",
				  next: "<img src='{$image_url}theme/default/assets/images/template/next.png' />",
				},
				},
                'dom': '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
                'ajax': {
                    'url': '{$base_url}backend/{$page}/load_data',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'id'
                    },
                    {
                        'data': 'name'
                    },
                    {
                        'data': 'email',
                        'name': 'อีเมล'
                    },
                    {
                        'data': 'last_login',
                        'render': function (data, type, full, meta) {
                            return ((data != '' && data != null) ? moment(data).format('DD.MM.YYYY HH.mm') : '-');
                        }
                    },
                    {
                        'data': 'type',
                        'render': function (data, type, full, meta) {
                            return ((data == 'A') ? 'Administrator' : 'Viewer');
                        }
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}backend/{$page}/detail/' + data + '"><img src="{$image_url}theme/default/assets/images/template/dots-icon.png" /></a><button type="button" onclick="call_modal(\'edit\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Edit.png" /></button><button type="button" onclick="call_modal(\'delete\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Delete.png" /></button>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.columns(1).search($('#fStatus').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}