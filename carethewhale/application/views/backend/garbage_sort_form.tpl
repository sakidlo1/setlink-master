<form name="add_edit" action="{$base_url}backend/{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_data();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มการคัดแยกย่อย
          {elseif $action == 'edit'}
            แก้ไขการคัดแยกย่อย
          {elseif $action == 'delete'}
            ยืนยันการลบการคัดแยกย่อย
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form">
          <div class="wrap-input">
            <label>ชื่อการคัดแยกย่อย </label>
            <input
              value="{$item.name}"
              type="text"
              class="form-control"
              name="name" id="name"
            />
          </div>
          <label id="name_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุชื่อ</small>
            </label>
          <div class="wrap-input">
            <label>รายละเอียด </label>
            <textarea name="description" id="description" class="form-control">{$item.description}</textarea>
          </div>
          <div class="wrap-input inline-label upload-multiple">
              <label>ภาพตัวอย่าง</label>
              <div class="upload-wrap upload-single-large">
                <div class="upload-content">
                  <div class="uploadpreview 21{if $item.image != ''} active-image{/if}"{if $item.image != ''} style="background-image:url('{$item.image}');"{/if}></div>
                  <div class="delete-img 21{if $item.image != ''} show{/if}">X</div>
                  <input type="hidden" name="file_image[]" id="file_21" value="{if $item.image != ''}{$item.image}{/if}">
                  <input
                    id="21"
                    type="file"
                    accept="image/*"
                    style="display: none"
                  />
                  <label
                    for="21"
                    style="
                      background-image: url('{$image_url}theme/default/assets/images/register/upload-logo.png');
                    "
                  ></label>
                </div>
              </div>
            </div>
          <div class="wrap-input">
              <label>สถานะ</label>
              <select name="status" id="status" class="form-control">
                <option value="">[ เลือกสถานะ ]</option>
                <option value="Y"{if $item.status == 'Y'} selected="selected"{/if}>เปิดใช้งาน</option>
                <option value="N"{if $item.status == 'N'} selected="selected"{/if}>ปิดใช้งาน</option>
              </select>
            </div>
          <label id="status_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกสถานะ</small>
            </label>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>