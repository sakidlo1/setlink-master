<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{$page_name} - {$site_name} : {$company_name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/main.css" />
    <link href="{$image_url}theme/default/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/bootstrap-datetimepicker.min.css" />
  </head>
  <body>
    <div class="login-page height100">
      <div class="login-left text-center with-overflow">
        <div class="over-flow-scroll-register">
          <div class="login-left-boxed">
            <div class="register-section">
              <a href="#" onclick="toBack()"
                ><img
                  src="{$image_url}theme/default/assets/images/forgot/eva_arrow-ios-back-fill.png"
                  alt="back-icon"
              /></a>
              <span>สมัครสมาชิก</span>
            </div>
            <div class="wizard">
              <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active">
                    <a
                      href="#step1"
                      data-toggle="tab"
                      aria-controls="step1"
                      role="tab"
                      aria-expanded="true"
                      ><span class="round-tab">1 </span></a
                    >
                  </li>
                  <li role="presentation" class="disabled">
                    <a
                      href="#step2"
                      data-toggle="tab"
                      aria-controls="step2"
                      role="tab"
                      aria-expanded="false"
                      ><span class="round-tab">2</span></a
                    >
                  </li>
                  <li role="presentation" class="disabled">
                    <a
                      href="#step3"
                      data-toggle="tab"
                      aria-controls="step3"
                      role="tab"
                      aria-expanded="false"
                      ><span class="round-tab">3</span></a
                    >
                  </li>
                </ul>
              </div>
              <script>
                var email_exists = true;
                function check_email_exists(check)
                {
                  if($('#email').val() != "" && $('#step1').is(":visible") && email_exists == true) {
                    $.post(
                      '{$base_url}{$page}/check_email_exists',
                      { email: $('#email').val() },
                      function( data ) {
                        if(data.status == false) {
                          email_exists = false;
                          check_data(true, true);
                        } else {
                          check_data(true);
                        }
                      }, 
                      "json"
                    );
                    return false;
                  } else {
                    return check_data(check);
                  }
                }

                function check_data(check, check2)
                {
                  if(typeof check2 == 'undefined')
                  {
                    check2 = false;
                  }

                  $('#logo_req').hide();
                  $('#name_req').hide();
                  $('#contact_name_req').hide();
                  $('#company_type_id_req').hide();
                  $('#zone_id_req').hide();
                  $('#open_day_req').hide();
                  $('#open_start_req').hide();
                  $('#open_end_req').hide();
                  $('#guest_qty_req').hide();
                  $('#staff_qty_req').hide();
                  $('#size_req').hide();
                  $('#address_req').hide();
                  $('#tel1_req').hide();
                  $('#email_req').hide();
                  $('#email_exists').hide();
                  $('#password_req').hide();
                  $('#confirm_password_req').hide();
                  $('#confirm_password_inc').hide();
                  $('.garbage_type_id_req').hide();
                  $('.garbage_type_weight_req').hide();
                  $('#garbage_type_description_req').hide();
                  $('#policy_name_req').hide();
                  //$('#policy_description_req').hide();
                  $('#person_name_req').hide();
                  //$('#person_description_req').hide();
                  $('#model_name_req').hide();
                  //$('#model_description_req').hide();
                  $('#activity_name_req').hide();
                  //$('#activity_description_req').hide();
                  $('#public_name_req').hide();
                  //$('#public_description_req').hide();
                  $('.has-error').removeClass('has-error');
                  
                  with(document.add_edit)
                  {
                    if($('#file_01').val() == "" && $('#step1').is(":visible"))
                    {
                      $('#logo_req').show();
                      $('#logo_req').parent('div').addClass('has-error');
                      $('.over-flow-scroll-register').scrollTop(0);
                      return false;
                    }
                    else if(name.value=="" && $('#step1').is(":visible"))
                    {
                      $('#name_req').show();
                      $('#name_req').parent('div').addClass('has-error');
                      $('#name').focus();
                      return false;
                    }
                    else if(contact_name.value=="" && $('#step1').is(":visible"))
                    {
                      $('#contact_name_req').show();
                      $('#contact_name_req').parent('div').addClass('has-error');
                      $('#contact_name').focus();
                      return false;
                    }
                    else if(company_type_id.value=="" && $('#step1').is(":visible"))
                    {
                      $('#company_type_id_req').show();
                      $('#company_type_id_req').parent('div').addClass('has-error');
                      $('#company_type_id').focus();
                      return false;
                    }
                    else if(zone_id.value=="" && $('#step1').is(":visible"))
                    {
                      $('#zone_id_req').show();
                      $('#zone_id_req').parent('div').addClass('has-error');
                      $('#zone_id').focus();
                      return false;
                    }
                    else if($('.form-check-input:checked').length == 0 && $('#step1').is(":visible"))
                    {
                      $('#open_day_req').show();
                      $('#open_day_req').parent('div').addClass('has-error');
                      $('.form-check-input')[0].focus();

                      return false;
                    }
                    else if(open_start.value=="" && $('#step1').is(":visible"))
                    {
                      $('#open_start_req').show();
                      $('#open_start_req').parent('div').addClass('has-error');
                      $('#open_start').focus();
                      return false;
                    }
                    else if(open_end.value=="" && $('#step1').is(":visible"))
                    {
                      $('#open_end_req').show();
                      $('#open_end_req').parent('div').addClass('has-error');
                      $('#open_end').focus();
                      return false;
                    }
                    else if(guest_qty.value=="" && $('#step1').is(":visible"))
                    {
                      $('#guest_qty_req').show();
                      $('#guest_qty_req').parent('div').addClass('has-error');
                      $('#guest_qty').focus();
                      return false;
                    }
                    else if(staff_qty.value=="" && $('#step1').is(":visible"))
                    {
                      $('#staff_qty_req').show();
                      $('#staff_qty_req').parent('div').addClass('has-error');
                      $('#staff_qty').focus();

                      return false;
                    }
                    else if(size.value=="" && $('#step1').is(":visible"))
                    {
                      $('#size_req').show();
                      $('#size_req').parent('div').addClass('has-error');
                      $('#size').focus();
                      return false;
                    }
                    else if(address.value=="" && $('#step1').is(":visible"))
                    {
                      $('#address_req').show();
                      $('#address_req').parent('div').addClass('has-error');
                      $('#address').focus();
                      return false;
                    }
                    else if(tel1.value=="" && $('#step1').is(":visible"))
                    {
                      $('#tel1_req').show();
                      $('#tel1_req').parent('div').addClass('has-error');
                      $('#tel1').focus();
                      return false;
                    }
                    else if(email.value=="" && $('#step1').is(":visible"))
                    {
                      $('#email_req').show();
                      $('#email_req').parent('div').addClass('has-error');
                      $('#email').focus();
                      return false;
                    }
                    else if(email_exists == true && $('#step1').is(":visible"))
                    {
                      $('#email_exists').show();
                      $('#email_exists').parent('div').addClass('has-error');
                      $('#email').focus();
                      return false;
                    }
                    else if(password.value=="" && $('#step1').is(":visible"))
                    {
                      $('#password_req').show();
                      $('#password_req').parent('div').addClass('has-error');
                      $('#password').focus();
                      return false;
                    }
                    else if(confirm_password.value=="" && $('#step1').is(":visible"))
                    {
                      $('#confirm_password_req').show();
                      $('#confirm_password_req').parent('div').addClass('has-error');
                      $('#confirm_password').focus();
                      return false;
                    }
                    else if(password.value!=confirm_password.value && $('#step1').is(":visible"))
                    {
                      $('#confirm_password_inc').show();
                      $('#confirm_password_inc').parent('div').addClass('has-error');
                      $('#confirm_password').focus();
                      return false;
                    }
                    else if(check2 == true && email_exists == false && $('#step1').is(":visible"))
                    {
                      var active = $(".wizard .nav-tabs li.active");
                      active.next().removeClass("disabled");
                      nextTab(active);
                      return false;
                    }
                    /*
                    else if($('select[name="garbage_type_id[]"]').filter(function() { return this.value == ""; }).length > 0 && $('#step2').is(":visible"))
                    {
                      var select = $('select[name="garbage_type_id[]"]').filter(function() { return this.value == ""; });
                      $(select).parent().parent().find('.garbage_type_id_req').show();
                      $(select).parent().parent().find('.garbage_type_id_req').parent('div').addClass('has-error');
                      $(select)[0].focus();
                      return false;
                    }
                    else if($('input[name="garbage_type_weight[]"]').filter(function() { return this.value == ""; }).length > 0 && $('#step2').is(":visible"))
                    {
                      var input = $('input[name="garbage_type_weight[]"]').filter(function() { return this.value == ""; });
                      $(input).parent().parent().find('.garbage_type_weight_req').show();
                      $(input).parent().parent().find('.garbage_type_weight_req').parent('div').addClass('has-error');
                      $(input)[0].focus();
                      return false;
                    }
                    */
                    else if(garbage_type_description.value=="" && $('#step2').is(":visible"))
                    {
                      $('#garbage_type_description_req').show();
                      $('#garbage_type_description_req').parent('div').addClass('has-error');
                      $('#garbage_type_description').focus();
                      return false;
                    }
                    else if(policy_name.value=="" && $('#step2').is(":visible"))
                    {
                      $('#policy_name_req').show();
                      $('#policy_name_req').parent('div').addClass('has-error');
                      $('#policy_name').focus();
                      return false;
                    }
                    /*
                    else if(policy_description.value=="" && $('#step2').is(":visible"))
                    {
                      $('#policy_description_req').show();
                      $('#policy_description_req').parent('div').addClass('has-error');
                      $('#policy_description').focus();
                      return false;
                    }
                    */
                    else if(person_name.value=="" && $('#step2').is(":visible"))
                    {
                      $('#person_name_req').show();
                      $('#person_name_req').parent('div').addClass('has-error');
                      $('#person_name').focus();
                      return false;
                    }
                    /*
                    else if(person_description.value=="" && $('#step2').is(":visible"))
                    {
                      $('#person_description_req').show();
                      $('#person_description_req').parent('div').addClass('has-error');
                      $('#person_description').focus();
                      return false;
                    }
                    */
                    else if(model_name.value=="" && $('#step3').is(":visible"))
                    {
                      $('#model_name_req').show();
                      $('#model_name_req').parent('div').addClass('has-error');
                      $('#model_name').focus();
                      return false;
                    }
                    /*
                    else if(model_description.value=="" && $('#step3').is(":visible"))
                    {
                      $('#model_description_req').show();
                      $('#model_description_req').parent('div').addClass('has-error');
                      $('#model_description').focus();
                      return false;
                    }
                    */
                    else if(activity_name.value=="" && $('#step3').is(":visible"))
                    {
                      $('#activity_name_req').show();
                      $('#activity_name_req').parent('div').addClass('has-error');
                      $('#activity_name').focus();
                      return false;
                    }
                    /*
                    else if(activity_description.value=="" && $('#step3').is(":visible"))
                    {
                      $('#activity_description_req').show();
                      $('#activity_description_req').parent('div').addClass('has-error');
                      $('#activity_description').focus();
                      return false;
                    }
                    */
                    else if(public_name.value=="" && $('#step3').is(":visible"))
                    {
                      $('#public_name_req').show();
                      $('#public_name_req').parent('div').addClass('has-error');
                      $('#public_name').focus();
                      return false;
                    }
                    /*
                    else if(public_description.value=="" && $('#step3').is(":visible"))
                    {
                      $('#public_description_req').show();
                      $('#public_description_req').parent('div').addClass('has-error');
                      $('#public_description').focus();
                      return false;
                    }
                    */
                    else if($('#is_accept').prop("checked") == false && $('#step3').is(":visible"))
                    {
                      $('#is_accept_req').show();
                      $('#is_accept_req').parent('div').addClass('has-error');
                      return false;
                    }
                    else if(check == false && $('#step3').is(":visible"))
                    {
                      document.add_edit.submit();
                    }
                  }
                }
              </script>
              <form name="add_edit" method="post" role="form" class="login-box" onsubmit="return check_email_exists(false);">
                <div class="tab-content" id="main_form">
                  <!-- TAB 1  -->
                  <div class="tab-pane active" role="tabpanel" id="step1">
                    <!-- UPLOAD -->
                    <div class="upload-wrap">
                      <div class="upload-content">
                        <div class="uploadpreview 01"></div>
                        <div class="delete-img 01">X</div>
                        <input type="hidden" name="file_logo[]" id="file_01" value="">
                        <input
                          id="01"
                          type="file"
                          accept="image/*"
                          style="display: none"
                        />
                        <label
                          for="01"
                          style="
                            background-image: url('{$image_url}theme/default/assets/images/register/upload-logo.png');
                          "
                        ></label>
                      </div>
                    </div>
                    <label id="logo_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาอัพโหลดโลโก้บริษัท</small>
                    </label>
                    <!-- UPLOAD -->
                    <div class="wrap-input">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="ชื่อบริษัท"
                        name="name" id="name" value="{if $climate_member.company.name != ''}{$climate_member.company.name}{/if}"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form1.png" />
                    </div>
                    <label id="name_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาระบุชื่อบริษัท</small>
                    </label>
                    <label>รายละเอียดบริษัท </label>
                    <div class="wrap-input">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="ชื่อ - นามสกุล"
                        name="contact_name" id="contact_name" value="{if $climate_member.name != ''}{$climate_member.name}{/if}{if $climate_member.surname != ''} {$climate_member.surname}{/if}"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form2.png" />
                    </div>
                    <label id="contact_name_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาระบุชื่อ - นามสกุล</small>
                    </label>
                    <div class="wrap-input">
                      <select name="company_type_id" id="company_type_id" 
                        class="form-control"
                        aria-placeholder="ประเภทอาคาร
                      "
                      >
                        <option value="">ประเภทอาคาร</option>
                        {foreach $company_type as $child_item}
                          <option value="{$child_item.id}">{$child_item.name}</option>
                        {/foreach}
                      </select>
                      <img src="{$image_url}theme/default/assets/images/register/form3.png" />
                    </div>
                    <label id="company_type_id_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาเลือกประเภทอาคาร</small>
                    </label>
                    <div class="wrap-input">
                      <select name="zone_id" id="zone_id" 
                        class="form-control"
                        aria-placeholder="โซน
                      "
                      >
                        <option value="">โซน</option>
                        {foreach $zone as $child_item}
                          <option value="{$child_item.id}">{$child_item.name}</option>
                        {/foreach}
                      </select>
                      <img src="{$image_url}theme/default/assets/images/register/form4.png" />
                    </div>
                    <label id="zone_id_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาเลือกโซน</small>
                    </label>
                    <div class="wrap-input date-open">
                      <img src="{$image_url}theme/default/assets/images/register/form5.png" />

                      <div class="row align-items-center">
                        <div class="col">
                          <label>วันเปิดทำการ</label>
                        </div>
                        <div class="col">
                          <div class="wrap-checkbox">
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox1"
                                value="Y"
                                name="open_mon"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox1"
                                >จ</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox2"
                                value="Y"
                                name="open_tue"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox2"
                                >อ</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox3"
                                value="Y"
                                name="open_wed"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox3"
                                >พ</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox4"
                                value="Y"
                                name="open_thu"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox4"
                                >พฤ</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox5"
                                value="Y"
                                name="open_fri"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox5"
                                >ศ</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox6"
                                value="Y"
                                name="open_sat"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox6"
                                >ส</label
                              >
                            </div>
                            <div class="form-check">
                              <input
                                class="form-check-input"
                                type="checkbox"
                                id="inlineCheckbox7"
                                value="Y"
                                name="open_sun"
                              />
                              <label
                                class="form-check-label"
                                for="inlineCheckbox7"
                                >อา</label
                              >
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <label id="open_day_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาระบุวันเปิดทำการ</small>
                    </label>
                    <div class="row">
                      <div class="col">
                        <div class="wrap-input time-picker">
                          <input class="form-control" name="open_start" id="open_start" />
                          <img src="{$image_url}theme/default/assets/images/register/form5.png" />
                        </div>
                        <label id="open_start_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเวลาเปิดทำการ</small>
                      </label>
                      </div>
                      <div class="col">
                        <div class="wrap-input time-picker">
                          <input class="form-control" name="open_end" id="open_end" />
                          <img src="{$image_url}theme/default/assets/images/register/form5.png" />
                        </div>
                        <label id="open_end_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเวลาปิดทำการ</small>
                      </label>
                      </div>
                    </div>
                    <div class="wrap-input">
                      <input type="number"
                         min="0" step="1"
                        class="form-control"
                        placeholder="จำนวนผู้มาติดต่อ (คน/เดือน )"
                        name="guest_qty"
                        id="guest_qty"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form7.png" />
                    </div>
                        <label id="guest_qty_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุจำนวนผู้มาติดต่อ (คน/เดือน)</small>
                      </label>
                    <div class="wrap-input">
                      <input type="number"
                         min="0" step="1"
                        class="form-control"
                        placeholder="จำนวนพนักงานในบริษัท (คน)"
                        name="staff_qty"
                        id="staff_qty"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form8.png" />
                    </div>

                        <label id="staff_qty_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุจำนวนพนักงานในบริษัท (คน)</small>
                      </label>
                    <div class="wrap-input">
                      <input
                        type="number"
                         min="0" step="0.01"
                        class="form-control"
                        placeholder="ขนาดพื้นที่ (ตร.ม.)"
                        name="size"
                        id="size"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form9.png" />
                    </div>

                        <label id="size_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุขนาดพื้นที่ (ตร.ม.)</small>
                      </label>
                    <div class="wrap-input textarea">
                      <textarea
                        placeholder="ที่ตั้งสำนักงาน"
                        class="form-control"
                        name="address"
                        id="address"
                      >{if $climate_member.company.address != ''}{$climate_member.company.address}{/if}</textarea>
                      <img src="{$image_url}theme/default/assets/images/register/form10.png" />
                    </div>

                        <label id="address_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุที่ตั้งสำนักงาน</small>
                      </label>
                    <div class="wrap-input">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="เบอร์โทรศัพท์ที่ 1"
                        name="tel1"
                        id="tel1" value="{if $climate_member.telephone != ''}{$climate_member.telephone}{/if}"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form11.png" />
                    </div>
                        <label id="tel1_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเบอร์โทรศัพท์</small>
                      </label>
                    <div class="wrap-input">
                      <input
                        type="text"
                        class="form-control"
                        placeholder="เบอร์โทรศัพท์ที่ 2"
                        name="tel2"
                        id="tel2" value="{if $climate_member.company.telephone != ''}{$climate_member.company.telephone}{/if}"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form11.png" />
                    </div>

                    <div class="seaparate"></div>

                    <div class="text-left">
                      <label>ข้อมูลบัญชี </label>
                    </div>
                    <div class="wrap-input">
                      <input
                        type="email"
                        class="form-control"
                        placeholder="อีเมล"
                        name="email"
                        id="email" value="{if $climate_member.email != ''}{$climate_member.email}{/if}"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form12.png" />
                    </div>
                        <label id="email_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุอีเมล</small>
                      </label>
                        <label id="email_exists" style="display: none;" class="text-danger col-12 text-right">
                          <small>อีเมลนี้มีอยู่ในระบบแล้ว</small>
                      </label>
                    <div class="wrap-input">
                      <input
                        type="password"
                        class="form-control"
                        name="password"
                        id="password"
                        placeholder="รหัสผ่าน"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form13.png" />
                    </div>
                        <label id="password_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุรหัสผ่าน</small>
                      </label>
                    <div class="wrap-input">
                      <input
                        type="password"
                        class="form-control"
                        name="confirm_password"
                        id="confirm_password"
                        placeholder="ยืนยันรหัสผ่าน"
                      />
                      <img src="{$image_url}theme/default/assets/images/register/form13.png" />
                    </div>

                        <label id="confirm_password_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณายืนยันรหัสผ่าน</small>
                      </label>
                        <label id="confirm_password_inc" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณายืนยันรหัสผ่านให้ถูกต้อง</small>
                      </label>
                    <button type="button" class="next-step">ถัดไป</button>
                  </div>
                  <!-- END TAB 1  -->

                  <!-- TAB 2  -->
                  <div class="tab-pane" role="tabpanel" id="step2">

                    {*
                    <div class="type-of-trash">
                      <label> ประเภทขยะ </label>
                      <button type="button" class="add-trash-btn" onclick="add_garbage_type();">
                        <img src="{$image_url}theme/default/assets/images/register/add.svg" />
                      </button>
                    </div>
                    <div class="element-trash">
                      <div class="text-left">
                        <label>ประเภทที่ 1</label>
                      </div>
                      <div class="wrap-input">
                        <select name="garbage_type_id[]" 
                          class="form-control"
                          aria-placeholder="ประเภทอาคาร
                        "
                        >
                          <option value="">
                            เลือกประเภทขยะ
                          </option>
                          {foreach $garbage_type as $child_item}
                            <option value="{$child_item.id}">{$child_item.name}</option>
                          {/foreach}
                        </select>
                        <img src="{$image_url}theme/default/assets/images/register/form14.png" />
                      </div>
                      <label style="display: none;" class="garbage_type_id_req text-danger col-12 text-right">
                          <small>กรุณาเลือกประเภทขยะ</small>
                      </label>
                      <div class="wrap-input weight">
                        <input
                          type="number"
                          min="0" step="1"
                          class="form-control"
                          placeholder="น้ำหนักรวม"
                          name="garbage_type_weight[]"
                        />
                      </div>
                      <label style="display: none;" class="garbage_type_weight_req text-danger col-12 text-right">
                          <small>กรุณาระบุน้ำหนักรวม (กก)</small>
                      </label>
                    </div>

                    <div class="seaparate"></div>

                    <div class="type-of-trash">
                      <label>การจัดการด้านสิ่งแวดล้อม</label>
                    </div>
                    *}

                    <div class="type-of-trash">
                      <label> ประเภทขยะ </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="ประเภท และปริมาณขยะต่อเดือนขององค์กร (โดยประมาณ)"
                          class="form-control"
                          name="garbage_type_description"
                          id="garbage_type_description"
                        ></textarea>
                      </div>
                      <label id="garbage_type_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                    </div>

                    <!-- ENVIROENMENT 1 -->
                    <div class="element-env">
                      <div class="text-left">
                        <label>นโยบายด้านสิ่งแวดล้อม</label>
                      </div>
                      <div class="img-upload-multiple">
                        <!-- Upload 1 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 02"></div>
                            <div class="delete-img 02">X</div>
                            <input type="hidden" name="file_policy[]" id="file_02" value="">
                            <input
                              id="02"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="02"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 2 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 03"></div>
                            <div class="delete-img 03">X</div>
                            <input type="hidden" name="file_policy[]" id="file_03" value="">
                            <input
                              id="03"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="03"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 3  -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 04"></div>
                            <div class="delete-img 04">X</div>
                            <input type="hidden" name="file_policy[]" id="file_04" value="">
                            <input
                              id="04"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="04"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-input no-border">
                        <input
                          type="text"
                          class="form-control"
                          placeholder="นโยบายด้านสิ่งแวดล้อม"
                          name="policy_name"
                          id="policy_name"
                        />
                      </div>
                        <label id="policy_name_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="คำอธิบาย"
                          class="form-control"
                          name="policy_description"
                          id="policy_description"
                        ></textarea>
                      </div>
                        <label id="policy_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                    </div>
                    <!-- END ENVIROENMENT 1 -->

                    <!-- ENVIROENMENT 2 -->
                    <div class="element-env">
                      <div class="top-trash text-left">
                        <label>ผู้รับผิดชอบด้านการจัดการขยะ</label>
                      </div>
                      <div class="img-upload-multiple">
                        <!-- Upload 1 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 05"></div>
                            <div class="delete-img 05">X</div>
                            <input type="hidden" name="file_person[]" id="file_05" value="">
                            <input
                              id="05"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="05"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 2 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 06"></div>
                            <div class="delete-img 06">X</div>
                            <input type="hidden" name="file_person[]" id="file_06" value="">
                            <input
                              id="06"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="06"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 3  -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 07"></div>
                            <div class="delete-img 07">X</div>
                            <input type="hidden" name="file_person[]" id="file_07" value="">
                            <input
                              id="07"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="07"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-input no-border">
                        <input
                          type="text"
                          class="form-control"
                          placeholder="ผู้รับผิดชอบด้านการจัดการขยะ"
                          name="person_name"
                          id="person_name"
                        />
                      </div>
                        <label id="person_name_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="คำอธิบาย"
                          class="form-control"
                          name="person_description"
                          id="person_description"
                        ></textarea>
                      </div>
                        <label id="person_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                    </div>
                    <!-- END ENVIROENMENT 2 -->

                    <button type="button" class="default-btn prev-step">
                      Back
                    </button>
                    <button type="button" class="next-step submit-register">
                      ถัดไป
                    </button>
                  </div>
                  <!-- END TAB 2  -->

                  <!-- TAB 3  -->
                  <div class="tab-pane" role="tabpanel" id="step3">
                    <div class="type-of-trash">
                      <label>การจัดการด้านสิ่งแวดล้อม</label>
                    </div>

                    <!-- ENVIROENMENT 1 -->
                    <div class="element-env">
                      <div class="top-trash text-left">
                        <label>รูปแบบการจัดการขยะ</label>
                      </div>
                      <div class="img-upload-multiple">
                        <!-- Upload 1 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 15"></div>
                            <div class="delete-img 15">X</div>
                            <input type="hidden" name="file_model[]" id="file_15" value="">
                            <input
                              id="15"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="15"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 2 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 16"></div>
                            <div class="delete-img 16">X</div>
                            <input type="hidden" name="file_model[]" id="file_16" value="">
                            <input
                              id="16"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="16"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 3  -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 08"></div>
                            <div class="delete-img 08">X</div>
                            <input type="hidden" name="file_model[]" id="file_08" value="">
                            <input
                              id="08"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="08"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-input no-border">
                        <input
                          type="text"
                          class="form-control"
                          placeholder="รูปแบบการจัดการขยะ"
                          name="model_name"
                          id="model_name"
                        />
                      </div>
                        <label id="model_name_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="คำอธิบาย"
                          class="form-control"
                          name="model_description"
                          id="model_description"
                        ></textarea>
                      </div>
                        <label id="model_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                    </div>
                    <!-- END ENVIROENMENT 1 -->

                    <!-- ENVIROENMENT 2 -->
                    <div class="element-env">
                      <div class="top-trash text-left">
                        <label>การจัดกิจกรรมกับหน่วยงานภายนอก</label>
                      </div>
                      <div class="img-upload-multiple">
                        <!-- Upload 1 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 09"></div>
                            <div class="delete-img 09">X</div>
                            <input type="hidden" name="file_activity[]" id="file_09" value="">
                            <input
                              id="09"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="09"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 2 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 10"></div>
                            <div class="delete-img 10">X</div>
                            <input type="hidden" name="file_activity[]" id="file_10" value="">
                            <input
                              id="10"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="10"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 3  -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 11"></div>
                            <div class="delete-img 11">X</div>
                            <input type="hidden" name="file_activity[]" id="file_11" value="">
                            <input
                              id="11"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="11"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-input no-border">
                        <input
                          type="text"
                          class="form-control"
                          placeholder="การจัดกิจกรรมกับหน่วยงานภายนอก"
                          name="activity_name"
                          id="activity_name"
                        />
                      </div>
                        <label id="activity_name_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="คำอธิบาย"
                          class="form-control"
                          name="activity_description"
                          id="activity_description"
                        ></textarea>
                      </div>
                        <label id="activity_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                    </div>
                    <!-- END ENVIROENMENT 2 -->

                    <!-- ENVIROENMENT 3 -->
                    <div class="element-env">
                      <div class="top-trash text-left">
                        <label>การจัดกิจกรรมประชาสัมพันธ์</label>
                      </div>
                      <div class="img-upload-multiple">
                        <!-- Upload 1 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 12"></div>
                            <div class="delete-img 12">X</div>
                            <input type="hidden" name="file_public[]" id="file_12" value="">
                            <input
                              id="12"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="12"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 2 -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 13"></div>
                            <div class="delete-img 13">X</div>
                            <input type="hidden" name="file_public[]" id="file_13" value="">
                            <input
                              id="13"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="13"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                        <!-- Upload 3  -->
                        <div class="upload-wrap upload-mulitiple-wrap">
                          <div class="upload-content">
                            <div class="uploadpreview 14"></div>
                            <div class="delete-img 14">X</div>
                            <input type="hidden" name="file_public[]" id="file_14" value="">
                            <input
                              id="14"
                              type="file"
                              accept="image/*"
                              style="display: none"
                            />
                            <label
                              for="14"
                              style="
                                background-image: url('{$image_url}theme/default/assets/images/register/upload-logo-small.png');
                              "
                            ></label>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-input no-border">
                        <input
                          type="text"
                          class="form-control"
                          placeholder="การจัดกิจกรรมประชาสัมพันธ์"
                          name="public_name"
                          id="public_name"
                        />
                      </div>
                        <label id="public_name_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border">
                        <textarea
                          placeholder="คำอธิบาย"
                          class="form-control"
                          name="public_description"
                          id="public_description"
                        ></textarea>
                      </div>
                        <label id="public_description_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณากรอกข้อมูล</small>
                      </label>
                      <div class="wrap-input no-border checkbox">
                        <label>
                        <input style="height: auto !important;" type="checkbox" name="is_accept" id="is_accept" value="Y"> กรุณายอมรับ <a href="#" data-toggle="modal" data-target="#termsModal">ข้อตกลงและเงื่อนไขการใช้งาน</a></label>
                      </div>
                        <label id="is_accept_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณายอมรับข้อตกลงและเงื่อนไขการใช้งาน</small>
                      </label>
                    </div>
                    <!-- END ENVIROENMENT 3 -->


                    <button type="button" class="default-btn prev-step">
                      Back
                    </button>
                    <button class="g-recaptcha default-btn next-step submit-register" data-sitekey="6LfUQH0cAAAAANitsD8aXtVXKHEEzoTu3y4G85TC" data-callback='onSubmit' data-action='submit'>สมัครสมาชิก</button>
                  </div>
                  <!-- END TAB 3  -->

                  <div class="clearfix"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="login-right">
        {include file='guest_stats.tpl'}
      </div>
    </div>
    <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="max-width: 800px; margin-left: 150px;">
          <p style="text-align: center;"><strong>ข้อตกลงและเงื่อนไขการใช้งาน&nbsp;</strong><br /><strong>Climate Care Platform (climatecare.setsocialimpact.com)&nbsp;</strong><br />&nbsp;</p>
<p>ผู้ใช้งานรับทราบและตกลงว่า จะใช้งาน<strong> Climate Care Platform (climatecare.setsocialimpact.com) </strong>เพื่อวัตถุประสงค์<br />ในการรายงานการเก็บรวบรวมข้อมูลด้านการดูแลสิ่งแวดล้อมเพื่อเป้าหมายในการลดภาวะโลกร้อน ได้แก่ ข้อมูล<br />ประเภทขยะชนิดต่างๆ ของนิติบุคคลที่เข้าร่วมโครงการ <strong>Care the Whale</strong> &nbsp;ข้อมูลในโครงการ Care the Bear และ<br />ข้อมูลการปลูกป่าในโครงการ Care the Wild &nbsp; &nbsp;เพื่อการประมวลผล สรุปผลออกมาเป็นรายงาน ข้อมูล ปริมาณ&nbsp;<br />คาร์บอนไดออกไซต์ที่ลดได้เมื่อเทียบเท่ากับจำนวนต้นไม้ที่ปลูกได้ &nbsp;<br />&nbsp;<br />การเข้าเว็บไซต์นี้ ตลอดจนการเปิดหน้าเว็บต่าง ๆ ที่อยู่ในเว็บไซต์นี้ ผู้ใช้งานเว็บไซต์ (&ldquo;ผู้ใช้งาน&rdquo;) ตกลงและยินยอมที่<br />จะปฏิบัติตามนโยบายของกลุ่มตลาดหลักทรัพย์ฯ (ตามนิยามที่จะได้กล่าวต่อไปในข้อตกลงและเงื่อนไขการใช้งาน<br />เว็บไซต์นี้) กฎหมายที่ใช้บังคับ และข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์ที่กำหนดไว้ในข้อตกลงนี้ทั้งที่กำหนดไว้เป็น<br />การทั่วไปและที่ได้กำหนดไว้โดยเฉพาะเจาะจงในส่วนใดส่วนหนึ่งของเว็บไซต์นี้ทั้งหมด (รวมเรียกว่า &ldquo;ข้อตกลงและ<br />เงื่อนไขการใช้งานเว็บไซต์&rdquo;) โดยผู้ใช้งานรับทราบและตกลงว่าข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์ที่กำหนดไว้นั้น<br />อาจมีการเปลี่ยนแปลงได้โดยไม่ต้องมีการบอกกล่าวล่วงหน้า&nbsp;<br />&nbsp;&nbsp;<br />อย่างไรก็ดี การเปลี่ยนแปลงใด ๆ ที่เกี่ยวกับข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์จะมีการเผยแพร่ไว้ในเว็บไซต์นี้&nbsp;<br />และเมื่อผู้ใช้งานได้เข้าใช้งานเว็บไซต์นี้ภายหลังการเปลี่ยนแปลงดังกล่าวแล้ว ย่อมถือว่าผู้ใช้งานตกลงยอมรับข้อตกลง<br />และเงื่อนไขการใช้งานเว็บไซต์ทั้งหมดตามที่ได้เปลี่ยนแปลงแล้ว ดังนั้น ผู้ใช้งานจึงควรติดตามข้อตกลงและเงื่อนไขการ<br />ใช้งานเว็บไซต์ที่กำหนดไว้นี้อยู่เสมอ&nbsp;<br />&nbsp;&nbsp;<br />ทั้งนี้ หากท่านไม่เห็นด้วยหรือประสงค์ที่จะปฏิเสธความมีผลผูกพันตามข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์ ขอ<br />ความกรุณาท่านยุติการเข้าชมและใช้งานเว็บไซต์นี้&nbsp;<br />&nbsp;<br /><strong>นิยาม </strong></p>
<table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr>
<td style="width: 18.0609%;"><strong>&ldquo;กลุ่มตลาดหลักทรัพย์ฯ&rdquo;</strong></td>
<td style="width: 78.0611%;">หมายถึง ตลาดหลักทรัพย์แห่งประเทศไทย และบริษัทในกลุ่มตลาดหลักทรัพย์แห่ง<br />ประเทศไทย&nbsp;</td>
</tr>
<tr>
<td style="width: 18.0609%;"><strong>&ldquo;บริษัทในกลุ่ม&nbsp;</strong><br /><strong>ตลาดหลักทรัพย์&nbsp;</strong><br /><strong>แห่งประเทศไทย&rdquo;</strong></td>
<td style="width: 78.0611%;">หมายถึง บริษัทที่จัดตั้งโดยตลาดหลักทรัพย์แห่งประเทศไทยหรือบริษัทย่อยของตลาด<br />หลักทรัพย์แห่งประเทศไทย และตลาดหลักทรัพย์แห่งประเทศไทยถือหุ้นไม่ว่าทางตรง<br />หรือทางอ้อมไม่น้อยกว่าร้อยละ 50 (ห้าสิบ) ของทุนจดทะเบียนของบริษัท&nbsp;<br />&nbsp;(<a href="https://www.set.or.th/th/about/overview/share_p1.html)" aria-invalid="true">https://www.set.or.th/th/about/overview/share_p1.html)</a>&nbsp;</td>
</tr>
<tr>
<td style="width: 18.0609%;"><strong>&ldquo;ผู้ใช้งาน&rdquo;</strong></td>
<td style="width: 78.0611%;">หมายถึง เจ้าหน้าที่สังกัดบริษัท และบริษัทที่เป็นสมาชิกโครงการ Care the Whale มี<br />หน้าที่กรอกข้อมูลรายงานขยะในระบบ Climate Care Platform เพื่อคำนวณปริมาณการ<br />ลดก๊าซเรือนกระจก และการนำรายงาน สถิติ ที่ประมวลได้จาก platform ไปใช้งานต่อ</td>
</tr>
<tr>
<td style="width: 18.0609%;"><strong>&ldquo;ข้อมูลส่วนบุคคล&rdquo;</strong></td>
<td style="width: 78.0611%;">หมายถึง ข้อมูลเกี่ยวกับบุคคลซึ่งทำให้สามารถระบุตัวบุคคลนั้นได้ไม่ว่าทางตรงหรือ<br />ทางอ้อม แต่ไม่รวมถึงข้อมูลของผู้ถึงแก่กรรมโดยเฉพาะ ตามกฎหมายว่าด้วยการคุ้มครอง<br />ข้อมูลส่วนบุคคล&nbsp;</td>
</tr>
<tr>
<td style="width: 18.0609%;"><strong>&ldquo;ประมวลผล&rdquo;</strong></td>
<td style="width: 78.0611%;">หมายถึง การดำเนินการใด ๆ กับข้อมูลส่วนบุคคลไม่ว่าด้วยวิธีการอัตโนมัติหรือไม่ก็ตาม&nbsp;<br />เช่น การเก็บรวบรวม การบันทึก การจัดระบบ การจัดเก็บ การปรับเปลี่ยนหรือการ<br />ดัดแปลง การเรียกคืน การปรึกษา การใช้ การเปิดเผย (โดยการส่ง โอน การเผยแพร่หรือ<br />การทำให้สามารถเข้าถึงหรือพร้อมใช้งานโดยวิธีใด ๆ) การจัดเรียง การนำมารวมกัน การ<br />บล็อกหรือจำกัด การลบหรือการทำลาย&nbsp;</td>
</tr>
</tbody>
</table>
<p><br /><strong>1. การใช้เว็บไซต์และทรัพย์สินทางปัญญา&nbsp;</strong><br />&nbsp;<br />1.1 &nbsp; &nbsp; บรรดาข้อมูลที่ปรากฏในเว็บไซต์นี้ซึ่งรวมถึงแต่ไม่จำกัดเฉพาะ ข้อความ รูปภาพ ตารางข้อมูล กราฟ ราคาหรือ<br />มูลค่าหลักทรัพย์ (ถ้ามี) เครื่องหมายการค้า กราฟิกต่าง ๆ เสียง การออกแบบหน้าจอแอปพลิเคชัน &nbsp;<br />การออกแบบส่วนต่อประสานกับผู้ใช้ (Interface) ข้อมูลต่าง ๆ ไม่ว่าจะอยู่ในรูปแบบใด โปรแกรมซอฟต์แวร์ใด&nbsp;<br />ๆ ที่มีอยู่ในเว็บไซต์นี้ รวมถึงข้อมูลทั้งหลายที่ผู้ใช้งานได้ดาวน์โหลดจากเว็บไซต์นี้ (ซึ่งต่อไปนี้จะรวมเรียกว่า&nbsp;<br />&ldquo;เนื้อหา&rdquo;) ถือเป็นสิทธิของกลุ่มตลาดหลักทรัพย์ฯ หรือผู้ให้อนุญาตแก่กลุ่มตลาดหลักทรัพย์ฯ ซึ่งเนื้อหาเหล่านั้น<br />ได้รับความคุ้มครองด้านทรัพย์สินทางปัญญา และ/หรือสิทธิในความเป็นเจ้าของอื่น ๆ ตามกฎหมายของ<br />ประเทศไทยและ/หรือกฎหมายของประเทศอื่นไม่ว่าในรูปแบบใด ๆ และไม่ว่าจะได้รับการจดทะเบียนไว้แล้ว<br />หรือไม่ก็ตาม&nbsp;<br />&nbsp;<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;ทั้งนี้ การนำชื่อทางการค้า เครื่องหมายการค้า เครื่องหมายบริการ และเครื่องหมายอื่น ๆ ของกลุ่มตลาด<br />หลักทรัพย์ฯ รวมถึงทรัพย์สินทางปัญญาใด ๆ ที่ปรากฏในเว็บไซต์นี้ไปใช้ ไม่ว่าจะด้วยวัตถุประสงค์ใด ๆ&nbsp;<br />ผู้ใช้งานจะต้องได้รับความยินยอมล่วงหน้าเป็นลายลักษณ์อักษรจากกลุ่มตลาดหลักทรัพย์ฯ ก่อนดำเนินการ &nbsp;<br />&nbsp;<br />1.2 บรรดาชื่อทางการค้า ชื่อสินค้า เครื่องหมายการค้า เครื่องหมายบริการ และเครื่องหมายอื่น ๆ รวมถึงทรัพย์สิน<br />ทางปัญญาอื่นใดที่ปรากฏในเว็บไซต์นี้ นอกเหนือจากสิ่งที่เป็นทรัพย์สินทางปัญญาของกลุ่มตลาดหลักทรัพย์ฯ&nbsp;<br />ซึ่งได้ถูกนำมาเรียบเรียงหรือจัดให้มีขึ้นเพื่อใช้เป็นส่วนประกอบของเว็บไซต์นี้ มีขึ้นเพื่อวัตถุประสงค์ในการ<br />ตกแต่งรูปลักษณ์ของเว็บไซต์ โดยกลุ่มตลาดหลักทรัพย์ฯ ในฐานะผู้ให้บริการเว็บไซต์ไม่ได้มีเจตนาที่จะกระทำ<br />การใด ๆ อันเป็นการละเมิดสิทธิในทางการค้าหรือทรัพย์สินทางปัญญาของผู้ใด&nbsp;<br />&nbsp;&nbsp;<br />1.3 เว้นแต่มีข้อความระบุไว้เป็นอย่างอื่นในเว็บไซต์นี้ บรรดาเนื้อหา ไฟล์ หรือเอกสารที่ปรากฏในเว็บไซต์นี้&nbsp;<br />ผู้ใช้งานมีสิทธิเพียงเข้าดู ดาวน์โหลด อัพโหลดเนื้อหา และพิมพ์เนื้อหาดังกล่าว เฉพาะเพื่อประโยชน์แก่การใช้<br />งานเพื่อเป้าหมายของการรายงานการรับผิดชอบต่อสังคมในเรื่องภาวะโลกร้อนและสิ่งแวดล้อม &nbsp;และผู้ใช้งานตก<br />ลงไม่ทำสำเนา หรือจัดเก็บ หรือดาวน์โหลดเนื้อหาไม่ว่าจะในรูปของเอกสารหรือในรูปแบบสื่ออิเล็กทรอนิกส์อื่น<br />ใดอันมีวัตถุประสงค์เพื่อการส่ง โอน จัดทำ เผยแพร่ พิมพ์ ทำซ ้า ดัดแปลง สร้างงานที่พัฒนา นำออกแสดง&nbsp;<br />ให้แก่บุคคลภายนอก ไม่ว่าการกระทำดังกล่าวจะเป็นไปเพื่อประโยชน์ในทางการค้าหรือได้รับประโยชน์เป็นสิ่ง<br />ตอบแทนอื่นใดหรือไม่ก็ตาม เว้นแต่จะได้รับความยินยอมเป็นลายลักษณ์อักษรล่วงหน้าจากกลุ่มตลาด<br />หลักทรัพย์ฯ ก่อนดำเนินการ&nbsp;<br />&nbsp;&nbsp;<br /><strong>2. ข้อสงวนสิทธิ์และข้อจำกัดความรับผิด&nbsp;</strong><br />&nbsp;<br />2.1 เว็บไซต์นี้มีขึ้นเพื่อวัตถุประสงค์ในการให้ข้อมูลและเพื่อการศึกษาด้านการลดปริมาณก๊าซเรือนกระจก จาก<br />โครงการ Care the Whale, Care the Wild และ Care the Bear ดังนั้น บรรดาข้อมูลต่าง ๆ ที่เผยแพร่บน<br />เว็บไซต์นี้ (ซึ่งต่อไปนี้จะรวมเรียกว่า &ldquo;บรรดาข้อมูลที่เผยแพร่&rdquo;) จึงเป็นข้อมูลจากสมาชิกในโครงการ เพื่อ<br />คำนวณการลดปริมาณก๊าซเรือนกระจก และประมวลผลเป็นรายงาน สถิติ เพื่อสมาชิกและโครงการ สามารถ<br />นำไปใช้เพื่อการทำงานในองค์กร และข้อมูลความรู้ทั่วไปด้านการดูแลสิ่งแวดล้อมจากตลาดหลักทรัพย์ฯ &nbsp;<br />&nbsp; &nbsp;<br />2.2 เนื้อหาที่แสดงบนเว็บไซต์นี้ ข้อมูลใน Climate Care Platform เป็นข้อมูลที่เกิดจากข้อมูลของ สมาชิกโครงการที่<br />นำมากรอก &nbsp; ถูกนำเสนอตามสภาพที่ได้รับ (&ldquo;as is&rdquo; basis) โดยกลุ่มตลาดหลักทรัพย์ฯ ไม่อาจรับรองหรือ<br />รับประกันใด ๆ ไม่ว่าทางตรงหรือทางอ้อมในเรื่องใด ไม่ว่าจะเป็นการรับประกันด้านความครบถ้วน ถูกต้อง<br />เหมาะสม ความเป็นปัจจุบัน หรือความน่าเชื่อถือของเนื้อหาและข้อมูล หรือการรับประกันผลที่จะได้รับจากการ<br />ใช้เนื้อหาหรือการใช้เว็บไซต์นี้ รวมถึงความเหมาะสมในการใช้งาน และกลุ่มตลาดหลักทรัพย์ฯ ไม่ต้อง<br />รับผิดชอบในความสูญหาย หรือความเสียหายใด ๆ รวมถึงไม่รับผิดชอบต่อผลกำไรหรือขาดทุนที่เกิดการใช้<br />เว็บไซต์และ/หรือจากการนำเนื้อหาข้อมูลที่ปรากฏในเว็บไซต์นี้ไปใช้ในทุกกรณี &nbsp; อันเนื่องมาจากข้อมูลใน&nbsp;<br />Climate Action Platform เป็นข้อมูลที่เกิดจากข้อมูลของ สมาชิกโครงการที่นำมากรอก &nbsp;<br />&nbsp;&nbsp;<br />2.3 กลุ่มตลาดหลักทรัพย์ฯ จะดำเนินการอย่างเหมาะสมเพื่อให้การเข้าถึงและการใช้งานเว็บไซต์นี้เป็นไปอย่าง<br />ต่อเนื่อง ไม่หยุดชะงัก และจะจัดให้มีระบบป้องกันการเข้าถึงข้อมูลใด ๆ จากบุคคลที่ไม่มีส่วนเกี่ยวข้อง อย่างไร<br />ก็ดี กลุ่มตลาดหลักทรัพย์ฯ ไม่อาจรับรองได้ว่าการเข้าใช้งานเว็บไซต์จะเป็นไปโดยปราศจากความล่าช้าและ<br />ข้อผิดพลาดใด ๆ และไม่อาจรับรองว่าเว็บไซต์นี้จะปราศจากโปรแกรม ไฟล์ หรือซอฟต์แวร์ที่มีจุดประสงค์มุ่ง<br />ร้าย หรืออาจสร้างความเสียหายต่อระบบหรือข้อมูลในระบบของผู้ใช้งาน อาทิ ไวรัส คอมพิวเตอร์เวิร์ม โทร<br />จันฮอร์ส สปายแวร์ หรือไฟล์ซึ่งเป็นอันตรายอื่น ๆ (รวมเรียกว่า &ldquo;ความบกพร่อง&rdquo;) ทั้งนี้ ไม่ว่าความบกพร่อง<br />ดังกล่าวจะเกิดขึ้นจากบุคคล หรือความขัดข้องทางเทคนิค หรือเหตุสุดวิสัยอย่างอื่น ดังนั้น ผู้ใช้งานจึงรับทราบ<br />เงื่อนไขดังกล่าวและตกลงว่าในกรณีที่มีความบกพร่องใด ๆ เกิดขึ้น กลุ่มตลาดหลักทรัพย์ฯ จะไม่รับผิดชอบใน<br />ความสูญหาย ความเสียหาย ค่าสินไหมทดแทน ค่าใช้จ่าย หรือค่าเสียหายใด ๆ ที่เกิดจากหรือเป็นผลสืบ<br />เนื่องมาจากความบกพร่องเหล่านั้นในทุกกรณี &nbsp;&nbsp;<br />&nbsp;&nbsp;<br />2.4 กลุ่มตลาดหลักทรัพย์ฯ สงวนสิทธิ์ในการพิจารณาไม่อนุญาตให้ผู้ใช้งานใช้งานเว็บไซต์นี้ รวมถึงสงวนสิทธิ์ใน<br />การเปลี่ยนแปลง หรือระงับการให้บริการเว็บไซต์ไม่ว่าบางส่วนหรือทั้งหมดและไม่ว่าในเวลาใด ๆ แก่ผู้ใช้งาน&nbsp;<br />โดยไม่จำเป็นต้องบอกกล่าวล่วงหน้าหรือระบุเหตุผลในการดำเนินการนั้น&nbsp;<br />&nbsp;&nbsp;<br />2.5 บรรดาข้อมูลรายละเอียด บทความ หรือข้อมูลใด ๆ ที่เผยแพร่ในเว็บไซต์นี้ เป็นลิขสิทธิ์ของผู้เขียนแต่ละท่าน&nbsp;<br />ซึ่งกลุ่มตลาดหลักทรัพย์ฯ ได้รับความอนุเคราะห์ให้นำมาเผยแพร่เพื่อเป็นประโยชน์แก่ประชาชนทั่วไป สำหรับ<br />เนื้อหา ลิงก์เชื่อมโยง รูปภาพ หรือเรื่องอื่นใด ตลอดจนข้อคิดเห็น หรือทรรศนะที่อยู่ในบทความซึ่งเผยแพร่อยู่<br />ในเว็บไซต์นี้เป็นความคิดเห็นหรือทรรศนะส่วนบุคคลของผู้เขียนแต่ละท่าน และไม่ได้เกี่ยวข้องกับกลุ่มตลาด<br />หลักทรัพย์ฯ หรือแสดงว่ากลุ่มตลาดหลักทรัพย์ฯ เห็นพ้อง ยอมรับ หรือสนับสนุนความคิดเห็นหรือทรรศนะ<br />ดังกล่าว ผู้ใช้งานควรศึกษาข้อมูลให้รอบคอบและใช้วิจารณญาณประกอบการตัดสินใจ &nbsp;<br />&nbsp;&nbsp;<br />&nbsp;&nbsp;<br />2.6 นอกเหนือจากที่กำหนดไว้ในข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์นี้ กลุ่มตลาดหลักทรัพย์ฯ กรรมการ&nbsp;<br />ผู้จัดการ ผู้บริหาร พนักงาน ลูกจ้าง ตัวแทน ที่ปรึกษา และ/หรือผู้เชี่ยวชาญของกลุ่มตลาดหลักทรัพย์ฯ จะไม่รับ<br />ผิดในความผิดพลาด หรือความบกพร่องใด ๆ ของเว็บไซต์ หรือจากข้อมูล เนื้อหาที่ปรากฏในเว็บไซต์ ตลอดจน<br />ไม่รับผิดในผลของการละเว้นการกระทำใด ๆ ที่เกี่ยวข้องกับเว็บไซต์นี้ ไม่ว่าจะเกิดจากสัญญา ละเมิด ประมาท&nbsp;<br />หรือเหตุอื่นใดที่อาจจะเกิดขึ้น ถึงแม้ว่ากลุ่มตลาดหลักทรัพย์ฯ จะได้รับแจ้งว่าอาจจะเกิดความเสียหายดังกล่าว<br />ขึ้นได้ก็ตาม&nbsp;<br />&nbsp;&nbsp;<br />&nbsp;&nbsp;<br />&nbsp;&nbsp;<br /><strong>3. ข้อมูลส่วนบุคคล&nbsp;</strong><br />&nbsp;<br />ผู้ใช้งานรับทราบและตกลงว่า ข้อมูลส่วนบุคคลของผู้ใช้งานสำหรับการใช้งานเว็บไซต์นี้ เช่น ข้อมูลที่ผู้ใช้งาน<br />ลงทะเบียน ข้อมูลการใช้งานเว็บไซต์ของผู้ใช้งาน และ/หรือข้อมูลอื่นใดของผู้ใช้งาน จะถูกประมวลผลและได้รับ<br />ความคุ้มครองตามคำประกาศเกี่ยวกับความเป็นส่วนตัว (Privacy Notice) ของกลุ่มตลาดหลักทรัพย์ฯ ซึ่ง<br />ผู้ใช้งานได้อ่านและรับทราบรายละเอียดและข้อกำหนดต่าง ๆ ตามคำประกาศเกี่ยวกับความเป็นส่วนตัว (Privacy&nbsp;<br />Notice) ของกลุ่มตลาดหลักทรัพย์ฯ แล้ว&nbsp;<br />&nbsp;&nbsp;<br /><strong>4. นโยบายการรักษาความมั่นคงปลอดภัยของเว็บไซต์&nbsp;</strong><br />&nbsp;<br />กลุ่มตลาดหลักทรัพย์ฯ ได้เลือกใช้เทคโนโลยี มาตรการรักษาความมั่นคงปลอดภัยในการทำธุรกรรมบน<br />เครือข่ายอินเทอร์เน็ต เพื่อป้องกันข้อมูลของผู้ใช้งานในระหว่างการส่งข้อมูลผ่านเครือข่ายการสื่อสาร หรือจาก<br />การถูกโจรกรรมข้อมูลโดยบุคคลหรือเครือข่ายอื่นทุกระบบที่ไม่ได้รับอนุญาตเข้ามาเชื่อมต่อกับเครือข่ายของ<br />กลุ่มตลาดหลักทรัพย์ฯ เช่น Firewall และการเข้ารหัสแบบ Secured Socket Layer (SSL) เป็นต้น&nbsp;<br />&nbsp; &nbsp;<br /><strong>5. การละเมิดข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์&nbsp;</strong><br />&nbsp;<br />ในกรณีที่ผู้ใช้งานละเมิดข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์ กลุ่มตลาดหลักทรัพย์ฯ สงวนสิทธิ์ที่จะระงับการ<br />เข้าถึงหรือยกเลิกการให้บริการใด ๆ ที่มีขึ้นบนเว็บไซต์นี้แก่ผู้ใช้งาน ทั้งนี้ การระงับหรือยกเลิกดังกล่าวไม่ถือ<br />Commented [AW1]: Insert hyperlink Privacy Notice&nbsp;<br />เป็นการตัดสิทธิกลุ่มตลาดหลักทรัพย์ฯ ที่จะได้รับการเยียวยาตามข้อตกลงและเงื่อนไขการใช้งานเว็บไซต์นี้&nbsp;<br />รวมถึงสิทธิอื่นใดตามกฎหมายที่เกี่ยวข้อง&nbsp;<br />&nbsp;&nbsp;<br />&nbsp;<br /><strong>6. กฎหมายที่บังคับใช้&nbsp;</strong><br />ข้อตกลงและเงื่อนไขการใช้งานนี้อยู่ภายใต้การบังคับและตีความตามกฎหมายไทย และศาลไทยเป็นผู้มีอำนาจ<br />ในการพิจารณาข้อพิพาทใดที่อาจเกิดขึ้น</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    <script src="{$image_url}theme/default/js/bootstrap5/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/popper.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/bootstrap.bundle.min.js"></script>
    <script src="{$image_url}theme/default/js/moment.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
     function onSubmit(token) {
       check_email_exists(false);
     }
   </script>
    <script>
      $(document).ready(function () {

        $(".upload-wrap input[type=file]").change(function () {
          var id = $(this).attr("id");
          var newimage = new FileReader();
          newimage.readAsDataURL(this.files[0]);
          newimage.onload = function (e) {
            $(".uploadpreview." + id).css(
              "background-image",
              "url(" + e.target.result + ")"
            );
            $(".uploadpreview." + id).addClass("active-image");
            $(".delete-img." + id).addClass("show");
            $("#file_" + id).val(e.target.result);
          };
        });
        $(".upload-wrap .delete-img").click(function () {
          $(this).removeClass("show");
          $(this).addClass("hide");
          $(this).prev(".uploadpreview").css("background-image", "url('')");
          $(this).prev(".uploadpreview").removeClass("active-image");
          $(this).next('input[type="hidden"]').val('');
        });

        $("#open_start").timepicker();
        $("#open_end").timepicker();
        $(".nav-tabs > li a[title]").tooltip();
        //Wizard
        $('a[data-toggle="tab"]').on("show.bs.tab", function (e) {
          var target = $(e.target);
          if (check_email_exists(true) == false) {
            return false;
          }
        });
        $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
          var target = $(e.target);
          if (target.parent().hasClass("disabled")) {
            return false;
          }
        });
        $(".next-step").click(function (e) {
          if(check_email_exists(true) != false)
          {
            var active = $(".wizard .nav-tabs li.active");
            active.next().removeClass("disabled");
            nextTab(active);
          }
        });
        $(".prev-step").click(function (e) {
          var active = $(".wizard .nav-tabs li.active");
          prevTab(active);
        });

      });

      function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
      }
      function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
      }

      function toBack() {
        var active = $(".wizard .nav-tabs li.active");
        if(active.index() == 0)
        {
          window.location.href = '{$base_url}member/login';
        }
        else
        {
          var active = $(".wizard .nav-tabs li.active");
          prevTab(active);
        }
      }

      $(".nav-tabs").on("click", "li", function () {
        if(check_email_exists(true) != false)
        {
          $(".nav-tabs li.active").removeClass("active");
          $(this).addClass("active");
        }
      });

      $(".count").each(function () {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                now = Number(Math.ceil(now)).toLocaleString("en");
                $(this).text(now);
              },
            }
          );
      });

      function add_garbage_type()
      {

        var html = `<div class="element-trash">
                      <div class="top-trash text-left">
                        <label>ประเภทที่ <font class="type_no"></font></label>
                        <button type="button" class="add-trash-btn" onclick="$(this).parent().parent().remove(); re_type_no();">
                          <img src="{$image_url}theme/default/assets/images/register/delete.svg" />
                        </button>
                      </div>
                      <div class="wrap-input">
                        <select name="garbage_type_id[]" 
                          class="form-control"
                          aria-placeholder="ประเภทอาคาร
                        "
                        >
                          <option value="">
                            เลือกประเภทขยะ
                          </option>
                          {foreach $garbage_type as $child_item}
                            <option value="{$child_item.id}">{$child_item.name}</option>
                          {/foreach}
                        </select>
                        <img src="{$image_url}theme/default/assets/images/register/form14.png" />
                      </div>
                      <label style="display: none;" class="garbage_type_id_req text-danger col-12 text-right">
                          <small>กรุณาเลือกประเภทขยะ</small>
                      </label>
                      <div class="wrap-input weight">
                        <input
                          type="number"
                          min="0" step="1"
                          class="form-control"
                          placeholder="น้ำหนักรวม"
                          name="garbage_type_weight[]"
                        />
                      </div>
                      <label style="display: none;" class="garbage_type_weight_req text-danger col-12 text-right">
                          <small>กรุณาระบุน้ำหนักรวม (กก)</small>
                      </label>
                    </div>`;

          $( html ).insertAfter(  $('.element-trash')[($('.element-trash').length - 1)] );
          re_type_no();
      }

      function re_type_no()
      {
        var cnt_type_no = 2;
          $(".type_no").each(function () {
            $(this).html(cnt_type_no);
            cnt_type_no++;
          });
      }
    </script>
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//climatecare.setsocialimpact.com/analytics/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '4']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
  </body>
</html>
