<div class="top-section-login">
    <div class="row align-items-center">
      <div class="col text-right">
        <img
          class="tree-image"
          src="{$image_url}theme/default/assets/images/login/login-right-image.png"
          alt="login-image-right"
        />
      </div>
      <div class="col text-right">
        <p class="count">{($stats.bear.cf + $stats.wild.cf + $stats.whale.cf)|number_format:0:".":""}</p>
        <p class="explain">kgCO<sub>2</sub>eq</p>
      </div>
    </div>
  </div>
  <div class="middle-element">
    <div class="sub-mid-element">
      <a href="https://www.carethebear.com/">
      <img
        src="{$image_url}theme/default/assets/images/login/sub-pic1.png"
        alt="login-square-1"
      />
      </a>
      <p>Care the Bear</p>
      <div class="table-boxed">
        <table>
          <tr>
            <td class="count">{$stats.bear.org|number_format:0:".":""}</td>
            <td>องค์กร</td>
          </tr>
          <tr>
            <td class="count">{$stats.bear.cf|number_format:0:".":""}</td>
            <td>kgCO<sub>2</sub>eq</td>
          </tr>
          <tr>
            <td class="count">{($stats.bear.cf/9)|number_format:0:".":""}</td>
            <td>ต้น</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="sub-mid-element">
      <a href="https://www.setsocialimpact.com/carethewild/">
      <img
        src="{$image_url}theme/default/assets/images/login/sub-pic2.png"
        alt="login-square-2" 
      />
      </a>
      <p>Care the Wild</p>
      <div class="table-boxed">
        <table>
          <tr>
            <td class="count">{$stats.wild.org|number_format:0:".":""}</td>
            <td>องค์กร</td>
          </tr>
          <tr>
            <td class="count">{$stats.wild.cf|number_format:0:".":""}</td>
            <td>kgCO<sub>2</sub>eq</td>
          </tr>
          <tr>
            <td class="count">{($stats.wild.cf/9)|number_format:0:".":""}</td>
            <td>ต้น</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="sub-mid-element">
      <a href="https://climatecare.setsocialimpact.com/">
      <img
        src="{$image_url}theme/default/assets/images/login/sub-pic3.png"
        alt="login-square-3"
      />
      </a>
      <p>Care the Whale</p>
      <div class="table-boxed">
        <table>
          <tr>
            <td class="count">{$stats.whale.org|number_format:0:".":""}</td>
            <td>องค์กร</td>
          </tr>
          <tr>
            <td class="count">{$stats.whale.cf|number_format:0:".":""}</td>
            <td>kgCO<sub>2</sub>eq</td>
          </tr>
          <tr>
            <td class="count">{($stats.whale.cf/9)|number_format:0:".":""}</td>
            <td>ต้น</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <p class="guest-stats-note">* kgCO<sub>2</sub>eq* = กิโลกรัมคาร์บอนเทียบเท่า</p>
  <div class="bottom-element">
    <img src="{$image_url}theme/default/assets/images/login/badge3.png" alt="badge3" />
    <img src="{$image_url}theme/default/assets/images/login/badge2.png" alt="badge2" />
    <img src="{$image_url}theme/default/assets/images/login/badge1.png" alt="badge1" />
  </div>