<form name="add_edit" action="{$base_url}{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_data();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มการจัดการขยะกลางทาง
          {elseif $action == 'edit'}
            แก้ไขการจัดการขยะกลางทาง
          {elseif $action == 'delete'}
            ยืนยันการลบการจัดการขยะกลางทาง
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form modal-with-upload-image">
          <div class="row align-items-center">
            <div class="col">
              <div class="wrap-content-modal-trash">
                <div class="left">
                  <img id="company_sort_point_image" src="{if $item.company_sort_point_image != ''}{$item.company_sort_point_image}{else}{$image_url}theme/default/assets/images/template/thumbnail-trash.png{/if}" style="max-height: 120px; max-width: 120px;" />
                </div>
                <div class="right">
                  <div class="wrap-input">
                    <label>สถานที่คัดแยก</label>
                    <select name="company_sort_point_id" id="company_sort_point_id" class="form-control">
                      <option value="">[ เลือกสถานที่ ]</option>
                      {foreach $company_sort_point as $company_sort_point_item}
                        <option data-image="{$company_sort_point_item.image}" value="{$company_sort_point_item.id}"{if $item.company_sort_point_id == $company_sort_point_item.id} selected="selected"{/if}>{$company_sort_point_item.name}</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
              </div>
              <label id="company_sort_point_id_req" style="display: none;" class="text-danger col-12 text-right">
                  <small>กรุณาเลือกสถานที่</small>
              </label>
            </div>
            <div class="col text-right">
              <img id="add_detail_btn" 
                style="width: 23px; height: 23px"
                src="{$image_url}theme/default/assets/images/register/add.svg"
              />
            </div>
          </div>
          <div class="table-section table-in-modal">
            <table id="table_detail">
              <thead>
                <tr>
                  <th data-field="order">ลำดับที่</th>
                  <th data-field="category">หมวดหมู่ขยะ</th>
                  <th data-field="detail">รายการขยะ</th>
                  <th data-field="type">ประเภทขยะ</th>
                  <th data-field="detail">ประเภทการคัดแยกย่อย</th>
                  <th data-field="weight" width="180">น้ำหนัก / จำนวน</th>
                  <th data-field="action" class="text-right"></th>
                </tr>
              </thead>
              <tbody>
                {foreach $item.detail as $detail_key => $detail_item}
                <tr>
                  <td class="row_no">{($detail_key + 1)}</td>
                  <td>
                    <select name="garbage_category_id[]" class="form-control">
                      <option value="">[ หมวดหมู่ขยะ ]</option>
                      {foreach $garbage_category as $garbage_category_item}
                        <option data-color="{$garbage_category_item.color}" value="{$garbage_category_item.id}"{if $detail_item.garbage_category_id == $garbage_category_item.id} selected="selected"{/if}>{$garbage_category_item.name}</option>
                      {/foreach}
                    </select>
                    <label style="display: none;" class="garbage_category_id_req text-danger col-12 text-right">
                        <small>กรุณาเลือกหมวดหมู่ขยะ</small>
                    </label>
                  </td>
                  <td>
                    <div class="wrap-select">
                      <select name="garbage_kind_id[]" class="form-control special-select">
                        <option value="">[ รายการขยะ ]</option>
                        {foreach $garbage_kind as $garbage_kind_item}
                          {if $garbage_kind_item.garbage_category_id == $detail_item.garbage_category_id}
                            <option data-image="{$garbage_kind_item.image}" value="{$garbage_kind_item.id}"{if $detail_item.garbage_kind_id == $garbage_kind_item.id} selected="selected"{/if}>{$garbage_kind_item.name}</option>
                          {/if}
                        {/foreach}
                      </select>
                      <img src="{$detail_item.garbage_kind_image}" style="max-width: 85px; max-height: 85px;">
                    </div>
                    <label style="display: none;" class="garbage_kind_id_req text-danger col-12 text-right">
                        <small>กรุณาเลือกรายการขยะ</small>
                    </label>
                  </td>
                  <td>
                    <div class="bin-wrap">
                      <img src="{$image_url}theme/default/assets/images/template/bin-{$detail_item.color}.png" >
                    </div>
                  </td>
                  <td>
                    <div class="wrap-select">
                      <select name="garbage_sort_id[]" class="form-control special-select">
                        <option value="">[ ประเภทการคัดแยกย่อย ]</option>
                        {foreach $garbage_sort as $garbage_sort_item}
                          <option data-image="{$garbage_sort_item.image}" value="{$garbage_sort_item.id}"{if $detail_item.garbage_sort_id == $garbage_sort_item.id} selected="selected"{/if}>{$garbage_sort_item.name}</option>
                        {/foreach}
                      </select>
                      <img src="{$detail_item.garbage_sort_image}" style="max-width: 85px; max-height: 85px;">
                    </div>
                    <label style="display: none;" class="garbage_sort_id_req text-danger col-12 text-right">
                        <small>กรุณาเลือกประเภทการคัดแยกย่อย</small>
                    </label>
                  </td>
                  <td>
                    <div class="wrap-select line-height-fix">
                      <input name="total[]" type="number" min="0" step="0.01" value="{$detail_item.total}" class="form-control input-border" style="min-width: 80px;">
                      <select name="type[]" class="form-control" style="padding-left: 0px;padding-right: 0px;">
                        <option value="">[หน่วย]</option>
                        <option value="W"{if $detail_item.type == 'W'} selected="selected"{/if}>ก.ก.</option>
                        <option value="Q"{if $detail_item.type == 'Q'} selected="selected"{/if}>ชิ้น</option>
                      </select>
                    </div>
                    <label style="display: none;" class="total_req text-danger col-12 text-right">
                        <small>กรุณาระบุจำนวน</small>
                    </label>
                    <label style="display: none;" class="type_req text-danger col-12 text-right">
                        <small>กรุณาเลือกหน่วย</small>
                    </label>
                  </td>
                  <td class="text-right">
                    <button onclick="$(this).parent().parent().remove(); re_table_no();"><img src="{$image_url}theme/default/assets/images/template/Delete2.png"></button>
                  </td>
                </tr>
                {/foreach}
              </tbody>
            </table>
            <label style="display: none;" class="detail_req text-danger col-12 text-right">
                <small>กรุณาเพิ่มรายละเอียดการจัดการขยะ</small>
            </label>
          </div>
          <div class="row reduce-wrap-margin">
            <div class="col-4">
              <div class="wrap-input with-special-arrange">
                <label style="display: block">น้ำหนักรวม</label>
                <div class="wrap-flex">
                  <input type="text" name="total_weight" id="total_weight" value="{if $item.total_weight > 0}{$item.total_weight}{else}0{/if}" disabled="disabled" readonly="readonly" class="form-control" />
                  <span>ก.ก.</span>
                  <input class="piece" name="total_qty" id="total_qty" value="{if $item.total_qty > 0}{$item.total_qty}{else}0{/if}" disabled="disabled" readonly="readonly" type="text" class="form-control" />
                  <span>ชิ้น</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row reduce-wrap-margin">
            <div class="col-4">
              <div class="wrap-input">
                <label>ผู้รับผิดชอบ</label>
                <select name="company_user_id" id="company_user_id" class="form-control">
                  <option value="">[ ผู้รับผิดชอบ ]</option>
                  {foreach $company_user as $company_user_item}
                    <option value="{$company_user_item.id}"{if $item.company_user_id == $company_user_item.id} selected="selected"{/if}>{$company_user_item.name}</option>
                  {/foreach}
                </select>
              </div>
              <label id="company_user_id_req" style="display: none;" class="text-danger col-12 text-right">
                  <small>กรุณาเลือกผู้รับผิดชอบ</small>
              </label>
            </div>
            <div class="col-4">
              <div class="wrap-input">
                <label>วันที่จัดการขยะ</label>
                <input
                  name="do_datetime"
                  id="do_datetime"
                  value="{$item.do_datetime}"
                  type="text"
                  class="form-control"
                />
              </div>
              <label id="do_datetime_req" style="display: none;" class="text-danger col-12 text-right">
                  <small>กรุณาเลือกวันที่จัดการขยะ</small>
              </label>
            </div>
          </div>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>