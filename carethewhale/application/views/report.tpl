{extends file="layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <h1>รายงาน</h1>
    <div class="breadcrumb-section clearfix">
        <div class="nav-section-left">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{$base_url}"><img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="#">รายงาน</a>
                    </li>
                </ol>
            </nav>
        </div>
        <div class="clear-fix"></div>
    </div>
    <div class="dashboard-section">
        <div class="dashboard-boxed">
            <div class="head-img">
                <img src="{$image_url}theme/default/assets/images/dashboad/global-icon.png" />
            </div>
            <h2>ผลรวมการลดคาร์บอนของบริษัท</h2>
            <div class="section-filter-date-action clearfix">
                <div class="left">
                    <div class="wrap-date-picker">
                        <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                        <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                        <input class="form-control daterange" onchange="get_total_data(this);" />
                    </div>
                </div>
            </div>
            <div class="section-main-tree">
                <div class="inner-wrap">
                    {*
                    <div class="root-tree">
                        <img src="{$image_url}theme/default/assets/images/dashboad/whale.png" />
                    </div>
                    *}
                    <div class="row">
                        <div class="col-7">
                            <div class="wrap-main-tree">
                                <!-- ความสูง ตามขนาดของต้นไม้ -->
                                <img style="width: 80%" src="{$image_url}theme/default/assets/images/dashboad/tree-main.png" />
                            </div>
                        </div>
                        <div class="col-5 align-self-center">
                            <table>
                                <tr>
                                    <td style="padding: 0px;"><img src="{$image_url}theme/default/assets/images/dashboad/whale.jpg" width="100" /></td>
                                    <td style="padding: 0px;"></td>
                                </tr>
                                <tr>
                                    <td class="count" id="total_cf">{$total.total_cf}</td>
                                    <td>
                                        <img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png" width="35" />
                                        <p>kgCO<sub>2</sub>eq</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="count" id="total_tree">{$total.total_tree}</td>
                                    <td>
                                        <img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png" width="35" />
                                        <p>ต้น</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- REPORT SECTION  -->
        <div class="report-first-section">
            <div class="row">
                <div class="col">
                    <div class="boxed">
                        <div class="top-section">
                            <h2>รายงานตามประเภทขยะ</h2>
                            <div class="section-filter-date-action clearfix">
                                <div class="left">
                                    <div class="wrap-date-picker">
                                        <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                                        <img class="arr"
                                            src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                                        <input class="form-control daterange" onchange="get_chart_data(this, 'garbage_type');" />
                                    </div>
                                </div>
                                <div class="right text-right">
                                    <button>
                                        <a href="{$base_url}report/type_waste">
                                            <img src="{$image_url}theme/default/assets/images/dashboad/Dots.png" />
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="chartdiv" id="chart_garbage_type"></div>
                        </div>
                        <div class="section-detail">
                            <table id="table_garbage_type">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/kg-icon.png"><br/>kg</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png"><br/>kgCO<sub>2</sub>eq</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png"><br/>ต้น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="boxed">
                        <div class="top-section">
                            <h2>สัดส่วนของขยะพลาสติก</h2>
                            <div class="section-filter-date-action clearfix">
                                <div class="left">
                                    <div class="wrap-date-picker">
                                        <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                                        <img class="arr"
                                            src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                                        <input class="form-control daterange" onchange="get_chart_data(this, 'garbage_plastic');" />
                                    </div>
                                </div>
                                <div class="right text-right">
                                    <button>
                                    	<a href="{$base_url}report/type?garbage=plastic">
                                        	<img src="{$image_url}theme/default/assets/images/dashboad/Dots.png" />
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="chartdiv" id="chart_garbage_plastic"></div>
                        </div>
                        <div class="section-detail">
                            <table id="table_garbage_plastic">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/kg-icon.png"><br/>kg</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png"><br/>kgCO<sub>2</sub>eq</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png"><br/>ต้น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="boxed">
                        <div class="top-section">
                            <h2>สัดส่วนของขยะรีไซเคิล</h2>
                            <div class="section-filter-date-action clearfix">
                                <div class="left">
                                    <div class="wrap-date-picker">
                                        <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                                        <img class="arr"
                                            src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                                        <input class="form-control daterange" onchange="get_chart_data(this, 'garbage_recycle');" />
                                    </div>
                                </div>
                                <div class="right text-right">
                                    <button>
                                    	<a href="{$base_url}report/type?garbage=recycle">
                                        	<img src="{$image_url}theme/default/assets/images/dashboad/Dots.png" />
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="chartdiv" id="chart_garbage_recycle"></div>
                        </div>
                        <div class="section-detail">
                            <table id="table_garbage_recycle">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/kg-icon.png"><br/>kg</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png"><br/>kgCO<sub>2</sub>eq</th>
                                        <th width="50" class="text-center"><img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png"><br/>ต้น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="report-print-section">
        <div class="row">
          <div class="col">
            <a href="{$base_url}report/top_10">
            <p>10 อันดับขยะที่พบมากที่สุด</p>
              </a> </div>
        </div>
         <div class="row">
          <div class="col">
            <a href="{$base_url}report/type">
            <p>รายงานขยะพลาสติก ขยะ Non recycle ขยะรีไซเคิล</p>
              </a> </div>
        </div>
         <div class="row">
          <div class="col">
            <a href="{$base_url}report/first">
            <p>รายงานการจัดขยะต้นทาง</p>
              </a> </div>
        </div>
        <div class="row">
          <div class="col">
            <a href="{$base_url}report/middle">
            <p>รายงานการจัดขยะกลางทาง</p>
              </a> </div>
        </div>
        <div class="row">
          <div class="col">
            <a href="{$base_url}report/last">
            <p>รายงานการจัดขยะปลายทาง</p>
              </a> </div>
        </div>
    </div>
    </div>
{/block}
{block name="script"}
	<script>
        $('.daterange').daterangepicker({
        	locale: {
		      format: 'YYYY-MM-DD'
		    },
        	startDate: moment().startOf('month'),
      		endDate: moment().endOf('month')
      	});

        function get_total_data(elm)
        {
        	var _range = elm.value.split(' - ');
        	$.get('{$base_url}report/get_total_cf/' + _range[0] + '/' + _range[1], function(data) {
                $('#total_cf').html(data.total_cf).prop("Counter", 0).animate(
                    {
                        Counter: $('#total_cf').text(),
                    },
                    {
                        duration: 4000,
                        easing: "swing",
                        step: function (now) {
                            now = Number(Math.ceil(now)).toLocaleString("en");
                            $('#total_cf').text(now);
                        },
                    }
                );;
                $('#total_tree').html(data.total_tree).prop("Counter", 0).animate(
                    {
                        Counter: $('#total_tree').text(),
                    },
                    {
                        duration: 4000,
                        easing: "swing",
                        step: function (now) {
                            now = Number(Math.ceil(now)).toLocaleString("en");
                            $('#total_tree').text(now);
                        },
                    }
                );;
            });
        }

        function get_chart_data(elm, type)
        {
        	var _range = elm.value.split(' - ');
        	$.get('{$base_url}report/get_total_cf_by_' + type + '/' + _range[0] + '/' + _range[1], function(data) {
				
				$('#table_' + type + ' tbody').html('');
			
	        	am4core.ready(function () {
		        	am4core.useTheme(am4themes_animated);
		            var chart = am4core.create("chart_" + type, am4charts.PieChart);

		            chart.data = [];

		            for(var i = 0; i < data.length; i++) {
			            chart.data.push({
			                "Type": data[i].name,
			                "Weight": data[i].weight.replace(',', '')
			            });

			            {literal}
			            $('#table_' + type + ' tbody').append(`
				            <tr>
	                            <td>
	                                <span style="background: #0081ff"></span>
	                                <p>${data[i].name}</p>
	                            </td>
	                            <td class="text-center">
	                                <p>${data[i].weight} กก.<span class="bond">(${data[i].percent}%)</span></p>
	                            </td>
	                            <td class="text-center">
	                                <p">${data[i].total_cf}</p>
	                            </td>
	                            <td class="text-center">
	                                <p>${data[i].total_tree}</p>
	                            </td>
	                        </tr>
                        `);
                        {/literal}
			        }

		            chart.innerRadius = am4core.percent(30);
		            var pieSeries = chart.series.push(new am4charts.PieSeries());
		            pieSeries.dataFields.value = "Weight";
		            pieSeries.dataFields.category = "Type";
		            pieSeries.slices.template.stroke = am4core.color("#fff");
		            pieSeries.slices.template.strokeWidth = 2;
		            pieSeries.slices.template.strokeOpacity = 1;
		            pieSeries.labels.template.fontSize = 18;
                    pieSeries.labels.template.disabled = true;
		            pieSeries.ticks.template.disabled = true;

		        });
		    });
        }
	</script>
    <style>
        .chartdiv {
            width: 100%;
            height: 320px;
            margin-top: -20px;
            margin-bottom: 40px;
            display: inline-block;
        }
    </style>
{/block}