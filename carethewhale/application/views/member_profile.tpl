{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>โปรไฟล์</h1>
		{if $smarty.get.alert == "update_data"}
        <div class="alert alert-warning">
			กรุณาอัพเดทข้อมูลการจัดการด้านสิ่งแวดล้อมของปี {$smarty.get.year}
        </div>
        {/if}
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">โปรไฟล์ </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
        <div class="alert alert-success">
          {$success_msg}
        </div>
        {/if}
        {if $error_msg != ""}
        <div class="alert alert-danger">
          {$error_msg}
        </div>
        {/if}
        <div class="company-detail">
          <img
            src="{$item.logo}"
            alt="{$item.name}"
            style="max-width: 120px; max-height: 120px;"
          />
          <div class="detail-nav">
            <div class="row">
              <div class="col-6">
                <h2 class="corp-name">{$item.name}</h2>
              </div>
              <div class="col-6 text-right">

              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <button class="code">รหัส {$item.id}</button>
              </div>
              <div class="col-6 text-right">
                <div class="wrap-right-action">
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <br/>
        <div class="company-detail-wrap">
              <div class="boxed">
                <script>

                function check_data()
                {
                  $('#contact_name_req').hide();
                  $('#tel1_req').hide();
                  $('#address_req').hide();
                  $('#company_type_id_req').hide();
                  $('#zone_id_req').hide();
                  $('#open_day_req').hide();
                  $('#open_start_req').hide();
                  $('#open_end_req').hide();
                  $('#guest_qty_req').hide();
                  $('#staff_qty_req').hide();
                  $('#size_req').hide();
                  $('#garbage_type_description_req').hide();
                  $('#policy_name_req').hide();
                  $('#policy_description_req').hide();
                  $('#person_name_req').hide();
                  $('#person_description_req').hide();
                  $('#model_name_req').hide();
                  $('#model_description_req').hide();
                  $('#activity_name_req').hide();
                  $('#activity_description_req').hide();
                  $('#public_name_req').hide();
                  $('#public_description_req').hide();
                  $('.has-error').removeClass('has-error');
                  
                  with(document.add_edit)
                  {
                    if(contact_name.value=="")
                    {
                      $('#contact_name_req').show();
                      $('#contact_name_req').parent('div').addClass('has-error');
                      $('#contact_name').focus();
                      return false;
                    }
                    else if(tel1.value=="")
                    {
                      $('#tel1_req').show();
                      $('#tel1_req').parent('div').addClass('has-error');
                      $('#tel1').focus();
                      return false;
                    }
                    else if(address.value=="")
                    {
                      $('#address_req').show();
                      $('#address_req').parent('div').addClass('has-error');
                      $('#address').focus();
                      return false;
                    }
                    else if(company_type_id.value=="")
                    {
                      $('#company_type_id_req').show();
                      $('#company_type_id_req').parent('div').addClass('has-error');
                      $('#company_type_id').focus();
                      return false;
                    }
                    else if(zone_id.value=="")
                    {
                      $('#zone_id_req').show();
                      $('#zone_id_req').parent('div').addClass('has-error');
                      $('#zone_id').focus();
                      return false;
                    }
                    else if($('.form-check-input:checked').length == 0)
                    {
                      $('#open_day_req').show();
                      $('#open_day_req').parent('div').addClass('has-error');
                      $('.form-check-input')[0].focus();

                      return false;
                    }
                    else if(open_start.value=="")
                    {
                      $('#open_start_req').show();
                      $('#open_start_req').parent('div').addClass('has-error');
                      $('#open_start').focus();
                      return false;
                    }
                    else if(open_end.value=="")
                    {
                      $('#open_end_req').show();
                      $('#open_end_req').parent('div').addClass('has-error');
                      $('#open_end').focus();
                      return false;
                    }
                    else if(guest_qty.value=="")
                    {
                      $('#guest_qty_req').show();
                      $('#guest_qty_req').parent('div').addClass('has-error');
                      $('#guest_qty').focus();
                      return false;
                    }
                    else if(staff_qty.value=="")
                    {
                      $('#staff_qty_req').show();
                      $('#staff_qty_req').parent('div').addClass('has-error');
                      $('#staff_qty').focus();

                      return false;
                    }
                    else if(size.value=="")
                    {
                      $('#size_req').show();
                      $('#size_req').parent('div').addClass('has-error');
                      $('#size').focus();
                      return false;
                    }
                    else if(garbage_type_description.value=="")
                    {
                      $('#garbage_type_description_req').show();
                      $('#garbage_type_description_req').parent('div').addClass('has-error');
                      $('#garbage_type_description').focus();
                      return false;
                    }
                    else if(policy_name.value=="")
                    {
                      $('#policy_name_req').show();
                      $('#policy_name_req').parent('div').addClass('has-error');
                      $('#policy_name').focus();
                      return false;
                    }
					/*
                    else if(policy_description.value=="")
                    {
                      $('#policy_description_req').show();
                      $('#policy_description_req').parent('div').addClass('has-error');
                      $('#policy_description').focus();
                      return false;
                    }
					*/
                    else if(person_name.value=="")
                    {
                      $('#person_name_req').show();
                      $('#person_name_req').parent('div').addClass('has-error');
                      $('#person_name').focus();
                      return false;
                    }
					/*
                    else if(person_description.value=="")
                    {
                      $('#person_description_req').show();
                      $('#person_description_req').parent('div').addClass('has-error');
                      $('#person_description').focus();
                      return false;
                    }
					*/
                    else if(model_name.value=="")
                    {
                      $('#model_name_req').show();
                      $('#model_name_req').parent('div').addClass('has-error');
                      $('#model_name').focus();
                      return false;
                    }
					/*
                    else if(model_description.value=="")
                    {
                      $('#model_description_req').show();
                      $('#model_description_req').parent('div').addClass('has-error');
                      $('#model_description').focus();
                      return false;
                    }
					*/
                    else if(activity_name.value=="")
                    {
                      $('#activity_name_req').show();
                      $('#activity_name_req').parent('div').addClass('has-error');
                      $('#activity_name').focus();
                      return false;
                    }
					/*
                    else if(activity_description.value=="")
                    {
                      $('#activity_description_req').show();
                      $('#activity_description_req').parent('div').addClass('has-error');
                      $('#activity_description').focus();
                      return false;
                    }
					*/
                    else if(public_name.value=="")
                    {
                      $('#public_name_req').show();
                      $('#public_name_req').parent('div').addClass('has-error');
                      $('#public_name').focus();
                      return false;
                    }
					/*
                    else if(public_description.value=="")
                    {
                      $('#public_description_req').show();
                      $('#public_description_req').parent('div').addClass('has-error');
                      $('#public_description').focus();
                      return false;
                    }
					*/
                  }
                }
              </script>
                <form name="add_edit" method="post" onsubmit="return check_data();">
                <div class="row">
                  <div class="col">
                    <h2>รายละเอียดบริษัท</h2>
                  </div>
                </div>
                <div class="form-detail">
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/User.png" />
                    <input
                      type="text"
                      name="contact_name" id="contact_name"
                      class="form-control"
                      placeholder="ชื่อ - นามสกุล"
                      value="{$item.user.name}"
                    />
                    <label id="contact_name_req" style="display: none;" class="text-danger col-12 text-right">
                        <small>กรุณาระบุชื่อ - นามสกุล</small>
                    </label>
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" name="tel1" id="tel1" class="form-control" value="{$item.tel1}" placeholder="โทร" />
                    <label id="tel1_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุเบอร์โทรศัพท์</small>
                      </label>
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Mobile.png" />
                    <input type="text" name="tel2" id="tel2" class="form-control" value="{$item.tel2}" placeholder="โทร" />
                  </div>
                  <div class="wrap-input">
                    <img src="{$image_url}theme/default/assets/images/company-detail/Message.png" />
                    <input
                      type="email"
                      name="email" id="email"
                      class="form-control"
                      value="{$item.user.email}"
                      placeholder="example@mail.com"
					  disabled="disabled"
                    />
                  </div>
                  
                </div> 
                <div class="row address">
                    <div class="col">
                      <br/>
                      <h2>ที่ตั้งสำนักงาน</h2>
                      <div class="form-group">
                        <label>ที่อยู่</label> 
                        <textarea class="form-control" name="address" rows="2" cols="70" id="address">{$item.address}</textarea>
                      </div>
                      <label id="address_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาระบุที่ตั้งสำนักงาน</small>
                      </label>
                    </div>
                  <div class="form-select">
                    <table class="table-date-open">
                      <tr>
                        <td><label>ประเภทอาคาร</label></td>
                        <td><label>Zone</label></td>
                        <td style="width: 320px">
                          <div class="wrap-input date-open">
                            <div class="row align-items-center">
                              <div class="col">
                                <label>วันเปิดทำการ</label>
                              </div>
                              <div class="col">
                                <div class="wrap-checkbox">
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      name="open_mon"
                                      id="open_mon"
                                      value="Y"
                                      {if $item.open_mon == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_mon"
                                      >จ</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_tue"
                                      name="open_tue"
                                      value="Y"
                                      {if $item.open_tue == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_tue"
                                      >อ</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_wed"
                                      name="open_wed"
                                      value="Y"
                                      {if $item.open_wed == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_wed"
                                      >พ</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_thu"
                                      name="open_thu"
                                      value="Y"
                                      {if $item.open_thu == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_thu"
                                      >พฤ</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_fri"
                                      name="open_fri"
                                      value="Y"
                                      {if $item.open_fri == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_fri"
                                      >ศ</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_sat"
                                      name="open_sat"
                                      value="Y"
                                      {if $item.open_sat == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_sat"
                                      >ส</label
                                    >
                                  </div>
                                  <div class="form-check">
                                    <input
                                      class="form-check-input"
                                      type="checkbox"
                                      id="open_sun"
                                      name="open_sun"
                                      value="Y"
                                      {if $item.open_sun == 'Y'} checked="checked"{/if}
                                    />
                                    <label
                                      class="form-check-label"
                                      for="open_sun"
                                      >อา</label
                                    >
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <label id="open_day_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุวันเปิดทำการ</small>
                          </label>
                        </td>
                        <td>จำนวน<br />ผู้มาติดต่อ</td>
                        <td width="150">
                          <input type="number" style="width: 60px"
                          min="0" step="1" name="guest_qty" id="guest_qty" class="form-control" value="{$item.guest_qty}" placeholder="จำนวนผู้มาติดต่อ" />
                          <small>คน/เดือน</small>
                          <label id="guest_qty_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุจำนวนผู้มาติดต่อ (คน/เดือน)</small>
                        </label>
                        </td>
                        <td>ขนาดพื้นที่</td>
                        <td width="150">
                          <input type="number" style="width: 60px"
                          min="0" step="0.01" name="size" id="size" class="form-control" value="{$item.size}" placeholder="ขนาดพื้นที่" />
                          <span>ตร.ม.</span>
                          <label id="size_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุขนาดพื้นที่ (ตร.ม.)</small>
                        </label>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <select name="company_type_id" id="company_type_id" 
                          class="form-control"
                          aria-placeholder="ประเภทอาคาร
                        "
                        >
                          <option value="">ประเภทอาคาร</option>
                          {foreach $company_type as $child_item}
                            <option value="{$child_item.id}"{if $item.company_type_id == $child_item.id} selected="selected" {/if}>{$child_item.name}</option>
                          {/foreach}
                        </select>
                      <label id="company_type_id_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาเลือกประเภทอาคาร</small>
                      </label>
                        </td>
                        <td>
                          <select name="zone_id" id="zone_id" 
                            class="form-control"
                            aria-placeholder="โซน
                          "
                          >
                            <option value="">โซน</option>
                            {foreach $zone as $child_item}
                              <option value="{$child_item.id}"{if $item.zone_id == $child_item.id} selected="selected" {/if}>{$child_item.name}</option>
                            {/foreach}
                          </select>
                      <label id="zone_id_req" style="display: none;" class="text-danger col-12 text-right">
                          <small>กรุณาเลือกโซน</small>
                      </label>
                        </td>
                        <td>
                          <div class="row">
                            <div class="col">
                              <div class="wrap-input time-picker">
                                <input
                                  value="{$item.open_start|substr:0:5}"
                                  name="open_start"
                                  placeholder="เวลาเปิด"
                                  class="form-control"
                                  id="open_start"
                                />
                              </div>
                          <label id="open_start_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุเวลาเปิดทำการ</small>
                        </label>
                            </div>
                            <div class="col">
                              <div class="wrap-input time-picker">
                                <input
                                  value="{$item.open_end|substr:0:5}"
                                  name="open_end"
                                  placeholder="เวลาปิด"
                                  class="form-control"
                                  id="open_end"
                                />
                              </div>
                          <label id="open_end_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุเวลาปิดทำการ</small>
                        </label>
                            </div>
                          </div>
                        </td>
                        <td>จำนวนพนักงาน<br />ในบริษัท</td>
                        <td width="150">
                          <input type="number" style="width:60px;"
                          min="0" step="1" name="staff_qty" id="staff_qty" class="form-control" value="{$item.staff_qty}" placeholder="จำนวนพนักงานในบริษัท" />
                          <small>คน</small>
                          <label id="staff_qty_req" style="display: none;" class="text-danger col-12 text-right">
                            <small>กรุณาระบุจำนวนพนักงานในบริษัท (คน)</small>
                        </label>
                        </td>
                        <td colspan="3" class="text-right"></td>
                      </tr>
                    </table>

                    <div class="col">
                      <hr/>
                      <h2>การจัดการด้านสิ่งแวดล้อม</h2>
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label>ประเภทขยะ</label>
                            <textarea class="form-control" name="garbage_type_description" id="garbage_type_description" rows="4" placeholder="ประเภทขยะ">{$item.garbage_type[0].description}</textarea>
                            <label id="garbage_type_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุประเภทขยะ</small>
                            </label>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label class="label-control">นโยบายด้านสิ่งแวดล้อม</label>
                            <br/>
                            <input style="width: 100%;" class="form-control" type="text" name="policy_name" id="policy_name" value="{$item.manage.policy_name}" placeholder="นโยบายด้านสิ่งแวดล้อม">
                            <label id="policy_name_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุนโยบายด้านสิ่งแวดล้อม</small>
                            </label>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="policy_description" id="policy_description" rows="2" placeholder="คำอธิบาย">{$item.manage.policy_description}</textarea>
                            <label id="policy_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุคำอธิบาย</small>
                            </label>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label class="label-control">ผู้รับผิดชอบด้านการจัดการขยะ</label>
                            <br/>
                            <input style="width: 100%;" class="form-control" type="text" name="person_name" id="person_name" value="{$item.manage.person_name}" placeholder="ผู้รับผิดชอบด้านการจัดการขยะ">
                            <label id="person_name_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุผู้รับผิดชอบด้านการจัดการขยะ</small>
                            </label>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="person_description" id="person_description" rows="2" placeholder="คำอธิบาย">{$item.manage.person_description}</textarea>
                            <label id="person_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุคำอธิบาย</small>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label class="label-control">รูปแบบการจัดการขยะ</label>
                            <br/>
                            <input style="width: 100%;" class="form-control" type="text" name="model_name" id="model_name" value="{$item.manage.model_name}" placeholder="รูปแบบการจัดการขยะ">
                            <label id="model_name_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุรูปแบบการจัดการขยะ</small>
                            </label>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="model_description" id="model_description" rows="2" placeholder="คำอธิบาย">{$item.manage.model_description}</textarea>
                            <label id="model_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุคำอธิบาย</small>
                            </label>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label class="label-control">การจัดกิจกรรมกับหน่วยงานภายนอก</label>
                            <br/>
                            <input style="width: 100%;" class="form-control" type="text" name="activity_name" id="activity_name" value="{$item.manage.activity_name}" placeholder="การจัดกิจกรรมกับหน่วยงานภายนอก">
                            <label id="activity_name_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุการจัดกิจกรรมกับหน่วยงานภายนอก</small>
                            </label>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="activity_description" id="activity_description" rows="2" placeholder="คำอธิบาย">{$item.manage.activity_description}</textarea>
                            <label id="activity_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุคำอธิบาย</small>
                            </label>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label class="label-control">การจัดกิจกรรมประชาสัมพันธ์</label>
                            <br/>
                            <input style="width: 100%;" class="form-control" type="text" name="public_name" id="public_name" value="{$item.manage.public_name}" placeholder="การจัดกิจกรรมประชาสัมพันธ์">
                            <label id="public_name_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุการจัดกิจกรรมประชาสัมพันธ์</small>
                            </label>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" name="public_description" id="public_description" rows="2" placeholder="คำอธิบาย">{$item.manage.public_description}</textarea>
                            <label id="public_description_req" style="display: none;" class="text-danger col-12 text-right">
                              <small>กรุณาระบุคำอธิบาย</small>
                            </label>
                          </div>
                        </div>
                      </div>
                      <hr/>
                      <div class="col text-right">
                        <div class="btn-right-group">
                          <input type="hidden" name="year" value="{$item.year}">
                          <button class="btn-save" type="submit" name="save" value="save">
                            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
                            บันทึก
                          </button>
                        </div>
                      </div>
                      <br/>
                    </div>
                  </div>
                </div>
              </form>
              </div>
            </div>
{/block}
{block name=script}
  <style>
    .form-control{
      font-size: 22px;
    }
  </style>
	<script>
		
        $(document).ready(function () {

            $("#open_start").timepicker();
            $("#open_end").timepicker();

        });
    </script>
{/block}