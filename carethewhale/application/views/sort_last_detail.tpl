{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
  <div class="wrap-trash-management trash-detail">
    <div class="detail-header">
      <h1>การจัดการขยะปลายทาง</h1>
      <p>รายการเลขที่ : <span>{$item.id}</span></p>
    </div>
    <div class="wrap-content-modal-trash">
      <div class="left">
        <img src="{$item.company_sort_point_image}" style="max-height: 120px; max-width: 120px;" />
      </div>
      <div class="right">
        <div class="wrap-input">
          <label>สถานที่คัดแยก</label>
          <select class="form-control">
            <option disabled="" selected="" value="">
              {$item.company_sort_point}
            </option>
          </select>
        </div>
      </div>
      <div class="last">
        <div class="warp-last">
          <button type="button" onclick="window.location='{$base_url}{$page}?edit={$item.id}';">
            <img src="{$image_url}theme/default/assets/images/template/Edit.png" />
          </button>
        </div>
      </div>
    </div>
    <div class="table-detail-trash">
      <table>
        <thead>
          <tr>
            <th>ลำดับที่</th>
            <th>หมวดหมู่ขยะ</th>
            <th>รายการขยะ</th>
            <th>ประเภทขยะ</th>
            <th>วิธีการจัดการ</th>
            <th>จัดการโดย</th>
            <th>น้ำหนัก / จำนวน</th>
          </tr>
        </thead>
        <tbody>
          {foreach $item.detail as $detail_key => $detail_item}
          <tr>
            <td>{($detail_key + 1)}</td>
            <td>
              <select class="form-control">
                <option value="1">{$detail_item.garbage_category}</option>
              </select>
            </td>
            <td>
              <div class="dp-flex">
                <select class="form-control">
                  <option value="1">{$detail_item.garbage_kind}</option>
                </select>
                <img
                  class="thumb"
                  src="{$detail_item.garbage_kind_image}"  style="max-width: 85px; max-height: 85px;"
                />
              </div>
            </td>
            <td>
              <img src="{$image_url}theme/default/assets/images/template/bin-{$detail_item.color}.png" />
            </td>
            <td>
              <select class="form-control">
                <option value="1">{$detail_item.garbage_disposal}</option>
              </select>
            </td>
            <td>
              <select class="form-control">
                <option value="1">{$detail_item.disposal_company}</option>
              </select>
            </td>
            <td>{$detail_item.total} กก.</td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </div>
    <div class="bottom-last-trash-detail">
      <label>น้ำหนักรวม</label>
      <div class="wrap-input">
        <div class="wrap-inside">
          <input type="text" value="{$item.total_weight}" />
          <span>ก.ก.</span>
        </div>
      </div>
      <br />
      <label>CF ที่ลดได้</label>
      <div class="wrap-input">
        <div class="wrap-inside">
          <input type="text" value="{$item.total_cf}" />
          <span>กิโลกรัมคาร์บอน</span>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="wrap-last">
            <label>ผู้รับผิดชอบ</label>
            <select class="form-control">
              <option value="1">{$item.company_user}</option>
            </select>
          </div>
        </div>
        <div class="col">
          <div class="wrap-last">
            <label>วันที่จัดการขยะ</label>
            <input
              type="text"
              class="form-control"
              value="{$item.do_datetime|date_format:"%d.%m.%Y %H:%M"}"
            />
          </div>
        </div>
        <div class="col"></div>
      </div>
    </div>
{/block}
{block name=script}

{/block}