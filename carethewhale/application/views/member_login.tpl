<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{$page_name} - {$site_name} : {$company_name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/main.css?ver={$smarty.now}" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/responsive.css?ver={$smarty.now}" />
    <link href="{$image_url}theme/default/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  </head>
  <body>
    <div class="login-page height100">
      <div class="login-left text-center">
        <div class="login-left-boxed">
          <h2>ยินดีต้อนรับ เข้าสู่ระบบ</h2>
          <img
            class="logo-login"
            src="{$image_url}theme/default/assets/images/login/logo-login2.png"
            alt="logo-login"
          />
          <h1>CLIMATE CARE PLATFORM</h1>
            <script>
                function check_login_data()
                {
                    $('#email_req').hide();
                    $('#password_req').hide();
                    $('.has-error').removeClass('has-error');
                    
                    with(document.login)
                    {
                        if(email.value=="")
                        {
                            $('#email_req').show();
                            $('#email_req').parent('div').addClass('has-error');
                            $('#email').focus();
                            return false;
                        }
                        else if(password.value=="")
                        {
                            $('#password_req').show();
                            $('#password_req').parent('div').addClass('has-error');
                            $('#password').focus();
                            return false;
                        }
                    }
                }
          </script>
          <form name="login" method="post" onsubmit="return check_login_data();">
            <input type="hidden" name="cs_token" value="{$cs_token}" />
            <div class="wrap-input">
              <input
                type="email"
                class="form-control"
                id="email"
                name="email"
                aria-describedby="emailHelp"
                placeholder="อีเมล์"
              />
              <img src="{$image_url}theme/default/assets/images/login/username.png" alt="icon-uname" />
            </div>
            <label id="email_req" style="display: none;" class="text-danger col-12 text-right">
                <small>Please input Email.</small>
            </label>
            <div class="wrap-input">
              <input
                type="password"
                class="form-control"
                id="password"
                name="password"
                placeholder="รหัสผ่าน"
              />
              <img
                class="pass-icon-rearrage"
                src="{$image_url}theme/default/assets/images/login/password.png"
                alt="icon-pass"
              />
            </div>
            <label id="password_req" style="display: none;" class="text-danger col-12 text-right">
                <small>Please input Password.</small>
            </label>
            {if $error_msg != ""}
                <label class="text-danger col-12 text-center">
                    <small>{$error_msg}</small>
                </label>
            {/if}
            <button type="submit" name="login" value="login" class="btn btn-primary">เข้าสู่่ระบบ</button>
          </form>
          <div class="bottom-form">
            <a href="{$base_url}member/register">สมัครบัญชีผู้ใช้</a>
            <a href="{$base_url}member/forget_password">ลืมรหัสผ่าน?</a>
          </div>
        </div>
      </div>
      <div class="login-right">
        {include file='guest_stats.tpl'}
      </div>
    </div>
    <script src="{$image_url}theme/default/js/bootstrap5/jquery.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/popper.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/bootstrap.bundle.min.js"></script>
    <script>
      $(".count").each(function () {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                now = Number(Math.ceil(now)).toLocaleString("en");
                $(this).text(now);
              },
            }
          );
      });
    </script>
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//climatecare.setsocialimpact.com/analytics/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '4']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->

  </body>
</html>
