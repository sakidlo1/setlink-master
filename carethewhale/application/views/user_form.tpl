<form name="add_edit" action="{$base_url}{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_email_exists();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มผู้ใช้
          {elseif $action == 'edit'}
            แก้ไขผู้ใช้
          {elseif $action == 'delete'}
            ยืนยันการลบผู้ใช้
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form">
          <div class="wrap-input">
            <label>ชื่อ - นามสกุล </label>
            <input
              value="{$item.name}"
              type="text"
              class="form-control"
              name="name" id="name"
            />
          </div>
          <label id="name_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุชื่อ - นามสกุล</small>
            </label>
          <div class="wrap-input">
            <label>Email</label>
            <input name="email" id="email" value="{$item.email}" type="text" class="form-control" />
          </div>
          <label id="email_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุอีเมล</small>
            </label>
          <label id="email_exists" style="display: none;" class="text-danger col-12 text-right">
                <small>อีเมลนี้มีอยู่ในระบบแล้ว กรุณาใช้อีเมลอื่น</small>
            </label>
          <div class="wrap-input">
            <label>Password{if $action == 'edit'} (หากไม่ต้องการเปลี่ยนให้ปล่อยว่างไว้){/if}</label>
            <input name="password" id="password" value="" type="password" class="form-control" />
          </div>
          <label id="password_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุรหัสผ่าน</small>
            </label>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>