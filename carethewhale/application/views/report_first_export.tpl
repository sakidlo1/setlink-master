<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
รายงานการจัดการขยะต้นทาง
<br />
<br />
{foreach $data as $key => $item}
{$item.text}
<br />
<br />
<table style="width: 100%; border: 1px #000000 solid;" border="1">
  <th>
    ลำดับที่
  </th>
  <th>
    รายการขยะ
  </th>
  <th>
    ประเภทขยะ
  </th>
  <th>
    ผลรวมขยะ
  </th>
  <th>
    หน่วย (ก.ก./ชิ้น)
  </th>
  {foreach $item.data as $no => $value}
      <tr>
        <td>{($no + 1)}</td>
        <td>{$value.name}</td>
        <td>{$value.type}</td>
        <td>{$value.total_weight|number_format:2:".":","}</td>
        <td>{if $value.unit == 'W'}ก.ก.{else}ชิ้น{/if}</td>
      </tr>
  {/foreach}
</table>
<br />
<br />
{/foreach}
</body>
</html>