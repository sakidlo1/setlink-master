{extends file="layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <link rel="stylesheet" href="{$image_url}theme/default/css/report.css" />
    <div class="main-content">
          <div>
            <div class="wrap-header-txt left">
              <h1>รายงาน</h1>
            </div>
            <div class="wrap-header-txt float-right">
              <!-- <h1>รายงานที่ 20 ลูกบ้าน</h1> -->
            </div>
          </div>

          <div class="breadcrumb-section clearfix">
            <div class="nav-section-left">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{$base_url}"><img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="{$base_url}report">รายงาน</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <a href="#">รายงานการจัดการขยะกลางทาง</a>
                  </li>
                </ol>
              </nav>
            </div>
            <div class="clear-fix"></div>
          </div>
          <div class="dashboard-section">
            <div class="report-boxed">
              <form name="report" method="get">
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">รายงานการจัดการขยะกลางทาง </span>
                </div>
                <div class="right text-right">
                  <button type="button" onclick="window.print();">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Print.png" />
                  </button>
                  <button type="submit" name="export" value="Y">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Import.png" />
                  </button>
                </div>
              </div>
              <div class="section-filter-date-action clearfix">
                <div class="float-right">
                  <select class="form-control quarter-boxed" name="type" onchange="document.report.submit();">
                    <option value="year"{if $smarty.get.type == 'year' || $smarty.get.type == ''} selected="selected"{/if}>รายปี</option>
                    <option value="quater"{if $smarty.get.type == 'quater'} selected="selected"{/if}>รายไตรมาส</option>
                    <option value="month"{if $smarty.get.type == 'month'} selected="selected"{/if}>รายเดือน</option>
                    <option value="range"{if $smarty.get.type == 'range'} selected="selected"{/if}>Year to Date</option>
                  </select>
                  <div hidden class="wrap-date-picker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                  </div>
                  {if $smarty.get.type == 'year' || $smarty.get.type == ''}
                  <div id="div-year" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year_start" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_start == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                    <span>-</span>
                    <select class="form-control quarter-boxed" name="year_end" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_end == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'quater' || $smarty.get.type == 'month'}
                  <div id="div-month" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'range'}
                  <div class="wrap-date-picker" id="div-datepicker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                    <input class="form-control daterange" name="dates" value="{$smarty.get.dates}" />
                  </div>
                  {/if}
                </div>
              </div>
            </form>
            </div>
            {foreach $data as $key => $item}
                <div class="report-boxed">
                  <div class="section-filter-date-action clearfix">
                    <div class="left">
                      <span class="header-font-size">{$item.text}</span>
                    </div>
                      <div class="right text-right">
                        (หน่วย : กก./ชิ้น)
                      </div>
                  </div>
                  <div class="section-main-tree">
                    <div class="inner-wrap">
                      <div class="row">
                        <div class="col-9">
                          <div class="chartdiv" id="chartdiv_{$key}"></div>
                        </div>
                        <div class="col-3 edit-padding-table-10px">
                          
                        </div>
                      </div>
                      {*
                      {$check = []}
                      {foreach $item.data as $no => $value}
                        {if $value.name|in_array:$check}

                        {else}
                          {$check[] = $value.name}
                          <div class="row method_label" {if $no > 0}style="display: none;"{/if}>
                            {foreach $item.data as $value2}
                              {if $value.garbage_kind_id == $value2.garbage_kind_id}
                              <div class="col-2 text-center">
                                <p>{$value2.sort}</p>
                              </div>
                              {/if}
                            {/foreach}
                          </div>
                        {/if}
                      {/foreach}
                      *}

                    </div>
                  </div>
                </div>
            {/foreach}
            <!-- section custome table -->
            <hr />
            <div class="section-table-report">
              <span class="header-font-size">รายงานการจัดการขยะกลางทาง </span>
              <br />
              {foreach $data as $key => $item}
                <span class="header-font-size">{$item.text} </span>
                  <table>
                    <thead>
                      <th>
                        ลำดับที่
                      </th>
                      <th>
                        รายการขยะ
                      </th>
                      <th>
                        การคัดแยกย่อย
                      </th>
                      <th>
                        ผลรวมปริมาณขยะ
                      </th>
                      <th>
                        หน่วย (กก./ชิ้น)
                      </th>
                    </thead>
                    <tbody>
                        {$total_1 = 0}
                      {foreach $item.data as $no => $value}
                          <tr>
                            <td>{($no + 1)}</td>
                            <td>{$value.name}</td>
                            <td>{$value.sort}</td>
                            <td>{$value.total_weight|number_format:2:".":","}</td>
                            <td>{if $value.unit == 'W'}กก.{else}ชิ้น{/if}</td>
                          </tr>
                        {$total_1 = $total_1 + $value.total_weight}
                      {/foreach}
                      <tr>
						<td colspan="3">รวม</td>
						<td>{$total_1|number_format:2:".":","}</td>
						<td>กก. / ชิ้น</td>
					  </tr>
                    </tbody>
                  </table>
              {/foreach}
            </div>
            <!-- END REPORT SECTION -->
          </div>
        </div>
{/block}
{block name="script"}
  <script>
        $('.daterange').daterangepicker({
          locale: {
          format: 'YYYY-MM-DD'
        },
          {if $smarty.get.dates == ''}
              startDate: moment().startOf('month'),
              endDate: moment().endOf('month')
          {/if}
        }).on('apply.daterangepicker', function(ev, picker) {
            document.report.submit();
        });

        am4core.ready(function () {

          {foreach $data as $key => $item}

              // Themes begin
              am4core.useTheme(am4themes_animated);
              // Themes end

              // Create chart instance
              var chart = am4core.create("chartdiv_{$key}", am4charts.XYChart);

              // Add data
              chart.data = [
                {$check_no = 1}
                {$check = []}
                {foreach $item.data as $no => $value}
                  {if $value.name|in_array:$check}

                  {else}
                    {if $check_no <= 10}
                      {$check[] = $value.name} 
                      {
                        "catacgory": "{$value.name}",
                        {foreach $item.data as $value2}
                          {if $value.garbage_kind_id == $value2.garbage_kind_id}
                            "{$value2.sort}": {$value2.total_weight},
                          {/if}
                        {/foreach}
                      },
                    {/if}
                    {$check_no = $check_no + 1}
                  {/if}
                {/foreach}
              ];

              chart.legend = new am4charts.Legend();
              chart.legend.position = "bottom";
              chart.legend.contentAlign = "left";
              chart.legend.width = undefined;
              chart.rtl = true;
              chart.colors.list = [
                am4core.color("#B6E700"),
                am4core.color("#44566C"),
                am4core.color("#80BCEE"),
              ]

              var cellSize = 60;
              chart.events.on("datavalidated", function (ev) {

                // Get objects of interest
                var chart = ev.target;
                var categoryAxis = chart.yAxes.getIndex(0);

                // Calculate how we need to adjust chart height
                var adjustHeight = chart.data.length * cellSize - categoryAxis.pixelHeight;

                // get current chart height
                var targetHeight = chart.pixelHeight + adjustHeight;

                // Set it on chart's container
                chart.svgContainer.htmlElement.style.height = targetHeight + "px";
              });



              // Create axes
              var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
              categoryAxis.dataFields.category = "catacgory";
              categoryAxis.renderer.grid.template.opacity = 0;

              var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
              valueAxis.min = 0;
              valueAxis.renderer.grid.template.opacity = 0;
              valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
              valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
              valueAxis.renderer.ticks.template.length = 10;
              valueAxis.renderer.line.strokeOpacity = 0.5;
              valueAxis.renderer.baseGrid.disabled = true;
              valueAxis.renderer.minGridDistance = 40;


              // Create series
              function createSeries(field, name, data) {
                var series = chart.series.push(new am4charts.ColumnSeries());
                series.name = {literal}field.replace(/[{()}]/g, ''){/literal};
                series.dataFields.valueX = field;
                series.dataFields.value = "catacgory";
                series.dataFields.categoryY = "catacgory";
                series.stacked = true;
                series.dataFields.valueY = field;

                // series.name = name;
                var labelBullet = series.bullets.push(new am4charts.LabelBullet());
                labelBullet.locationX = 0.5;
                labelBullet.label.text = "{literal}{valueX}{/literal}";
                labelBullet.label.fill = am4core.color("#fff");
                labelBullet.width = "100"



                 series.columns.template.events.on("over", function (ev) {

                   $('.method_label').hide();
                   $('.method_label:eq('+ev.target.dataItem.index+')').show();

                 });
              }

              {$check_no = 1}
              {$check = []}
              {foreach $item.data as $value}
                {if $value.sort|in_array:$check}
                  
                {else}
                  {if $check_no <= 10}
                    {$check[] = $value.sort} 
                    createSeries("{$value.sort}");
                  {/if}
                  {$check_no = $check_no + 1}
                {/if}
              {/foreach}

            {/foreach}

        });

  </script>
  <style>
    .chartdiv {
      width: 100%;
      height: 500px;
    }
  </style>
{/block}