{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<div class="detail-page-top-section">
      <h1>{$item.id} : {$item.name}</h1>
      <div class="breadcrumb-section clearfix">
        <div class="nav-section-left">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="{$base_url}"
                  ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                /></a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                <a href="#">สถานที่ตั้งถัง </a>
              </li>
            </ol>
          </nav>
        </div>
        <div class="clear-fix"></div>
      </div>
    </div>
    <div class="detail-section">
      <div class="row">
        <div class="col">
          <label>รหัสที่ตั้ง</label>
          <p>{$item.id}</p>
        </div>
        <div class="col">
          <label>ชื่อสถานที่ตั้งถัง/จุดคัดแยก</label>
          <p>{$item.name}</p>
        </div>
      </div>
      <div class="detail">
        <label>คำอธิบาย</label>
        <p>{if $item.description != ''}{$item.description|nl2br}{else}-{/if}</p>
        <label>วันที่สร้าง </label>
        <p>{$item.created_on|date_format:"%d.%m.%Y %H:%M"}</p>
        <label>วันที่แก้ไขล่าสุด </label>
        <p>{$item.updated_on|date_format:"%d.%m.%Y %H:%M"}</p>
      </div>
    </div>
{/block}
{block name=script}

{/block}