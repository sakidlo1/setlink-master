{extends file="layout.tpl"}
{block name=meta_title}Report - {$site_name} : {$company_name}{/block}
{block name=body}
    <link rel="stylesheet" href="{$image_url}theme/default/css/report.css" />
    <div class="main-content">
          <div>
            <div class="wrap-header-txt left">
              <h1>รายงาน</h1>
            </div>
            <div class="wrap-header-txt float-right">
              <!-- <h1>รายงานที่ 20 ลูกบ้าน</h1> -->
            </div>
          </div>

          <div class="breadcrumb-section clearfix">
            <div class="nav-section-left">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="{$base_url}"><img src="{$image_url}theme/default/assets/images/template/home-icon.png" /></a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="{$base_url}report">รายงาน</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <a href="#">รายงานการจัดการปลายทาง</a>
                  </li>
                </ol>
              </nav>
            </div>
            <div class="clear-fix"></div>
          </div>
          <div class="dashboard-section">
            <div class="report-boxed">
              <form name="report" method="get">
              <div class="section-filter-date-action clearfix">
                <div class="left">
                  <span class="header-font-size">รายงานการจัดการปลายทาง </span>
                </div>
                <div class="right text-right">
                  <button type="button" onclick="window.print();">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Print.png" />
                  </button>
                  <button type="submit" name="export" value="Y">
                    <img src="{$image_url}theme/default/assets/images/dashboad/Import.png" />
                  </button>
                </div>
              </div>
              <div class="section-filter-date-action clearfix">
                <div class="float-right">
                  <select class="form-control quarter-boxed" name="type" onchange="document.report.submit();">
                    <option value="year"{if $smarty.get.type == 'year' || $smarty.get.type == ''} selected="selected"{/if}>รายปี</option>
                    <option value="quater"{if $smarty.get.type == 'quater'} selected="selected"{/if}>รายไตรมาส</option>
                    <option value="month"{if $smarty.get.type == 'month'} selected="selected"{/if}>รายเดือน</option>
                    <option value="range"{if $smarty.get.type == 'range'} selected="selected"{/if}>Year to Date</option>
                  </select>
                  <div hidden class="wrap-date-picker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                  </div>
                  {if $smarty.get.type == 'year' || $smarty.get.type == ''}
                  <div id="div-year" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year_start" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_start == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                    <span>-</span>
                    <select class="form-control quarter-boxed" name="year_end" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year_end == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'quater' || $smarty.get.type == 'month'}
                  <div id="div-month" class="wrap-select-filter">
                    <select class="form-control quarter-boxed" name="year" onchange="document.report.submit();">
                        {for $year=$max_year to $min_year step -1}
                            <option value="{$year}"{if $smarty.get.year == $year} selected="selected"{/if}>{$year}</option>
                        {/for}
                    </select>
                  </div>
                  {elseif $smarty.get.type == 'range'}
                  <div class="wrap-date-picker" id="div-datepicker">
                    <img class="cal" src="{$image_url}theme/default/assets/images/dashboad/Calendar.png" />
                    <img class="arr" src="{$image_url}theme/default/assets/images/dashboad/arrow-down.png" />
                    <input class="form-control daterange" name="dates" value="{$smarty.get.dates}" />
                  </div>
                  {/if}
                </div>
              </div>
            </form>
            </div>
            {foreach $data as $key => $item}
                <div class="report-boxed">
                  <div class="section-filter-date-action clearfix">
                    <div class="left">
                      <span class="header-font-size">{$item.text}</span>
                    </div>
                    <div class="right text-right">

                    </div>
                  </div>
                  <div class="section-main-tree">
                <div class="inner-wrap">
                  <div class="row">
                    <div class="col-9">
                      <div class="chartdiv" id="chartdiv_{$key}"></div>
                    </div>
                    <div class="col-3">
                      <table class="table-no-padding">
                        <tr>
                          <td>
                            &nbsp;
                          </td>
                          <td>
                            <img src="{$image_url}theme/default/assets/images/dashboad/co2-icon.png" alt="co2-icon">
                            <p>kgCO<sub>2</sub>eq</p>
                          </td>
                          <td>
                            <img src="{$image_url}theme/default/assets/images/dashboad/tree-blue-icon.png" alt="tree-icon">
                            <p>จำนวน<br />ต้นไม้</p>
                          </td>
                        </tr>
                        {$check_no = 1}
                        {$tmp_data = []}
                        {$check = []}
                        {foreach $item.data as $no => $value}
                          {if $value.disposal|in_array:$check}

                          {else}
                            {if $check_no <= 10}
                              {$check[] = $value.disposal}
                              {$tmp_data[] = $item.data[$no]}
                            {/if}
                            {$check_no = $check_no + 1}
                          {/if}
                        {/foreach}

                        {$check_no = 1}
                        {$check = []}
                          {foreach $tmp_data as $no => $value}
                            {if $value.disposal|in_array:$check}

                            {else}
                              {if $check_no <= 10}
                                {$check[] = $value.disposal}
                                {$total_cf = 0}
                                {foreach $item.data as $value2}
                                    {if $value.garbage_disposal_id == $value2.garbage_disposal_id}
                                      {$total_cf = $total_cf + $value2.total_cf}
                                    {/if}
                                {/foreach}
                                <tr>
								                  <td>{$value.disposal}</td>
                                  <td>
                                    <p>{$total_cf|number_format:2:".":","}</p>
                                  </td>
                                  <td>
                                    <p>{($total_cf/9)|number_format:0:".":","}</p>
                                  </td>
                                </tr>
                              {/if}
                              {$check_no = $check_no + 1}
                            {/if}
                          {/foreach}
                      </table>
                    </div>
                  </div>
                </div>
              </div>
                </div>
            {/foreach}
            <!-- section custome table -->
            <hr />
            <div class="section-table-report">
              <span class="header-font-size">รายงานการจัดการขยะปลายทาง </span>
              <br />
              {foreach $data as $key => $item}
                <span class="header-font-size">{$item.text} </span>
                  <table>
                    <thead>
                      <th>
                        ลำดับที่
                      </th>
                      <th>
                        วิธีการจัดการ
                      </th>
                      <th>
                        รายการขยะ
                      </th>
                      <th>
                        บริษัท/หน่วยงาน
                      </th>
                      <th>
                        ปริมาณขยะ (กก.)
                      </th>
                      <th>
                        kgCO<sub>2</sub>eq
                      </th>
                      <th>
                        จำนวนต้นไม้
                      </th>
                    </thead>
                    <tbody>
                        {$total_1 = 0}
						{$total_2 = 0}
						{$total_3 = 0}
                      {foreach $item.data as $no => $value}
                          <tr>
                            <td>{($no + 1)}</td>
                            <td>{$value.disposal}</td>
                            <td>{$value.name}</td>
                            <td>{$value.disposal_company}</td>
                            <td>{$value.total_weight|number_format:2:".":","}</td>
                            <td>{$value.total_cf|number_format:2:".":","}</td>
                            <td>{($value.total_cf/9)|number_format:0:".":","}</td>
                          </tr>
                        {$total_1 = $total_1 + $value.total_weight}
						{$total_2 = $total_2 + $value.total_cf}
						{$total_3 = $total_3 + ($value.total_cf/9)}
                      {/foreach}
                      	<tr>
						<td colspan="4">รวม</td>
						<td>{$total_1|number_format:2:".":","}</td>
						<td>{$total_2|number_format:2:".":","}</td>
						<td>{$total_3|number_format:0:".":","}</td>
					  </tr>
                    </tbody>
                  </table>
              {/foreach}
            </div>
            <!-- END REPORT SECTION -->
          </div>
        </div>
{/block}
{block name="script"}
	<style>
		table.table-no-padding tr th, table.table-no-padding tr td{
			padding: 5px 5px !important;
			font-size: 20px !important;
		}
	</style>
  <script>
        $('.daterange').daterangepicker({
          locale: {
          format: 'YYYY-MM-DD'
        },
          {if $smarty.get.dates == ''}
              startDate: moment().startOf('month'),
              endDate: moment().endOf('month')
          {/if}
        }).on('apply.daterangepicker', function(ev, picker) {
            document.report.submit();
        });

        am4core.ready(function () {

          {foreach $data as $key => $item}
              
              // Themes begin
              am4core.useTheme(am4themes_animated);
              // Themes end

              var chart = am4core.create("chartdiv_{$key}", am4charts.XYChart);
              chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

              chart.data = [
                {$check_no = 1}
                {$check = []}
                {foreach $item.data as $no => $value}
                  {if $value.disposal|in_array:$check}

                  {else}
                    {if $check_no <= 10}
                      {$check[] = $value.disposal}
                      {
                        category: "{$value.disposal}",
                        {foreach $item.data as $value2}
                          {if $value.garbage_disposal_id == $value2.garbage_disposal_id}
                            value{$value2.disposal_company_id}: {$value2.total_weight},
                          {/if}
                        {/foreach}
                      },
                    {/if}
                    {$check_no = $check_no + 1}
                  {/if}
                {/foreach}
              ];

              chart.colors.step = 2;
              chart.padding(30, 30, 10, 30);
              chart.legend = new am4charts.Legend();

              var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
              categoryAxis.dataFields.category = "category";
              categoryAxis.renderer.grid.template.location = 0;
              categoryAxis.renderer.minGridDistance = 30;
              categoryAxis.renderer.labels.template.rotation = 320;
              categoryAxis.renderer.labels.template.verticalCenter = "middle";
              categoryAxis.renderer.labels.template.horizontalCenter = "right";

              var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
              valueAxis.min = 0;
              valueAxis.max = 100;
              valueAxis.strictMinMax = true;
              valueAxis.calculateTotals = true;
              valueAxis.renderer.minWidth = 50;

              {$check_no = 1}
              {$check = []}
              {foreach $item.data as $no => $value}
                {if $value.disposal_company_id|in_array:$check}

                {else}
                  {if $check_no <= 10}
                    {$check[] = $value.disposal_company_id}
                    var series{$value.disposal_company_id} = chart.series.push(new am4charts.ColumnSeries());
                    series{$value.disposal_company_id}.columns.template.width = am4core.percent(80);
                    series{$value.disposal_company_id}.columns.template.tooltipText =
                      "{literal}{name}: {valueY.totalPercent.formatNumber('#.00')}{/literal}%";
                    series{$value.disposal_company_id}.name = "{$value.disposal_company}";
                    series{$value.disposal_company_id}.dataFields.categoryX = "category";
                    series{$value.disposal_company_id}.dataFields.valueY = "value{$value.disposal_company_id}";
                    series{$value.disposal_company_id}.dataFields.valueYShow = "totalPercent";
                    series{$value.disposal_company_id}.dataItems.template.locations.categoryX = 0.5;
                    series{$value.disposal_company_id}.stacked = true;
                    series{$value.disposal_company_id}.tooltip.pointerOrientation = "vertical";

                    var bullet{$value.disposal_company_id} = series{$value.disposal_company_id}.bullets.push(new am4charts.LabelBullet());
                    bullet{$value.disposal_company_id}.interactionsEnabled = false;
                    bullet{$value.disposal_company_id}.label.text = "{literal}{valueY.totalPercent.formatNumber('#.00')}{/literal}%";
                    bullet{$value.disposal_company_id}.label.fill = am4core.color("#ffffff");
                    bullet{$value.disposal_company_id}.locationY = 0.5;
                  {/if}
                  {$check_no = $check_no + 1}
                {/if}
              {/foreach}


              chart.scrollbarX = new am4core.Scrollbar();

            {/foreach}

        });

  </script>
  <style>
    .chartdiv {
      width: 100%;
      height: 500px;
    }
  </style>
{/block}