{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
    <h1>การจัดการขยะต้นทาง </h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item">
                  <a href="#">การจัดการขยะ </a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">ต้นทาง </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
        <div class="alert alert-success">
            {$success_msg}
        </div>
        {/if}
        {if $error_msg != ""}
        <div class="alert alert-danger">
            {$error_msg}
        </div>
        {/if}
        <div class="filter-section clearfix">
          <div class="filter-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="fName">ค้นหา : &nbsp;&nbsp;&nbsp;</label>
                        <input class="form-control" id="fName" type="text" size="10" />
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="button" value="Search" id="fSearch" />
                    </div>
                </div>
            </div>
          <div class="filter-right">
            <button type="button" onclick="call_modal('add');">
              <img src="{$image_url}theme/default/assets/images/register/add.svg" />
            </button>
          </div>
        </div>
        <div class="table-section">
            <table id="data-tables" class="display table-button-no-style" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>วันที่ทำรายการ</th>
                        <th>รายการเลขที่</th>
                        <th>สถานที่ตั้งถัง/จุดคัดแยก</th>
                        <th>น้ำหนักรวม</th>
                        <th>จำนวนชิ้น</th>
                        <th>ผู้รับผิดชอบ</th>
                        <th>ผู้บันทึกรายการ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal fade modal-with-form modal-large-with-table" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
              
            </div>
          </div>
        </div>
{/block}
{block name=script}
    <script>
        function init_form(action = '')
        {
            $('select[name="garbage_category_id[]"]').change(function(){ 

                var _self = $(this); 
                if(_self.val() == '')
                {
                    _self.parent().parent().find('.bin-wrap img').attr('src', '');
                    _self.parent().parent().find('select[name="garbage_kind_id[]"]').html('<option value="">[ รายการขยะ ]</option>');
                }
                else
                {
                    _self.parent().parent().find('.bin-wrap img').attr('src', '{$image_url}theme/default/assets/images/template/bin-' + _self.find('option:selected').attr('data-color') + '.png');
                    $.get('{$base_url}{$page}/get_garbage_kind/' + $(this).val(), function( data ) {
                      _self.parent().parent().find('select[name="garbage_kind_id[]"]').html(data);
                    });
                }

            });

            $('select[name="garbage_kind_id[]"]').change(function(){ 
                
                var _self = $(this); 
                if(_self.val() == '')
                {
                    _self.parent().find('img').attr('src', '{$image_url}theme/default/assets/images/template/mock-bin.png');
                    _self.parent().find('img').popover({
                        html: true,
                        trigger: 'hover',
                        placement: 'auto',
                        content: function () { return '<img src="{$image_url}theme/default/assets/images/template/mock-bin.png" width="200" />'; }
                    });
                }
                else
                {
                    _self.parent().find('img').attr('src', _self.find('option:selected').attr('data-image'));
                    _self.parent().find('img').popover({
                        html: true,
                        trigger: 'hover',
                        placement: 'auto',
                        content: function () { return '<img src="' + _self.find('option:selected').attr('data-image') + '" width="200" />'; }
                    });
                }

            });

            $('input[name="total[]"], select[name="type[]"]').change(function(){ 
                
                re_calc();

            });

            $("#do_datetime").datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });

            if(action == 'edit')
            {
                $('select[name="garbage_kind_id[]"]').trigger('change');
            }
        }

        function add_table_row()
        {
            var html = `<tr>
                          <td class="row_no"></td>
                          <td>
                            <select name="garbage_category_id[]" class="form-control">
                              <option value="">[ หมวดหมู่ขยะ ]</option>
                              {foreach $garbage_category as $garbage_category_item}
                                <option data-color="{$garbage_category_item.color}" value="{$garbage_category_item.id}">{$garbage_category_item.name}</option>
                              {/foreach}
                            </select>
                            <label style="display: none;" class="garbage_category_id_req text-danger col-12 text-right">
                                <small>กรุณาเลือกหมวดหมู่ขยะ</small>
                            </label>
                          </td>
                          <td>
                            <div class="wrap-select">
                              <select name="garbage_kind_id[]" class="form-control special-select">
                                <option value="">[ รายการขยะ ]</option>
                              </select>
                              <img src="{$image_url}theme/default/assets/images/template/mock-bin.png" style="max-width: 85px; max-height: 85px;">
                            </div>
                            <label style="display: none;" class="garbage_kind_id_req text-danger col-12 text-right">
                                <small>กรุณาเลือกรายการขยะ</small>
                            </label>
                          </td>
                          <td>
                            <div class="bin-wrap">
                              <img src="">
                            </div>
                          </td>
                          <td>
                            <div class="wrap-select line-height-fix">
                              <input name="total[]" type="number" min="0" step="0.01" value="" class="form-control input-border">
                              <select name="type[]" class="form-control">
                                <option value="">[ หน่วย ]</option>
                                <option value="W">ก.ก.</option>
                                <option value="Q">ชิ้น</option>
                              </select>
                            </div>
                            <label style="display: none;" class="total_req text-danger col-12 text-right">
                                <small>กรุณาระบุจำนวน</small>
                            </label>
                            <label style="display: none;" class="type_req text-danger col-12 text-right">
                                <small>กรุณาเลือกหน่วย</small>
                            </label>
                          </td>
                          <td class="text-right">
                            <button onclick="$(this).parent().parent().remove(); re_table_no();"><img src="{$image_url}theme/default/assets/images/template/Delete2.png"></button>
                          </td>
                        </tr>`;

              $('#table_detail tbody').append(html);
              re_table_no();
              init_form();
        }

        function re_table_no()
        {
            var cnt_row_no = 1;
              $(".row_no").each(function () {
                $(this).html(cnt_row_no);
                cnt_row_no++;
            });

            re_calc();
        }

        function re_calc()
        {
            $('#total_weight').val('0');
            $('#total_qty').val('0');

            $('input[name="total[]"]').each(function (index) {
                if($('select[name="type[]"]')[index].value == 'W' && $('input[name="total[]"]')[index].value != "") {
                    $('#total_weight').val(parseFloat($('#total_weight').val()) + parseFloat($('input[name="total[]"]')[index].value));
                } else if($('select[name="type[]"]')[index].value == 'Q' && $('input[name="total[]"]')[index].value != "") {
                    $('#total_qty').val(parseInt($('#total_qty').val()) + parseInt($('input[name="total[]"]')[index].value));
                }
            });
        }

        function call_modal(action, id)
        {
            if(typeof id == 'undefined') {
                id = 0
            }

            if(action == 'delete') {
                $('#modalAction').addClass('modal-delete');
            } else {
                $('#modalAction').removeClass('modal-delete');
            }

            $.get('{$base_url}{$page}/form/' + action + '/' + id, function( data ) {
              $('#modalAction .modal-content').html(data);
              $('#modalAction').modal('show');

                $('#company_sort_point_id').change(function(){ 
                    
                    if($(this).val() == '')
                    {
                        $('#company_sort_point_image').attr('src', '{$image_url}theme/default/assets/images/template/thumbnail-trash.png');
                    }
                    else
                    {
                        $('#company_sort_point_image').attr('src', $(this).find('option:selected').attr('data-image'));
                    }

                });

                $('#add_detail_btn').click(function(){
                    add_table_row();
                });

                if(action == 'add') {
                    add_table_row();
                } else {
                    init_form(action);
                }
            });
        }

        function check_data()
        {
            $('#company_sort_point_id_req').hide();
            $('.detail_req').hide();
            $('.garbage_category_id_req').hide();
            $('.garbage_kind_id_req').hide();
            $('.total_req').hide();
            $('.type_req').hide();
            $('#company_user_id_req').hide();
            $('#do_datetime_req').hide();
            $('.has-error').removeClass('has-error');
            
            with(document.add_edit)
            {
                var check_detail_error = false;
                for(var i = 0; i < $('select[name="garbage_category_id[]"]').length; i++)
                {
                    if($('select[name="garbage_category_id[]"]')[i].value == "")
                    {
                        check_detail_error = true;
                    }
                    else if($('select[name="garbage_kind_id[]"]')[i].value == "")
                    {
                        check_detail_error = true;
                    }
                    else if($('input[name="total[]"]')[i].value == "")
                    {
                        check_detail_error = true;
                    }
                    else if($('select[name="type[]"]')[i].value == "")
                    {
                        check_detail_error = true;
                    }
                }

                if(company_sort_point_id.value=="")
                {
                    $('#company_sort_point_id_req').show();
                    $('#company_sort_point_id_req').parent('div').addClass('has-error');
                    $('#company_sort_point_id').focus();
                    return false;
                }
                else if($('input[name="total[]"]').length == 0)
                {
                    $('.detail_req').show();
                    $('#detail_req').parent('div').addClass('has-error');
                    return false;
                }
                else if(check_detail_error == true)
                {
                    for(var i = 0; i < $('select[name="garbage_category_id[]"]').length; i++)
                    {
                        if($('select[name="garbage_category_id[]"]')[i].value == "")
                        {
                            var select = $('select[name="garbage_category_id[]"]').filter(function() { return this.value == ""; });
                            $(select).parent().find('.garbage_category_id_req').show();
                            $(select).parent().find('.garbage_category_id_req').parent('div').addClass('has-error');
                            $(select)[0].focus();
                            return false;
                        }
                        else if($('select[name="garbage_kind_id[]"]')[i].value == "")
                        {
                            var select = $('select[name="garbage_kind_id[]"]').filter(function() { return this.value == ""; });
                            $(select).parent().parent().find('.garbage_kind_id_req').show();
                            $(select).parent().parent().find('.garbage_kind_id_req').parent('div').addClass('has-error');
                            $(select)[0].focus();
                            return false;
                        }
                        else if($('input[name="total[]"]')[i].value == "")
                        {
                            var input = $('input[name="total[]"]').filter(function() { return this.value == ""; });
                            $(input).parent().parent().find('.total_req').show();
                            $(input).parent().parent().find('.total_req').parent('div').addClass('has-error');
                            $(input)[0].focus();
                            return false;
                        }
                        else if($('select[name="type[]"]')[i].value == "")
                        {
                            var select = $('select[name="type[]"]').filter(function() { return this.value == ""; });
                            $(select).parent().parent().find('.type_req').show();
                            $(select).parent().parent().find('.type_req').parent('div').addClass('has-error');
                            $(select)[0].focus();
                            return false;
                        }
                    }
                }
                else if(company_user_id.value=="")
                {
                    $('#company_user_id_req').show();
                    $('#company_user_id_req').parent('div').addClass('has-error');
                    $('#company_user_id').focus();
                    return false;
                }
                else if(do_datetime.value=="")
                {
                    $('#do_datetime_req').show();
                    $('#do_datetime_req').parent('div').addClass('has-error');
                    $('#do_datetime').focus();
                    return false;
                }
            }
        }

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "desc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                    { orderable: false, targets: -1 },
                    {  className: "status", targets: -2 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                responsive: true,
                        bPaginate: true,
                        bLengthChange: true,
                        bInfo: true,
                        bAutoWidth: false,
                        language: {
                        paginate: {
                          previous: "<img src='{$image_url}theme/default/assets/images/template/previous.png'>",
                          next: "<img src='{$image_url}theme/default/assets/images/template/next.png' />",
                        },
                        },
                'dom': '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
                'ajax': {
                    'url': '{$base_url}{$page}/load_data',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'do_datetime',
                        'render': function (data, type, full, meta) {
                            return ((data != '' && data != null) ? moment(data).format('DD.MM.YYYY HH.mm') : '-');
                        }
                    },
                    {
                        'data': 'id'
                    },
                    {
                        'data': 'company_sort_point'
                    },
                    {
                        'data': 'total_weight'
                    },
                    {
                        'data': 'total_qty'
                    },
                    {
                        'data': 'company_user'
                    },
                    {
                        'data': 'update_name'
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}{$page}/detail/' + data + '"><img src="{$image_url}theme/default/assets/images/template/dots-icon.png" /></a><button type="button" onclick="call_modal(\'edit\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Edit.png" /></button><button type="button" onclick="call_modal(\'delete\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Delete.png" /></button>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.draw();
            });

            {if $smarty.get.edit > 0}
                call_modal('edit', {$smarty.get.edit});
            {/if}

        });
    </script>
{/block}