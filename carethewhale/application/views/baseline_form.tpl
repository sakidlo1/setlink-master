<form id="baseline_form" name="add_edit" action="{$base_url}{$page}/form/{$action}/{$id}" method="post" onsubmit="return check_data();">
  <input type="hidden" name="action" id="action" value="{$action}">
  <input type="hidden" name="id" id="id" value="{$id}">
  <input type="hidden" name="item_garbage_category_id" id="item_garbage_category_id" value="{$item.garbage_category_id}">
  <input type="hidden" name="item_garbage_kind_id" id="item_garbage_kind_id" value="{$item.garbage_kind_id}">
	<div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">
          {if $action == 'add'}
            เพิ่มปีฐานของขยะ
          {elseif $action == 'edit'}
            แก้ไขปีฐานของขยะ
          {elseif $action == 'delete'}
            ยืนยันการลบปีฐานของขยะ
          {/if}
        </h5>
        <button
          type="button"
          class="close btn-close-modal"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {if $action == 'add' || $action == 'edit'}
        <div class="in-modal-form">
          <div class="wrap-input">
            <label>สถานที่คัดแยก</label>
            <select name="company_sort_point_id" id="company_sort_point_id" class="form-control">
              <option value="">[ สถานที่คัดแยก ]</option>
              {foreach $company_sort_point as $child_item}
                <option value="{$child_item.id}"{if $item.company_sort_point_id == $child_item.id} selected="selected"{/if}>{$child_item.name}</option>
              {/foreach}
            </select>
          </div>
          <label id="company_sort_point_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกสถานที่คัดแยก</small>
            </label>
          <div class="wrap-input">
            <label>หมวดหมู่ขยะ</label>
            <select name="garbage_category_id" id="garbage_category_id" class="form-control">
              <option value="">[ หมวดหมู่ขยะ ]</option>
              {foreach $garbage_category as $child_item}
                <option value="{$child_item.id}"{if $item.garbage_category_id == $child_item.id} selected="selected"{/if}>{$child_item.name}</option>
              {/foreach}
            </select>
          </div>
          <label id="garbage_category_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกหมวดหมู่ขยะ</small>
            </label>
          <div class="wrap-input">
            <label>รายการขยะ</label>
            <select name="garbage_kind_id" id="garbage_kind_id" class="form-control">
              <option value="">[ รายการขยะ ]</option>
            </select>
          </div>
          <label id="garbage_kind_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกรายการขยะ</small>
            </label>
          <div class="wrap-input">
            <label>วิธีการจัดการขยะ</label>
            <select name="garbage_disposal_id" id="garbage_disposal_id" class="form-control">
              <option value="">[ วิธีการจัดการขยะ ]</option>
              {foreach $garbage_disposal as $child_item}
                <option value="{$child_item.id}"{if $item.garbage_disposal_id == $child_item.id} selected="selected"{/if}>{$child_item.name}</option>
              {/foreach}
            </select>
          </div>
          <label id="garbage_disposal_id_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกวิธีการจัดการขยะ</small>
            </label>
          <div class="wrap-input special-input">
            <label>ค่า Baseline </label>
            <input value="{$item.baseline}" name="baseline" id="baseline" type="number" step=".01" class="form-control" />
            <span>ก.ก.</span>
          </div>
          <label id="baseline_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณากรอกค่า Baseline</small>
            </label>
          <div class="wrap-input inline-checkbox">
            <label style="display: block">ระยะเวลาของการจัดการขยะ</label>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                id="is_daily"
                value="is_daily"
                {if $item.is_daily == 'Y'} checked="checked"{/if}
              />
              <label class="form-check-label" for="is_daily">รายวัน</label>
            </div>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                id="is_weekly"
                value="is_weekly"
                {if $item.is_weekly == 'Y'} checked="checked"{/if}
              />
              <label class="form-check-label" for="is_weekly">รายสัปดาห์</label
              >
            </div>
            <div class="form-check form-check-inline">
              <input
                class="form-check-input"
                type="radio"
                name="type"
                id="is_monthly"
                value="is_monthly"
                {if $item.is_monthly == 'Y'} checked="checked"{/if}
              />
              <label class="form-check-label" for="is_monthly">รายเดือน</label
              >
            </div>
          </div>
          <label id="type_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาเลือกระยะเวลาของการจัดการขยะ</small>
            </label>
        </div>
        {elseif $action == 'delete'}
          กรุณาตรวจสอบข้อมูล ({$item.id} : {$item.name}) ที่คุณ ต้องการลบ
          เนื่องจากหากลบข้อมูลไปแล้วอาจส่งผลต่อ การทำงานอื่นได้
        {/if}
      </div>
      <div class="modal-footer">
        {if $action == 'add'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
          <img src="{$image_url}theme/default/assets/images/template/plus-icon.png" />
          เพิ่ม
        </button>
        {elseif $action == 'edit'}
        <button type="submit" name="save" value="save" class="btn btn-primary">
            <img src="{$image_url}theme/default/assets/images/template/save-icon.png" />
            บันทึก
          </button>
          <button
            data-dismiss="modal"
            type="button"
            class="btn btn-primary cancle-btn"
          >
            ยกเลิก
          </button>
        {elseif $action == 'delete'}
        <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              ยกเลิก
            </button>
            <button type="submit" name="save" value="save" class="btn btn-primary">ลบข้อมูล</button>
        {/if}
      </div>
</form>
<style>
  #baseline_form .form-check-input
  {
    opacity: 1;
    position: static;
    width: auto;
    height: auto;
    margin-right: 5px;
    margin-top: -1px;
  }
</style>