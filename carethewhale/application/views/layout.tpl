<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{block name=meta_title}{$page} - {$site_name} : {$company_name}{/block}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{$image_url}theme/default/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/bootstrap-datetimepicker.min.css" />
    <link
      href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="{$image_url}theme/default/css/datatables.min.css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/select.dataTables.min.css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/responsive.dataTables.min.css" />
    <link
      href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="{$image_url}theme/default/css/daterangepicker.css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/main.css?ver={$smarty.now}" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/responsive.css?ver={$smarty.now}" />
<!-- Matomo -->
<script type="text/javascript">
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//climatecare.setsocialimpact.com/analytics/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '4']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
  </head>
  <body id="{$page}-{$sub_page}">
    <div class="page-wrapper chiller-theme toggled">
      <a id="show-sidebar" class="btn" href="#">
        <i class="fas fa-bars"></i>
      </a>
      <nav id="sidebar" class="sidebar-wrapper">
        <div class="sidebar-content">
          <div class="sidebar-brand">
            <a href="{$base_url}">
              <img src="{$image_url}theme/default/assets/images/login/logo-login.png" alt="" />
              <span>CLIMATE CARE PLATFORM</span>
            </a>
            <div class="hide-sidebar" id="close-sidebar">
              <i class="fas fa-bars"></i>
            </div>
          </div>
          <div class="sidebar-menu">
            <ul>
              <li{if $page == 'home'} class="current"{/if}>
                <a href="{$base_url}">
                  <img
                    class="img-show"
                    src="{$image_url}theme/default/assets/images/nav/main1-show.png"
                  />
                  <img
                    class="img-hide"
                    src="{$image_url}theme/default/assets/images/nav/main1-hide.png"
                  />
                  <span>ภาพรวมข้อมูล</span>
                </a>
              </li>
              {if $member.type == 'M'}
                <li{if $page == 'user'} class="current"{/if}>
                  <a href="{$base_url}user">
                    <img
                      class="img-show"
                      src="{$image_url}theme/default/assets/images/nav/corp-main2-show.png"
                    />
                    <img
                      class="img-hide"
                      src="{$image_url}theme/default/assets/images/nav/corp-main2-hide.png"
                    />
                    <span>ผู้ใช้งาน</span>
                  </a>
                </li>
                <li{if $page == 'sort_point'} class="current"{/if}>
                  <a href="{$base_url}sort_point">
                    <img
                      class="img-show"
                      src="{$image_url}theme/default/assets/images/nav/corp-main3-show.png"
                    />
                    <img
                      class="img-hide"
                      src="{$image_url}theme/default/assets/images/nav/corp-main3-hide.png"
                    />
                    <span>สถานที่ตั้งถัง</span>
                  </a>
                </li>
              {/if}
              <li class="sidebar-dropdown{if $authen->controller == 'sort_first' || $authen->controller == 'sort_middle' || $authen->controller == 'sort_last'} active{/if}">
                <a href="#">
                  <img
                    class="img-show"
                    src="{$image_url}theme/default/assets/images/nav/corp-main5-show.png"
                  />
                  <img
                    class="img-hide"
                    src="{$image_url}theme/default/assets/images/nav/corp-main5-hide.png"
                  />
                  <span>การจัดการขยะ</span>
                </a>
                <div class="sidebar-submenu"{if $authen->controller == 'sort_first' || $authen->controller == 'sort_middle' || $authen->controller == 'sort_last'} style="display: block"{/if}>
                  <ul>
                    <li{if $authen->controller == 'sort_first'} class="current"{/if}>
                      <a href="{$base_url}sort_first">ต้นทาง</a>
                    </li>
                    <li{if $authen->controller == 'sort_middle'} class="current"{/if}>
                      <a href="{$base_url}sort_middle">กลางทาง</a>
                    </li>
                    <li{if $authen->controller == 'sort_last'} class="current"{/if}>
                      <a href="{$base_url}sort_last">ปลายทาง</a>
                    </li>
                  </ul>
                </div>
              </li>
              {if $member.type == 'M'}
              <li{if $page == 'report'} class="current"{/if}>
                <a href="{$base_url}report">
                  <img
                    class="img-show"
                    src="{$image_url}theme/default/assets/images/nav/corp-main6-show.png"
                  />
                  <img
                    class="img-hide"
                    src="{$image_url}theme/default/assets/images/nav/corp-main6-hide.png"
                  />
                  <span>รายงาน</span>
                </a>
              </li>
              <li{if $page == 'baseline'} class="current"{/if}>
                <a href="{$base_url}baseline">
                  <img
                    class="img-show"
                    src="{$image_url}theme/default/assets/images/nav/corp-main7-show.png"
                  />
                  <img
                    class="img-hide"
                    src="{$image_url}theme/default/assets/images/nav/corp-main7-hide.png"
                  />
                  <span>ปีฐาน</span>
                </a>
              </li>
              {/if}
            </ul>
          </div>
        </div>
      </nav>
      <main class="page-content">
        <div class="content">
          <div class="top-bar clearfix">
          	<div class="top-bar-left text-left">
              <div class="wrap-search">
                <img src="{$image_url}theme/default/assets/images/template/search-icon.png" />
                <input
                  type="text"
                  class="form-control"
                  placeholder="ค้นหาข้อมูล"
                />
              </div>
            </div>
            <div class="top-bar-right text-right">
              <div class="noti">
              </div>
              <div class="user-detail">
                <div class="dropdown show">
                  <a
                    class="btn dropdown-toggle"
                    href="#"
                    role="button"
                    id="dropdownMenuLink"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div class="wrap-userdetail">
                      <div class="left">
                        <img src="{$member.avatar}" onerror="this.src='{$image_url}theme/default/assets/images/template/thumbnail-trash.png';" style="width: 30px; height: 30px;">
                      </div>
                      <div class="right">
                        <span class="user-name">{$member.name}</span>
                        <span class="user-role">
                        	{if $member.type == 'M'}
                        		Administrator
                        	{elseif $member.type == 'S'}
                        		Staff
                        	{/if}
                        </span>
                      </div>
                    </div>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					{if $member.type == 'M'}
						<a class="dropdown-item" href="{$base_url}member/profile">โปรไฟล์</a>
					{/if}
                    <a class="dropdown-item" href="{$base_url}member/logout">ออกจากระบบ</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="main-content">
            {block name=body}{/block}
          </div>
        </div>
      </main>
      <!-- page-content" -->
    </div>
    <!-- page-wrapper -->

    <footer>
        <div class="copyright">ตลาดหลักทรัพย์แห่งประเทศไทย | สงวนลิขสิทธิ์</div>
        <div class="cookies" style="display: none;">
            <div class="container">
                <p>กลุ่มตลาดหลักทรัพย์แห่งประเทศไทยมีการใช้งานคุกกี้ (Cookies) <nobr>เพื่อจัดการข้อมูลส่วนบุคคลและ</nobr><nobr>ช่วยเพิ่มประสิทธิภาพการใช้งานเว็บไซต์</nobr>
                <br />
                ท่านสามารถศึกษารายละเอียดเพิ่มเติมและการตั้งค่าคุกกี้ได้ที่ <nobr>นโยบายการใช้คุ้กกี้</nobr> </p>
                <button onclick="setCookie('cookies-consent','yes',7); $('.cookies').hide();" type="button" class="btn btn-black">ยอมรับ</button>
            </div>
        </div>
    </footer>

    <script src="{$image_url}theme/default/js/bootstrap5/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{$image_url}theme/default/js/jquery.dataTables.min.js"></script>
    <script src="{$image_url}theme/default/js/datatables-select.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/popper.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/bootstrap.bundle.min.js"></script>
    <script src="{$image_url}theme/default/js/dataTables.responsive.min.js"></script>
    <script src="{$image_url}theme/default/js/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.3.3/backbone-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.marionette/2.4.7/backbone.marionette.min.js"></script>
    <script src="{$image_url}theme/default/js/upload-multiple.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://use.fontawesome.com/7ad89d9866.js"></script>
    <script type="text/javascript" src="{$image_url}theme/default/js/moment.min.js"></script>
    <script type="text/javascript" src="{$image_url}theme/default/js/daterangepicker.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap-datetimepicker.min.js"></script>

    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

    <script>
    	$(function ($) {
        $(".sidebar-dropdown > a").click(function () {
          $("li.current").addClass("history");
          $("li.current").removeClass("current");
          $(".sidebar-submenu").slideUp(200);

          if ($(this).parent().hasClass("active")) {
            $(".sidebar-dropdown").removeClass("active");
            $(this).parent().removeClass("active");

            $("li.history").addClass("current");
            $("li.history").removeClass("history");
          } else {
            $(".sidebar-dropdown").removeClass("active");
            $(this).next(".sidebar-submenu").slideDown(200);
            $(this).parent().addClass("active");
          }
        });

        $("#close-sidebar").click(function () {
          $(".page-wrapper").removeClass("toggled");
        });
        $("#show-sidebar").click(function () {
          $(".page-wrapper").addClass("toggled");
        });

        if($(window).width() <= 991)
        {
          $(".page-wrapper").removeClass("toggled");
        }
      });
    </script>

    <script>
      function setCookie(name,value,days) {
          var expires = "";
          if (days) {
              var date = new Date();
              date.setTime(date.getTime() + (days*24*60*60*1000));
              expires = "; expires=" + date.toUTCString();
          }
          document.cookie = name + "=" + (value || "")  + expires + "; path=/";
      }
      function getCookie(name) {
          var nameEQ = name + "=";
          var ca = document.cookie.split(';');
          for(var i=0;i < ca.length;i++) {
              var c = ca[i];
              while (c.charAt(0)==' ') c = c.substring(1,c.length);
              if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
          }
          return null;
      }
      function cookieConsent() {
        if (!getCookie('cookies-consent')) {
          $('.cookies').show();
        }
      }

      window.onload = function() { cookieConsent(); };
    </script>
    {block name=script}{/block}
  </body>
</html>
