{extends file="layout.tpl"}
{block name=meta_title}{$page_name} - {$site_name} : {$company_name}{/block}
{block name=body}
	<h1>สถานที่ตั้งถัง </h1>
        <div class="breadcrumb-section clearfix">
          <div class="nav-section-left">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="{$base_url}"
                    ><img src="{$image_url}theme/default/assets/images/template/home-icon.png"
                  /></a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                  <a href="#">สถานที่ตั้งถัง  </a>
                </li>
              </ol>
            </nav>
          </div>
          <div class="clear-fix"></div>
        </div>
        {if $success_msg != ""}
		<div class="alert alert-success">
			{$success_msg}
		</div>
		{/if}
		{if $error_msg != ""}
		<div class="alert alert-danger">
			{$error_msg}
		</div>
		{/if}
        <div class="filter-section clearfix">
          <div class="filter-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="fName">ค้นหา : &nbsp;&nbsp;&nbsp;</label>
                        <input class="form-control" id="fName" type="text" size="10" />
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="button" value="Search" id="fSearch" />
                    </div>
                </div>
            </div>
          <div class="filter-right">
            <button type="button" onclick="call_modal('add');">
              <img src="{$image_url}theme/default/assets/images/register/add.svg" />
            </button>
          </div>
        </div>
        <div class="table-section">
          	<table id="data-tables" class="display table-button-no-style" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>รหัสที่ตั้ง</th>
						<th>ชื่อสถานที่ตั้งถัง/จุดคัดแยก</th>
						<th>คำอธิบาย</th>
						<th>วันที่สร้าง</th>
                        <th>วันที่แก้ไขล่าสุด</th>
						<th></th>
					</tr>
				</thead>
			</table>
        </div>
        <div class="modal fade modal-with-form" id="modalAction" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	      <div class="modal-dialog modal-dialog-scrollable" role="document">
	        <div class="modal-content">
	          
	        </div>
	      </div>
	    </div>
{/block}
{block name=script}
	<script>
		function call_modal(action, id)
		{
			if(typeof id == 'undefined') {
				id = 0
			}

			if(action == 'delete') {
				$('#modalAction').addClass('modal-delete');
                $('#modalAction').removeClass('modal-right-full');
			} else {
                $('#modalAction').addClass('modal-right-full');
				$('#modalAction').removeClass('modal-delete');
			}

			$.get('{$base_url}{$page}/form/' + action + '/' + id, function( data ) {
			  $('#modalAction .modal-content').html(data);
			  $('#modalAction').modal('show');

              $(".upload-wrap input[type=file]").change(function () {
                  var id = $(this).attr("id");
                  var newimage = new FileReader();
                  newimage.readAsDataURL(this.files[0]);
                  newimage.onload = function (e) {
                    $(".uploadpreview." + id).css(
                      "background-image",
                      "url(" + e.target.result + ")"
                    );
                    $(".uploadpreview." + id).addClass("active-image");
                    $(".delete-img." + id).addClass("show");
                    $("#file_" + id).val(e.target.result);
                  };
                });
                $(".delete-img").click(function () {
                  $(this).removeClass("show");
                  $(this).addClass("hide");
                  $(this).prev(".uploadpreview").css("background-image", "url('')");
                  $(this).prev(".uploadpreview").removeClass("active-image");
                  $(this).next('input[name="file_image[]"]').val('');
                });
			});
		}

		function check_data()
		{
			$('#name_req').hide();
			$('.has-error').removeClass('has-error');
			
			with(document.add_edit)
			{
				if(name.value=="")
				{
					$('#name_req').show();
					$('#name_req').parent('div').addClass('has-error');
					$('#name').focus();
					return false;
				}
			}
		}

        $(document).ready(function () {

            $('#data-tables').DataTable({
                "order": [[0, "asc"]],
                "lengthMenu": [[20, 50, 100, 200], [20, 50, 100, 200]],
                "pageLength": 20,
                columnDefs: [
                	{ orderable: false, targets: -1 },
                	{  className: "status", targets: -2 }
                ],
                'processing': true,
                'serverSide': true,
                'orderMulti': false,
                responsive: true,
        				bPaginate: true,
        				bLengthChange: true,
        				bInfo: true,
        				bAutoWidth: false,
        				language: {
        				paginate: {
        				  previous: "<img src='{$image_url}theme/default/assets/images/template/previous.png'>",
        				  next: "<img src='{$image_url}theme/default/assets/images/template/next.png' />",
        				},
        				},
                'dom': '<"top"l<"clear">>rt<"bottom"ip<"clear">>',
                'ajax': {
                    'url': '{$base_url}{$page}/load_data',
                    'type': 'POST',
                    'dataType': 'json'
                },
                'columns': [
                    {
                        'data': 'id'
                    },
                    {
                        'data': 'name'
                    },
                    {
                        'data': 'description'
                    },
                    {
                        'data': 'created_on',
                        'render': function (data, type, full, meta) {
                            return ((data != '' && data != null) ? moment(data).format('DD.MM.YYYY HH.mm') : '-');
                        }
                    },
                    {
                        'data': 'updated_on',
                        'render': function (data, type, full, meta) {
                            return ((data != '' && data != null) ? moment(data).format('DD.MM.YYYY HH.mm') : '-');
                        }
                    },
                    {
                        'data': 'id',
                        'render': function (data, type, full, meta) {
                            return '<a href="{$base_url}{$page}/detail/' + data + '"><img src="{$image_url}theme/default/assets/images/template/dots-icon.png" /></a><button type="button" onclick="call_modal(\'edit\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Edit.png" /></button><button type="button" onclick="call_modal(\'delete\', '+data+');"><img src="{$image_url}theme/default/assets/images/template/Delete.png" /></button>';
                        }
                    }
                ]
            });

            oTable = $('#data-tables').DataTable();

            $('#fSearch').click(function () {
                oTable.columns(0).search($('#fName').val().trim());
                oTable.draw();
            });

        });
    </script>
{/block}