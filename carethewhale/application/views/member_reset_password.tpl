<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{$page_name} - {$site_name} : {$company_name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/main.css?ver={$smarty.now}" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/responsive.css?ver={$smarty.now}" />
    <link href="{$image_url}theme/default/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  </head>
  <body>
    <div class="login-page height100">
      <div class="login-left text-center">
        <div class="login-left-boxed">
          <h2>ตั้งรหัสผ่านใหม่</h2>
          <img
            class="logo-login"
            src="{$image_url}theme/default/assets/images/login/logo-login2.png"
            alt="logo-login"
          />
          <h1>CLIMATE CARE PLATFORM</h1>
            <script>
                function check_login_data()
                {
                    $('#password_req').hide();
                    $('#confirm_password_req').hide();
                    $('#confirm_password_inc').hide();
                    $('.has-error').removeClass('has-error');
                    
                    with(document.login)
                    {
                        if(password.value=="")
                        {
                            $('#password_req').show();
                            $('#password_req').parent('div').addClass('has-error');
                            $('#password').focus();
                            return false;
                        }
                        else if(cpassword.value=="")
                        {
                            $('#confirm_password_req').show();
                            $('#confirm_password_req').parent('div').addClass('has-error');
                            $('#cpassword').focus();
                            return false;
                        }
                        else if(password.value!=cpassword.value)
                        {
                            $('#confirm_password_inc').show();
                            $('#confirm_password_inc').parent('div').addClass('has-error');
                            $('#cpassword').focus();
                            return false;
                        }
                    }
                }
          </script>
          <form name="login" method="post" onsubmit="return check_login_data();">
            <div class="wrap-input">
              <input
                type="password"
                class="form-control"
                id="password"
                name="password"
                placeholder="รหัสผ่านใหม่"
              />
              <img
                class="pass-icon-rearrage"
                src="{$image_url}theme/default/assets/images/login/password.png"
                alt="icon-pass"
              />
            </div>
            <label id="password_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณาระบุรหัสผ่านใหม่</small>
            </label>
            <div class="wrap-input">
              <input
                type="password"
                class="form-control"
                id="cpassword"
                name="cpassword"
                placeholder="ยืนยันรหัสผ่าน"
              />
              <img
                class="pass-icon-rearrage"
                src="{$image_url}theme/default/assets/images/login/password.png"
                alt="icon-pass"
              />
            </div>
            <label id="confirm_password_req" style="display: none;" class="text-danger col-12 text-right">
                <small>กรุณายืนยันรหัสผ่าน</small>
            </label>
            <label id="confirm_password_inc" style="display: none;" class="text-danger col-12 text-right">
                <small>ยืนยันรหัสผ่านไม่ถูกต้อง</small>
            </label>
            {if $error_msg != ""}
                <label class="text-danger col-12 text-center">
                    <small>{$error_msg}</small>
                </label>
            {/if}
            {if $success_msg != ""}
                <label class="text-success col-12 text-center">
                    <small>{$success_msg}</small>
                </label>
            {/if}
            <button type="submit" name="reset_password" value="reset_password" class="btn btn-primary">ยืนยัน</button>
          </form>
          <div class="bottom-form">
            <a href="{$base_url}member/login">เข้าสู่ระบบ</a>
          </div>
        </div>
      </div>
      <div class="login-right">
        {include file='guest_stats.tpl'}
      </div>
    </div>
    <script src="{$image_url}theme/default/js/bootstrap5/jquery.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/popper.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/bootstrap.bundle.min.js"></script>
    <script>
      $(".count").each(function () {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                now = Number(Math.ceil(now)).toLocaleString("en");
                $(this).text(now);
              },
            }
          );
      });
    </script>
  </body>
</html>