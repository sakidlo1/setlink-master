<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>{$page_name} - {$site_name} : {$company_name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/main.css?ver={$smarty.now}" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/responsive.css?ver={$smarty.now}" />
    <link href="{$image_url}theme/default/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$image_url}theme/default/css/bootstrap-datetimepicker.min.css" />
  </head>
  <body>
    <div class="login-page height100">
      <div class="login-left text-center">
        <div class="login-left-boxed register-complete">
          <img
            class="logo-login"
            src="{$image_url}theme/default/assets/images/login/logo-login2.png"
            alt="logo-login"
          />
          <h1>CLIMATE CARE PLATFORM</h1>
          <div class="message-section">
            <span>เกิดข้อผิดพลาดในการสมัครสมาชิก</span>
            <div class="alert-success-register">
              <button onclick="window.location='{$base_url}';">
                <img src="{$image_url}theme/default/assets/images/success-register/cross.png" />
              </button>
              <p>
                ไม่ผ่านระบบป้องกันโปรแกรมอัตโนมัติ
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="login-right">
        {include file='guest_stats.tpl'}
      </div>
    </div>
    <script src="{$image_url}theme/default/js/bootstrap5/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/popper.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap5/bootstrap.bundle.min.js"></script>
    <script src="{$image_url}theme/default/js/moment.min.js"></script>
    <script src="{$image_url}theme/default/js/bootstrap-datetimepicker.min.js"></script>
    <script>
      $(".count").each(function () {
        $(this)
          .prop("Counter", 0)
          .animate(
            {
              Counter: $(this).text(),
            },
            {
              duration: 4000,
              easing: "swing",
              step: function (now) {
                now = Number(Math.ceil(now)).toLocaleString("en");
                $(this).text(now);
              },
            }
          );
      });
    </script>
  </body>
</html>