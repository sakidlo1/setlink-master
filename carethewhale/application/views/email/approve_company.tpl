<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div>
	    <a href="{$base_url}">
			<img src="{$image_url}theme/default/assets/images/login/logo-login.png">
		</a>
		<br />
		<br />
	</div>
	<div>
		<br />
		[[company_name]]
		<br />
		<p style="padding-left: 30px;">
			สามารถเข้าใช้งาน Climate Care Platform ได้ตั้งแต่วันนี้  
			<br/>
			ได้ที่ <a href="https://climatecare.setsocialimpact.com">https://climatecare.setsocialimpact.com</a>
		</p>
		<br />
		ขอบคุณที่ใช้บริการ
		<br />
		Climate Care Platform
		<br />
		<br />
	</div>
	<hr />
	<div>
		Dear [[company_name]],
		<br />
		<p style="padding-left: 30px;">
			Can access Climate Care Platform.
			<br />
			at <a href="https://climatecare.setsocialimpact.com">https://climatecare.setsocialimpact.com</a>
		</p>
		<br />
		Best regards,
		<br />
		Climate Care Platform
		<br />
		<br />
	</div>
  </body>
</html>