<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div>
	    <a href="{$base_url}">
			<img src="{$image_url}theme/default/assets/images/login/logo-login.png">
		</a>
		<br />
		<br />
	</div>
	<div>
		<br />
		เรียนคุณ [[customer_name]],
		<br />
		<p style="padding-left: 30px;">
			คุณได้ทำการแจ้งลืมรหัสผ่านที่ {$site_name}
			<br />
			กรุณาคลิกลิงค์ด้านล่าง เพื่อตั้งรหัสผ่านใหม่
			<br />
			<br />
			<a href="[[link]]">
				ตั้งรหัสผ่านใหม่
			</a>
		</p>
		<br />
		ขอบคุณที่ใช้บริการ,
		<br />
		{$site_name}
		<br />
		<br />
	</div>
	<hr />
	<div>
		Dear [[customer_name]],
		<br />
		<p style="padding-left: 30px;">
			You have informed that you forgot your password on {$site_name}.
			<br />
			Please click on the link below to reset your password.
			<br />
			<br />
			<a href="[[link]]">
				Reset Password
			</a>
		</p>
		<br />
		Best regards,
		<br />
		{$site_name}
		<br />
		<br />
	</div>
  </body>
</html>