<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <div>
	    <a href="{$base_url}">
			<img src="{$image_url}theme/default/assets/images/login/logo-login.png">
		</a>
		<br />
		<br />
	</div>
	<div>
		<br />
		เรียนคุณ [[customer_name]],
		<br />
		<p style="padding-left: 30px;">
			คุณได้ทำการสร้างบัญชีใน {$site_name} เรียบร้อยแล้ว
			<br />
			กรุณาคลิกลิงค์ด้านล่าง เพื่อยืนยันตัวตนของท่านให้สำเร็จ
			<br />
			<br />
			<a href="[[link]]">
				ยืนยันตัวตน
			</a>
		</p>
		<br />
		ขอบคุณที่ใช้บริการ,
		<br />
		{$site_name}
		<br />
		<br />
	</div>
	<hr />
	<div>
		Dear [[customer_name]],
		<br />
		<p style="padding-left: 30px;">
			You have successfully created an account on {$site_name}.
			<br />
			Please click on the link below to successfully verify your identity.
			<br />
			<br />
			<a href="[[link]]">
				Verify Email
			</a>
		</p>
		<br />
		Best regards,
		<br />
		{$site_name}
		<br />
		<br />
	</div>
  </body>
</html>