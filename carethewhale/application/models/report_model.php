<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class report_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_year()
    {
        $this->db->select('min(YEAR(do_datetime)) as min_year, max(YEAR(do_datetime)) as max_year', false);
        $this->db->from('company_sort_last');
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function get_total_cf($start = '', $end = '')
    {
    	$this->db->select('sum(total_cf) as total_cf');
        $this->db->from('company_sort_last');
        $this->db->where("do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        return $data['total_cf'];
    }

    function get_total_cf_by_garbage_type($start = '', $end = '')
    {
    	$this->db->select('garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
        $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_type.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_total_cf_by_garbage_plastic($start = '', $end = '')
    {
    	$this->db->select('garbage_kind.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where('garbage_kind.garbage_category_id', 5);
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_kind.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_total_cf_by_garbage_recycle($start = '', $end = '')
    {
    	$this->db->select('garbage_category.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where('garbage_category.garbage_type_id', 2);
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_category.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

    function top_10()
    {
        $get_year = $this->get_year();

        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');
        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("YEAR(company_sort_last.do_datetime) = '".$i."'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $this->db->limit(10);
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $this->db->limit(10);
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $this->db->limit(10);
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
            $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_last.status', 'Y');
            $this->db->group_by('garbage_kind.id');
            $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
            $this->db->limit(10);
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }

        return $data;
    }

    function type()
    {
        $garbage = (($this->input->get('garbage') != '') ? $this->input->get('garbage') : 'non_recycle'); 
        
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');

        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');

                if($garbage == 'plastic')
                {
                    $this->db->where('garbage_kind.garbage_category_id', 5);
                }
                else if($garbage == 'recycle')
                {
                    $this->db->where('garbage_category.garbage_type_id', 2);
                }
                else if($garbage == 'non_recycle')
                {
                    $this->db->where("garbage_category.garbage_type_id <> 2");
                }

                $this->db->where("YEAR(company_sort_last.do_datetime) = '".$i."'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');

                if($garbage == 'plastic')
                {
                    $this->db->where('garbage_kind.garbage_category_id', 5);
                }
                else if($garbage == 'recycle')
                {
                    $this->db->where('garbage_category.garbage_type_id', 2);
                }
                else if($garbage == 'non_recycle')
                {
                    $this->db->where("garbage_category.garbage_type_id <> 2");
                }

                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');

                if($garbage == 'plastic')
                {
                    $this->db->where('garbage_kind.garbage_category_id', 5);
                }
                else if($garbage == 'recycle')
                {
                    $this->db->where('garbage_category.garbage_type_id', 2);
                }
                else if($garbage == 'non_recycle')
                {
                    $this->db->where("garbage_category.garbage_type_id <> 2");
                }

                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');

            if($garbage == 'plastic')
            {
                $this->db->where('garbage_kind.garbage_category_id', 5);
            }
            else if($garbage == 'recycle')
            {
                $this->db->where('garbage_category.garbage_type_id', 2);
            }
            else if($garbage == 'non_recycle')
            {
                $this->db->where("garbage_category.garbage_type_id <> 2");
            }

            $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_last.status', 'Y');
            $this->db->group_by('garbage_kind.id');
            $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }

        return $data;
    }
	
	function type_waste()
    {
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');

        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("YEAR(company_sort_last.do_datetime) = '".$i."'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf");
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
            $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_last.status', 'Y');
            $this->db->group_by('garbage_type.id');
            $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }

        return $data;
    }

    function first()
    {
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');

        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_first_detail.total) as total_weight, company_sort_first_detail.type as unit");
                $this->db->from('company_sort_first_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_first_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_first', 'company_sort_first.id = company_sort_first_detail.company_sort_first_id');
                $this->db->where("YEAR(company_sort_first.do_datetime) = '".$i."'");
                $this->db->where('company_sort_first.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_first.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_first_detail.total) as total_weight, company_sort_first_detail.type as unit");
                $this->db->from('company_sort_first_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_first_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_first', 'company_sort_first.id = company_sort_first_detail.company_sort_first_id');
                $this->db->where("company_sort_first.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_first.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_first.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_first.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_first_detail.total) as total_weight, company_sort_first_detail.type as unit");
                $this->db->from('company_sort_first_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_first_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_first', 'company_sort_first.id = company_sort_first_detail.company_sort_first_id');
                $this->db->where("company_sort_first.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_first.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_first.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_first.status', 'Y');
                $this->db->group_by('garbage_kind.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_kind.name, garbage_type.name as type, sum(company_sort_first_detail.total) as total_weight, company_sort_first_detail.type as unit");
            $this->db->from('company_sort_first_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_first_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('company_sort_first', 'company_sort_first.id = company_sort_first_detail.company_sort_first_id');
            $this->db->where("company_sort_first.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_first.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_first.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_first.status', 'Y');
            $this->db->group_by('garbage_kind.id');
            $this->db->order_by('total_weight desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }

        return $data;
    }

    function middle()
    {
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');

        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_sort.id as garbage_sort_id, garbage_kind.name, garbage_type.name as type, garbage_sort.name as sort, sum(company_sort_middle_detail.total) as total_weight, company_sort_middle_detail.type as unit");
                $this->db->from('company_sort_middle_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_middle_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_sort', 'garbage_sort.id = company_sort_middle_detail.garbage_sort_id');
                $this->db->join('company_sort_middle', 'company_sort_middle.id = company_sort_middle_detail.company_sort_middle_id');
                $this->db->where("YEAR(company_sort_middle.do_datetime) = '".$i."'");
                $this->db->where('company_sort_middle.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_middle.status', 'Y');
                $this->db->group_by('garbage_kind.id, garbage_sort.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_sort.id as garbage_sort_id, garbage_kind.name, garbage_type.name as type, garbage_sort.name as sort, sum(company_sort_middle_detail.total) as total_weight, company_sort_middle_detail.type as unit");
                $this->db->from('company_sort_middle_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_middle_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_sort', 'garbage_sort.id = company_sort_middle_detail.garbage_sort_id');
                $this->db->join('company_sort_middle', 'company_sort_middle.id = company_sort_middle_detail.company_sort_middle_id');
                $this->db->where("company_sort_middle.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_middle.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_middle.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_middle.status', 'Y');
                $this->db->group_by('garbage_kind.id, garbage_sort.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_sort.id as garbage_sort_id, garbage_kind.name, garbage_type.name as type, garbage_sort.name as sort, sum(company_sort_middle_detail.total) as total_weight, company_sort_middle_detail.type as unit");
                $this->db->from('company_sort_middle_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_middle_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_sort', 'garbage_sort.id = company_sort_middle_detail.garbage_sort_id');
                $this->db->join('company_sort_middle', 'company_sort_middle.id = company_sort_middle_detail.company_sort_middle_id');
                $this->db->where("company_sort_middle.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_middle.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_middle.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_middle.status', 'Y');
                $this->db->group_by('garbage_kind.id, garbage_sort.id');
                $this->db->order_by('total_weight desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_kind.id as garbage_kind_id, garbage_sort.id as garbage_sort_id, garbage_kind.name, garbage_type.name as type, garbage_sort.name as sort, sum(company_sort_middle_detail.total) as total_weight, company_sort_middle_detail.type as unit");
            $this->db->from('company_sort_middle_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_middle_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('garbage_sort', 'garbage_sort.id = company_sort_middle_detail.garbage_sort_id');
            $this->db->join('company_sort_middle', 'company_sort_middle.id = company_sort_middle_detail.company_sort_middle_id');
            $this->db->where("company_sort_middle.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_middle.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_middle.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_middle.status', 'Y');
            $this->db->group_by('garbage_kind.id, garbage_sort.id');
            $this->db->order_by('total_weight desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }

        return $data;
    }

    function last()
    {
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');

        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_disposal.id as garbage_disposal_id, disposal_company.id as disposal_company_id, garbage_kind.name, garbage_type.name as type, garbage_disposal.name as disposal, disposal_company.name as disposal_company, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf, CONCAT(garbage_disposal.name, ' - ', disposal_company.name) as disposal_with_company", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_disposal', 'garbage_disposal.id = company_sort_last_detail.garbage_disposal_id');
                $this->db->join('disposal_company', 'disposal_company.id = company_sort_last_detail.disposal_company_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("YEAR(company_sort_last.do_datetime) = '".$i."'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id, disposal_with_company');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_disposal.id as garbage_disposal_id, disposal_company.id as disposal_company_id, garbage_kind.name, garbage_type.name as type, garbage_disposal.name as disposal, disposal_company.name as disposal_company, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf, CONCAT(garbage_disposal.name, ' - ', disposal_company.name) as disposal_with_company", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_disposal', 'garbage_disposal.id = company_sort_last_detail.garbage_disposal_id');
                $this->db->join('disposal_company', 'disposal_company.id = company_sort_last_detail.disposal_company_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id, disposal_with_company');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("garbage_kind.id as garbage_kind_id, garbage_disposal.id as garbage_disposal_id, disposal_company.id as disposal_company_id, garbage_kind.name, garbage_type.name as type, garbage_disposal.name as disposal, disposal_company.name as disposal_company, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf, CONCAT(garbage_disposal.name, ' - ', disposal_company.name) as disposal_with_company", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('garbage_disposal', 'garbage_disposal.id = company_sort_last_detail.garbage_disposal_id');
                $this->db->join('disposal_company', 'disposal_company.id = company_sort_last_detail.disposal_company_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->group_by('garbage_kind.id, disposal_with_company');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("garbage_kind.id as garbage_kind_id, garbage_disposal.id as garbage_disposal_id, disposal_company.id as disposal_company_id, garbage_kind.name, garbage_type.name as type, garbage_disposal.name as disposal, disposal_company.name as disposal_company, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf, CONCAT(garbage_disposal.name, ' - ', disposal_company.name) as disposal_with_company", false);
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('garbage_disposal', 'garbage_disposal.id = company_sort_last_detail.garbage_disposal_id');
            $this->db->join('disposal_company', 'disposal_company.id = company_sort_last_detail.disposal_company_id');
            $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
            $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
            $this->db->where('company_sort_last.status', 'Y');
            $this->db->group_by('garbage_kind.id, disposal_with_company');
            $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }
        
        return $data;
    }


    // puu
    function company_type()
    {
        $get_year = $this->get_year();
        
        $type = (($this->input->get('type') != '') ? $this->input->get('type') : 'year');
        $year_start = (($this->input->get('year_start') != '') ? $this->input->get('year_start') : (($get_year['min_year'] > 0) ? $get_year['min_year'] : date('Y')));
        $year_end = (($this->input->get('year_end') != '') ? $this->input->get('year_end') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $year = (($this->input->get('year') != '') ? $this->input->get('year') : (($get_year['max_year'] > 0) ? $get_year['max_year'] : date('Y')));
        $dates = (($this->input->get('dates') != '') ? $this->input->get('dates') : '');
        
        if($dates == '')
        {
            $start = date('Y-m').'-01';
            $end = date('Y-m-t');
        }
        else
        {
            $_dates = explode(' - ', $dates);
            $start = $_dates[0];
            $end = $_dates[1];
        }

        $data = [];

        if($type == 'year')
        {
            for($i = $year_end; $i >= $year_start; $i--)
            {
                $this->db->select("company_type.id as company_type_id, garbage_type.id as garbage_type_id, company_type.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->join('company', 'company.id = company_sort_last.company_id');
                $this->db->join('company_type', 'company_type.id = company.company_type_id');
                $this->db->where("YEAR(company_sort_last.do_datetime) = '".$i."'");
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->where('company.status', 'Y');
                $this->db->group_by('company_type.id, garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ปี '.$i,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'quater')
        {
            for($i = 4; $i >= 1; $i--)
            {
                if($i == 1)
                {
                    $start = $year.'-01-01';
                    $end = $year.'-03-31';
                }
                else if($i == 2)
                {
                    $start = $year.'-04-01';
                    $end = $year.'-06-30';
                }
                else if($i == 3)
                {
                    $start = $year.'-07-01';
                    $end = $year.'-09-30';
                }
                else if($i == 4)
                {
                    $start = $year.'-10-01';
                    $end = $year.'-12-31';
                }

                $this->db->select("company_type.id as company_type_id, garbage_type.id as garbage_type_id, company_type.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->join('company', 'company.id = company_sort_last.company_id');
                $this->db->join('company_type', 'company_type.id = company.company_type_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->where('company.status', 'Y');
                $this->db->group_by('company_type.id, garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();
                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'ไตรมาส '.$i.' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'month')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            for($i = 12; $i >= 1; $i--)
            {
                $start = $year.'-'.(($i < 10) ? '0'.$i : $i).'-01';
                $end = date('Y-m-t', strtotime($start));

                $this->db->select("company_type.id as company_type_id, garbage_type.id as garbage_type_id, company_type.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
                $this->db->from('company_sort_last_detail');
                $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
                $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
                $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
                $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
                $this->db->join('company', 'company.id = company_sort_last.company_id');
                $this->db->join('company_type', 'company_type.id = company.company_type_id');
                $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
                $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
                $this->db->where('company_sort_last.status', 'Y');
                $this->db->where('company.status', 'Y');
                $this->db->group_by('company_type.id, garbage_type.id');
                $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
                $query = $this->db->get();

                $result = $query->result_array();

                if(count($result) > 0)
                {
                    $data[] = array(
                        'text' => 'เดือน '.$month[($i - 1)].' ปี '.$year,
                        'data' => $result
                    );
                }
            }
        }
        else if($type == 'range')
        {
            $month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];

            $this->db->select("company_type.id as company_type_id, garbage_type.id as garbage_type_id, company_type.name, garbage_type.name as type, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf", false);
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
            $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
            $this->db->join('company', 'company.id = company_sort_last.company_id');
            $this->db->join('company_type', 'company_type.id = company.company_type_id');
            $this->db->where("company_sort_last.do_datetime >= '".$start." 00:00:00'");
            $this->db->where("company_sort_last.do_datetime <= '".$end." 23:59:59'");
            $this->db->where('company_sort_last.status', 'Y');
            $this->db->where('company.status', 'Y');
            $this->db->group_by('company_type.id, garbage_type.id');
            $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
            $query = $this->db->get();
            $result = $query->result_array();

            if(count($result) > 0)
            {
                $data[] = array(
                    'text' => date('j', strtotime($start)).' '.$month[(date('n', strtotime($start)) - 1)].' '.date('Y', strtotime($start)).' - '.date('j', strtotime($end)).' '.$month[(date('n', strtotime($end)) - 1)].' '.date('Y', strtotime($end)),
                    'data' => $result
                );
            }
        }
        
        return $data;
    }
}