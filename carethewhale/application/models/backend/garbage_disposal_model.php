<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class garbage_disposal_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_garbage_type()
    {
        $this->db->select('*');
        $this->db->from('garbage_type');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('garbage_disposal');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(garbage_disposal.name LIKE '%".$fName."%' or garbage_disposal.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('garbage_disposal.status', $fStatus);
        }

        $this->db->where("garbage_disposal.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('garbage_disposal.*, garbage_type.name as garbage_type, garbage_type.color');
		$this->db->from('garbage_disposal');
        $this->db->join('garbage_type', 'garbage_type.id = garbage_disposal.garbage_type_id');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(garbage_disposal.name LIKE '%".$fName."%' or garbage_disposal.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('garbage_disposal.status', $fStatus);
        }

        $this->db->where("garbage_disposal.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('garbage_disposal.*, garbage_type.name as garbage_type, garbage_type.color');
		$this->db->from('garbage_disposal');
        $this->db->join('garbage_type', 'garbage_type.id = garbage_disposal.garbage_type_id');
        $this->db->where("garbage_disposal.status <> 'D'");
		$this->db->where('garbage_disposal.id', $id);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
        $data['garbage_type_id'] = $this->input->post('garbage_type_id');
		$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['status'] = $this->input->post('status');
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
        $this->db->insert('garbage_disposal', $data);
    }
	
	function update($id)
    {
    	$data = array();
        $data['garbage_type_id'] = $this->input->post('garbage_type_id');
    	$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['status'] = $this->input->post('status');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('garbage_disposal', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('garbage_disposal', $data);
    }
}