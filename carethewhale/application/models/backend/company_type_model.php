<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class company_type_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('company_type');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(company_type.name LIKE '%".$fName."%' or company_type.description LIKE '%".$fName."%' or company_type.remark LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('company_type.status', $fStatus);
        }

        $this->db->where("company_type.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company_type.*');
		$this->db->from('company_type');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company_type.name LIKE '%".$fName."%' or company_type.description LIKE '%".$fName."%' or company_type.remark LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('company_type.status', $fStatus);
        }

        $this->db->where("company_type.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('company_type.*');
		$this->db->from('company_type');
        $this->db->where("company_type.status <> 'D'");
		$this->db->where('company_type.id', $id);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
		$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['remark'] = $this->input->post('remark');
		$data['status'] = $this->input->post('status');
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
        $this->db->insert('company_type', $data);
    }
	
	function update($id)
    {
    	$data = array();
    	$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['remark'] = $this->input->post('remark');
		$data['status'] = $this->input->post('status');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('company_type', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('company_type', $data);
    }
}