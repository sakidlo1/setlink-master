<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function check_email_exists($input, $id)
    {
    	if(@$input['email'] == "")
    	{
    		return true;
    	}

		$this->db->select('count(*) as count_rec');
        $this->db->from('user');
        $this->db->where('email', $input['email']);
        $this->db->where("status <> 'D'");

        if($id > 0)
        {
        	$this->db->where("id <> ".$id);
        }

        $query = $this->db->get();
        $data = $query->row_array();

        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function login()
    {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $this->input->post('email'));
		$this->db->where('status', 'Y');
		$query = $this->db->get();
		$user = $query->row_array();

		if(@$user['id'] > 0 && $user['password'] == md5($this->input->post('password')))
		{
			$data['last_login'] = date('Y-m-d H:i:s');
			$this->db->where('id', $user['id']);
			$this->db->update('user', $data);

			return $user;
		}
		else
		{
			return false;
		}
    }

    function forget_password()
    {
    	$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $this->input->post('email'));
		$this->db->where('user.status', 'Y');
		$query = $this->db->get();
		$data = $query->row_array();

		if(@$data['id'] > 0)
		{
			$salt = md5($this->input->post('email').'-'.time());
			$data2['salt'] = $salt;
			$this->db->where('id', $data['id']);
			$this->db->update('user', $data2);

			$data['salt'] = $salt;
			return $data;
		}
		else
		{
			return false;
		}
    }

    function get_salt($email)
    {
    	$this->db->select('*');
		$this->db->from('user');
		$this->db->where('email', $email);
		$this->db->where('user.status', 'Y');
		$query = $this->db->get();
		$data = $query->row_array();

		if(@$data['id'] > 0)
		{
			return $data['salt'];
		}
		else
		{
			return false;
		}
    }

    function reset_password()
    {
		$data['password'] = md5($this->input->post('password'));
		$data['salt'] = '';
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
		$this->db->where('email', $this->input->get('email'));
		$this->db->where('user.status', 'Y');
		$this->db->update('user', $data);
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('user');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(user.name LIKE '%".$fName."%' or user.email LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('user.status', $fStatus);
        }

		$this->db->where("user.type <> 'S'");
        $this->db->where("user.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('user.*');
		$this->db->from('user');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(user.name LIKE '%".$fName."%' or user.email LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('user.status', $fStatus);
        }

		$this->db->where("user.type <> 'S'");
        $this->db->where("user.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('user.*');
		$this->db->from('user');
		$this->db->where("user.type <> 'S'");
        $this->db->where("user.status <> 'D'");
		$this->db->where('user.id', $id);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
    	$data['type'] = $this->input->post('type');
		$data['email'] = $this->input->post('email');
        $data['password'] = md5($this->input->post('password'));
        $data['avatar'] = '';
		$data['name'] = $this->input->post('name');
		$data['status'] = 'Y';
		$data['salt'] = '';
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
		$data['last_login'] = NULL;
        $this->db->insert('user', $data);
    }
	
	function update($id)
    {
    	$data = array();
    	$data['type'] = $this->input->post('type');
		$data['email'] = $this->input->post('email');

		if($this->input->post('password')!="")
		{
			$data['password'] = md5($this->input->post('password'));
		}

		$data['name'] = $this->input->post('name');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
		$this->db->where("type <> 'S'");
    	$this->db->where("status <> 'D'");
		$this->db->update('user', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
		$this->db->where("type <> 'S'");
    	$this->db->where("status <> 'D'");
		$this->db->update('user', $data);
    }
}