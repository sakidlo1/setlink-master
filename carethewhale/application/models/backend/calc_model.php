<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class calc_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_garbage_kind()
    {
        $this->db->select('*');
        $this->db->from('garbage_kind');
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_disposal()
    {
        $this->db->select('*');
        $this->db->from('garbage_disposal');
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_calc()
    {
        $this->db->select('*');
        $this->db->from('calc');
        $query = $this->db->get();
        return $query->result_array();
    }
	
	function update()
    {
        $this->db->where("garbage_kind_id > 0");
        $this->db->where("garbage_disposal_id > 0");
        $this->db->delete('calc');

        $garbage_kind = $this->get_garbage_kind();
        $garbage_disposal = $this->get_garbage_disposal();

        foreach($garbage_kind as $key => $value)
        {
            foreach($garbage_disposal as $key2 => $value2)
            {
                $data = array();
                $data['garbage_kind_id'] = $value['id'];
                $data['garbage_disposal_id'] = $value2['id'];
                $data['value1'] = (($this->input->post('value1_'.$value['id'].'_'.$value2['id']) > 0) ? $this->input->post('value1_'.$value['id'].'_'.$value2['id']) : 0);
                $data['value2'] = (($this->input->post('value2_'.$value['id'].'_'.$value2['id']) > 0) ? $this->input->post('value2_'.$value['id'].'_'.$value2['id']) : 0);
                $this->db->insert('calc', $data);
            }
        }
    }
}