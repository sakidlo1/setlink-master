<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class garbage_sort_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('garbage_sort');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(garbage_sort.name LIKE '%".$fName."%' or garbage_sort.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('garbage_sort.status', $fStatus);
        }

        $this->db->where("garbage_sort.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('garbage_sort.*');
		$this->db->from('garbage_sort');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(garbage_sort.name LIKE '%".$fName."%' or garbage_sort.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('garbage_sort.status', $fStatus);
        }

        $this->db->where("garbage_sort.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('garbage_sort.*');
		$this->db->from('garbage_sort');
        $this->db->where("garbage_sort.status <> 'D'");
		$this->db->where('garbage_sort.id', $id);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
		$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['image'] = '';
		$data['status'] = $this->input->post('status');
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
        $this->db->insert('garbage_sort', $data);

        $_id = $this->db->insert_id();

        if(count($this->input->post('file_image')) > 0)
        {
            $file_image = [];

            foreach ($this->input->post('file_image') as $key => $value)
            {
                $file_data = $this->input->post('file_image')[$key];
                $file_path = 'garbage_sort/'.($_id % 4000).'/'.$_id.'/';

                if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                {
                    $file_image[] = $file_data;
                }
                else if($file_data != '')
                {
                    list($type, $file) = explode(';', $file_data);
                    if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                        $file = substr($file, strpos($file, ',') + 1);
                        $type = strtolower($type[1]);

                        if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                        {
                            // not allow
                        }

                        $file = base64_decode($file);

                        if ($file === false) {
                            // decode error
                        }

                        $_path = explode('/', $file_path);
                        $_check_path = './images/upload';
                        for($i = 0; $i < (count($_path) - 1); $i++)
                        {
                            $_check_path .= '/'.$_path[$i];
                            if(!is_dir($_check_path))
                            {
                                mkdir($_check_path,0777);
                            }
                        }

                        $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                        $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                        if(file_put_contents($fullname, $file)){
                            $file_image[] = $fullurl;
                        }else{
                            // error
                        }
                    } else {
                        // file not match type
                    }
                }
            }

            if(count($file_image) > 0)
            {
                foreach ($file_image as $key => $value)
                {
                    $data = array();
                    $data['image'] = $value;
                    $this->db->where('id', $_id);
                    $this->db->update('garbage_sort', $data);
                }
            }
        }
    }
	
	function update($id)
    {
    	$data = array();
    	$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['status'] = $this->input->post('status');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('garbage_sort', $data);

        $_id = $id;

        if(count($this->input->post('file_image')) > 0)
        {
            $file_image = [];

            foreach ($this->input->post('file_image') as $key => $value)
            {
                $file_data = $this->input->post('file_image')[$key];
                $file_path = 'garbage_sort/'.($_id % 4000).'/'.$_id.'/';

                if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                {
                    $file_image[] = $file_data;
                }
                else if($file_data != '')
                {
                    list($type, $file) = explode(';', $file_data);
                    if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                        $file = substr($file, strpos($file, ',') + 1);
                        $type = strtolower($type[1]);

                        if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                        {
                            // not allow
                        }

                        $file = base64_decode($file);

                        if ($file === false) {
                            // decode error
                        }

                        $_path = explode('/', $file_path);
                        $_check_path = './images/upload';
                        for($i = 0; $i < (count($_path) - 1); $i++)
                        {
                            $_check_path .= '/'.$_path[$i];
                            if(!is_dir($_check_path))
                            {
                                mkdir($_check_path,0777);
                            }
                        }

                        $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                        $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                        if(file_put_contents($fullname, $file)){
                            $file_image[] = $fullurl;
                        }else{
                            // error
                        }
                    } else {
                        // file not match type
                    }
                }
            }

            if(count($file_image) > 0)
            {
                foreach ($file_image as $key => $value)
                {
                    $data = array();
                    $data['image'] = $value;
                    $this->db->where('id', $_id);
                    $this->db->update('garbage_sort', $data);
                }
            }
        }
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('garbage_sort', $data);
    }
}