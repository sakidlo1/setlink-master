<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class company_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function check_email_exists($input, $company_id)
    {
        if(@$input['email'] == "")
        {
            return true;
        }

        $this->db->select('count(*) as count_rec');
        $this->db->from('company_user');
        $this->db->where('email', $input['email']);
        $this->db->where("company_id <> ".$company_id);
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        $data = $query->row_array();

        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function get_company_type()
    {
        $this->db->select('*');
        $this->db->from('company_type');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_zone()
    {
        $this->db->select('*');
        $this->db->from('zone');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('company');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(company.name LIKE '%".$fName."%' or company.address LIKE '%".$fName."%' or company.tel1 LIKE '%".$fName."%' or company.tel2 LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('company.status', $fStatus);
        }

        $this->db->where("company.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company.*, company_type.name as company_type, zone.name as zone');
		$this->db->from('company');
        $this->db->join('company_type', 'company_type.id = company.company_type_id');
        $this->db->join('zone', 'zone.id = company.zone_id');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company.name LIKE '%".$fName."%' or company.address LIKE '%".$fName."%' or company.tel1 LIKE '%".$fName."%' or company.tel2 LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('company.status', $fStatus);
        }

        $this->db->where("company.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id, $year = 0)
    {
        $this->db->select('company.*, company_type.name as company_type, zone.name as zone');
        $this->db->from('company');
        $this->db->join('company_type', 'company_type.id = company.company_type_id');
        $this->db->join('zone', 'zone.id = company.zone_id');
        $this->db->where("company.status <> 'D'");
		$this->db->where('company.id', $id);
		$query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            $this->db->select('*');
            $this->db->from('company_user');
            $this->db->where('company_id', $data['id']);
            $this->db->where('type', 'M');
            $query = $this->db->get();
            $data['user'] = $query->row_array();

            if($year > 0)
            {  
                $data['year'] = $year;
            }
            else
            {
                $this->db->select('max(year) as year');
                $this->db->from('company_garbage_type');
                $this->db->where('company_id', $data['id']);
                $query = $this->db->get();
                $year = $query->row_array();
                $data['year'] = $year['year'];
            }

            $this->db->select('max(year) as year');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where("year < ".$data['year']);
            $query = $this->db->get();
            $prev_year = $query->row_array();
            if($prev_year != '')
            {
                $data['prev_year'] = $prev_year['year'];
            }
            else
            {
                $data['prev_year'] = '';
            }

            $this->db->select('min(year) as year');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where("year > ".$data['year']);
            $query = $this->db->get();
            $next_year = $query->row_array();
            if($prev_year != '')
            {
                $data['next_year'] = $next_year['year'];
            }
            else
            {
                $data['next_year'] = '';
            }

            $this->db->select('*');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['garbage_type'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_manage');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['manage'] = $query->row_array();

            $this->db->select('*');
            $this->db->from('company_policy_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['policy_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_person_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['person_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_model_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['model_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_activity_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['activity_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_public_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['public_image'] = $query->result_array();
        }
        
        return $data;
    }
	
	function update($id)
    {
        $company = $this->get_by_id($id);

    	$data = array();
        $data['company_type_id'] = $this->input->post('company_type_id');
        $data['zone_id'] = $this->input->post('zone_id');
        $data['open_mon'] = (($this->input->post('open_mon') == 'Y') ? 'Y' : 'N');
        $data['open_tue'] = (($this->input->post('open_tue') == 'Y') ? 'Y' : 'N');
        $data['open_wed'] = (($this->input->post('open_wed') == 'Y') ? 'Y' : 'N');
        $data['open_thu'] = (($this->input->post('open_thu') == 'Y') ? 'Y' : 'N');
        $data['open_fri'] = (($this->input->post('open_fri') == 'Y') ? 'Y' : 'N');
        $data['open_sat'] = (($this->input->post('open_sat') == 'Y') ? 'Y' : 'N');
        $data['open_sun'] = (($this->input->post('open_sun') == 'Y') ? 'Y' : 'N');
        $data['open_start'] = $this->input->post('open_start').':00';
        $data['open_end'] = $this->input->post('open_end').':00';
        $data['guest_qty'] = $this->input->post('guest_qty');
        $data['staff_qty'] = $this->input->post('staff_qty');
        $data['size'] = $this->input->post('size');
        $data['address'] = $this->input->post('address');
        $data['tel1'] = $this->input->post('tel1');
        $data['tel2'] = $this->input->post('tel2');
        $data['status'] = (($this->input->post('status') == 'Y') ? 'Y' : 'N');
		$data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_type'] = 'H';
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('company', $data);

        $this->db->select('*');
        $this->db->from('company_user');
        $this->db->where('email', $this->input->post('email'));
        $this->db->where('company_id', $id);
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        $check = $query->row_array();

        if(@$check['id'] > 0)
        {
            if($check['type'] == 'M')
            {
                $data = array();
                $data['name'] = $this->input->post('contact_name');
                $data['updated_on'] = date('Y-m-d H:i:s');
                $data['updated_type'] = 'H';
                $data['updated_by'] = $this->authen->id;
                $this->db->where('id', $check['id']);
                $this->db->where('company_id', $id);
                $this->db->where("status <> 'D'");
                $this->db->update('company_user', $data);
            }
            else
            {
                $data = array();
                $data['type'] = 'S';
                $data['updated_on'] = date('Y-m-d H:i:s');
                $data['updated_type'] = 'H';
                $data['updated_by'] = $this->authen->id;
                $this->db->where('type', 'M');
                $this->db->where('company_id', $id);
                $this->db->where("status <> 'D'");
                $this->db->update('company_user', $data);

                $data = array();
                $data['type'] = 'M';
                $data['name'] = $this->input->post('contact_name');
                $data['updated_on'] = date('Y-m-d H:i:s');
                $data['updated_type'] = 'H';
                $data['updated_by'] = $this->authen->id;
                $this->db->where('id', $check['id']);
                $this->db->where('company_id', $id);
                $this->db->where('type', 'S');
                $this->db->where("status <> 'D'");
                $this->db->update('company_user', $data);
            }
        }
        else
        {
            $data = array();
            $data['type'] = 'S';
            $data['updated_on'] = date('Y-m-d H:i:s');
            $data['updated_type'] = 'H';
            $data['updated_by'] = $this->authen->id;
            $this->db->where('type', 'M');
            $this->db->where('company_id', $id);
            $this->db->where("status <> 'D'");
            $this->db->update('company_user', $data);

            $data = array();
            $data['company_id'] = $id;
            $data['type'] = 'M';
            $data['email'] = $this->input->post('email');
            $data['password'] = 'Waiting for Forget Password.';
            $data['avatar'] = $company['logo'];
            $data['name'] = $this->input->post('contact_name');
            $data['status'] = 'Y';
            $data['salt'] = '';
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = 0;
            $data['updated_on'] = date('Y-m-d H:i:s');
            $data['updated_type'] = 'H';
            $data['updated_by'] = 0;
            $this->db->insert('company_user', $data);
        }
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_type'] = 'H';
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('company', $data);
    }
}