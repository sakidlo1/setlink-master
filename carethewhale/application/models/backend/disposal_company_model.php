<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class disposal_company_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_garbage_disposal()
    {
        $this->db->select('*');
        $this->db->from('garbage_disposal');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('disposal_company');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(disposal_company.name LIKE '%".$fName."%' or disposal_company.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
        	$this->db->where('disposal_company.status', $fStatus);
        }

        $this->db->where("disposal_company.status <> 'D'");

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('disposal_company.*');
		$this->db->from('disposal_company');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(disposal_company.name LIKE '%".$fName."%' or disposal_company.description LIKE '%".$fName."%')");
        }

        $fStatus = @$this->input->post('columns')[1]['search']['value'];
        if($fStatus != "")
        {
            $this->db->where('disposal_company.status', $fStatus);
        }

        $this->db->where("disposal_company.status <> 'D'");

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('disposal_company.*');
		$this->db->from('disposal_company');
        $this->db->where("disposal_company.status <> 'D'");
		$this->db->where('disposal_company.id', $id);
		$query = $this->db->get();
        $data = $query->row_array();
    
        $this->db->select('*');
        $this->db->from('disposal_company_detail');
        $this->db->where('disposal_company_id', $id);
        $query = $this->db->get();
        $data['detail'] = $query->result_array();

        return $data;
    }

    function insert()
    {
    	$data = array();
		$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['status'] = $this->input->post('status');
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
        $this->db->insert('disposal_company', $data);

        $_id = $this->db->insert_id();

        foreach($this->input->post('garbage_disposal_id') as $key => $value)
        {
            $data = array();
            $data['disposal_company_id'] = $_id;
            $data['garbage_disposal_id'] = $value;
            $this->db->insert('disposal_company_detail', $data);
        }
    }
	
	function update($id)
    {
    	$data = array();
    	$data['name'] = $this->input->post('name');
		$data['description'] = $this->input->post('description');
		$data['status'] = $this->input->post('status');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('disposal_company', $data);

        $_id = $id;

        $this->db->where('disposal_company_id', $_id);
        $this->db->delete('disposal_company_detail');

        foreach($this->input->post('garbage_disposal_id') as $key => $value)
        {
            $data = array();
            $data['disposal_company_id'] = $_id;
            $data['garbage_disposal_id'] = $value;
            $this->db->insert('disposal_company_detail', $data);
        }
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
		$this->db->update('disposal_company', $data);
    }
}