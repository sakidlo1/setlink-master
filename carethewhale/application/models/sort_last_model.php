<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sort_last_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_company_sort_point()
    {
        $this->db->select('*');
        $this->db->from('company_sort_point');
        $this->db->where('status', 'Y');
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_company_user()
    {
        $this->db->select('*');
        $this->db->from('company_user');
        $this->db->where('status', 'Y');
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_category()
    {
        $this->db->select('*');
        $this->db->from('garbage_category');
        $this->db->where('status', 'Y');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_kind($garbage_category_id = 0)
    {
        $this->db->select('*, (select image from garbage_kind_image where garbage_kind_id = garbage_kind.id order by order_on asc limit 1) as image');
        $this->db->from('garbage_kind');
        $this->db->where('status', 'Y');

        if($garbage_category_id > 0)
        {
            $this->db->where('garbage_category_id', $garbage_category_id);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_disposal()
    {
        $this->db->select('garbage_disposal.*, garbage_type.color');
        $this->db->from('garbage_disposal');
        $this->db->join('garbage_type', 'garbage_type.id = garbage_disposal.garbage_type_id');
        $this->db->where('garbage_disposal.status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_disposal_company($garbage_disposal_id = 0)
    {
        $this->db->select('disposal_company.*, disposal_company_detail.garbage_disposal_id');
        $this->db->from('disposal_company');
        $this->db->join('disposal_company_detail', 'disposal_company_detail.disposal_company_id = disposal_company.id');
        $this->db->where('disposal_company.status', 'Y');

        if($garbage_disposal_id > 0)
        {
            $this->db->where('disposal_company_detail.garbage_disposal_id', $garbage_disposal_id);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('company_sort_last');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_sort_last.company_sort_point_id');
        $this->db->join('company_user', 'company_user.id = company_sort_last.company_user_id');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(company_sort_point.name LIKE '%".$fName."%' or company_user.name LIKE '%".$fName."%')");
        }

        $this->db->where("company_sort_last.status <> 'D'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company_sort_last.*, company_sort_point.name as company_sort_point, company_user.name as company_user, updated.name as update_name');
		$this->db->from('company_sort_last');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_sort_last.company_sort_point_id');
        $this->db->join('company_user', 'company_user.id = company_sort_last.company_user_id');
        $this->db->join('company_user as updated', 'updated.id = company_sort_last.updated_by');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company_sort_point.name LIKE '%".$fName."%' or company_user.name LIKE '%".$fName."%')");
        }

        $this->db->where("company_sort_last.status <> 'D'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('company_sort_last.*, company_sort_point.name as company_sort_point, company_sort_point.image as company_sort_point_image, company_user.name as company_user, updated.name as update_name');
        $this->db->from('company_sort_last');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_sort_last.company_sort_point_id');
        $this->db->join('company_user', 'company_user.id = company_sort_last.company_user_id');
        $this->db->join('company_user as updated', 'updated.id = company_sort_last.updated_by');
        $this->db->where("company_sort_last.status <> 'D'");
		$this->db->where('company_sort_last.id', $id);
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
        $data = $query->row_array();
        
        if(@$data['id'] > 0)
        {
            $this->db->select('company_sort_last_detail.*, garbage_kind.name as garbage_kind, (select image from garbage_kind_image where garbage_kind_id = garbage_kind.id order by order_on asc limit 1) as garbage_kind_image, garbage_category.name as garbage_category, garbage_kind.garbage_category_id, garbage_disposal.name as garbage_disposal, garbage_type.color, disposal_company.name as disposal_company');
            $this->db->from('company_sort_last_detail');
            $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
            $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
            $this->db->join('garbage_disposal', 'garbage_disposal.id = company_sort_last_detail.garbage_disposal_id');
            $this->db->join('garbage_type', 'garbage_type.id = garbage_disposal.garbage_type_id');
            $this->db->join('disposal_company', 'disposal_company.id = company_sort_last_detail.disposal_company_id');
            $this->db->where('company_sort_last_detail.company_sort_last_id', $data['id']);
            $this->db->order_by('company_sort_last_detail.order_on', 'asc');
            $query = $this->db->get();
            $data['detail'] = $query->result_array();
        }

        return $data;
    }

    function get_baseline($company_sort_point_id, $garbage_kind_id, $garbage_disposal_id)
    {
        $this->db->select('*');
        $this->db->from('company_baseline');
        $this->db->where("status <> 'D'");
        $this->db->where('company_sort_point_id', $company_sort_point_id);
        $this->db->where('garbage_kind_id', $garbage_kind_id);
        $this->db->where('garbage_disposal_id', $garbage_disposal_id);
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['baseline'] > 0)
        {
            return $data['baseline'];
        }
        else
        {
            $this->db->select('*');
            $this->db->from('company_baseline');
            $this->db->where("status <> 'D'");
            $this->db->where('company_sort_point_id', 0);
            $this->db->where('garbage_kind_id', $garbage_kind_id);
            $this->db->where('garbage_disposal_id', $garbage_disposal_id);
            $this->db->where('company_id', $this->authen->user_data['company_id']);
            $query = $this->db->get();
            $data = $query->row_array();

            if(@$data['baseline'] > 0)
            {
                return $data['baseline'];
            }
            else
            {
                return '0';
            }
        }
    }

    function get_calc($garbage_kind_id, $garbage_disposal_id)
    {
        $this->db->select('*');
        $this->db->from('calc');
        $this->db->where('garbage_kind_id', $garbage_kind_id);
        $this->db->where('garbage_disposal_id', $garbage_disposal_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
        $data['company_id'] = $this->authen->user_data['company_id'];
		$data['company_sort_point_id'] = $this->input->post('company_sort_point_id');
        $data['total_weight'] = 0;
        $data['total_cf'] = 0;
        $data['company_user_id'] = $this->input->post('company_user_id');
        $data['do_datetime'] = $this->input->post('do_datetime');
		$data['status'] = 'Y';
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
        $this->db->insert('company_sort_last', $data);

        $_id = $this->db->insert_id();

        $total_weight = 0;
        $total_cf = 0;
        foreach($this->input->post('garbage_kind_id') as $key => $value)
        {
            $data = array();
            $data['company_sort_last_id'] = $_id;
            $data['garbage_kind_id'] = $value;
            $data['garbage_disposal_id'] = $this->input->post('garbage_disposal_id')[$key];
            $data['disposal_company_id'] = $this->input->post('disposal_company_id')[$key];
            $data['total'] = $this->input->post('total')[$key];
            $data['baseline'] = $this->get_baseline($this->input->post('company_sort_point_id'), $data['garbage_kind_id'], $data['garbage_disposal_id']);

            $calc = $this->get_calc($data['garbage_kind_id'], $data['garbage_disposal_id']);

            $data['value1'] = ((@$calc['value1'] > 0) ? $calc['value1'] : 0);
            $data['value2'] = ((@$calc['value2'] > 0) ? $calc['value2'] : 0);

            if(($data['baseline'] - $data['total']) > 0)
            {
                $data['total_reduce'] = ($data['baseline'] - $data['total']);

                if($data['value1'] > 0 && $data['value2'] == 0)
                {
                    $data['total_cf'] = ($data['total_reduce'] * $data['value1']);
                }
                else if($data['value1'] > 0 && $data['value2'] > 0)
                {
                    $data['total_cf'] = (($data['total_reduce'] * $data['value1']) + ($data['total'] * $data['value2']));
                }
                else
                {
                    $data['total_cf'] = 0;
                }
            }
            else
            {
                $data['total_reduce'] = 0;
                $data['total_cf'] = ($data['total'] * $data['value2']);
            }

            $data['order_on'] = ($key + 1);
            $this->db->insert('company_sort_last_detail', $data);

            $total_weight = $total_weight + $data['total'];
            $total_cf = $total_cf + $data['total_cf'];
        }

        $data = array();
        $data['total_weight'] = $total_weight;
        $data['total_cf'] = $total_cf;
        $this->db->where('id', $_id);
        $this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $this->db->update('company_sort_last', $data);
    }
	
	function update($id)
    {
    	$data = array();
        $data['company_sort_point_id'] = $this->input->post('company_sort_point_id');
        $data['total_weight'] = 0;
        $data['total_cf'] = 0;
        $data['company_user_id'] = $this->input->post('company_user_id');
        $data['do_datetime'] = $this->input->post('do_datetime');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_sort_last', $data);

        $_id = $id;

        $this->db->where('company_sort_last_id', $_id);
        $this->db->delete('company_sort_last_detail');

        $total_weight = 0;
        $total_cf = 0;
        foreach($this->input->post('garbage_kind_id') as $key => $value)
        {
            $data = array();
            $data['company_sort_last_id'] = $_id;
            $data['garbage_kind_id'] = $value;
            $data['garbage_disposal_id'] = $this->input->post('garbage_disposal_id')[$key];
            $data['disposal_company_id'] = $this->input->post('disposal_company_id')[$key];
            $data['total'] = $this->input->post('total')[$key];
            $data['baseline'] = $this->get_baseline($this->input->post('company_sort_point_id'), $data['garbage_kind_id'], $data['garbage_disposal_id']);

            $calc = $this->get_calc($data['garbage_kind_id'], $data['garbage_disposal_id']);

            $data['value1'] = ((@$calc['value1'] > 0) ? $calc['value1'] : 0);
            $data['value2'] = ((@$calc['value2'] > 0) ? $calc['value2'] : 0);

            if(($data['baseline'] - $data['total']) > 0)
            {
                $data['total_reduce'] = ($data['baseline'] - $data['total']);

                if($data['value1'] > 0 && $data['value2'] == 0)
                {
                    $data['total_cf'] = ($data['total_reduce'] * $data['value1']);
                }
                else if($data['value1'] > 0 && $data['value2'] > 0)
                {
                    $data['total_cf'] = (($data['total_reduce'] * $data['value1']) + ($data['total'] * $data['value2']));
                }
                else
                {
                    $data['total_cf'] = 0;
                }
            }
            else
            {
                $data['total_reduce'] = 0;
                $data['total_cf'] = ($data['total'] * $data['value2']);
            }

            $data['order_on'] = ($key + 1);
            $this->db->insert('company_sort_last_detail', $data);

            $total_weight = $total_weight + $data['total'];
            $total_cf = $total_cf + $data['total_cf'];
        }

        $data = array();
        $data['total_weight'] = $total_weight;
        $data['total_cf'] = $total_cf;
        $this->db->where('id', $_id);
        $this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $this->db->update('company_sort_last', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_sort_last', $data);
    }
}