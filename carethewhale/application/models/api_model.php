<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class api_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_summary_cf()
    {
        $this->db->select("sum(total_weight) as total_weight, sum(total_cf) as total_cf, (select count(*) from company where status = 'Y') as total_company", false);
        $this->db->from('company_sort_last');
        $this->db->join('company', 'company.id = company_sort_last.company_id');
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();
		
		return array('total_company' => number_format($data['total_company'], 0), 'total_weight' => number_format($data['total_weight'], 2), 'total_cf' => number_format($data['total_cf'], 2), 'total_tree' => number_format(($data['total_cf'] / 9), 2));
    }
	
	function get_company()
	{
		$this->db->select('logo, name');
        $this->db->from('company');
        $this->db->where('status', 'Y');
        $this->db->order_by('id', 'asc');
        $query = $this->db->get();
        return $query->result_array();
	}
}