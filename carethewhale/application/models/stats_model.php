<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stats_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function get_total_cf()
    {
        $this->db->select("sum(total_cf) as total_cf, (select count(*) from company where status = 'Y') as total_company", false);
        $this->db->from('company_sort_last');
        $this->db->join('company', 'company.id = company_sort_last.company_id');
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_stats()
    {
        $this->db->select("*", false);
        $this->db->from('stats');
        $this->db->where('id', 1);
        $query = $this->db->get();
        return $query->row_array();
    }
}