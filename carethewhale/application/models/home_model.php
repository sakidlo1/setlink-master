<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function get_total_cf($start = '', $end = '')
    {
    	$this->db->select('sum(total_cf) as total_cf');
        $this->db->from('company_sort_last');
        $this->db->where("do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        return $data['total_cf'];
    }

    function get_total_cf_by_garbage_type($start = '', $end = '')
    {
    	$this->db->select('garbage_type.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
        $this->db->join('garbage_type', 'garbage_type.id = garbage_category.garbage_type_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_type.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_total_cf_by_garbage_plastic($start = '', $end = '')
    {
    	$this->db->select('garbage_kind.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where('garbage_kind.garbage_category_id', 5);
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_kind.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_total_cf_by_garbage_recycle($start = '', $end = '')
    {
    	$this->db->select('garbage_category.name, sum(company_sort_last_detail.total) as total_weight, sum(company_sort_last_detail.total_cf) as total_cf');
        $this->db->from('company_sort_last_detail');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_sort_last_detail.garbage_kind_id');
        $this->db->join('garbage_category', 'garbage_category.id = garbage_kind.garbage_category_id');
        $this->db->join('company_sort_last', 'company_sort_last.id = company_sort_last_detail.company_sort_last_id');
        $this->db->where('garbage_category.garbage_type_id', 2);
        $this->db->where("company_sort_last.do_datetime >= '".(($start != '') ? $start : date('Y-m').'-01')." 00:00:00'");
        $this->db->where("company_sort_last.do_datetime <= '".(($end != '') ? $end : date('Y-m-t'))." 23:59:59'");
        $this->db->where('company_sort_last.company_id', $this->authen->user_data['company_id']);
        $this->db->where('company_sort_last.status', 'Y');
        $this->db->group_by('garbage_category.id');
        $this->db->order_by('sum(company_sort_last_detail.total_cf) desc, sum(company_sort_last_detail.total) desc');
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result_array();
    }
}