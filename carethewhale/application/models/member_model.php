<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class member_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_company_type()
    {
        $this->db->select('*');
        $this->db->from('company_type');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_zone()
    {
        $this->db->select('*');
        $this->db->from('zone');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_type()
    {
        $this->db->select('*');
        $this->db->from('garbage_type');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function check_email_exists($input)
    {
        if(@$input['email'] == "")
        {
            return true;
        }

        $this->db->select('count(*) as count_rec');
        $this->db->from('company_user');
        $this->db->where('email', $input['email']);
        $this->db->where("status <> 'D'");
        $query = $this->db->get();
        $data = $query->row_array();

        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function login()
    {
        $this->db->select('company_user.*, company.is_accept, company.logo, company.name as company, company.address, company.tel1, company.tel2');
        $this->db->from('company_user');
        $this->db->join('company', 'company.id = company_user.company_id');
        $this->db->where('company_user.email', $this->input->post('email'));
        $this->db->where('company_user.password', md5($this->input->post('password')));
        $this->db->where('company_user.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $user = $query->row_array();

        if(@$user['id'] > 0 && $user['password'] == md5($this->input->post('password')))
        {
            $data['last_login'] = date('Y-m-d H:i:s');
            $this->db->where('id', $user['id']);
            $this->db->update('company_user', $data);
			
			$this->db->select('max(year) as year');
			$this->db->from('company_garbage_type');
			$this->db->where('company_id', $user['company_id']);
			$query = $this->db->get();
			$year = $query->row_array();
			$user['year_data'] = $year['year'];

            return $user;
        }
        else
        {
            return false;
        }
    }
	
	function auto_login_by_email($email)
    {
		$this->db->select('company_user.*, company.is_accept, company.logo, company.name as company, company.address, company.tel1, company.tel2');
        $this->db->from('company_user');
        $this->db->join('company', 'company.id = company_user.company_id');
        $this->db->where('company_user.email', $email);
        $this->db->where('company_user.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $user = $query->row_array();
		
        if(@$user['id'] > 0)
        {
            $data['last_login'] = date('Y-m-d H:i:s');
            $this->db->where('id', $user['id']);
            $this->db->update('company_user', $data);
			
			$this->db->select('max(year) as year');
			$this->db->from('company_garbage_type');
			$this->db->where('company_id', $user['company_id']);
			$query = $this->db->get();
			$year = $query->row_array();
			$user['year_data'] = $year['year'];

            return $user;
        }
        else
        {
            return false;
        }
    }

    function register()
    {
        if($this->check_email_exists(array('email' => $this->input->post('email'))))
        {
            return false;
        }
        else
        {
            $year = date('Y');
            $now = date('Y-m-d H:i:s');

            $data = array();
            $data['company_type_id'] = $this->input->post('company_type_id');
            $data['zone_id'] = $this->input->post('zone_id');
            $data['logo'] = '';
            $data['name'] = $this->input->post('name');
            $data['open_mon'] = (($this->input->post('open_mon') == 'Y') ? 'Y' : 'N');
            $data['open_tue'] = (($this->input->post('open_tue') == 'Y') ? 'Y' : 'N');
            $data['open_wed'] = (($this->input->post('open_wed') == 'Y') ? 'Y' : 'N');
            $data['open_thu'] = (($this->input->post('open_thu') == 'Y') ? 'Y' : 'N');
            $data['open_fri'] = (($this->input->post('open_fri') == 'Y') ? 'Y' : 'N');
            $data['open_sat'] = (($this->input->post('open_sat') == 'Y') ? 'Y' : 'N');
            $data['open_sun'] = (($this->input->post('open_sun') == 'Y') ? 'Y' : 'N');
            $data['open_start'] = $this->input->post('open_start').':00';
            $data['open_end'] = $this->input->post('open_end').':00';
            $data['guest_qty'] = $this->input->post('guest_qty');
            $data['staff_qty'] = $this->input->post('staff_qty');
            $data['size'] = $this->input->post('size');
            $data['address'] = $this->input->post('address');
            $data['tel1'] = $this->input->post('tel1');
            $data['tel2'] = $this->input->post('tel2');
            $data['is_accept'] = (($this->input->post('is_accept') == 'Y') ? 'Y' : 'N');
            $data['status'] = 'N';
            $data['created_on'] = $now;
            $data['updated_on'] = $now;
            $data['updated_type'] = 'C';
            $data['updated_by'] = 0;
            $this->db->insert('company', $data);

            $_id = $this->db->insert_id();

            $data = array();
            $data['company_id'] = $_id;
            $data['year'] = $year;
            $data['policy_name'] = $this->input->post('policy_name');
            $data['policy_description'] = $this->input->post('policy_description');
            $data['person_name'] = $this->input->post('person_name');
            $data['person_description'] = $this->input->post('person_description');
            $data['model_name'] = $this->input->post('model_name');
            $data['model_description'] = $this->input->post('model_description');
            $data['activity_name'] = $this->input->post('activity_name');
            $data['activity_description'] = $this->input->post('activity_description');
            $data['public_name'] = $this->input->post('public_name');
            $data['public_description'] = $this->input->post('public_description');
            $data['created_on'] = $now;
            $this->db->insert('company_manage', $data);

            $file_logo = '';

            if(count($this->input->post('file_logo')) > 0)
            {
                foreach ($this->input->post('file_logo') as $key => $value)
                {
                    $file_data = $this->input->post('file_logo')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/';

                    if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_logo = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if($file_logo != '')
                {
                    $data = array();
                    $data['logo'] = $file_logo;
                    $this->db->where('id', $_id);
                    $this->db->update('company', $data);
                }
            }

            $data = array();
            $data['company_id'] = $_id;
            $data['type'] = 'M';
            $data['email'] = $this->input->post('email');
            $data['password'] = md5($this->input->post('password'));
            $data['avatar'] = $file_logo;
            $data['name'] = $this->input->post('contact_name');
            $data['status'] = 'Y';
            $data['salt'] = '';
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = 0;
            $data['updated_on'] = date('Y-m-d H:i:s');
            $data['updated_type'] = 'C';
            $data['updated_by'] = 0;
            $this->db->insert('company_user', $data);

            /*
            foreach($this->input->post('garbage_type_id') as $key => $value)
            {
                $data = array();
                $data['company_id'] = $_id;
                $data['year'] = $year;
                $data['garbage_type_id'] = $value;
                $data['weight'] = $this->input->post('garbage_type_weight')[$key];
                $this->db->insert('company_garbage_type', $data);
            }
            */
            
            $data = array();
            $data['company_id'] = $_id;
            $data['year'] = $year;
            $data['description'] = $this->input->post('garbage_type_description');
            $this->db->insert('company_garbage_type', $data);

            if(count($this->input->post('file_policy')) > 0)
            {
                $file_policy = [];

                foreach ($this->input->post('file_policy') as $key => $value)
                {
                    $file_data = $this->input->post('file_policy')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/policy/';

                    if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                    {
                        $file_policy[] = $file_data;
                    }
                    else if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_policy[] = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if(count($file_policy) > 0)
                {
                    foreach ($file_policy as $key => $value)
                    {
                        $data = array();
                        $data['company_id'] = $_id;
                        $data['year'] = $year;
                        $data['image'] = $value;
                        $data['order_on'] = ($key + 1);
                        $this->db->insert('company_policy_image', $data);
                    }
                }
            }

            if(count($this->input->post('file_person')) > 0)
            {
                $file_person = [];

                foreach ($this->input->post('file_person') as $key => $value)
                {
                    $file_data = $this->input->post('file_person')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/person/';

                    if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                    {
                        $file_person[] = $file_data;
                    }
                    else if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_person[] = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if(count($file_person) > 0)
                {
                    foreach ($file_person as $key => $value)
                    {
                        $data = array();
                        $data['company_id'] = $_id;
                        $data['year'] = $year;
                        $data['image'] = $value;
                        $data['order_on'] = ($key + 1);
                        $this->db->insert('company_person_image', $data);
                    }
                }
            }

            if(count($this->input->post('file_model')) > 0)
            {
                $file_model = [];

                foreach ($this->input->post('file_model') as $key => $value)
                {
                    $file_data = $this->input->post('file_model')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/model/';

                    if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                    {
                        $file_model[] = $file_data;
                    }
                    else if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_model[] = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if(count($file_model) > 0)
                {
                    foreach ($file_model as $key => $value)
                    {
                        $data = array();
                        $data['company_id'] = $_id;
                        $data['year'] = $year;
                        $data['image'] = $value;
                        $data['order_on'] = ($key + 1);
                        $this->db->insert('company_model_image', $data);
                    }
                }
            }

            if(count($this->input->post('file_activity')) > 0)
            {
                $file_activity = [];

                foreach ($this->input->post('file_activity') as $key => $value)
                {
                    $file_data = $this->input->post('file_activity')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/activity/';

                    if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                    {
                        $file_activity[] = $file_data;
                    }
                    else if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_activity[] = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if(count($file_activity) > 0)
                {
                    foreach ($file_activity as $key => $value)
                    {
                        $data = array();
                        $data['company_id'] = $_id;
                        $data['year'] = $year;
                        $data['image'] = $value;
                        $data['order_on'] = ($key + 1);
                        $this->db->insert('company_activity_image', $data);
                    }
                }
            }

            if(count($this->input->post('file_public')) > 0)
            {
                $file_public = [];

                foreach ($this->input->post('file_public') as $key => $value)
                {
                    $file_data = $this->input->post('file_public')[$key];
                    $file_path = 'company/'.($_id % 4000).'/'.$_id.'/public/';

                    if(substr($file_data, 0, 4) == 'http' && $file_data != '')
                    {
                        $file_public[] = $file_data;
                    }
                    else if($file_data != '')
                    {
                        list($type, $file) = explode(';', $file_data);
                        if (preg_match('/^data:image\/(\w+);base64,/', $file_data, $type)) {
                            $file = substr($file, strpos($file, ',') + 1);
                            $type = strtolower($type[1]);

                            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ]))
                            {
                                // not allow
                            }

                            $file = base64_decode($file);

                            if ($file === false) {
                                // decode error
                            }

                            $_path = explode('/', $file_path);
                            $_check_path = './images/upload';
                            for($i = 0; $i < (count($_path) - 1); $i++)
                            {
                                $_check_path .= '/'.$_path[$i];
                                if(!is_dir($_check_path))
                                {
                                    mkdir($_check_path,0777);
                                }
                            }

                            $fullname = './images/upload/'.$file_path.time().'-'.($key + 1).'.'.(($type == 'jpeg') ? 'jpg' : $type);
                            $fullurl = config_item('image_url').str_replace('./images/', '', $fullname);
                            if(file_put_contents($fullname, $file)){
                                $file_public[] = $fullurl;
                            }else{
                                // error
                            }
                        } else {
                            // file not match type
                        }
                    }
                }

                if(count($file_public) > 0)
                {
                    foreach ($file_public as $key => $value)
                    {
                        $data = array();
                        $data['company_id'] = $_id;
                        $data['year'] = $year;
                        $data['image'] = $value;
                        $data['order_on'] = ($key + 1);
                        $this->db->insert('company_public_image', $data);
                    }
                }
            }   

            return true;
        }
    }

    function forget_password()
    {
        $this->db->select('company_user.*');
        $this->db->from('company_user');
        $this->db->join('company', 'company.id = company_user.company_id');
        $this->db->where('company_user.email', $this->input->post('email'));
        $this->db->where('company_user.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            $salt = md5($this->input->post('email').'-'.time());
            $data2['salt'] = $salt;
            $this->db->where('id', $data['id']);
            $this->db->update('company_user', $data2);

            $data['salt'] = $salt;
            return $data;
        }
        else
        {
            return false;
        }
    }

    function get_salt($email)
    {
        $this->db->select('company_user.*');
        $this->db->from('company_user');
        $this->db->join('company', 'company.id = company_user.company_id');
        $this->db->where('company_user.email', $email);
        $this->db->where('company_user.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            return $data['salt'];
        }
        else
        {
            return false;
        }
    }

    function reset_password()
    {
        $data['password'] = md5($this->input->post('password'));
        $data['salt'] = '';
        $data['updated_on'] = date('Y-m-d H:i:s');
        $data['updated_by'] = 0;
        $this->db->where('email', $this->input->get('email'));
        $this->db->where('company_user.status', 'Y');
        $this->db->update('company_user', $data);
    }

    function get_profile()
    {
        $this->db->select('company.*, company_type.name as company_type, zone.name as zone');
        $this->db->from('company');
        $this->db->join('company_type', 'company_type.id = company.company_type_id');
        $this->db->join('zone', 'zone.id = company.zone_id');
        $this->db->where("company.status <> 'D'");
		$this->db->where('company.id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
        $data = $query->row_array();

        if(@$data['id'] > 0)
        {
            $this->db->select('*');
            $this->db->from('company_user');
            $this->db->where('company_id', $data['id']);
            $this->db->where('type', 'M');
            $query = $this->db->get();
            $data['user'] = $query->row_array();

            $this->db->select('max(year) as year');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $query = $this->db->get();
            $year = $query->row_array();
            $data['year'] = $year['year'];

            $this->db->select('max(year) as year');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where("year < ".$data['year']);
            $query = $this->db->get();
            $prev_year = $query->row_array();
            if($prev_year != '')
            {
                $data['prev_year'] = $prev_year['year'];
            }
            else
            {
                $data['prev_year'] = '';
            }

            $this->db->select('min(year) as year');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where("year > ".$data['year']);
            $query = $this->db->get();
            $next_year = $query->row_array();
            if($prev_year != '')
            {
                $data['next_year'] = $next_year['year'];
            }
            else
            {
                $data['next_year'] = '';
            }

            $this->db->select('*');
            $this->db->from('company_garbage_type');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['garbage_type'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_manage');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['manage'] = $query->row_array();

            $this->db->select('*');
            $this->db->from('company_policy_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['policy_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_person_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['person_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_model_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['model_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_activity_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['activity_image'] = $query->result_array();

            $this->db->select('*');
            $this->db->from('company_public_image');
            $this->db->where('company_id', $data['id']);
            $this->db->where('year', $data['year']);
            $query = $this->db->get();
            $data['public_image'] = $query->result_array();
        }
        
        return $data;
    }

    function update_profile()
    {
        $data = array();
		$data['company_type_id'] = $this->input->post('company_type_id');
		$data['zone_id'] = $this->input->post('zone_id');
		$data['open_mon'] = (($this->input->post('open_mon') == 'Y') ? 'Y' : 'N');
		$data['open_tue'] = (($this->input->post('open_tue') == 'Y') ? 'Y' : 'N');
		$data['open_wed'] = (($this->input->post('open_wed') == 'Y') ? 'Y' : 'N');
		$data['open_thu'] = (($this->input->post('open_thu') == 'Y') ? 'Y' : 'N');
		$data['open_fri'] = (($this->input->post('open_fri') == 'Y') ? 'Y' : 'N');
		$data['open_sat'] = (($this->input->post('open_sat') == 'Y') ? 'Y' : 'N');
		$data['open_sun'] = (($this->input->post('open_sun') == 'Y') ? 'Y' : 'N');
		$data['open_start'] = $this->input->post('open_start').':00';
		$data['open_end'] = $this->input->post('open_end').':00';
		$data['guest_qty'] = $this->input->post('guest_qty');
		$data['staff_qty'] = $this->input->post('staff_qty');
		$data['size'] = $this->input->post('size');
		$data['address'] = $this->input->post('address');
		$data['tel1'] = $this->input->post('tel1');
		$data['tel2'] = $this->input->post('tel2');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_type'] = 'C';
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $this->authen->user_data['company_id']);
		$this->db->where("status <> 'D'");
		$this->db->update('company', $data);
		
		$this->db->select('max(year) as year');
		$this->db->from('company_garbage_type');
		$this->db->where('company_id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
		$year = $query->row_array();
		$year = $year['year'];

		if($year < date('Y'))
		{
			$data2 = array();
			$data2['company_id'] = $this->authen->user_data['company_id'];
			$data2['year'] = date('Y');
			$data2['description'] = $this->input->post('garbage_type_description');
			$this->db->insert('company_garbage_type', $data2);
		}
		else
		{
			$data2 = array();
			$data2['description'] = $this->input->post('garbage_type_description');
			$this->db->where('company_id', $this->authen->user_data['company_id']);
			$this->db->where('year', $year);
			$this->db->update('company_garbage_type', $data2);
		}
		
		$this->db->select('max(year) as year');
		$this->db->from('company_manage');
		$this->db->where('company_id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
		$year = $query->row_array();
		$year = $year['year'];

		if($year < date('Y'))
		{
			$data3 = array();
			$data3['company_id'] = $this->authen->user_data['company_id'];
			$data3['year'] = date('Y');
			$data3['policy_name'] = $this->input->post('policy_name');
			$data3['policy_description'] = $this->input->post('policy_description');
			$data3['person_name'] = $this->input->post('person_name');
			$data3['person_description'] = $this->input->post('person_description');
			$data3['model_name'] = $this->input->post('model_name');
			$data3['model_description'] = $this->input->post('model_description');
			$data3['activity_name'] = $this->input->post('activity_name');
			$data3['activity_description'] = $this->input->post('activity_description');
			$data3['public_name'] = $this->input->post('public_name');
			$data3['public_description'] = $this->input->post('public_description');
			$data3['created_on'] = date('Y-m-d H:i:s');
			$this->db->insert('company_manage', $data3);
		}
		else
		{
			$data3 = array();
			$data3['policy_name'] = $this->input->post('policy_name');
			$data3['policy_description'] = $this->input->post('policy_description');
			$data3['person_name'] = $this->input->post('person_name');
			$data3['person_description'] = $this->input->post('person_description');
			$data3['model_name'] = $this->input->post('model_name');
			$data3['model_description'] = $this->input->post('model_description');
			$data3['activity_name'] = $this->input->post('activity_name');
			$data3['activity_description'] = $this->input->post('activity_description');
			$data3['public_name'] = $this->input->post('public_name');
			$data3['public_description'] = $this->input->post('public_description');
			$this->db->where('company_id', $this->authen->user_data['company_id']);
			$this->db->where('year', $year);
			$this->db->update('company_manage', $data3);
		}

		$data4 = array();
		$data4['name'] = $this->input->post('contact_name');
		$data4['updated_on'] = date('Y-m-d H:i:s');
		$data4['updated_type'] = 'C';
		$data4['updated_by'] = $this->authen->id;
		$this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->where('type', 'M');
		$this->db->update('company_user', $data4);
    }

    function update_accept()
    {
        $data = array();
        $data['is_accept'] = (($this->input->post('is_accept') == 'Y') ? 'Y' : 'N');
        $this->db->where('id', $this->authen->user_data['company_id']);
        $this->db->where("status <> 'D'");
        $this->db->update('company', $data);

        $this->db->select('company_user.*, company.is_accept');
        $this->db->from('company_user');
        $this->db->join('company', 'company.id = company_user.company_id');
        $this->db->where('company_user.id', $this->authen->id);
        $this->db->where('company_user.status', 'Y');
        $this->db->where('company.status', 'Y');
        $query = $this->db->get();
        $user = $query->row_array();

        if(@$user['id'] > 0)
        {
            return $user;
        }
        else
        {
            return false;
        }
    }

}