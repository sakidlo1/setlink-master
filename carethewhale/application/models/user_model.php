<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function check_email_exists($input, $id)
    {
    	if(@$input['email'] == "")
    	{
    		return true;
    	}

		$this->db->select('count(*) as count_rec');
        $this->db->from('company_user');
        $this->db->where('email', $input['email']);
        $this->db->where("status <> 'D'");

        if($id > 0)
        {
        	$this->db->where("id <> ".$id);
        }

        $query = $this->db->get();
        $data = $query->row_array();

        if($data['count_rec'] > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('company_user');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(company_user.name LIKE '%".$fName."%' or company_user.email LIKE '%".$fName."%')");
        }

		$this->db->where("company_user.type <> 'M'");
        $this->db->where("company_user.status <> 'D'");
        $this->db->where('company_user.company_id', $this->authen->user_data['company_id']);

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company_user.*');
		$this->db->from('company_user');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(company_user.name LIKE '%".$fName."%' or company_user.email LIKE '%".$fName."%')");
        }

		$this->db->where("company_user.type <> 'M'");
        $this->db->where("company_user.status <> 'D'");
        $this->db->where('company_user.company_id', $this->authen->user_data['company_id']);

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('company_user.*');
		$this->db->from('company_user');
		$this->db->where("company_user.type <> 'M'");
        $this->db->where("company_user.status <> 'D'");
		$this->db->where('company_user.id', $id);
		$this->db->where('company_user.company_id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
    	$data['company_id'] = $this->authen->user_data['company_id'];
    	$data['type'] = 'S';
		$data['email'] = $this->input->post('email');
        $data['password'] = md5($this->input->post('password'));
        $data['avatar'] = '';
		$data['name'] = $this->input->post('name');
		$data['status'] = 'Y';
		$data['salt'] = '';
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_type'] = 'C';
		$data['updated_by'] = 0;
		$data['last_login'] = NULL;
        $this->db->insert('company_user', $data);
    }
	
	function update($id)
    {
    	$data = array();
		$data['email'] = $this->input->post('email');

		if($this->input->post('password')!="")
		{
			$data['password'] = md5($this->input->post('password'));
		}

		$data['name'] = $this->input->post('name');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_type'] = 'C';
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
		$this->db->where("type <> 'M'");
    	$this->db->where("status <> 'D'");
    	$this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_user', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
    	$data['updated_type'] = 'C';
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
		$this->db->where("type <> 'M'");
    	$this->db->where("status <> 'D'");
    	$this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_user', $data);
    }
}