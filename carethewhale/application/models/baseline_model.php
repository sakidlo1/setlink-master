<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class baseline_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_garbage_category()
    {
        $this->db->select('*');
        $this->db->from('garbage_category');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_kind($garbage_category_id)
    {
        $this->db->select('*');
        $this->db->from('garbage_kind');
        $this->db->where('status', 'Y');
        $this->db->where('garbage_category_id', $garbage_category_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_garbage_disposal()
    {
        $this->db->select('*');
        $this->db->from('garbage_disposal');
        $this->db->where('status', 'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_company_sort_point()
    {
        $this->db->select('*');
        $this->db->from('company_sort_point');
        $this->db->where('status', 'Y');
        $this->db->where('company_id', $this->authen->user_data['company_id']);
        $query = $this->db->get();
        return $query->result_array();
    }

	function count_all()
    {
        $this->db->select('count(*) as count_rec');
		$this->db->from('company_baseline');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_baseline.company_sort_point_id', 'left');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_baseline.garbage_kind_id');
        $this->db->join('garbage_disposal', 'garbage_disposal.id = company_baseline.garbage_disposal_id');

        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
        	$this->db->where("(garbage_kind.name LIKE '%".$fName."%' or garbage_disposal.name LIKE '%".$fName."%' or company_sort_point.name LIKE '%".$fName."%')");
        }

        $this->db->where("company_baseline.status <> 'D'");
        $this->db->where('company_baseline.company_id', $this->authen->user_data['company_id']);

		$query = $this->db->get();
        $data = $query->row_array();
        return $data['count_rec'];
    }

    function get_all($start = 0, $limit = 0)
    {
        $this->db->select('company_baseline.*, garbage_kind.name as garbage_kind, garbage_disposal.name as garbage_disposal, company_sort_point.name as company_sort_point');
		$this->db->from('company_baseline');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_baseline.company_sort_point_id', 'left');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_baseline.garbage_kind_id');
        $this->db->join('garbage_disposal', 'garbage_disposal.id = company_baseline.garbage_disposal_id');
        
        $fName = @$this->input->post('columns')[0]['search']['value'];
        if($fName != "")
        {
            $this->db->where("(garbage_kind.name LIKE '%".$fName."%' or garbage_disposal.name LIKE '%".$fName."%' or company_sort_point.name LIKE '%".$fName."%')");
        }

        $this->db->where("company_baseline.status <> 'D'");
        $this->db->where('company_baseline.company_id', $this->authen->user_data['company_id']);

		if(@$this->input->post('order')[0]['column'] != "" && $this->input->post('order')[0]['dir'] != "")
		{
			$this->db->order_by($this->input->post('columns')[$this->input->post('order')[0]['column']]['data'].' '.$this->input->post('order')[0]['dir']);
		}
		else
		{
			$this->db->order_by('id asc');
		}

		if($limit > 0)
		{
			$this->db->limit($limit, $start);	
		}
		
		$query = $this->db->get();
        return $query->result_array();
    }
	
	function get_by_id($id)
    {
        $this->db->select('company_baseline.*, garbage_kind.garbage_category_id');
		$this->db->from('company_baseline');
        $this->db->join('company_sort_point', 'company_sort_point.id = company_baseline.company_sort_point_id', 'left');
        $this->db->join('garbage_kind', 'garbage_kind.id = company_baseline.garbage_kind_id');
        $this->db->join('garbage_disposal', 'garbage_disposal.id = company_baseline.garbage_disposal_id');
        $this->db->where("company_baseline.status <> 'D'");
		$this->db->where('company_baseline.id', $id);
        $this->db->where('company_baseline.company_id', $this->authen->user_data['company_id']);
		$query = $this->db->get();
        return $query->row_array();
    }

    function insert()
    {
    	$data = array();
        $data['company_id'] = $this->authen->user_data['company_id'];
        $data['company_sort_point_id'] = $this->input->post('company_sort_point_id');
		$data['garbage_kind_id'] = $this->input->post('garbage_kind_id');
		$data['garbage_disposal_id'] = $this->input->post('garbage_disposal_id');
        $data['baseline'] = $this->input->post('baseline');
        $data['is_daily'] = (($this->input->post('type') == 'is_daily') ? 'Y' : 'N');
        $data['is_weekly'] = (($this->input->post('type') == 'is_weekly') ? 'Y' : 'N');
        $data['is_monthly'] = (($this->input->post('type') == 'is_monthly') ? 'Y' : 'N');
		$data['status'] = 'Y';
		$data['created_on'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->authen->id;
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = 0;
        $this->db->insert('company_baseline', $data);
    }
	
	function update($id)
    {
    	$data = array();
    	$data['garbage_kind_id'] = $this->input->post('garbage_kind_id');
        $data['company_sort_point_id'] = $this->input->post('company_sort_point_id');
        $data['garbage_disposal_id'] = $this->input->post('garbage_disposal_id');
        $data['baseline'] = $this->input->post('baseline');
        $data['is_daily'] = (($this->input->post('type') == 'is_daily') ? 'Y' : 'N');
        $data['is_weekly'] = (($this->input->post('type') == 'is_weekly') ? 'Y' : 'N');
        $data['is_monthly'] = (($this->input->post('type') == 'is_monthly') ? 'Y' : 'N');
		$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_baseline', $data);
    }
	
	function delete($id)
    {
    	$data = array();
    	$data['status'] = 'D';
    	$data['updated_on'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->authen->id;
		$this->db->where('id', $id);
    	$this->db->where("status <> 'D'");
        $this->db->where('company_id', $this->authen->user_data['company_id']);
		$this->db->update('company_baseline', $data);
    }
}