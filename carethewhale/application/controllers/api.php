<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->model('api_model', 'this_model');
	}

	public function get_stats()
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_summary_cf();
		echo json_encode($data);
	}

	public function get_company()
	{
		header('Content-Type: application/json');
		$data = $this->this_model->get_company();
		echo json_encode($data);
	}
}