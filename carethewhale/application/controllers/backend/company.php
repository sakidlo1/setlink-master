<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->smarty->assign('page_name', 'บริษัทสมาชิก');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
    }

    public function check_email_exists($company_id)
    {
    	header('Content-Type: application/json');
		
		$output = array(
			"status" => (($this->this_model->check_email_exists($_POST, $company_id) == false) ? false : true)
		);

		echo json_encode($output);
    }
    
	public function index()
	{	
		if($this->input->get('delete') == "1")
		{
			$this->smarty->assign('success_msg', 'ลบข้อมูลเรียบร้อยแล้ว');
		}

		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all();
		$data = $this->this_model->get_all($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}
	
	public function form($action = '', $id = 0)
	{
		if($this->input->post('action') != "")
		{
			if($action == 'delete')
			{
				$this->this_model->delete($id);
				redirect('/backend/'.$this->this_page.'?delete=1');
			}
		}
		else
		{
			if($id > 0)
			{
				$this->smarty->assign('item', $this->this_model->get_by_id($id));
			}

			$this->smarty->assign('action', $action);
			$this->smarty->assign('id', $id);
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function edit($id = 0, $year = 0)
	{
		$item = $this->this_model->get_by_id($id, $year);

		if(@$item['id'] > 0)
		{
			if($this->input->post('contact_name') != '')
			{
				$this->this_model->update($id);

				if($item['status'] == 'N' && $this->input->post('status') == 'Y')
				{
					$email = $item['user']['email'];
					$subject = 'การเข้าใช้งาน Climate Care Platform';

					$message = $this->smarty->fetch('email/approve_company.tpl');
					$message = str_replace('[[company_name]]', $item['name'], $message);

					$this->load->library('mailer');
					$this->mailer->send($email, $subject, $message);
				}
				

				redirect('/backend/'.$this->this_page.'/detail/'.$id.'?update=1');
			}

			$this->smarty->assign('company_type', $this->this_model->get_company_type());
			$this->smarty->assign('zone', $this->this_model->get_zone());
			$this->smarty->assign('item', $item);
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
		else
		{
			redirect('/backend/'.$this->this_page);
		}
	}

	public function detail($id = 0, $year = 0)
	{
		if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}

		if($id > 0)
		{
			$this->smarty->assign('item', $this->this_model->get_by_id($id, $year));
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}