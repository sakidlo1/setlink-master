<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->smarty->assign('page_name', 'ผู้ใช้งาน');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
		$this->load->model('stats_model');

		$whale = $this->stats_model->get_total_cf();
		$stats_data = $this->stats_model->get_stats();
		$stats = array(
			'whale' => array(
				'org' => $whale['total_company'],
				'cf' => $whale['total_cf']
			),
			'bear' => array(
				'org' => $stats_data['care_the_bear_org'],
				'cf' => $stats_data['care_the_bear_cf']
			),
			'wild' => array(
				'org' => $stats_data['care_the_wild_org'],
				'cf' => $stats_data['care_the_wild_cf']
			)
		);
		$this->smarty->assign('stats', $stats);
    }

    public function check_email_exists($id)
    {
    	header('Content-Type: application/json');
		
		$output = array(
			"status" => (($this->this_model->check_email_exists($_POST, $id) == false) ? false : true)
		);

		echo json_encode($output);
    }

    public function login()
	{
		$this->smarty->assign('page_name', 'เข้าสู่ระบบ');

		if(@$_SESSION['carethewhale_admin']['id'] > 0)
		{
			redirect('/backend/home');
		}

		if(@$this->input->post('login') && $_SESSION['cs_token'] == $this->input->post('cs_token') && $_COOKIE['cs_token'] == $this->input->post('cs_token'))
		{
			$data = $this->this_model->login();
			if(@$data['id']>0)
			{
				$_SESSION['carethewhale_admin'] = $data;
				redirect('/backend/home');
			}
			else
			{
				$this->smarty->assign('error_msg', 'อีเมล และรหัสผ่านไม่ถูกต้อง');
				$cs_token = md5(uniqid(rand(), true));
				$_SESSION['cs_token'] = $cs_token;
				setcookie("cs_token", $cs_token);
				$this->smarty->assign('cs_token', $cs_token);
				$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
		}
		else
		{
			$cs_token = md5(uniqid(rand(), true));
			$_SESSION['cs_token'] = $cs_token;
			setcookie("cs_token", $cs_token);
			$this->smarty->assign('cs_token', $cs_token);
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}
	
	public function logout()
	{
		unset($_SESSION['carethewhale_admin']);
		redirect('/backend/home');
	}

	public function forget_password()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'ลืมรหัสผ่าน');

		if($this->input->post('forget_password') && $_SESSION['cs_token'] == $this->input->post('cs_token') && $_COOKIE['cs_token'] == $this->input->post('cs_token'))
		{
			$member = $this->this_model->forget_password();
			if($member != false)
			{
				$email = $member['email'];
				$reset_password_url = config_item('base_url').'backend/user/reset_password?email='.$email.'&salt='.$member['salt'];
				$to = $email;
				$subject = 'Forgot Password - '.config_item('site_name');

				$message = $this->smarty->fetch('email/forget_password.tpl');
				$message = str_replace('[[customer_name]]', $member['name'], $message);
				$message = str_replace('[[link]]', $reset_password_url, $message);

				$this->load->library('mailer');
				$this->mailer->send($email, $subject, $message);

				$this->smarty->assign('success_msg', 'กรุณาตรวจสอบอีเมลเพื่อ ตั้งรหัสผ่านใหม่');

				$cs_token = md5(uniqid(rand(), true));
				$_SESSION['cs_token'] = $cs_token;
				setcookie("cs_token", $cs_token);
				$this->smarty->assign('cs_token', $cs_token);
				$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
			else
			{
				$this->smarty->assign('error_msg', 'ไม่พบอีเมลนี้ในระบบ');
				$cs_token = md5(uniqid(rand(), true));
				$_SESSION['cs_token'] = $cs_token;
				setcookie("cs_token", $cs_token);
				$this->smarty->assign('cs_token', $cs_token);
				$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
		}
		else
		{
			$cs_token = md5(uniqid(rand(), true));
			$_SESSION['cs_token'] = $cs_token;
			setcookie("cs_token", $cs_token);
			$this->smarty->assign('cs_token', $cs_token);
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function reset_password()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		$this->smarty->assign('page_name', 'ตั้งรหัสผ่านใหม่');

		if($this->input->get('email') != "" && $this->input->get('salt') != "")
		{
			$salt = $this->this_model->get_salt($this->input->get('email'));
			if($salt == false || $salt == "" || $salt != $this->input->get('salt'))
			{
				redirect('/backend/home');
			}
		}
		else
		{
			redirect('/backend/home');
		}
		
		if($this->input->post('reset_password'))
		{
			$this->this_model->reset_password();
			$this->smarty->assign('success_msg', 'ตั้งรหัสผ่านใหม่เรียบร้อยแล้ว');
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
		else
		{
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}
	
	public function index()
	{	
		if($this->input->get('add') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}
		else if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}
		else if($this->input->get('delete') == "1")
		{
			$this->smarty->assign('success_msg', 'ลบข้อมูลเรียบร้อยแล้ว');
		}

		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all();
		$data = $this->this_model->get_all($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}
	
	public function form($action = '', $id = 0)
	{
		if($this->input->post('action') != "")
		{
			if($action == 'add')
			{
				$this->this_model->insert();
				redirect('/backend/'.$this->this_page.'?add=1');
			}
			else if($action == 'edit')
			{
				$this->this_model->update($id);
				redirect('/backend/'.$this->this_page.'?update=1');
			}
			else if($action == 'delete')
			{
				$this->this_model->delete($id);
				redirect('/backend/'.$this->this_page.'?delete=1');
			}
		}
		else
		{
			if($id > 0)
			{
				$this->smarty->assign('item', $this->this_model->get_by_id($id));
			}

			$this->smarty->assign('action', $action);
			$this->smarty->assign('id', $id);
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function detail($id = 0)
	{
		if($id > 0)
		{
			$this->smarty->assign('item', $this->this_model->get_by_id($id));
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}