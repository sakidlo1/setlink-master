<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		
		if($this->authen->controller != "")
		{
			$this->this_page = $this->authen->controller;
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_page = "home";
			$this->this_sub_page = 'index';
		}
		
		$this->smarty->assign('page_name', 'Home');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
	}
	
	public function index()
	{
		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function get_total_cf($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total = $this->this_model->get_total_cf($start, $end);

		$data = array(
			"total_cf" => $total['total_cf'],
			"total_tree" => ($total['total_cf'] / 9),
			"total_company" => $total['total_company']
		);

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_type($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_type($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_plastic($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_plastic($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_recycle($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_recycle($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}
}