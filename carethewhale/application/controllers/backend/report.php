<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);

        $this->load->library('pagination');
		
		$this->this_page = $this->authen->controller;
		if($this->authen->controller != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}
		
		$this->smarty->assign('page_name', 'Report');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');

		$year = $this->this_model->get_year();
		$this->smarty->assign('min_year', $year['min_year']);
		$this->smarty->assign('max_year', $year['max_year']);
	}
	
	public function index()
	{
		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function get_total_cf($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total = $this->this_model->get_total_cf($start, $end);

		$data = array(
			"total_cf" => $total['total_cf'],
			"total_tree" => ($total['total_cf'] / 9),
			"total_company" => $total['total_company']
		);

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_type($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_type($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_plastic($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_plastic($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}

	public function get_total_cf_by_garbage_recycle($start = '', $end = '')
	{
		header('Content-Type: application/json');

		$total_cf = $this->this_model->get_total_cf_by_garbage_recycle($start, $end);

		$data = array();

		$all = 0;
		foreach($total_cf as $key => $value)
		{
			$all = $all + $value['total_weight'];
		}

		foreach($total_cf as $key => $value)
		{
			$data[] = array(
				"name" => $value['name'],
				"weight" => number_format($value['total_weight'], 2),
				"percent" => number_format((($value['total_weight'] / $all) * 100), 2),
				"total_cf" => number_format($value['total_cf'], 2),
				"total_tree" => number_format(($value['total_cf'] / 9), 0)
			);
		}

		echo json_encode($data);
	}

	public function top_10()
	{
		$this->smarty->assign('data', $this->this_model->top_10());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function type()
	{
		$this->smarty->assign('data', $this->this_model->type());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'_'.(($this->input->get('garbage') != '') ? $this->input->get('garbage') : 'non_recycle').'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
	
	public function type_waste()
	{
		$this->smarty->assign('data', $this->this_model->type_waste());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function first()
	{
		$this->smarty->assign('data', $this->this_model->first());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function middle()
	{
		$this->smarty->assign('data', $this->this_model->middle());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function last()
	{
		$this->smarty->assign('data', $this->this_model->last());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function company()
	{
		$this->smarty->assign('data', $this->this_model->company());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function company_name()
	{
        //$dates =  $this->input->get('dates');
		$data['company_data'] = $this->this_model->getFilteredData(
            $this->input->get('company_id'),
            $this->input->get('start_date'),
            $this->input->get('end_date'),
            $this->input->get('garbage_kind_id'),
            $this->input->get('garbage_type_id'),
        );
        $filter = [
           'company_id' => $this->input->get('company_id'),
           //'dates' => $this->input->get('dates'),
		   'start_date' => $this->input->get('start_date'),
		   'end_date' => $this->input->get('end_date'),
           'garbage_kind_id' => $this->input->get('garbage_kind_id'),
           'garbage_type_id' => $this->input->get('garbage_type_id'),
        ];

        $this->smarty->assign('filter',$filter);
		$this->smarty->assign('garbage_kind', $this->this_model->get_garbage_kind());
		$this->smarty->assign('garbage_type', $this->this_model->get_garbage_type());
		$this->smarty->assign('company', $this->this_model->get_company());

        $this->smarty->assign($data);
		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
	
	public function company_type()
	{
		$this->smarty->assign('data', $this->this_model->company_type());

		if($this->input->get('export') == 'Y')
		{
			$html = $this->smarty->fetch('backend/'.$this->this_page.'_'.$this->this_sub_page.'_export.tpl');

			if(trim($html) != "")
			{
				$this->load->library('excel');
				$tmpfile = BASEPATH.'../images/upload/'.time().'.html';
				file_put_contents($tmpfile, $html);

				$reader = new PHPExcel_Reader_HTML; 
				$content = $reader->load($tmpfile);
				unlink($tmpfile);

				$objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel5');
				ob_end_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$this->this_page.'_'.$this->this_sub_page.'.xls"');
				header('Cache-Control: max-age=0');
				
				$objWriter->save('php://output');
				exit();
			}
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}