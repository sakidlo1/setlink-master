<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Garbage_category extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->smarty->assign('page_name', 'หมวดหมู่ขยะ');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
    }
    
	public function index()
	{	
		if($this->input->get('add') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}
		else if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}
		else if($this->input->get('delete') == "1")
		{
			$this->smarty->assign('success_msg', 'ลบข้อมูลเรียบร้อยแล้ว');
		}

		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}

	public function load_data()
	{
		header('Content-Type: application/json');

		$total = $this->this_model->count_all();
		$data = $this->this_model->get_all($this->input->post('start'), $this->input->post('length'));

		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsFiltered" => $total,
			"recordsTotal" => $total,
			"data" => $data
		);

		echo json_encode($output);
	}
	
	public function form($action = '', $id = 0)
	{
		if($this->input->post('action') != "")
		{
			if($action == 'add')
			{
				$this->this_model->insert();
				redirect('/backend/'.$this->this_page.'?add=1');
			}
			else if($action == 'edit')
			{
				$this->this_model->update($id);
				redirect('/backend/'.$this->this_page.'?update=1');
			}
			else if($action == 'delete')
			{
				$this->this_model->delete($id);
				redirect('/backend/'.$this->this_page.'?delete=1');
			}
		}
		else
		{
			if($id > 0)
			{
				$this->smarty->assign('item', $this->this_model->get_by_id($id));
			}

			$this->smarty->assign('action', $action);
			$this->smarty->assign('id', $id);
			$this->smarty->assign('garbage_type', $this->this_model->get_garbage_type());
			$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function detail($id = 0)
	{
		if($id > 0)
		{
			$this->smarty->assign('item', $this->this_model->get_by_id($id));
		}

		$this->smarty->display('backend/'.$this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}