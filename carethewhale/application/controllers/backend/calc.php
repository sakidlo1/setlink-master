<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calc extends CI_Controller {

	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('authen');
		$this->smarty->assign('admin', $this->authen->user_data);
		$this->smarty->assign('authen', $this->authen);
		$this->this_page = $this->authen->controller;
		
		if($this->authen->function != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->smarty->assign('page_name', 'รายละเอียดการคำนวณ');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('backend/'.$this->this_page.'_model', 'this_model');
    }
    
	public function index()
	{
		if($this->input->get('update') == "1")
		{
			$this->smarty->assign('success_msg', 'บันทึกข้อมูลเรียบร้อยแล้ว');
		}

		if($this->input->post('save') != '')
		{
			$this->this_model->update();
			redirect('/backend/'.$this->this_page.'?update=1');
		}

		$this->smarty->assign('garbage_kind', $this->this_model->get_garbage_kind());
		$this->smarty->assign('garbage_disposal', $this->this_model->get_garbage_disposal());
		$this->smarty->assign('calc', $this->this_model->get_calc());
		$this->smarty->display('backend/'.$this->this_page.'.tpl');
	}
}