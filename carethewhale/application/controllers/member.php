<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
class Member extends CI_Controller {

	public function __construct()
    {
		parent::__construct();

		$this->load->library('authen_member', NULL, 'authen');
		$this->smarty->assign('authen', $this->authen);
		$this->smarty->assign('member', $this->authen->user_data);
		
		$this->this_page = $this->authen->controller;
		if($this->authen->controller != "")
		{
			$this->this_sub_page = $this->authen->function;
		}
		else
		{
			$this->this_sub_page = 'index';
		}

		$this->load->model($this->this_page.'_model', 'this_model');

		$this->smarty->assign('page_name', 'Member');
		$this->smarty->assign('page', $this->this_page);
		$this->smarty->assign('sub_page', $this->this_sub_page);
		$this->smarty->assign('site_name', config_item('site_name'));
		$this->smarty->assign('company_name', config_item('company_name'));
		$this->smarty->assign('base_url', config_item('base_url'));
		$this->smarty->assign('image_url', config_item('image_url'));
		$this->smarty->assign('error_msg', '');
		$this->smarty->assign('success_msg', '');

		$this->load->model('stats_model');
		$whale = $this->stats_model->get_total_cf();
		$stats_data = $this->stats_model->get_stats();
		$stats = array(
			'whale' => array(
				'org' => $whale['total_company'],
				'cf' => $whale['total_cf']
			),
			'bear' => array(
				'org' => $stats_data['care_the_bear_org'],
				'cf' => $stats_data['care_the_bear_cf']
			),
			'wild' => array(
				'org' => $stats_data['care_the_wild_org'],
				'cf' => $stats_data['care_the_wild_cf']
			)
		);
		$this->smarty->assign('stats', $stats);
	}

	public function check_email_exists()
    {
    	header('Content-Type: application/json');
		
		$output = array(
			"status" => (($this->this_model->check_email_exists($_POST) == false) ? false : true)
		);

		echo json_encode($output);
    }
    public function member_profile()
    {
        $this->smarty->assign('page_name', 'Profile edit form');
        $this->smarty->assign('company_type', $this->this_model->get_company_type());
        $this->smarty->display('edit_member_profile.tpl');
    }
	public function login()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		if($this->input->post('email') != '' && $_SESSION['cs_token'] == $this->input->post('cs_token') && $_COOKIE['cs_token'] == $this->input->post('cs_token'))
		{
			$data = $this->this_model->login();
			if(@$data['id']>0)
			{
				$_SESSION['member'] = $data;
				redirect('/');
			}
			else
			{
				$this->smarty->assign('error_msg', 'อีเมล และรหัสผ่านไม่ถูกต้อง');
			}
		}
		else
		{
			$cs_token = md5(uniqid(rand(), true));
			$_SESSION['cs_token'] = $cs_token;
			setcookie("cs_token", $cs_token);
			$this->smarty->assign('cs_token', $cs_token);
		}

		$this->smarty->assign('page_name', 'เข้าสู่ระบบ');
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function register()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}
		
		if(@$_SESSION['climate_member']['id'] > 0)
		{
			$this->smarty->assign('climate_member', $_SESSION['climate_member']);
		}

		if($this->input->post('name') != '')
		{
			$post_data = http_build_query(
			    array(
			        'secret' => '6LfUQH0cAAAAAMcAuIfS9ysXvB4q8-aolZFcWxCq',
			        'response' => $_POST['g-recaptcha-response'],
			        'remoteip' => $_SERVER['REMOTE_ADDR']
			    )
			);
			$opts = array('http' =>
			    array(
			        'method'  => 'POST',
			        'header'  => 'Content-type: application/x-www-form-urlencoded',
			        'content' => $post_data
			    )
			);
			$context  = stream_context_create($opts);
			$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
			$result = json_decode($response);

			if($result->success)
			{
				if($this->this_model->register())
				{
					redirect('/member/register_success');
				}
				else
				{
					redirect('/member/register_error');
				}
			}
			else
			{
				redirect('/member/register_error_captcha');
			}
		}

		$this->smarty->assign('page_name', 'สมัครสมาชิก');
		$this->smarty->assign('company_type', $this->this_model->get_company_type());
		$this->smarty->assign('zone', $this->this_model->get_zone());
		$this->smarty->assign('garbage_type', $this->this_model->get_garbage_type());
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function register_success()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'สมัครสมาชิกสำเร็จ');
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function register_error()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'สมัครสมาชิกไม่สำเร็จ');
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function register_error_captcha()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'สมัครสมาชิกไม่สำเร็จ');
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function logout()
	{
		unset($_SESSION['climate_member']);
		unset($_SESSION['carethebear_member']);
		unset($_SESSION['member']);
		redirect('/');
	}

	public function forget_password()
	{
		if($this->authen->id > 0)
		{
			redirect('/');
		}

		$this->smarty->assign('page_name', 'ลืมรหัสผ่าน');

		if($this->input->post('forget_password') && $_SESSION['cs_token'] == $this->input->post('cs_token') && $_COOKIE['cs_token'] == $this->input->post('cs_token'))
		{
			$member = $this->this_model->forget_password();
			if($member != false)
			{
				$email = $member['email'];
				$reset_password_url = config_item('base_url').'member/reset_password?email='.$email.'&salt='.$member['salt'];
				$to = $email;
				$subject = 'Forgot Password - '.config_item('site_name');

				$message = $this->smarty->fetch('email/forget_password.tpl');
				$message = str_replace('[[customer_name]]', $member['name'], $message);
				$message = str_replace('[[link]]', $reset_password_url, $message);

				$this->load->library('mailer');
				$this->mailer->send($email, $subject, $message);

				$this->smarty->assign('success_msg', 'กรุณาตรวจสอบอีเมลเพื่อ ตั้งรหัสผ่านใหม่');

				$cs_token = md5(uniqid(rand(), true));
				$_SESSION['cs_token'] = $cs_token;
				setcookie("cs_token", $cs_token);
				$this->smarty->assign('cs_token', $cs_token);
				$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
			else
			{
				$this->smarty->assign('error_msg', 'ไม่พบอีเมลนี้ในระบบ');
				$cs_token = md5(uniqid(rand(), true));
				$_SESSION['cs_token'] = $cs_token;
				setcookie("cs_token", $cs_token);
				$this->smarty->assign('cs_token', $cs_token);
				$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
			}
		}
		else
		{
			$cs_token = md5(uniqid(rand(), true));
			$_SESSION['cs_token'] = $cs_token;
			setcookie("cs_token", $cs_token);
			$this->smarty->assign('cs_token', $cs_token);
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function reset_password()
	{
		$this->smarty->assign('page_name', 'ตั้งรหัสผ่านใหม่');

		if($this->input->get('email') != "" && $this->input->get('salt') != "")
		{
			$salt = $this->this_model->get_salt($this->input->get('email'));
			if($salt == false || $salt == "" || $salt != $this->input->get('salt'))
			{
				redirect('/');
			}
		}
		else
		{
			redirect('/');
		}
		
		if($this->input->post('reset_password'))
		{
			$this->this_model->reset_password();
			$this->smarty->assign('success_msg', 'ตั้งรหัสผ่านใหม่เรียบร้อยแล้ว');
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
		else
		{
			$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
		}
	}

	public function profile()
	{
		if($this->input->post('save'))
		{
			$this->this_model->update_profile();
			redirect('/member/profile?update=1');
		}
		
		if($this->input->get('update') == 1)
		{
			$this->smarty->assign('success_msg', 'แก้ไขข้อมูลเรียบร้อยแล้ว');
		}

		$this->smarty->assign('company_type', $this->this_model->get_company_type());
		$this->smarty->assign('zone', $this->this_model->get_zone());
		$this->smarty->assign('item', $this->this_model->get_profile());
		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}

	public function accept()
	{
		if($this->input->post('save'))
		{
			$data = $this->this_model->update_accept();
			if(@$data['id']>0)
			{
				$_SESSION['member'] = $data;
			}
			redirect('/');
		}

		$this->smarty->display($this->this_page.'_'.$this->this_sub_page.'.tpl');
	}
}