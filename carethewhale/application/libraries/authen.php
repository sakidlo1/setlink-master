<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class authen {

    public $id;
    public $user_data;
    public $controller;
    public $function;
	public $is_login;

    public function __construct()
    {
        $CI =& get_instance();
		$CI->load->helper('url');
        
        if(@$_SESSION['carethewhale_admin']['id'] > 0)
        {
            $this->id = $_SESSION['carethewhale_admin']['id'];
            $this->user_data = $_SESSION['carethewhale_admin'];
            $this->is_login = true;
        }
        else
        {
            $this->is_login = false;
        }

        $this->controller = $CI->uri->segment(2);
        $this->function = $CI->uri->segment(3);

        if($this->is_login == false)
        {
            if($this->controller != "user")
            {
                redirect('/backend/user/login');
            }
            else
            {
                if($this->function != "login" && $this->function != "forget_password" && $this->function != "reset_password")
                {
                    redirect('/backend/user/login');
                }
            }
        }
        else
        {
            if($this->controller == "user" && ($this->function == "login" || $this->function == "forget_password" || $this->function == "reset_password"))
            {
                redirect('/backend');
            }
        }
    }
}

?>