<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty datestring modifier plugin
 *
 * Type:     modifier<br>
 * Name:     datestring<br>
 * Purpose:  simple search/datestring
 *
 * @link http://smarty.php.net/manual/en/language.modifier.datestring.php datestring (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 * @author Uwe Tews
 * @param string $string  input string
 * @return string
 */
function smarty_modifier_datestring($string, $lang, $format = 'datetime', $type = 'long')
{
	if($string == '0000-00-00' || $string == '0000-00-00 00:00:00')
	{
		return '-';
	}

	$_date = explode(' ', $string);
	$date = explode('-', $_date[0]);
	$time = array();
	if(@$_date[1] != '' && $format == 'datetime')
	{
		$time = explode(':', $_date[1]);
	}

	if($lang == 'th')
	{
		$month = array(
			'01' => array(
				'long' => 'มกราคม',
				'short' => 'ม.ค.'
			),
			'02' => array(
				'long' => 'กุมภาพันธ์',
				'short' => 'ก.พ.'
			),
			'03' => array(
				'long' => 'มีนาคม',
				'short' => 'มี.ค.'
			),
			'04' => array(
				'long' => 'เมษายน',
				'short' => 'เม.ย.'
			),
			'05' => array(
				'long' => 'พฤษภาคม',
				'short' => 'พ.ค.'
			),
			'06' => array(
				'long' => 'มิถุนายน',
				'short' => 'มิ.ย.'
			),
			'07' => array(
				'long' => 'กรกฎาคม',
				'short' => 'ก.ค.'
			),
			'08' => array(
				'long' => 'สิงหาคม',
				'short' => 'ส.ค.'
			),
			'09' => array(
				'long' => 'กันยายน',
				'short' => 'ก.ย.'
			),
			'10' => array(
				'long' => 'ตุลาคม',
				'short' => 'ต.ค.'
			),
			'11' => array(
				'long' => 'พฤศจิกายน',
				'short' => 'พ.ย.'
			),
			'12' => array(
				'long' => 'ธันวาคม',
				'short' => 'ธ.ค.'
			)
		);

		if(count($time) > 0)
		{
			$datestring = ((int)$date[2]).' '.$month[$date[1]][$type].' '.($date[0] + 543).' '.$time[0].':'.$time[1].' น.';
		}
		else
		{
			$datestring = ((int)$date[2]).' '.$month[$date[1]][$type].' '.($date[0] + 543);
		}
	}
	else
	{
		if(count($time) > 0)
		{
			if($type == 'short')
			{
				$datestring = date('j M Y H:i', strtotime($string));
			}
			else
			{
				$datestring = date('j F Y H:i', strtotime($string));
			}
		}
		else
		{
			if($type == 'short')
			{
				$datestring = date('j M Y', strtotime($string));
			}
			else
			{
				$datestring = date('j F Y', strtotime($string));
			}
		}
	}

    return $datestring;
}
