<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @UsersManager/index.twig */
class __TwigTemplate_8f12c684defe2fc583d911b41cf3ba069b12591add9274de8995ced93d713d44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ManageAccess"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@UsersManager/index.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<piwik-users-manager
    initial-site-id=\"";
        // line 8
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["idSiteSelected"]) || array_key_exists("idSiteSelected", $context) ? $context["idSiteSelected"] : (function () { throw new RuntimeError('Variable "idSiteSelected" does not exist.', 8, $this->source); })()), "html", null, true);
        echo "\"
    initial-site-name=\"";
        // line 9
        echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [(isset($context["defaultReportSiteName"]) || array_key_exists("defaultReportSiteName", $context) ? $context["defaultReportSiteName"] : (function () { throw new RuntimeError('Variable "defaultReportSiteName" does not exist.', 9, $this->source); })())]);
        echo "\"
    current-user-role=\"'";
        // line 10
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["currentUserRole"]) || array_key_exists("currentUserRole", $context) ? $context["currentUserRole"] : (function () { throw new RuntimeError('Variable "currentUserRole" does not exist.', 10, $this->source); })()), "html", null, true);
        echo "'\"
    access-levels=\"";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["accessLevels"]) || array_key_exists("accessLevels", $context) ? $context["accessLevels"] : (function () { throw new RuntimeError('Variable "accessLevels" does not exist.', 11, $this->source); })())), "html_attr");
        echo "\"
    filter-access-levels=\"";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["filterAccessLevels"]) || array_key_exists("filterAccessLevels", $context) ? $context["filterAccessLevels"] : (function () { throw new RuntimeError('Variable "filterAccessLevels" does not exist.', 12, $this->source); })())), "html_attr");
        echo "\"
>
</piwik-users-manager>

";
    }

    public function getTemplateName()
    {
        return "@UsersManager/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 12,  71 => 11,  67 => 10,  63 => 9,  59 => 8,  55 => 6,  51 => 5,  46 => 1,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'UsersManager_ManageAccess'|translate }}{% endset %}

{% block content %}

<piwik-users-manager
    initial-site-id=\"{{ idSiteSelected }}\"
    initial-site-name=\"{{ defaultReportSiteName|rawSafeDecoded }}\"
    current-user-role=\"'{{ currentUserRole }}'\"
    access-levels=\"{{ accessLevels|json_encode|e('html_attr') }}\"
    filter-access-levels=\"{{ filterAccessLevels|json_encode|e('html_attr') }}\"
>
</piwik-users-manager>

{% endblock %}
", "@UsersManager/index.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/UsersManager/templates/index.twig");
    }
}
