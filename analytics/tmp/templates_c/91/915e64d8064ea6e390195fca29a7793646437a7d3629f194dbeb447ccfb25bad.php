<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CoreAdminHome/trackingCodeGenerator.twig */
class __TwigTemplate_31e591e68dded9516b4f87cb7ca978c9f2bf552ee70122c82aed8da61a25e17f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_TrackingCode"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@CoreAdminHome/trackingCodeGenerator.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"plugins/CoreAdminHome/stylesheets/jsTrackingGenerator.css\" />
";
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "
    <div class=\"card\">
        <div class=\"card-content\">
            <h2 piwik-enriched-headline
                help-url=\"https://matomo.org/docs/tracking-api/\"
                rate=\"";
        // line 16
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_TrackingCode"]), "html_attr");
        echo "\">";
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_TrackingCode"]), "html", null, true);
        echo "</h2>
            <p style=\"padding-left: 0;\">";
        // line 17
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_TrackingCodeIntro"]), "html", null, true);
        echo "</p>
        </div>
        <div class=\"card-action\">
            ";
        // line 20
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_GoTo2"]), "html", null, true);
        echo ":
            <a href=\"#javaScriptTracking\">";
        // line 21
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JavaScriptTracking"]), "html", null, true);
        echo "</a>
            <a href=\"#imageTracking\">";
        // line 22
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTracking"]), "html", null, true);
        echo "</a>
            <a href=\"#importServerLogs\">";
        // line 23
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImportingServerLogs"]), "html", null, true);
        echo "</a>
            <a href=\"#mobileAppsAndSdks\">";
        // line 24
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SitesManager_MobileAppsAndSDKs"]), "html", null, true);
        echo "</a>
            <a href=\"#trackingApi\">";
        // line 25
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_HttpTrackingApi"]), "html", null, true);
        echo "</a>
            ";
        // line 26
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.endTrackingCodePageTableOfContents"]);
        echo "
        </div>
    </div>

    <input type=\"hidden\" name=\"numMaxCustomVariables\"
           value=\"";
        // line 31
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["maxCustomVariables"]) || array_key_exists("maxCustomVariables", $context) ? $context["maxCustomVariables"] : (function () { throw new RuntimeError('Variable "maxCustomVariables" does not exist.', 31, $this->source); })()), "html_attr");
        echo "\">

<div piwik-content-block
     anchor=\"javaScriptTracking\"
     content-title=\"";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JavaScriptTracking"]), "html_attr");
        echo "\">

    <div id=\"js-code-options\" ng-controller=\"JsTrackingCodeController as jsTrackingCode\">

        <p>
            ";
        // line 40
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro1"]), "html", null, true);
        echo "
            <br/><br/>
            ";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro2"]), "html", null, true);
        echo " ";
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro3a", "<a href=\"https://matomo.org/integrate/\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo " ";
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro3b"]);
        echo "
            <br/><br/>
            ";
        // line 44
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro4", "<a href=\"#image-tracking-link\">", "</a>"]);
        echo "
            <br/><br/>
            ";
        // line 46
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTrackingIntro5", "<a rel=\"noreferrer noopener\" target=\"_blank\" href=\"https://developer.matomo.org/guides/tracking-javascript-guide\">", "</a>"]);
        echo "
        </p>

        <div piwik-field uicontrol=\"site\" name=\"js-tracker-website\"
             class=\"jsTrackingCodeWebsite\"
             ng-model=\"jsTrackingCode.site\"
             ng-change=\"jsTrackingCode.changeSite(true)\"
             introduction=\"";
        // line 53
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Website"]), "html_attr");
        echo "\"
             value='";
        // line 54
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["defaultSite"]) || array_key_exists("defaultSite", $context) ? $context["defaultSite"] : (function () { throw new RuntimeError('Variable "defaultSite" does not exist.', 54, $this->source); })())), "html", null, true);
        echo "'>
        </div>

        <div id=\"optional-js-tracking-options\">

            ";
        // line 60
        echo "            <div id=\"jsTrackAllSubdomainsInlineHelp\" class=\"inline-help-node\">
                ";
        // line 61
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_MergeSubdomainsDesc", "x.<span class='current-site-host'></span>", "y.<span class='current-site-host'></span>"]);
        echo "
                ";
        // line 62
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_LearnMore", " (<a href=\"https://developer.matomo.org/guides/tracking-javascript-guide#measuring-domains-andor-sub-domains\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>)"]);
        echo "
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-all-subdomains\"
                 ng-model=\"jsTrackingCode.trackAllSubdomains\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 introduction=\"";
        // line 69
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Options"]), "html_attr");
        echo "\"
                 data-title=\"";
        // line 70
        echo \Piwik\piwik_escape_filter($this->env, (call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_MergeSubdomains"]) . " <span class='current-site-name'></span>"), "html_attr");
        echo "\"
                 value=\"\" inline-help=\"#jsTrackAllSubdomainsInlineHelp\">
            </div>

            ";
        // line 75
        echo "            <div id=\"jsTrackGroupByDomainInlineHelp\" class=\"inline-help-node\">
                ";
        // line 76
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_GroupPageTitlesByDomainDesc1", "<span class='current-site-host'></span>"]);
        echo "
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-group-by-domain\"
                 ng-model=\"jsTrackingCode.groupByDomain\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"";
        // line 83
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_GroupPageTitlesByDomain"]), "html_attr");
        echo "\"
                 value=\"\" inline-help=\"#jsTrackGroupByDomainInlineHelp\">
            </div>

            ";
        // line 88
        echo "            <div id=\"jsTrackAllAliasesInlineHelp\" class=\"inline-help-node\">
                ";
        // line 89
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_MergeAliasesDesc", "<span class='current-site-alias'></span>"]);
        echo "
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-all-aliases\"
                 ng-model=\"jsTrackingCode.trackAllAliases\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"";
        // line 96
        echo \Piwik\piwik_escape_filter($this->env, (call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_MergeAliases"]) . " <span class='current-site-name'></span>"), "html_attr");
        echo "\"
                 value=\"\" inline-help=\"#jsTrackAllAliasesInlineHelp\">
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-noscript\"
                 ng-model=\"jsTrackingCode.trackNoScript\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"";
        // line 104
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_TrackNoScript"]), "html_attr");
        echo "\"
                 value=\"\" inline-help=\"\">
            </div>

            <h3>";
        // line 108
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Mobile_Advanced"]), "html", null, true);
        echo "</h3>

            <p>
                <a href=\"javascript:;\"
                   ng-show=\"!jsTrackingCode.showAdvanced\"
                   ng-click=\"jsTrackingCode.showAdvanced = true\">";
        // line 113
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Show"]), "html", null, true);
        echo "</a>
                <a href=\"javascript:;\"
                   ng-show=\"jsTrackingCode.showAdvanced\"
                   ng-click=\"jsTrackingCode.showAdvanced = false\">";
        // line 116
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Hide"]), "html", null, true);
        echo "</a>
            </p>

            <div id=\"javascript-advanced-options\" ng-show=\"jsTrackingCode.showAdvanced\">

                ";
        // line 122
        echo "                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-visitor-cv-check\"
                     ng-model=\"jsTrackingCode.trackCustomVars\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     data-title=\"";
        // line 126
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_VisitorCustomVars"]), "html_attr");
        echo "\"
                     value=\"\" inline-help=\"";
        // line 127
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_VisitorCustomVarsDesc"]), "html_attr");
        echo "\">
                </div>

                <div id=\"javascript-tracking-visitor-cv\" ng-show=\"jsTrackingCode.trackCustomVars\">
                    <div class=\"row\">
                        <div class=\"col s12 m3\">
                            ";
        // line 133
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Name"]), "html", null, true);
        echo "
                        </div>
                        <div class=\"col s12 m3\">
                            ";
        // line 136
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Value"]), "html", null, true);
        echo "
                        </div>
                    </div>
                    <div class=\"row\" ng-repeat=\"customVar in jsTrackingCode.customVars\">
                        <div class=\"col s12 m6 l3\">
                            <input type=\"text\" class=\"custom-variable-name\"
                                   ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                   ng-model=\"jsTrackingCode.customVars[\$index.toString()].name\"
                                   placeholder=\"e.g. Type\"/>
                        </div>
                        <div class=\"col s12 m6 l3\">
                            <input type=\"text\" class=\"custom-variable-value\"
                                   ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                   ng-model=\"jsTrackingCode.customVars[\$index.toString()].value\"
                                   placeholder=\"e.g. Customer\"/>
                        </div>
                    </div>
                    <div class=\"row\" ng-show=\"jsTrackingCode.canAddMoreCustomVariables\">
                        <div class=\"col s12\">
                            <a href=\"javascript:;\"
                               ng-click=\"jsTrackingCode.addCustomVar()\"
                               class=\"add-custom-variable\"><span class=\"icon-add\"></span> ";
        // line 157
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Add"]), "html", null, true);
        echo "</a>
                        </div>
                    </div>
                </div>

                ";
        // line 163
        echo "                <div id=\"jsCrossDomain\" class=\"inline-help-node\">
                    ";
        // line 164
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CrossDomain"]), "html", null, true);
        echo "
                    <br/>
                    ";
        // line 166
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CrossDomain_NeedsMultipleDomains"]), "html", null, true);
        echo "
                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-cross-domain\"
                     ng-model=\"jsTrackingCode.crossDomain\"
                     ng-change=\"jsTrackingCode.updateTrackingCode();jsTrackingCode.onCrossDomainToggle();\"
                     data-disabled=\"jsTrackingCode.isLoading || !jsTrackingCode.hasManySiteUrls\"
                     data-title=\"";
        // line 173
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_EnableCrossDomainLinking"]), "html_attr");
        echo "\"
                     value=\"\" inline-help=\"#jsCrossDomain\">
                </div>

                ";
        // line 178
        echo "                <div id=\"jsDoNotTrackInlineHelp\" class=\"inline-help-node\">
                    ";
        // line 179
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_EnableDoNotTrackDesc"]), "html", null, true);
        echo "
                    ";
        // line 180
        if ((isset($context["serverSideDoNotTrackEnabled"]) || array_key_exists("serverSideDoNotTrackEnabled", $context) ? $context["serverSideDoNotTrackEnabled"] : (function () { throw new RuntimeError('Variable "serverSideDoNotTrackEnabled" does not exist.', 180, $this->source); })())) {
            // line 181
            echo "                        <br/>
                        ";
            // line 182
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_EnableDoNotTrack_AlreadyEnabled"]), "html", null, true);
            echo "
                    ";
        }
        // line 184
        echo "                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-do-not-track\"
                     ng-model=\"jsTrackingCode.doNotTrack\"
                     ng-change=\"jsTrackingCode.updateTrackingCode() \"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     data-title=\"";
        // line 190
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_EnableDoNotTrack"]), "html_attr");
        echo "\"
                     value=\"\" inline-help=\"#jsDoNotTrackInlineHelp\">
                </div>

                ";
        // line 195
        echo "                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-disable-cookies\"
                     ng-model=\"jsTrackingCode.disableCookies\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-title=\"";
        // line 199
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_DisableCookies"]), "html_attr");
        echo "\"
                     value=\"\" inline-help=\"";
        // line 200
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_DisableCookiesDesc"]), "html_attr");
        echo "\">
                </div>

                ";
        // line 204
        echo "                <div id=\"jsTrackCampaignParamsInlineHelp\" class=\"inline-help-node\">
                    ";
        // line 205
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CustomCampaignQueryParamDesc", "<a href=\"https://matomo.org/faq/general/#faq_119\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo "
                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"custom-campaign-query-params-check\"
                     ng-model=\"jsTrackingCode.useCustomCampaignParams\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-title=\"";
        // line 212
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CustomCampaignQueryParam"]), "html_attr");
        echo "\"
                     value=\"\" inline-help=\"#jsTrackCampaignParamsInlineHelp\">
                </div>

                <div ng-show=\"jsTrackingCode.useCustomCampaignParams\" id=\"js-campaign-query-param-extra\">
                    <div class=\"row\">
                        <div class=\"col s12\">
                            <div piwik-field uicontrol=\"text\" name=\"custom-campaign-name-query-param\"
                                 ng-model=\"jsTrackingCode.customCampaignName\"
                                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                 data-disabled=\"jsTrackingCode.isLoading\"
                                 data-title=\"";
        // line 223
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CampaignNameParam"]), "html_attr");
        echo "\"
                                 value=\"\">
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col s12\">
                            <div piwik-field uicontrol=\"text\" name=\"custom-campaign-keyword-query-param\"
                                 ng-model=\"jsTrackingCode.customCampaignKeyword\"
                                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                 data-disabled=\"jsTrackingCode.isLoading\"
                                 data-title=\"";
        // line 234
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CampaignKwdParam"]), "html_attr");
        echo "\"
                                 value=\"\">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div id=\"javascript-output-section\">
            <div class=\"valign-wrapper trackingHelpHeader matchWidth\">
                <div>
                    <h3>";
        // line 248
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_JsTrackingTag"]), "html", null, true);
        echo "</h3>

                    <p>";
        // line 250
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_JSTracking_CodeNoteBeforeClosingHead", "&lt;/head&gt;"]);
        echo "</p>
                </div>

                <button class=\"btn\" id=\"emailJsBtn\" ng-click=\"jsTrackingCode.sendEmail()\">
                    ";
        // line 254
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SitesManager_EmailInstructionsButton"]), "html", null, true);
        echo "
                </button>
            </div>
            <div id=\"javascript-text\">
                <pre piwik-select-on-focus class=\"codeblock\"
                     ng-bind=\"jsTrackingCode.trackingCode\"> </pre>
            </div>

        </div>
    </div>
</div>

<div piwik-content-block content-title=\"";
        // line 266
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTracking"]), "html_attr");
        echo "\"
     anchor=\"imageTracking\">
    <a name=\"image-tracking-link\"></a>

    <div id=\"image-tracking-code-options\" ng-controller=\"ImageTrackingCodeController as imageTrackingCode\">

        <p>
            ";
        // line 273
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTrackingIntro1"]), "html", null, true);
        echo " ";
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTrackingIntro2", "<code>&lt;noscript&gt;&lt;/noscript&gt;</code>"]);
        echo "
        </p>
        <p>
            ";
        // line 276
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTrackingIntro3", "<a href=\"https://matomo.org/docs/tracking-api/reference/\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo "
        </p>

        ";
        // line 280
        echo "        <div piwik-field uicontrol=\"site\" name=\"image-tracker-website\"
             ng-model=\"imageTrackingCode.site\"
             ng-change=\"imageTrackingCode.changeSite(true)\"
             introduction=\"";
        // line 283
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Website"]), "html_attr");
        echo "\"
             value='";
        // line 284
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["defaultSite"]) || array_key_exists("defaultSite", $context) ? $context["defaultSite"] : (function () { throw new RuntimeError('Variable "defaultSite" does not exist.', 284, $this->source); })())), "html", null, true);
        echo "'>
        </div>

        ";
        // line 288
        echo "        <div piwik-field uicontrol=\"text\" name=\"image-tracker-action-name\"
             ng-model=\"imageTrackingCode.pageName\"
             ng-change=\"imageTrackingCode.updateTrackingCode()\"
             data-disabled=\"imageTrackingCode.isLoading\"
             introduction=\"";
        // line 292
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Options"]), "html_attr");
        echo "\"
             data-title=\"";
        // line 293
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["Actions_ColumnPageName"]), "html_attr");
        echo "\"
             value=\"\">
        </div>

        ";
        // line 298
        echo "        <div piwik-field uicontrol=\"checkbox\" name=\"image-tracking-goal-check\"
             ng-model=\"imageTrackingCode.trackGoal\"
             ng-change=\"imageTrackingCode.updateTrackingCode()\"
             data-disabled=\"imageTrackingCode.isLoading\"
             data-title=\"";
        // line 302
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_TrackAGoal"]), "html_attr");
        echo "\"
             value=\"\">
        </div>

        <div ng-show=\"imageTrackingCode.trackGoal\"
             id=\"image-tracking-goal-sub\">
            <div class=\"row\">
                <div class=\"col s12 m6\">
                    <div piwik-field uicontrol=\"select\" name=\"image-tracker-goal\"
                         options=\"imageTrackingCode.allGoals\"
                         data-disabled=\"imageTrackingCode.isLoading\"
                         ng-model=\"imageTrackingCode.trackIdGoal\"
                         full-width=\"true\"
                         ng-change=\"imageTrackingCode.updateTrackingCode()\"
                         value=\"\">
                    </div>
                </div>
                <div class=\"col s12 m6\">
                    <div piwik-field uicontrol=\"text\" name=\"image-revenue\"
                         ng-model=\"imageTrackingCode.revenue\"
                         ng-change=\"imageTrackingCode.updateTrackingCode()\"
                         data-disabled=\"imageTrackingCode.isLoading\"
                         full-width=\"true\"
                         data-title=\"";
        // line 325
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_WithOptionalRevenue"]), "html_attr");
        echo " <span class='site-currency'></span>\"
                         value=\"\">
                    </div>
                </div>
            </div>
        </div>

        <div id=\"image-link-output-section\">
            <h3>";
        // line 333
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImageTrackingLink"]), "html", null, true);
        echo "</h3>

            <div id=\"image-tracking-text\">
                <pre piwik-select-on-focus
                          ng-bind=\"imageTrackingCode.trackingCode\"> </pre>
            </div>
        </div>
    </div>
</div>

<div piwik-content-block content-title=\"";
        // line 343
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImportingServerLogs"]), "html_attr");
        echo "\"
     anchor=\"importServerLogs\">
    <p>
        ";
        // line 346
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ImportingServerLogsDesc", "<a href=\"https://matomo.org/log-analytics/\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo "
    </p>
</div>

<div piwik-content-block content-title=\"";
        // line 350
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SitesManager_MobileAppsAndSDKs"]), "html", null, true);
        echo "\" anchor=\"mobileAppsAndSdks\">
    <p>";
        // line 351
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["SitesManager_MobileAppsAndSDKsDescription", "<a href=\"https://matomo.org/integrate/#programming-language-platforms-and-frameworks\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo "</p>
</div>

<div piwik-content-block content-title=\"";
        // line 354
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_HttpTrackingApi"]), "html", null, true);
        echo "\" anchor=\"trackingApi\">
    <p>";
        // line 355
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_HttpTrackingApiDescription", "<a href=\"https://developer.matomo.org/api-reference/tracking-api\" rel=\"noreferrer noopener\" target=\"_blank\">", "</a>"]);
        echo "</p>
</div>

";
        // line 358
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), ["Template.endTrackingCodePage"]);
        echo "

";
    }

    public function getTemplateName()
    {
        return "@CoreAdminHome/trackingCodeGenerator.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  636 => 358,  630 => 355,  626 => 354,  620 => 351,  616 => 350,  609 => 346,  603 => 343,  590 => 333,  579 => 325,  553 => 302,  547 => 298,  540 => 293,  536 => 292,  530 => 288,  524 => 284,  520 => 283,  515 => 280,  509 => 276,  501 => 273,  491 => 266,  476 => 254,  469 => 250,  464 => 248,  447 => 234,  433 => 223,  419 => 212,  409 => 205,  406 => 204,  400 => 200,  396 => 199,  390 => 195,  383 => 190,  375 => 184,  370 => 182,  367 => 181,  365 => 180,  361 => 179,  358 => 178,  351 => 173,  341 => 166,  336 => 164,  333 => 163,  325 => 157,  301 => 136,  295 => 133,  286 => 127,  282 => 126,  276 => 122,  268 => 116,  262 => 113,  254 => 108,  247 => 104,  236 => 96,  226 => 89,  223 => 88,  216 => 83,  206 => 76,  203 => 75,  196 => 70,  192 => 69,  182 => 62,  178 => 61,  175 => 60,  167 => 54,  163 => 53,  153 => 46,  148 => 44,  139 => 42,  134 => 40,  126 => 35,  119 => 31,  111 => 26,  107 => 25,  103 => 24,  99 => 23,  95 => 22,  91 => 21,  87 => 20,  81 => 17,  75 => 16,  68 => 11,  64 => 10,  56 => 4,  52 => 3,  47 => 1,  43 => 8,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% block head %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"plugins/CoreAdminHome/stylesheets/jsTrackingGenerator.css\" />
{% endblock %}

{% set title %}{{ 'CoreAdminHome_TrackingCode'|translate }}{% endset %}

{% block content %}

    <div class=\"card\">
        <div class=\"card-content\">
            <h2 piwik-enriched-headline
                help-url=\"https://matomo.org/docs/tracking-api/\"
                rate=\"{{ 'CoreAdminHome_TrackingCode'|translate|e('html_attr') }}\">{{ 'CoreAdminHome_TrackingCode'|translate  }}</h2>
            <p style=\"padding-left: 0;\">{{ 'CoreAdminHome_TrackingCodeIntro'|translate }}</p>
        </div>
        <div class=\"card-action\">
            {{ 'General_GoTo2'|translate }}:
            <a href=\"#javaScriptTracking\">{{ 'CoreAdminHome_JavaScriptTracking'|translate  }}</a>
            <a href=\"#imageTracking\">{{ 'CoreAdminHome_ImageTracking'|translate }}</a>
            <a href=\"#importServerLogs\">{{ 'CoreAdminHome_ImportingServerLogs'|translate }}</a>
            <a href=\"#mobileAppsAndSdks\">{{ 'SitesManager_MobileAppsAndSDKs'|translate }}</a>
            <a href=\"#trackingApi\">{{ 'CoreAdminHome_HttpTrackingApi'|translate }}</a>
            {{ postEvent('Template.endTrackingCodePageTableOfContents') }}
        </div>
    </div>

    <input type=\"hidden\" name=\"numMaxCustomVariables\"
           value=\"{{ maxCustomVariables|e('html_attr') }}\">

<div piwik-content-block
     anchor=\"javaScriptTracking\"
     content-title=\"{{ 'CoreAdminHome_JavaScriptTracking'|translate|e('html_attr') }}\">

    <div id=\"js-code-options\" ng-controller=\"JsTrackingCodeController as jsTrackingCode\">

        <p>
            {{ 'CoreAdminHome_JSTrackingIntro1'|translate }}
            <br/><br/>
            {{ 'CoreAdminHome_JSTrackingIntro2'|translate }} {{ 'CoreAdminHome_JSTrackingIntro3a'|translate('<a href=\"https://matomo.org/integrate/\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }} {{ 'CoreAdminHome_JSTrackingIntro3b'|translate|raw }}
            <br/><br/>
            {{ 'CoreAdminHome_JSTrackingIntro4'|translate('<a href=\"#image-tracking-link\">','</a>')|raw }}
            <br/><br/>
            {{ 'CoreAdminHome_JSTrackingIntro5'|translate('<a rel=\"noreferrer noopener\" target=\"_blank\" href=\"https://developer.matomo.org/guides/tracking-javascript-guide\">','</a>')|raw }}
        </p>

        <div piwik-field uicontrol=\"site\" name=\"js-tracker-website\"
             class=\"jsTrackingCodeWebsite\"
             ng-model=\"jsTrackingCode.site\"
             ng-change=\"jsTrackingCode.changeSite(true)\"
             introduction=\"{{ 'General_Website'|translate|e('html_attr') }}\"
             value='{{ defaultSite|json_encode }}'>
        </div>

        <div id=\"optional-js-tracking-options\">

            {# track across all subdomains #}
            <div id=\"jsTrackAllSubdomainsInlineHelp\" class=\"inline-help-node\">
                {{ 'CoreAdminHome_JSTracking_MergeSubdomainsDesc'|translate(\"x.<span class='current-site-host'></span>\",\"y.<span class='current-site-host'></span>\")|raw }}
                {{ 'General_LearnMore'|translate(' (<a href=\"https://developer.matomo.org/guides/tracking-javascript-guide#measuring-domains-andor-sub-domains\" rel=\"noreferrer noopener\" target=\"_blank\">', '</a>)')|raw }}
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-all-subdomains\"
                 ng-model=\"jsTrackingCode.trackAllSubdomains\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 introduction=\"{{ 'General_Options'|translate|e('html_attr') }}\"
                 data-title=\"{{ ('CoreAdminHome_JSTracking_MergeSubdomains'|translate ~ \" <span class='current-site-name'></span>\")|e('html_attr') }}\"
                 value=\"\" inline-help=\"#jsTrackAllSubdomainsInlineHelp\">
            </div>

            {# group page titles by site domain #}
            <div id=\"jsTrackGroupByDomainInlineHelp\" class=\"inline-help-node\">
                {{ 'CoreAdminHome_JSTracking_GroupPageTitlesByDomainDesc1'|translate(\"<span class='current-site-host'></span>\")|raw }}
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-group-by-domain\"
                 ng-model=\"jsTrackingCode.groupByDomain\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"{{ 'CoreAdminHome_JSTracking_GroupPageTitlesByDomain'|translate|e('html_attr') }}\"
                 value=\"\" inline-help=\"#jsTrackGroupByDomainInlineHelp\">
            </div>

            {# track across all site aliases #}
            <div id=\"jsTrackAllAliasesInlineHelp\" class=\"inline-help-node\">
                {{ 'CoreAdminHome_JSTracking_MergeAliasesDesc'|translate(\"<span class='current-site-alias'></span>\")|raw }}
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-all-aliases\"
                 ng-model=\"jsTrackingCode.trackAllAliases\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"{{ ('CoreAdminHome_JSTracking_MergeAliases'|translate ~ \" <span class='current-site-name'></span>\")|e('html_attr') }}\"
                 value=\"\" inline-help=\"#jsTrackAllAliasesInlineHelp\">
            </div>

            <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-noscript\"
                 ng-model=\"jsTrackingCode.trackNoScript\"
                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                 data-disabled=\"jsTrackingCode.isLoading\"
                 data-title=\"{{ 'CoreAdminHome_JSTracking_TrackNoScript'|translate|e('html_attr') }}\"
                 value=\"\" inline-help=\"\">
            </div>

            <h3>{{ 'Mobile_Advanced'|translate }}</h3>

            <p>
                <a href=\"javascript:;\"
                   ng-show=\"!jsTrackingCode.showAdvanced\"
                   ng-click=\"jsTrackingCode.showAdvanced = true\">{{ 'General_Show'|translate }}</a>
                <a href=\"javascript:;\"
                   ng-show=\"jsTrackingCode.showAdvanced\"
                   ng-click=\"jsTrackingCode.showAdvanced = false\">{{ 'General_Hide'|translate }}</a>
            </p>

            <div id=\"javascript-advanced-options\" ng-show=\"jsTrackingCode.showAdvanced\">

                {# visitor custom variable #}
                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-visitor-cv-check\"
                     ng-model=\"jsTrackingCode.trackCustomVars\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     data-title=\"{{ 'CoreAdminHome_JSTracking_VisitorCustomVars'|translate|e('html_attr') }}\"
                     value=\"\" inline-help=\"{{ 'CoreAdminHome_JSTracking_VisitorCustomVarsDesc'|translate|e('html_attr') }}\">
                </div>

                <div id=\"javascript-tracking-visitor-cv\" ng-show=\"jsTrackingCode.trackCustomVars\">
                    <div class=\"row\">
                        <div class=\"col s12 m3\">
                            {{ 'General_Name'|translate }}
                        </div>
                        <div class=\"col s12 m3\">
                            {{ 'General_Value'|translate }}
                        </div>
                    </div>
                    <div class=\"row\" ng-repeat=\"customVar in jsTrackingCode.customVars\">
                        <div class=\"col s12 m6 l3\">
                            <input type=\"text\" class=\"custom-variable-name\"
                                   ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                   ng-model=\"jsTrackingCode.customVars[\$index.toString()].name\"
                                   placeholder=\"e.g. Type\"/>
                        </div>
                        <div class=\"col s12 m6 l3\">
                            <input type=\"text\" class=\"custom-variable-value\"
                                   ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                   ng-model=\"jsTrackingCode.customVars[\$index.toString()].value\"
                                   placeholder=\"e.g. Customer\"/>
                        </div>
                    </div>
                    <div class=\"row\" ng-show=\"jsTrackingCode.canAddMoreCustomVariables\">
                        <div class=\"col s12\">
                            <a href=\"javascript:;\"
                               ng-click=\"jsTrackingCode.addCustomVar()\"
                               class=\"add-custom-variable\"><span class=\"icon-add\"></span> {{ 'General_Add'|translate }}</a>
                        </div>
                    </div>
                </div>

                {# cross domain support #}
                <div id=\"jsCrossDomain\" class=\"inline-help-node\">
                    {{ \"CoreAdminHome_JSTracking_CrossDomain\"|translate }}
                    <br/>
                    {{ 'CoreAdminHome_JSTracking_CrossDomain_NeedsMultipleDomains'|translate }}
                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-cross-domain\"
                     ng-model=\"jsTrackingCode.crossDomain\"
                     ng-change=\"jsTrackingCode.updateTrackingCode();jsTrackingCode.onCrossDomainToggle();\"
                     data-disabled=\"jsTrackingCode.isLoading || !jsTrackingCode.hasManySiteUrls\"
                     data-title=\"{{ 'CoreAdminHome_JSTracking_EnableCrossDomainLinking'|translate|e('html_attr') }}\"
                     value=\"\" inline-help=\"#jsCrossDomain\">
                </div>

                {# do not track support #}
                <div id=\"jsDoNotTrackInlineHelp\" class=\"inline-help-node\">
                    {{ 'CoreAdminHome_JSTracking_EnableDoNotTrackDesc'|translate }}
                    {% if serverSideDoNotTrackEnabled %}
                        <br/>
                        {{ 'CoreAdminHome_JSTracking_EnableDoNotTrack_AlreadyEnabled'|translate }}
                    {% endif %}
                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-do-not-track\"
                     ng-model=\"jsTrackingCode.doNotTrack\"
                     ng-change=\"jsTrackingCode.updateTrackingCode() \"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     data-title=\"{{ 'CoreAdminHome_JSTracking_EnableDoNotTrack'|translate|e('html_attr') }}\"
                     value=\"\" inline-help=\"#jsDoNotTrackInlineHelp\">
                </div>

                {# disable all cookies options #}
                <div piwik-field uicontrol=\"checkbox\" name=\"javascript-tracking-disable-cookies\"
                     ng-model=\"jsTrackingCode.disableCookies\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-title=\"{{ 'CoreAdminHome_JSTracking_DisableCookies'|translate|e('html_attr') }}\"
                     value=\"\" inline-help=\"{{ 'CoreAdminHome_JSTracking_DisableCookiesDesc'|translate|e('html_attr') }}\">
                </div>

                {# custom campaign name/keyword query params #}
                <div id=\"jsTrackCampaignParamsInlineHelp\" class=\"inline-help-node\">
                    {{ 'CoreAdminHome_JSTracking_CustomCampaignQueryParamDesc'|translate('<a href=\"https://matomo.org/faq/general/#faq_119\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }}
                </div>

                <div piwik-field uicontrol=\"checkbox\" name=\"custom-campaign-query-params-check\"
                     ng-model=\"jsTrackingCode.useCustomCampaignParams\"
                     data-disabled=\"jsTrackingCode.isLoading\"
                     ng-change=\"jsTrackingCode.updateTrackingCode()\"
                     data-title=\"{{ 'CoreAdminHome_JSTracking_CustomCampaignQueryParam'|translate|e('html_attr') }}\"
                     value=\"\" inline-help=\"#jsTrackCampaignParamsInlineHelp\">
                </div>

                <div ng-show=\"jsTrackingCode.useCustomCampaignParams\" id=\"js-campaign-query-param-extra\">
                    <div class=\"row\">
                        <div class=\"col s12\">
                            <div piwik-field uicontrol=\"text\" name=\"custom-campaign-name-query-param\"
                                 ng-model=\"jsTrackingCode.customCampaignName\"
                                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                 data-disabled=\"jsTrackingCode.isLoading\"
                                 data-title=\"{{ 'CoreAdminHome_JSTracking_CampaignNameParam'|translate|e('html_attr') }}\"
                                 value=\"\">
                            </div>
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col s12\">
                            <div piwik-field uicontrol=\"text\" name=\"custom-campaign-keyword-query-param\"
                                 ng-model=\"jsTrackingCode.customCampaignKeyword\"
                                 ng-change=\"jsTrackingCode.updateTrackingCode()\"
                                 data-disabled=\"jsTrackingCode.isLoading\"
                                 data-title=\"{{ 'CoreAdminHome_JSTracking_CampaignKwdParam'|translate|e('html_attr') }}\"
                                 value=\"\">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div id=\"javascript-output-section\">
            <div class=\"valign-wrapper trackingHelpHeader matchWidth\">
                <div>
                    <h3>{{ 'General_JsTrackingTag'|translate }}</h3>

                    <p>{{ 'CoreAdminHome_JSTracking_CodeNoteBeforeClosingHead'|translate(\"&lt;/head&gt;\")|raw }}</p>
                </div>

                <button class=\"btn\" id=\"emailJsBtn\" ng-click=\"jsTrackingCode.sendEmail()\">
                    {{ 'SitesManager_EmailInstructionsButton'|translate }}
                </button>
            </div>
            <div id=\"javascript-text\">
                <pre piwik-select-on-focus class=\"codeblock\"
                     ng-bind=\"jsTrackingCode.trackingCode\"> </pre>
            </div>

        </div>
    </div>
</div>

<div piwik-content-block content-title=\"{{ 'CoreAdminHome_ImageTracking'|translate|e('html_attr') }}\"
     anchor=\"imageTracking\">
    <a name=\"image-tracking-link\"></a>

    <div id=\"image-tracking-code-options\" ng-controller=\"ImageTrackingCodeController as imageTrackingCode\">

        <p>
            {{ 'CoreAdminHome_ImageTrackingIntro1'|translate }} {{ 'CoreAdminHome_ImageTrackingIntro2'|translate(\"<code>&lt;noscript&gt;&lt;/noscript&gt;</code>\")|raw }}
        </p>
        <p>
            {{ 'CoreAdminHome_ImageTrackingIntro3'|translate('<a href=\"https://matomo.org/docs/tracking-api/reference/\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }}
        </p>

        {# website #}
        <div piwik-field uicontrol=\"site\" name=\"image-tracker-website\"
             ng-model=\"imageTrackingCode.site\"
             ng-change=\"imageTrackingCode.changeSite(true)\"
             introduction=\"{{ 'General_Website'|translate|e('html_attr') }}\"
             value='{{ defaultSite|json_encode }}'>
        </div>

        {# action_name #}
        <div piwik-field uicontrol=\"text\" name=\"image-tracker-action-name\"
             ng-model=\"imageTrackingCode.pageName\"
             ng-change=\"imageTrackingCode.updateTrackingCode()\"
             data-disabled=\"imageTrackingCode.isLoading\"
             introduction=\"{{ 'General_Options'|translate|e('html_attr') }}\"
             data-title=\"{{ 'Actions_ColumnPageName'|translate|e('html_attr') }}\"
             value=\"\">
        </div>

        {# goal #}
        <div piwik-field uicontrol=\"checkbox\" name=\"image-tracking-goal-check\"
             ng-model=\"imageTrackingCode.trackGoal\"
             ng-change=\"imageTrackingCode.updateTrackingCode()\"
             data-disabled=\"imageTrackingCode.isLoading\"
             data-title=\"{{ 'CoreAdminHome_TrackAGoal'|translate|e('html_attr') }}\"
             value=\"\">
        </div>

        <div ng-show=\"imageTrackingCode.trackGoal\"
             id=\"image-tracking-goal-sub\">
            <div class=\"row\">
                <div class=\"col s12 m6\">
                    <div piwik-field uicontrol=\"select\" name=\"image-tracker-goal\"
                         options=\"imageTrackingCode.allGoals\"
                         data-disabled=\"imageTrackingCode.isLoading\"
                         ng-model=\"imageTrackingCode.trackIdGoal\"
                         full-width=\"true\"
                         ng-change=\"imageTrackingCode.updateTrackingCode()\"
                         value=\"\">
                    </div>
                </div>
                <div class=\"col s12 m6\">
                    <div piwik-field uicontrol=\"text\" name=\"image-revenue\"
                         ng-model=\"imageTrackingCode.revenue\"
                         ng-change=\"imageTrackingCode.updateTrackingCode()\"
                         data-disabled=\"imageTrackingCode.isLoading\"
                         full-width=\"true\"
                         data-title=\"{{ 'CoreAdminHome_WithOptionalRevenue'|translate|e('html_attr') }} <span class='site-currency'></span>\"
                         value=\"\">
                    </div>
                </div>
            </div>
        </div>

        <div id=\"image-link-output-section\">
            <h3>{{ 'CoreAdminHome_ImageTrackingLink'|translate }}</h3>

            <div id=\"image-tracking-text\">
                <pre piwik-select-on-focus
                          ng-bind=\"imageTrackingCode.trackingCode\"> </pre>
            </div>
        </div>
    </div>
</div>

<div piwik-content-block content-title=\"{{ 'CoreAdminHome_ImportingServerLogs'|translate|e('html_attr') }}\"
     anchor=\"importServerLogs\">
    <p>
        {{ 'CoreAdminHome_ImportingServerLogsDesc'|translate('<a href=\"https://matomo.org/log-analytics/\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }}
    </p>
</div>

<div piwik-content-block content-title=\"{{ 'SitesManager_MobileAppsAndSDKs'|translate }}\" anchor=\"mobileAppsAndSdks\">
    <p>{{ 'SitesManager_MobileAppsAndSDKsDescription'|translate('<a href=\"https://matomo.org/integrate/#programming-language-platforms-and-frameworks\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }}</p>
</div>

<div piwik-content-block content-title=\"{{ 'CoreAdminHome_HttpTrackingApi'|translate }}\" anchor=\"trackingApi\">
    <p>{{ 'CoreAdminHome_HttpTrackingApiDescription'|translate('<a href=\"https://developer.matomo.org/api-reference/tracking-api\" rel=\"noreferrer noopener\" target=\"_blank\">','</a>')|raw }}</p>
</div>

{{ postEvent('Template.endTrackingCodePage') }}

{% endblock %}
", "@CoreAdminHome/trackingCodeGenerator.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/CoreAdminHome/templates/trackingCodeGenerator.twig");
    }
}
