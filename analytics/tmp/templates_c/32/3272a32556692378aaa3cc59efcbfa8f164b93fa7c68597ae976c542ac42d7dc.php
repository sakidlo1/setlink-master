<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @CoreAdminHome/generalSettings.twig */
class __TwigTemplate_64096b53c266dfded8f42bf822b75113d9a8bd3199a631be7eb4de0908be11c7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_MenuGeneralSettings"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@CoreAdminHome/generalSettings.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        $macros["piwik"] = $this->loadTemplate("macros.twig", "@CoreAdminHome/generalSettings.twig", 7)->unwrap();
        // line 8
        echo "    ";
        $macros["ajax"] = $this->loadTemplate("ajaxMacros.twig", "@CoreAdminHome/generalSettings.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        echo twig_call_macro($macros["ajax"], "macro_errorDiv", [], 10, $context, $this->getSourceContext());
        echo "
    ";
        // line 11
        echo twig_call_macro($macros["ajax"], "macro_loadingDiv", [], 11, $context, $this->getSourceContext());
        echo "

";
        // line 13
        if ((isset($context["isGeneralSettingsAdminEnabled"]) || array_key_exists("isGeneralSettingsAdminEnabled", $context) ? $context["isGeneralSettingsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isGeneralSettingsAdminEnabled" does not exist.', 13, $this->source); })())) {
            // line 14
            echo "    <div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_ArchivingSettings"]), "html_attr");
            echo "\">
        <div ng-controller=\"ArchivingController as archivingSettings\">
            <div class=\"form-group row\">
                <h3 class=\"col s12\">";
            // line 17
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_AllowPiwikArchivingToTriggerBrowser"]), "html", null, true);
            echo "</h3>
                <div class=\"col s12 m6\">
                    <p>
                        <label>
                            <input type=\"radio\" value=\"1\" id=\"enableBrowserTriggerArchiving1\"
                                   name=\"enableBrowserTriggerArchiving\" ";
            // line 22
            if ((0 === twig_compare((isset($context["enableBrowserTriggerArchiving"]) || array_key_exists("enableBrowserTriggerArchiving", $context) ? $context["enableBrowserTriggerArchiving"] : (function () { throw new RuntimeError('Variable "enableBrowserTriggerArchiving" does not exist.', 22, $this->source); })()), 1))) {
                echo " checked=\"checked\"";
            }
            // line 23
            echo "                            />
                            <span>";
            // line 24
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Yes"]), "html", null, true);
            echo "</span>
                            <span class=\"form-description\">";
            // line 25
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Default"]), "html", null, true);
            echo "</span>
                        </label>
                    </p>

                    <p>
                    <label for=\"enableBrowserTriggerArchiving2\">
                        <input type=\"radio\" value=\"0\"
                               id=\"enableBrowserTriggerArchiving2\"
                               name=\"enableBrowserTriggerArchiving\"
                                ";
            // line 34
            if ((0 === twig_compare((isset($context["enableBrowserTriggerArchiving"]) || array_key_exists("enableBrowserTriggerArchiving", $context) ? $context["enableBrowserTriggerArchiving"] : (function () { throw new RuntimeError('Variable "enableBrowserTriggerArchiving" does not exist.', 34, $this->source); })()), 0))) {
                echo " checked=\"checked\"";
            }
            echo " />
                        <span>";
            // line 35
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_No"]), "html", null, true);
            echo "</span>
                        <span class=\"form-description\">";
            // line 36
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ArchivingTriggerDescription", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/setup-auto-archiving/'>", "</a>"]);
            echo "</span>
                    </label>
                    </p>
                </div><div class=\"col s12 m6\">
                    <div class=\"form-help\">
                        ";
            // line 41
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ArchivingInlineHelp"]), "html", null, true);
            echo "
                        <br/>
                        ";
            // line 43
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SeeTheOfficialDocumentationForMoreInformation", "<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/setup-auto-archiving/'>", "</a>"]);
            echo "
                    </div>
                </div>
            </div>

            <div class=\"form-group row\">
                <h3 class=\"col s12\">
                    ";
            // line 50
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ReportsContainingTodayWillBeProcessedAtMostEvery"]), "html", null, true);
            echo "
                </h3>
                <div class=\"input-field col s12 m6\">
                    <input  type=\"text\" value='";
            // line 53
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["todayArchiveTimeToLive"]) || array_key_exists("todayArchiveTimeToLive", $context) ? $context["todayArchiveTimeToLive"] : (function () { throw new RuntimeError('Variable "todayArchiveTimeToLive" does not exist.', 53, $this->source); })()), "html_attr");
            echo "' id='todayArchiveTimeToLive' ";
            if ( !(isset($context["isGeneralSettingsAdminEnabled"]) || array_key_exists("isGeneralSettingsAdminEnabled", $context) ? $context["isGeneralSettingsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isGeneralSettingsAdminEnabled" does not exist.', 53, $this->source); })())) {
                echo "disabled=\"disabled\"";
            }
            echo " />
                    <span class=\"form-description\">
                        ";
            // line 55
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_RearchiveTimeIntervalOnlyForTodayReports"]), "html", null, true);
            echo "
                    </span>
                </div>
                <div class=\"col s12 m6\">
                    ";
            // line 59
            if ((isset($context["isGeneralSettingsAdminEnabled"]) || array_key_exists("isGeneralSettingsAdminEnabled", $context) ? $context["isGeneralSettingsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isGeneralSettingsAdminEnabled" does not exist.', 59, $this->source); })())) {
                // line 60
                echo "                        <div class=\"form-help\">
                            ";
                // line 61
                if ((isset($context["showWarningCron"]) || array_key_exists("showWarningCron", $context) ? $context["showWarningCron"] : (function () { throw new RuntimeError('Variable "showWarningCron" does not exist.', 61, $this->source); })())) {
                    // line 62
                    echo "                                <strong>
                                    ";
                    // line 63
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_NewReportsWillBeProcessedByCron"]), "html", null, true);
                    echo "<br/>
                                    ";
                    // line 64
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_ReportsWillBeProcessedAtMostEveryHour"]), "html", null, true);
                    echo "
                                    ";
                    // line 65
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_IfArchivingIsFastYouCanSetupCronRunMoreOften"]), "html", null, true);
                    echo "<br/>
                                </strong>
                            ";
                }
                // line 68
                echo "                            ";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmallTrafficYouCanLeaveDefault", (isset($context["todayArchiveTimeToLiveDefault"]) || array_key_exists("todayArchiveTimeToLiveDefault", $context) ? $context["todayArchiveTimeToLiveDefault"] : (function () { throw new RuntimeError('Variable "todayArchiveTimeToLiveDefault" does not exist.', 68, $this->source); })())]), "html", null, true);
                echo "
                            <br/>
                            ";
                // line 70
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_MediumToHighTrafficItIsRecommendedTo", 1800, 3600]), "html", null, true);
                echo "
                        </div>
                    ";
            }
            // line 73
            echo "                </div>
            </div>

            <div onconfirm=\"archivingSettings.save()\" saving=\"archivingSettings.isLoading\" piwik-save-button></div>
        </div>
    </div>

    ";
            // line 80
            if ( !(isset($context["isMultiServerEnvironment"]) || array_key_exists("isMultiServerEnvironment", $context) ? $context["isMultiServerEnvironment"] : (function () { throw new RuntimeError('Variable "isMultiServerEnvironment" does not exist.', 80, $this->source); })())) {
                // line 81
                echo "        <div piwik-content-block content-title=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_EmailServerSettings"]), "html_attr");
                echo "\">

        <div piwik-form ng-controller=\"MailSmtpController as mailSettings\">
            <div piwik-field uicontrol=\"checkbox\" name=\"mailUseSmtp\"
                 ng-model=\"mailSettings.enabled\"
                 data-title=\"";
                // line 86
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_UseSMTPServerForEmail"]), "html_attr");
                echo "\"
                 value=\"";
                // line 87
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 87, $this->source); })()), "transport", [], "any", false, false, false, 87), "smtp"))) {
                    echo "1";
                }
                echo "\"
                 inline-help=\"";
                // line 88
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SelectYesIfYouWantToSendEmailsViaServer"]), "html_attr");
                echo "\">
            </div>

            <div id=\"smtpSettings\"
                 ng-show=\"mailSettings.enabled\">

                <div piwik-field uicontrol=\"text\" name=\"mailHost\"
                     ng-model=\"mailSettings.mailHost\"
                     data-title=\"";
                // line 96
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpServerAddress"]), "html_attr");
                echo "\"
                     value=\"";
                // line 97
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 97, $this->source); })()), "host", [], "any", false, false, false, 97), "html_attr");
                echo "\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailPort\"
                     ng-model=\"mailSettings.mailPort\"
                     data-title=\"";
                // line 102
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpPort"]), "html_attr");
                echo "\"
                     value=\"";
                // line 103
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 103, $this->source); })()), "port", [], "any", false, false, false, 103), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OptionalSmtpPort"]), "html_attr");
                echo "\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"mailType\"
                     ng-model=\"mailSettings.mailType\"
                     data-title=\"";
                // line 108
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_AuthenticationMethodSmtp"]), "html_attr");
                echo "\"
                     options=\"";
                // line 109
                echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["mailTypes"]) || array_key_exists("mailTypes", $context) ? $context["mailTypes"] : (function () { throw new RuntimeError('Variable "mailTypes" does not exist.', 109, $this->source); })())), "html", null, true);
                echo "\"
                     value=\"";
                // line 110
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 110, $this->source); })()), "type", [], "any", false, false, false, 110), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OnlyUsedIfUserPwdIsSet"]), "html_attr");
                echo "\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailUsername\"
                     ng-model=\"mailSettings.mailUsername\"
                     data-title=\"";
                // line 115
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpUsername"]), "html_attr");
                echo "\"
                     value=\"";
                // line 116
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 116, $this->source); })()), "username", [], "any", false, false, false, 116), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OnlyEnterIfRequired"]), "html_attr");
                echo "\"
                     autocomplete=\"off\">
                </div>

                ";
                // line 120
                ob_start();
                // line 121
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_OnlyEnterIfRequiredPassword"]), "html", null, true);
                echo "<br/>
                    ";
                // line 122
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_WarningPasswordStored", "<strong>", "</strong>"]);
                $context["help"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 124
                echo "
                <div piwik-field uicontrol=\"password\" name=\"mailPassword\"
                     ng-model=\"mailSettings.mailPassword\"
                     ng-change=\"mailSettings.passwordChanged = true\"
                     data-title=\"";
                // line 128
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpPassword"]), "html_attr");
                echo "\"
                     value=\"";
                // line 129
                echo ((twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 129, $this->source); })()), "password", [], "any", false, false, false, 129)) ? ("******") : (""));
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["help"]) || array_key_exists("help", $context) ? $context["help"] : (function () { throw new RuntimeError('Variable "help" does not exist.', 129, $this->source); })()), "html_attr");
                echo "\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailFromAddress\"
                     ng-model=\"mailSettings.mailFromAddress\"
                     title=\"";
                // line 135
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpFromAddress"]), "html_attr");
                echo "\"
                     value=\"";
                // line 136
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 136, $this->source); })()), "noreply_email_address", [], "any", false, false, false, 136), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpFromEmailHelp", (isset($context["mailHost"]) || array_key_exists("mailHost", $context) ? $context["mailHost"] : (function () { throw new RuntimeError('Variable "mailHost" does not exist.', 136, $this->source); })())]), "html_attr");
                echo "\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailFromName\"
                     ng-model=\"mailSettings.mailFromName\"
                     title=\"";
                // line 142
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpFromName"]), "html_attr");
                echo "\"
                     value=\"";
                // line 143
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 143, $this->source); })()), "noreply_email_name", [], "any", false, false, false, 143), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_NameShownInTheSenderColumn"]), "html_attr");
                echo "\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"mailEncryption\"
                     ng-model=\"mailSettings.mailEncryption\"
                     data-title=\"";
                // line 149
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_SmtpEncryption"]), "html_attr");
                echo "\"
                     options=\"";
                // line 150
                echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["mailEncryptions"]) || array_key_exists("mailEncryptions", $context) ? $context["mailEncryptions"] : (function () { throw new RuntimeError('Variable "mailEncryptions" does not exist.', 150, $this->source); })())), "html", null, true);
                echo "\"
                     value=\"";
                // line 151
                echo \Piwik\piwik_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["mail"]) || array_key_exists("mail", $context) ? $context["mail"] : (function () { throw new RuntimeError('Variable "mail" does not exist.', 151, $this->source); })()), "encryption", [], "any", false, false, false, 151), "html_attr");
                echo "\" inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_EncryptedSmtpTransport"]), "html_attr");
                echo "\">
                </div>
            </div>

            <div onconfirm=\"mailSettings.save()\" saving=\"mailSettings.isLoading\" piwik-save-button></div>
        </div>
    </div>
    ";
            }
        }
        // line 160
        echo "
";
        // line 161
        if ((isset($context["customLogoEnabled"]) || array_key_exists("customLogoEnabled", $context) ? $context["customLogoEnabled"] : (function () { throw new RuntimeError('Variable "customLogoEnabled" does not exist.', 161, $this->source); })())) {
            // line 162
            echo "<div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_BrandingSettings"]), "html_attr");
            echo "\">

    <div piwik-form ng-controller=\"BrandingController as brandingSettings\">

        <p>";
            // line 166
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_CustomLogoHelpText"]), "html", null, true);
            echo "</p>

        ";
            // line 168
            ob_start();
            // line 169
            ob_start();
            echo "\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_GiveUsYourFeedback"]), "html", null, true);
            echo "\"";
            $context["giveUsFeedbackText"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 170
            echo "            ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_CustomLogoFeedbackInfo", (isset($context["giveUsFeedbackText"]) || array_key_exists("giveUsFeedbackText", $context) ? $context["giveUsFeedbackText"] : (function () { throw new RuntimeError('Variable "giveUsFeedbackText" does not exist.', 170, $this->source); })()), "<a href='?module=CorePluginsAdmin&action=plugins' rel='noreferrer noopener' target='_blank'>", "</a>"]);
            $context["help"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 172
            echo "
        <div piwik-field uicontrol=\"checkbox\" name=\"useCustomLogo\"
             ng-model=\"brandingSettings.enabled\"
             ng-change=\"brandingSettings.toggleCustomLogo()\"
             data-title=\"";
            // line 176
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_UseCustomLogo"]), "html_attr");
            echo "\"
             value=\"";
            // line 177
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, (isset($context["branding"]) || array_key_exists("branding", $context) ? $context["branding"] : (function () { throw new RuntimeError('Variable "branding" does not exist.', 177, $this->source); })()), "use_custom_logo", [], "any", false, false, false, 177), 1))) {
                echo "1";
            }
            echo "\"
             ";
            // line 178
            if ((isset($context["isPluginsAdminEnabled"]) || array_key_exists("isPluginsAdminEnabled", $context) ? $context["isPluginsAdminEnabled"] : (function () { throw new RuntimeError('Variable "isPluginsAdminEnabled" does not exist.', 178, $this->source); })())) {
                echo "inline-help=\"";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["help"]) || array_key_exists("help", $context) ? $context["help"] : (function () { throw new RuntimeError('Variable "help" does not exist.', 178, $this->source); })()), "html_attr");
                echo "\"";
            }
            echo ">
        </div>

        <div id=\"logoSettings\" ng-show=\"brandingSettings.enabled\">
            <form id=\"logoUploadForm\" method=\"post\" enctype=\"multipart/form-data\" action=\"index.php?module=CoreAdminHome&format=json&action=uploadCustomLogo\">
                ";
            // line 183
            if ((isset($context["fileUploadEnabled"]) || array_key_exists("fileUploadEnabled", $context) ? $context["fileUploadEnabled"] : (function () { throw new RuntimeError('Variable "fileUploadEnabled" does not exist.', 183, $this->source); })())) {
                // line 184
                echo "                    <input type=\"hidden\" name=\"token_auth\" value=\"";
                echo \Piwik\piwik_escape_filter($this->env, (isset($context["token_auth"]) || array_key_exists("token_auth", $context) ? $context["token_auth"] : (function () { throw new RuntimeError('Variable "token_auth" does not exist.', 184, $this->source); })()), "html", null, true);
                echo "\"/>
                    <input type=\"hidden\" name=\"force_api_session\" value=\"1\"/>

                    ";
                // line 187
                if ((isset($context["logosWriteable"]) || array_key_exists("logosWriteable", $context) ? $context["logosWriteable"] : (function () { throw new RuntimeError('Variable "logosWriteable" does not exist.', 187, $this->source); })())) {
                    // line 188
                    echo "                        <div class=\"alert alert-warning uploaderror\" style=\"display:none;\">
                            ";
                    // line 189
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LogoUploadFailed"]), "html", null, true);
                    echo "
                        </div>

                        <div piwik-field uicontrol=\"file\" name=\"customLogo\"
                             ng-change=\"brandingSettings.updateLogo()\"
                             ng-model=\"brandingSettings.customLogo\"
                             data-title=\"";
                    // line 195
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LogoUpload"]), "html_attr");
                    echo "\"
                             inline-help=\"";
                    // line 196
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LogoUploadHelp", "JPG / PNG / GIF", 110]), "html_attr");
                    echo "\">
                        </div>

                        <div class=\"row\">
                            <div class=\"col s12\">
                                <img data-src=\"";
                    // line 201
                    echo \Piwik\piwik_escape_filter($this->env, (isset($context["pathUserLogo"]) || array_key_exists("pathUserLogo", $context) ? $context["pathUserLogo"] : (function () { throw new RuntimeError('Variable "pathUserLogo" does not exist.', 201, $this->source); })()), "html", null, true);
                    echo "\" data-src-exists=\"";
                    echo (((isset($context["hasUserLogo"]) || array_key_exists("hasUserLogo", $context) ? $context["hasUserLogo"] : (function () { throw new RuntimeError('Variable "hasUserLogo" does not exist.', 201, $this->source); })())) ? ("1") : ("0"));
                    echo "\"
                                     id=\"currentLogo\" style=\"max-height: 150px\"/>
                            </div>
                        </div>

                        <div piwik-field uicontrol=\"file\" name=\"customFavicon\"
                             ng-change=\"brandingSettings.updateLogo()\"
                             ng-model=\"brandingSettings.customFavicon\"
                             data-title=\"";
                    // line 209
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_FaviconUpload"]), "html_attr");
                    echo "\"
                             inline-help=\"";
                    // line 210
                    echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LogoUploadHelp", "JPG / PNG / GIF", 16]), "html_attr");
                    echo "\">
                        </div>

                        <div class=\"row\">
                            <div class=\"col s12\">
                                <img data-src=\"";
                    // line 215
                    echo \Piwik\piwik_escape_filter($this->env, (isset($context["pathUserFavicon"]) || array_key_exists("pathUserFavicon", $context) ? $context["pathUserFavicon"] : (function () { throw new RuntimeError('Variable "pathUserFavicon" does not exist.', 215, $this->source); })()), "html", null, true);
                    echo "\" data-src-exists=\"";
                    echo (((isset($context["hasUserFavicon"]) || array_key_exists("hasUserFavicon", $context) ? $context["hasUserFavicon"] : (function () { throw new RuntimeError('Variable "hasUserFavicon" does not exist.', 215, $this->source); })())) ? ("1") : ("0"));
                    echo "\"
                                     id=\"currentFavicon\" width=\"16\" height=\"16\"/>
                            </div>
                        </div>

                    ";
                } else {
                    // line 221
                    echo "                        <div class=\"alert alert-warning\">
                            ";
                    // line 222
                    echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_LogoNotWriteableInstruction", (("<code>" .                     // line 223
(isset($context["pathUserLogoDirectory"]) || array_key_exists("pathUserLogoDirectory", $context) ? $context["pathUserLogoDirectory"] : (function () { throw new RuntimeError('Variable "pathUserLogoDirectory" does not exist.', 223, $this->source); })())) . "</code><br/>"), ((((((isset($context["pathUserLogo"]) || array_key_exists("pathUserLogo", $context) ? $context["pathUserLogo"] : (function () { throw new RuntimeError('Variable "pathUserLogo" does not exist.', 223, $this->source); })()) . ", ") . (isset($context["pathUserLogoSmall"]) || array_key_exists("pathUserLogoSmall", $context) ? $context["pathUserLogoSmall"] : (function () { throw new RuntimeError('Variable "pathUserLogoSmall" does not exist.', 223, $this->source); })())) . ", ") . (isset($context["pathUserLogoSVG"]) || array_key_exists("pathUserLogoSVG", $context) ? $context["pathUserLogoSVG"] : (function () { throw new RuntimeError('Variable "pathUserLogoSVG" does not exist.', 223, $this->source); })())) . "")]);
                    echo "
                        </div>
                    ";
                }
                // line 226
                echo "                ";
            } else {
                // line 227
                echo "                    <div class=\"alert alert-warning\">
                        ";
                // line 228
                echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["CoreAdminHome_FileUploadDisabled", "file_uploads=1"]), "html", null, true);
                echo "
                    </div>
                ";
            }
            // line 231
            echo "            </form>
        </div>

        <div onconfirm=\"brandingSettings.save()\" saving=\"brandingSettings.isLoading\" piwik-save-button></div>
    </div>
</div>
";
        }
        // line 238
        echo "
";
        // line 239
        if ((isset($context["isDataPurgeSettingsEnabled"]) || array_key_exists("isDataPurgeSettingsEnabled", $context) ? $context["isDataPurgeSettingsEnabled"] : (function () { throw new RuntimeError('Variable "isDataPurgeSettingsEnabled" does not exist.', 239, $this->source); })())) {
            // line 240
            echo "    <div piwik-content-block content-title=\"";
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DeleteDataSettings"]), "html_attr");
            echo "\">
        <p>";
            // line 241
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DeleteDataDescription"]), "html", null, true);
            echo "</p>
        <p>
            <a href='";
            // line 243
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["module" => "PrivacyManager", "action" => "privacySettings"]]), "html", null, true);
            echo "#deleteLogsAnchor'>
                ";
            // line 244
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_ClickHereSettings", (("'" . call_user_func_array($this->env->getFilter('translate')->getCallable(), ["PrivacyManager_DeleteDataSettings"])) . "'")]), "html", null, true);
            echo "
            </a>
        </p>
    </div>
";
        }
        // line 249
        echo "
<div piwik-plugin-settings mode=\"admin\"></div>

";
    }

    public function getTemplateName()
    {
        return "@CoreAdminHome/generalSettings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  573 => 249,  565 => 244,  561 => 243,  556 => 241,  551 => 240,  549 => 239,  546 => 238,  537 => 231,  531 => 228,  528 => 227,  525 => 226,  519 => 223,  518 => 222,  515 => 221,  504 => 215,  496 => 210,  492 => 209,  479 => 201,  471 => 196,  467 => 195,  458 => 189,  455 => 188,  453 => 187,  446 => 184,  444 => 183,  432 => 178,  426 => 177,  422 => 176,  416 => 172,  412 => 170,  406 => 169,  404 => 168,  399 => 166,  391 => 162,  389 => 161,  386 => 160,  372 => 151,  368 => 150,  364 => 149,  353 => 143,  349 => 142,  338 => 136,  334 => 135,  323 => 129,  319 => 128,  313 => 124,  310 => 122,  306 => 121,  304 => 120,  295 => 116,  291 => 115,  281 => 110,  277 => 109,  273 => 108,  263 => 103,  259 => 102,  251 => 97,  247 => 96,  236 => 88,  230 => 87,  226 => 86,  217 => 81,  215 => 80,  206 => 73,  200 => 70,  194 => 68,  188 => 65,  184 => 64,  180 => 63,  177 => 62,  175 => 61,  172 => 60,  170 => 59,  163 => 55,  154 => 53,  148 => 50,  138 => 43,  133 => 41,  125 => 36,  121 => 35,  115 => 34,  103 => 25,  99 => 24,  96 => 23,  92 => 22,  84 => 17,  77 => 14,  75 => 13,  70 => 11,  66 => 10,  63 => 9,  60 => 8,  58 => 7,  55 => 6,  51 => 5,  46 => 1,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'CoreAdminHome_MenuGeneralSettings'|translate }}{% endset %}

{% block content %}

    {% import 'macros.twig' as piwik %}
    {% import 'ajaxMacros.twig' as ajax %}

    {{ ajax.errorDiv() }}
    {{ ajax.loadingDiv() }}

{% if isGeneralSettingsAdminEnabled %}
    <div piwik-content-block content-title=\"{{ 'CoreAdminHome_ArchivingSettings'|translate|e('html_attr') }}\">
        <div ng-controller=\"ArchivingController as archivingSettings\">
            <div class=\"form-group row\">
                <h3 class=\"col s12\">{{ 'General_AllowPiwikArchivingToTriggerBrowser'|translate }}</h3>
                <div class=\"col s12 m6\">
                    <p>
                        <label>
                            <input type=\"radio\" value=\"1\" id=\"enableBrowserTriggerArchiving1\"
                                   name=\"enableBrowserTriggerArchiving\" {% if enableBrowserTriggerArchiving==1 %} checked=\"checked\"{% endif %}
                            />
                            <span>{{ 'General_Yes'|translate }}</span>
                            <span class=\"form-description\">{{ 'General_Default'|translate }}</span>
                        </label>
                    </p>

                    <p>
                    <label for=\"enableBrowserTriggerArchiving2\">
                        <input type=\"radio\" value=\"0\"
                               id=\"enableBrowserTriggerArchiving2\"
                               name=\"enableBrowserTriggerArchiving\"
                                {% if enableBrowserTriggerArchiving==0 %} checked=\"checked\"{% endif %} />
                        <span>{{ 'General_No'|translate }}</span>
                        <span class=\"form-description\">{{ 'General_ArchivingTriggerDescription'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/setup-auto-archiving/'>\",\"</a>\")|raw }}</span>
                    </label>
                    </p>
                </div><div class=\"col s12 m6\">
                    <div class=\"form-help\">
                        {{ 'General_ArchivingInlineHelp'|translate }}
                        <br/>
                        {{ 'General_SeeTheOfficialDocumentationForMoreInformation'|translate(\"<a target='_blank' rel='noreferrer noopener' href='https://matomo.org/docs/setup-auto-archiving/'>\",\"</a>\")|raw }}
                    </div>
                </div>
            </div>

            <div class=\"form-group row\">
                <h3 class=\"col s12\">
                    {{ 'General_ReportsContainingTodayWillBeProcessedAtMostEvery'|translate }}
                </h3>
                <div class=\"input-field col s12 m6\">
                    <input  type=\"text\" value='{{ todayArchiveTimeToLive|e('html_attr') }}' id='todayArchiveTimeToLive' {% if not isGeneralSettingsAdminEnabled %}disabled=\"disabled\"{% endif %} />
                    <span class=\"form-description\">
                        {{ 'General_RearchiveTimeIntervalOnlyForTodayReports'|translate }}
                    </span>
                </div>
                <div class=\"col s12 m6\">
                    {% if isGeneralSettingsAdminEnabled %}
                        <div class=\"form-help\">
                            {% if showWarningCron %}
                                <strong>
                                    {{ 'General_NewReportsWillBeProcessedByCron'|translate }}<br/>
                                    {{ 'General_ReportsWillBeProcessedAtMostEveryHour'|translate }}
                                    {{ 'General_IfArchivingIsFastYouCanSetupCronRunMoreOften'|translate }}<br/>
                                </strong>
                            {% endif %}
                            {{ 'General_SmallTrafficYouCanLeaveDefault'|translate( todayArchiveTimeToLiveDefault ) }}
                            <br/>
                            {{ 'General_MediumToHighTrafficItIsRecommendedTo'|translate(1800,3600) }}
                        </div>
                    {% endif %}
                </div>
            </div>

            <div onconfirm=\"archivingSettings.save()\" saving=\"archivingSettings.isLoading\" piwik-save-button></div>
        </div>
    </div>

    {% if not isMultiServerEnvironment %}
        <div piwik-content-block content-title=\"{{ 'CoreAdminHome_EmailServerSettings'|translate|e('html_attr') }}\">

        <div piwik-form ng-controller=\"MailSmtpController as mailSettings\">
            <div piwik-field uicontrol=\"checkbox\" name=\"mailUseSmtp\"
                 ng-model=\"mailSettings.enabled\"
                 data-title=\"{{ 'General_UseSMTPServerForEmail'|translate|e('html_attr') }}\"
                 value=\"{% if mail.transport == 'smtp' %}1{% endif %}\"
                 inline-help=\"{{ 'General_SelectYesIfYouWantToSendEmailsViaServer'|translate|e('html_attr') }}\">
            </div>

            <div id=\"smtpSettings\"
                 ng-show=\"mailSettings.enabled\">

                <div piwik-field uicontrol=\"text\" name=\"mailHost\"
                     ng-model=\"mailSettings.mailHost\"
                     data-title=\"{{ 'General_SmtpServerAddress'|translate|e('html_attr') }}\"
                     value=\"{{ mail.host|e('html_attr') }}\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailPort\"
                     ng-model=\"mailSettings.mailPort\"
                     data-title=\"{{ 'General_SmtpPort'|translate|e('html_attr') }}\"
                     value=\"{{ mail.port|e('html_attr') }}\" inline-help=\"{{ 'General_OptionalSmtpPort'|translate|e('html_attr') }}\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"mailType\"
                     ng-model=\"mailSettings.mailType\"
                     data-title=\"{{ 'General_AuthenticationMethodSmtp'|translate|e('html_attr') }}\"
                     options=\"{{ mailTypes|json_encode }}\"
                     value=\"{{ mail.type|e('html_attr') }}\" inline-help=\"{{ 'General_OnlyUsedIfUserPwdIsSet'|translate|e('html_attr') }}\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailUsername\"
                     ng-model=\"mailSettings.mailUsername\"
                     data-title=\"{{ 'General_SmtpUsername'|translate|e('html_attr') }}\"
                     value=\"{{ mail.username|e('html_attr') }}\" inline-help=\"{{ 'General_OnlyEnterIfRequired'|translate|e('html_attr') }}\"
                     autocomplete=\"off\">
                </div>

                {% set help -%}
                    {{ 'General_OnlyEnterIfRequiredPassword'|translate }}<br/>
                    {{ 'General_WarningPasswordStored'|translate(\"<strong>\",\"</strong>\")|raw }}
                {%- endset %}

                <div piwik-field uicontrol=\"password\" name=\"mailPassword\"
                     ng-model=\"mailSettings.mailPassword\"
                     ng-change=\"mailSettings.passwordChanged = true\"
                     data-title=\"{{ 'General_SmtpPassword'|translate|e('html_attr') }}\"
                     value=\"{{ mail.password ? '******' }}\" inline-help=\"{{ help|e('html_attr') }}\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailFromAddress\"
                     ng-model=\"mailSettings.mailFromAddress\"
                     title=\"{{ 'General_SmtpFromAddress'|translate|e('html_attr') }}\"
                     value=\"{{ mail.noreply_email_address|e('html_attr') }}\" inline-help=\"{{ 'General_SmtpFromEmailHelp'|translate(mailHost)|e('html_attr') }}\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"text\" name=\"mailFromName\"
                     ng-model=\"mailSettings.mailFromName\"
                     title=\"{{ 'General_SmtpFromName'|translate|e('html_attr') }}\"
                     value=\"{{ mail.noreply_email_name|e('html_attr') }}\" inline-help=\"{{ 'General_NameShownInTheSenderColumn'|translate|e('html_attr') }}\"
                     autocomplete=\"off\">
                </div>

                <div piwik-field uicontrol=\"select\" name=\"mailEncryption\"
                     ng-model=\"mailSettings.mailEncryption\"
                     data-title=\"{{ 'General_SmtpEncryption'|translate|e('html_attr') }}\"
                     options=\"{{ mailEncryptions|json_encode }}\"
                     value=\"{{ mail.encryption|e('html_attr') }}\" inline-help=\"{{ 'General_EncryptedSmtpTransport'|translate|e('html_attr') }}\">
                </div>
            </div>

            <div onconfirm=\"mailSettings.save()\" saving=\"mailSettings.isLoading\" piwik-save-button></div>
        </div>
    </div>
    {% endif %}
{% endif %}

{% if customLogoEnabled %}
<div piwik-content-block content-title=\"{{ 'CoreAdminHome_BrandingSettings'|translate|e('html_attr') }}\">

    <div piwik-form ng-controller=\"BrandingController as brandingSettings\">

        <p>{{ 'CoreAdminHome_CustomLogoHelpText'|translate }}</p>

        {% set help -%}
            {% set giveUsFeedbackText %}\"{{ 'General_GiveUsYourFeedback'|translate }}\"{% endset %}
            {{ 'CoreAdminHome_CustomLogoFeedbackInfo'|translate(giveUsFeedbackText,\"<a href='?module=CorePluginsAdmin&action=plugins' rel='noreferrer noopener' target='_blank'>\",\"</a>\")|raw }}
        {%- endset %}

        <div piwik-field uicontrol=\"checkbox\" name=\"useCustomLogo\"
             ng-model=\"brandingSettings.enabled\"
             ng-change=\"brandingSettings.toggleCustomLogo()\"
             data-title=\"{{ 'CoreAdminHome_UseCustomLogo'|translate|e('html_attr') }}\"
             value=\"{% if branding.use_custom_logo == 1 %}1{% endif %}\"
             {% if isPluginsAdminEnabled %}inline-help=\"{{ help|e('html_attr') }}\"{% endif %}>
        </div>

        <div id=\"logoSettings\" ng-show=\"brandingSettings.enabled\">
            <form id=\"logoUploadForm\" method=\"post\" enctype=\"multipart/form-data\" action=\"index.php?module=CoreAdminHome&format=json&action=uploadCustomLogo\">
                {% if fileUploadEnabled %}
                    <input type=\"hidden\" name=\"token_auth\" value=\"{{ token_auth }}\"/>
                    <input type=\"hidden\" name=\"force_api_session\" value=\"1\"/>

                    {% if logosWriteable %}
                        <div class=\"alert alert-warning uploaderror\" style=\"display:none;\">
                            {{ 'CoreAdminHome_LogoUploadFailed'|translate }}
                        </div>

                        <div piwik-field uicontrol=\"file\" name=\"customLogo\"
                             ng-change=\"brandingSettings.updateLogo()\"
                             ng-model=\"brandingSettings.customLogo\"
                             data-title=\"{{ 'CoreAdminHome_LogoUpload'|translate|e('html_attr') }}\"
                             inline-help=\"{{ 'CoreAdminHome_LogoUploadHelp'|translate(\"JPG / PNG / GIF\", 110)|e('html_attr') }}\">
                        </div>

                        <div class=\"row\">
                            <div class=\"col s12\">
                                <img data-src=\"{{ pathUserLogo }}\" data-src-exists=\"{{ hasUserLogo ? '1':'0' }}\"
                                     id=\"currentLogo\" style=\"max-height: 150px\"/>
                            </div>
                        </div>

                        <div piwik-field uicontrol=\"file\" name=\"customFavicon\"
                             ng-change=\"brandingSettings.updateLogo()\"
                             ng-model=\"brandingSettings.customFavicon\"
                             data-title=\"{{ 'CoreAdminHome_FaviconUpload'|translate|e('html_attr') }}\"
                             inline-help=\"{{ 'CoreAdminHome_LogoUploadHelp'|translate(\"JPG / PNG / GIF\", 16)|e('html_attr') }}\">
                        </div>

                        <div class=\"row\">
                            <div class=\"col s12\">
                                <img data-src=\"{{ pathUserFavicon }}\" data-src-exists=\"{{ hasUserFavicon ? '1':'0' }}\"
                                     id=\"currentFavicon\" width=\"16\" height=\"16\"/>
                            </div>
                        </div>

                    {% else %}
                        <div class=\"alert alert-warning\">
                            {{ 'CoreAdminHome_LogoNotWriteableInstruction'
                                |translate(\"<code>\"~pathUserLogoDirectory~\"</code><br/>\", pathUserLogo ~\", \"~ pathUserLogoSmall ~\", \"~ pathUserLogoSVG ~\"\")|raw }}
                        </div>
                    {% endif %}
                {% else %}
                    <div class=\"alert alert-warning\">
                        {{ 'CoreAdminHome_FileUploadDisabled'|translate(\"file_uploads=1\") }}
                    </div>
                {% endif %}
            </form>
        </div>

        <div onconfirm=\"brandingSettings.save()\" saving=\"brandingSettings.isLoading\" piwik-save-button></div>
    </div>
</div>
{% endif %}

{% if isDataPurgeSettingsEnabled %}
    <div piwik-content-block content-title=\"{{ 'PrivacyManager_DeleteDataSettings'|translate|e('html_attr') }}\">
        <p>{{ 'PrivacyManager_DeleteDataDescription'|translate }}</p>
        <p>
            <a href='{{ linkTo({'module':\"PrivacyManager\", 'action':\"privacySettings\"}) }}#deleteLogsAnchor'>
                {{ 'PrivacyManager_ClickHereSettings'|translate(\"'\" ~ 'PrivacyManager_DeleteDataSettings'|translate ~ \"'\") }}
            </a>
        </p>
    </div>
{% endif %}

<div piwik-plugin-settings mode=\"admin\"></div>

{% endblock %}
", "@CoreAdminHome/generalSettings.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/CoreAdminHome/templates/generalSettings.twig");
    }
}
