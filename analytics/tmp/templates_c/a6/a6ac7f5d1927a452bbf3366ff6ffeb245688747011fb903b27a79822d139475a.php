<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @UsersManager/userSettings.twig */
class __TwigTemplate_ebf59dbb9019dbb1950cc6c89749f6103037fc0fbbd308498adf07f6a0c89c11 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        ob_start();
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_PersonalSettings"]), "html", null, true);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("admin.twig", "@UsersManager/userSettings.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<div piwik-content-block content-title=\"";
        // line 7
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 7, $this->source); })()), "html_attr");
        echo "\" feature=\"true\">
    <form id=\"userSettingsTable\" piwik-form ng-controller=\"PersonalSettingsController as personalSettings\">

        <div piwik-field uicontrol=\"text\" name=\"username\"
             data-title=\"";
        // line 11
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Username"]), "html_attr");
        echo "\"
             value=\"";
        // line 12
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["userLogin"]) || array_key_exists("userLogin", $context) ? $context["userLogin"] : (function () { throw new RuntimeError('Variable "userLogin" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "\" data-disabled=\"true\"
             ng-model=\"personalSettings.username\"
             inline-help=\"";
        // line 14
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_YourUsernameCannotBeChanged"]), "html_attr");
        echo "\">
        </div>

        ";
        // line 17
        if ((isset($context["isUsersAdminEnabled"]) || array_key_exists("isUsersAdminEnabled", $context) ? $context["isUsersAdminEnabled"] : (function () { throw new RuntimeError('Variable "isUsersAdminEnabled" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "        <div piwik-field uicontrol=\"text\" name=\"email\"
             ng-model=\"personalSettings.email\"
             ng-change=\"personalSettings.requirePasswordConfirmation()\"
             maxlength=\"100\"
             data-title=\"";
            // line 22
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_Email"]), "html_attr");
            echo "\"
             value=\"";
            // line 23
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["userEmail"]) || array_key_exists("userEmail", $context) ? $context["userEmail"] : (function () { throw new RuntimeError('Variable "userEmail" does not exist.', 23, $this->source); })()), "html", null, true);
            echo "\">
        </div>
        ";
        }
        // line 26
        echo "
        <div id=\"languageHelp\" class=\"inline-help-node\">
            <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/translations/\">
                ";
        // line 29
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["LanguagesManager_AboutPiwikTranslations"]), "html", null, true);
        echo "</a>
        </div>

        <div piwik-field uicontrol=\"select\" name=\"language\"
             ng-model=\"personalSettings.language\"
             data-title=\"";
        // line 34
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Language"]), "html_attr");
        echo "\"
             options=\"";
        // line 35
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["languageOptions"]) || array_key_exists("languageOptions", $context) ? $context["languageOptions"] : (function () { throw new RuntimeError('Variable "languageOptions" does not exist.', 35, $this->source); })())), "html", null, true);
        echo "\"
             inline-help=\"#languageHelp\"
             value=\"";
        // line 37
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["currentLanguageCode"]) || array_key_exists("currentLanguageCode", $context) ? $context["currentLanguageCode"] : (function () { throw new RuntimeError('Variable "currentLanguageCode" does not exist.', 37, $this->source); })()), "html", null, true);
        echo "\">
        </div>

        <div piwik-field uicontrol=\"select\" name=\"timeformat\"
             ng-model=\"personalSettings.timeformat\"
             data-title=\"";
        // line 42
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_TimeFormat"]), "html_attr");
        echo "\"
             value=\"";
        // line 43
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["currentTimeformat"]) || array_key_exists("currentTimeformat", $context) ? $context["currentTimeformat"] : (function () { throw new RuntimeError('Variable "currentTimeformat" does not exist.', 43, $this->source); })()), "html", null, true);
        echo "\" options=\"";
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["timeFormats"]) || array_key_exists("timeFormats", $context) ? $context["timeFormats"] : (function () { throw new RuntimeError('Variable "timeFormats" does not exist.', 43, $this->source); })())), "html", null, true);
        echo "\">
        </div>

        <div piwik-field uicontrol=\"radio\" name=\"defaultReport\"
             ng-model=\"personalSettings.defaultReport\"
             introduction=\"";
        // line 48
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ReportToLoadByDefault"]), "html_attr");
        echo "\"
             data-title=\"";
        // line 49
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_AllWebsitesDashboard"]), "html_attr");
        echo "\"
             value=\"";
        // line 50
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["defaultReport"]) || array_key_exists("defaultReport", $context) ? $context["defaultReport"] : (function () { throw new RuntimeError('Variable "defaultReport" does not exist.', 50, $this->source); })()), "html", null, true);
        echo "\" options=\"";
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["defaultReportOptions"]) || array_key_exists("defaultReportOptions", $context) ? $context["defaultReportOptions"] : (function () { throw new RuntimeError('Variable "defaultReportOptions" does not exist.', 50, $this->source); })())), "html", null, true);
        echo "\">
        </div>

        <div piwik-siteselector
             ng-model=\"personalSettings.site\"
             show-selected-site=\"true\"
             class=\"sites_autocomplete\"
             siteid=\"";
        // line 57
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["defaultReportIdSite"]) || array_key_exists("defaultReportIdSite", $context) ? $context["defaultReportIdSite"] : (function () { throw new RuntimeError('Variable "defaultReportIdSite" does not exist.', 57, $this->source); })()), "html", null, true);
        echo "\"
             sitename=\"";
        // line 58
        echo call_user_func_array($this->env->getFilter('rawSafeDecoded')->getCallable(), [(isset($context["defaultReportSiteName"]) || array_key_exists("defaultReportSiteName", $context) ? $context["defaultReportSiteName"] : (function () { throw new RuntimeError('Variable "defaultReportSiteName" does not exist.', 58, $this->source); })())]);
        echo "\"
             switch-site-on-select=\"false\"
             show-all-sites-item=\"false\"
             showselectedsite=\"true\"
             id=\"defaultReportSiteSelector\"
        ></div>

        <div piwik-field uicontrol=\"radio\" name=\"defaultDate\"
             ng-model=\"personalSettings.defaultDate\"
             introduction=\"";
        // line 67
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ReportDateToLoadByDefault"]), "html_attr");
        echo "\"
             value=\"";
        // line 68
        echo \Piwik\piwik_escape_filter($this->env, (isset($context["defaultDate"]) || array_key_exists("defaultDate", $context) ? $context["defaultDate"] : (function () { throw new RuntimeError('Variable "defaultDate" does not exist.', 68, $this->source); })()), "html", null, true);
        echo "\" options=\"";
        echo \Piwik\piwik_escape_filter($this->env, json_encode((isset($context["availableDefaultDates"]) || array_key_exists("availableDefaultDates", $context) ? $context["availableDefaultDates"] : (function () { throw new RuntimeError('Variable "availableDefaultDates" does not exist.', 68, $this->source); })())), "html", null, true);
        echo "\">
        </div>

        <div piwik-save-button onconfirm=\"personalSettings.save()\"
             saving=\"personalSettings.loading\"></div>

        <div class=\"modal\" id=\"confirmChangesWithPassword\">
            <div class=\"modal-content\">
                <h2>";
        // line 76
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ConfirmWithPassword"]), "html", null, true);
        echo "</h2>

                <div piwik-field uicontrol=\"password\" name=\"currentPassword\" autocomplete=\"off\"
                     ng-model=\"personalSettings.passwordCurrent\"
                     full-width=\"true\"
                     data-title=\"";
        // line 81
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_YourCurrentPassword"]), "html_attr");
        echo "\"
                     value=\"\">
                </div>
            </div>
            <div class=\"modal-footer\">
                <a href=\"javascript:void(0)\" class=\"modal-action btn\" ng-click=\"personalSettings.save()\">";
        // line 86
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Ok"]), "html", null, true);
        echo "</a>
                <a href=\"javascript:void(0)\" class=\"modal-action modal-close modal-no\"  ng-click=\"personalSettings.cancelSave()\">";
        // line 87
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["General_Cancel"]), "html", null, true);
        echo "</a>
            </div>
        </div>

    </form>
</div>

";
        // line 94
        if ((isset($context["showNewsletterSignup"]) || array_key_exists("showNewsletterSignup", $context) ? $context["showNewsletterSignup"] : (function () { throw new RuntimeError('Variable "showNewsletterSignup" does not exist.', 94, $this->source); })())) {
            // line 95
            echo "    <div ng-controller=\"PersonalSettingsController as personalSettings\">
        <div piwik-content-block id=\"newsletterSignup\"
             ng-show=\"personalSettings.showNewsletterSignup\"
             content-title=\"";
            // line 98
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_NewsletterSignupTitle"]), "html_attr");
            echo "\">

            <div piwik-field uicontrol=\"checkbox\" name=\"newsletterSignupCheckbox\" id=\"newsletterSignupCheckbox\"
                 ng-model=\"personalSettings.newsletterSignupCheckbox\"
                 full-width=\"true\"
                 data-title=\"";
            // line 103
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_NewsletterSignupMessage", "<a href=\"https://matomo.org/privacy-policy/\" target=\"_blank\">", "</a>"]), "html_attr");
            echo "\"
            >
            </div>

            <div piwik-save-button id=\"newsletterSignupBtn\"
                 onconfirm=\"personalSettings.signupForNewsletter()\"
                 data-disabled=\"!personalSettings.newsletterSignupCheckbox\"
                 value=\"";
            // line 110
            echo "{{ personalSettings.newsletterSignupButtonTitle }}";
            echo "\"
                 saving=\"personalSettings.isProcessingNewsletterSignup\">
            </div>
        </div>
    </div>
";
        }
        // line 116
        echo "
<div piwik-plugin-settings mode=\"user\"></div>

<div piwik-content-block
     content-title=\"";
        // line 120
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ExcludeVisitsViaCookie"]), "html_attr");
        echo "\">
    <p>
        ";
        // line 122
        if ((isset($context["ignoreCookieSet"]) || array_key_exists("ignoreCookieSet", $context) ? $context["ignoreCookieSet"] : (function () { throw new RuntimeError('Variable "ignoreCookieSet" does not exist.', 122, $this->source); })())) {
            // line 123
            echo "            ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_YourVisitsAreIgnoredOnDomain", "<strong>", (isset($context["piwikHost"]) || array_key_exists("piwikHost", $context) ? $context["piwikHost"] : (function () { throw new RuntimeError('Variable "piwikHost" does not exist.', 123, $this->source); })()), "</strong>"]);
            echo "
        ";
        } else {
            // line 125
            echo "            ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_YourVisitsAreNotIgnored", "<strong>", "</strong>"]);
            echo "
        ";
        }
        // line 127
        echo "    </p>
    <span style=\"margin-left:20px;\">
    <a href='";
        // line 129
        echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFunction('linkTo')->getCallable(), [["ignoreSalt" => (isset($context["ignoreSalt"]) || array_key_exists("ignoreSalt", $context) ? $context["ignoreSalt"] : (function () { throw new RuntimeError('Variable "ignoreSalt" does not exist.', 129, $this->source); })()), "module" => "UsersManager", "action" => "setIgnoreCookie"]]), "html", null, true);
        echo "#excludeCookie'>&rsaquo; ";
        if ((isset($context["ignoreCookieSet"]) || array_key_exists("ignoreCookieSet", $context) ? $context["ignoreCookieSet"] : (function () { throw new RuntimeError('Variable "ignoreCookieSet" does not exist.', 129, $this->source); })())) {
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ClickHereToDeleteTheCookie"]), "html", null, true);
            echo "
        ";
        } else {
            // line 130
            echo \Piwik\piwik_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), ["UsersManager_ClickHereToSetTheCookieOnDomain", (isset($context["piwikHost"]) || array_key_exists("piwikHost", $context) ? $context["piwikHost"] : (function () { throw new RuntimeError('Variable "piwikHost" does not exist.', 130, $this->source); })())]), "html", null, true);
        }
        // line 131
        echo "        <br/>
    </a></span>
</div>

";
    }

    public function getTemplateName()
    {
        return "@UsersManager/userSettings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 131,  296 => 130,  288 => 129,  284 => 127,  278 => 125,  272 => 123,  270 => 122,  265 => 120,  259 => 116,  250 => 110,  240 => 103,  232 => 98,  227 => 95,  225 => 94,  215 => 87,  211 => 86,  203 => 81,  195 => 76,  182 => 68,  178 => 67,  166 => 58,  162 => 57,  150 => 50,  146 => 49,  142 => 48,  132 => 43,  128 => 42,  120 => 37,  115 => 35,  111 => 34,  103 => 29,  98 => 26,  92 => 23,  88 => 22,  82 => 18,  80 => 17,  74 => 14,  69 => 12,  65 => 11,  58 => 7,  55 => 6,  51 => 5,  46 => 1,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.twig' %}

{% set title %}{{ 'UsersManager_PersonalSettings'|translate }}{% endset %}

{% block content %}

<div piwik-content-block content-title=\"{{ title|e('html_attr') }}\" feature=\"true\">
    <form id=\"userSettingsTable\" piwik-form ng-controller=\"PersonalSettingsController as personalSettings\">

        <div piwik-field uicontrol=\"text\" name=\"username\"
             data-title=\"{{ 'General_Username'|translate|e('html_attr') }}\"
             value=\"{{ userLogin }}\" data-disabled=\"true\"
             ng-model=\"personalSettings.username\"
             inline-help=\"{{ 'UsersManager_YourUsernameCannotBeChanged'|translate|e('html_attr') }}\">
        </div>

        {% if isUsersAdminEnabled %}
        <div piwik-field uicontrol=\"text\" name=\"email\"
             ng-model=\"personalSettings.email\"
             ng-change=\"personalSettings.requirePasswordConfirmation()\"
             maxlength=\"100\"
             data-title=\"{{ 'UsersManager_Email'|translate|e('html_attr') }}\"
             value=\"{{ userEmail }}\">
        </div>
        {% endif %}

        <div id=\"languageHelp\" class=\"inline-help-node\">
            <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/translations/\">
                {{ 'LanguagesManager_AboutPiwikTranslations'|translate }}</a>
        </div>

        <div piwik-field uicontrol=\"select\" name=\"language\"
             ng-model=\"personalSettings.language\"
             data-title=\"{{ 'General_Language'|translate|e('html_attr') }}\"
             options=\"{{ languageOptions|json_encode }}\"
             inline-help=\"#languageHelp\"
             value=\"{{ currentLanguageCode }}\">
        </div>

        <div piwik-field uicontrol=\"select\" name=\"timeformat\"
             ng-model=\"personalSettings.timeformat\"
             data-title=\"{{ 'General_TimeFormat'|translate|e('html_attr') }}\"
             value=\"{{ currentTimeformat }}\" options=\"{{ timeFormats|json_encode }}\">
        </div>

        <div piwik-field uicontrol=\"radio\" name=\"defaultReport\"
             ng-model=\"personalSettings.defaultReport\"
             introduction=\"{{ 'UsersManager_ReportToLoadByDefault'|translate|e('html_attr') }}\"
             data-title=\"{{ 'General_AllWebsitesDashboard'|translate|e('html_attr') }}\"
             value=\"{{ defaultReport }}\" options=\"{{ defaultReportOptions|json_encode }}\">
        </div>

        <div piwik-siteselector
             ng-model=\"personalSettings.site\"
             show-selected-site=\"true\"
             class=\"sites_autocomplete\"
             siteid=\"{{ defaultReportIdSite }}\"
             sitename=\"{{ defaultReportSiteName|rawSafeDecoded }}\"
             switch-site-on-select=\"false\"
             show-all-sites-item=\"false\"
             showselectedsite=\"true\"
             id=\"defaultReportSiteSelector\"
        ></div>

        <div piwik-field uicontrol=\"radio\" name=\"defaultDate\"
             ng-model=\"personalSettings.defaultDate\"
             introduction=\"{{ 'UsersManager_ReportDateToLoadByDefault'|translate|e('html_attr') }}\"
             value=\"{{ defaultDate }}\" options=\"{{ availableDefaultDates|json_encode }}\">
        </div>

        <div piwik-save-button onconfirm=\"personalSettings.save()\"
             saving=\"personalSettings.loading\"></div>

        <div class=\"modal\" id=\"confirmChangesWithPassword\">
            <div class=\"modal-content\">
                <h2>{{ 'UsersManager_ConfirmWithPassword'|translate }}</h2>

                <div piwik-field uicontrol=\"password\" name=\"currentPassword\" autocomplete=\"off\"
                     ng-model=\"personalSettings.passwordCurrent\"
                     full-width=\"true\"
                     data-title=\"{{ 'UsersManager_YourCurrentPassword'|translate|e('html_attr') }}\"
                     value=\"\">
                </div>
            </div>
            <div class=\"modal-footer\">
                <a href=\"javascript:void(0)\" class=\"modal-action btn\" ng-click=\"personalSettings.save()\">{{ 'General_Ok'|translate }}</a>
                <a href=\"javascript:void(0)\" class=\"modal-action modal-close modal-no\"  ng-click=\"personalSettings.cancelSave()\">{{ 'General_Cancel'|translate }}</a>
            </div>
        </div>

    </form>
</div>

{% if showNewsletterSignup %}
    <div ng-controller=\"PersonalSettingsController as personalSettings\">
        <div piwik-content-block id=\"newsletterSignup\"
             ng-show=\"personalSettings.showNewsletterSignup\"
             content-title=\"{{ 'UsersManager_NewsletterSignupTitle'|translate|e('html_attr') }}\">

            <div piwik-field uicontrol=\"checkbox\" name=\"newsletterSignupCheckbox\" id=\"newsletterSignupCheckbox\"
                 ng-model=\"personalSettings.newsletterSignupCheckbox\"
                 full-width=\"true\"
                 data-title=\"{{ 'UsersManager_NewsletterSignupMessage'|translate('<a href=\"https://matomo.org/privacy-policy/\" target=\"_blank\">', '</a>')|e('html_attr') }}\"
            >
            </div>

            <div piwik-save-button id=\"newsletterSignupBtn\"
                 onconfirm=\"personalSettings.signupForNewsletter()\"
                 data-disabled=\"!personalSettings.newsletterSignupCheckbox\"
                 value=\"{{ '{{ personalSettings.newsletterSignupButtonTitle }}'|raw }}\"
                 saving=\"personalSettings.isProcessingNewsletterSignup\">
            </div>
        </div>
    </div>
{% endif %}

<div piwik-plugin-settings mode=\"user\"></div>

<div piwik-content-block
     content-title=\"{{ 'UsersManager_ExcludeVisitsViaCookie'|translate|e('html_attr') }}\">
    <p>
        {% if ignoreCookieSet %}
            {{ 'UsersManager_YourVisitsAreIgnoredOnDomain'|translate(\"<strong>\", piwikHost, \"</strong>\")|raw }}
        {% else %}
            {{ 'UsersManager_YourVisitsAreNotIgnored'|translate(\"<strong>\",\"</strong>\")|raw }}
        {% endif %}
    </p>
    <span style=\"margin-left:20px;\">
    <a href='{{ linkTo({'ignoreSalt':ignoreSalt, 'module': 'UsersManager', 'action':'setIgnoreCookie'}) }}#excludeCookie'>&rsaquo; {% if ignoreCookieSet %}{{ 'UsersManager_ClickHereToDeleteTheCookie'|translate }}
        {% else %}{{'UsersManager_ClickHereToSetTheCookieOnDomain'|translate(piwikHost) }}{% endif %}
        <br/>
    </a></span>
</div>

{% endblock %}
", "@UsersManager/userSettings.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/UsersManager/templates/userSettings.twig");
    }
}
