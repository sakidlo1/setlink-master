<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @ProfessionalServices/promoExperiments.twig */
class __TwigTemplate_d2d6519697de84c250ebbdeb071545bbf039d4d8f67f087c112c2fc55ab0fa6d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/ab-testing-learn-more/\">A/B Testing for Matomo</a> you can immediately increase conversions and sales by creating different versions of a page to see which one grows your business.
</p>
";
    }

    public function getTemplateName()
    {
        return "@ProfessionalServices/promoExperiments.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<p style=\"margin-top:3em\" class=\" alert-info alert\">Did you know?
    With <a target=\"_blank\" rel=\"noreferrer noopener\" href=\"https://matomo.org/recommends/ab-testing-learn-more/\">A/B Testing for Matomo</a> you can immediately increase conversions and sales by creating different versions of a page to see which one grows your business.
</p>
", "@ProfessionalServices/promoExperiments.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/ProfessionalServices/templates/promoExperiments.twig");
    }
}
