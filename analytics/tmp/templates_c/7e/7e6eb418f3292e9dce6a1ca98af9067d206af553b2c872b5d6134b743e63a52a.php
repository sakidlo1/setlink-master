<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Login/resetPassword.twig */
class __TwigTemplate_cdd8067302c73db42cafa2aa9bac09418c9953021c408851f2d6d3303dabb3ad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("infoMessage", $context) &&  !twig_test_empty((isset($context["infoMessage"]) || array_key_exists("infoMessage", $context) ? $context["infoMessage"] : (function () { throw new RuntimeError('Variable "infoMessage" does not exist.', 1, $this->source); })())))) {
            // line 2
            echo "    <p class=\"message\">";
            echo \Piwik\piwik_escape_filter($this->env, (isset($context["infoMessage"]) || array_key_exists("infoMessage", $context) ? $context["infoMessage"] : (function () { throw new RuntimeError('Variable "infoMessage" does not exist.', 2, $this->source); })()), "html", null, true);
            echo "</p>
";
        }
        // line 4
        $this->loadTemplate("@Login/_formErrors.twig", "@Login/resetPassword.twig", 4)->display($context);
    }

    public function getTemplateName()
    {
        return "@Login/resetPassword.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if infoMessage is defined and infoMessage is not empty %}
    <p class=\"message\">{{ infoMessage }}</p>
{% endif %}
{% include '@Login/_formErrors.twig' %}", "@Login/resetPassword.twig", "/opt/www/climate.setsocialimpact.com/public_html/analytics/plugins/Login/templates/resetPassword.twig");
    }
}
